﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Holder : MonoBehaviour 
{
    public GameObject myCanvas;

    public IEnumerator TurnOff() 
    {
        yield return new WaitForSeconds(7);
        myCanvas.SetActive(false);
    }
}
