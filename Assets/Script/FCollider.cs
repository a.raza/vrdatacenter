﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FCollider : MonoBehaviour {

    //public void OnCollisionEnter(Collision collision)
    //{
    //    Debug.Log(collision.gameObject.name);
    //    if (collision.gameObject.GetComponent<Holder>() != null) 
    //    {
    //        var h = collision.gameObject.GetComponent<Holder>();

    //        h.myCanvas.SetActive(true);
    //        Debug.Log("Collide");
    //    }
    //}

    public void OnCollisionStay(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.GetComponent<Holder>() != null)
        {
            var h = collision.gameObject.GetComponent<Holder>();

            h.myCanvas.SetActive(true);
            Debug.Log("Collide");
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.GetComponent<Holder>() != null)
        {
            var h = collision.gameObject.GetComponent<Holder>();
            if(h.myCanvas.activeInHierarchy == true)
            StartCoroutine(h.TurnOff());
            Debug.Log("Collided");
        }
    }


}
