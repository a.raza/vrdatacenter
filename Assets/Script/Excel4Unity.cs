﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Excel4Unity : MonoBehaviour
{
    public string exFileName;
    //private Excel xls;
    [SerializeField]
    public Data[] data;
    public GameObject server;
    private Dictionary<int, float> unitPosition = new Dictionary<int, float>();
    public Transform[] racks;
    private string[] rackNumbers;

    private static Excel4Unity instance = null;

    //Singleton
    public static Excel4Unity Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);

        UnitCalculation();
        //StartCoroutine(Read());
        rackNumbers = GetRackNumbers();
        racks = GetRacksAs3DObjects();

        Debug.Log("Least Amount Of Racks: "+ LeastAmountOfRacks());

        for (int i = 1; i < (int)data.Length/2; i++)
        {
            var rack = PrimaryRack(i);
            var infoHolder = PlaceServerOnRack(int.Parse(data[i].Start_At), int.Parse(data[i].High), rack);
            var textSpace = infoHolder.GetComponent<TextMeshProUGUI>();
            textSpace.text += data[i].Model_Brand + "\n";
            textSpace.text += data[i].Serial_Number + "\n";
            textSpace.text += data[i].Data_Centre + "\n";
            textSpace.text += data[i].Rack + "\n";

            var parent = textSpace.transform.parent;

            parent.SetParent(rack);
            parent.transform.localScale = new Vector3(0.6666667f, 1, 0.6666667f);
        }
    }

    private Transform PrimaryRack(int i)
    {
        for (int r = 0; r < racks.Length; r++)
        {
            if (data[i].Rack == racks[r].name)
            {
                return racks[r];
            }
        }

        return null;
    }

    //Reads the File and stores the data
    //public IEnumerator Read()
    //{
    //    //Put error if file not found
    //    string path = Application.dataPath + "/" + exFileName + ".xlsx";
    //    xls = ExcelHelper.LoadExcel(path);
    //    data = Details();

    //    Debug.Log("Data Limit: " + data.Length);

    //    yield return new WaitForSeconds(1);
    //}

    private Transform PlaceServerOnRack(int startAt, int endAt, Transform parent)
    {
        float start, end;
        unitPosition.TryGetValue(startAt, out start);
        unitPosition.TryGetValue(endAt, out end);

        //will put the server in the middle of StartAt/Low and High
        var dis = (start + end) / 2;

        //this will aid in the size of the server
        float height = start > end ? start - end : end - start;

        var obj = Instantiate(server);
        var returnObj = obj.transform.GetChild(0).Find("DataInfo");

        var x = server.transform.localPosition.x;
        var z = server.transform.localPosition.z;
        obj.transform.SetParent(parent);

        //Rotation of the server in rack
        obj.transform.localRotation = new Quaternion(0, 0, 0, 0);
        //Position of the server in rack
        obj.transform.localPosition = new Vector3(x, dis, z);
        //Size of the server in rack
        obj.transform.localScale = new Vector3(obj.transform.localScale.x, height- 0.01904761904762f, obj.transform.localScale.z);

        return returnObj; //sends the server
    }

    //private Data[] Details()
    //{
    //    var rows = Rows();
    //    var columns = Columns();

    //    List<Data> dataInfo = new List<Data>();

    //    for (int c = 2; c < rows; c++)
    //    {
    //        Data _data = new Data
    //        {
    //            Data_Centre = (string)xls.Tables[0].GetValue(c, 1),
    //            Rack = (string)xls.Tables[0].GetValue(c, 2),
    //            Low = (string)xls.Tables[0].GetValue(c, 3),
    //            High = (string)xls.Tables[0].GetValue(c, 4),
    //            Start_At = (string)xls.Tables[0].GetValue(c, 5),
    //            Number_Of_U = (string)xls.Tables[0].GetValue(c, 6),
    //            CI_Name = (string)xls.Tables[0].GetValue(c, 7),
    //            Model_Brand = (string)xls.Tables[0].GetValue(c, 8),
    //            ModelNature = (string)xls.Tables[0].GetValue(c, 9),
    //            Category_FullName = (string)xls.Tables[0].GetValue(c, 10),
    //            Model = (string)xls.Tables[0].GetValue(c, 11),
    //            CI_Description = (string)xls.Tables[0].GetValue(c, 12),
    //            Xreference = (string)xls.Tables[0].GetValue(c, 13),
    //            Criticality = (string)xls.Tables[0].GetValue(c, 14),
    //            Serial_Number = (string)xls.Tables[0].GetValue(c, 15),
    //            Bay_Number = (string)xls.Tables[0].GetValue(c, 16)
    //        };

    //        dataInfo.Add(_data);

    //    }

    //    return dataInfo.ToArray();
    //}

    //private int Columns()
    //{
    //    var columns = 0;

    //    for (int i = 2; i < xls.Tables[0].NumberOfColumns; i++)
    //    {
    //        columns = i;
    //    }

    //    Debug.Log("columns :" + columns);

    //    return columns;
    //}

    ////gets the Max number of Column
    //private int Rows()
    //{
    //    var rows = 0;
    //    for (int i = 2; i < xls.Tables[0].NumberOfRows; i++)
    //    {
    //        rows = i;
    //    }

    //    Debug.Log("rows :" + rows);

    //    return rows;
    //}

    //Since there is no way to measure Unit in 3D world,
    //Following uses the size of the rack in the 3D world and stores 42 U position according ly
    private void UnitCalculation()
    {
        //the size in the rack is 4.6f
        //Starts from 2.4f to -2.2f

        float current = -2.2f; //we starts from the bottom

        for (int i = 0; i <= 42; i++) //as there are (42 U) in the rack
        {
            //Debug.Log("Index: " + i);
            //Debug.Log("Current: " + current);
            unitPosition.Add(i, current);
            current += 0.10952380952381f;
        }
    }

    // gets the rack available in the 3D datacenter
    private Transform[] GetRacksAs3DObjects()
    {
        List<Transform> _racks = new List<Transform>();

        int i = 0;
        foreach(Transform rack in FindObjectsOfType<Transform>()) 
        {
            if (rack.tag == "rack")
            {
                rack.name = rackNumbers[i];
                _racks.Add(rack);
                i++;
            }
        }

        Debug.Log("Amount Rack Available In 3D DataCenter: " + _racks.Count);

        return _racks.ToArray();
    }

    private string[] GetRackNumbers() 
    {
        List<string> rackNumbers = new List<string>();

        for(int i = 0; i < data.Length; i++) 
        {
            if (!rackNumbers.Contains(data[i].Rack)) 
            {
                rackNumbers.Add(data[i].Rack);
            }
        }

        return rackNumbers.ToArray();
    }

    //Gets the least amount of racks that can be place in the 3D datacenter
    private int LeastAmountOfRacks()
    {
        int rackObjsAmount = racks.Length;
        int rackAmountInFile = rackNumbers.Length;

        return rackObjsAmount > rackAmountInFile ? rackAmountInFile : rackObjsAmount;
    }
}


[System.Serializable]
public struct Data
{
    public string
    Data_Centre,
    Rack,
    Low,
    High,
    Start_At,
    Number_Of_U,
    CI_Name,
    Model_Brand,
    ModelNature,
    Category_FullName,
    Model,
    CI_Description,
    Xreference,
    Criticality,
    Serial_Number,
    Bay_Number;
}


public enum DataCenterInfo // not using it but you never know
{
    Data_Centre = 1,
    Rack = 2,
    Low = 3,
    High = 4,
    Start_At = 5,
    Number_Of_U = 6,
    CI_Name = 7,
    Model_Brand = 8,
    ModelNature = 9,
    Category_FullName = 10,
    Model = 11,
    CI_Description = 12,
    Xreference = 13,
    Criticality = 14,
    Serial_Number = 15,
    Bay_Number = 16,

};
