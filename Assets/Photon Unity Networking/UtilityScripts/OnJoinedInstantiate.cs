using UnityEngine;
using System.Collections;

public class OnJoinedInstantiate : MonoBehaviour
{
    public Transform SpawnPosition;
    public string m_cam, m_controller;
    //public bool m_needController;

    public void OnJoinedRoom()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
            GetObj(m_cam);
        else
            GetObj(m_controller);


        //if(m_needController == true)
        //{
        //    GetObj(m_controller);
        //}
        //else
        //{
        //    GetObj(m_cam);
        //}

        //if(m_needController == true)
        //{
        //    m_needController = false;
        //}
        //else
        //{
        //    m_needController = true;
        //}
    }

    void GetObj(string _obj)
    {
        PhotonNetwork.Instantiate(_obj, SpawnPosition.position, Quaternion.identity, 0);
    }
}
