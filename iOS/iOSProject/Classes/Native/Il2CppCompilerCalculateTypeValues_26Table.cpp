﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t3546202735;
// System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>
struct Dictionary_2_t1466267835;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// ExitGames.Client.Photon.SerializeStreamMethod
struct SerializeStreamMethod_t2169445464;
// ExitGames.Client.Photon.DeserializeStreamMethod
struct DeserializeStreamMethod_t3070531629;
// RpsCore
struct RpsCore_t2154144697;
// DemoBoxesGui
struct DemoBoxesGui_t3999198613;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// OnClickFlashRpc
struct OnClickFlashRpc_t2987886900;
// UnityEngine.Collider
struct Collider_t1773347010;
// DragToMove
struct DragToMove_t3834199052;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;
// UnityEngine.Animation
struct Animation_t3648466861;
// UnityEngine.GUIText
struct GUIText_t402233326;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.Transform
struct Transform_t3600365921;
// ThirdPersonController
struct ThirdPersonController_t2544474708;
// UnityEngine.Camera
struct Camera_t4157153871;
// PhotonView
struct PhotonView_t2207721820;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Animator
struct Animator_t434523843;
// PhotonTransformView
struct PhotonTransformView_t372465615;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// ExitGames.Demos.DemoAnimator.PlayerManager
struct PlayerManager_t3964432985;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// System.Collections.Generic.Dictionary`2<System.String,ExitGames.Demos.DemoHubManager/DemoData>
struct Dictionary_2_t994373823;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// ExitGames.Client.Photon.Chat.ChatClient
struct ChatClient_t3322764984;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UI.Toggle>
struct Dictionary_2_t2520633360;
// System.Collections.Generic.Dictionary`2<System.String,FriendItem>
struct Dictionary_2_t2701966392;
// ChatGui
struct ChatGui_t3673337520;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.UI.Button
struct Button_t4055032469;
// RPGCamera
struct RPGCamera_t2077525570;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Font
struct Font_t1956802104;
// ThirdPersonCamera
struct ThirdPersonCamera_t2998681409;
// PickupController
struct PickupController_t2983846149;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// CubeInter/State[]
struct StateU5BU5D_t3626104901;
// ColorPerPlayer
struct ColorPerPlayer_t432550946;
// UnityEngine.Material
struct Material_t340375123;
// PhotonAnimatorView
struct PhotonAnimatorView_t3352472062;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// PunTurnManager
struct PunTurnManager_t1223962931;
// ExitGames.Demos.DemoAnimator.LoaderAnime
struct LoaderAnime_t64665300;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CINTERNALONVIDEOEVENTCALLBACKU3EC__ANONSTOREY0_T2222373319_H
#define U3CINTERNALONVIDEOEVENTCALLBACKU3EC__ANONSTOREY0_T2222373319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey0
struct  U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319  : public RuntimeObject
{
public:
	// GvrVideoPlayerTexture GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey0::player
	GvrVideoPlayerTexture_t3546202735 * ___player_0;
	// System.Int32 GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey0::eventId
	int32_t ___eventId_1;

public:
	inline static int32_t get_offset_of_player_0() { return static_cast<int32_t>(offsetof(U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319, ___player_0)); }
	inline GvrVideoPlayerTexture_t3546202735 * get_player_0() const { return ___player_0; }
	inline GvrVideoPlayerTexture_t3546202735 ** get_address_of_player_0() { return &___player_0; }
	inline void set_player_0(GvrVideoPlayerTexture_t3546202735 * value)
	{
		___player_0 = value;
		Il2CppCodeGenWriteBarrier((&___player_0), value);
	}

	inline static int32_t get_offset_of_eventId_1() { return static_cast<int32_t>(offsetof(U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319, ___eventId_1)); }
	inline int32_t get_eventId_1() const { return ___eventId_1; }
	inline int32_t* get_address_of_eventId_1() { return &___eventId_1; }
	inline void set_eventId_1(int32_t value)
	{
		___eventId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALONVIDEOEVENTCALLBACKU3EC__ANONSTOREY0_T2222373319_H
#ifndef GAMEOBJECTEXTENSIONS_T182180175_H
#define GAMEOBJECTEXTENSIONS_T182180175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectExtensions
struct  GameObjectExtensions_t182180175  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSIONS_T182180175_H
#ifndef EXTENSIONS_T2612146612_H
#define EXTENSIONS_T2612146612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Extensions
struct  Extensions_t2612146612  : public RuntimeObject
{
public:

public:
};

struct Extensions_t2612146612_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]> Extensions::ParametersOfMethods
	Dictionary_2_t1466267835 * ___ParametersOfMethods_0;

public:
	inline static int32_t get_offset_of_ParametersOfMethods_0() { return static_cast<int32_t>(offsetof(Extensions_t2612146612_StaticFields, ___ParametersOfMethods_0)); }
	inline Dictionary_2_t1466267835 * get_ParametersOfMethods_0() const { return ___ParametersOfMethods_0; }
	inline Dictionary_2_t1466267835 ** get_address_of_ParametersOfMethods_0() { return &___ParametersOfMethods_0; }
	inline void set_ParametersOfMethods_0(Dictionary_2_t1466267835 * value)
	{
		___ParametersOfMethods_0 = value;
		Il2CppCodeGenWriteBarrier((&___ParametersOfMethods_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T2612146612_H
#ifndef ENCRYPTIONDATAPARAMETERS_T25380775_H
#define ENCRYPTIONDATAPARAMETERS_T25380775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EncryptionDataParameters
struct  EncryptionDataParameters_t25380775  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTIONDATAPARAMETERS_T25380775_H
#ifndef CUSTOMTYPES_T2914914968_H
#define CUSTOMTYPES_T2914914968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTypes
struct  CustomTypes_t2914914968  : public RuntimeObject
{
public:

public:
};

struct CustomTypes_t2914914968_StaticFields
{
public:
	// System.Byte[] CustomTypes::memVector3
	ByteU5BU5D_t4116647657* ___memVector3_0;
	// System.Byte[] CustomTypes::memVector2
	ByteU5BU5D_t4116647657* ___memVector2_1;
	// System.Byte[] CustomTypes::memQuarternion
	ByteU5BU5D_t4116647657* ___memQuarternion_2;
	// System.Byte[] CustomTypes::memPlayer
	ByteU5BU5D_t4116647657* ___memPlayer_3;
	// ExitGames.Client.Photon.SerializeStreamMethod CustomTypes::<>f__mg$cache0
	SerializeStreamMethod_t2169445464 * ___U3CU3Ef__mgU24cache0_4;
	// ExitGames.Client.Photon.DeserializeStreamMethod CustomTypes::<>f__mg$cache1
	DeserializeStreamMethod_t3070531629 * ___U3CU3Ef__mgU24cache1_5;
	// ExitGames.Client.Photon.SerializeStreamMethod CustomTypes::<>f__mg$cache2
	SerializeStreamMethod_t2169445464 * ___U3CU3Ef__mgU24cache2_6;
	// ExitGames.Client.Photon.DeserializeStreamMethod CustomTypes::<>f__mg$cache3
	DeserializeStreamMethod_t3070531629 * ___U3CU3Ef__mgU24cache3_7;
	// ExitGames.Client.Photon.SerializeStreamMethod CustomTypes::<>f__mg$cache4
	SerializeStreamMethod_t2169445464 * ___U3CU3Ef__mgU24cache4_8;
	// ExitGames.Client.Photon.DeserializeStreamMethod CustomTypes::<>f__mg$cache5
	DeserializeStreamMethod_t3070531629 * ___U3CU3Ef__mgU24cache5_9;
	// ExitGames.Client.Photon.SerializeStreamMethod CustomTypes::<>f__mg$cache6
	SerializeStreamMethod_t2169445464 * ___U3CU3Ef__mgU24cache6_10;
	// ExitGames.Client.Photon.DeserializeStreamMethod CustomTypes::<>f__mg$cache7
	DeserializeStreamMethod_t3070531629 * ___U3CU3Ef__mgU24cache7_11;

public:
	inline static int32_t get_offset_of_memVector3_0() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___memVector3_0)); }
	inline ByteU5BU5D_t4116647657* get_memVector3_0() const { return ___memVector3_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_memVector3_0() { return &___memVector3_0; }
	inline void set_memVector3_0(ByteU5BU5D_t4116647657* value)
	{
		___memVector3_0 = value;
		Il2CppCodeGenWriteBarrier((&___memVector3_0), value);
	}

	inline static int32_t get_offset_of_memVector2_1() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___memVector2_1)); }
	inline ByteU5BU5D_t4116647657* get_memVector2_1() const { return ___memVector2_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_memVector2_1() { return &___memVector2_1; }
	inline void set_memVector2_1(ByteU5BU5D_t4116647657* value)
	{
		___memVector2_1 = value;
		Il2CppCodeGenWriteBarrier((&___memVector2_1), value);
	}

	inline static int32_t get_offset_of_memQuarternion_2() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___memQuarternion_2)); }
	inline ByteU5BU5D_t4116647657* get_memQuarternion_2() const { return ___memQuarternion_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_memQuarternion_2() { return &___memQuarternion_2; }
	inline void set_memQuarternion_2(ByteU5BU5D_t4116647657* value)
	{
		___memQuarternion_2 = value;
		Il2CppCodeGenWriteBarrier((&___memQuarternion_2), value);
	}

	inline static int32_t get_offset_of_memPlayer_3() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___memPlayer_3)); }
	inline ByteU5BU5D_t4116647657* get_memPlayer_3() const { return ___memPlayer_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_memPlayer_3() { return &___memPlayer_3; }
	inline void set_memPlayer_3(ByteU5BU5D_t4116647657* value)
	{
		___memPlayer_3 = value;
		Il2CppCodeGenWriteBarrier((&___memPlayer_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline SerializeStreamMethod_t2169445464 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline SerializeStreamMethod_t2169445464 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(SerializeStreamMethod_t2169445464 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_5() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache1_5)); }
	inline DeserializeStreamMethod_t3070531629 * get_U3CU3Ef__mgU24cache1_5() const { return ___U3CU3Ef__mgU24cache1_5; }
	inline DeserializeStreamMethod_t3070531629 ** get_address_of_U3CU3Ef__mgU24cache1_5() { return &___U3CU3Ef__mgU24cache1_5; }
	inline void set_U3CU3Ef__mgU24cache1_5(DeserializeStreamMethod_t3070531629 * value)
	{
		___U3CU3Ef__mgU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_6() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache2_6)); }
	inline SerializeStreamMethod_t2169445464 * get_U3CU3Ef__mgU24cache2_6() const { return ___U3CU3Ef__mgU24cache2_6; }
	inline SerializeStreamMethod_t2169445464 ** get_address_of_U3CU3Ef__mgU24cache2_6() { return &___U3CU3Ef__mgU24cache2_6; }
	inline void set_U3CU3Ef__mgU24cache2_6(SerializeStreamMethod_t2169445464 * value)
	{
		___U3CU3Ef__mgU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_7() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache3_7)); }
	inline DeserializeStreamMethod_t3070531629 * get_U3CU3Ef__mgU24cache3_7() const { return ___U3CU3Ef__mgU24cache3_7; }
	inline DeserializeStreamMethod_t3070531629 ** get_address_of_U3CU3Ef__mgU24cache3_7() { return &___U3CU3Ef__mgU24cache3_7; }
	inline void set_U3CU3Ef__mgU24cache3_7(DeserializeStreamMethod_t3070531629 * value)
	{
		___U3CU3Ef__mgU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_8() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache4_8)); }
	inline SerializeStreamMethod_t2169445464 * get_U3CU3Ef__mgU24cache4_8() const { return ___U3CU3Ef__mgU24cache4_8; }
	inline SerializeStreamMethod_t2169445464 ** get_address_of_U3CU3Ef__mgU24cache4_8() { return &___U3CU3Ef__mgU24cache4_8; }
	inline void set_U3CU3Ef__mgU24cache4_8(SerializeStreamMethod_t2169445464 * value)
	{
		___U3CU3Ef__mgU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_9() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache5_9)); }
	inline DeserializeStreamMethod_t3070531629 * get_U3CU3Ef__mgU24cache5_9() const { return ___U3CU3Ef__mgU24cache5_9; }
	inline DeserializeStreamMethod_t3070531629 ** get_address_of_U3CU3Ef__mgU24cache5_9() { return &___U3CU3Ef__mgU24cache5_9; }
	inline void set_U3CU3Ef__mgU24cache5_9(DeserializeStreamMethod_t3070531629 * value)
	{
		___U3CU3Ef__mgU24cache5_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_10() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache6_10)); }
	inline SerializeStreamMethod_t2169445464 * get_U3CU3Ef__mgU24cache6_10() const { return ___U3CU3Ef__mgU24cache6_10; }
	inline SerializeStreamMethod_t2169445464 ** get_address_of_U3CU3Ef__mgU24cache6_10() { return &___U3CU3Ef__mgU24cache6_10; }
	inline void set_U3CU3Ef__mgU24cache6_10(SerializeStreamMethod_t2169445464 * value)
	{
		___U3CU3Ef__mgU24cache6_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_11() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache7_11)); }
	inline DeserializeStreamMethod_t3070531629 * get_U3CU3Ef__mgU24cache7_11() const { return ___U3CU3Ef__mgU24cache7_11; }
	inline DeserializeStreamMethod_t3070531629 ** get_address_of_U3CU3Ef__mgU24cache7_11() { return &___U3CU3Ef__mgU24cache7_11; }
	inline void set_U3CU3Ef__mgU24cache7_11(DeserializeStreamMethod_t3070531629 * value)
	{
		___U3CU3Ef__mgU24cache7_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTYPES_T2914914968_H
#ifndef U3CCYCLEREMOTEHANDCOROUTINEU3EC__ITERATOR1_T811054456_H
#define U3CCYCLEREMOTEHANDCOROUTINEU3EC__ITERATOR1_T811054456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore/<CycleRemoteHandCoroutine>c__Iterator1
struct  U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456  : public RuntimeObject
{
public:
	// RpsCore RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::$this
	RpsCore_t2154144697 * ___U24this_0;
	// System.Object RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456, ___U24this_0)); }
	inline RpsCore_t2154144697 * get_U24this_0() const { return ___U24this_0; }
	inline RpsCore_t2154144697 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RpsCore_t2154144697 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCYCLEREMOTEHANDCOROUTINEU3EC__ITERATOR1_T811054456_H
#ifndef U3CSHOWRESULTSBEGINNEXTTURNCOROUTINEU3EC__ITERATOR0_T2002201197_H
#define U3CSHOWRESULTSBEGINNEXTTURNCOROUTINEU3EC__ITERATOR0_T2002201197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0
struct  U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197  : public RuntimeObject
{
public:
	// RpsCore RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::$this
	RpsCore_t2154144697 * ___U24this_0;
	// System.Object RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197, ___U24this_0)); }
	inline RpsCore_t2154144697 * get_U24this_0() const { return ___U24this_0; }
	inline RpsCore_t2154144697 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RpsCore_t2154144697 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWRESULTSBEGINNEXTTURNCOROUTINEU3EC__ITERATOR0_T2002201197_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSWAPTIPU3EC__ITERATOR0_T141855054_H
#define U3CSWAPTIPU3EC__ITERATOR0_T141855054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoBoxesGui/<SwapTip>c__Iterator0
struct  U3CSwapTipU3Ec__Iterator0_t141855054  : public RuntimeObject
{
public:
	// System.Single DemoBoxesGui/<SwapTip>c__Iterator0::<alpha>__0
	float ___U3CalphaU3E__0_0;
	// DemoBoxesGui DemoBoxesGui/<SwapTip>c__Iterator0::$this
	DemoBoxesGui_t3999198613 * ___U24this_1;
	// System.Object DemoBoxesGui/<SwapTip>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean DemoBoxesGui/<SwapTip>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 DemoBoxesGui/<SwapTip>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CalphaU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSwapTipU3Ec__Iterator0_t141855054, ___U3CalphaU3E__0_0)); }
	inline float get_U3CalphaU3E__0_0() const { return ___U3CalphaU3E__0_0; }
	inline float* get_address_of_U3CalphaU3E__0_0() { return &___U3CalphaU3E__0_0; }
	inline void set_U3CalphaU3E__0_0(float value)
	{
		___U3CalphaU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSwapTipU3Ec__Iterator0_t141855054, ___U24this_1)); }
	inline DemoBoxesGui_t3999198613 * get_U24this_1() const { return ___U24this_1; }
	inline DemoBoxesGui_t3999198613 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DemoBoxesGui_t3999198613 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSwapTipU3Ec__Iterator0_t141855054, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSwapTipU3Ec__Iterator0_t141855054, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSwapTipU3Ec__Iterator0_t141855054, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSWAPTIPU3EC__ITERATOR0_T141855054_H
#ifndef U3CINTERNALONEXCEPTIONCALLBACKU3EC__ANONSTOREY1_T3301768987_H
#define U3CINTERNALONEXCEPTIONCALLBACKU3EC__ANONSTOREY1_T3301768987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey1
struct  U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987  : public RuntimeObject
{
public:
	// GvrVideoPlayerTexture GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey1::player
	GvrVideoPlayerTexture_t3546202735 * ___player_0;
	// System.String GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey1::type
	String_t* ___type_1;
	// System.String GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey1::msg
	String_t* ___msg_2;

public:
	inline static int32_t get_offset_of_player_0() { return static_cast<int32_t>(offsetof(U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987, ___player_0)); }
	inline GvrVideoPlayerTexture_t3546202735 * get_player_0() const { return ___player_0; }
	inline GvrVideoPlayerTexture_t3546202735 ** get_address_of_player_0() { return &___player_0; }
	inline void set_player_0(GvrVideoPlayerTexture_t3546202735 * value)
	{
		___player_0 = value;
		Il2CppCodeGenWriteBarrier((&___player_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_msg_2() { return static_cast<int32_t>(offsetof(U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987, ___msg_2)); }
	inline String_t* get_msg_2() const { return ___msg_2; }
	inline String_t** get_address_of_msg_2() { return &___msg_2; }
	inline void set_msg_2(String_t* value)
	{
		___msg_2 = value;
		Il2CppCodeGenWriteBarrier((&___msg_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALONEXCEPTIONCALLBACKU3EC__ANONSTOREY1_T3301768987_H
#ifndef DEMOBTN_T3232561033_H
#define DEMOBTN_T3232561033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HubGui/DemoBtn
struct  DemoBtn_t3232561033 
{
public:
	// System.String HubGui/DemoBtn::Text
	String_t* ___Text_0;
	// System.String HubGui/DemoBtn::Link
	String_t* ___Link_1;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(DemoBtn_t3232561033, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___Text_0), value);
	}

	inline static int32_t get_offset_of_Link_1() { return static_cast<int32_t>(offsetof(DemoBtn_t3232561033, ___Link_1)); }
	inline String_t* get_Link_1() const { return ___Link_1; }
	inline String_t** get_address_of_Link_1() { return &___Link_1; }
	inline void set_Link_1(String_t* value)
	{
		___Link_1 = value;
		Il2CppCodeGenWriteBarrier((&___Link_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HubGui/DemoBtn
struct DemoBtn_t3232561033_marshaled_pinvoke
{
	char* ___Text_0;
	char* ___Link_1;
};
// Native definition for COM marshalling of HubGui/DemoBtn
struct DemoBtn_t3232561033_marshaled_com
{
	Il2CppChar* ___Text_0;
	Il2CppChar* ___Link_1;
};
#endif // DEMOBTN_T3232561033_H
#ifndef DEMODATA_T1209117524_H
#define DEMODATA_T1209117524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoHubManager/DemoData
struct  DemoData_t1209117524 
{
public:
	// System.String ExitGames.Demos.DemoHubManager/DemoData::Title
	String_t* ___Title_0;
	// System.String ExitGames.Demos.DemoHubManager/DemoData::Description
	String_t* ___Description_1;
	// System.String ExitGames.Demos.DemoHubManager/DemoData::Scene
	String_t* ___Scene_2;
	// System.String ExitGames.Demos.DemoHubManager/DemoData::TutorialLink
	String_t* ___TutorialLink_3;
	// System.String ExitGames.Demos.DemoHubManager/DemoData::DocLink
	String_t* ___DocLink_4;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(DemoData_t1209117524, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier((&___Title_0), value);
	}

	inline static int32_t get_offset_of_Description_1() { return static_cast<int32_t>(offsetof(DemoData_t1209117524, ___Description_1)); }
	inline String_t* get_Description_1() const { return ___Description_1; }
	inline String_t** get_address_of_Description_1() { return &___Description_1; }
	inline void set_Description_1(String_t* value)
	{
		___Description_1 = value;
		Il2CppCodeGenWriteBarrier((&___Description_1), value);
	}

	inline static int32_t get_offset_of_Scene_2() { return static_cast<int32_t>(offsetof(DemoData_t1209117524, ___Scene_2)); }
	inline String_t* get_Scene_2() const { return ___Scene_2; }
	inline String_t** get_address_of_Scene_2() { return &___Scene_2; }
	inline void set_Scene_2(String_t* value)
	{
		___Scene_2 = value;
		Il2CppCodeGenWriteBarrier((&___Scene_2), value);
	}

	inline static int32_t get_offset_of_TutorialLink_3() { return static_cast<int32_t>(offsetof(DemoData_t1209117524, ___TutorialLink_3)); }
	inline String_t* get_TutorialLink_3() const { return ___TutorialLink_3; }
	inline String_t** get_address_of_TutorialLink_3() { return &___TutorialLink_3; }
	inline void set_TutorialLink_3(String_t* value)
	{
		___TutorialLink_3 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialLink_3), value);
	}

	inline static int32_t get_offset_of_DocLink_4() { return static_cast<int32_t>(offsetof(DemoData_t1209117524, ___DocLink_4)); }
	inline String_t* get_DocLink_4() const { return ___DocLink_4; }
	inline String_t** get_address_of_DocLink_4() { return &___DocLink_4; }
	inline void set_DocLink_4(String_t* value)
	{
		___DocLink_4 = value;
		Il2CppCodeGenWriteBarrier((&___DocLink_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ExitGames.Demos.DemoHubManager/DemoData
struct DemoData_t1209117524_marshaled_pinvoke
{
	char* ___Title_0;
	char* ___Description_1;
	char* ___Scene_2;
	char* ___TutorialLink_3;
	char* ___DocLink_4;
};
// Native definition for COM marshalling of ExitGames.Demos.DemoHubManager/DemoData
struct DemoData_t1209117524_marshaled_com
{
	Il2CppChar* ___Title_0;
	Il2CppChar* ___Description_1;
	Il2CppChar* ___Scene_2;
	Il2CppChar* ___TutorialLink_3;
	Il2CppChar* ___DocLink_4;
};
#endif // DEMODATA_T1209117524_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef CLOUDREGIONFLAG_T3756941471_H
#define CLOUDREGIONFLAG_T3756941471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRegionFlag
struct  CloudRegionFlag_t3756941471 
{
public:
	// System.Int32 CloudRegionFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudRegionFlag_t3756941471, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDREGIONFLAG_T3756941471_H
#ifndef ENCRYPTIONMODE_T4213192103_H
#define ENCRYPTIONMODE_T4213192103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EncryptionMode
struct  EncryptionMode_t4213192103 
{
public:
	// System.Int32 EncryptionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EncryptionMode_t4213192103, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTIONMODE_T4213192103_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef COLLISIONFLAGS_T1776808576_H
#define COLLISIONFLAGS_T1776808576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CollisionFlags
struct  CollisionFlags_t1776808576 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionFlags_t1776808576, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONFLAGS_T1776808576_H
#ifndef U3CFLASHU3EC__ITERATOR0_T2775285351_H
#define U3CFLASHU3EC__ITERATOR0_T2775285351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickFlashRpc/<Flash>c__Iterator0
struct  U3CFlashU3Ec__Iterator0_t2775285351  : public RuntimeObject
{
public:
	// System.Single OnClickFlashRpc/<Flash>c__Iterator0::<f>__1
	float ___U3CfU3E__1_0;
	// UnityEngine.Color OnClickFlashRpc/<Flash>c__Iterator0::<lerped>__2
	Color_t2555686324  ___U3ClerpedU3E__2_1;
	// OnClickFlashRpc OnClickFlashRpc/<Flash>c__Iterator0::$this
	OnClickFlashRpc_t2987886900 * ___U24this_2;
	// System.Object OnClickFlashRpc/<Flash>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean OnClickFlashRpc/<Flash>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 OnClickFlashRpc/<Flash>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CfU3E__1_0() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2775285351, ___U3CfU3E__1_0)); }
	inline float get_U3CfU3E__1_0() const { return ___U3CfU3E__1_0; }
	inline float* get_address_of_U3CfU3E__1_0() { return &___U3CfU3E__1_0; }
	inline void set_U3CfU3E__1_0(float value)
	{
		___U3CfU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3ClerpedU3E__2_1() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2775285351, ___U3ClerpedU3E__2_1)); }
	inline Color_t2555686324  get_U3ClerpedU3E__2_1() const { return ___U3ClerpedU3E__2_1; }
	inline Color_t2555686324 * get_address_of_U3ClerpedU3E__2_1() { return &___U3ClerpedU3E__2_1; }
	inline void set_U3ClerpedU3E__2_1(Color_t2555686324  value)
	{
		___U3ClerpedU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2775285351, ___U24this_2)); }
	inline OnClickFlashRpc_t2987886900 * get_U24this_2() const { return ___U24this_2; }
	inline OnClickFlashRpc_t2987886900 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(OnClickFlashRpc_t2987886900 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2775285351, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2775285351, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2775285351, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFLASHU3EC__ITERATOR0_T2775285351_H
#ifndef RESOURCETYPEOPTION_T3060300527_H
#define RESOURCETYPEOPTION_T3060300527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickLoadSomething/ResourceTypeOption
struct  ResourceTypeOption_t3060300527 
{
public:
	// System.Byte OnClickLoadSomething/ResourceTypeOption::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ResourceTypeOption_t3060300527, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCETYPEOPTION_T3060300527_H
#ifndef CHARACTERSTATE_T1684161606_H
#define CHARACTERSTATE_T1684161606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterState
struct  CharacterState_t1684161606 
{
public:
	// System.Int32 CharacterState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharacterState_t1684161606, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSTATE_T1684161606_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef PICKUPCHARACTERSTATE_T636162321_H
#define PICKUPCHARACTERSTATE_T636162321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickupCharacterState
struct  PickupCharacterState_t636162321 
{
public:
	// System.Int32 PickupCharacterState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PickupCharacterState_t636162321, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUPCHARACTERSTATE_T636162321_H
#ifndef RESULTTYPE_T940288093_H
#define RESULTTYPE_T940288093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore/ResultType
struct  ResultType_t940288093 
{
public:
	// System.Int32 RpsCore/ResultType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ResultType_t940288093, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTTYPE_T940288093_H
#ifndef HAND_T2784208099_H
#define HAND_T2784208099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore/Hand
struct  Hand_t2784208099 
{
public:
	// System.Int32 RpsCore/Hand::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Hand_t2784208099, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HAND_T2784208099_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef STATE_T1609032204_H
#define STATE_T1609032204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CubeInter/State
struct  State_t1609032204 
{
public:
	// System.Double CubeInter/State::timestamp
	double ___timestamp_0;
	// UnityEngine.Vector3 CubeInter/State::pos
	Vector3_t3722313464  ___pos_1;
	// UnityEngine.Quaternion CubeInter/State::rot
	Quaternion_t2301928331  ___rot_2;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(State_t1609032204, ___timestamp_0)); }
	inline double get_timestamp_0() const { return ___timestamp_0; }
	inline double* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(double value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(State_t1609032204, ___pos_1)); }
	inline Vector3_t3722313464  get_pos_1() const { return ___pos_1; }
	inline Vector3_t3722313464 * get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(Vector3_t3722313464  value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_rot_2() { return static_cast<int32_t>(offsetof(State_t1609032204, ___rot_2)); }
	inline Quaternion_t2301928331  get_rot_2() const { return ___rot_2; }
	inline Quaternion_t2301928331 * get_address_of_rot_2() { return &___rot_2; }
	inline void set_rot_2(Quaternion_t2301928331  value)
	{
		___rot_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T1609032204_H
#ifndef PHOTONTARGETS_T2730697525_H
#define PHOTONTARGETS_T2730697525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTargets
struct  PhotonTargets_t2730697525 
{
public:
	// System.Int32 PhotonTargets::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonTargets_t2730697525, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONTARGETS_T2730697525_H
#ifndef CONNECTIONSTATE_T836644691_H
#define CONNECTIONSTATE_T836644691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectionState
struct  ConnectionState_t836644691 
{
public:
	// System.Int32 ConnectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionState_t836644691, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONSTATE_T836644691_H
#ifndef GUISTATE_T1902662126_H
#define GUISTATE_T1902662126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUICustomAuth/GuiState
struct  GuiState_t1902662126 
{
public:
	// System.Int32 GUICustomAuth/GuiState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GuiState_t1902662126, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISTATE_T1902662126_H
#ifndef TEAM_T2865224648_H
#define TEAM_T2865224648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PunTeams/Team
struct  Team_t2865224648 
{
public:
	// System.Byte PunTeams/Team::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Team_t2865224648, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAM_T2865224648_H
#ifndef CLOUDREGIONCODE_T1925019500_H
#define CLOUDREGIONCODE_T1925019500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRegionCode
struct  CloudRegionCode_t1925019500 
{
public:
	// System.Int32 CloudRegionCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudRegionCode_t1925019500, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDREGIONCODE_T1925019500_H
#ifndef PHOTONNETWORKINGMESSAGE_T1476457985_H
#define PHOTONNETWORKINGMESSAGE_T1476457985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonNetworkingMessage
struct  PhotonNetworkingMessage_t1476457985 
{
public:
	// System.Int32 PhotonNetworkingMessage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonNetworkingMessage_t1476457985, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONNETWORKINGMESSAGE_T1476457985_H
#ifndef PHOTONLOGLEVEL_T4226222036_H
#define PHOTONLOGLEVEL_T4226222036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonLogLevel
struct  PhotonLogLevel_t4226222036 
{
public:
	// System.Int32 PhotonLogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonLogLevel_t4226222036, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONLOGLEVEL_T4226222036_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef U3CRECORDMOUSEU3EC__ITERATOR0_T1035515074_H
#define U3CRECORDMOUSEU3EC__ITERATOR0_T1035515074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragToMove/<RecordMouse>c__Iterator0
struct  U3CRecordMouseU3Ec__Iterator0_t1035515074  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DragToMove/<RecordMouse>c__Iterator0::<v3>__1
	Vector3_t3722313464  ___U3Cv3U3E__1_0;
	// UnityEngine.Ray DragToMove/<RecordMouse>c__Iterator0::<inputRay>__1
	Ray_t3785851493  ___U3CinputRayU3E__1_1;
	// UnityEngine.RaycastHit DragToMove/<RecordMouse>c__Iterator0::<hit>__1
	RaycastHit_t1056001966  ___U3ChitU3E__1_2;
	// DragToMove DragToMove/<RecordMouse>c__Iterator0::$this
	DragToMove_t3834199052 * ___U24this_3;
	// System.Object DragToMove/<RecordMouse>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean DragToMove/<RecordMouse>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 DragToMove/<RecordMouse>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3Cv3U3E__1_0() { return static_cast<int32_t>(offsetof(U3CRecordMouseU3Ec__Iterator0_t1035515074, ___U3Cv3U3E__1_0)); }
	inline Vector3_t3722313464  get_U3Cv3U3E__1_0() const { return ___U3Cv3U3E__1_0; }
	inline Vector3_t3722313464 * get_address_of_U3Cv3U3E__1_0() { return &___U3Cv3U3E__1_0; }
	inline void set_U3Cv3U3E__1_0(Vector3_t3722313464  value)
	{
		___U3Cv3U3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CinputRayU3E__1_1() { return static_cast<int32_t>(offsetof(U3CRecordMouseU3Ec__Iterator0_t1035515074, ___U3CinputRayU3E__1_1)); }
	inline Ray_t3785851493  get_U3CinputRayU3E__1_1() const { return ___U3CinputRayU3E__1_1; }
	inline Ray_t3785851493 * get_address_of_U3CinputRayU3E__1_1() { return &___U3CinputRayU3E__1_1; }
	inline void set_U3CinputRayU3E__1_1(Ray_t3785851493  value)
	{
		___U3CinputRayU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3ChitU3E__1_2() { return static_cast<int32_t>(offsetof(U3CRecordMouseU3Ec__Iterator0_t1035515074, ___U3ChitU3E__1_2)); }
	inline RaycastHit_t1056001966  get_U3ChitU3E__1_2() const { return ___U3ChitU3E__1_2; }
	inline RaycastHit_t1056001966 * get_address_of_U3ChitU3E__1_2() { return &___U3ChitU3E__1_2; }
	inline void set_U3ChitU3E__1_2(RaycastHit_t1056001966  value)
	{
		___U3ChitU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CRecordMouseU3Ec__Iterator0_t1035515074, ___U24this_3)); }
	inline DragToMove_t3834199052 * get_U24this_3() const { return ___U24this_3; }
	inline DragToMove_t3834199052 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(DragToMove_t3834199052 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CRecordMouseU3Ec__Iterator0_t1035515074, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CRecordMouseU3Ec__Iterator0_t1035515074, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CRecordMouseU3Ec__Iterator0_t1035515074, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECORDMOUSEU3EC__ITERATOR0_T1035515074_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef THIRDPERSONCONTROLLER_T2544474708_H
#define THIRDPERSONCONTROLLER_T2544474708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdPersonController
struct  ThirdPersonController_t2544474708  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AnimationClip ThirdPersonController::idleAnimation
	AnimationClip_t2318505987 * ___idleAnimation_2;
	// UnityEngine.AnimationClip ThirdPersonController::walkAnimation
	AnimationClip_t2318505987 * ___walkAnimation_3;
	// UnityEngine.AnimationClip ThirdPersonController::runAnimation
	AnimationClip_t2318505987 * ___runAnimation_4;
	// UnityEngine.AnimationClip ThirdPersonController::jumpPoseAnimation
	AnimationClip_t2318505987 * ___jumpPoseAnimation_5;
	// System.Single ThirdPersonController::walkMaxAnimationSpeed
	float ___walkMaxAnimationSpeed_6;
	// System.Single ThirdPersonController::trotMaxAnimationSpeed
	float ___trotMaxAnimationSpeed_7;
	// System.Single ThirdPersonController::runMaxAnimationSpeed
	float ___runMaxAnimationSpeed_8;
	// System.Single ThirdPersonController::jumpAnimationSpeed
	float ___jumpAnimationSpeed_9;
	// System.Single ThirdPersonController::landAnimationSpeed
	float ___landAnimationSpeed_10;
	// UnityEngine.Animation ThirdPersonController::_animation
	Animation_t3648466861 * ____animation_11;
	// CharacterState ThirdPersonController::_characterState
	int32_t ____characterState_12;
	// System.Single ThirdPersonController::walkSpeed
	float ___walkSpeed_13;
	// System.Single ThirdPersonController::trotSpeed
	float ___trotSpeed_14;
	// System.Single ThirdPersonController::runSpeed
	float ___runSpeed_15;
	// System.Single ThirdPersonController::inAirControlAcceleration
	float ___inAirControlAcceleration_16;
	// System.Single ThirdPersonController::jumpHeight
	float ___jumpHeight_17;
	// System.Single ThirdPersonController::gravity
	float ___gravity_18;
	// System.Single ThirdPersonController::speedSmoothing
	float ___speedSmoothing_19;
	// System.Single ThirdPersonController::rotateSpeed
	float ___rotateSpeed_20;
	// System.Single ThirdPersonController::trotAfterSeconds
	float ___trotAfterSeconds_21;
	// System.Boolean ThirdPersonController::canJump
	bool ___canJump_22;
	// System.Single ThirdPersonController::jumpRepeatTime
	float ___jumpRepeatTime_23;
	// System.Single ThirdPersonController::jumpTimeout
	float ___jumpTimeout_24;
	// System.Single ThirdPersonController::groundedTimeout
	float ___groundedTimeout_25;
	// System.Single ThirdPersonController::lockCameraTimer
	float ___lockCameraTimer_26;
	// UnityEngine.Vector3 ThirdPersonController::moveDirection
	Vector3_t3722313464  ___moveDirection_27;
	// System.Single ThirdPersonController::verticalSpeed
	float ___verticalSpeed_28;
	// System.Single ThirdPersonController::moveSpeed
	float ___moveSpeed_29;
	// UnityEngine.CollisionFlags ThirdPersonController::collisionFlags
	int32_t ___collisionFlags_30;
	// System.Boolean ThirdPersonController::jumping
	bool ___jumping_31;
	// System.Boolean ThirdPersonController::jumpingReachedApex
	bool ___jumpingReachedApex_32;
	// System.Boolean ThirdPersonController::movingBack
	bool ___movingBack_33;
	// System.Boolean ThirdPersonController::isMoving
	bool ___isMoving_34;
	// System.Single ThirdPersonController::walkTimeStart
	float ___walkTimeStart_35;
	// System.Single ThirdPersonController::lastJumpButtonTime
	float ___lastJumpButtonTime_36;
	// System.Single ThirdPersonController::lastJumpTime
	float ___lastJumpTime_37;
	// UnityEngine.Vector3 ThirdPersonController::inAirVelocity
	Vector3_t3722313464  ___inAirVelocity_38;
	// System.Single ThirdPersonController::lastGroundedTime
	float ___lastGroundedTime_39;
	// System.Boolean ThirdPersonController::isControllable
	bool ___isControllable_40;
	// UnityEngine.Vector3 ThirdPersonController::lastPos
	Vector3_t3722313464  ___lastPos_41;
	// UnityEngine.Vector3 ThirdPersonController::velocity
	Vector3_t3722313464  ___velocity_42;

public:
	inline static int32_t get_offset_of_idleAnimation_2() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___idleAnimation_2)); }
	inline AnimationClip_t2318505987 * get_idleAnimation_2() const { return ___idleAnimation_2; }
	inline AnimationClip_t2318505987 ** get_address_of_idleAnimation_2() { return &___idleAnimation_2; }
	inline void set_idleAnimation_2(AnimationClip_t2318505987 * value)
	{
		___idleAnimation_2 = value;
		Il2CppCodeGenWriteBarrier((&___idleAnimation_2), value);
	}

	inline static int32_t get_offset_of_walkAnimation_3() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___walkAnimation_3)); }
	inline AnimationClip_t2318505987 * get_walkAnimation_3() const { return ___walkAnimation_3; }
	inline AnimationClip_t2318505987 ** get_address_of_walkAnimation_3() { return &___walkAnimation_3; }
	inline void set_walkAnimation_3(AnimationClip_t2318505987 * value)
	{
		___walkAnimation_3 = value;
		Il2CppCodeGenWriteBarrier((&___walkAnimation_3), value);
	}

	inline static int32_t get_offset_of_runAnimation_4() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___runAnimation_4)); }
	inline AnimationClip_t2318505987 * get_runAnimation_4() const { return ___runAnimation_4; }
	inline AnimationClip_t2318505987 ** get_address_of_runAnimation_4() { return &___runAnimation_4; }
	inline void set_runAnimation_4(AnimationClip_t2318505987 * value)
	{
		___runAnimation_4 = value;
		Il2CppCodeGenWriteBarrier((&___runAnimation_4), value);
	}

	inline static int32_t get_offset_of_jumpPoseAnimation_5() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___jumpPoseAnimation_5)); }
	inline AnimationClip_t2318505987 * get_jumpPoseAnimation_5() const { return ___jumpPoseAnimation_5; }
	inline AnimationClip_t2318505987 ** get_address_of_jumpPoseAnimation_5() { return &___jumpPoseAnimation_5; }
	inline void set_jumpPoseAnimation_5(AnimationClip_t2318505987 * value)
	{
		___jumpPoseAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___jumpPoseAnimation_5), value);
	}

	inline static int32_t get_offset_of_walkMaxAnimationSpeed_6() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___walkMaxAnimationSpeed_6)); }
	inline float get_walkMaxAnimationSpeed_6() const { return ___walkMaxAnimationSpeed_6; }
	inline float* get_address_of_walkMaxAnimationSpeed_6() { return &___walkMaxAnimationSpeed_6; }
	inline void set_walkMaxAnimationSpeed_6(float value)
	{
		___walkMaxAnimationSpeed_6 = value;
	}

	inline static int32_t get_offset_of_trotMaxAnimationSpeed_7() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___trotMaxAnimationSpeed_7)); }
	inline float get_trotMaxAnimationSpeed_7() const { return ___trotMaxAnimationSpeed_7; }
	inline float* get_address_of_trotMaxAnimationSpeed_7() { return &___trotMaxAnimationSpeed_7; }
	inline void set_trotMaxAnimationSpeed_7(float value)
	{
		___trotMaxAnimationSpeed_7 = value;
	}

	inline static int32_t get_offset_of_runMaxAnimationSpeed_8() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___runMaxAnimationSpeed_8)); }
	inline float get_runMaxAnimationSpeed_8() const { return ___runMaxAnimationSpeed_8; }
	inline float* get_address_of_runMaxAnimationSpeed_8() { return &___runMaxAnimationSpeed_8; }
	inline void set_runMaxAnimationSpeed_8(float value)
	{
		___runMaxAnimationSpeed_8 = value;
	}

	inline static int32_t get_offset_of_jumpAnimationSpeed_9() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___jumpAnimationSpeed_9)); }
	inline float get_jumpAnimationSpeed_9() const { return ___jumpAnimationSpeed_9; }
	inline float* get_address_of_jumpAnimationSpeed_9() { return &___jumpAnimationSpeed_9; }
	inline void set_jumpAnimationSpeed_9(float value)
	{
		___jumpAnimationSpeed_9 = value;
	}

	inline static int32_t get_offset_of_landAnimationSpeed_10() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___landAnimationSpeed_10)); }
	inline float get_landAnimationSpeed_10() const { return ___landAnimationSpeed_10; }
	inline float* get_address_of_landAnimationSpeed_10() { return &___landAnimationSpeed_10; }
	inline void set_landAnimationSpeed_10(float value)
	{
		___landAnimationSpeed_10 = value;
	}

	inline static int32_t get_offset_of__animation_11() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ____animation_11)); }
	inline Animation_t3648466861 * get__animation_11() const { return ____animation_11; }
	inline Animation_t3648466861 ** get_address_of__animation_11() { return &____animation_11; }
	inline void set__animation_11(Animation_t3648466861 * value)
	{
		____animation_11 = value;
		Il2CppCodeGenWriteBarrier((&____animation_11), value);
	}

	inline static int32_t get_offset_of__characterState_12() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ____characterState_12)); }
	inline int32_t get__characterState_12() const { return ____characterState_12; }
	inline int32_t* get_address_of__characterState_12() { return &____characterState_12; }
	inline void set__characterState_12(int32_t value)
	{
		____characterState_12 = value;
	}

	inline static int32_t get_offset_of_walkSpeed_13() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___walkSpeed_13)); }
	inline float get_walkSpeed_13() const { return ___walkSpeed_13; }
	inline float* get_address_of_walkSpeed_13() { return &___walkSpeed_13; }
	inline void set_walkSpeed_13(float value)
	{
		___walkSpeed_13 = value;
	}

	inline static int32_t get_offset_of_trotSpeed_14() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___trotSpeed_14)); }
	inline float get_trotSpeed_14() const { return ___trotSpeed_14; }
	inline float* get_address_of_trotSpeed_14() { return &___trotSpeed_14; }
	inline void set_trotSpeed_14(float value)
	{
		___trotSpeed_14 = value;
	}

	inline static int32_t get_offset_of_runSpeed_15() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___runSpeed_15)); }
	inline float get_runSpeed_15() const { return ___runSpeed_15; }
	inline float* get_address_of_runSpeed_15() { return &___runSpeed_15; }
	inline void set_runSpeed_15(float value)
	{
		___runSpeed_15 = value;
	}

	inline static int32_t get_offset_of_inAirControlAcceleration_16() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___inAirControlAcceleration_16)); }
	inline float get_inAirControlAcceleration_16() const { return ___inAirControlAcceleration_16; }
	inline float* get_address_of_inAirControlAcceleration_16() { return &___inAirControlAcceleration_16; }
	inline void set_inAirControlAcceleration_16(float value)
	{
		___inAirControlAcceleration_16 = value;
	}

	inline static int32_t get_offset_of_jumpHeight_17() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___jumpHeight_17)); }
	inline float get_jumpHeight_17() const { return ___jumpHeight_17; }
	inline float* get_address_of_jumpHeight_17() { return &___jumpHeight_17; }
	inline void set_jumpHeight_17(float value)
	{
		___jumpHeight_17 = value;
	}

	inline static int32_t get_offset_of_gravity_18() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___gravity_18)); }
	inline float get_gravity_18() const { return ___gravity_18; }
	inline float* get_address_of_gravity_18() { return &___gravity_18; }
	inline void set_gravity_18(float value)
	{
		___gravity_18 = value;
	}

	inline static int32_t get_offset_of_speedSmoothing_19() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___speedSmoothing_19)); }
	inline float get_speedSmoothing_19() const { return ___speedSmoothing_19; }
	inline float* get_address_of_speedSmoothing_19() { return &___speedSmoothing_19; }
	inline void set_speedSmoothing_19(float value)
	{
		___speedSmoothing_19 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_20() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___rotateSpeed_20)); }
	inline float get_rotateSpeed_20() const { return ___rotateSpeed_20; }
	inline float* get_address_of_rotateSpeed_20() { return &___rotateSpeed_20; }
	inline void set_rotateSpeed_20(float value)
	{
		___rotateSpeed_20 = value;
	}

	inline static int32_t get_offset_of_trotAfterSeconds_21() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___trotAfterSeconds_21)); }
	inline float get_trotAfterSeconds_21() const { return ___trotAfterSeconds_21; }
	inline float* get_address_of_trotAfterSeconds_21() { return &___trotAfterSeconds_21; }
	inline void set_trotAfterSeconds_21(float value)
	{
		___trotAfterSeconds_21 = value;
	}

	inline static int32_t get_offset_of_canJump_22() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___canJump_22)); }
	inline bool get_canJump_22() const { return ___canJump_22; }
	inline bool* get_address_of_canJump_22() { return &___canJump_22; }
	inline void set_canJump_22(bool value)
	{
		___canJump_22 = value;
	}

	inline static int32_t get_offset_of_jumpRepeatTime_23() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___jumpRepeatTime_23)); }
	inline float get_jumpRepeatTime_23() const { return ___jumpRepeatTime_23; }
	inline float* get_address_of_jumpRepeatTime_23() { return &___jumpRepeatTime_23; }
	inline void set_jumpRepeatTime_23(float value)
	{
		___jumpRepeatTime_23 = value;
	}

	inline static int32_t get_offset_of_jumpTimeout_24() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___jumpTimeout_24)); }
	inline float get_jumpTimeout_24() const { return ___jumpTimeout_24; }
	inline float* get_address_of_jumpTimeout_24() { return &___jumpTimeout_24; }
	inline void set_jumpTimeout_24(float value)
	{
		___jumpTimeout_24 = value;
	}

	inline static int32_t get_offset_of_groundedTimeout_25() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___groundedTimeout_25)); }
	inline float get_groundedTimeout_25() const { return ___groundedTimeout_25; }
	inline float* get_address_of_groundedTimeout_25() { return &___groundedTimeout_25; }
	inline void set_groundedTimeout_25(float value)
	{
		___groundedTimeout_25 = value;
	}

	inline static int32_t get_offset_of_lockCameraTimer_26() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___lockCameraTimer_26)); }
	inline float get_lockCameraTimer_26() const { return ___lockCameraTimer_26; }
	inline float* get_address_of_lockCameraTimer_26() { return &___lockCameraTimer_26; }
	inline void set_lockCameraTimer_26(float value)
	{
		___lockCameraTimer_26 = value;
	}

	inline static int32_t get_offset_of_moveDirection_27() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___moveDirection_27)); }
	inline Vector3_t3722313464  get_moveDirection_27() const { return ___moveDirection_27; }
	inline Vector3_t3722313464 * get_address_of_moveDirection_27() { return &___moveDirection_27; }
	inline void set_moveDirection_27(Vector3_t3722313464  value)
	{
		___moveDirection_27 = value;
	}

	inline static int32_t get_offset_of_verticalSpeed_28() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___verticalSpeed_28)); }
	inline float get_verticalSpeed_28() const { return ___verticalSpeed_28; }
	inline float* get_address_of_verticalSpeed_28() { return &___verticalSpeed_28; }
	inline void set_verticalSpeed_28(float value)
	{
		___verticalSpeed_28 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_29() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___moveSpeed_29)); }
	inline float get_moveSpeed_29() const { return ___moveSpeed_29; }
	inline float* get_address_of_moveSpeed_29() { return &___moveSpeed_29; }
	inline void set_moveSpeed_29(float value)
	{
		___moveSpeed_29 = value;
	}

	inline static int32_t get_offset_of_collisionFlags_30() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___collisionFlags_30)); }
	inline int32_t get_collisionFlags_30() const { return ___collisionFlags_30; }
	inline int32_t* get_address_of_collisionFlags_30() { return &___collisionFlags_30; }
	inline void set_collisionFlags_30(int32_t value)
	{
		___collisionFlags_30 = value;
	}

	inline static int32_t get_offset_of_jumping_31() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___jumping_31)); }
	inline bool get_jumping_31() const { return ___jumping_31; }
	inline bool* get_address_of_jumping_31() { return &___jumping_31; }
	inline void set_jumping_31(bool value)
	{
		___jumping_31 = value;
	}

	inline static int32_t get_offset_of_jumpingReachedApex_32() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___jumpingReachedApex_32)); }
	inline bool get_jumpingReachedApex_32() const { return ___jumpingReachedApex_32; }
	inline bool* get_address_of_jumpingReachedApex_32() { return &___jumpingReachedApex_32; }
	inline void set_jumpingReachedApex_32(bool value)
	{
		___jumpingReachedApex_32 = value;
	}

	inline static int32_t get_offset_of_movingBack_33() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___movingBack_33)); }
	inline bool get_movingBack_33() const { return ___movingBack_33; }
	inline bool* get_address_of_movingBack_33() { return &___movingBack_33; }
	inline void set_movingBack_33(bool value)
	{
		___movingBack_33 = value;
	}

	inline static int32_t get_offset_of_isMoving_34() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___isMoving_34)); }
	inline bool get_isMoving_34() const { return ___isMoving_34; }
	inline bool* get_address_of_isMoving_34() { return &___isMoving_34; }
	inline void set_isMoving_34(bool value)
	{
		___isMoving_34 = value;
	}

	inline static int32_t get_offset_of_walkTimeStart_35() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___walkTimeStart_35)); }
	inline float get_walkTimeStart_35() const { return ___walkTimeStart_35; }
	inline float* get_address_of_walkTimeStart_35() { return &___walkTimeStart_35; }
	inline void set_walkTimeStart_35(float value)
	{
		___walkTimeStart_35 = value;
	}

	inline static int32_t get_offset_of_lastJumpButtonTime_36() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___lastJumpButtonTime_36)); }
	inline float get_lastJumpButtonTime_36() const { return ___lastJumpButtonTime_36; }
	inline float* get_address_of_lastJumpButtonTime_36() { return &___lastJumpButtonTime_36; }
	inline void set_lastJumpButtonTime_36(float value)
	{
		___lastJumpButtonTime_36 = value;
	}

	inline static int32_t get_offset_of_lastJumpTime_37() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___lastJumpTime_37)); }
	inline float get_lastJumpTime_37() const { return ___lastJumpTime_37; }
	inline float* get_address_of_lastJumpTime_37() { return &___lastJumpTime_37; }
	inline void set_lastJumpTime_37(float value)
	{
		___lastJumpTime_37 = value;
	}

	inline static int32_t get_offset_of_inAirVelocity_38() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___inAirVelocity_38)); }
	inline Vector3_t3722313464  get_inAirVelocity_38() const { return ___inAirVelocity_38; }
	inline Vector3_t3722313464 * get_address_of_inAirVelocity_38() { return &___inAirVelocity_38; }
	inline void set_inAirVelocity_38(Vector3_t3722313464  value)
	{
		___inAirVelocity_38 = value;
	}

	inline static int32_t get_offset_of_lastGroundedTime_39() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___lastGroundedTime_39)); }
	inline float get_lastGroundedTime_39() const { return ___lastGroundedTime_39; }
	inline float* get_address_of_lastGroundedTime_39() { return &___lastGroundedTime_39; }
	inline void set_lastGroundedTime_39(float value)
	{
		___lastGroundedTime_39 = value;
	}

	inline static int32_t get_offset_of_isControllable_40() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___isControllable_40)); }
	inline bool get_isControllable_40() const { return ___isControllable_40; }
	inline bool* get_address_of_isControllable_40() { return &___isControllable_40; }
	inline void set_isControllable_40(bool value)
	{
		___isControllable_40 = value;
	}

	inline static int32_t get_offset_of_lastPos_41() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___lastPos_41)); }
	inline Vector3_t3722313464  get_lastPos_41() const { return ___lastPos_41; }
	inline Vector3_t3722313464 * get_address_of_lastPos_41() { return &___lastPos_41; }
	inline void set_lastPos_41(Vector3_t3722313464  value)
	{
		___lastPos_41 = value;
	}

	inline static int32_t get_offset_of_velocity_42() { return static_cast<int32_t>(offsetof(ThirdPersonController_t2544474708, ___velocity_42)); }
	inline Vector3_t3722313464  get_velocity_42() const { return ___velocity_42; }
	inline Vector3_t3722313464 * get_address_of_velocity_42() { return &___velocity_42; }
	inline void set_velocity_42(Vector3_t3722313464  value)
	{
		___velocity_42 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONCONTROLLER_T2544474708_H
#ifndef DEMOBOXESGUI_T3999198613_H
#define DEMOBOXESGUI_T3999198613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoBoxesGui
struct  DemoBoxesGui_t3999198613  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean DemoBoxesGui::HideUI
	bool ___HideUI_2;
	// UnityEngine.GUIText DemoBoxesGui::GuiTextForTips
	GUIText_t402233326 * ___GuiTextForTips_3;
	// System.Int32 DemoBoxesGui::tipsIndex
	int32_t ___tipsIndex_4;
	// System.String[] DemoBoxesGui::tips
	StringU5BU5D_t1281789340* ___tips_5;
	// System.Single DemoBoxesGui::timeSinceLastTip
	float ___timeSinceLastTip_7;

public:
	inline static int32_t get_offset_of_HideUI_2() { return static_cast<int32_t>(offsetof(DemoBoxesGui_t3999198613, ___HideUI_2)); }
	inline bool get_HideUI_2() const { return ___HideUI_2; }
	inline bool* get_address_of_HideUI_2() { return &___HideUI_2; }
	inline void set_HideUI_2(bool value)
	{
		___HideUI_2 = value;
	}

	inline static int32_t get_offset_of_GuiTextForTips_3() { return static_cast<int32_t>(offsetof(DemoBoxesGui_t3999198613, ___GuiTextForTips_3)); }
	inline GUIText_t402233326 * get_GuiTextForTips_3() const { return ___GuiTextForTips_3; }
	inline GUIText_t402233326 ** get_address_of_GuiTextForTips_3() { return &___GuiTextForTips_3; }
	inline void set_GuiTextForTips_3(GUIText_t402233326 * value)
	{
		___GuiTextForTips_3 = value;
		Il2CppCodeGenWriteBarrier((&___GuiTextForTips_3), value);
	}

	inline static int32_t get_offset_of_tipsIndex_4() { return static_cast<int32_t>(offsetof(DemoBoxesGui_t3999198613, ___tipsIndex_4)); }
	inline int32_t get_tipsIndex_4() const { return ___tipsIndex_4; }
	inline int32_t* get_address_of_tipsIndex_4() { return &___tipsIndex_4; }
	inline void set_tipsIndex_4(int32_t value)
	{
		___tipsIndex_4 = value;
	}

	inline static int32_t get_offset_of_tips_5() { return static_cast<int32_t>(offsetof(DemoBoxesGui_t3999198613, ___tips_5)); }
	inline StringU5BU5D_t1281789340* get_tips_5() const { return ___tips_5; }
	inline StringU5BU5D_t1281789340** get_address_of_tips_5() { return &___tips_5; }
	inline void set_tips_5(StringU5BU5D_t1281789340* value)
	{
		___tips_5 = value;
		Il2CppCodeGenWriteBarrier((&___tips_5), value);
	}

	inline static int32_t get_offset_of_timeSinceLastTip_7() { return static_cast<int32_t>(offsetof(DemoBoxesGui_t3999198613, ___timeSinceLastTip_7)); }
	inline float get_timeSinceLastTip_7() const { return ___timeSinceLastTip_7; }
	inline float* get_address_of_timeSinceLastTip_7() { return &___timeSinceLastTip_7; }
	inline void set_timeSinceLastTip_7(float value)
	{
		___timeSinceLastTip_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOBOXESGUI_T3999198613_H
#ifndef DRAGTOMOVE_T3834199052_H
#define DRAGTOMOVE_T3834199052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragToMove
struct  DragToMove_t3834199052  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DragToMove::speed
	float ___speed_2;
	// UnityEngine.Transform[] DragToMove::cubes
	TransformU5BU5D_t807237628* ___cubes_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DragToMove::PositionsQueue
	List_1_t899420910 * ___PositionsQueue_4;
	// UnityEngine.Vector3[] DragToMove::cubeStartPositions
	Vector3U5BU5D_t1718750761* ___cubeStartPositions_5;
	// System.Int32 DragToMove::nextPosIndex
	int32_t ___nextPosIndex_6;
	// System.Single DragToMove::lerpTime
	float ___lerpTime_7;
	// System.Boolean DragToMove::recording
	bool ___recording_8;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(DragToMove_t3834199052, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_cubes_3() { return static_cast<int32_t>(offsetof(DragToMove_t3834199052, ___cubes_3)); }
	inline TransformU5BU5D_t807237628* get_cubes_3() const { return ___cubes_3; }
	inline TransformU5BU5D_t807237628** get_address_of_cubes_3() { return &___cubes_3; }
	inline void set_cubes_3(TransformU5BU5D_t807237628* value)
	{
		___cubes_3 = value;
		Il2CppCodeGenWriteBarrier((&___cubes_3), value);
	}

	inline static int32_t get_offset_of_PositionsQueue_4() { return static_cast<int32_t>(offsetof(DragToMove_t3834199052, ___PositionsQueue_4)); }
	inline List_1_t899420910 * get_PositionsQueue_4() const { return ___PositionsQueue_4; }
	inline List_1_t899420910 ** get_address_of_PositionsQueue_4() { return &___PositionsQueue_4; }
	inline void set_PositionsQueue_4(List_1_t899420910 * value)
	{
		___PositionsQueue_4 = value;
		Il2CppCodeGenWriteBarrier((&___PositionsQueue_4), value);
	}

	inline static int32_t get_offset_of_cubeStartPositions_5() { return static_cast<int32_t>(offsetof(DragToMove_t3834199052, ___cubeStartPositions_5)); }
	inline Vector3U5BU5D_t1718750761* get_cubeStartPositions_5() const { return ___cubeStartPositions_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_cubeStartPositions_5() { return &___cubeStartPositions_5; }
	inline void set_cubeStartPositions_5(Vector3U5BU5D_t1718750761* value)
	{
		___cubeStartPositions_5 = value;
		Il2CppCodeGenWriteBarrier((&___cubeStartPositions_5), value);
	}

	inline static int32_t get_offset_of_nextPosIndex_6() { return static_cast<int32_t>(offsetof(DragToMove_t3834199052, ___nextPosIndex_6)); }
	inline int32_t get_nextPosIndex_6() const { return ___nextPosIndex_6; }
	inline int32_t* get_address_of_nextPosIndex_6() { return &___nextPosIndex_6; }
	inline void set_nextPosIndex_6(int32_t value)
	{
		___nextPosIndex_6 = value;
	}

	inline static int32_t get_offset_of_lerpTime_7() { return static_cast<int32_t>(offsetof(DragToMove_t3834199052, ___lerpTime_7)); }
	inline float get_lerpTime_7() const { return ___lerpTime_7; }
	inline float* get_address_of_lerpTime_7() { return &___lerpTime_7; }
	inline void set_lerpTime_7(float value)
	{
		___lerpTime_7 = value;
	}

	inline static int32_t get_offset_of_recording_8() { return static_cast<int32_t>(offsetof(DragToMove_t3834199052, ___recording_8)); }
	inline bool get_recording_8() const { return ___recording_8; }
	inline bool* get_address_of_recording_8() { return &___recording_8; }
	inline void set_recording_8(bool value)
	{
		___recording_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGTOMOVE_T3834199052_H
#ifndef IELDEMO_T899827249_H
#define IELDEMO_T899827249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IELdemo
struct  IELdemo_t899827249  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUISkin IELdemo::Skin
	GUISkin_t1244372282 * ___Skin_2;

public:
	inline static int32_t get_offset_of_Skin_2() { return static_cast<int32_t>(offsetof(IELdemo_t899827249, ___Skin_2)); }
	inline GUISkin_t1244372282 * get_Skin_2() const { return ___Skin_2; }
	inline GUISkin_t1244372282 ** get_address_of_Skin_2() { return &___Skin_2; }
	inline void set_Skin_2(GUISkin_t1244372282 * value)
	{
		___Skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IELDEMO_T899827249_H
#ifndef THIRDPERSONCAMERA_T2998681409_H
#define THIRDPERSONCAMERA_T2998681409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdPersonCamera
struct  ThirdPersonCamera_t2998681409  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform ThirdPersonCamera::cameraTransform
	Transform_t3600365921 * ___cameraTransform_2;
	// UnityEngine.Transform ThirdPersonCamera::_target
	Transform_t3600365921 * ____target_3;
	// System.Single ThirdPersonCamera::distance
	float ___distance_4;
	// System.Single ThirdPersonCamera::height
	float ___height_5;
	// System.Single ThirdPersonCamera::angularSmoothLag
	float ___angularSmoothLag_6;
	// System.Single ThirdPersonCamera::angularMaxSpeed
	float ___angularMaxSpeed_7;
	// System.Single ThirdPersonCamera::heightSmoothLag
	float ___heightSmoothLag_8;
	// System.Single ThirdPersonCamera::snapSmoothLag
	float ___snapSmoothLag_9;
	// System.Single ThirdPersonCamera::snapMaxSpeed
	float ___snapMaxSpeed_10;
	// System.Single ThirdPersonCamera::clampHeadPositionScreenSpace
	float ___clampHeadPositionScreenSpace_11;
	// System.Single ThirdPersonCamera::lockCameraTimeout
	float ___lockCameraTimeout_12;
	// UnityEngine.Vector3 ThirdPersonCamera::headOffset
	Vector3_t3722313464  ___headOffset_13;
	// UnityEngine.Vector3 ThirdPersonCamera::centerOffset
	Vector3_t3722313464  ___centerOffset_14;
	// System.Single ThirdPersonCamera::heightVelocity
	float ___heightVelocity_15;
	// System.Single ThirdPersonCamera::angleVelocity
	float ___angleVelocity_16;
	// System.Boolean ThirdPersonCamera::snap
	bool ___snap_17;
	// ThirdPersonController ThirdPersonCamera::controller
	ThirdPersonController_t2544474708 * ___controller_18;
	// System.Single ThirdPersonCamera::targetHeight
	float ___targetHeight_19;
	// UnityEngine.Camera ThirdPersonCamera::m_CameraTransformCamera
	Camera_t4157153871 * ___m_CameraTransformCamera_20;

public:
	inline static int32_t get_offset_of_cameraTransform_2() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___cameraTransform_2)); }
	inline Transform_t3600365921 * get_cameraTransform_2() const { return ___cameraTransform_2; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_2() { return &___cameraTransform_2; }
	inline void set_cameraTransform_2(Transform_t3600365921 * value)
	{
		___cameraTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_2), value);
	}

	inline static int32_t get_offset_of__target_3() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ____target_3)); }
	inline Transform_t3600365921 * get__target_3() const { return ____target_3; }
	inline Transform_t3600365921 ** get_address_of__target_3() { return &____target_3; }
	inline void set__target_3(Transform_t3600365921 * value)
	{
		____target_3 = value;
		Il2CppCodeGenWriteBarrier((&____target_3), value);
	}

	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___height_5)); }
	inline float get_height_5() const { return ___height_5; }
	inline float* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(float value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_angularSmoothLag_6() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___angularSmoothLag_6)); }
	inline float get_angularSmoothLag_6() const { return ___angularSmoothLag_6; }
	inline float* get_address_of_angularSmoothLag_6() { return &___angularSmoothLag_6; }
	inline void set_angularSmoothLag_6(float value)
	{
		___angularSmoothLag_6 = value;
	}

	inline static int32_t get_offset_of_angularMaxSpeed_7() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___angularMaxSpeed_7)); }
	inline float get_angularMaxSpeed_7() const { return ___angularMaxSpeed_7; }
	inline float* get_address_of_angularMaxSpeed_7() { return &___angularMaxSpeed_7; }
	inline void set_angularMaxSpeed_7(float value)
	{
		___angularMaxSpeed_7 = value;
	}

	inline static int32_t get_offset_of_heightSmoothLag_8() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___heightSmoothLag_8)); }
	inline float get_heightSmoothLag_8() const { return ___heightSmoothLag_8; }
	inline float* get_address_of_heightSmoothLag_8() { return &___heightSmoothLag_8; }
	inline void set_heightSmoothLag_8(float value)
	{
		___heightSmoothLag_8 = value;
	}

	inline static int32_t get_offset_of_snapSmoothLag_9() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___snapSmoothLag_9)); }
	inline float get_snapSmoothLag_9() const { return ___snapSmoothLag_9; }
	inline float* get_address_of_snapSmoothLag_9() { return &___snapSmoothLag_9; }
	inline void set_snapSmoothLag_9(float value)
	{
		___snapSmoothLag_9 = value;
	}

	inline static int32_t get_offset_of_snapMaxSpeed_10() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___snapMaxSpeed_10)); }
	inline float get_snapMaxSpeed_10() const { return ___snapMaxSpeed_10; }
	inline float* get_address_of_snapMaxSpeed_10() { return &___snapMaxSpeed_10; }
	inline void set_snapMaxSpeed_10(float value)
	{
		___snapMaxSpeed_10 = value;
	}

	inline static int32_t get_offset_of_clampHeadPositionScreenSpace_11() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___clampHeadPositionScreenSpace_11)); }
	inline float get_clampHeadPositionScreenSpace_11() const { return ___clampHeadPositionScreenSpace_11; }
	inline float* get_address_of_clampHeadPositionScreenSpace_11() { return &___clampHeadPositionScreenSpace_11; }
	inline void set_clampHeadPositionScreenSpace_11(float value)
	{
		___clampHeadPositionScreenSpace_11 = value;
	}

	inline static int32_t get_offset_of_lockCameraTimeout_12() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___lockCameraTimeout_12)); }
	inline float get_lockCameraTimeout_12() const { return ___lockCameraTimeout_12; }
	inline float* get_address_of_lockCameraTimeout_12() { return &___lockCameraTimeout_12; }
	inline void set_lockCameraTimeout_12(float value)
	{
		___lockCameraTimeout_12 = value;
	}

	inline static int32_t get_offset_of_headOffset_13() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___headOffset_13)); }
	inline Vector3_t3722313464  get_headOffset_13() const { return ___headOffset_13; }
	inline Vector3_t3722313464 * get_address_of_headOffset_13() { return &___headOffset_13; }
	inline void set_headOffset_13(Vector3_t3722313464  value)
	{
		___headOffset_13 = value;
	}

	inline static int32_t get_offset_of_centerOffset_14() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___centerOffset_14)); }
	inline Vector3_t3722313464  get_centerOffset_14() const { return ___centerOffset_14; }
	inline Vector3_t3722313464 * get_address_of_centerOffset_14() { return &___centerOffset_14; }
	inline void set_centerOffset_14(Vector3_t3722313464  value)
	{
		___centerOffset_14 = value;
	}

	inline static int32_t get_offset_of_heightVelocity_15() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___heightVelocity_15)); }
	inline float get_heightVelocity_15() const { return ___heightVelocity_15; }
	inline float* get_address_of_heightVelocity_15() { return &___heightVelocity_15; }
	inline void set_heightVelocity_15(float value)
	{
		___heightVelocity_15 = value;
	}

	inline static int32_t get_offset_of_angleVelocity_16() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___angleVelocity_16)); }
	inline float get_angleVelocity_16() const { return ___angleVelocity_16; }
	inline float* get_address_of_angleVelocity_16() { return &___angleVelocity_16; }
	inline void set_angleVelocity_16(float value)
	{
		___angleVelocity_16 = value;
	}

	inline static int32_t get_offset_of_snap_17() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___snap_17)); }
	inline bool get_snap_17() const { return ___snap_17; }
	inline bool* get_address_of_snap_17() { return &___snap_17; }
	inline void set_snap_17(bool value)
	{
		___snap_17 = value;
	}

	inline static int32_t get_offset_of_controller_18() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___controller_18)); }
	inline ThirdPersonController_t2544474708 * get_controller_18() const { return ___controller_18; }
	inline ThirdPersonController_t2544474708 ** get_address_of_controller_18() { return &___controller_18; }
	inline void set_controller_18(ThirdPersonController_t2544474708 * value)
	{
		___controller_18 = value;
		Il2CppCodeGenWriteBarrier((&___controller_18), value);
	}

	inline static int32_t get_offset_of_targetHeight_19() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___targetHeight_19)); }
	inline float get_targetHeight_19() const { return ___targetHeight_19; }
	inline float* get_address_of_targetHeight_19() { return &___targetHeight_19; }
	inline void set_targetHeight_19(float value)
	{
		___targetHeight_19 = value;
	}

	inline static int32_t get_offset_of_m_CameraTransformCamera_20() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2998681409, ___m_CameraTransformCamera_20)); }
	inline Camera_t4157153871 * get_m_CameraTransformCamera_20() const { return ___m_CameraTransformCamera_20; }
	inline Camera_t4157153871 ** get_address_of_m_CameraTransformCamera_20() { return &___m_CameraTransformCamera_20; }
	inline void set_m_CameraTransformCamera_20(Camera_t4157153871 * value)
	{
		___m_CameraTransformCamera_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraTransformCamera_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONCAMERA_T2998681409_H
#ifndef PLAYERDIAMOND_T4061521841_H
#define PLAYERDIAMOND_T4061521841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerDiamond
struct  PlayerDiamond_t4061521841  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform PlayerDiamond::HeadTransform
	Transform_t3600365921 * ___HeadTransform_2;
	// System.Single PlayerDiamond::HeightOffset
	float ___HeightOffset_3;
	// PhotonView PlayerDiamond::m_PhotonView
	PhotonView_t2207721820 * ___m_PhotonView_4;
	// UnityEngine.Renderer PlayerDiamond::m_DiamondRenderer
	Renderer_t2627027031 * ___m_DiamondRenderer_5;
	// System.Single PlayerDiamond::m_Rotation
	float ___m_Rotation_6;
	// System.Single PlayerDiamond::m_Height
	float ___m_Height_7;

public:
	inline static int32_t get_offset_of_HeadTransform_2() { return static_cast<int32_t>(offsetof(PlayerDiamond_t4061521841, ___HeadTransform_2)); }
	inline Transform_t3600365921 * get_HeadTransform_2() const { return ___HeadTransform_2; }
	inline Transform_t3600365921 ** get_address_of_HeadTransform_2() { return &___HeadTransform_2; }
	inline void set_HeadTransform_2(Transform_t3600365921 * value)
	{
		___HeadTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___HeadTransform_2), value);
	}

	inline static int32_t get_offset_of_HeightOffset_3() { return static_cast<int32_t>(offsetof(PlayerDiamond_t4061521841, ___HeightOffset_3)); }
	inline float get_HeightOffset_3() const { return ___HeightOffset_3; }
	inline float* get_address_of_HeightOffset_3() { return &___HeightOffset_3; }
	inline void set_HeightOffset_3(float value)
	{
		___HeightOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_PhotonView_4() { return static_cast<int32_t>(offsetof(PlayerDiamond_t4061521841, ___m_PhotonView_4)); }
	inline PhotonView_t2207721820 * get_m_PhotonView_4() const { return ___m_PhotonView_4; }
	inline PhotonView_t2207721820 ** get_address_of_m_PhotonView_4() { return &___m_PhotonView_4; }
	inline void set_m_PhotonView_4(PhotonView_t2207721820 * value)
	{
		___m_PhotonView_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PhotonView_4), value);
	}

	inline static int32_t get_offset_of_m_DiamondRenderer_5() { return static_cast<int32_t>(offsetof(PlayerDiamond_t4061521841, ___m_DiamondRenderer_5)); }
	inline Renderer_t2627027031 * get_m_DiamondRenderer_5() const { return ___m_DiamondRenderer_5; }
	inline Renderer_t2627027031 ** get_address_of_m_DiamondRenderer_5() { return &___m_DiamondRenderer_5; }
	inline void set_m_DiamondRenderer_5(Renderer_t2627027031 * value)
	{
		___m_DiamondRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_DiamondRenderer_5), value);
	}

	inline static int32_t get_offset_of_m_Rotation_6() { return static_cast<int32_t>(offsetof(PlayerDiamond_t4061521841, ___m_Rotation_6)); }
	inline float get_m_Rotation_6() const { return ___m_Rotation_6; }
	inline float* get_address_of_m_Rotation_6() { return &___m_Rotation_6; }
	inline void set_m_Rotation_6(float value)
	{
		___m_Rotation_6 = value;
	}

	inline static int32_t get_offset_of_m_Height_7() { return static_cast<int32_t>(offsetof(PlayerDiamond_t4061521841, ___m_Height_7)); }
	inline float get_m_Height_7() const { return ___m_Height_7; }
	inline float* get_address_of_m_Height_7() { return &___m_Height_7; }
	inline void set_m_Height_7(float value)
	{
		___m_Height_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDIAMOND_T4061521841_H
#ifndef IDLERUNJUMP_T572119292_H
#define IDLERUNJUMP_T572119292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IdleRunJump
struct  IdleRunJump_t572119292  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator IdleRunJump::animator
	Animator_t434523843 * ___animator_2;
	// System.Single IdleRunJump::DirectionDampTime
	float ___DirectionDampTime_3;
	// System.Boolean IdleRunJump::ApplyGravity
	bool ___ApplyGravity_4;
	// System.Single IdleRunJump::SynchronizedMaxSpeed
	float ___SynchronizedMaxSpeed_5;
	// System.Single IdleRunJump::TurnSpeedModifier
	float ___TurnSpeedModifier_6;
	// System.Single IdleRunJump::SynchronizedTurnSpeed
	float ___SynchronizedTurnSpeed_7;
	// System.Single IdleRunJump::SynchronizedSpeedAcceleration
	float ___SynchronizedSpeedAcceleration_8;
	// PhotonView IdleRunJump::m_PhotonView
	PhotonView_t2207721820 * ___m_PhotonView_9;
	// PhotonTransformView IdleRunJump::m_TransformView
	PhotonTransformView_t372465615 * ___m_TransformView_10;
	// System.Single IdleRunJump::m_SpeedModifier
	float ___m_SpeedModifier_11;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___animator_2)); }
	inline Animator_t434523843 * get_animator_2() const { return ___animator_2; }
	inline Animator_t434523843 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t434523843 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier((&___animator_2), value);
	}

	inline static int32_t get_offset_of_DirectionDampTime_3() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___DirectionDampTime_3)); }
	inline float get_DirectionDampTime_3() const { return ___DirectionDampTime_3; }
	inline float* get_address_of_DirectionDampTime_3() { return &___DirectionDampTime_3; }
	inline void set_DirectionDampTime_3(float value)
	{
		___DirectionDampTime_3 = value;
	}

	inline static int32_t get_offset_of_ApplyGravity_4() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___ApplyGravity_4)); }
	inline bool get_ApplyGravity_4() const { return ___ApplyGravity_4; }
	inline bool* get_address_of_ApplyGravity_4() { return &___ApplyGravity_4; }
	inline void set_ApplyGravity_4(bool value)
	{
		___ApplyGravity_4 = value;
	}

	inline static int32_t get_offset_of_SynchronizedMaxSpeed_5() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___SynchronizedMaxSpeed_5)); }
	inline float get_SynchronizedMaxSpeed_5() const { return ___SynchronizedMaxSpeed_5; }
	inline float* get_address_of_SynchronizedMaxSpeed_5() { return &___SynchronizedMaxSpeed_5; }
	inline void set_SynchronizedMaxSpeed_5(float value)
	{
		___SynchronizedMaxSpeed_5 = value;
	}

	inline static int32_t get_offset_of_TurnSpeedModifier_6() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___TurnSpeedModifier_6)); }
	inline float get_TurnSpeedModifier_6() const { return ___TurnSpeedModifier_6; }
	inline float* get_address_of_TurnSpeedModifier_6() { return &___TurnSpeedModifier_6; }
	inline void set_TurnSpeedModifier_6(float value)
	{
		___TurnSpeedModifier_6 = value;
	}

	inline static int32_t get_offset_of_SynchronizedTurnSpeed_7() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___SynchronizedTurnSpeed_7)); }
	inline float get_SynchronizedTurnSpeed_7() const { return ___SynchronizedTurnSpeed_7; }
	inline float* get_address_of_SynchronizedTurnSpeed_7() { return &___SynchronizedTurnSpeed_7; }
	inline void set_SynchronizedTurnSpeed_7(float value)
	{
		___SynchronizedTurnSpeed_7 = value;
	}

	inline static int32_t get_offset_of_SynchronizedSpeedAcceleration_8() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___SynchronizedSpeedAcceleration_8)); }
	inline float get_SynchronizedSpeedAcceleration_8() const { return ___SynchronizedSpeedAcceleration_8; }
	inline float* get_address_of_SynchronizedSpeedAcceleration_8() { return &___SynchronizedSpeedAcceleration_8; }
	inline void set_SynchronizedSpeedAcceleration_8(float value)
	{
		___SynchronizedSpeedAcceleration_8 = value;
	}

	inline static int32_t get_offset_of_m_PhotonView_9() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___m_PhotonView_9)); }
	inline PhotonView_t2207721820 * get_m_PhotonView_9() const { return ___m_PhotonView_9; }
	inline PhotonView_t2207721820 ** get_address_of_m_PhotonView_9() { return &___m_PhotonView_9; }
	inline void set_m_PhotonView_9(PhotonView_t2207721820 * value)
	{
		___m_PhotonView_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_PhotonView_9), value);
	}

	inline static int32_t get_offset_of_m_TransformView_10() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___m_TransformView_10)); }
	inline PhotonTransformView_t372465615 * get_m_TransformView_10() const { return ___m_TransformView_10; }
	inline PhotonTransformView_t372465615 ** get_address_of_m_TransformView_10() { return &___m_TransformView_10; }
	inline void set_m_TransformView_10(PhotonTransformView_t372465615 * value)
	{
		___m_TransformView_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TransformView_10), value);
	}

	inline static int32_t get_offset_of_m_SpeedModifier_11() { return static_cast<int32_t>(offsetof(IdleRunJump_t572119292, ___m_SpeedModifier_11)); }
	inline float get_m_SpeedModifier_11() const { return ___m_SpeedModifier_11; }
	inline float* get_address_of_m_SpeedModifier_11() { return &___m_SpeedModifier_11; }
	inline void set_m_SpeedModifier_11(float value)
	{
		___m_SpeedModifier_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDLERUNJUMP_T572119292_H
#ifndef PLAYERUI_T3173905257_H
#define PLAYERUI_T3173905257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoAnimator.PlayerUI
struct  PlayerUI_t3173905257  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 ExitGames.Demos.DemoAnimator.PlayerUI::ScreenOffset
	Vector3_t3722313464  ___ScreenOffset_2;
	// UnityEngine.UI.Text ExitGames.Demos.DemoAnimator.PlayerUI::PlayerNameText
	Text_t1901882714 * ___PlayerNameText_3;
	// UnityEngine.UI.Slider ExitGames.Demos.DemoAnimator.PlayerUI::PlayerHealthSlider
	Slider_t3903728902 * ___PlayerHealthSlider_4;
	// ExitGames.Demos.DemoAnimator.PlayerManager ExitGames.Demos.DemoAnimator.PlayerUI::_target
	PlayerManager_t3964432985 * ____target_5;
	// System.Single ExitGames.Demos.DemoAnimator.PlayerUI::_characterControllerHeight
	float ____characterControllerHeight_6;
	// UnityEngine.Transform ExitGames.Demos.DemoAnimator.PlayerUI::_targetTransform
	Transform_t3600365921 * ____targetTransform_7;
	// UnityEngine.Renderer ExitGames.Demos.DemoAnimator.PlayerUI::_targetRenderer
	Renderer_t2627027031 * ____targetRenderer_8;
	// UnityEngine.Vector3 ExitGames.Demos.DemoAnimator.PlayerUI::_targetPosition
	Vector3_t3722313464  ____targetPosition_9;

public:
	inline static int32_t get_offset_of_ScreenOffset_2() { return static_cast<int32_t>(offsetof(PlayerUI_t3173905257, ___ScreenOffset_2)); }
	inline Vector3_t3722313464  get_ScreenOffset_2() const { return ___ScreenOffset_2; }
	inline Vector3_t3722313464 * get_address_of_ScreenOffset_2() { return &___ScreenOffset_2; }
	inline void set_ScreenOffset_2(Vector3_t3722313464  value)
	{
		___ScreenOffset_2 = value;
	}

	inline static int32_t get_offset_of_PlayerNameText_3() { return static_cast<int32_t>(offsetof(PlayerUI_t3173905257, ___PlayerNameText_3)); }
	inline Text_t1901882714 * get_PlayerNameText_3() const { return ___PlayerNameText_3; }
	inline Text_t1901882714 ** get_address_of_PlayerNameText_3() { return &___PlayerNameText_3; }
	inline void set_PlayerNameText_3(Text_t1901882714 * value)
	{
		___PlayerNameText_3 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerNameText_3), value);
	}

	inline static int32_t get_offset_of_PlayerHealthSlider_4() { return static_cast<int32_t>(offsetof(PlayerUI_t3173905257, ___PlayerHealthSlider_4)); }
	inline Slider_t3903728902 * get_PlayerHealthSlider_4() const { return ___PlayerHealthSlider_4; }
	inline Slider_t3903728902 ** get_address_of_PlayerHealthSlider_4() { return &___PlayerHealthSlider_4; }
	inline void set_PlayerHealthSlider_4(Slider_t3903728902 * value)
	{
		___PlayerHealthSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerHealthSlider_4), value);
	}

	inline static int32_t get_offset_of__target_5() { return static_cast<int32_t>(offsetof(PlayerUI_t3173905257, ____target_5)); }
	inline PlayerManager_t3964432985 * get__target_5() const { return ____target_5; }
	inline PlayerManager_t3964432985 ** get_address_of__target_5() { return &____target_5; }
	inline void set__target_5(PlayerManager_t3964432985 * value)
	{
		____target_5 = value;
		Il2CppCodeGenWriteBarrier((&____target_5), value);
	}

	inline static int32_t get_offset_of__characterControllerHeight_6() { return static_cast<int32_t>(offsetof(PlayerUI_t3173905257, ____characterControllerHeight_6)); }
	inline float get__characterControllerHeight_6() const { return ____characterControllerHeight_6; }
	inline float* get_address_of__characterControllerHeight_6() { return &____characterControllerHeight_6; }
	inline void set__characterControllerHeight_6(float value)
	{
		____characterControllerHeight_6 = value;
	}

	inline static int32_t get_offset_of__targetTransform_7() { return static_cast<int32_t>(offsetof(PlayerUI_t3173905257, ____targetTransform_7)); }
	inline Transform_t3600365921 * get__targetTransform_7() const { return ____targetTransform_7; }
	inline Transform_t3600365921 ** get_address_of__targetTransform_7() { return &____targetTransform_7; }
	inline void set__targetTransform_7(Transform_t3600365921 * value)
	{
		____targetTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&____targetTransform_7), value);
	}

	inline static int32_t get_offset_of__targetRenderer_8() { return static_cast<int32_t>(offsetof(PlayerUI_t3173905257, ____targetRenderer_8)); }
	inline Renderer_t2627027031 * get__targetRenderer_8() const { return ____targetRenderer_8; }
	inline Renderer_t2627027031 ** get_address_of__targetRenderer_8() { return &____targetRenderer_8; }
	inline void set__targetRenderer_8(Renderer_t2627027031 * value)
	{
		____targetRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&____targetRenderer_8), value);
	}

	inline static int32_t get_offset_of__targetPosition_9() { return static_cast<int32_t>(offsetof(PlayerUI_t3173905257, ____targetPosition_9)); }
	inline Vector3_t3722313464  get__targetPosition_9() const { return ____targetPosition_9; }
	inline Vector3_t3722313464 * get_address_of__targetPosition_9() { return &____targetPosition_9; }
	inline void set__targetPosition_9(Vector3_t3722313464  value)
	{
		____targetPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERUI_T3173905257_H
#ifndef PLAYERNAMEINPUTFIELD_T21913183_H
#define PLAYERNAMEINPUTFIELD_T21913183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoAnimator.PlayerNameInputField
struct  PlayerNameInputField_t21913183  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct PlayerNameInputField_t21913183_StaticFields
{
public:
	// System.String ExitGames.Demos.DemoAnimator.PlayerNameInputField::playerNamePrefKey
	String_t* ___playerNamePrefKey_2;

public:
	inline static int32_t get_offset_of_playerNamePrefKey_2() { return static_cast<int32_t>(offsetof(PlayerNameInputField_t21913183_StaticFields, ___playerNamePrefKey_2)); }
	inline String_t* get_playerNamePrefKey_2() const { return ___playerNamePrefKey_2; }
	inline String_t** get_address_of_playerNamePrefKey_2() { return &___playerNamePrefKey_2; }
	inline void set_playerNamePrefKey_2(String_t* value)
	{
		___playerNamePrefKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerNamePrefKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERNAMEINPUTFIELD_T21913183_H
#ifndef DEMOOWNERSHIPGUI_T3404283064_H
#define DEMOOWNERSHIPGUI_T3404283064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoOwnershipGui
struct  DemoOwnershipGui_t3404283064  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUISkin DemoOwnershipGui::Skin
	GUISkin_t1244372282 * ___Skin_2;
	// System.Boolean DemoOwnershipGui::TransferOwnershipOnRequest
	bool ___TransferOwnershipOnRequest_3;

public:
	inline static int32_t get_offset_of_Skin_2() { return static_cast<int32_t>(offsetof(DemoOwnershipGui_t3404283064, ___Skin_2)); }
	inline GUISkin_t1244372282 * get_Skin_2() const { return ___Skin_2; }
	inline GUISkin_t1244372282 ** get_address_of_Skin_2() { return &___Skin_2; }
	inline void set_Skin_2(GUISkin_t1244372282 * value)
	{
		___Skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_2), value);
	}

	inline static int32_t get_offset_of_TransferOwnershipOnRequest_3() { return static_cast<int32_t>(offsetof(DemoOwnershipGui_t3404283064, ___TransferOwnershipOnRequest_3)); }
	inline bool get_TransferOwnershipOnRequest_3() const { return ___TransferOwnershipOnRequest_3; }
	inline bool* get_address_of_TransferOwnershipOnRequest_3() { return &___TransferOwnershipOnRequest_3; }
	inline void set_TransferOwnershipOnRequest_3(bool value)
	{
		___TransferOwnershipOnRequest_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOOWNERSHIPGUI_T3404283064_H
#ifndef LOADERANIME_T64665300_H
#define LOADERANIME_T64665300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoAnimator.LoaderAnime
struct  LoaderAnime_t64665300  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ExitGames.Demos.DemoAnimator.LoaderAnime::speed
	float ___speed_2;
	// System.Single ExitGames.Demos.DemoAnimator.LoaderAnime::radius
	float ___radius_3;
	// UnityEngine.GameObject ExitGames.Demos.DemoAnimator.LoaderAnime::particles
	GameObject_t1113636619 * ___particles_4;
	// UnityEngine.Vector3 ExitGames.Demos.DemoAnimator.LoaderAnime::_offset
	Vector3_t3722313464  ____offset_5;
	// UnityEngine.Transform ExitGames.Demos.DemoAnimator.LoaderAnime::_transform
	Transform_t3600365921 * ____transform_6;
	// UnityEngine.Transform ExitGames.Demos.DemoAnimator.LoaderAnime::_particleTransform
	Transform_t3600365921 * ____particleTransform_7;
	// System.Boolean ExitGames.Demos.DemoAnimator.LoaderAnime::_isAnimating
	bool ____isAnimating_8;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(LoaderAnime_t64665300, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_radius_3() { return static_cast<int32_t>(offsetof(LoaderAnime_t64665300, ___radius_3)); }
	inline float get_radius_3() const { return ___radius_3; }
	inline float* get_address_of_radius_3() { return &___radius_3; }
	inline void set_radius_3(float value)
	{
		___radius_3 = value;
	}

	inline static int32_t get_offset_of_particles_4() { return static_cast<int32_t>(offsetof(LoaderAnime_t64665300, ___particles_4)); }
	inline GameObject_t1113636619 * get_particles_4() const { return ___particles_4; }
	inline GameObject_t1113636619 ** get_address_of_particles_4() { return &___particles_4; }
	inline void set_particles_4(GameObject_t1113636619 * value)
	{
		___particles_4 = value;
		Il2CppCodeGenWriteBarrier((&___particles_4), value);
	}

	inline static int32_t get_offset_of__offset_5() { return static_cast<int32_t>(offsetof(LoaderAnime_t64665300, ____offset_5)); }
	inline Vector3_t3722313464  get__offset_5() const { return ____offset_5; }
	inline Vector3_t3722313464 * get_address_of__offset_5() { return &____offset_5; }
	inline void set__offset_5(Vector3_t3722313464  value)
	{
		____offset_5 = value;
	}

	inline static int32_t get_offset_of__transform_6() { return static_cast<int32_t>(offsetof(LoaderAnime_t64665300, ____transform_6)); }
	inline Transform_t3600365921 * get__transform_6() const { return ____transform_6; }
	inline Transform_t3600365921 ** get_address_of__transform_6() { return &____transform_6; }
	inline void set__transform_6(Transform_t3600365921 * value)
	{
		____transform_6 = value;
		Il2CppCodeGenWriteBarrier((&____transform_6), value);
	}

	inline static int32_t get_offset_of__particleTransform_7() { return static_cast<int32_t>(offsetof(LoaderAnime_t64665300, ____particleTransform_7)); }
	inline Transform_t3600365921 * get__particleTransform_7() const { return ____particleTransform_7; }
	inline Transform_t3600365921 ** get_address_of__particleTransform_7() { return &____particleTransform_7; }
	inline void set__particleTransform_7(Transform_t3600365921 * value)
	{
		____particleTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&____particleTransform_7), value);
	}

	inline static int32_t get_offset_of__isAnimating_8() { return static_cast<int32_t>(offsetof(LoaderAnime_t64665300, ____isAnimating_8)); }
	inline bool get__isAnimating_8() const { return ____isAnimating_8; }
	inline bool* get_address_of__isAnimating_8() { return &____isAnimating_8; }
	inline void set__isAnimating_8(bool value)
	{
		____isAnimating_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADERANIME_T64665300_H
#ifndef MONOBEHAVIOUR_T3225183318_H
#define MONOBEHAVIOUR_T3225183318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.MonoBehaviour
struct  MonoBehaviour_t3225183318  : public MonoBehaviour_t3962482529
{
public:
	// PhotonView Photon.MonoBehaviour::pvCache
	PhotonView_t2207721820 * ___pvCache_2;

public:
	inline static int32_t get_offset_of_pvCache_2() { return static_cast<int32_t>(offsetof(MonoBehaviour_t3225183318, ___pvCache_2)); }
	inline PhotonView_t2207721820 * get_pvCache_2() const { return ___pvCache_2; }
	inline PhotonView_t2207721820 ** get_address_of_pvCache_2() { return &___pvCache_2; }
	inline void set_pvCache_2(PhotonView_t2207721820 * value)
	{
		___pvCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3225183318_H
#ifndef WORKERMENU_T663890918_H
#define WORKERMENU_T663890918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorkerMenu
struct  WorkerMenu_t663890918  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUISkin WorkerMenu::Skin
	GUISkin_t1244372282 * ___Skin_2;
	// UnityEngine.Vector2 WorkerMenu::WidthAndHeight
	Vector2_t2156229523  ___WidthAndHeight_3;
	// System.String WorkerMenu::roomName
	String_t* ___roomName_4;
	// UnityEngine.Vector2 WorkerMenu::scrollPos
	Vector2_t2156229523  ___scrollPos_5;
	// System.Boolean WorkerMenu::connectFailed
	bool ___connectFailed_6;
	// System.String WorkerMenu::errorDialog
	String_t* ___errorDialog_9;
	// System.Double WorkerMenu::timeToClearDialog
	double ___timeToClearDialog_10;

public:
	inline static int32_t get_offset_of_Skin_2() { return static_cast<int32_t>(offsetof(WorkerMenu_t663890918, ___Skin_2)); }
	inline GUISkin_t1244372282 * get_Skin_2() const { return ___Skin_2; }
	inline GUISkin_t1244372282 ** get_address_of_Skin_2() { return &___Skin_2; }
	inline void set_Skin_2(GUISkin_t1244372282 * value)
	{
		___Skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_2), value);
	}

	inline static int32_t get_offset_of_WidthAndHeight_3() { return static_cast<int32_t>(offsetof(WorkerMenu_t663890918, ___WidthAndHeight_3)); }
	inline Vector2_t2156229523  get_WidthAndHeight_3() const { return ___WidthAndHeight_3; }
	inline Vector2_t2156229523 * get_address_of_WidthAndHeight_3() { return &___WidthAndHeight_3; }
	inline void set_WidthAndHeight_3(Vector2_t2156229523  value)
	{
		___WidthAndHeight_3 = value;
	}

	inline static int32_t get_offset_of_roomName_4() { return static_cast<int32_t>(offsetof(WorkerMenu_t663890918, ___roomName_4)); }
	inline String_t* get_roomName_4() const { return ___roomName_4; }
	inline String_t** get_address_of_roomName_4() { return &___roomName_4; }
	inline void set_roomName_4(String_t* value)
	{
		___roomName_4 = value;
		Il2CppCodeGenWriteBarrier((&___roomName_4), value);
	}

	inline static int32_t get_offset_of_scrollPos_5() { return static_cast<int32_t>(offsetof(WorkerMenu_t663890918, ___scrollPos_5)); }
	inline Vector2_t2156229523  get_scrollPos_5() const { return ___scrollPos_5; }
	inline Vector2_t2156229523 * get_address_of_scrollPos_5() { return &___scrollPos_5; }
	inline void set_scrollPos_5(Vector2_t2156229523  value)
	{
		___scrollPos_5 = value;
	}

	inline static int32_t get_offset_of_connectFailed_6() { return static_cast<int32_t>(offsetof(WorkerMenu_t663890918, ___connectFailed_6)); }
	inline bool get_connectFailed_6() const { return ___connectFailed_6; }
	inline bool* get_address_of_connectFailed_6() { return &___connectFailed_6; }
	inline void set_connectFailed_6(bool value)
	{
		___connectFailed_6 = value;
	}

	inline static int32_t get_offset_of_errorDialog_9() { return static_cast<int32_t>(offsetof(WorkerMenu_t663890918, ___errorDialog_9)); }
	inline String_t* get_errorDialog_9() const { return ___errorDialog_9; }
	inline String_t** get_address_of_errorDialog_9() { return &___errorDialog_9; }
	inline void set_errorDialog_9(String_t* value)
	{
		___errorDialog_9 = value;
		Il2CppCodeGenWriteBarrier((&___errorDialog_9), value);
	}

	inline static int32_t get_offset_of_timeToClearDialog_10() { return static_cast<int32_t>(offsetof(WorkerMenu_t663890918, ___timeToClearDialog_10)); }
	inline double get_timeToClearDialog_10() const { return ___timeToClearDialog_10; }
	inline double* get_address_of_timeToClearDialog_10() { return &___timeToClearDialog_10; }
	inline void set_timeToClearDialog_10(double value)
	{
		___timeToClearDialog_10 = value;
	}
};

struct WorkerMenu_t663890918_StaticFields
{
public:
	// System.String WorkerMenu::SceneNameMenu
	String_t* ___SceneNameMenu_7;
	// System.String WorkerMenu::SceneNameGame
	String_t* ___SceneNameGame_8;

public:
	inline static int32_t get_offset_of_SceneNameMenu_7() { return static_cast<int32_t>(offsetof(WorkerMenu_t663890918_StaticFields, ___SceneNameMenu_7)); }
	inline String_t* get_SceneNameMenu_7() const { return ___SceneNameMenu_7; }
	inline String_t** get_address_of_SceneNameMenu_7() { return &___SceneNameMenu_7; }
	inline void set_SceneNameMenu_7(String_t* value)
	{
		___SceneNameMenu_7 = value;
		Il2CppCodeGenWriteBarrier((&___SceneNameMenu_7), value);
	}

	inline static int32_t get_offset_of_SceneNameGame_8() { return static_cast<int32_t>(offsetof(WorkerMenu_t663890918_StaticFields, ___SceneNameGame_8)); }
	inline String_t* get_SceneNameGame_8() const { return ___SceneNameGame_8; }
	inline String_t** get_address_of_SceneNameGame_8() { return &___SceneNameGame_8; }
	inline void set_SceneNameGame_8(String_t* value)
	{
		___SceneNameGame_8 = value;
		Il2CppCodeGenWriteBarrier((&___SceneNameGame_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORKERMENU_T663890918_H
#ifndef CLICKDETECTOR_T4035850562_H
#define CLICKDETECTOR_T4035850562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickDetector
struct  ClickDetector_t4035850562  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKDETECTOR_T4035850562_H
#ifndef GAMELOGIC_T3731221617_H
#define GAMELOGIC_T3731221617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameLogic
struct  GameLogic_t3731221617  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct GameLogic_t3731221617_StaticFields
{
public:
	// System.Int32 GameLogic::playerWhoIsIt
	int32_t ___playerWhoIsIt_2;
	// PhotonView GameLogic::ScenePhotonView
	PhotonView_t2207721820 * ___ScenePhotonView_3;

public:
	inline static int32_t get_offset_of_playerWhoIsIt_2() { return static_cast<int32_t>(offsetof(GameLogic_t3731221617_StaticFields, ___playerWhoIsIt_2)); }
	inline int32_t get_playerWhoIsIt_2() const { return ___playerWhoIsIt_2; }
	inline int32_t* get_address_of_playerWhoIsIt_2() { return &___playerWhoIsIt_2; }
	inline void set_playerWhoIsIt_2(int32_t value)
	{
		___playerWhoIsIt_2 = value;
	}

	inline static int32_t get_offset_of_ScenePhotonView_3() { return static_cast<int32_t>(offsetof(GameLogic_t3731221617_StaticFields, ___ScenePhotonView_3)); }
	inline PhotonView_t2207721820 * get_ScenePhotonView_3() const { return ___ScenePhotonView_3; }
	inline PhotonView_t2207721820 ** get_address_of_ScenePhotonView_3() { return &___ScenePhotonView_3; }
	inline void set_ScenePhotonView_3(PhotonView_t2207721820 * value)
	{
		___ScenePhotonView_3 = value;
		Il2CppCodeGenWriteBarrier((&___ScenePhotonView_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMELOGIC_T3731221617_H
#ifndef ONCLICKLOADSOMETHING_T137357684_H
#define ONCLICKLOADSOMETHING_T137357684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickLoadSomething
struct  OnClickLoadSomething_t137357684  : public MonoBehaviour_t3962482529
{
public:
	// OnClickLoadSomething/ResourceTypeOption OnClickLoadSomething::ResourceTypeToLoad
	uint8_t ___ResourceTypeToLoad_2;
	// System.String OnClickLoadSomething::ResourceToLoad
	String_t* ___ResourceToLoad_3;

public:
	inline static int32_t get_offset_of_ResourceTypeToLoad_2() { return static_cast<int32_t>(offsetof(OnClickLoadSomething_t137357684, ___ResourceTypeToLoad_2)); }
	inline uint8_t get_ResourceTypeToLoad_2() const { return ___ResourceTypeToLoad_2; }
	inline uint8_t* get_address_of_ResourceTypeToLoad_2() { return &___ResourceTypeToLoad_2; }
	inline void set_ResourceTypeToLoad_2(uint8_t value)
	{
		___ResourceTypeToLoad_2 = value;
	}

	inline static int32_t get_offset_of_ResourceToLoad_3() { return static_cast<int32_t>(offsetof(OnClickLoadSomething_t137357684, ___ResourceToLoad_3)); }
	inline String_t* get_ResourceToLoad_3() const { return ___ResourceToLoad_3; }
	inline String_t** get_address_of_ResourceToLoad_3() { return &___ResourceToLoad_3; }
	inline void set_ResourceToLoad_3(String_t* value)
	{
		___ResourceToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceToLoad_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKLOADSOMETHING_T137357684_H
#ifndef CAMERAWORK_T2164329516_H
#define CAMERAWORK_T2164329516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoAnimator.CameraWork
struct  CameraWork_t2164329516  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ExitGames.Demos.DemoAnimator.CameraWork::distance
	float ___distance_2;
	// System.Single ExitGames.Demos.DemoAnimator.CameraWork::height
	float ___height_3;
	// System.Single ExitGames.Demos.DemoAnimator.CameraWork::heightSmoothLag
	float ___heightSmoothLag_4;
	// UnityEngine.Vector3 ExitGames.Demos.DemoAnimator.CameraWork::centerOffset
	Vector3_t3722313464  ___centerOffset_5;
	// System.Boolean ExitGames.Demos.DemoAnimator.CameraWork::followOnStart
	bool ___followOnStart_6;
	// UnityEngine.Transform ExitGames.Demos.DemoAnimator.CameraWork::cameraTransform
	Transform_t3600365921 * ___cameraTransform_7;
	// System.Boolean ExitGames.Demos.DemoAnimator.CameraWork::isFollowing
	bool ___isFollowing_8;
	// System.Single ExitGames.Demos.DemoAnimator.CameraWork::heightVelocity
	float ___heightVelocity_9;
	// System.Single ExitGames.Demos.DemoAnimator.CameraWork::targetHeight
	float ___targetHeight_10;

public:
	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(CameraWork_t2164329516, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(CameraWork_t2164329516, ___height_3)); }
	inline float get_height_3() const { return ___height_3; }
	inline float* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(float value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_heightSmoothLag_4() { return static_cast<int32_t>(offsetof(CameraWork_t2164329516, ___heightSmoothLag_4)); }
	inline float get_heightSmoothLag_4() const { return ___heightSmoothLag_4; }
	inline float* get_address_of_heightSmoothLag_4() { return &___heightSmoothLag_4; }
	inline void set_heightSmoothLag_4(float value)
	{
		___heightSmoothLag_4 = value;
	}

	inline static int32_t get_offset_of_centerOffset_5() { return static_cast<int32_t>(offsetof(CameraWork_t2164329516, ___centerOffset_5)); }
	inline Vector3_t3722313464  get_centerOffset_5() const { return ___centerOffset_5; }
	inline Vector3_t3722313464 * get_address_of_centerOffset_5() { return &___centerOffset_5; }
	inline void set_centerOffset_5(Vector3_t3722313464  value)
	{
		___centerOffset_5 = value;
	}

	inline static int32_t get_offset_of_followOnStart_6() { return static_cast<int32_t>(offsetof(CameraWork_t2164329516, ___followOnStart_6)); }
	inline bool get_followOnStart_6() const { return ___followOnStart_6; }
	inline bool* get_address_of_followOnStart_6() { return &___followOnStart_6; }
	inline void set_followOnStart_6(bool value)
	{
		___followOnStart_6 = value;
	}

	inline static int32_t get_offset_of_cameraTransform_7() { return static_cast<int32_t>(offsetof(CameraWork_t2164329516, ___cameraTransform_7)); }
	inline Transform_t3600365921 * get_cameraTransform_7() const { return ___cameraTransform_7; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_7() { return &___cameraTransform_7; }
	inline void set_cameraTransform_7(Transform_t3600365921 * value)
	{
		___cameraTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_7), value);
	}

	inline static int32_t get_offset_of_isFollowing_8() { return static_cast<int32_t>(offsetof(CameraWork_t2164329516, ___isFollowing_8)); }
	inline bool get_isFollowing_8() const { return ___isFollowing_8; }
	inline bool* get_address_of_isFollowing_8() { return &___isFollowing_8; }
	inline void set_isFollowing_8(bool value)
	{
		___isFollowing_8 = value;
	}

	inline static int32_t get_offset_of_heightVelocity_9() { return static_cast<int32_t>(offsetof(CameraWork_t2164329516, ___heightVelocity_9)); }
	inline float get_heightVelocity_9() const { return ___heightVelocity_9; }
	inline float* get_address_of_heightVelocity_9() { return &___heightVelocity_9; }
	inline void set_heightVelocity_9(float value)
	{
		___heightVelocity_9 = value;
	}

	inline static int32_t get_offset_of_targetHeight_10() { return static_cast<int32_t>(offsetof(CameraWork_t2164329516, ___targetHeight_10)); }
	inline float get_targetHeight_10() const { return ___targetHeight_10; }
	inline float* get_address_of_targetHeight_10() { return &___targetHeight_10; }
	inline void set_targetHeight_10(float value)
	{
		___targetHeight_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAWORK_T2164329516_H
#ifndef JUMPANDRUNMOVEMENT_T3103546220_H
#define JUMPANDRUNMOVEMENT_T3103546220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JumpAndRunMovement
struct  JumpAndRunMovement_t3103546220  : public MonoBehaviour_t3962482529
{
public:
	// System.Single JumpAndRunMovement::Speed
	float ___Speed_2;
	// System.Single JumpAndRunMovement::JumpForce
	float ___JumpForce_3;
	// UnityEngine.Animator JumpAndRunMovement::m_Animator
	Animator_t434523843 * ___m_Animator_4;
	// UnityEngine.Rigidbody2D JumpAndRunMovement::m_Body
	Rigidbody2D_t939494601 * ___m_Body_5;
	// PhotonView JumpAndRunMovement::m_PhotonView
	PhotonView_t2207721820 * ___m_PhotonView_6;
	// System.Boolean JumpAndRunMovement::m_IsGrounded
	bool ___m_IsGrounded_7;

public:
	inline static int32_t get_offset_of_Speed_2() { return static_cast<int32_t>(offsetof(JumpAndRunMovement_t3103546220, ___Speed_2)); }
	inline float get_Speed_2() const { return ___Speed_2; }
	inline float* get_address_of_Speed_2() { return &___Speed_2; }
	inline void set_Speed_2(float value)
	{
		___Speed_2 = value;
	}

	inline static int32_t get_offset_of_JumpForce_3() { return static_cast<int32_t>(offsetof(JumpAndRunMovement_t3103546220, ___JumpForce_3)); }
	inline float get_JumpForce_3() const { return ___JumpForce_3; }
	inline float* get_address_of_JumpForce_3() { return &___JumpForce_3; }
	inline void set_JumpForce_3(float value)
	{
		___JumpForce_3 = value;
	}

	inline static int32_t get_offset_of_m_Animator_4() { return static_cast<int32_t>(offsetof(JumpAndRunMovement_t3103546220, ___m_Animator_4)); }
	inline Animator_t434523843 * get_m_Animator_4() const { return ___m_Animator_4; }
	inline Animator_t434523843 ** get_address_of_m_Animator_4() { return &___m_Animator_4; }
	inline void set_m_Animator_4(Animator_t434523843 * value)
	{
		___m_Animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_4), value);
	}

	inline static int32_t get_offset_of_m_Body_5() { return static_cast<int32_t>(offsetof(JumpAndRunMovement_t3103546220, ___m_Body_5)); }
	inline Rigidbody2D_t939494601 * get_m_Body_5() const { return ___m_Body_5; }
	inline Rigidbody2D_t939494601 ** get_address_of_m_Body_5() { return &___m_Body_5; }
	inline void set_m_Body_5(Rigidbody2D_t939494601 * value)
	{
		___m_Body_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Body_5), value);
	}

	inline static int32_t get_offset_of_m_PhotonView_6() { return static_cast<int32_t>(offsetof(JumpAndRunMovement_t3103546220, ___m_PhotonView_6)); }
	inline PhotonView_t2207721820 * get_m_PhotonView_6() const { return ___m_PhotonView_6; }
	inline PhotonView_t2207721820 ** get_address_of_m_PhotonView_6() { return &___m_PhotonView_6; }
	inline void set_m_PhotonView_6(PhotonView_t2207721820 * value)
	{
		___m_PhotonView_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PhotonView_6), value);
	}

	inline static int32_t get_offset_of_m_IsGrounded_7() { return static_cast<int32_t>(offsetof(JumpAndRunMovement_t3103546220, ___m_IsGrounded_7)); }
	inline bool get_m_IsGrounded_7() const { return ___m_IsGrounded_7; }
	inline bool* get_address_of_m_IsGrounded_7() { return &___m_IsGrounded_7; }
	inline void set_m_IsGrounded_7(bool value)
	{
		___m_IsGrounded_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMPANDRUNMOVEMENT_T3103546220_H
#ifndef GUIFRIENDSINROOM_T3253954278_H
#define GUIFRIENDSINROOM_T3253954278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIFriendsInRoom
struct  GUIFriendsInRoom_t3253954278  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rect GUIFriendsInRoom::GuiRect
	Rect_t2360479859  ___GuiRect_2;

public:
	inline static int32_t get_offset_of_GuiRect_2() { return static_cast<int32_t>(offsetof(GUIFriendsInRoom_t3253954278, ___GuiRect_2)); }
	inline Rect_t2360479859  get_GuiRect_2() const { return ___GuiRect_2; }
	inline Rect_t2360479859 * get_address_of_GuiRect_2() { return &___GuiRect_2; }
	inline void set_GuiRect_2(Rect_t2360479859  value)
	{
		___GuiRect_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIFRIENDSINROOM_T3253954278_H
#ifndef HUBGUI_T2100725939_H
#define HUBGUI_T2100725939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HubGui
struct  HubGui_t2100725939  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUISkin HubGui::Skin
	GUISkin_t1244372282 * ___Skin_2;
	// UnityEngine.Vector2 HubGui::scrollPos
	Vector2_t2156229523  ___scrollPos_3;
	// System.String HubGui::demoDescription
	String_t* ___demoDescription_4;
	// HubGui/DemoBtn HubGui::demoBtn
	DemoBtn_t3232561033  ___demoBtn_5;
	// HubGui/DemoBtn HubGui::webLink
	DemoBtn_t3232561033  ___webLink_6;
	// UnityEngine.GUIStyle HubGui::m_Headline
	GUIStyle_t3956901511 * ___m_Headline_7;

public:
	inline static int32_t get_offset_of_Skin_2() { return static_cast<int32_t>(offsetof(HubGui_t2100725939, ___Skin_2)); }
	inline GUISkin_t1244372282 * get_Skin_2() const { return ___Skin_2; }
	inline GUISkin_t1244372282 ** get_address_of_Skin_2() { return &___Skin_2; }
	inline void set_Skin_2(GUISkin_t1244372282 * value)
	{
		___Skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_2), value);
	}

	inline static int32_t get_offset_of_scrollPos_3() { return static_cast<int32_t>(offsetof(HubGui_t2100725939, ___scrollPos_3)); }
	inline Vector2_t2156229523  get_scrollPos_3() const { return ___scrollPos_3; }
	inline Vector2_t2156229523 * get_address_of_scrollPos_3() { return &___scrollPos_3; }
	inline void set_scrollPos_3(Vector2_t2156229523  value)
	{
		___scrollPos_3 = value;
	}

	inline static int32_t get_offset_of_demoDescription_4() { return static_cast<int32_t>(offsetof(HubGui_t2100725939, ___demoDescription_4)); }
	inline String_t* get_demoDescription_4() const { return ___demoDescription_4; }
	inline String_t** get_address_of_demoDescription_4() { return &___demoDescription_4; }
	inline void set_demoDescription_4(String_t* value)
	{
		___demoDescription_4 = value;
		Il2CppCodeGenWriteBarrier((&___demoDescription_4), value);
	}

	inline static int32_t get_offset_of_demoBtn_5() { return static_cast<int32_t>(offsetof(HubGui_t2100725939, ___demoBtn_5)); }
	inline DemoBtn_t3232561033  get_demoBtn_5() const { return ___demoBtn_5; }
	inline DemoBtn_t3232561033 * get_address_of_demoBtn_5() { return &___demoBtn_5; }
	inline void set_demoBtn_5(DemoBtn_t3232561033  value)
	{
		___demoBtn_5 = value;
	}

	inline static int32_t get_offset_of_webLink_6() { return static_cast<int32_t>(offsetof(HubGui_t2100725939, ___webLink_6)); }
	inline DemoBtn_t3232561033  get_webLink_6() const { return ___webLink_6; }
	inline DemoBtn_t3232561033 * get_address_of_webLink_6() { return &___webLink_6; }
	inline void set_webLink_6(DemoBtn_t3232561033  value)
	{
		___webLink_6 = value;
	}

	inline static int32_t get_offset_of_m_Headline_7() { return static_cast<int32_t>(offsetof(HubGui_t2100725939, ___m_Headline_7)); }
	inline GUIStyle_t3956901511 * get_m_Headline_7() const { return ___m_Headline_7; }
	inline GUIStyle_t3956901511 ** get_address_of_m_Headline_7() { return &___m_Headline_7; }
	inline void set_m_Headline_7(GUIStyle_t3956901511 * value)
	{
		___m_Headline_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Headline_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUBGUI_T2100725939_H
#ifndef ONCLICKRIGHTDESTROY_T2880403765_H
#define ONCLICKRIGHTDESTROY_T2880403765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickRightDestroy
struct  OnClickRightDestroy_t2880403765  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKRIGHTDESTROY_T2880403765_H
#ifndef MOVECAM_T1672202783_H
#define MOVECAM_T1672202783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveCam
struct  MoveCam_t1672202783  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 MoveCam::originalPos
	Vector3_t3722313464  ___originalPos_2;
	// UnityEngine.Vector3 MoveCam::randomPos
	Vector3_t3722313464  ___randomPos_3;
	// UnityEngine.Transform MoveCam::camTransform
	Transform_t3600365921 * ___camTransform_4;
	// UnityEngine.Transform MoveCam::lookAt
	Transform_t3600365921 * ___lookAt_5;

public:
	inline static int32_t get_offset_of_originalPos_2() { return static_cast<int32_t>(offsetof(MoveCam_t1672202783, ___originalPos_2)); }
	inline Vector3_t3722313464  get_originalPos_2() const { return ___originalPos_2; }
	inline Vector3_t3722313464 * get_address_of_originalPos_2() { return &___originalPos_2; }
	inline void set_originalPos_2(Vector3_t3722313464  value)
	{
		___originalPos_2 = value;
	}

	inline static int32_t get_offset_of_randomPos_3() { return static_cast<int32_t>(offsetof(MoveCam_t1672202783, ___randomPos_3)); }
	inline Vector3_t3722313464  get_randomPos_3() const { return ___randomPos_3; }
	inline Vector3_t3722313464 * get_address_of_randomPos_3() { return &___randomPos_3; }
	inline void set_randomPos_3(Vector3_t3722313464  value)
	{
		___randomPos_3 = value;
	}

	inline static int32_t get_offset_of_camTransform_4() { return static_cast<int32_t>(offsetof(MoveCam_t1672202783, ___camTransform_4)); }
	inline Transform_t3600365921 * get_camTransform_4() const { return ___camTransform_4; }
	inline Transform_t3600365921 ** get_address_of_camTransform_4() { return &___camTransform_4; }
	inline void set_camTransform_4(Transform_t3600365921 * value)
	{
		___camTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___camTransform_4), value);
	}

	inline static int32_t get_offset_of_lookAt_5() { return static_cast<int32_t>(offsetof(MoveCam_t1672202783, ___lookAt_5)); }
	inline Transform_t3600365921 * get_lookAt_5() const { return ___lookAt_5; }
	inline Transform_t3600365921 ** get_address_of_lookAt_5() { return &___lookAt_5; }
	inline void set_lookAt_5(Transform_t3600365921 * value)
	{
		___lookAt_5 = value;
		Il2CppCodeGenWriteBarrier((&___lookAt_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVECAM_T1672202783_H
#ifndef DEMOHUBMANAGER_T3958170276_H
#define DEMOHUBMANAGER_T3958170276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoHubManager
struct  DemoHubManager_t3958170276  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ExitGames.Demos.DemoHubManager::TitleText
	Text_t1901882714 * ___TitleText_2;
	// UnityEngine.UI.Text ExitGames.Demos.DemoHubManager::DescriptionText
	Text_t1901882714 * ___DescriptionText_3;
	// UnityEngine.GameObject ExitGames.Demos.DemoHubManager::OpenSceneButton
	GameObject_t1113636619 * ___OpenSceneButton_4;
	// UnityEngine.GameObject ExitGames.Demos.DemoHubManager::OpenTutorialLinkButton
	GameObject_t1113636619 * ___OpenTutorialLinkButton_5;
	// UnityEngine.GameObject ExitGames.Demos.DemoHubManager::OpenDocLinkButton
	GameObject_t1113636619 * ___OpenDocLinkButton_6;
	// System.String ExitGames.Demos.DemoHubManager::MainDemoWebLink
	String_t* ___MainDemoWebLink_7;
	// System.Collections.Generic.Dictionary`2<System.String,ExitGames.Demos.DemoHubManager/DemoData> ExitGames.Demos.DemoHubManager::_data
	Dictionary_2_t994373823 * ____data_8;
	// System.String ExitGames.Demos.DemoHubManager::currentSelection
	String_t* ___currentSelection_9;
	// UnityEngine.Rect ExitGames.Demos.DemoHubManager::BugFixbounds
	Rect_t2360479859  ___BugFixbounds_10;

public:
	inline static int32_t get_offset_of_TitleText_2() { return static_cast<int32_t>(offsetof(DemoHubManager_t3958170276, ___TitleText_2)); }
	inline Text_t1901882714 * get_TitleText_2() const { return ___TitleText_2; }
	inline Text_t1901882714 ** get_address_of_TitleText_2() { return &___TitleText_2; }
	inline void set_TitleText_2(Text_t1901882714 * value)
	{
		___TitleText_2 = value;
		Il2CppCodeGenWriteBarrier((&___TitleText_2), value);
	}

	inline static int32_t get_offset_of_DescriptionText_3() { return static_cast<int32_t>(offsetof(DemoHubManager_t3958170276, ___DescriptionText_3)); }
	inline Text_t1901882714 * get_DescriptionText_3() const { return ___DescriptionText_3; }
	inline Text_t1901882714 ** get_address_of_DescriptionText_3() { return &___DescriptionText_3; }
	inline void set_DescriptionText_3(Text_t1901882714 * value)
	{
		___DescriptionText_3 = value;
		Il2CppCodeGenWriteBarrier((&___DescriptionText_3), value);
	}

	inline static int32_t get_offset_of_OpenSceneButton_4() { return static_cast<int32_t>(offsetof(DemoHubManager_t3958170276, ___OpenSceneButton_4)); }
	inline GameObject_t1113636619 * get_OpenSceneButton_4() const { return ___OpenSceneButton_4; }
	inline GameObject_t1113636619 ** get_address_of_OpenSceneButton_4() { return &___OpenSceneButton_4; }
	inline void set_OpenSceneButton_4(GameObject_t1113636619 * value)
	{
		___OpenSceneButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___OpenSceneButton_4), value);
	}

	inline static int32_t get_offset_of_OpenTutorialLinkButton_5() { return static_cast<int32_t>(offsetof(DemoHubManager_t3958170276, ___OpenTutorialLinkButton_5)); }
	inline GameObject_t1113636619 * get_OpenTutorialLinkButton_5() const { return ___OpenTutorialLinkButton_5; }
	inline GameObject_t1113636619 ** get_address_of_OpenTutorialLinkButton_5() { return &___OpenTutorialLinkButton_5; }
	inline void set_OpenTutorialLinkButton_5(GameObject_t1113636619 * value)
	{
		___OpenTutorialLinkButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___OpenTutorialLinkButton_5), value);
	}

	inline static int32_t get_offset_of_OpenDocLinkButton_6() { return static_cast<int32_t>(offsetof(DemoHubManager_t3958170276, ___OpenDocLinkButton_6)); }
	inline GameObject_t1113636619 * get_OpenDocLinkButton_6() const { return ___OpenDocLinkButton_6; }
	inline GameObject_t1113636619 ** get_address_of_OpenDocLinkButton_6() { return &___OpenDocLinkButton_6; }
	inline void set_OpenDocLinkButton_6(GameObject_t1113636619 * value)
	{
		___OpenDocLinkButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___OpenDocLinkButton_6), value);
	}

	inline static int32_t get_offset_of_MainDemoWebLink_7() { return static_cast<int32_t>(offsetof(DemoHubManager_t3958170276, ___MainDemoWebLink_7)); }
	inline String_t* get_MainDemoWebLink_7() const { return ___MainDemoWebLink_7; }
	inline String_t** get_address_of_MainDemoWebLink_7() { return &___MainDemoWebLink_7; }
	inline void set_MainDemoWebLink_7(String_t* value)
	{
		___MainDemoWebLink_7 = value;
		Il2CppCodeGenWriteBarrier((&___MainDemoWebLink_7), value);
	}

	inline static int32_t get_offset_of__data_8() { return static_cast<int32_t>(offsetof(DemoHubManager_t3958170276, ____data_8)); }
	inline Dictionary_2_t994373823 * get__data_8() const { return ____data_8; }
	inline Dictionary_2_t994373823 ** get_address_of__data_8() { return &____data_8; }
	inline void set__data_8(Dictionary_2_t994373823 * value)
	{
		____data_8 = value;
		Il2CppCodeGenWriteBarrier((&____data_8), value);
	}

	inline static int32_t get_offset_of_currentSelection_9() { return static_cast<int32_t>(offsetof(DemoHubManager_t3958170276, ___currentSelection_9)); }
	inline String_t* get_currentSelection_9() const { return ___currentSelection_9; }
	inline String_t** get_address_of_currentSelection_9() { return &___currentSelection_9; }
	inline void set_currentSelection_9(String_t* value)
	{
		___currentSelection_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentSelection_9), value);
	}

	inline static int32_t get_offset_of_BugFixbounds_10() { return static_cast<int32_t>(offsetof(DemoHubManager_t3958170276, ___BugFixbounds_10)); }
	inline Rect_t2360479859  get_BugFixbounds_10() const { return ___BugFixbounds_10; }
	inline Rect_t2360479859 * get_address_of_BugFixbounds_10() { return &___BugFixbounds_10; }
	inline void set_BugFixbounds_10(Rect_t2360479859  value)
	{
		___BugFixbounds_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOHUBMANAGER_T3958170276_H
#ifndef TODEMOHUBBUTTON_T117408609_H
#define TODEMOHUBBUTTON_T117408609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.ToDemoHubButton
struct  ToDemoHubButton_t117408609  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CanvasGroup ExitGames.Demos.ToDemoHubButton::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_3;

public:
	inline static int32_t get_offset_of__canvasGroup_3() { return static_cast<int32_t>(offsetof(ToDemoHubButton_t117408609, ____canvasGroup_3)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_3() const { return ____canvasGroup_3; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_3() { return &____canvasGroup_3; }
	inline void set__canvasGroup_3(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_3), value);
	}
};

struct ToDemoHubButton_t117408609_StaticFields
{
public:
	// ExitGames.Demos.ToDemoHubButton ExitGames.Demos.ToDemoHubButton::instance
	ToDemoHubButton_t117408609 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ToDemoHubButton_t117408609_StaticFields, ___instance_2)); }
	inline ToDemoHubButton_t117408609 * get_instance_2() const { return ___instance_2; }
	inline ToDemoHubButton_t117408609 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ToDemoHubButton_t117408609 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TODEMOHUBBUTTON_T117408609_H
#ifndef GUIFRIENDFINDING_T537375819_H
#define GUIFRIENDFINDING_T537375819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIFriendFinding
struct  GUIFriendFinding_t537375819  : public MonoBehaviour_t3962482529
{
public:
	// System.String[] GUIFriendFinding::friendListOfSomeCommunity
	StringU5BU5D_t1281789340* ___friendListOfSomeCommunity_2;
	// UnityEngine.Rect GUIFriendFinding::GuiRect
	Rect_t2360479859  ___GuiRect_3;
	// System.String GUIFriendFinding::ExpectedUsers
	String_t* ___ExpectedUsers_4;

public:
	inline static int32_t get_offset_of_friendListOfSomeCommunity_2() { return static_cast<int32_t>(offsetof(GUIFriendFinding_t537375819, ___friendListOfSomeCommunity_2)); }
	inline StringU5BU5D_t1281789340* get_friendListOfSomeCommunity_2() const { return ___friendListOfSomeCommunity_2; }
	inline StringU5BU5D_t1281789340** get_address_of_friendListOfSomeCommunity_2() { return &___friendListOfSomeCommunity_2; }
	inline void set_friendListOfSomeCommunity_2(StringU5BU5D_t1281789340* value)
	{
		___friendListOfSomeCommunity_2 = value;
		Il2CppCodeGenWriteBarrier((&___friendListOfSomeCommunity_2), value);
	}

	inline static int32_t get_offset_of_GuiRect_3() { return static_cast<int32_t>(offsetof(GUIFriendFinding_t537375819, ___GuiRect_3)); }
	inline Rect_t2360479859  get_GuiRect_3() const { return ___GuiRect_3; }
	inline Rect_t2360479859 * get_address_of_GuiRect_3() { return &___GuiRect_3; }
	inline void set_GuiRect_3(Rect_t2360479859  value)
	{
		___GuiRect_3 = value;
	}

	inline static int32_t get_offset_of_ExpectedUsers_4() { return static_cast<int32_t>(offsetof(GUIFriendFinding_t537375819, ___ExpectedUsers_4)); }
	inline String_t* get_ExpectedUsers_4() const { return ___ExpectedUsers_4; }
	inline String_t** get_address_of_ExpectedUsers_4() { return &___ExpectedUsers_4; }
	inline void set_ExpectedUsers_4(String_t* value)
	{
		___ExpectedUsers_4 = value;
		Il2CppCodeGenWriteBarrier((&___ExpectedUsers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIFRIENDFINDING_T537375819_H
#ifndef CHANNELSELECTOR_T3480308131_H
#define CHANNELSELECTOR_T3480308131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChannelSelector
struct  ChannelSelector_t3480308131  : public MonoBehaviour_t3962482529
{
public:
	// System.String ChannelSelector::Channel
	String_t* ___Channel_2;

public:
	inline static int32_t get_offset_of_Channel_2() { return static_cast<int32_t>(offsetof(ChannelSelector_t3480308131, ___Channel_2)); }
	inline String_t* get_Channel_2() const { return ___Channel_2; }
	inline String_t** get_address_of_Channel_2() { return &___Channel_2; }
	inline void set_Channel_2(String_t* value)
	{
		___Channel_2 = value;
		Il2CppCodeGenWriteBarrier((&___Channel_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELSELECTOR_T3480308131_H
#ifndef CHATAPPIDCHECKERUI_T722438586_H
#define CHATAPPIDCHECKERUI_T722438586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatAppIdCheckerUI
struct  ChatAppIdCheckerUI_t722438586  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ChatAppIdCheckerUI::Description
	Text_t1901882714 * ___Description_2;

public:
	inline static int32_t get_offset_of_Description_2() { return static_cast<int32_t>(offsetof(ChatAppIdCheckerUI_t722438586, ___Description_2)); }
	inline Text_t1901882714 * get_Description_2() const { return ___Description_2; }
	inline Text_t1901882714 ** get_address_of_Description_2() { return &___Description_2; }
	inline void set_Description_2(Text_t1901882714 * value)
	{
		___Description_2 = value;
		Il2CppCodeGenWriteBarrier((&___Description_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATAPPIDCHECKERUI_T722438586_H
#ifndef CHATGUI_T3673337520_H
#define CHATGUI_T3673337520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatGui
struct  ChatGui_t3673337520  : public MonoBehaviour_t3962482529
{
public:
	// System.String[] ChatGui::ChannelsToJoinOnConnect
	StringU5BU5D_t1281789340* ___ChannelsToJoinOnConnect_2;
	// System.String[] ChatGui::FriendsList
	StringU5BU5D_t1281789340* ___FriendsList_3;
	// System.Int32 ChatGui::HistoryLengthToFetch
	int32_t ___HistoryLengthToFetch_4;
	// System.String ChatGui::<UserName>k__BackingField
	String_t* ___U3CUserNameU3Ek__BackingField_5;
	// System.String ChatGui::selectedChannelName
	String_t* ___selectedChannelName_6;
	// ExitGames.Client.Photon.Chat.ChatClient ChatGui::chatClient
	ChatClient_t3322764984 * ___chatClient_7;
	// UnityEngine.GameObject ChatGui::missingAppIdErrorPanel
	GameObject_t1113636619 * ___missingAppIdErrorPanel_8;
	// UnityEngine.GameObject ChatGui::ConnectingLabel
	GameObject_t1113636619 * ___ConnectingLabel_9;
	// UnityEngine.RectTransform ChatGui::ChatPanel
	RectTransform_t3704657025 * ___ChatPanel_10;
	// UnityEngine.GameObject ChatGui::UserIdFormPanel
	GameObject_t1113636619 * ___UserIdFormPanel_11;
	// UnityEngine.UI.InputField ChatGui::InputFieldChat
	InputField_t3762917431 * ___InputFieldChat_12;
	// UnityEngine.UI.Text ChatGui::CurrentChannelText
	Text_t1901882714 * ___CurrentChannelText_13;
	// UnityEngine.UI.Toggle ChatGui::ChannelToggleToInstantiate
	Toggle_t2735377061 * ___ChannelToggleToInstantiate_14;
	// UnityEngine.GameObject ChatGui::FriendListUiItemtoInstantiate
	GameObject_t1113636619 * ___FriendListUiItemtoInstantiate_15;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UI.Toggle> ChatGui::channelToggles
	Dictionary_2_t2520633360 * ___channelToggles_16;
	// System.Collections.Generic.Dictionary`2<System.String,FriendItem> ChatGui::friendListItemLUT
	Dictionary_2_t2701966392 * ___friendListItemLUT_17;
	// System.Boolean ChatGui::ShowState
	bool ___ShowState_18;
	// UnityEngine.GameObject ChatGui::Title
	GameObject_t1113636619 * ___Title_19;
	// UnityEngine.UI.Text ChatGui::StateText
	Text_t1901882714 * ___StateText_20;
	// UnityEngine.UI.Text ChatGui::UserIdText
	Text_t1901882714 * ___UserIdText_21;
	// System.Int32 ChatGui::TestLength
	int32_t ___TestLength_23;
	// System.Byte[] ChatGui::testBytes
	ByteU5BU5D_t4116647657* ___testBytes_24;

public:
	inline static int32_t get_offset_of_ChannelsToJoinOnConnect_2() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ChannelsToJoinOnConnect_2)); }
	inline StringU5BU5D_t1281789340* get_ChannelsToJoinOnConnect_2() const { return ___ChannelsToJoinOnConnect_2; }
	inline StringU5BU5D_t1281789340** get_address_of_ChannelsToJoinOnConnect_2() { return &___ChannelsToJoinOnConnect_2; }
	inline void set_ChannelsToJoinOnConnect_2(StringU5BU5D_t1281789340* value)
	{
		___ChannelsToJoinOnConnect_2 = value;
		Il2CppCodeGenWriteBarrier((&___ChannelsToJoinOnConnect_2), value);
	}

	inline static int32_t get_offset_of_FriendsList_3() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___FriendsList_3)); }
	inline StringU5BU5D_t1281789340* get_FriendsList_3() const { return ___FriendsList_3; }
	inline StringU5BU5D_t1281789340** get_address_of_FriendsList_3() { return &___FriendsList_3; }
	inline void set_FriendsList_3(StringU5BU5D_t1281789340* value)
	{
		___FriendsList_3 = value;
		Il2CppCodeGenWriteBarrier((&___FriendsList_3), value);
	}

	inline static int32_t get_offset_of_HistoryLengthToFetch_4() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___HistoryLengthToFetch_4)); }
	inline int32_t get_HistoryLengthToFetch_4() const { return ___HistoryLengthToFetch_4; }
	inline int32_t* get_address_of_HistoryLengthToFetch_4() { return &___HistoryLengthToFetch_4; }
	inline void set_HistoryLengthToFetch_4(int32_t value)
	{
		___HistoryLengthToFetch_4 = value;
	}

	inline static int32_t get_offset_of_U3CUserNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___U3CUserNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CUserNameU3Ek__BackingField_5() const { return ___U3CUserNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CUserNameU3Ek__BackingField_5() { return &___U3CUserNameU3Ek__BackingField_5; }
	inline void set_U3CUserNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CUserNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserNameU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_selectedChannelName_6() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___selectedChannelName_6)); }
	inline String_t* get_selectedChannelName_6() const { return ___selectedChannelName_6; }
	inline String_t** get_address_of_selectedChannelName_6() { return &___selectedChannelName_6; }
	inline void set_selectedChannelName_6(String_t* value)
	{
		___selectedChannelName_6 = value;
		Il2CppCodeGenWriteBarrier((&___selectedChannelName_6), value);
	}

	inline static int32_t get_offset_of_chatClient_7() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___chatClient_7)); }
	inline ChatClient_t3322764984 * get_chatClient_7() const { return ___chatClient_7; }
	inline ChatClient_t3322764984 ** get_address_of_chatClient_7() { return &___chatClient_7; }
	inline void set_chatClient_7(ChatClient_t3322764984 * value)
	{
		___chatClient_7 = value;
		Il2CppCodeGenWriteBarrier((&___chatClient_7), value);
	}

	inline static int32_t get_offset_of_missingAppIdErrorPanel_8() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___missingAppIdErrorPanel_8)); }
	inline GameObject_t1113636619 * get_missingAppIdErrorPanel_8() const { return ___missingAppIdErrorPanel_8; }
	inline GameObject_t1113636619 ** get_address_of_missingAppIdErrorPanel_8() { return &___missingAppIdErrorPanel_8; }
	inline void set_missingAppIdErrorPanel_8(GameObject_t1113636619 * value)
	{
		___missingAppIdErrorPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___missingAppIdErrorPanel_8), value);
	}

	inline static int32_t get_offset_of_ConnectingLabel_9() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ConnectingLabel_9)); }
	inline GameObject_t1113636619 * get_ConnectingLabel_9() const { return ___ConnectingLabel_9; }
	inline GameObject_t1113636619 ** get_address_of_ConnectingLabel_9() { return &___ConnectingLabel_9; }
	inline void set_ConnectingLabel_9(GameObject_t1113636619 * value)
	{
		___ConnectingLabel_9 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectingLabel_9), value);
	}

	inline static int32_t get_offset_of_ChatPanel_10() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ChatPanel_10)); }
	inline RectTransform_t3704657025 * get_ChatPanel_10() const { return ___ChatPanel_10; }
	inline RectTransform_t3704657025 ** get_address_of_ChatPanel_10() { return &___ChatPanel_10; }
	inline void set_ChatPanel_10(RectTransform_t3704657025 * value)
	{
		___ChatPanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___ChatPanel_10), value);
	}

	inline static int32_t get_offset_of_UserIdFormPanel_11() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___UserIdFormPanel_11)); }
	inline GameObject_t1113636619 * get_UserIdFormPanel_11() const { return ___UserIdFormPanel_11; }
	inline GameObject_t1113636619 ** get_address_of_UserIdFormPanel_11() { return &___UserIdFormPanel_11; }
	inline void set_UserIdFormPanel_11(GameObject_t1113636619 * value)
	{
		___UserIdFormPanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___UserIdFormPanel_11), value);
	}

	inline static int32_t get_offset_of_InputFieldChat_12() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___InputFieldChat_12)); }
	inline InputField_t3762917431 * get_InputFieldChat_12() const { return ___InputFieldChat_12; }
	inline InputField_t3762917431 ** get_address_of_InputFieldChat_12() { return &___InputFieldChat_12; }
	inline void set_InputFieldChat_12(InputField_t3762917431 * value)
	{
		___InputFieldChat_12 = value;
		Il2CppCodeGenWriteBarrier((&___InputFieldChat_12), value);
	}

	inline static int32_t get_offset_of_CurrentChannelText_13() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___CurrentChannelText_13)); }
	inline Text_t1901882714 * get_CurrentChannelText_13() const { return ___CurrentChannelText_13; }
	inline Text_t1901882714 ** get_address_of_CurrentChannelText_13() { return &___CurrentChannelText_13; }
	inline void set_CurrentChannelText_13(Text_t1901882714 * value)
	{
		___CurrentChannelText_13 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentChannelText_13), value);
	}

	inline static int32_t get_offset_of_ChannelToggleToInstantiate_14() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ChannelToggleToInstantiate_14)); }
	inline Toggle_t2735377061 * get_ChannelToggleToInstantiate_14() const { return ___ChannelToggleToInstantiate_14; }
	inline Toggle_t2735377061 ** get_address_of_ChannelToggleToInstantiate_14() { return &___ChannelToggleToInstantiate_14; }
	inline void set_ChannelToggleToInstantiate_14(Toggle_t2735377061 * value)
	{
		___ChannelToggleToInstantiate_14 = value;
		Il2CppCodeGenWriteBarrier((&___ChannelToggleToInstantiate_14), value);
	}

	inline static int32_t get_offset_of_FriendListUiItemtoInstantiate_15() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___FriendListUiItemtoInstantiate_15)); }
	inline GameObject_t1113636619 * get_FriendListUiItemtoInstantiate_15() const { return ___FriendListUiItemtoInstantiate_15; }
	inline GameObject_t1113636619 ** get_address_of_FriendListUiItemtoInstantiate_15() { return &___FriendListUiItemtoInstantiate_15; }
	inline void set_FriendListUiItemtoInstantiate_15(GameObject_t1113636619 * value)
	{
		___FriendListUiItemtoInstantiate_15 = value;
		Il2CppCodeGenWriteBarrier((&___FriendListUiItemtoInstantiate_15), value);
	}

	inline static int32_t get_offset_of_channelToggles_16() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___channelToggles_16)); }
	inline Dictionary_2_t2520633360 * get_channelToggles_16() const { return ___channelToggles_16; }
	inline Dictionary_2_t2520633360 ** get_address_of_channelToggles_16() { return &___channelToggles_16; }
	inline void set_channelToggles_16(Dictionary_2_t2520633360 * value)
	{
		___channelToggles_16 = value;
		Il2CppCodeGenWriteBarrier((&___channelToggles_16), value);
	}

	inline static int32_t get_offset_of_friendListItemLUT_17() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___friendListItemLUT_17)); }
	inline Dictionary_2_t2701966392 * get_friendListItemLUT_17() const { return ___friendListItemLUT_17; }
	inline Dictionary_2_t2701966392 ** get_address_of_friendListItemLUT_17() { return &___friendListItemLUT_17; }
	inline void set_friendListItemLUT_17(Dictionary_2_t2701966392 * value)
	{
		___friendListItemLUT_17 = value;
		Il2CppCodeGenWriteBarrier((&___friendListItemLUT_17), value);
	}

	inline static int32_t get_offset_of_ShowState_18() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ShowState_18)); }
	inline bool get_ShowState_18() const { return ___ShowState_18; }
	inline bool* get_address_of_ShowState_18() { return &___ShowState_18; }
	inline void set_ShowState_18(bool value)
	{
		___ShowState_18 = value;
	}

	inline static int32_t get_offset_of_Title_19() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___Title_19)); }
	inline GameObject_t1113636619 * get_Title_19() const { return ___Title_19; }
	inline GameObject_t1113636619 ** get_address_of_Title_19() { return &___Title_19; }
	inline void set_Title_19(GameObject_t1113636619 * value)
	{
		___Title_19 = value;
		Il2CppCodeGenWriteBarrier((&___Title_19), value);
	}

	inline static int32_t get_offset_of_StateText_20() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___StateText_20)); }
	inline Text_t1901882714 * get_StateText_20() const { return ___StateText_20; }
	inline Text_t1901882714 ** get_address_of_StateText_20() { return &___StateText_20; }
	inline void set_StateText_20(Text_t1901882714 * value)
	{
		___StateText_20 = value;
		Il2CppCodeGenWriteBarrier((&___StateText_20), value);
	}

	inline static int32_t get_offset_of_UserIdText_21() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___UserIdText_21)); }
	inline Text_t1901882714 * get_UserIdText_21() const { return ___UserIdText_21; }
	inline Text_t1901882714 ** get_address_of_UserIdText_21() { return &___UserIdText_21; }
	inline void set_UserIdText_21(Text_t1901882714 * value)
	{
		___UserIdText_21 = value;
		Il2CppCodeGenWriteBarrier((&___UserIdText_21), value);
	}

	inline static int32_t get_offset_of_TestLength_23() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___TestLength_23)); }
	inline int32_t get_TestLength_23() const { return ___TestLength_23; }
	inline int32_t* get_address_of_TestLength_23() { return &___TestLength_23; }
	inline void set_TestLength_23(int32_t value)
	{
		___TestLength_23 = value;
	}

	inline static int32_t get_offset_of_testBytes_24() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___testBytes_24)); }
	inline ByteU5BU5D_t4116647657* get_testBytes_24() const { return ___testBytes_24; }
	inline ByteU5BU5D_t4116647657** get_address_of_testBytes_24() { return &___testBytes_24; }
	inline void set_testBytes_24(ByteU5BU5D_t4116647657* value)
	{
		___testBytes_24 = value;
		Il2CppCodeGenWriteBarrier((&___testBytes_24), value);
	}
};

struct ChatGui_t3673337520_StaticFields
{
public:
	// System.String ChatGui::HelpText
	String_t* ___HelpText_22;

public:
	inline static int32_t get_offset_of_HelpText_22() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520_StaticFields, ___HelpText_22)); }
	inline String_t* get_HelpText_22() const { return ___HelpText_22; }
	inline String_t** get_address_of_HelpText_22() { return &___HelpText_22; }
	inline void set_HelpText_22(String_t* value)
	{
		___HelpText_22 = value;
		Il2CppCodeGenWriteBarrier((&___HelpText_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATGUI_T3673337520_H
#ifndef FRIENDITEM_T2916710093_H
#define FRIENDITEM_T2916710093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendItem
struct  FriendItem_t2916710093  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text FriendItem::NameLabel
	Text_t1901882714 * ___NameLabel_2;
	// UnityEngine.UI.Text FriendItem::StatusLabel
	Text_t1901882714 * ___StatusLabel_3;
	// UnityEngine.UI.Text FriendItem::Health
	Text_t1901882714 * ___Health_4;

public:
	inline static int32_t get_offset_of_NameLabel_2() { return static_cast<int32_t>(offsetof(FriendItem_t2916710093, ___NameLabel_2)); }
	inline Text_t1901882714 * get_NameLabel_2() const { return ___NameLabel_2; }
	inline Text_t1901882714 ** get_address_of_NameLabel_2() { return &___NameLabel_2; }
	inline void set_NameLabel_2(Text_t1901882714 * value)
	{
		___NameLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&___NameLabel_2), value);
	}

	inline static int32_t get_offset_of_StatusLabel_3() { return static_cast<int32_t>(offsetof(FriendItem_t2916710093, ___StatusLabel_3)); }
	inline Text_t1901882714 * get_StatusLabel_3() const { return ___StatusLabel_3; }
	inline Text_t1901882714 ** get_address_of_StatusLabel_3() { return &___StatusLabel_3; }
	inline void set_StatusLabel_3(Text_t1901882714 * value)
	{
		___StatusLabel_3 = value;
		Il2CppCodeGenWriteBarrier((&___StatusLabel_3), value);
	}

	inline static int32_t get_offset_of_Health_4() { return static_cast<int32_t>(offsetof(FriendItem_t2916710093, ___Health_4)); }
	inline Text_t1901882714 * get_Health_4() const { return ___Health_4; }
	inline Text_t1901882714 ** get_address_of_Health_4() { return &___Health_4; }
	inline void set_Health_4(Text_t1901882714 * value)
	{
		___Health_4 = value;
		Il2CppCodeGenWriteBarrier((&___Health_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRIENDITEM_T2916710093_H
#ifndef IGNOREUIRAYCASTWHENINACTIVE_T3624510311_H
#define IGNOREUIRAYCASTWHENINACTIVE_T3624510311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IgnoreUiRaycastWhenInactive
struct  IgnoreUiRaycastWhenInactive_t3624510311  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNOREUIRAYCASTWHENINACTIVE_T3624510311_H
#ifndef NAMEPICKGUI_T2955268898_H
#define NAMEPICKGUI_T2955268898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NamePickGui
struct  NamePickGui_t2955268898  : public MonoBehaviour_t3962482529
{
public:
	// ChatGui NamePickGui::chatNewComponent
	ChatGui_t3673337520 * ___chatNewComponent_3;
	// UnityEngine.UI.InputField NamePickGui::idInput
	InputField_t3762917431 * ___idInput_4;

public:
	inline static int32_t get_offset_of_chatNewComponent_3() { return static_cast<int32_t>(offsetof(NamePickGui_t2955268898, ___chatNewComponent_3)); }
	inline ChatGui_t3673337520 * get_chatNewComponent_3() const { return ___chatNewComponent_3; }
	inline ChatGui_t3673337520 ** get_address_of_chatNewComponent_3() { return &___chatNewComponent_3; }
	inline void set_chatNewComponent_3(ChatGui_t3673337520 * value)
	{
		___chatNewComponent_3 = value;
		Il2CppCodeGenWriteBarrier((&___chatNewComponent_3), value);
	}

	inline static int32_t get_offset_of_idInput_4() { return static_cast<int32_t>(offsetof(NamePickGui_t2955268898, ___idInput_4)); }
	inline InputField_t3762917431 * get_idInput_4() const { return ___idInput_4; }
	inline InputField_t3762917431 ** get_address_of_idInput_4() { return &___idInput_4; }
	inline void set_idInput_4(InputField_t3762917431 * value)
	{
		___idInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___idInput_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEPICKGUI_T2955268898_H
#ifndef GUICUSTOMAUTH_T2390915668_H
#define GUICUSTOMAUTH_T2390915668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUICustomAuth
struct  GUICustomAuth_t2390915668  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rect GUICustomAuth::GuiRect
	Rect_t2360479859  ___GuiRect_2;
	// System.String GUICustomAuth::authName
	String_t* ___authName_3;
	// System.String GUICustomAuth::authToken
	String_t* ___authToken_4;
	// System.String GUICustomAuth::authDebugMessage
	String_t* ___authDebugMessage_5;
	// GUICustomAuth/GuiState GUICustomAuth::guiState
	int32_t ___guiState_6;
	// UnityEngine.GameObject GUICustomAuth::RootOf3dButtons
	GameObject_t1113636619 * ___RootOf3dButtons_7;

public:
	inline static int32_t get_offset_of_GuiRect_2() { return static_cast<int32_t>(offsetof(GUICustomAuth_t2390915668, ___GuiRect_2)); }
	inline Rect_t2360479859  get_GuiRect_2() const { return ___GuiRect_2; }
	inline Rect_t2360479859 * get_address_of_GuiRect_2() { return &___GuiRect_2; }
	inline void set_GuiRect_2(Rect_t2360479859  value)
	{
		___GuiRect_2 = value;
	}

	inline static int32_t get_offset_of_authName_3() { return static_cast<int32_t>(offsetof(GUICustomAuth_t2390915668, ___authName_3)); }
	inline String_t* get_authName_3() const { return ___authName_3; }
	inline String_t** get_address_of_authName_3() { return &___authName_3; }
	inline void set_authName_3(String_t* value)
	{
		___authName_3 = value;
		Il2CppCodeGenWriteBarrier((&___authName_3), value);
	}

	inline static int32_t get_offset_of_authToken_4() { return static_cast<int32_t>(offsetof(GUICustomAuth_t2390915668, ___authToken_4)); }
	inline String_t* get_authToken_4() const { return ___authToken_4; }
	inline String_t** get_address_of_authToken_4() { return &___authToken_4; }
	inline void set_authToken_4(String_t* value)
	{
		___authToken_4 = value;
		Il2CppCodeGenWriteBarrier((&___authToken_4), value);
	}

	inline static int32_t get_offset_of_authDebugMessage_5() { return static_cast<int32_t>(offsetof(GUICustomAuth_t2390915668, ___authDebugMessage_5)); }
	inline String_t* get_authDebugMessage_5() const { return ___authDebugMessage_5; }
	inline String_t** get_address_of_authDebugMessage_5() { return &___authDebugMessage_5; }
	inline void set_authDebugMessage_5(String_t* value)
	{
		___authDebugMessage_5 = value;
		Il2CppCodeGenWriteBarrier((&___authDebugMessage_5), value);
	}

	inline static int32_t get_offset_of_guiState_6() { return static_cast<int32_t>(offsetof(GUICustomAuth_t2390915668, ___guiState_6)); }
	inline int32_t get_guiState_6() const { return ___guiState_6; }
	inline int32_t* get_address_of_guiState_6() { return &___guiState_6; }
	inline void set_guiState_6(int32_t value)
	{
		___guiState_6 = value;
	}

	inline static int32_t get_offset_of_RootOf3dButtons_7() { return static_cast<int32_t>(offsetof(GUICustomAuth_t2390915668, ___RootOf3dButtons_7)); }
	inline GameObject_t1113636619 * get_RootOf3dButtons_7() const { return ___RootOf3dButtons_7; }
	inline GameObject_t1113636619 ** get_address_of_RootOf3dButtons_7() { return &___RootOf3dButtons_7; }
	inline void set_RootOf3dButtons_7(GameObject_t1113636619 * value)
	{
		___RootOf3dButtons_7 = value;
		Il2CppCodeGenWriteBarrier((&___RootOf3dButtons_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUICUSTOMAUTH_T2390915668_H
#ifndef TOHUBBUTTON_T3593506886_H
#define TOHUBBUTTON_T3593506886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToHubButton
struct  ToHubButton_t3593506886  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D ToHubButton::ButtonTexture
	Texture2D_t3840446185 * ___ButtonTexture_2;
	// UnityEngine.Rect ToHubButton::ButtonRect
	Rect_t2360479859  ___ButtonRect_3;

public:
	inline static int32_t get_offset_of_ButtonTexture_2() { return static_cast<int32_t>(offsetof(ToHubButton_t3593506886, ___ButtonTexture_2)); }
	inline Texture2D_t3840446185 * get_ButtonTexture_2() const { return ___ButtonTexture_2; }
	inline Texture2D_t3840446185 ** get_address_of_ButtonTexture_2() { return &___ButtonTexture_2; }
	inline void set_ButtonTexture_2(Texture2D_t3840446185 * value)
	{
		___ButtonTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonTexture_2), value);
	}

	inline static int32_t get_offset_of_ButtonRect_3() { return static_cast<int32_t>(offsetof(ToHubButton_t3593506886, ___ButtonRect_3)); }
	inline Rect_t2360479859  get_ButtonRect_3() const { return ___ButtonRect_3; }
	inline Rect_t2360479859 * get_address_of_ButtonRect_3() { return &___ButtonRect_3; }
	inline void set_ButtonRect_3(Rect_t2360479859  value)
	{
		___ButtonRect_3 = value;
	}
};

struct ToHubButton_t3593506886_StaticFields
{
public:
	// ToHubButton ToHubButton::instance
	ToHubButton_t3593506886 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(ToHubButton_t3593506886_StaticFields, ___instance_4)); }
	inline ToHubButton_t3593506886 * get_instance_4() const { return ___instance_4; }
	inline ToHubButton_t3593506886 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(ToHubButton_t3593506886 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOHUBBUTTON_T3593506886_H
#ifndef PICKUPTRIGGERFORWARD_T166635472_H
#define PICKUPTRIGGERFORWARD_T166635472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickupTriggerForward
struct  PickupTriggerForward_t166635472  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUPTRIGGERFORWARD_T166635472_H
#ifndef ONCLICKDISABLEOBJ_T3071582932_H
#define ONCLICKDISABLEOBJ_T3071582932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickDisableObj
struct  OnClickDisableObj_t3071582932  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKDISABLEOBJ_T3071582932_H
#ifndef DEMO2DJUMPANDRUN_T894816729_H
#define DEMO2DJUMPANDRUN_T894816729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Demo2DJumpAndRun
struct  Demo2DJumpAndRun_t894816729  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMO2DJUMPANDRUN_T894816729_H
#ifndef RPSDEBUG_T2473089555_H
#define RPSDEBUG_T2473089555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsDebug
struct  RpsDebug_t2473089555  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button RpsDebug::ConnectionDebugButton
	Button_t4055032469 * ___ConnectionDebugButton_2;
	// System.Boolean RpsDebug::ShowConnectionDebug
	bool ___ShowConnectionDebug_3;

public:
	inline static int32_t get_offset_of_ConnectionDebugButton_2() { return static_cast<int32_t>(offsetof(RpsDebug_t2473089555, ___ConnectionDebugButton_2)); }
	inline Button_t4055032469 * get_ConnectionDebugButton_2() const { return ___ConnectionDebugButton_2; }
	inline Button_t4055032469 ** get_address_of_ConnectionDebugButton_2() { return &___ConnectionDebugButton_2; }
	inline void set_ConnectionDebugButton_2(Button_t4055032469 * value)
	{
		___ConnectionDebugButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectionDebugButton_2), value);
	}

	inline static int32_t get_offset_of_ShowConnectionDebug_3() { return static_cast<int32_t>(offsetof(RpsDebug_t2473089555, ___ShowConnectionDebug_3)); }
	inline bool get_ShowConnectionDebug_3() const { return ___ShowConnectionDebug_3; }
	inline bool* get_address_of_ShowConnectionDebug_3() { return &___ShowConnectionDebug_3; }
	inline void set_ShowConnectionDebug_3(bool value)
	{
		___ShowConnectionDebug_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPSDEBUG_T2473089555_H
#ifndef DEMORPGMOVEMENT_T3459243107_H
#define DEMORPGMOVEMENT_T3459243107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoRPGMovement
struct  DemoRPGMovement_t3459243107  : public MonoBehaviour_t3962482529
{
public:
	// RPGCamera DemoRPGMovement::Camera
	RPGCamera_t2077525570 * ___Camera_2;

public:
	inline static int32_t get_offset_of_Camera_2() { return static_cast<int32_t>(offsetof(DemoRPGMovement_t3459243107, ___Camera_2)); }
	inline RPGCamera_t2077525570 * get_Camera_2() const { return ___Camera_2; }
	inline RPGCamera_t2077525570 ** get_address_of_Camera_2() { return &___Camera_2; }
	inline void set_Camera_2(RPGCamera_t2077525570 * value)
	{
		___Camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMORPGMOVEMENT_T3459243107_H
#ifndef RPGCAMERA_T2077525570_H
#define RPGCAMERA_T2077525570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCamera
struct  RPGCamera_t2077525570  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform RPGCamera::Target
	Transform_t3600365921 * ___Target_2;
	// System.Single RPGCamera::MaximumDistance
	float ___MaximumDistance_3;
	// System.Single RPGCamera::MinimumDistance
	float ___MinimumDistance_4;
	// System.Single RPGCamera::ScrollModifier
	float ___ScrollModifier_5;
	// System.Single RPGCamera::TurnModifier
	float ___TurnModifier_6;
	// UnityEngine.Transform RPGCamera::m_CameraTransform
	Transform_t3600365921 * ___m_CameraTransform_7;
	// UnityEngine.Vector3 RPGCamera::m_LookAtPoint
	Vector3_t3722313464  ___m_LookAtPoint_8;
	// UnityEngine.Vector3 RPGCamera::m_LocalForwardVector
	Vector3_t3722313464  ___m_LocalForwardVector_9;
	// System.Single RPGCamera::m_Distance
	float ___m_Distance_10;

public:
	inline static int32_t get_offset_of_Target_2() { return static_cast<int32_t>(offsetof(RPGCamera_t2077525570, ___Target_2)); }
	inline Transform_t3600365921 * get_Target_2() const { return ___Target_2; }
	inline Transform_t3600365921 ** get_address_of_Target_2() { return &___Target_2; }
	inline void set_Target_2(Transform_t3600365921 * value)
	{
		___Target_2 = value;
		Il2CppCodeGenWriteBarrier((&___Target_2), value);
	}

	inline static int32_t get_offset_of_MaximumDistance_3() { return static_cast<int32_t>(offsetof(RPGCamera_t2077525570, ___MaximumDistance_3)); }
	inline float get_MaximumDistance_3() const { return ___MaximumDistance_3; }
	inline float* get_address_of_MaximumDistance_3() { return &___MaximumDistance_3; }
	inline void set_MaximumDistance_3(float value)
	{
		___MaximumDistance_3 = value;
	}

	inline static int32_t get_offset_of_MinimumDistance_4() { return static_cast<int32_t>(offsetof(RPGCamera_t2077525570, ___MinimumDistance_4)); }
	inline float get_MinimumDistance_4() const { return ___MinimumDistance_4; }
	inline float* get_address_of_MinimumDistance_4() { return &___MinimumDistance_4; }
	inline void set_MinimumDistance_4(float value)
	{
		___MinimumDistance_4 = value;
	}

	inline static int32_t get_offset_of_ScrollModifier_5() { return static_cast<int32_t>(offsetof(RPGCamera_t2077525570, ___ScrollModifier_5)); }
	inline float get_ScrollModifier_5() const { return ___ScrollModifier_5; }
	inline float* get_address_of_ScrollModifier_5() { return &___ScrollModifier_5; }
	inline void set_ScrollModifier_5(float value)
	{
		___ScrollModifier_5 = value;
	}

	inline static int32_t get_offset_of_TurnModifier_6() { return static_cast<int32_t>(offsetof(RPGCamera_t2077525570, ___TurnModifier_6)); }
	inline float get_TurnModifier_6() const { return ___TurnModifier_6; }
	inline float* get_address_of_TurnModifier_6() { return &___TurnModifier_6; }
	inline void set_TurnModifier_6(float value)
	{
		___TurnModifier_6 = value;
	}

	inline static int32_t get_offset_of_m_CameraTransform_7() { return static_cast<int32_t>(offsetof(RPGCamera_t2077525570, ___m_CameraTransform_7)); }
	inline Transform_t3600365921 * get_m_CameraTransform_7() const { return ___m_CameraTransform_7; }
	inline Transform_t3600365921 ** get_address_of_m_CameraTransform_7() { return &___m_CameraTransform_7; }
	inline void set_m_CameraTransform_7(Transform_t3600365921 * value)
	{
		___m_CameraTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraTransform_7), value);
	}

	inline static int32_t get_offset_of_m_LookAtPoint_8() { return static_cast<int32_t>(offsetof(RPGCamera_t2077525570, ___m_LookAtPoint_8)); }
	inline Vector3_t3722313464  get_m_LookAtPoint_8() const { return ___m_LookAtPoint_8; }
	inline Vector3_t3722313464 * get_address_of_m_LookAtPoint_8() { return &___m_LookAtPoint_8; }
	inline void set_m_LookAtPoint_8(Vector3_t3722313464  value)
	{
		___m_LookAtPoint_8 = value;
	}

	inline static int32_t get_offset_of_m_LocalForwardVector_9() { return static_cast<int32_t>(offsetof(RPGCamera_t2077525570, ___m_LocalForwardVector_9)); }
	inline Vector3_t3722313464  get_m_LocalForwardVector_9() const { return ___m_LocalForwardVector_9; }
	inline Vector3_t3722313464 * get_address_of_m_LocalForwardVector_9() { return &___m_LocalForwardVector_9; }
	inline void set_m_LocalForwardVector_9(Vector3_t3722313464  value)
	{
		___m_LocalForwardVector_9 = value;
	}

	inline static int32_t get_offset_of_m_Distance_10() { return static_cast<int32_t>(offsetof(RPGCamera_t2077525570, ___m_Distance_10)); }
	inline float get_m_Distance_10() const { return ___m_Distance_10; }
	inline float* get_address_of_m_Distance_10() { return &___m_Distance_10; }
	inline void set_m_Distance_10(float value)
	{
		___m_Distance_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPGCAMERA_T2077525570_H
#ifndef RPGMOVEMENT_T3706718442_H
#define RPGMOVEMENT_T3706718442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGMovement
struct  RPGMovement_t3706718442  : public MonoBehaviour_t3962482529
{
public:
	// System.Single RPGMovement::ForwardSpeed
	float ___ForwardSpeed_2;
	// System.Single RPGMovement::BackwardSpeed
	float ___BackwardSpeed_3;
	// System.Single RPGMovement::StrafeSpeed
	float ___StrafeSpeed_4;
	// System.Single RPGMovement::RotateSpeed
	float ___RotateSpeed_5;
	// UnityEngine.CharacterController RPGMovement::m_CharacterController
	CharacterController_t1138636865 * ___m_CharacterController_6;
	// UnityEngine.Vector3 RPGMovement::m_LastPosition
	Vector3_t3722313464  ___m_LastPosition_7;
	// UnityEngine.Animator RPGMovement::m_Animator
	Animator_t434523843 * ___m_Animator_8;
	// PhotonView RPGMovement::m_PhotonView
	PhotonView_t2207721820 * ___m_PhotonView_9;
	// PhotonTransformView RPGMovement::m_TransformView
	PhotonTransformView_t372465615 * ___m_TransformView_10;
	// System.Single RPGMovement::m_AnimatorSpeed
	float ___m_AnimatorSpeed_11;
	// UnityEngine.Vector3 RPGMovement::m_CurrentMovement
	Vector3_t3722313464  ___m_CurrentMovement_12;
	// System.Single RPGMovement::m_CurrentTurnSpeed
	float ___m_CurrentTurnSpeed_13;

public:
	inline static int32_t get_offset_of_ForwardSpeed_2() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___ForwardSpeed_2)); }
	inline float get_ForwardSpeed_2() const { return ___ForwardSpeed_2; }
	inline float* get_address_of_ForwardSpeed_2() { return &___ForwardSpeed_2; }
	inline void set_ForwardSpeed_2(float value)
	{
		___ForwardSpeed_2 = value;
	}

	inline static int32_t get_offset_of_BackwardSpeed_3() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___BackwardSpeed_3)); }
	inline float get_BackwardSpeed_3() const { return ___BackwardSpeed_3; }
	inline float* get_address_of_BackwardSpeed_3() { return &___BackwardSpeed_3; }
	inline void set_BackwardSpeed_3(float value)
	{
		___BackwardSpeed_3 = value;
	}

	inline static int32_t get_offset_of_StrafeSpeed_4() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___StrafeSpeed_4)); }
	inline float get_StrafeSpeed_4() const { return ___StrafeSpeed_4; }
	inline float* get_address_of_StrafeSpeed_4() { return &___StrafeSpeed_4; }
	inline void set_StrafeSpeed_4(float value)
	{
		___StrafeSpeed_4 = value;
	}

	inline static int32_t get_offset_of_RotateSpeed_5() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___RotateSpeed_5)); }
	inline float get_RotateSpeed_5() const { return ___RotateSpeed_5; }
	inline float* get_address_of_RotateSpeed_5() { return &___RotateSpeed_5; }
	inline void set_RotateSpeed_5(float value)
	{
		___RotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_CharacterController_6() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___m_CharacterController_6)); }
	inline CharacterController_t1138636865 * get_m_CharacterController_6() const { return ___m_CharacterController_6; }
	inline CharacterController_t1138636865 ** get_address_of_m_CharacterController_6() { return &___m_CharacterController_6; }
	inline void set_m_CharacterController_6(CharacterController_t1138636865 * value)
	{
		___m_CharacterController_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharacterController_6), value);
	}

	inline static int32_t get_offset_of_m_LastPosition_7() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___m_LastPosition_7)); }
	inline Vector3_t3722313464  get_m_LastPosition_7() const { return ___m_LastPosition_7; }
	inline Vector3_t3722313464 * get_address_of_m_LastPosition_7() { return &___m_LastPosition_7; }
	inline void set_m_LastPosition_7(Vector3_t3722313464  value)
	{
		___m_LastPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Animator_8() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___m_Animator_8)); }
	inline Animator_t434523843 * get_m_Animator_8() const { return ___m_Animator_8; }
	inline Animator_t434523843 ** get_address_of_m_Animator_8() { return &___m_Animator_8; }
	inline void set_m_Animator_8(Animator_t434523843 * value)
	{
		___m_Animator_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_8), value);
	}

	inline static int32_t get_offset_of_m_PhotonView_9() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___m_PhotonView_9)); }
	inline PhotonView_t2207721820 * get_m_PhotonView_9() const { return ___m_PhotonView_9; }
	inline PhotonView_t2207721820 ** get_address_of_m_PhotonView_9() { return &___m_PhotonView_9; }
	inline void set_m_PhotonView_9(PhotonView_t2207721820 * value)
	{
		___m_PhotonView_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_PhotonView_9), value);
	}

	inline static int32_t get_offset_of_m_TransformView_10() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___m_TransformView_10)); }
	inline PhotonTransformView_t372465615 * get_m_TransformView_10() const { return ___m_TransformView_10; }
	inline PhotonTransformView_t372465615 ** get_address_of_m_TransformView_10() { return &___m_TransformView_10; }
	inline void set_m_TransformView_10(PhotonTransformView_t372465615 * value)
	{
		___m_TransformView_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TransformView_10), value);
	}

	inline static int32_t get_offset_of_m_AnimatorSpeed_11() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___m_AnimatorSpeed_11)); }
	inline float get_m_AnimatorSpeed_11() const { return ___m_AnimatorSpeed_11; }
	inline float* get_address_of_m_AnimatorSpeed_11() { return &___m_AnimatorSpeed_11; }
	inline void set_m_AnimatorSpeed_11(float value)
	{
		___m_AnimatorSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_CurrentMovement_12() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___m_CurrentMovement_12)); }
	inline Vector3_t3722313464  get_m_CurrentMovement_12() const { return ___m_CurrentMovement_12; }
	inline Vector3_t3722313464 * get_address_of_m_CurrentMovement_12() { return &___m_CurrentMovement_12; }
	inline void set_m_CurrentMovement_12(Vector3_t3722313464  value)
	{
		___m_CurrentMovement_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentTurnSpeed_13() { return static_cast<int32_t>(offsetof(RPGMovement_t3706718442, ___m_CurrentTurnSpeed_13)); }
	inline float get_m_CurrentTurnSpeed_13() const { return ___m_CurrentTurnSpeed_13; }
	inline float* get_address_of_m_CurrentTurnSpeed_13() { return &___m_CurrentTurnSpeed_13; }
	inline void set_m_CurrentTurnSpeed_13(float value)
	{
		___m_CurrentTurnSpeed_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPGMOVEMENT_T3706718442_H
#ifndef INSTANTIATECUBE_T1661801423_H
#define INSTANTIATECUBE_T1661801423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantiateCube
struct  InstantiateCube_t1661801423  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject InstantiateCube::Prefab
	GameObject_t1113636619 * ___Prefab_2;
	// System.Int32 InstantiateCube::InstantiateType
	int32_t ___InstantiateType_3;
	// System.Boolean InstantiateCube::showGui
	bool ___showGui_4;

public:
	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(InstantiateCube_t1661801423, ___Prefab_2)); }
	inline GameObject_t1113636619 * get_Prefab_2() const { return ___Prefab_2; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(GameObject_t1113636619 * value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_InstantiateType_3() { return static_cast<int32_t>(offsetof(InstantiateCube_t1661801423, ___InstantiateType_3)); }
	inline int32_t get_InstantiateType_3() const { return ___InstantiateType_3; }
	inline int32_t* get_address_of_InstantiateType_3() { return &___InstantiateType_3; }
	inline void set_InstantiateType_3(int32_t value)
	{
		___InstantiateType_3 = value;
	}

	inline static int32_t get_offset_of_showGui_4() { return static_cast<int32_t>(offsetof(InstantiateCube_t1661801423, ___showGui_4)); }
	inline bool get_showGui_4() const { return ___showGui_4; }
	inline bool* get_address_of_showGui_4() { return &___showGui_4; }
	inline void set_showGui_4(bool value)
	{
		___showGui_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTIATECUBE_T1661801423_H
#ifndef PICKUPDEMOGUI_T2879317730_H
#define PICKUPDEMOGUI_T2879317730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickupDemoGui
struct  PickupDemoGui_t2879317730  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PickupDemoGui::ShowScores
	bool ___ShowScores_2;
	// System.Boolean PickupDemoGui::ShowDropButton
	bool ___ShowDropButton_3;
	// System.Boolean PickupDemoGui::ShowTeams
	bool ___ShowTeams_4;
	// System.Single PickupDemoGui::DropOffset
	float ___DropOffset_5;

public:
	inline static int32_t get_offset_of_ShowScores_2() { return static_cast<int32_t>(offsetof(PickupDemoGui_t2879317730, ___ShowScores_2)); }
	inline bool get_ShowScores_2() const { return ___ShowScores_2; }
	inline bool* get_address_of_ShowScores_2() { return &___ShowScores_2; }
	inline void set_ShowScores_2(bool value)
	{
		___ShowScores_2 = value;
	}

	inline static int32_t get_offset_of_ShowDropButton_3() { return static_cast<int32_t>(offsetof(PickupDemoGui_t2879317730, ___ShowDropButton_3)); }
	inline bool get_ShowDropButton_3() const { return ___ShowDropButton_3; }
	inline bool* get_address_of_ShowDropButton_3() { return &___ShowDropButton_3; }
	inline void set_ShowDropButton_3(bool value)
	{
		___ShowDropButton_3 = value;
	}

	inline static int32_t get_offset_of_ShowTeams_4() { return static_cast<int32_t>(offsetof(PickupDemoGui_t2879317730, ___ShowTeams_4)); }
	inline bool get_ShowTeams_4() const { return ___ShowTeams_4; }
	inline bool* get_address_of_ShowTeams_4() { return &___ShowTeams_4; }
	inline void set_ShowTeams_4(bool value)
	{
		___ShowTeams_4 = value;
	}

	inline static int32_t get_offset_of_DropOffset_5() { return static_cast<int32_t>(offsetof(PickupDemoGui_t2879317730, ___DropOffset_5)); }
	inline float get_DropOffset_5() const { return ___DropOffset_5; }
	inline float* get_address_of_DropOffset_5() { return &___DropOffset_5; }
	inline void set_DropOffset_5(float value)
	{
		___DropOffset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUPDEMOGUI_T2879317730_H
#ifndef MESSAGEOVERLAY_T775660691_H
#define MESSAGEOVERLAY_T775660691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageOverlay
struct  MessageOverlay_t775660691  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] MessageOverlay::Objects
	GameObjectU5BU5D_t3328599146* ___Objects_2;

public:
	inline static int32_t get_offset_of_Objects_2() { return static_cast<int32_t>(offsetof(MessageOverlay_t775660691, ___Objects_2)); }
	inline GameObjectU5BU5D_t3328599146* get_Objects_2() const { return ___Objects_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Objects_2() { return &___Objects_2; }
	inline void set_Objects_2(GameObjectU5BU5D_t3328599146* value)
	{
		___Objects_2 = value;
		Il2CppCodeGenWriteBarrier((&___Objects_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEOVERLAY_T775660691_H
#ifndef PICKUPCONTROLLER_T2983846149_H
#define PICKUPCONTROLLER_T2983846149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickupController
struct  PickupController_t2983846149  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AnimationClip PickupController::idleAnimation
	AnimationClip_t2318505987 * ___idleAnimation_2;
	// UnityEngine.AnimationClip PickupController::walkAnimation
	AnimationClip_t2318505987 * ___walkAnimation_3;
	// UnityEngine.AnimationClip PickupController::runAnimation
	AnimationClip_t2318505987 * ___runAnimation_4;
	// UnityEngine.AnimationClip PickupController::jumpPoseAnimation
	AnimationClip_t2318505987 * ___jumpPoseAnimation_5;
	// System.Single PickupController::walkMaxAnimationSpeed
	float ___walkMaxAnimationSpeed_6;
	// System.Single PickupController::trotMaxAnimationSpeed
	float ___trotMaxAnimationSpeed_7;
	// System.Single PickupController::runMaxAnimationSpeed
	float ___runMaxAnimationSpeed_8;
	// System.Single PickupController::jumpAnimationSpeed
	float ___jumpAnimationSpeed_9;
	// System.Single PickupController::landAnimationSpeed
	float ___landAnimationSpeed_10;
	// UnityEngine.Animation PickupController::_animation
	Animation_t3648466861 * ____animation_11;
	// PickupCharacterState PickupController::_characterState
	int32_t ____characterState_12;
	// System.Single PickupController::walkSpeed
	float ___walkSpeed_13;
	// System.Single PickupController::trotSpeed
	float ___trotSpeed_14;
	// System.Single PickupController::runSpeed
	float ___runSpeed_15;
	// System.Single PickupController::inAirControlAcceleration
	float ___inAirControlAcceleration_16;
	// System.Single PickupController::jumpHeight
	float ___jumpHeight_17;
	// System.Single PickupController::gravity
	float ___gravity_18;
	// System.Single PickupController::speedSmoothing
	float ___speedSmoothing_19;
	// System.Single PickupController::rotateSpeed
	float ___rotateSpeed_20;
	// System.Single PickupController::trotAfterSeconds
	float ___trotAfterSeconds_21;
	// System.Boolean PickupController::canJump
	bool ___canJump_22;
	// System.Single PickupController::jumpRepeatTime
	float ___jumpRepeatTime_23;
	// System.Single PickupController::jumpTimeout
	float ___jumpTimeout_24;
	// System.Single PickupController::groundedTimeout
	float ___groundedTimeout_25;
	// System.Single PickupController::lockCameraTimer
	float ___lockCameraTimer_26;
	// UnityEngine.Vector3 PickupController::moveDirection
	Vector3_t3722313464  ___moveDirection_27;
	// System.Single PickupController::verticalSpeed
	float ___verticalSpeed_28;
	// System.Single PickupController::moveSpeed
	float ___moveSpeed_29;
	// UnityEngine.CollisionFlags PickupController::collisionFlags
	int32_t ___collisionFlags_30;
	// System.Boolean PickupController::jumping
	bool ___jumping_31;
	// System.Boolean PickupController::jumpingReachedApex
	bool ___jumpingReachedApex_32;
	// System.Boolean PickupController::movingBack
	bool ___movingBack_33;
	// System.Boolean PickupController::isMoving
	bool ___isMoving_34;
	// System.Single PickupController::walkTimeStart
	float ___walkTimeStart_35;
	// System.Single PickupController::lastJumpButtonTime
	float ___lastJumpButtonTime_36;
	// System.Single PickupController::lastJumpTime
	float ___lastJumpTime_37;
	// UnityEngine.Vector3 PickupController::inAirVelocity
	Vector3_t3722313464  ___inAirVelocity_38;
	// System.Single PickupController::lastGroundedTime
	float ___lastGroundedTime_39;
	// UnityEngine.Vector3 PickupController::velocity
	Vector3_t3722313464  ___velocity_40;
	// UnityEngine.Vector3 PickupController::lastPos
	Vector3_t3722313464  ___lastPos_41;
	// UnityEngine.Vector3 PickupController::remotePosition
	Vector3_t3722313464  ___remotePosition_42;
	// System.Boolean PickupController::isControllable
	bool ___isControllable_43;
	// System.Boolean PickupController::DoRotate
	bool ___DoRotate_44;
	// System.Single PickupController::RemoteSmoothing
	float ___RemoteSmoothing_45;
	// System.Boolean PickupController::AssignAsTagObject
	bool ___AssignAsTagObject_46;

public:
	inline static int32_t get_offset_of_idleAnimation_2() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___idleAnimation_2)); }
	inline AnimationClip_t2318505987 * get_idleAnimation_2() const { return ___idleAnimation_2; }
	inline AnimationClip_t2318505987 ** get_address_of_idleAnimation_2() { return &___idleAnimation_2; }
	inline void set_idleAnimation_2(AnimationClip_t2318505987 * value)
	{
		___idleAnimation_2 = value;
		Il2CppCodeGenWriteBarrier((&___idleAnimation_2), value);
	}

	inline static int32_t get_offset_of_walkAnimation_3() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___walkAnimation_3)); }
	inline AnimationClip_t2318505987 * get_walkAnimation_3() const { return ___walkAnimation_3; }
	inline AnimationClip_t2318505987 ** get_address_of_walkAnimation_3() { return &___walkAnimation_3; }
	inline void set_walkAnimation_3(AnimationClip_t2318505987 * value)
	{
		___walkAnimation_3 = value;
		Il2CppCodeGenWriteBarrier((&___walkAnimation_3), value);
	}

	inline static int32_t get_offset_of_runAnimation_4() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___runAnimation_4)); }
	inline AnimationClip_t2318505987 * get_runAnimation_4() const { return ___runAnimation_4; }
	inline AnimationClip_t2318505987 ** get_address_of_runAnimation_4() { return &___runAnimation_4; }
	inline void set_runAnimation_4(AnimationClip_t2318505987 * value)
	{
		___runAnimation_4 = value;
		Il2CppCodeGenWriteBarrier((&___runAnimation_4), value);
	}

	inline static int32_t get_offset_of_jumpPoseAnimation_5() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___jumpPoseAnimation_5)); }
	inline AnimationClip_t2318505987 * get_jumpPoseAnimation_5() const { return ___jumpPoseAnimation_5; }
	inline AnimationClip_t2318505987 ** get_address_of_jumpPoseAnimation_5() { return &___jumpPoseAnimation_5; }
	inline void set_jumpPoseAnimation_5(AnimationClip_t2318505987 * value)
	{
		___jumpPoseAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___jumpPoseAnimation_5), value);
	}

	inline static int32_t get_offset_of_walkMaxAnimationSpeed_6() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___walkMaxAnimationSpeed_6)); }
	inline float get_walkMaxAnimationSpeed_6() const { return ___walkMaxAnimationSpeed_6; }
	inline float* get_address_of_walkMaxAnimationSpeed_6() { return &___walkMaxAnimationSpeed_6; }
	inline void set_walkMaxAnimationSpeed_6(float value)
	{
		___walkMaxAnimationSpeed_6 = value;
	}

	inline static int32_t get_offset_of_trotMaxAnimationSpeed_7() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___trotMaxAnimationSpeed_7)); }
	inline float get_trotMaxAnimationSpeed_7() const { return ___trotMaxAnimationSpeed_7; }
	inline float* get_address_of_trotMaxAnimationSpeed_7() { return &___trotMaxAnimationSpeed_7; }
	inline void set_trotMaxAnimationSpeed_7(float value)
	{
		___trotMaxAnimationSpeed_7 = value;
	}

	inline static int32_t get_offset_of_runMaxAnimationSpeed_8() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___runMaxAnimationSpeed_8)); }
	inline float get_runMaxAnimationSpeed_8() const { return ___runMaxAnimationSpeed_8; }
	inline float* get_address_of_runMaxAnimationSpeed_8() { return &___runMaxAnimationSpeed_8; }
	inline void set_runMaxAnimationSpeed_8(float value)
	{
		___runMaxAnimationSpeed_8 = value;
	}

	inline static int32_t get_offset_of_jumpAnimationSpeed_9() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___jumpAnimationSpeed_9)); }
	inline float get_jumpAnimationSpeed_9() const { return ___jumpAnimationSpeed_9; }
	inline float* get_address_of_jumpAnimationSpeed_9() { return &___jumpAnimationSpeed_9; }
	inline void set_jumpAnimationSpeed_9(float value)
	{
		___jumpAnimationSpeed_9 = value;
	}

	inline static int32_t get_offset_of_landAnimationSpeed_10() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___landAnimationSpeed_10)); }
	inline float get_landAnimationSpeed_10() const { return ___landAnimationSpeed_10; }
	inline float* get_address_of_landAnimationSpeed_10() { return &___landAnimationSpeed_10; }
	inline void set_landAnimationSpeed_10(float value)
	{
		___landAnimationSpeed_10 = value;
	}

	inline static int32_t get_offset_of__animation_11() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ____animation_11)); }
	inline Animation_t3648466861 * get__animation_11() const { return ____animation_11; }
	inline Animation_t3648466861 ** get_address_of__animation_11() { return &____animation_11; }
	inline void set__animation_11(Animation_t3648466861 * value)
	{
		____animation_11 = value;
		Il2CppCodeGenWriteBarrier((&____animation_11), value);
	}

	inline static int32_t get_offset_of__characterState_12() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ____characterState_12)); }
	inline int32_t get__characterState_12() const { return ____characterState_12; }
	inline int32_t* get_address_of__characterState_12() { return &____characterState_12; }
	inline void set__characterState_12(int32_t value)
	{
		____characterState_12 = value;
	}

	inline static int32_t get_offset_of_walkSpeed_13() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___walkSpeed_13)); }
	inline float get_walkSpeed_13() const { return ___walkSpeed_13; }
	inline float* get_address_of_walkSpeed_13() { return &___walkSpeed_13; }
	inline void set_walkSpeed_13(float value)
	{
		___walkSpeed_13 = value;
	}

	inline static int32_t get_offset_of_trotSpeed_14() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___trotSpeed_14)); }
	inline float get_trotSpeed_14() const { return ___trotSpeed_14; }
	inline float* get_address_of_trotSpeed_14() { return &___trotSpeed_14; }
	inline void set_trotSpeed_14(float value)
	{
		___trotSpeed_14 = value;
	}

	inline static int32_t get_offset_of_runSpeed_15() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___runSpeed_15)); }
	inline float get_runSpeed_15() const { return ___runSpeed_15; }
	inline float* get_address_of_runSpeed_15() { return &___runSpeed_15; }
	inline void set_runSpeed_15(float value)
	{
		___runSpeed_15 = value;
	}

	inline static int32_t get_offset_of_inAirControlAcceleration_16() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___inAirControlAcceleration_16)); }
	inline float get_inAirControlAcceleration_16() const { return ___inAirControlAcceleration_16; }
	inline float* get_address_of_inAirControlAcceleration_16() { return &___inAirControlAcceleration_16; }
	inline void set_inAirControlAcceleration_16(float value)
	{
		___inAirControlAcceleration_16 = value;
	}

	inline static int32_t get_offset_of_jumpHeight_17() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___jumpHeight_17)); }
	inline float get_jumpHeight_17() const { return ___jumpHeight_17; }
	inline float* get_address_of_jumpHeight_17() { return &___jumpHeight_17; }
	inline void set_jumpHeight_17(float value)
	{
		___jumpHeight_17 = value;
	}

	inline static int32_t get_offset_of_gravity_18() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___gravity_18)); }
	inline float get_gravity_18() const { return ___gravity_18; }
	inline float* get_address_of_gravity_18() { return &___gravity_18; }
	inline void set_gravity_18(float value)
	{
		___gravity_18 = value;
	}

	inline static int32_t get_offset_of_speedSmoothing_19() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___speedSmoothing_19)); }
	inline float get_speedSmoothing_19() const { return ___speedSmoothing_19; }
	inline float* get_address_of_speedSmoothing_19() { return &___speedSmoothing_19; }
	inline void set_speedSmoothing_19(float value)
	{
		___speedSmoothing_19 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_20() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___rotateSpeed_20)); }
	inline float get_rotateSpeed_20() const { return ___rotateSpeed_20; }
	inline float* get_address_of_rotateSpeed_20() { return &___rotateSpeed_20; }
	inline void set_rotateSpeed_20(float value)
	{
		___rotateSpeed_20 = value;
	}

	inline static int32_t get_offset_of_trotAfterSeconds_21() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___trotAfterSeconds_21)); }
	inline float get_trotAfterSeconds_21() const { return ___trotAfterSeconds_21; }
	inline float* get_address_of_trotAfterSeconds_21() { return &___trotAfterSeconds_21; }
	inline void set_trotAfterSeconds_21(float value)
	{
		___trotAfterSeconds_21 = value;
	}

	inline static int32_t get_offset_of_canJump_22() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___canJump_22)); }
	inline bool get_canJump_22() const { return ___canJump_22; }
	inline bool* get_address_of_canJump_22() { return &___canJump_22; }
	inline void set_canJump_22(bool value)
	{
		___canJump_22 = value;
	}

	inline static int32_t get_offset_of_jumpRepeatTime_23() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___jumpRepeatTime_23)); }
	inline float get_jumpRepeatTime_23() const { return ___jumpRepeatTime_23; }
	inline float* get_address_of_jumpRepeatTime_23() { return &___jumpRepeatTime_23; }
	inline void set_jumpRepeatTime_23(float value)
	{
		___jumpRepeatTime_23 = value;
	}

	inline static int32_t get_offset_of_jumpTimeout_24() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___jumpTimeout_24)); }
	inline float get_jumpTimeout_24() const { return ___jumpTimeout_24; }
	inline float* get_address_of_jumpTimeout_24() { return &___jumpTimeout_24; }
	inline void set_jumpTimeout_24(float value)
	{
		___jumpTimeout_24 = value;
	}

	inline static int32_t get_offset_of_groundedTimeout_25() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___groundedTimeout_25)); }
	inline float get_groundedTimeout_25() const { return ___groundedTimeout_25; }
	inline float* get_address_of_groundedTimeout_25() { return &___groundedTimeout_25; }
	inline void set_groundedTimeout_25(float value)
	{
		___groundedTimeout_25 = value;
	}

	inline static int32_t get_offset_of_lockCameraTimer_26() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___lockCameraTimer_26)); }
	inline float get_lockCameraTimer_26() const { return ___lockCameraTimer_26; }
	inline float* get_address_of_lockCameraTimer_26() { return &___lockCameraTimer_26; }
	inline void set_lockCameraTimer_26(float value)
	{
		___lockCameraTimer_26 = value;
	}

	inline static int32_t get_offset_of_moveDirection_27() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___moveDirection_27)); }
	inline Vector3_t3722313464  get_moveDirection_27() const { return ___moveDirection_27; }
	inline Vector3_t3722313464 * get_address_of_moveDirection_27() { return &___moveDirection_27; }
	inline void set_moveDirection_27(Vector3_t3722313464  value)
	{
		___moveDirection_27 = value;
	}

	inline static int32_t get_offset_of_verticalSpeed_28() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___verticalSpeed_28)); }
	inline float get_verticalSpeed_28() const { return ___verticalSpeed_28; }
	inline float* get_address_of_verticalSpeed_28() { return &___verticalSpeed_28; }
	inline void set_verticalSpeed_28(float value)
	{
		___verticalSpeed_28 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_29() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___moveSpeed_29)); }
	inline float get_moveSpeed_29() const { return ___moveSpeed_29; }
	inline float* get_address_of_moveSpeed_29() { return &___moveSpeed_29; }
	inline void set_moveSpeed_29(float value)
	{
		___moveSpeed_29 = value;
	}

	inline static int32_t get_offset_of_collisionFlags_30() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___collisionFlags_30)); }
	inline int32_t get_collisionFlags_30() const { return ___collisionFlags_30; }
	inline int32_t* get_address_of_collisionFlags_30() { return &___collisionFlags_30; }
	inline void set_collisionFlags_30(int32_t value)
	{
		___collisionFlags_30 = value;
	}

	inline static int32_t get_offset_of_jumping_31() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___jumping_31)); }
	inline bool get_jumping_31() const { return ___jumping_31; }
	inline bool* get_address_of_jumping_31() { return &___jumping_31; }
	inline void set_jumping_31(bool value)
	{
		___jumping_31 = value;
	}

	inline static int32_t get_offset_of_jumpingReachedApex_32() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___jumpingReachedApex_32)); }
	inline bool get_jumpingReachedApex_32() const { return ___jumpingReachedApex_32; }
	inline bool* get_address_of_jumpingReachedApex_32() { return &___jumpingReachedApex_32; }
	inline void set_jumpingReachedApex_32(bool value)
	{
		___jumpingReachedApex_32 = value;
	}

	inline static int32_t get_offset_of_movingBack_33() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___movingBack_33)); }
	inline bool get_movingBack_33() const { return ___movingBack_33; }
	inline bool* get_address_of_movingBack_33() { return &___movingBack_33; }
	inline void set_movingBack_33(bool value)
	{
		___movingBack_33 = value;
	}

	inline static int32_t get_offset_of_isMoving_34() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___isMoving_34)); }
	inline bool get_isMoving_34() const { return ___isMoving_34; }
	inline bool* get_address_of_isMoving_34() { return &___isMoving_34; }
	inline void set_isMoving_34(bool value)
	{
		___isMoving_34 = value;
	}

	inline static int32_t get_offset_of_walkTimeStart_35() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___walkTimeStart_35)); }
	inline float get_walkTimeStart_35() const { return ___walkTimeStart_35; }
	inline float* get_address_of_walkTimeStart_35() { return &___walkTimeStart_35; }
	inline void set_walkTimeStart_35(float value)
	{
		___walkTimeStart_35 = value;
	}

	inline static int32_t get_offset_of_lastJumpButtonTime_36() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___lastJumpButtonTime_36)); }
	inline float get_lastJumpButtonTime_36() const { return ___lastJumpButtonTime_36; }
	inline float* get_address_of_lastJumpButtonTime_36() { return &___lastJumpButtonTime_36; }
	inline void set_lastJumpButtonTime_36(float value)
	{
		___lastJumpButtonTime_36 = value;
	}

	inline static int32_t get_offset_of_lastJumpTime_37() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___lastJumpTime_37)); }
	inline float get_lastJumpTime_37() const { return ___lastJumpTime_37; }
	inline float* get_address_of_lastJumpTime_37() { return &___lastJumpTime_37; }
	inline void set_lastJumpTime_37(float value)
	{
		___lastJumpTime_37 = value;
	}

	inline static int32_t get_offset_of_inAirVelocity_38() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___inAirVelocity_38)); }
	inline Vector3_t3722313464  get_inAirVelocity_38() const { return ___inAirVelocity_38; }
	inline Vector3_t3722313464 * get_address_of_inAirVelocity_38() { return &___inAirVelocity_38; }
	inline void set_inAirVelocity_38(Vector3_t3722313464  value)
	{
		___inAirVelocity_38 = value;
	}

	inline static int32_t get_offset_of_lastGroundedTime_39() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___lastGroundedTime_39)); }
	inline float get_lastGroundedTime_39() const { return ___lastGroundedTime_39; }
	inline float* get_address_of_lastGroundedTime_39() { return &___lastGroundedTime_39; }
	inline void set_lastGroundedTime_39(float value)
	{
		___lastGroundedTime_39 = value;
	}

	inline static int32_t get_offset_of_velocity_40() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___velocity_40)); }
	inline Vector3_t3722313464  get_velocity_40() const { return ___velocity_40; }
	inline Vector3_t3722313464 * get_address_of_velocity_40() { return &___velocity_40; }
	inline void set_velocity_40(Vector3_t3722313464  value)
	{
		___velocity_40 = value;
	}

	inline static int32_t get_offset_of_lastPos_41() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___lastPos_41)); }
	inline Vector3_t3722313464  get_lastPos_41() const { return ___lastPos_41; }
	inline Vector3_t3722313464 * get_address_of_lastPos_41() { return &___lastPos_41; }
	inline void set_lastPos_41(Vector3_t3722313464  value)
	{
		___lastPos_41 = value;
	}

	inline static int32_t get_offset_of_remotePosition_42() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___remotePosition_42)); }
	inline Vector3_t3722313464  get_remotePosition_42() const { return ___remotePosition_42; }
	inline Vector3_t3722313464 * get_address_of_remotePosition_42() { return &___remotePosition_42; }
	inline void set_remotePosition_42(Vector3_t3722313464  value)
	{
		___remotePosition_42 = value;
	}

	inline static int32_t get_offset_of_isControllable_43() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___isControllable_43)); }
	inline bool get_isControllable_43() const { return ___isControllable_43; }
	inline bool* get_address_of_isControllable_43() { return &___isControllable_43; }
	inline void set_isControllable_43(bool value)
	{
		___isControllable_43 = value;
	}

	inline static int32_t get_offset_of_DoRotate_44() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___DoRotate_44)); }
	inline bool get_DoRotate_44() const { return ___DoRotate_44; }
	inline bool* get_address_of_DoRotate_44() { return &___DoRotate_44; }
	inline void set_DoRotate_44(bool value)
	{
		___DoRotate_44 = value;
	}

	inline static int32_t get_offset_of_RemoteSmoothing_45() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___RemoteSmoothing_45)); }
	inline float get_RemoteSmoothing_45() const { return ___RemoteSmoothing_45; }
	inline float* get_address_of_RemoteSmoothing_45() { return &___RemoteSmoothing_45; }
	inline void set_RemoteSmoothing_45(float value)
	{
		___RemoteSmoothing_45 = value;
	}

	inline static int32_t get_offset_of_AssignAsTagObject_46() { return static_cast<int32_t>(offsetof(PickupController_t2983846149, ___AssignAsTagObject_46)); }
	inline bool get_AssignAsTagObject_46() const { return ___AssignAsTagObject_46; }
	inline bool* get_address_of_AssignAsTagObject_46() { return &___AssignAsTagObject_46; }
	inline void set_AssignAsTagObject_46(bool value)
	{
		___AssignAsTagObject_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUPCONTROLLER_T2983846149_H
#ifndef ONPICKEDUPSCRIPT_T4029004332_H
#define ONPICKEDUPSCRIPT_T4029004332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnPickedUpScript
struct  OnPickedUpScript_t4029004332  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPICKEDUPSCRIPT_T4029004332_H
#ifndef ONCOLLIDESWITCHTEAM_T3835393565_H
#define ONCOLLIDESWITCHTEAM_T3835393565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnCollideSwitchTeam
struct  OnCollideSwitchTeam_t3835393565  : public MonoBehaviour_t3962482529
{
public:
	// PunTeams/Team OnCollideSwitchTeam::TeamToSwitchTo
	uint8_t ___TeamToSwitchTo_2;

public:
	inline static int32_t get_offset_of_TeamToSwitchTo_2() { return static_cast<int32_t>(offsetof(OnCollideSwitchTeam_t3835393565, ___TeamToSwitchTo_2)); }
	inline uint8_t get_TeamToSwitchTo_2() const { return ___TeamToSwitchTo_2; }
	inline uint8_t* get_address_of_TeamToSwitchTo_2() { return &___TeamToSwitchTo_2; }
	inline void set_TeamToSwitchTo_2(uint8_t value)
	{
		___TeamToSwitchTo_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCOLLIDESWITCHTEAM_T3835393565_H
#ifndef PUNBEHAVIOUR_T987309092_H
#define PUNBEHAVIOUR_T987309092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.PunBehaviour
struct  PunBehaviour_t987309092  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNBEHAVIOUR_T987309092_H
#ifndef ONDOUBLECLICKDESTROY_T3154303920_H
#define ONDOUBLECLICKDESTROY_T3154303920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnDoubleclickDestroy
struct  OnDoubleclickDestroy_t3154303920  : public MonoBehaviour_t3225183318
{
public:
	// System.Single OnDoubleclickDestroy::timeOfLastClick
	float ___timeOfLastClick_3;

public:
	inline static int32_t get_offset_of_timeOfLastClick_3() { return static_cast<int32_t>(offsetof(OnDoubleclickDestroy_t3154303920, ___timeOfLastClick_3)); }
	inline float get_timeOfLastClick_3() const { return ___timeOfLastClick_3; }
	inline float* get_address_of_timeOfLastClick_3() { return &___timeOfLastClick_3; }
	inline void set_timeOfLastClick_3(float value)
	{
		___timeOfLastClick_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOUBLECLICKDESTROY_T3154303920_H
#ifndef CLICKANDDRAG_T858977362_H
#define CLICKANDDRAG_T858977362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickAndDrag
struct  ClickAndDrag_t858977362  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.Vector3 ClickAndDrag::camOnPress
	Vector3_t3722313464  ___camOnPress_3;
	// System.Boolean ClickAndDrag::following
	bool ___following_4;
	// System.Single ClickAndDrag::factor
	float ___factor_5;

public:
	inline static int32_t get_offset_of_camOnPress_3() { return static_cast<int32_t>(offsetof(ClickAndDrag_t858977362, ___camOnPress_3)); }
	inline Vector3_t3722313464  get_camOnPress_3() const { return ___camOnPress_3; }
	inline Vector3_t3722313464 * get_address_of_camOnPress_3() { return &___camOnPress_3; }
	inline void set_camOnPress_3(Vector3_t3722313464  value)
	{
		___camOnPress_3 = value;
	}

	inline static int32_t get_offset_of_following_4() { return static_cast<int32_t>(offsetof(ClickAndDrag_t858977362, ___following_4)); }
	inline bool get_following_4() const { return ___following_4; }
	inline bool* get_address_of_following_4() { return &___following_4; }
	inline void set_following_4(bool value)
	{
		___following_4 = value;
	}

	inline static int32_t get_offset_of_factor_5() { return static_cast<int32_t>(offsetof(ClickAndDrag_t858977362, ___factor_5)); }
	inline float get_factor_5() const { return ___factor_5; }
	inline float* get_address_of_factor_5() { return &___factor_5; }
	inline void set_factor_5(float value)
	{
		___factor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKANDDRAG_T858977362_H
#ifndef ONAWAKEPHYSICSSETTINGS_T2449268769_H
#define ONAWAKEPHYSICSSETTINGS_T2449268769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnAwakePhysicsSettings
struct  OnAwakePhysicsSettings_t2449268769  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONAWAKEPHYSICSSETTINGS_T2449268769_H
#ifndef SHOWINFOOFPLAYER_T601303432_H
#define SHOWINFOOFPLAYER_T601303432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowInfoOfPlayer
struct  ShowInfoOfPlayer_t601303432  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.GameObject ShowInfoOfPlayer::textGo
	GameObject_t1113636619 * ___textGo_3;
	// UnityEngine.TextMesh ShowInfoOfPlayer::tm
	TextMesh_t1536577757 * ___tm_4;
	// System.Single ShowInfoOfPlayer::CharacterSize
	float ___CharacterSize_5;
	// UnityEngine.Font ShowInfoOfPlayer::font
	Font_t1956802104 * ___font_6;
	// System.Boolean ShowInfoOfPlayer::DisableOnOwnObjects
	bool ___DisableOnOwnObjects_7;

public:
	inline static int32_t get_offset_of_textGo_3() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___textGo_3)); }
	inline GameObject_t1113636619 * get_textGo_3() const { return ___textGo_3; }
	inline GameObject_t1113636619 ** get_address_of_textGo_3() { return &___textGo_3; }
	inline void set_textGo_3(GameObject_t1113636619 * value)
	{
		___textGo_3 = value;
		Il2CppCodeGenWriteBarrier((&___textGo_3), value);
	}

	inline static int32_t get_offset_of_tm_4() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___tm_4)); }
	inline TextMesh_t1536577757 * get_tm_4() const { return ___tm_4; }
	inline TextMesh_t1536577757 ** get_address_of_tm_4() { return &___tm_4; }
	inline void set_tm_4(TextMesh_t1536577757 * value)
	{
		___tm_4 = value;
		Il2CppCodeGenWriteBarrier((&___tm_4), value);
	}

	inline static int32_t get_offset_of_CharacterSize_5() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___CharacterSize_5)); }
	inline float get_CharacterSize_5() const { return ___CharacterSize_5; }
	inline float* get_address_of_CharacterSize_5() { return &___CharacterSize_5; }
	inline void set_CharacterSize_5(float value)
	{
		___CharacterSize_5 = value;
	}

	inline static int32_t get_offset_of_font_6() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___font_6)); }
	inline Font_t1956802104 * get_font_6() const { return ___font_6; }
	inline Font_t1956802104 ** get_address_of_font_6() { return &___font_6; }
	inline void set_font_6(Font_t1956802104 * value)
	{
		___font_6 = value;
		Il2CppCodeGenWriteBarrier((&___font_6), value);
	}

	inline static int32_t get_offset_of_DisableOnOwnObjects_7() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___DisableOnOwnObjects_7)); }
	inline bool get_DisableOnOwnObjects_7() const { return ___DisableOnOwnObjects_7; }
	inline bool* get_address_of_DisableOnOwnObjects_7() { return &___DisableOnOwnObjects_7; }
	inline void set_DisableOnOwnObjects_7(bool value)
	{
		___DisableOnOwnObjects_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWINFOOFPLAYER_T601303432_H
#ifndef WORKERINGAME_T3415221707_H
#define WORKERINGAME_T3415221707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorkerInGame
struct  WorkerInGame_t3415221707  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.Transform WorkerInGame::playerPrefab
	Transform_t3600365921 * ___playerPrefab_3;

public:
	inline static int32_t get_offset_of_playerPrefab_3() { return static_cast<int32_t>(offsetof(WorkerInGame_t3415221707, ___playerPrefab_3)); }
	inline Transform_t3600365921 * get_playerPrefab_3() const { return ___playerPrefab_3; }
	inline Transform_t3600365921 ** get_address_of_playerPrefab_3() { return &___playerPrefab_3; }
	inline void set_playerPrefab_3(Transform_t3600365921 * value)
	{
		___playerPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___playerPrefab_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORKERINGAME_T3415221707_H
#ifndef THIRDPERSONNETWORK_T3674108340_H
#define THIRDPERSONNETWORK_T3674108340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdPersonNetwork
struct  ThirdPersonNetwork_t3674108340  : public MonoBehaviour_t3225183318
{
public:
	// ThirdPersonCamera ThirdPersonNetwork::cameraScript
	ThirdPersonCamera_t2998681409 * ___cameraScript_3;
	// ThirdPersonController ThirdPersonNetwork::controllerScript
	ThirdPersonController_t2544474708 * ___controllerScript_4;
	// System.Boolean ThirdPersonNetwork::firstTake
	bool ___firstTake_5;
	// UnityEngine.Vector3 ThirdPersonNetwork::correctPlayerPos
	Vector3_t3722313464  ___correctPlayerPos_6;
	// UnityEngine.Quaternion ThirdPersonNetwork::correctPlayerRot
	Quaternion_t2301928331  ___correctPlayerRot_7;

public:
	inline static int32_t get_offset_of_cameraScript_3() { return static_cast<int32_t>(offsetof(ThirdPersonNetwork_t3674108340, ___cameraScript_3)); }
	inline ThirdPersonCamera_t2998681409 * get_cameraScript_3() const { return ___cameraScript_3; }
	inline ThirdPersonCamera_t2998681409 ** get_address_of_cameraScript_3() { return &___cameraScript_3; }
	inline void set_cameraScript_3(ThirdPersonCamera_t2998681409 * value)
	{
		___cameraScript_3 = value;
		Il2CppCodeGenWriteBarrier((&___cameraScript_3), value);
	}

	inline static int32_t get_offset_of_controllerScript_4() { return static_cast<int32_t>(offsetof(ThirdPersonNetwork_t3674108340, ___controllerScript_4)); }
	inline ThirdPersonController_t2544474708 * get_controllerScript_4() const { return ___controllerScript_4; }
	inline ThirdPersonController_t2544474708 ** get_address_of_controllerScript_4() { return &___controllerScript_4; }
	inline void set_controllerScript_4(ThirdPersonController_t2544474708 * value)
	{
		___controllerScript_4 = value;
		Il2CppCodeGenWriteBarrier((&___controllerScript_4), value);
	}

	inline static int32_t get_offset_of_firstTake_5() { return static_cast<int32_t>(offsetof(ThirdPersonNetwork_t3674108340, ___firstTake_5)); }
	inline bool get_firstTake_5() const { return ___firstTake_5; }
	inline bool* get_address_of_firstTake_5() { return &___firstTake_5; }
	inline void set_firstTake_5(bool value)
	{
		___firstTake_5 = value;
	}

	inline static int32_t get_offset_of_correctPlayerPos_6() { return static_cast<int32_t>(offsetof(ThirdPersonNetwork_t3674108340, ___correctPlayerPos_6)); }
	inline Vector3_t3722313464  get_correctPlayerPos_6() const { return ___correctPlayerPos_6; }
	inline Vector3_t3722313464 * get_address_of_correctPlayerPos_6() { return &___correctPlayerPos_6; }
	inline void set_correctPlayerPos_6(Vector3_t3722313464  value)
	{
		___correctPlayerPos_6 = value;
	}

	inline static int32_t get_offset_of_correctPlayerRot_7() { return static_cast<int32_t>(offsetof(ThirdPersonNetwork_t3674108340, ___correctPlayerRot_7)); }
	inline Quaternion_t2301928331  get_correctPlayerRot_7() const { return ___correctPlayerRot_7; }
	inline Quaternion_t2301928331 * get_address_of_correctPlayerRot_7() { return &___correctPlayerRot_7; }
	inline void set_correctPlayerRot_7(Quaternion_t2301928331  value)
	{
		___correctPlayerRot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONNETWORK_T3674108340_H
#ifndef CUBELERP_T1035047565_H
#define CUBELERP_T1035047565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CubeLerp
struct  CubeLerp_t1035047565  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.Vector3 CubeLerp::latestCorrectPos
	Vector3_t3722313464  ___latestCorrectPos_3;
	// UnityEngine.Vector3 CubeLerp::onUpdatePos
	Vector3_t3722313464  ___onUpdatePos_4;
	// System.Single CubeLerp::fraction
	float ___fraction_5;

public:
	inline static int32_t get_offset_of_latestCorrectPos_3() { return static_cast<int32_t>(offsetof(CubeLerp_t1035047565, ___latestCorrectPos_3)); }
	inline Vector3_t3722313464  get_latestCorrectPos_3() const { return ___latestCorrectPos_3; }
	inline Vector3_t3722313464 * get_address_of_latestCorrectPos_3() { return &___latestCorrectPos_3; }
	inline void set_latestCorrectPos_3(Vector3_t3722313464  value)
	{
		___latestCorrectPos_3 = value;
	}

	inline static int32_t get_offset_of_onUpdatePos_4() { return static_cast<int32_t>(offsetof(CubeLerp_t1035047565, ___onUpdatePos_4)); }
	inline Vector3_t3722313464  get_onUpdatePos_4() const { return ___onUpdatePos_4; }
	inline Vector3_t3722313464 * get_address_of_onUpdatePos_4() { return &___onUpdatePos_4; }
	inline void set_onUpdatePos_4(Vector3_t3722313464  value)
	{
		___onUpdatePos_4 = value;
	}

	inline static int32_t get_offset_of_fraction_5() { return static_cast<int32_t>(offsetof(CubeLerp_t1035047565, ___fraction_5)); }
	inline float get_fraction_5() const { return ___fraction_5; }
	inline float* get_address_of_fraction_5() { return &___fraction_5; }
	inline void set_fraction_5(float value)
	{
		___fraction_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBELERP_T1035047565_H
#ifndef CUBEEXTRA_T2395085633_H
#define CUBEEXTRA_T2395085633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CubeExtra
struct  CubeExtra_t2395085633  : public MonoBehaviour_t3225183318
{
public:
	// System.Single CubeExtra::Factor
	float ___Factor_3;
	// UnityEngine.Vector3 CubeExtra::latestCorrectPos
	Vector3_t3722313464  ___latestCorrectPos_4;
	// UnityEngine.Vector3 CubeExtra::movementVector
	Vector3_t3722313464  ___movementVector_5;
	// UnityEngine.Vector3 CubeExtra::errorVector
	Vector3_t3722313464  ___errorVector_6;
	// System.Double CubeExtra::lastTime
	double ___lastTime_7;

public:
	inline static int32_t get_offset_of_Factor_3() { return static_cast<int32_t>(offsetof(CubeExtra_t2395085633, ___Factor_3)); }
	inline float get_Factor_3() const { return ___Factor_3; }
	inline float* get_address_of_Factor_3() { return &___Factor_3; }
	inline void set_Factor_3(float value)
	{
		___Factor_3 = value;
	}

	inline static int32_t get_offset_of_latestCorrectPos_4() { return static_cast<int32_t>(offsetof(CubeExtra_t2395085633, ___latestCorrectPos_4)); }
	inline Vector3_t3722313464  get_latestCorrectPos_4() const { return ___latestCorrectPos_4; }
	inline Vector3_t3722313464 * get_address_of_latestCorrectPos_4() { return &___latestCorrectPos_4; }
	inline void set_latestCorrectPos_4(Vector3_t3722313464  value)
	{
		___latestCorrectPos_4 = value;
	}

	inline static int32_t get_offset_of_movementVector_5() { return static_cast<int32_t>(offsetof(CubeExtra_t2395085633, ___movementVector_5)); }
	inline Vector3_t3722313464  get_movementVector_5() const { return ___movementVector_5; }
	inline Vector3_t3722313464 * get_address_of_movementVector_5() { return &___movementVector_5; }
	inline void set_movementVector_5(Vector3_t3722313464  value)
	{
		___movementVector_5 = value;
	}

	inline static int32_t get_offset_of_errorVector_6() { return static_cast<int32_t>(offsetof(CubeExtra_t2395085633, ___errorVector_6)); }
	inline Vector3_t3722313464  get_errorVector_6() const { return ___errorVector_6; }
	inline Vector3_t3722313464 * get_address_of_errorVector_6() { return &___errorVector_6; }
	inline void set_errorVector_6(Vector3_t3722313464  value)
	{
		___errorVector_6 = value;
	}

	inline static int32_t get_offset_of_lastTime_7() { return static_cast<int32_t>(offsetof(CubeExtra_t2395085633, ___lastTime_7)); }
	inline double get_lastTime_7() const { return ___lastTime_7; }
	inline double* get_address_of_lastTime_7() { return &___lastTime_7; }
	inline void set_lastTime_7(double value)
	{
		___lastTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEEXTRA_T2395085633_H
#ifndef MATERIALPEROWNER_T3683227728_H
#define MATERIALPEROWNER_T3683227728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaterialPerOwner
struct  MaterialPerOwner_t3683227728  : public MonoBehaviour_t3225183318
{
public:
	// System.Int32 MaterialPerOwner::assignedColorForUserId
	int32_t ___assignedColorForUserId_3;
	// UnityEngine.Renderer MaterialPerOwner::m_Renderer
	Renderer_t2627027031 * ___m_Renderer_4;

public:
	inline static int32_t get_offset_of_assignedColorForUserId_3() { return static_cast<int32_t>(offsetof(MaterialPerOwner_t3683227728, ___assignedColorForUserId_3)); }
	inline int32_t get_assignedColorForUserId_3() const { return ___assignedColorForUserId_3; }
	inline int32_t* get_address_of_assignedColorForUserId_3() { return &___assignedColorForUserId_3; }
	inline void set_assignedColorForUserId_3(int32_t value)
	{
		___assignedColorForUserId_3 = value;
	}

	inline static int32_t get_offset_of_m_Renderer_4() { return static_cast<int32_t>(offsetof(MaterialPerOwner_t3683227728, ___m_Renderer_4)); }
	inline Renderer_t2627027031 * get_m_Renderer_4() const { return ___m_Renderer_4; }
	inline Renderer_t2627027031 ** get_address_of_m_Renderer_4() { return &___m_Renderer_4; }
	inline void set_m_Renderer_4(Renderer_t2627027031 * value)
	{
		___m_Renderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPEROWNER_T3683227728_H
#ifndef PICKUPCAMERA_T3869569942_H
#define PICKUPCAMERA_T3869569942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickupCamera
struct  PickupCamera_t3869569942  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.Transform PickupCamera::cameraTransform
	Transform_t3600365921 * ___cameraTransform_3;
	// UnityEngine.Transform PickupCamera::_target
	Transform_t3600365921 * ____target_4;
	// System.Single PickupCamera::distance
	float ___distance_5;
	// System.Single PickupCamera::height
	float ___height_6;
	// System.Single PickupCamera::angularSmoothLag
	float ___angularSmoothLag_7;
	// System.Single PickupCamera::angularMaxSpeed
	float ___angularMaxSpeed_8;
	// System.Single PickupCamera::heightSmoothLag
	float ___heightSmoothLag_9;
	// System.Single PickupCamera::snapSmoothLag
	float ___snapSmoothLag_10;
	// System.Single PickupCamera::snapMaxSpeed
	float ___snapMaxSpeed_11;
	// System.Single PickupCamera::clampHeadPositionScreenSpace
	float ___clampHeadPositionScreenSpace_12;
	// System.Single PickupCamera::lockCameraTimeout
	float ___lockCameraTimeout_13;
	// UnityEngine.Vector3 PickupCamera::headOffset
	Vector3_t3722313464  ___headOffset_14;
	// UnityEngine.Vector3 PickupCamera::centerOffset
	Vector3_t3722313464  ___centerOffset_15;
	// System.Single PickupCamera::heightVelocity
	float ___heightVelocity_16;
	// System.Single PickupCamera::angleVelocity
	float ___angleVelocity_17;
	// System.Boolean PickupCamera::snap
	bool ___snap_18;
	// PickupController PickupCamera::controller
	PickupController_t2983846149 * ___controller_19;
	// System.Single PickupCamera::targetHeight
	float ___targetHeight_20;
	// UnityEngine.Camera PickupCamera::m_CameraTransformCamera
	Camera_t4157153871 * ___m_CameraTransformCamera_21;

public:
	inline static int32_t get_offset_of_cameraTransform_3() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___cameraTransform_3)); }
	inline Transform_t3600365921 * get_cameraTransform_3() const { return ___cameraTransform_3; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_3() { return &___cameraTransform_3; }
	inline void set_cameraTransform_3(Transform_t3600365921 * value)
	{
		___cameraTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_3), value);
	}

	inline static int32_t get_offset_of__target_4() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ____target_4)); }
	inline Transform_t3600365921 * get__target_4() const { return ____target_4; }
	inline Transform_t3600365921 ** get_address_of__target_4() { return &____target_4; }
	inline void set__target_4(Transform_t3600365921 * value)
	{
		____target_4 = value;
		Il2CppCodeGenWriteBarrier((&____target_4), value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_angularSmoothLag_7() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___angularSmoothLag_7)); }
	inline float get_angularSmoothLag_7() const { return ___angularSmoothLag_7; }
	inline float* get_address_of_angularSmoothLag_7() { return &___angularSmoothLag_7; }
	inline void set_angularSmoothLag_7(float value)
	{
		___angularSmoothLag_7 = value;
	}

	inline static int32_t get_offset_of_angularMaxSpeed_8() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___angularMaxSpeed_8)); }
	inline float get_angularMaxSpeed_8() const { return ___angularMaxSpeed_8; }
	inline float* get_address_of_angularMaxSpeed_8() { return &___angularMaxSpeed_8; }
	inline void set_angularMaxSpeed_8(float value)
	{
		___angularMaxSpeed_8 = value;
	}

	inline static int32_t get_offset_of_heightSmoothLag_9() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___heightSmoothLag_9)); }
	inline float get_heightSmoothLag_9() const { return ___heightSmoothLag_9; }
	inline float* get_address_of_heightSmoothLag_9() { return &___heightSmoothLag_9; }
	inline void set_heightSmoothLag_9(float value)
	{
		___heightSmoothLag_9 = value;
	}

	inline static int32_t get_offset_of_snapSmoothLag_10() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___snapSmoothLag_10)); }
	inline float get_snapSmoothLag_10() const { return ___snapSmoothLag_10; }
	inline float* get_address_of_snapSmoothLag_10() { return &___snapSmoothLag_10; }
	inline void set_snapSmoothLag_10(float value)
	{
		___snapSmoothLag_10 = value;
	}

	inline static int32_t get_offset_of_snapMaxSpeed_11() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___snapMaxSpeed_11)); }
	inline float get_snapMaxSpeed_11() const { return ___snapMaxSpeed_11; }
	inline float* get_address_of_snapMaxSpeed_11() { return &___snapMaxSpeed_11; }
	inline void set_snapMaxSpeed_11(float value)
	{
		___snapMaxSpeed_11 = value;
	}

	inline static int32_t get_offset_of_clampHeadPositionScreenSpace_12() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___clampHeadPositionScreenSpace_12)); }
	inline float get_clampHeadPositionScreenSpace_12() const { return ___clampHeadPositionScreenSpace_12; }
	inline float* get_address_of_clampHeadPositionScreenSpace_12() { return &___clampHeadPositionScreenSpace_12; }
	inline void set_clampHeadPositionScreenSpace_12(float value)
	{
		___clampHeadPositionScreenSpace_12 = value;
	}

	inline static int32_t get_offset_of_lockCameraTimeout_13() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___lockCameraTimeout_13)); }
	inline float get_lockCameraTimeout_13() const { return ___lockCameraTimeout_13; }
	inline float* get_address_of_lockCameraTimeout_13() { return &___lockCameraTimeout_13; }
	inline void set_lockCameraTimeout_13(float value)
	{
		___lockCameraTimeout_13 = value;
	}

	inline static int32_t get_offset_of_headOffset_14() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___headOffset_14)); }
	inline Vector3_t3722313464  get_headOffset_14() const { return ___headOffset_14; }
	inline Vector3_t3722313464 * get_address_of_headOffset_14() { return &___headOffset_14; }
	inline void set_headOffset_14(Vector3_t3722313464  value)
	{
		___headOffset_14 = value;
	}

	inline static int32_t get_offset_of_centerOffset_15() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___centerOffset_15)); }
	inline Vector3_t3722313464  get_centerOffset_15() const { return ___centerOffset_15; }
	inline Vector3_t3722313464 * get_address_of_centerOffset_15() { return &___centerOffset_15; }
	inline void set_centerOffset_15(Vector3_t3722313464  value)
	{
		___centerOffset_15 = value;
	}

	inline static int32_t get_offset_of_heightVelocity_16() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___heightVelocity_16)); }
	inline float get_heightVelocity_16() const { return ___heightVelocity_16; }
	inline float* get_address_of_heightVelocity_16() { return &___heightVelocity_16; }
	inline void set_heightVelocity_16(float value)
	{
		___heightVelocity_16 = value;
	}

	inline static int32_t get_offset_of_angleVelocity_17() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___angleVelocity_17)); }
	inline float get_angleVelocity_17() const { return ___angleVelocity_17; }
	inline float* get_address_of_angleVelocity_17() { return &___angleVelocity_17; }
	inline void set_angleVelocity_17(float value)
	{
		___angleVelocity_17 = value;
	}

	inline static int32_t get_offset_of_snap_18() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___snap_18)); }
	inline bool get_snap_18() const { return ___snap_18; }
	inline bool* get_address_of_snap_18() { return &___snap_18; }
	inline void set_snap_18(bool value)
	{
		___snap_18 = value;
	}

	inline static int32_t get_offset_of_controller_19() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___controller_19)); }
	inline PickupController_t2983846149 * get_controller_19() const { return ___controller_19; }
	inline PickupController_t2983846149 ** get_address_of_controller_19() { return &___controller_19; }
	inline void set_controller_19(PickupController_t2983846149 * value)
	{
		___controller_19 = value;
		Il2CppCodeGenWriteBarrier((&___controller_19), value);
	}

	inline static int32_t get_offset_of_targetHeight_20() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___targetHeight_20)); }
	inline float get_targetHeight_20() const { return ___targetHeight_20; }
	inline float* get_address_of_targetHeight_20() { return &___targetHeight_20; }
	inline void set_targetHeight_20(float value)
	{
		___targetHeight_20 = value;
	}

	inline static int32_t get_offset_of_m_CameraTransformCamera_21() { return static_cast<int32_t>(offsetof(PickupCamera_t3869569942, ___m_CameraTransformCamera_21)); }
	inline Camera_t4157153871 * get_m_CameraTransformCamera_21() const { return ___m_CameraTransformCamera_21; }
	inline Camera_t4157153871 ** get_address_of_m_CameraTransformCamera_21() { return &___m_CameraTransformCamera_21; }
	inline void set_m_CameraTransformCamera_21(Camera_t4157153871 * value)
	{
		___m_CameraTransformCamera_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraTransformCamera_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUPCAMERA_T3869569942_H
#ifndef ONCLICKREQUESTOWNERSHIP_T1795600179_H
#define ONCLICKREQUESTOWNERSHIP_T1795600179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickRequestOwnership
struct  OnClickRequestOwnership_t1795600179  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKREQUESTOWNERSHIP_T1795600179_H
#ifndef ONCLICKCALLMETHOD_T3787258516_H
#define ONCLICKCALLMETHOD_T3787258516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickCallMethod
struct  OnClickCallMethod_t3787258516  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.GameObject OnClickCallMethod::TargetGameObject
	GameObject_t1113636619 * ___TargetGameObject_3;
	// System.String OnClickCallMethod::TargetMethod
	String_t* ___TargetMethod_4;

public:
	inline static int32_t get_offset_of_TargetGameObject_3() { return static_cast<int32_t>(offsetof(OnClickCallMethod_t3787258516, ___TargetGameObject_3)); }
	inline GameObject_t1113636619 * get_TargetGameObject_3() const { return ___TargetGameObject_3; }
	inline GameObject_t1113636619 ** get_address_of_TargetGameObject_3() { return &___TargetGameObject_3; }
	inline void set_TargetGameObject_3(GameObject_t1113636619 * value)
	{
		___TargetGameObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___TargetGameObject_3), value);
	}

	inline static int32_t get_offset_of_TargetMethod_4() { return static_cast<int32_t>(offsetof(OnClickCallMethod_t3787258516, ___TargetMethod_4)); }
	inline String_t* get_TargetMethod_4() const { return ___TargetMethod_4; }
	inline String_t** get_address_of_TargetMethod_4() { return &___TargetMethod_4; }
	inline void set_TargetMethod_4(String_t* value)
	{
		___TargetMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&___TargetMethod_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKCALLMETHOD_T3787258516_H
#ifndef AUDIORPC_T2629069994_H
#define AUDIORPC_T2629069994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioRpc
struct  AudioRpc_t2629069994  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.AudioClip AudioRpc::marco
	AudioClip_t3680889665 * ___marco_3;
	// UnityEngine.AudioClip AudioRpc::polo
	AudioClip_t3680889665 * ___polo_4;
	// UnityEngine.AudioSource AudioRpc::m_Source
	AudioSource_t3935305588 * ___m_Source_5;

public:
	inline static int32_t get_offset_of_marco_3() { return static_cast<int32_t>(offsetof(AudioRpc_t2629069994, ___marco_3)); }
	inline AudioClip_t3680889665 * get_marco_3() const { return ___marco_3; }
	inline AudioClip_t3680889665 ** get_address_of_marco_3() { return &___marco_3; }
	inline void set_marco_3(AudioClip_t3680889665 * value)
	{
		___marco_3 = value;
		Il2CppCodeGenWriteBarrier((&___marco_3), value);
	}

	inline static int32_t get_offset_of_polo_4() { return static_cast<int32_t>(offsetof(AudioRpc_t2629069994, ___polo_4)); }
	inline AudioClip_t3680889665 * get_polo_4() const { return ___polo_4; }
	inline AudioClip_t3680889665 ** get_address_of_polo_4() { return &___polo_4; }
	inline void set_polo_4(AudioClip_t3680889665 * value)
	{
		___polo_4 = value;
		Il2CppCodeGenWriteBarrier((&___polo_4), value);
	}

	inline static int32_t get_offset_of_m_Source_5() { return static_cast<int32_t>(offsetof(AudioRpc_t2629069994, ___m_Source_5)); }
	inline AudioSource_t3935305588 * get_m_Source_5() const { return ___m_Source_5; }
	inline AudioSource_t3935305588 ** get_address_of_m_Source_5() { return &___m_Source_5; }
	inline void set_m_Source_5(AudioSource_t3935305588 * value)
	{
		___m_Source_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Source_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIORPC_T2629069994_H
#ifndef HIGHLIGHTOWNEDGAMEOBJ_T513766295_H
#define HIGHLIGHTOWNEDGAMEOBJ_T513766295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightOwnedGameObj
struct  HighlightOwnedGameObj_t513766295  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.GameObject HighlightOwnedGameObj::PointerPrefab
	GameObject_t1113636619 * ___PointerPrefab_3;
	// System.Single HighlightOwnedGameObj::Offset
	float ___Offset_4;
	// UnityEngine.Transform HighlightOwnedGameObj::markerTransform
	Transform_t3600365921 * ___markerTransform_5;

public:
	inline static int32_t get_offset_of_PointerPrefab_3() { return static_cast<int32_t>(offsetof(HighlightOwnedGameObj_t513766295, ___PointerPrefab_3)); }
	inline GameObject_t1113636619 * get_PointerPrefab_3() const { return ___PointerPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_PointerPrefab_3() { return &___PointerPrefab_3; }
	inline void set_PointerPrefab_3(GameObject_t1113636619 * value)
	{
		___PointerPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PointerPrefab_3), value);
	}

	inline static int32_t get_offset_of_Offset_4() { return static_cast<int32_t>(offsetof(HighlightOwnedGameObj_t513766295, ___Offset_4)); }
	inline float get_Offset_4() const { return ___Offset_4; }
	inline float* get_address_of_Offset_4() { return &___Offset_4; }
	inline void set_Offset_4(float value)
	{
		___Offset_4 = value;
	}

	inline static int32_t get_offset_of_markerTransform_5() { return static_cast<int32_t>(offsetof(HighlightOwnedGameObj_t513766295, ___markerTransform_5)); }
	inline Transform_t3600365921 * get_markerTransform_5() const { return ___markerTransform_5; }
	inline Transform_t3600365921 ** get_address_of_markerTransform_5() { return &___markerTransform_5; }
	inline void set_markerTransform_5(Transform_t3600365921 * value)
	{
		___markerTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___markerTransform_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHTOWNEDGAMEOBJ_T513766295_H
#ifndef PLAYERANIMATORMANAGER_T2952684908_H
#define PLAYERANIMATORMANAGER_T2952684908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoAnimator.PlayerAnimatorManager
struct  PlayerAnimatorManager_t2952684908  : public MonoBehaviour_t3225183318
{
public:
	// System.Single ExitGames.Demos.DemoAnimator.PlayerAnimatorManager::DirectionDampTime
	float ___DirectionDampTime_3;
	// UnityEngine.Animator ExitGames.Demos.DemoAnimator.PlayerAnimatorManager::animator
	Animator_t434523843 * ___animator_4;

public:
	inline static int32_t get_offset_of_DirectionDampTime_3() { return static_cast<int32_t>(offsetof(PlayerAnimatorManager_t2952684908, ___DirectionDampTime_3)); }
	inline float get_DirectionDampTime_3() const { return ___DirectionDampTime_3; }
	inline float* get_address_of_DirectionDampTime_3() { return &___DirectionDampTime_3; }
	inline void set_DirectionDampTime_3(float value)
	{
		___DirectionDampTime_3 = value;
	}

	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(PlayerAnimatorManager_t2952684908, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERANIMATORMANAGER_T2952684908_H
#ifndef CUBEINTER_T3475839432_H
#define CUBEINTER_T3475839432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CubeInter
struct  CubeInter_t3475839432  : public MonoBehaviour_t3225183318
{
public:
	// CubeInter/State[] CubeInter::m_BufferedState
	StateU5BU5D_t3626104901* ___m_BufferedState_3;
	// System.Int32 CubeInter::m_TimestampCount
	int32_t ___m_TimestampCount_4;
	// System.Double CubeInter::InterpolationDelay
	double ___InterpolationDelay_5;

public:
	inline static int32_t get_offset_of_m_BufferedState_3() { return static_cast<int32_t>(offsetof(CubeInter_t3475839432, ___m_BufferedState_3)); }
	inline StateU5BU5D_t3626104901* get_m_BufferedState_3() const { return ___m_BufferedState_3; }
	inline StateU5BU5D_t3626104901** get_address_of_m_BufferedState_3() { return &___m_BufferedState_3; }
	inline void set_m_BufferedState_3(StateU5BU5D_t3626104901* value)
	{
		___m_BufferedState_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_BufferedState_3), value);
	}

	inline static int32_t get_offset_of_m_TimestampCount_4() { return static_cast<int32_t>(offsetof(CubeInter_t3475839432, ___m_TimestampCount_4)); }
	inline int32_t get_m_TimestampCount_4() const { return ___m_TimestampCount_4; }
	inline int32_t* get_address_of_m_TimestampCount_4() { return &___m_TimestampCount_4; }
	inline void set_m_TimestampCount_4(int32_t value)
	{
		___m_TimestampCount_4 = value;
	}

	inline static int32_t get_offset_of_InterpolationDelay_5() { return static_cast<int32_t>(offsetof(CubeInter_t3475839432, ___InterpolationDelay_5)); }
	inline double get_InterpolationDelay_5() const { return ___InterpolationDelay_5; }
	inline double* get_address_of_InterpolationDelay_5() { return &___InterpolationDelay_5; }
	inline void set_InterpolationDelay_5(double value)
	{
		___InterpolationDelay_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEINTER_T3475839432_H
#ifndef NETWORKCHARACTER_T2672613317_H
#define NETWORKCHARACTER_T2672613317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkCharacter
struct  NetworkCharacter_t2672613317  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.Vector3 NetworkCharacter::correctPlayerPos
	Vector3_t3722313464  ___correctPlayerPos_3;
	// UnityEngine.Quaternion NetworkCharacter::correctPlayerRot
	Quaternion_t2301928331  ___correctPlayerRot_4;

public:
	inline static int32_t get_offset_of_correctPlayerPos_3() { return static_cast<int32_t>(offsetof(NetworkCharacter_t2672613317, ___correctPlayerPos_3)); }
	inline Vector3_t3722313464  get_correctPlayerPos_3() const { return ___correctPlayerPos_3; }
	inline Vector3_t3722313464 * get_address_of_correctPlayerPos_3() { return &___correctPlayerPos_3; }
	inline void set_correctPlayerPos_3(Vector3_t3722313464  value)
	{
		___correctPlayerPos_3 = value;
	}

	inline static int32_t get_offset_of_correctPlayerRot_4() { return static_cast<int32_t>(offsetof(NetworkCharacter_t2672613317, ___correctPlayerRot_4)); }
	inline Quaternion_t2301928331  get_correctPlayerRot_4() const { return ___correctPlayerRot_4; }
	inline Quaternion_t2301928331 * get_address_of_correctPlayerRot_4() { return &___correctPlayerRot_4; }
	inline void set_correctPlayerRot_4(Quaternion_t2301928331  value)
	{
		___correctPlayerRot_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCHARACTER_T2672613317_H
#ifndef MYTHIRDPERSONCONTROLLER_T1931332062_H
#define MYTHIRDPERSONCONTROLLER_T1931332062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// myThirdPersonController
struct  myThirdPersonController_t1931332062  : public ThirdPersonController_t2544474708
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYTHIRDPERSONCONTROLLER_T1931332062_H
#ifndef COLORPERPLAYERAPPLY_T3143695489_H
#define COLORPERPLAYERAPPLY_T3143695489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPerPlayerApply
struct  ColorPerPlayerApply_t3143695489  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.Renderer ColorPerPlayerApply::rendererComponent
	Renderer_t2627027031 * ___rendererComponent_4;
	// System.Boolean ColorPerPlayerApply::isInitialized
	bool ___isInitialized_5;

public:
	inline static int32_t get_offset_of_rendererComponent_4() { return static_cast<int32_t>(offsetof(ColorPerPlayerApply_t3143695489, ___rendererComponent_4)); }
	inline Renderer_t2627027031 * get_rendererComponent_4() const { return ___rendererComponent_4; }
	inline Renderer_t2627027031 ** get_address_of_rendererComponent_4() { return &___rendererComponent_4; }
	inline void set_rendererComponent_4(Renderer_t2627027031 * value)
	{
		___rendererComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___rendererComponent_4), value);
	}

	inline static int32_t get_offset_of_isInitialized_5() { return static_cast<int32_t>(offsetof(ColorPerPlayerApply_t3143695489, ___isInitialized_5)); }
	inline bool get_isInitialized_5() const { return ___isInitialized_5; }
	inline bool* get_address_of_isInitialized_5() { return &___isInitialized_5; }
	inline void set_isInitialized_5(bool value)
	{
		___isInitialized_5 = value;
	}
};

struct ColorPerPlayerApply_t3143695489_StaticFields
{
public:
	// ColorPerPlayer ColorPerPlayerApply::colorPickerCache
	ColorPerPlayer_t432550946 * ___colorPickerCache_3;

public:
	inline static int32_t get_offset_of_colorPickerCache_3() { return static_cast<int32_t>(offsetof(ColorPerPlayerApply_t3143695489_StaticFields, ___colorPickerCache_3)); }
	inline ColorPerPlayer_t432550946 * get_colorPickerCache_3() const { return ___colorPickerCache_3; }
	inline ColorPerPlayer_t432550946 ** get_address_of_colorPickerCache_3() { return &___colorPickerCache_3; }
	inline void set_colorPickerCache_3(ColorPerPlayer_t432550946 * value)
	{
		___colorPickerCache_3 = value;
		Il2CppCodeGenWriteBarrier((&___colorPickerCache_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPERPLAYERAPPLY_T3143695489_H
#ifndef ONCLICKFLASHRPC_T2987886900_H
#define ONCLICKFLASHRPC_T2987886900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickFlashRpc
struct  OnClickFlashRpc_t2987886900  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.Material OnClickFlashRpc::originalMaterial
	Material_t340375123 * ___originalMaterial_3;
	// UnityEngine.Color OnClickFlashRpc::originalColor
	Color_t2555686324  ___originalColor_4;
	// System.Boolean OnClickFlashRpc::isFlashing
	bool ___isFlashing_5;

public:
	inline static int32_t get_offset_of_originalMaterial_3() { return static_cast<int32_t>(offsetof(OnClickFlashRpc_t2987886900, ___originalMaterial_3)); }
	inline Material_t340375123 * get_originalMaterial_3() const { return ___originalMaterial_3; }
	inline Material_t340375123 ** get_address_of_originalMaterial_3() { return &___originalMaterial_3; }
	inline void set_originalMaterial_3(Material_t340375123 * value)
	{
		___originalMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___originalMaterial_3), value);
	}

	inline static int32_t get_offset_of_originalColor_4() { return static_cast<int32_t>(offsetof(OnClickFlashRpc_t2987886900, ___originalColor_4)); }
	inline Color_t2555686324  get_originalColor_4() const { return ___originalColor_4; }
	inline Color_t2555686324 * get_address_of_originalColor_4() { return &___originalColor_4; }
	inline void set_originalColor_4(Color_t2555686324  value)
	{
		___originalColor_4 = value;
	}

	inline static int32_t get_offset_of_isFlashing_5() { return static_cast<int32_t>(offsetof(OnClickFlashRpc_t2987886900, ___isFlashing_5)); }
	inline bool get_isFlashing_5() const { return ___isFlashing_5; }
	inline bool* get_address_of_isFlashing_5() { return &___isFlashing_5; }
	inline void set_isFlashing_5(bool value)
	{
		___isFlashing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKFLASHRPC_T2987886900_H
#ifndef PLAYERMANAGER_T3964432985_H
#define PLAYERMANAGER_T3964432985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoAnimator.PlayerManager
struct  PlayerManager_t3964432985  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.GameObject ExitGames.Demos.DemoAnimator.PlayerManager::PlayerUiPrefab
	GameObject_t1113636619 * ___PlayerUiPrefab_3;
	// UnityEngine.GameObject ExitGames.Demos.DemoAnimator.PlayerManager::Beams
	GameObject_t1113636619 * ___Beams_4;
	// System.Single ExitGames.Demos.DemoAnimator.PlayerManager::Health
	float ___Health_5;
	// System.Boolean ExitGames.Demos.DemoAnimator.PlayerManager::IsFiring
	bool ___IsFiring_7;

public:
	inline static int32_t get_offset_of_PlayerUiPrefab_3() { return static_cast<int32_t>(offsetof(PlayerManager_t3964432985, ___PlayerUiPrefab_3)); }
	inline GameObject_t1113636619 * get_PlayerUiPrefab_3() const { return ___PlayerUiPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_PlayerUiPrefab_3() { return &___PlayerUiPrefab_3; }
	inline void set_PlayerUiPrefab_3(GameObject_t1113636619 * value)
	{
		___PlayerUiPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerUiPrefab_3), value);
	}

	inline static int32_t get_offset_of_Beams_4() { return static_cast<int32_t>(offsetof(PlayerManager_t3964432985, ___Beams_4)); }
	inline GameObject_t1113636619 * get_Beams_4() const { return ___Beams_4; }
	inline GameObject_t1113636619 ** get_address_of_Beams_4() { return &___Beams_4; }
	inline void set_Beams_4(GameObject_t1113636619 * value)
	{
		___Beams_4 = value;
		Il2CppCodeGenWriteBarrier((&___Beams_4), value);
	}

	inline static int32_t get_offset_of_Health_5() { return static_cast<int32_t>(offsetof(PlayerManager_t3964432985, ___Health_5)); }
	inline float get_Health_5() const { return ___Health_5; }
	inline float* get_address_of_Health_5() { return &___Health_5; }
	inline void set_Health_5(float value)
	{
		___Health_5 = value;
	}

	inline static int32_t get_offset_of_IsFiring_7() { return static_cast<int32_t>(offsetof(PlayerManager_t3964432985, ___IsFiring_7)); }
	inline bool get_IsFiring_7() const { return ___IsFiring_7; }
	inline bool* get_address_of_IsFiring_7() { return &___IsFiring_7; }
	inline void set_IsFiring_7(bool value)
	{
		___IsFiring_7 = value;
	}
};

struct PlayerManager_t3964432985_StaticFields
{
public:
	// UnityEngine.GameObject ExitGames.Demos.DemoAnimator.PlayerManager::LocalPlayerInstance
	GameObject_t1113636619 * ___LocalPlayerInstance_6;

public:
	inline static int32_t get_offset_of_LocalPlayerInstance_6() { return static_cast<int32_t>(offsetof(PlayerManager_t3964432985_StaticFields, ___LocalPlayerInstance_6)); }
	inline GameObject_t1113636619 * get_LocalPlayerInstance_6() const { return ___LocalPlayerInstance_6; }
	inline GameObject_t1113636619 ** get_address_of_LocalPlayerInstance_6() { return &___LocalPlayerInstance_6; }
	inline void set_LocalPlayerInstance_6(GameObject_t1113636619 * value)
	{
		___LocalPlayerInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___LocalPlayerInstance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMANAGER_T3964432985_H
#ifndef DEMOMECANIMGUI_T3514443486_H
#define DEMOMECANIMGUI_T3514443486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoMecanimGUI
struct  DemoMecanimGUI_t3514443486  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.GUISkin DemoMecanimGUI::Skin
	GUISkin_t1244372282 * ___Skin_3;
	// PhotonAnimatorView DemoMecanimGUI::m_AnimatorView
	PhotonAnimatorView_t3352472062 * ___m_AnimatorView_4;
	// UnityEngine.Animator DemoMecanimGUI::m_RemoteAnimator
	Animator_t434523843 * ___m_RemoteAnimator_5;
	// System.Single DemoMecanimGUI::m_SlideIn
	float ___m_SlideIn_6;
	// System.Single DemoMecanimGUI::m_FoundPlayerSlideIn
	float ___m_FoundPlayerSlideIn_7;
	// System.Boolean DemoMecanimGUI::m_IsOpen
	bool ___m_IsOpen_8;

public:
	inline static int32_t get_offset_of_Skin_3() { return static_cast<int32_t>(offsetof(DemoMecanimGUI_t3514443486, ___Skin_3)); }
	inline GUISkin_t1244372282 * get_Skin_3() const { return ___Skin_3; }
	inline GUISkin_t1244372282 ** get_address_of_Skin_3() { return &___Skin_3; }
	inline void set_Skin_3(GUISkin_t1244372282 * value)
	{
		___Skin_3 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_3), value);
	}

	inline static int32_t get_offset_of_m_AnimatorView_4() { return static_cast<int32_t>(offsetof(DemoMecanimGUI_t3514443486, ___m_AnimatorView_4)); }
	inline PhotonAnimatorView_t3352472062 * get_m_AnimatorView_4() const { return ___m_AnimatorView_4; }
	inline PhotonAnimatorView_t3352472062 ** get_address_of_m_AnimatorView_4() { return &___m_AnimatorView_4; }
	inline void set_m_AnimatorView_4(PhotonAnimatorView_t3352472062 * value)
	{
		___m_AnimatorView_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimatorView_4), value);
	}

	inline static int32_t get_offset_of_m_RemoteAnimator_5() { return static_cast<int32_t>(offsetof(DemoMecanimGUI_t3514443486, ___m_RemoteAnimator_5)); }
	inline Animator_t434523843 * get_m_RemoteAnimator_5() const { return ___m_RemoteAnimator_5; }
	inline Animator_t434523843 ** get_address_of_m_RemoteAnimator_5() { return &___m_RemoteAnimator_5; }
	inline void set_m_RemoteAnimator_5(Animator_t434523843 * value)
	{
		___m_RemoteAnimator_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_RemoteAnimator_5), value);
	}

	inline static int32_t get_offset_of_m_SlideIn_6() { return static_cast<int32_t>(offsetof(DemoMecanimGUI_t3514443486, ___m_SlideIn_6)); }
	inline float get_m_SlideIn_6() const { return ___m_SlideIn_6; }
	inline float* get_address_of_m_SlideIn_6() { return &___m_SlideIn_6; }
	inline void set_m_SlideIn_6(float value)
	{
		___m_SlideIn_6 = value;
	}

	inline static int32_t get_offset_of_m_FoundPlayerSlideIn_7() { return static_cast<int32_t>(offsetof(DemoMecanimGUI_t3514443486, ___m_FoundPlayerSlideIn_7)); }
	inline float get_m_FoundPlayerSlideIn_7() const { return ___m_FoundPlayerSlideIn_7; }
	inline float* get_address_of_m_FoundPlayerSlideIn_7() { return &___m_FoundPlayerSlideIn_7; }
	inline void set_m_FoundPlayerSlideIn_7(float value)
	{
		___m_FoundPlayerSlideIn_7 = value;
	}

	inline static int32_t get_offset_of_m_IsOpen_8() { return static_cast<int32_t>(offsetof(DemoMecanimGUI_t3514443486, ___m_IsOpen_8)); }
	inline bool get_m_IsOpen_8() const { return ___m_IsOpen_8; }
	inline bool* get_address_of_m_IsOpen_8() { return &___m_IsOpen_8; }
	inline void set_m_IsOpen_8(bool value)
	{
		___m_IsOpen_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOMECANIMGUI_T3514443486_H
#ifndef RPSCORE_T2154144697_H
#define RPSCORE_T2154144697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore
struct  RpsCore_t2154144697  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.RectTransform RpsCore::ConnectUiView
	RectTransform_t3704657025 * ___ConnectUiView_3;
	// UnityEngine.RectTransform RpsCore::GameUiView
	RectTransform_t3704657025 * ___GameUiView_4;
	// UnityEngine.CanvasGroup RpsCore::ButtonCanvasGroup
	CanvasGroup_t4083511760 * ___ButtonCanvasGroup_5;
	// UnityEngine.RectTransform RpsCore::TimerFillImage
	RectTransform_t3704657025 * ___TimerFillImage_6;
	// UnityEngine.UI.Text RpsCore::TurnText
	Text_t1901882714 * ___TurnText_7;
	// UnityEngine.UI.Text RpsCore::TimeText
	Text_t1901882714 * ___TimeText_8;
	// UnityEngine.UI.Text RpsCore::RemotePlayerText
	Text_t1901882714 * ___RemotePlayerText_9;
	// UnityEngine.UI.Text RpsCore::LocalPlayerText
	Text_t1901882714 * ___LocalPlayerText_10;
	// UnityEngine.UI.Image RpsCore::WinOrLossImage
	Image_t2670269651 * ___WinOrLossImage_11;
	// UnityEngine.UI.Image RpsCore::localSelectionImage
	Image_t2670269651 * ___localSelectionImage_12;
	// RpsCore/Hand RpsCore::localSelection
	int32_t ___localSelection_13;
	// UnityEngine.UI.Image RpsCore::remoteSelectionImage
	Image_t2670269651 * ___remoteSelectionImage_14;
	// RpsCore/Hand RpsCore::remoteSelection
	int32_t ___remoteSelection_15;
	// UnityEngine.Sprite RpsCore::SelectedRock
	Sprite_t280657092 * ___SelectedRock_16;
	// UnityEngine.Sprite RpsCore::SelectedPaper
	Sprite_t280657092 * ___SelectedPaper_17;
	// UnityEngine.Sprite RpsCore::SelectedScissors
	Sprite_t280657092 * ___SelectedScissors_18;
	// UnityEngine.Sprite RpsCore::SpriteWin
	Sprite_t280657092 * ___SpriteWin_19;
	// UnityEngine.Sprite RpsCore::SpriteLose
	Sprite_t280657092 * ___SpriteLose_20;
	// UnityEngine.Sprite RpsCore::SpriteDraw
	Sprite_t280657092 * ___SpriteDraw_21;
	// UnityEngine.RectTransform RpsCore::DisconnectedPanel
	RectTransform_t3704657025 * ___DisconnectedPanel_22;
	// RpsCore/ResultType RpsCore::result
	int32_t ___result_23;
	// PunTurnManager RpsCore::turnManager
	PunTurnManager_t1223962931 * ___turnManager_24;
	// RpsCore/Hand RpsCore::randomHand
	int32_t ___randomHand_25;
	// System.Boolean RpsCore::IsShowingResults
	bool ___IsShowingResults_26;

public:
	inline static int32_t get_offset_of_ConnectUiView_3() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___ConnectUiView_3)); }
	inline RectTransform_t3704657025 * get_ConnectUiView_3() const { return ___ConnectUiView_3; }
	inline RectTransform_t3704657025 ** get_address_of_ConnectUiView_3() { return &___ConnectUiView_3; }
	inline void set_ConnectUiView_3(RectTransform_t3704657025 * value)
	{
		___ConnectUiView_3 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectUiView_3), value);
	}

	inline static int32_t get_offset_of_GameUiView_4() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___GameUiView_4)); }
	inline RectTransform_t3704657025 * get_GameUiView_4() const { return ___GameUiView_4; }
	inline RectTransform_t3704657025 ** get_address_of_GameUiView_4() { return &___GameUiView_4; }
	inline void set_GameUiView_4(RectTransform_t3704657025 * value)
	{
		___GameUiView_4 = value;
		Il2CppCodeGenWriteBarrier((&___GameUiView_4), value);
	}

	inline static int32_t get_offset_of_ButtonCanvasGroup_5() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___ButtonCanvasGroup_5)); }
	inline CanvasGroup_t4083511760 * get_ButtonCanvasGroup_5() const { return ___ButtonCanvasGroup_5; }
	inline CanvasGroup_t4083511760 ** get_address_of_ButtonCanvasGroup_5() { return &___ButtonCanvasGroup_5; }
	inline void set_ButtonCanvasGroup_5(CanvasGroup_t4083511760 * value)
	{
		___ButtonCanvasGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCanvasGroup_5), value);
	}

	inline static int32_t get_offset_of_TimerFillImage_6() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___TimerFillImage_6)); }
	inline RectTransform_t3704657025 * get_TimerFillImage_6() const { return ___TimerFillImage_6; }
	inline RectTransform_t3704657025 ** get_address_of_TimerFillImage_6() { return &___TimerFillImage_6; }
	inline void set_TimerFillImage_6(RectTransform_t3704657025 * value)
	{
		___TimerFillImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___TimerFillImage_6), value);
	}

	inline static int32_t get_offset_of_TurnText_7() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___TurnText_7)); }
	inline Text_t1901882714 * get_TurnText_7() const { return ___TurnText_7; }
	inline Text_t1901882714 ** get_address_of_TurnText_7() { return &___TurnText_7; }
	inline void set_TurnText_7(Text_t1901882714 * value)
	{
		___TurnText_7 = value;
		Il2CppCodeGenWriteBarrier((&___TurnText_7), value);
	}

	inline static int32_t get_offset_of_TimeText_8() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___TimeText_8)); }
	inline Text_t1901882714 * get_TimeText_8() const { return ___TimeText_8; }
	inline Text_t1901882714 ** get_address_of_TimeText_8() { return &___TimeText_8; }
	inline void set_TimeText_8(Text_t1901882714 * value)
	{
		___TimeText_8 = value;
		Il2CppCodeGenWriteBarrier((&___TimeText_8), value);
	}

	inline static int32_t get_offset_of_RemotePlayerText_9() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___RemotePlayerText_9)); }
	inline Text_t1901882714 * get_RemotePlayerText_9() const { return ___RemotePlayerText_9; }
	inline Text_t1901882714 ** get_address_of_RemotePlayerText_9() { return &___RemotePlayerText_9; }
	inline void set_RemotePlayerText_9(Text_t1901882714 * value)
	{
		___RemotePlayerText_9 = value;
		Il2CppCodeGenWriteBarrier((&___RemotePlayerText_9), value);
	}

	inline static int32_t get_offset_of_LocalPlayerText_10() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___LocalPlayerText_10)); }
	inline Text_t1901882714 * get_LocalPlayerText_10() const { return ___LocalPlayerText_10; }
	inline Text_t1901882714 ** get_address_of_LocalPlayerText_10() { return &___LocalPlayerText_10; }
	inline void set_LocalPlayerText_10(Text_t1901882714 * value)
	{
		___LocalPlayerText_10 = value;
		Il2CppCodeGenWriteBarrier((&___LocalPlayerText_10), value);
	}

	inline static int32_t get_offset_of_WinOrLossImage_11() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___WinOrLossImage_11)); }
	inline Image_t2670269651 * get_WinOrLossImage_11() const { return ___WinOrLossImage_11; }
	inline Image_t2670269651 ** get_address_of_WinOrLossImage_11() { return &___WinOrLossImage_11; }
	inline void set_WinOrLossImage_11(Image_t2670269651 * value)
	{
		___WinOrLossImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___WinOrLossImage_11), value);
	}

	inline static int32_t get_offset_of_localSelectionImage_12() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___localSelectionImage_12)); }
	inline Image_t2670269651 * get_localSelectionImage_12() const { return ___localSelectionImage_12; }
	inline Image_t2670269651 ** get_address_of_localSelectionImage_12() { return &___localSelectionImage_12; }
	inline void set_localSelectionImage_12(Image_t2670269651 * value)
	{
		___localSelectionImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___localSelectionImage_12), value);
	}

	inline static int32_t get_offset_of_localSelection_13() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___localSelection_13)); }
	inline int32_t get_localSelection_13() const { return ___localSelection_13; }
	inline int32_t* get_address_of_localSelection_13() { return &___localSelection_13; }
	inline void set_localSelection_13(int32_t value)
	{
		___localSelection_13 = value;
	}

	inline static int32_t get_offset_of_remoteSelectionImage_14() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___remoteSelectionImage_14)); }
	inline Image_t2670269651 * get_remoteSelectionImage_14() const { return ___remoteSelectionImage_14; }
	inline Image_t2670269651 ** get_address_of_remoteSelectionImage_14() { return &___remoteSelectionImage_14; }
	inline void set_remoteSelectionImage_14(Image_t2670269651 * value)
	{
		___remoteSelectionImage_14 = value;
		Il2CppCodeGenWriteBarrier((&___remoteSelectionImage_14), value);
	}

	inline static int32_t get_offset_of_remoteSelection_15() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___remoteSelection_15)); }
	inline int32_t get_remoteSelection_15() const { return ___remoteSelection_15; }
	inline int32_t* get_address_of_remoteSelection_15() { return &___remoteSelection_15; }
	inline void set_remoteSelection_15(int32_t value)
	{
		___remoteSelection_15 = value;
	}

	inline static int32_t get_offset_of_SelectedRock_16() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SelectedRock_16)); }
	inline Sprite_t280657092 * get_SelectedRock_16() const { return ___SelectedRock_16; }
	inline Sprite_t280657092 ** get_address_of_SelectedRock_16() { return &___SelectedRock_16; }
	inline void set_SelectedRock_16(Sprite_t280657092 * value)
	{
		___SelectedRock_16 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedRock_16), value);
	}

	inline static int32_t get_offset_of_SelectedPaper_17() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SelectedPaper_17)); }
	inline Sprite_t280657092 * get_SelectedPaper_17() const { return ___SelectedPaper_17; }
	inline Sprite_t280657092 ** get_address_of_SelectedPaper_17() { return &___SelectedPaper_17; }
	inline void set_SelectedPaper_17(Sprite_t280657092 * value)
	{
		___SelectedPaper_17 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedPaper_17), value);
	}

	inline static int32_t get_offset_of_SelectedScissors_18() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SelectedScissors_18)); }
	inline Sprite_t280657092 * get_SelectedScissors_18() const { return ___SelectedScissors_18; }
	inline Sprite_t280657092 ** get_address_of_SelectedScissors_18() { return &___SelectedScissors_18; }
	inline void set_SelectedScissors_18(Sprite_t280657092 * value)
	{
		___SelectedScissors_18 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedScissors_18), value);
	}

	inline static int32_t get_offset_of_SpriteWin_19() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SpriteWin_19)); }
	inline Sprite_t280657092 * get_SpriteWin_19() const { return ___SpriteWin_19; }
	inline Sprite_t280657092 ** get_address_of_SpriteWin_19() { return &___SpriteWin_19; }
	inline void set_SpriteWin_19(Sprite_t280657092 * value)
	{
		___SpriteWin_19 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteWin_19), value);
	}

	inline static int32_t get_offset_of_SpriteLose_20() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SpriteLose_20)); }
	inline Sprite_t280657092 * get_SpriteLose_20() const { return ___SpriteLose_20; }
	inline Sprite_t280657092 ** get_address_of_SpriteLose_20() { return &___SpriteLose_20; }
	inline void set_SpriteLose_20(Sprite_t280657092 * value)
	{
		___SpriteLose_20 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteLose_20), value);
	}

	inline static int32_t get_offset_of_SpriteDraw_21() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SpriteDraw_21)); }
	inline Sprite_t280657092 * get_SpriteDraw_21() const { return ___SpriteDraw_21; }
	inline Sprite_t280657092 ** get_address_of_SpriteDraw_21() { return &___SpriteDraw_21; }
	inline void set_SpriteDraw_21(Sprite_t280657092 * value)
	{
		___SpriteDraw_21 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteDraw_21), value);
	}

	inline static int32_t get_offset_of_DisconnectedPanel_22() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___DisconnectedPanel_22)); }
	inline RectTransform_t3704657025 * get_DisconnectedPanel_22() const { return ___DisconnectedPanel_22; }
	inline RectTransform_t3704657025 ** get_address_of_DisconnectedPanel_22() { return &___DisconnectedPanel_22; }
	inline void set_DisconnectedPanel_22(RectTransform_t3704657025 * value)
	{
		___DisconnectedPanel_22 = value;
		Il2CppCodeGenWriteBarrier((&___DisconnectedPanel_22), value);
	}

	inline static int32_t get_offset_of_result_23() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___result_23)); }
	inline int32_t get_result_23() const { return ___result_23; }
	inline int32_t* get_address_of_result_23() { return &___result_23; }
	inline void set_result_23(int32_t value)
	{
		___result_23 = value;
	}

	inline static int32_t get_offset_of_turnManager_24() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___turnManager_24)); }
	inline PunTurnManager_t1223962931 * get_turnManager_24() const { return ___turnManager_24; }
	inline PunTurnManager_t1223962931 ** get_address_of_turnManager_24() { return &___turnManager_24; }
	inline void set_turnManager_24(PunTurnManager_t1223962931 * value)
	{
		___turnManager_24 = value;
		Il2CppCodeGenWriteBarrier((&___turnManager_24), value);
	}

	inline static int32_t get_offset_of_randomHand_25() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___randomHand_25)); }
	inline int32_t get_randomHand_25() const { return ___randomHand_25; }
	inline int32_t* get_address_of_randomHand_25() { return &___randomHand_25; }
	inline void set_randomHand_25(int32_t value)
	{
		___randomHand_25 = value;
	}

	inline static int32_t get_offset_of_IsShowingResults_26() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___IsShowingResults_26)); }
	inline bool get_IsShowingResults_26() const { return ___IsShowingResults_26; }
	inline bool* get_address_of_IsShowingResults_26() { return &___IsShowingResults_26; }
	inline void set_IsShowingResults_26(bool value)
	{
		___IsShowingResults_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPSCORE_T2154144697_H
#ifndef LAUNCHER_T2367134418_H
#define LAUNCHER_T2367134418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoAnimator.Launcher
struct  Launcher_t2367134418  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.GameObject ExitGames.Demos.DemoAnimator.Launcher::controlPanel
	GameObject_t1113636619 * ___controlPanel_3;
	// UnityEngine.UI.Text ExitGames.Demos.DemoAnimator.Launcher::feedbackText
	Text_t1901882714 * ___feedbackText_4;
	// System.Byte ExitGames.Demos.DemoAnimator.Launcher::maxPlayersPerRoom
	uint8_t ___maxPlayersPerRoom_5;
	// ExitGames.Demos.DemoAnimator.LoaderAnime ExitGames.Demos.DemoAnimator.Launcher::loaderAnime
	LoaderAnime_t64665300 * ___loaderAnime_6;
	// System.Boolean ExitGames.Demos.DemoAnimator.Launcher::isConnecting
	bool ___isConnecting_7;
	// System.String ExitGames.Demos.DemoAnimator.Launcher::_gameVersion
	String_t* ____gameVersion_8;

public:
	inline static int32_t get_offset_of_controlPanel_3() { return static_cast<int32_t>(offsetof(Launcher_t2367134418, ___controlPanel_3)); }
	inline GameObject_t1113636619 * get_controlPanel_3() const { return ___controlPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_controlPanel_3() { return &___controlPanel_3; }
	inline void set_controlPanel_3(GameObject_t1113636619 * value)
	{
		___controlPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___controlPanel_3), value);
	}

	inline static int32_t get_offset_of_feedbackText_4() { return static_cast<int32_t>(offsetof(Launcher_t2367134418, ___feedbackText_4)); }
	inline Text_t1901882714 * get_feedbackText_4() const { return ___feedbackText_4; }
	inline Text_t1901882714 ** get_address_of_feedbackText_4() { return &___feedbackText_4; }
	inline void set_feedbackText_4(Text_t1901882714 * value)
	{
		___feedbackText_4 = value;
		Il2CppCodeGenWriteBarrier((&___feedbackText_4), value);
	}

	inline static int32_t get_offset_of_maxPlayersPerRoom_5() { return static_cast<int32_t>(offsetof(Launcher_t2367134418, ___maxPlayersPerRoom_5)); }
	inline uint8_t get_maxPlayersPerRoom_5() const { return ___maxPlayersPerRoom_5; }
	inline uint8_t* get_address_of_maxPlayersPerRoom_5() { return &___maxPlayersPerRoom_5; }
	inline void set_maxPlayersPerRoom_5(uint8_t value)
	{
		___maxPlayersPerRoom_5 = value;
	}

	inline static int32_t get_offset_of_loaderAnime_6() { return static_cast<int32_t>(offsetof(Launcher_t2367134418, ___loaderAnime_6)); }
	inline LoaderAnime_t64665300 * get_loaderAnime_6() const { return ___loaderAnime_6; }
	inline LoaderAnime_t64665300 ** get_address_of_loaderAnime_6() { return &___loaderAnime_6; }
	inline void set_loaderAnime_6(LoaderAnime_t64665300 * value)
	{
		___loaderAnime_6 = value;
		Il2CppCodeGenWriteBarrier((&___loaderAnime_6), value);
	}

	inline static int32_t get_offset_of_isConnecting_7() { return static_cast<int32_t>(offsetof(Launcher_t2367134418, ___isConnecting_7)); }
	inline bool get_isConnecting_7() const { return ___isConnecting_7; }
	inline bool* get_address_of_isConnecting_7() { return &___isConnecting_7; }
	inline void set_isConnecting_7(bool value)
	{
		___isConnecting_7 = value;
	}

	inline static int32_t get_offset_of__gameVersion_8() { return static_cast<int32_t>(offsetof(Launcher_t2367134418, ____gameVersion_8)); }
	inline String_t* get__gameVersion_8() const { return ____gameVersion_8; }
	inline String_t** get_address_of__gameVersion_8() { return &____gameVersion_8; }
	inline void set__gameVersion_8(String_t* value)
	{
		____gameVersion_8 = value;
		Il2CppCodeGenWriteBarrier((&____gameVersion_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHER_T2367134418_H
#ifndef RPSDEMOCONNECT_T1717666331_H
#define RPSDEMOCONNECT_T1717666331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsDemoConnect
struct  RpsDemoConnect_t1717666331  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.UI.InputField RpsDemoConnect::InputField
	InputField_t3762917431 * ___InputField_3;
	// System.String RpsDemoConnect::UserId
	String_t* ___UserId_4;
	// System.String RpsDemoConnect::previousRoomPlayerPrefKey
	String_t* ___previousRoomPlayerPrefKey_5;
	// System.String RpsDemoConnect::previousRoom
	String_t* ___previousRoom_6;

public:
	inline static int32_t get_offset_of_InputField_3() { return static_cast<int32_t>(offsetof(RpsDemoConnect_t1717666331, ___InputField_3)); }
	inline InputField_t3762917431 * get_InputField_3() const { return ___InputField_3; }
	inline InputField_t3762917431 ** get_address_of_InputField_3() { return &___InputField_3; }
	inline void set_InputField_3(InputField_t3762917431 * value)
	{
		___InputField_3 = value;
		Il2CppCodeGenWriteBarrier((&___InputField_3), value);
	}

	inline static int32_t get_offset_of_UserId_4() { return static_cast<int32_t>(offsetof(RpsDemoConnect_t1717666331, ___UserId_4)); }
	inline String_t* get_UserId_4() const { return ___UserId_4; }
	inline String_t** get_address_of_UserId_4() { return &___UserId_4; }
	inline void set_UserId_4(String_t* value)
	{
		___UserId_4 = value;
		Il2CppCodeGenWriteBarrier((&___UserId_4), value);
	}

	inline static int32_t get_offset_of_previousRoomPlayerPrefKey_5() { return static_cast<int32_t>(offsetof(RpsDemoConnect_t1717666331, ___previousRoomPlayerPrefKey_5)); }
	inline String_t* get_previousRoomPlayerPrefKey_5() const { return ___previousRoomPlayerPrefKey_5; }
	inline String_t** get_address_of_previousRoomPlayerPrefKey_5() { return &___previousRoomPlayerPrefKey_5; }
	inline void set_previousRoomPlayerPrefKey_5(String_t* value)
	{
		___previousRoomPlayerPrefKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___previousRoomPlayerPrefKey_5), value);
	}

	inline static int32_t get_offset_of_previousRoom_6() { return static_cast<int32_t>(offsetof(RpsDemoConnect_t1717666331, ___previousRoom_6)); }
	inline String_t* get_previousRoom_6() const { return ___previousRoom_6; }
	inline String_t** get_address_of_previousRoom_6() { return &___previousRoom_6; }
	inline void set_previousRoom_6(String_t* value)
	{
		___previousRoom_6 = value;
		Il2CppCodeGenWriteBarrier((&___previousRoom_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPSDEMOCONNECT_T1717666331_H
#ifndef RANDOMMATCHMAKER_T1319082016_H
#define RANDOMMATCHMAKER_T1319082016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomMatchmaker
struct  RandomMatchmaker_t1319082016  : public PunBehaviour_t987309092
{
public:
	// PhotonView RandomMatchmaker::myPhotonView
	PhotonView_t2207721820 * ___myPhotonView_3;

public:
	inline static int32_t get_offset_of_myPhotonView_3() { return static_cast<int32_t>(offsetof(RandomMatchmaker_t1319082016, ___myPhotonView_3)); }
	inline PhotonView_t2207721820 * get_myPhotonView_3() const { return ___myPhotonView_3; }
	inline PhotonView_t2207721820 ** get_address_of_myPhotonView_3() { return &___myPhotonView_3; }
	inline void set_myPhotonView_3(PhotonView_t2207721820 * value)
	{
		___myPhotonView_3 = value;
		Il2CppCodeGenWriteBarrier((&___myPhotonView_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMMATCHMAKER_T1319082016_H
#ifndef COLORPERPLAYER_T432550946_H
#define COLORPERPLAYER_T432550946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPerPlayer
struct  ColorPerPlayer_t432550946  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.Color[] ColorPerPlayer::Colors
	ColorU5BU5D_t941916413* ___Colors_3;
	// System.Boolean ColorPerPlayer::ShowColorLabel
	bool ___ShowColorLabel_5;
	// UnityEngine.Rect ColorPerPlayer::ColorLabelArea
	Rect_t2360479859  ___ColorLabelArea_6;
	// UnityEngine.Texture2D ColorPerPlayer::img
	Texture2D_t3840446185 * ___img_7;
	// UnityEngine.Color ColorPerPlayer::MyColor
	Color_t2555686324  ___MyColor_8;
	// System.Boolean ColorPerPlayer::<ColorPicked>k__BackingField
	bool ___U3CColorPickedU3Ek__BackingField_9;
	// System.Boolean ColorPerPlayer::isInitialized
	bool ___isInitialized_10;

public:
	inline static int32_t get_offset_of_Colors_3() { return static_cast<int32_t>(offsetof(ColorPerPlayer_t432550946, ___Colors_3)); }
	inline ColorU5BU5D_t941916413* get_Colors_3() const { return ___Colors_3; }
	inline ColorU5BU5D_t941916413** get_address_of_Colors_3() { return &___Colors_3; }
	inline void set_Colors_3(ColorU5BU5D_t941916413* value)
	{
		___Colors_3 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_3), value);
	}

	inline static int32_t get_offset_of_ShowColorLabel_5() { return static_cast<int32_t>(offsetof(ColorPerPlayer_t432550946, ___ShowColorLabel_5)); }
	inline bool get_ShowColorLabel_5() const { return ___ShowColorLabel_5; }
	inline bool* get_address_of_ShowColorLabel_5() { return &___ShowColorLabel_5; }
	inline void set_ShowColorLabel_5(bool value)
	{
		___ShowColorLabel_5 = value;
	}

	inline static int32_t get_offset_of_ColorLabelArea_6() { return static_cast<int32_t>(offsetof(ColorPerPlayer_t432550946, ___ColorLabelArea_6)); }
	inline Rect_t2360479859  get_ColorLabelArea_6() const { return ___ColorLabelArea_6; }
	inline Rect_t2360479859 * get_address_of_ColorLabelArea_6() { return &___ColorLabelArea_6; }
	inline void set_ColorLabelArea_6(Rect_t2360479859  value)
	{
		___ColorLabelArea_6 = value;
	}

	inline static int32_t get_offset_of_img_7() { return static_cast<int32_t>(offsetof(ColorPerPlayer_t432550946, ___img_7)); }
	inline Texture2D_t3840446185 * get_img_7() const { return ___img_7; }
	inline Texture2D_t3840446185 ** get_address_of_img_7() { return &___img_7; }
	inline void set_img_7(Texture2D_t3840446185 * value)
	{
		___img_7 = value;
		Il2CppCodeGenWriteBarrier((&___img_7), value);
	}

	inline static int32_t get_offset_of_MyColor_8() { return static_cast<int32_t>(offsetof(ColorPerPlayer_t432550946, ___MyColor_8)); }
	inline Color_t2555686324  get_MyColor_8() const { return ___MyColor_8; }
	inline Color_t2555686324 * get_address_of_MyColor_8() { return &___MyColor_8; }
	inline void set_MyColor_8(Color_t2555686324  value)
	{
		___MyColor_8 = value;
	}

	inline static int32_t get_offset_of_U3CColorPickedU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ColorPerPlayer_t432550946, ___U3CColorPickedU3Ek__BackingField_9)); }
	inline bool get_U3CColorPickedU3Ek__BackingField_9() const { return ___U3CColorPickedU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CColorPickedU3Ek__BackingField_9() { return &___U3CColorPickedU3Ek__BackingField_9; }
	inline void set_U3CColorPickedU3Ek__BackingField_9(bool value)
	{
		___U3CColorPickedU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_isInitialized_10() { return static_cast<int32_t>(offsetof(ColorPerPlayer_t432550946, ___isInitialized_10)); }
	inline bool get_isInitialized_10() const { return ___isInitialized_10; }
	inline bool* get_address_of_isInitialized_10() { return &___isInitialized_10; }
	inline void set_isInitialized_10(bool value)
	{
		___isInitialized_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPERPLAYER_T432550946_H
#ifndef GAMEMANAGER_T3450760368_H
#define GAMEMANAGER_T3450760368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Demos.DemoAnimator.GameManager
struct  GameManager_t3450760368  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.GameObject ExitGames.Demos.DemoAnimator.GameManager::playerPrefab
	GameObject_t1113636619 * ___playerPrefab_4;
	// UnityEngine.GameObject ExitGames.Demos.DemoAnimator.GameManager::instance
	GameObject_t1113636619 * ___instance_5;

public:
	inline static int32_t get_offset_of_playerPrefab_4() { return static_cast<int32_t>(offsetof(GameManager_t3450760368, ___playerPrefab_4)); }
	inline GameObject_t1113636619 * get_playerPrefab_4() const { return ___playerPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_playerPrefab_4() { return &___playerPrefab_4; }
	inline void set_playerPrefab_4(GameObject_t1113636619 * value)
	{
		___playerPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerPrefab_4), value);
	}

	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(GameManager_t3450760368, ___instance_5)); }
	inline GameObject_t1113636619 * get_instance_5() const { return ___instance_5; }
	inline GameObject_t1113636619 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(GameObject_t1113636619 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

struct GameManager_t3450760368_StaticFields
{
public:
	// ExitGames.Demos.DemoAnimator.GameManager ExitGames.Demos.DemoAnimator.GameManager::Instance
	GameManager_t3450760368 * ___Instance_3;

public:
	inline static int32_t get_offset_of_Instance_3() { return static_cast<int32_t>(offsetof(GameManager_t3450760368_StaticFields, ___Instance_3)); }
	inline GameManager_t3450760368 * get_Instance_3() const { return ___Instance_3; }
	inline GameManager_t3450760368 ** get_address_of_Instance_3() { return &___Instance_3; }
	inline void set_Instance_3(GameManager_t3450760368 * value)
	{
		___Instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T3450760368_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[2] = 
{
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319::get_offset_of_player_0(),
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319::get_offset_of_eventId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[3] = 
{
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987::get_offset_of_player_0(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987::get_offset_of_type_1(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987::get_offset_of_msg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (Demo2DJumpAndRun_t894816729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (JumpAndRunMovement_t3103546220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[6] = 
{
	JumpAndRunMovement_t3103546220::get_offset_of_Speed_2(),
	JumpAndRunMovement_t3103546220::get_offset_of_JumpForce_3(),
	JumpAndRunMovement_t3103546220::get_offset_of_m_Animator_4(),
	JumpAndRunMovement_t3103546220::get_offset_of_m_Body_5(),
	JumpAndRunMovement_t3103546220::get_offset_of_m_PhotonView_6(),
	JumpAndRunMovement_t3103546220::get_offset_of_m_IsGrounded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (ColorPerPlayer_t432550946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[8] = 
{
	ColorPerPlayer_t432550946::get_offset_of_Colors_3(),
	0,
	ColorPerPlayer_t432550946::get_offset_of_ShowColorLabel_5(),
	ColorPerPlayer_t432550946::get_offset_of_ColorLabelArea_6(),
	ColorPerPlayer_t432550946::get_offset_of_img_7(),
	ColorPerPlayer_t432550946::get_offset_of_MyColor_8(),
	ColorPerPlayer_t432550946::get_offset_of_U3CColorPickedU3Ek__BackingField_9(),
	ColorPerPlayer_t432550946::get_offset_of_isInitialized_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (ColorPerPlayerApply_t3143695489), -1, sizeof(ColorPerPlayerApply_t3143695489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2605[3] = 
{
	ColorPerPlayerApply_t3143695489_StaticFields::get_offset_of_colorPickerCache_3(),
	ColorPerPlayerApply_t3143695489::get_offset_of_rendererComponent_4(),
	ColorPerPlayerApply_t3143695489::get_offset_of_isInitialized_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (DemoBoxesGui_t3999198613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[7] = 
{
	DemoBoxesGui_t3999198613::get_offset_of_HideUI_2(),
	DemoBoxesGui_t3999198613::get_offset_of_GuiTextForTips_3(),
	DemoBoxesGui_t3999198613::get_offset_of_tipsIndex_4(),
	DemoBoxesGui_t3999198613::get_offset_of_tips_5(),
	0,
	DemoBoxesGui_t3999198613::get_offset_of_timeSinceLastTip_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (U3CSwapTipU3Ec__Iterator0_t141855054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[5] = 
{
	U3CSwapTipU3Ec__Iterator0_t141855054::get_offset_of_U3CalphaU3E__0_0(),
	U3CSwapTipU3Ec__Iterator0_t141855054::get_offset_of_U24this_1(),
	U3CSwapTipU3Ec__Iterator0_t141855054::get_offset_of_U24current_2(),
	U3CSwapTipU3Ec__Iterator0_t141855054::get_offset_of_U24disposing_3(),
	U3CSwapTipU3Ec__Iterator0_t141855054::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (OnAwakePhysicsSettings_t2449268769), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (OnClickFlashRpc_t2987886900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[3] = 
{
	OnClickFlashRpc_t2987886900::get_offset_of_originalMaterial_3(),
	OnClickFlashRpc_t2987886900::get_offset_of_originalColor_4(),
	OnClickFlashRpc_t2987886900::get_offset_of_isFlashing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (U3CFlashU3Ec__Iterator0_t2775285351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2610[6] = 
{
	U3CFlashU3Ec__Iterator0_t2775285351::get_offset_of_U3CfU3E__1_0(),
	U3CFlashU3Ec__Iterator0_t2775285351::get_offset_of_U3ClerpedU3E__2_1(),
	U3CFlashU3Ec__Iterator0_t2775285351::get_offset_of_U24this_2(),
	U3CFlashU3Ec__Iterator0_t2775285351::get_offset_of_U24current_3(),
	U3CFlashU3Ec__Iterator0_t2775285351::get_offset_of_U24disposing_4(),
	U3CFlashU3Ec__Iterator0_t2775285351::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (OnDoubleclickDestroy_t3154303920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2611[2] = 
{
	OnDoubleclickDestroy_t3154303920::get_offset_of_timeOfLastClick_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (ClickAndDrag_t858977362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2612[3] = 
{
	ClickAndDrag_t858977362::get_offset_of_camOnPress_3(),
	ClickAndDrag_t858977362::get_offset_of_following_4(),
	ClickAndDrag_t858977362::get_offset_of_factor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (DemoOwnershipGui_t3404283064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[2] = 
{
	DemoOwnershipGui_t3404283064::get_offset_of_Skin_2(),
	DemoOwnershipGui_t3404283064::get_offset_of_TransferOwnershipOnRequest_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (HighlightOwnedGameObj_t513766295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[3] = 
{
	HighlightOwnedGameObj_t513766295::get_offset_of_PointerPrefab_3(),
	HighlightOwnedGameObj_t513766295::get_offset_of_Offset_4(),
	HighlightOwnedGameObj_t513766295::get_offset_of_markerTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (InstantiateCube_t1661801423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[3] = 
{
	InstantiateCube_t1661801423::get_offset_of_Prefab_2(),
	InstantiateCube_t1661801423::get_offset_of_InstantiateType_3(),
	InstantiateCube_t1661801423::get_offset_of_showGui_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (MaterialPerOwner_t3683227728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[2] = 
{
	MaterialPerOwner_t3683227728::get_offset_of_assignedColorForUserId_3(),
	MaterialPerOwner_t3683227728::get_offset_of_m_Renderer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (OnClickDisableObj_t3071582932), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (OnClickRequestOwnership_t1795600179), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (OnClickRightDestroy_t2880403765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (ShowInfoOfPlayer_t601303432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[5] = 
{
	ShowInfoOfPlayer_t601303432::get_offset_of_textGo_3(),
	ShowInfoOfPlayer_t601303432::get_offset_of_tm_4(),
	ShowInfoOfPlayer_t601303432::get_offset_of_CharacterSize_5(),
	ShowInfoOfPlayer_t601303432::get_offset_of_font_6(),
	ShowInfoOfPlayer_t601303432::get_offset_of_DisableOnOwnObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (ChannelSelector_t3480308131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[1] = 
{
	ChannelSelector_t3480308131::get_offset_of_Channel_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (ChatAppIdCheckerUI_t722438586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[1] = 
{
	ChatAppIdCheckerUI_t722438586::get_offset_of_Description_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (ChatGui_t3673337520), -1, sizeof(ChatGui_t3673337520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2623[23] = 
{
	ChatGui_t3673337520::get_offset_of_ChannelsToJoinOnConnect_2(),
	ChatGui_t3673337520::get_offset_of_FriendsList_3(),
	ChatGui_t3673337520::get_offset_of_HistoryLengthToFetch_4(),
	ChatGui_t3673337520::get_offset_of_U3CUserNameU3Ek__BackingField_5(),
	ChatGui_t3673337520::get_offset_of_selectedChannelName_6(),
	ChatGui_t3673337520::get_offset_of_chatClient_7(),
	ChatGui_t3673337520::get_offset_of_missingAppIdErrorPanel_8(),
	ChatGui_t3673337520::get_offset_of_ConnectingLabel_9(),
	ChatGui_t3673337520::get_offset_of_ChatPanel_10(),
	ChatGui_t3673337520::get_offset_of_UserIdFormPanel_11(),
	ChatGui_t3673337520::get_offset_of_InputFieldChat_12(),
	ChatGui_t3673337520::get_offset_of_CurrentChannelText_13(),
	ChatGui_t3673337520::get_offset_of_ChannelToggleToInstantiate_14(),
	ChatGui_t3673337520::get_offset_of_FriendListUiItemtoInstantiate_15(),
	ChatGui_t3673337520::get_offset_of_channelToggles_16(),
	ChatGui_t3673337520::get_offset_of_friendListItemLUT_17(),
	ChatGui_t3673337520::get_offset_of_ShowState_18(),
	ChatGui_t3673337520::get_offset_of_Title_19(),
	ChatGui_t3673337520::get_offset_of_StateText_20(),
	ChatGui_t3673337520::get_offset_of_UserIdText_21(),
	ChatGui_t3673337520_StaticFields::get_offset_of_HelpText_22(),
	ChatGui_t3673337520::get_offset_of_TestLength_23(),
	ChatGui_t3673337520::get_offset_of_testBytes_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (FriendItem_t2916710093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[3] = 
{
	FriendItem_t2916710093::get_offset_of_NameLabel_2(),
	FriendItem_t2916710093::get_offset_of_StatusLabel_3(),
	FriendItem_t2916710093::get_offset_of_Health_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (IgnoreUiRaycastWhenInactive_t3624510311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (NamePickGui_t2955268898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[3] = 
{
	0,
	NamePickGui_t2955268898::get_offset_of_chatNewComponent_3(),
	NamePickGui_t2955268898::get_offset_of_idInput_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (GUICustomAuth_t2390915668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[6] = 
{
	GUICustomAuth_t2390915668::get_offset_of_GuiRect_2(),
	GUICustomAuth_t2390915668::get_offset_of_authName_3(),
	GUICustomAuth_t2390915668::get_offset_of_authToken_4(),
	GUICustomAuth_t2390915668::get_offset_of_authDebugMessage_5(),
	GUICustomAuth_t2390915668::get_offset_of_guiState_6(),
	GUICustomAuth_t2390915668::get_offset_of_RootOf3dButtons_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (GuiState_t1902662126)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2628[5] = 
{
	GuiState_t1902662126::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (GUIFriendFinding_t537375819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[3] = 
{
	GUIFriendFinding_t537375819::get_offset_of_friendListOfSomeCommunity_2(),
	GUIFriendFinding_t537375819::get_offset_of_GuiRect_3(),
	GUIFriendFinding_t537375819::get_offset_of_ExpectedUsers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (GUIFriendsInRoom_t3253954278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[1] = 
{
	GUIFriendsInRoom_t3253954278::get_offset_of_GuiRect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (OnClickCallMethod_t3787258516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[2] = 
{
	OnClickCallMethod_t3787258516::get_offset_of_TargetGameObject_3(),
	OnClickCallMethod_t3787258516::get_offset_of_TargetMethod_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (HubGui_t2100725939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[6] = 
{
	HubGui_t2100725939::get_offset_of_Skin_2(),
	HubGui_t2100725939::get_offset_of_scrollPos_3(),
	HubGui_t2100725939::get_offset_of_demoDescription_4(),
	HubGui_t2100725939::get_offset_of_demoBtn_5(),
	HubGui_t2100725939::get_offset_of_webLink_6(),
	HubGui_t2100725939::get_offset_of_m_Headline_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (DemoBtn_t3232561033)+ sizeof (RuntimeObject), sizeof(DemoBtn_t3232561033_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2633[2] = 
{
	DemoBtn_t3232561033::get_offset_of_Text_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DemoBtn_t3232561033::get_offset_of_Link_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (MoveCam_t1672202783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[4] = 
{
	MoveCam_t1672202783::get_offset_of_originalPos_2(),
	MoveCam_t1672202783::get_offset_of_randomPos_3(),
	MoveCam_t1672202783::get_offset_of_camTransform_4(),
	MoveCam_t1672202783::get_offset_of_lookAt_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (DemoHubManager_t3958170276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2635[9] = 
{
	DemoHubManager_t3958170276::get_offset_of_TitleText_2(),
	DemoHubManager_t3958170276::get_offset_of_DescriptionText_3(),
	DemoHubManager_t3958170276::get_offset_of_OpenSceneButton_4(),
	DemoHubManager_t3958170276::get_offset_of_OpenTutorialLinkButton_5(),
	DemoHubManager_t3958170276::get_offset_of_OpenDocLinkButton_6(),
	DemoHubManager_t3958170276::get_offset_of_MainDemoWebLink_7(),
	DemoHubManager_t3958170276::get_offset_of__data_8(),
	DemoHubManager_t3958170276::get_offset_of_currentSelection_9(),
	DemoHubManager_t3958170276::get_offset_of_BugFixbounds_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (DemoData_t1209117524)+ sizeof (RuntimeObject), sizeof(DemoData_t1209117524_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2636[5] = 
{
	DemoData_t1209117524::get_offset_of_Title_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DemoData_t1209117524::get_offset_of_Description_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DemoData_t1209117524::get_offset_of_Scene_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DemoData_t1209117524::get_offset_of_TutorialLink_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DemoData_t1209117524::get_offset_of_DocLink_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (ToDemoHubButton_t117408609), -1, sizeof(ToDemoHubButton_t117408609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2637[2] = 
{
	ToDemoHubButton_t117408609_StaticFields::get_offset_of_instance_2(),
	ToDemoHubButton_t117408609::get_offset_of__canvasGroup_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (ToHubButton_t3593506886), -1, sizeof(ToHubButton_t3593506886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2638[3] = 
{
	ToHubButton_t3593506886::get_offset_of_ButtonTexture_2(),
	ToHubButton_t3593506886::get_offset_of_ButtonRect_3(),
	ToHubButton_t3593506886_StaticFields::get_offset_of_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (DemoMecanimGUI_t3514443486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2639[6] = 
{
	DemoMecanimGUI_t3514443486::get_offset_of_Skin_3(),
	DemoMecanimGUI_t3514443486::get_offset_of_m_AnimatorView_4(),
	DemoMecanimGUI_t3514443486::get_offset_of_m_RemoteAnimator_5(),
	DemoMecanimGUI_t3514443486::get_offset_of_m_SlideIn_6(),
	DemoMecanimGUI_t3514443486::get_offset_of_m_FoundPlayerSlideIn_7(),
	DemoMecanimGUI_t3514443486::get_offset_of_m_IsOpen_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (MessageOverlay_t775660691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[1] = 
{
	MessageOverlay_t775660691::get_offset_of_Objects_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (OnCollideSwitchTeam_t3835393565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[1] = 
{
	OnCollideSwitchTeam_t3835393565::get_offset_of_TeamToSwitchTo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (OnPickedUpScript_t4029004332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (PickupCamera_t3869569942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[19] = 
{
	PickupCamera_t3869569942::get_offset_of_cameraTransform_3(),
	PickupCamera_t3869569942::get_offset_of__target_4(),
	PickupCamera_t3869569942::get_offset_of_distance_5(),
	PickupCamera_t3869569942::get_offset_of_height_6(),
	PickupCamera_t3869569942::get_offset_of_angularSmoothLag_7(),
	PickupCamera_t3869569942::get_offset_of_angularMaxSpeed_8(),
	PickupCamera_t3869569942::get_offset_of_heightSmoothLag_9(),
	PickupCamera_t3869569942::get_offset_of_snapSmoothLag_10(),
	PickupCamera_t3869569942::get_offset_of_snapMaxSpeed_11(),
	PickupCamera_t3869569942::get_offset_of_clampHeadPositionScreenSpace_12(),
	PickupCamera_t3869569942::get_offset_of_lockCameraTimeout_13(),
	PickupCamera_t3869569942::get_offset_of_headOffset_14(),
	PickupCamera_t3869569942::get_offset_of_centerOffset_15(),
	PickupCamera_t3869569942::get_offset_of_heightVelocity_16(),
	PickupCamera_t3869569942::get_offset_of_angleVelocity_17(),
	PickupCamera_t3869569942::get_offset_of_snap_18(),
	PickupCamera_t3869569942::get_offset_of_controller_19(),
	PickupCamera_t3869569942::get_offset_of_targetHeight_20(),
	PickupCamera_t3869569942::get_offset_of_m_CameraTransformCamera_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (PickupCharacterState_t636162321)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2644[6] = 
{
	PickupCharacterState_t636162321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (PickupController_t2983846149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[45] = 
{
	PickupController_t2983846149::get_offset_of_idleAnimation_2(),
	PickupController_t2983846149::get_offset_of_walkAnimation_3(),
	PickupController_t2983846149::get_offset_of_runAnimation_4(),
	PickupController_t2983846149::get_offset_of_jumpPoseAnimation_5(),
	PickupController_t2983846149::get_offset_of_walkMaxAnimationSpeed_6(),
	PickupController_t2983846149::get_offset_of_trotMaxAnimationSpeed_7(),
	PickupController_t2983846149::get_offset_of_runMaxAnimationSpeed_8(),
	PickupController_t2983846149::get_offset_of_jumpAnimationSpeed_9(),
	PickupController_t2983846149::get_offset_of_landAnimationSpeed_10(),
	PickupController_t2983846149::get_offset_of__animation_11(),
	PickupController_t2983846149::get_offset_of__characterState_12(),
	PickupController_t2983846149::get_offset_of_walkSpeed_13(),
	PickupController_t2983846149::get_offset_of_trotSpeed_14(),
	PickupController_t2983846149::get_offset_of_runSpeed_15(),
	PickupController_t2983846149::get_offset_of_inAirControlAcceleration_16(),
	PickupController_t2983846149::get_offset_of_jumpHeight_17(),
	PickupController_t2983846149::get_offset_of_gravity_18(),
	PickupController_t2983846149::get_offset_of_speedSmoothing_19(),
	PickupController_t2983846149::get_offset_of_rotateSpeed_20(),
	PickupController_t2983846149::get_offset_of_trotAfterSeconds_21(),
	PickupController_t2983846149::get_offset_of_canJump_22(),
	PickupController_t2983846149::get_offset_of_jumpRepeatTime_23(),
	PickupController_t2983846149::get_offset_of_jumpTimeout_24(),
	PickupController_t2983846149::get_offset_of_groundedTimeout_25(),
	PickupController_t2983846149::get_offset_of_lockCameraTimer_26(),
	PickupController_t2983846149::get_offset_of_moveDirection_27(),
	PickupController_t2983846149::get_offset_of_verticalSpeed_28(),
	PickupController_t2983846149::get_offset_of_moveSpeed_29(),
	PickupController_t2983846149::get_offset_of_collisionFlags_30(),
	PickupController_t2983846149::get_offset_of_jumping_31(),
	PickupController_t2983846149::get_offset_of_jumpingReachedApex_32(),
	PickupController_t2983846149::get_offset_of_movingBack_33(),
	PickupController_t2983846149::get_offset_of_isMoving_34(),
	PickupController_t2983846149::get_offset_of_walkTimeStart_35(),
	PickupController_t2983846149::get_offset_of_lastJumpButtonTime_36(),
	PickupController_t2983846149::get_offset_of_lastJumpTime_37(),
	PickupController_t2983846149::get_offset_of_inAirVelocity_38(),
	PickupController_t2983846149::get_offset_of_lastGroundedTime_39(),
	PickupController_t2983846149::get_offset_of_velocity_40(),
	PickupController_t2983846149::get_offset_of_lastPos_41(),
	PickupController_t2983846149::get_offset_of_remotePosition_42(),
	PickupController_t2983846149::get_offset_of_isControllable_43(),
	PickupController_t2983846149::get_offset_of_DoRotate_44(),
	PickupController_t2983846149::get_offset_of_RemoteSmoothing_45(),
	PickupController_t2983846149::get_offset_of_AssignAsTagObject_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (PickupDemoGui_t2879317730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[4] = 
{
	PickupDemoGui_t2879317730::get_offset_of_ShowScores_2(),
	PickupDemoGui_t2879317730::get_offset_of_ShowDropButton_3(),
	PickupDemoGui_t2879317730::get_offset_of_ShowTeams_4(),
	PickupDemoGui_t2879317730::get_offset_of_DropOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (PickupTriggerForward_t166635472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (RpsCore_t2154144697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[24] = 
{
	RpsCore_t2154144697::get_offset_of_ConnectUiView_3(),
	RpsCore_t2154144697::get_offset_of_GameUiView_4(),
	RpsCore_t2154144697::get_offset_of_ButtonCanvasGroup_5(),
	RpsCore_t2154144697::get_offset_of_TimerFillImage_6(),
	RpsCore_t2154144697::get_offset_of_TurnText_7(),
	RpsCore_t2154144697::get_offset_of_TimeText_8(),
	RpsCore_t2154144697::get_offset_of_RemotePlayerText_9(),
	RpsCore_t2154144697::get_offset_of_LocalPlayerText_10(),
	RpsCore_t2154144697::get_offset_of_WinOrLossImage_11(),
	RpsCore_t2154144697::get_offset_of_localSelectionImage_12(),
	RpsCore_t2154144697::get_offset_of_localSelection_13(),
	RpsCore_t2154144697::get_offset_of_remoteSelectionImage_14(),
	RpsCore_t2154144697::get_offset_of_remoteSelection_15(),
	RpsCore_t2154144697::get_offset_of_SelectedRock_16(),
	RpsCore_t2154144697::get_offset_of_SelectedPaper_17(),
	RpsCore_t2154144697::get_offset_of_SelectedScissors_18(),
	RpsCore_t2154144697::get_offset_of_SpriteWin_19(),
	RpsCore_t2154144697::get_offset_of_SpriteLose_20(),
	RpsCore_t2154144697::get_offset_of_SpriteDraw_21(),
	RpsCore_t2154144697::get_offset_of_DisconnectedPanel_22(),
	RpsCore_t2154144697::get_offset_of_result_23(),
	RpsCore_t2154144697::get_offset_of_turnManager_24(),
	RpsCore_t2154144697::get_offset_of_randomHand_25(),
	RpsCore_t2154144697::get_offset_of_IsShowingResults_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (Hand_t2784208099)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2649[5] = 
{
	Hand_t2784208099::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (ResultType_t940288093)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2650[5] = 
{
	ResultType_t940288093::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[4] = 
{
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197::get_offset_of_U24this_0(),
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197::get_offset_of_U24current_1(),
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197::get_offset_of_U24disposing_2(),
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[4] = 
{
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456::get_offset_of_U24this_0(),
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456::get_offset_of_U24current_1(),
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456::get_offset_of_U24disposing_2(),
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (RpsDebug_t2473089555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[2] = 
{
	RpsDebug_t2473089555::get_offset_of_ConnectionDebugButton_2(),
	RpsDebug_t2473089555::get_offset_of_ShowConnectionDebug_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (RpsDemoConnect_t1717666331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[6] = 
{
	RpsDemoConnect_t1717666331::get_offset_of_InputField_3(),
	RpsDemoConnect_t1717666331::get_offset_of_UserId_4(),
	RpsDemoConnect_t1717666331::get_offset_of_previousRoomPlayerPrefKey_5(),
	RpsDemoConnect_t1717666331::get_offset_of_previousRoom_6(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (DemoRPGMovement_t3459243107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[1] = 
{
	DemoRPGMovement_t3459243107::get_offset_of_Camera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (RPGCamera_t2077525570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[9] = 
{
	RPGCamera_t2077525570::get_offset_of_Target_2(),
	RPGCamera_t2077525570::get_offset_of_MaximumDistance_3(),
	RPGCamera_t2077525570::get_offset_of_MinimumDistance_4(),
	RPGCamera_t2077525570::get_offset_of_ScrollModifier_5(),
	RPGCamera_t2077525570::get_offset_of_TurnModifier_6(),
	RPGCamera_t2077525570::get_offset_of_m_CameraTransform_7(),
	RPGCamera_t2077525570::get_offset_of_m_LookAtPoint_8(),
	RPGCamera_t2077525570::get_offset_of_m_LocalForwardVector_9(),
	RPGCamera_t2077525570::get_offset_of_m_Distance_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (RPGMovement_t3706718442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[12] = 
{
	RPGMovement_t3706718442::get_offset_of_ForwardSpeed_2(),
	RPGMovement_t3706718442::get_offset_of_BackwardSpeed_3(),
	RPGMovement_t3706718442::get_offset_of_StrafeSpeed_4(),
	RPGMovement_t3706718442::get_offset_of_RotateSpeed_5(),
	RPGMovement_t3706718442::get_offset_of_m_CharacterController_6(),
	RPGMovement_t3706718442::get_offset_of_m_LastPosition_7(),
	RPGMovement_t3706718442::get_offset_of_m_Animator_8(),
	RPGMovement_t3706718442::get_offset_of_m_PhotonView_9(),
	RPGMovement_t3706718442::get_offset_of_m_TransformView_10(),
	RPGMovement_t3706718442::get_offset_of_m_AnimatorSpeed_11(),
	RPGMovement_t3706718442::get_offset_of_m_CurrentMovement_12(),
	RPGMovement_t3706718442::get_offset_of_m_CurrentTurnSpeed_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (CubeExtra_t2395085633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[5] = 
{
	CubeExtra_t2395085633::get_offset_of_Factor_3(),
	CubeExtra_t2395085633::get_offset_of_latestCorrectPos_4(),
	CubeExtra_t2395085633::get_offset_of_movementVector_5(),
	CubeExtra_t2395085633::get_offset_of_errorVector_6(),
	CubeExtra_t2395085633::get_offset_of_lastTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (CubeInter_t3475839432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[3] = 
{
	CubeInter_t3475839432::get_offset_of_m_BufferedState_3(),
	CubeInter_t3475839432::get_offset_of_m_TimestampCount_4(),
	CubeInter_t3475839432::get_offset_of_InterpolationDelay_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (State_t1609032204)+ sizeof (RuntimeObject), sizeof(State_t1609032204 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2660[3] = 
{
	State_t1609032204::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	State_t1609032204::get_offset_of_pos_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	State_t1609032204::get_offset_of_rot_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (CubeLerp_t1035047565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[3] = 
{
	CubeLerp_t1035047565::get_offset_of_latestCorrectPos_3(),
	CubeLerp_t1035047565::get_offset_of_onUpdatePos_4(),
	CubeLerp_t1035047565::get_offset_of_fraction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (DragToMove_t3834199052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[7] = 
{
	DragToMove_t3834199052::get_offset_of_speed_2(),
	DragToMove_t3834199052::get_offset_of_cubes_3(),
	DragToMove_t3834199052::get_offset_of_PositionsQueue_4(),
	DragToMove_t3834199052::get_offset_of_cubeStartPositions_5(),
	DragToMove_t3834199052::get_offset_of_nextPosIndex_6(),
	DragToMove_t3834199052::get_offset_of_lerpTime_7(),
	DragToMove_t3834199052::get_offset_of_recording_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (U3CRecordMouseU3Ec__Iterator0_t1035515074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[7] = 
{
	U3CRecordMouseU3Ec__Iterator0_t1035515074::get_offset_of_U3Cv3U3E__1_0(),
	U3CRecordMouseU3Ec__Iterator0_t1035515074::get_offset_of_U3CinputRayU3E__1_1(),
	U3CRecordMouseU3Ec__Iterator0_t1035515074::get_offset_of_U3ChitU3E__1_2(),
	U3CRecordMouseU3Ec__Iterator0_t1035515074::get_offset_of_U24this_3(),
	U3CRecordMouseU3Ec__Iterator0_t1035515074::get_offset_of_U24current_4(),
	U3CRecordMouseU3Ec__Iterator0_t1035515074::get_offset_of_U24disposing_5(),
	U3CRecordMouseU3Ec__Iterator0_t1035515074::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (IELdemo_t899827249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[1] = 
{
	IELdemo_t899827249::get_offset_of_Skin_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (ThirdPersonCamera_t2998681409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[19] = 
{
	ThirdPersonCamera_t2998681409::get_offset_of_cameraTransform_2(),
	ThirdPersonCamera_t2998681409::get_offset_of__target_3(),
	ThirdPersonCamera_t2998681409::get_offset_of_distance_4(),
	ThirdPersonCamera_t2998681409::get_offset_of_height_5(),
	ThirdPersonCamera_t2998681409::get_offset_of_angularSmoothLag_6(),
	ThirdPersonCamera_t2998681409::get_offset_of_angularMaxSpeed_7(),
	ThirdPersonCamera_t2998681409::get_offset_of_heightSmoothLag_8(),
	ThirdPersonCamera_t2998681409::get_offset_of_snapSmoothLag_9(),
	ThirdPersonCamera_t2998681409::get_offset_of_snapMaxSpeed_10(),
	ThirdPersonCamera_t2998681409::get_offset_of_clampHeadPositionScreenSpace_11(),
	ThirdPersonCamera_t2998681409::get_offset_of_lockCameraTimeout_12(),
	ThirdPersonCamera_t2998681409::get_offset_of_headOffset_13(),
	ThirdPersonCamera_t2998681409::get_offset_of_centerOffset_14(),
	ThirdPersonCamera_t2998681409::get_offset_of_heightVelocity_15(),
	ThirdPersonCamera_t2998681409::get_offset_of_angleVelocity_16(),
	ThirdPersonCamera_t2998681409::get_offset_of_snap_17(),
	ThirdPersonCamera_t2998681409::get_offset_of_controller_18(),
	ThirdPersonCamera_t2998681409::get_offset_of_targetHeight_19(),
	ThirdPersonCamera_t2998681409::get_offset_of_m_CameraTransformCamera_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (CharacterState_t1684161606)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2666[6] = 
{
	CharacterState_t1684161606::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (ThirdPersonController_t2544474708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[41] = 
{
	ThirdPersonController_t2544474708::get_offset_of_idleAnimation_2(),
	ThirdPersonController_t2544474708::get_offset_of_walkAnimation_3(),
	ThirdPersonController_t2544474708::get_offset_of_runAnimation_4(),
	ThirdPersonController_t2544474708::get_offset_of_jumpPoseAnimation_5(),
	ThirdPersonController_t2544474708::get_offset_of_walkMaxAnimationSpeed_6(),
	ThirdPersonController_t2544474708::get_offset_of_trotMaxAnimationSpeed_7(),
	ThirdPersonController_t2544474708::get_offset_of_runMaxAnimationSpeed_8(),
	ThirdPersonController_t2544474708::get_offset_of_jumpAnimationSpeed_9(),
	ThirdPersonController_t2544474708::get_offset_of_landAnimationSpeed_10(),
	ThirdPersonController_t2544474708::get_offset_of__animation_11(),
	ThirdPersonController_t2544474708::get_offset_of__characterState_12(),
	ThirdPersonController_t2544474708::get_offset_of_walkSpeed_13(),
	ThirdPersonController_t2544474708::get_offset_of_trotSpeed_14(),
	ThirdPersonController_t2544474708::get_offset_of_runSpeed_15(),
	ThirdPersonController_t2544474708::get_offset_of_inAirControlAcceleration_16(),
	ThirdPersonController_t2544474708::get_offset_of_jumpHeight_17(),
	ThirdPersonController_t2544474708::get_offset_of_gravity_18(),
	ThirdPersonController_t2544474708::get_offset_of_speedSmoothing_19(),
	ThirdPersonController_t2544474708::get_offset_of_rotateSpeed_20(),
	ThirdPersonController_t2544474708::get_offset_of_trotAfterSeconds_21(),
	ThirdPersonController_t2544474708::get_offset_of_canJump_22(),
	ThirdPersonController_t2544474708::get_offset_of_jumpRepeatTime_23(),
	ThirdPersonController_t2544474708::get_offset_of_jumpTimeout_24(),
	ThirdPersonController_t2544474708::get_offset_of_groundedTimeout_25(),
	ThirdPersonController_t2544474708::get_offset_of_lockCameraTimer_26(),
	ThirdPersonController_t2544474708::get_offset_of_moveDirection_27(),
	ThirdPersonController_t2544474708::get_offset_of_verticalSpeed_28(),
	ThirdPersonController_t2544474708::get_offset_of_moveSpeed_29(),
	ThirdPersonController_t2544474708::get_offset_of_collisionFlags_30(),
	ThirdPersonController_t2544474708::get_offset_of_jumping_31(),
	ThirdPersonController_t2544474708::get_offset_of_jumpingReachedApex_32(),
	ThirdPersonController_t2544474708::get_offset_of_movingBack_33(),
	ThirdPersonController_t2544474708::get_offset_of_isMoving_34(),
	ThirdPersonController_t2544474708::get_offset_of_walkTimeStart_35(),
	ThirdPersonController_t2544474708::get_offset_of_lastJumpButtonTime_36(),
	ThirdPersonController_t2544474708::get_offset_of_lastJumpTime_37(),
	ThirdPersonController_t2544474708::get_offset_of_inAirVelocity_38(),
	ThirdPersonController_t2544474708::get_offset_of_lastGroundedTime_39(),
	ThirdPersonController_t2544474708::get_offset_of_isControllable_40(),
	ThirdPersonController_t2544474708::get_offset_of_lastPos_41(),
	ThirdPersonController_t2544474708::get_offset_of_velocity_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (ThirdPersonNetwork_t3674108340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[5] = 
{
	ThirdPersonNetwork_t3674108340::get_offset_of_cameraScript_3(),
	ThirdPersonNetwork_t3674108340::get_offset_of_controllerScript_4(),
	ThirdPersonNetwork_t3674108340::get_offset_of_firstTake_5(),
	ThirdPersonNetwork_t3674108340::get_offset_of_correctPlayerPos_6(),
	ThirdPersonNetwork_t3674108340::get_offset_of_correctPlayerRot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (WorkerInGame_t3415221707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[1] = 
{
	WorkerInGame_t3415221707::get_offset_of_playerPrefab_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (WorkerMenu_t663890918), -1, sizeof(WorkerMenu_t663890918_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2670[9] = 
{
	WorkerMenu_t663890918::get_offset_of_Skin_2(),
	WorkerMenu_t663890918::get_offset_of_WidthAndHeight_3(),
	WorkerMenu_t663890918::get_offset_of_roomName_4(),
	WorkerMenu_t663890918::get_offset_of_scrollPos_5(),
	WorkerMenu_t663890918::get_offset_of_connectFailed_6(),
	WorkerMenu_t663890918_StaticFields::get_offset_of_SceneNameMenu_7(),
	WorkerMenu_t663890918_StaticFields::get_offset_of_SceneNameGame_8(),
	WorkerMenu_t663890918::get_offset_of_errorDialog_9(),
	WorkerMenu_t663890918::get_offset_of_timeToClearDialog_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (AudioRpc_t2629069994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[3] = 
{
	AudioRpc_t2629069994::get_offset_of_marco_3(),
	AudioRpc_t2629069994::get_offset_of_polo_4(),
	AudioRpc_t2629069994::get_offset_of_m_Source_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (ClickDetector_t4035850562), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (GameLogic_t3731221617), -1, sizeof(GameLogic_t3731221617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2673[2] = 
{
	GameLogic_t3731221617_StaticFields::get_offset_of_playerWhoIsIt_2(),
	GameLogic_t3731221617_StaticFields::get_offset_of_ScenePhotonView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (myThirdPersonController_t1931332062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (NetworkCharacter_t2672613317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[2] = 
{
	NetworkCharacter_t2672613317::get_offset_of_correctPlayerPos_3(),
	NetworkCharacter_t2672613317::get_offset_of_correctPlayerRot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (OnClickLoadSomething_t137357684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[2] = 
{
	OnClickLoadSomething_t137357684::get_offset_of_ResourceTypeToLoad_2(),
	OnClickLoadSomething_t137357684::get_offset_of_ResourceToLoad_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (ResourceTypeOption_t3060300527)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2677[3] = 
{
	ResourceTypeOption_t3060300527::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (RandomMatchmaker_t1319082016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[1] = 
{
	RandomMatchmaker_t1319082016::get_offset_of_myPhotonView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (CameraWork_t2164329516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[9] = 
{
	CameraWork_t2164329516::get_offset_of_distance_2(),
	CameraWork_t2164329516::get_offset_of_height_3(),
	CameraWork_t2164329516::get_offset_of_heightSmoothLag_4(),
	CameraWork_t2164329516::get_offset_of_centerOffset_5(),
	CameraWork_t2164329516::get_offset_of_followOnStart_6(),
	CameraWork_t2164329516::get_offset_of_cameraTransform_7(),
	CameraWork_t2164329516::get_offset_of_isFollowing_8(),
	CameraWork_t2164329516::get_offset_of_heightVelocity_9(),
	CameraWork_t2164329516::get_offset_of_targetHeight_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (GameManager_t3450760368), -1, sizeof(GameManager_t3450760368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2680[3] = 
{
	GameManager_t3450760368_StaticFields::get_offset_of_Instance_3(),
	GameManager_t3450760368::get_offset_of_playerPrefab_4(),
	GameManager_t3450760368::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (Launcher_t2367134418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[6] = 
{
	Launcher_t2367134418::get_offset_of_controlPanel_3(),
	Launcher_t2367134418::get_offset_of_feedbackText_4(),
	Launcher_t2367134418::get_offset_of_maxPlayersPerRoom_5(),
	Launcher_t2367134418::get_offset_of_loaderAnime_6(),
	Launcher_t2367134418::get_offset_of_isConnecting_7(),
	Launcher_t2367134418::get_offset_of__gameVersion_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (LoaderAnime_t64665300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[7] = 
{
	LoaderAnime_t64665300::get_offset_of_speed_2(),
	LoaderAnime_t64665300::get_offset_of_radius_3(),
	LoaderAnime_t64665300::get_offset_of_particles_4(),
	LoaderAnime_t64665300::get_offset_of__offset_5(),
	LoaderAnime_t64665300::get_offset_of__transform_6(),
	LoaderAnime_t64665300::get_offset_of__particleTransform_7(),
	LoaderAnime_t64665300::get_offset_of__isAnimating_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (PlayerAnimatorManager_t2952684908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[2] = 
{
	PlayerAnimatorManager_t2952684908::get_offset_of_DirectionDampTime_3(),
	PlayerAnimatorManager_t2952684908::get_offset_of_animator_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (PlayerManager_t3964432985), -1, sizeof(PlayerManager_t3964432985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2684[5] = 
{
	PlayerManager_t3964432985::get_offset_of_PlayerUiPrefab_3(),
	PlayerManager_t3964432985::get_offset_of_Beams_4(),
	PlayerManager_t3964432985::get_offset_of_Health_5(),
	PlayerManager_t3964432985_StaticFields::get_offset_of_LocalPlayerInstance_6(),
	PlayerManager_t3964432985::get_offset_of_IsFiring_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (PlayerNameInputField_t21913183), -1, sizeof(PlayerNameInputField_t21913183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2685[1] = 
{
	PlayerNameInputField_t21913183_StaticFields::get_offset_of_playerNamePrefKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (PlayerUI_t3173905257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[8] = 
{
	PlayerUI_t3173905257::get_offset_of_ScreenOffset_2(),
	PlayerUI_t3173905257::get_offset_of_PlayerNameText_3(),
	PlayerUI_t3173905257::get_offset_of_PlayerHealthSlider_4(),
	PlayerUI_t3173905257::get_offset_of__target_5(),
	PlayerUI_t3173905257::get_offset_of__characterControllerHeight_6(),
	PlayerUI_t3173905257::get_offset_of__targetTransform_7(),
	PlayerUI_t3173905257::get_offset_of__targetRenderer_8(),
	PlayerUI_t3173905257::get_offset_of__targetPosition_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (IdleRunJump_t572119292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[10] = 
{
	IdleRunJump_t572119292::get_offset_of_animator_2(),
	IdleRunJump_t572119292::get_offset_of_DirectionDampTime_3(),
	IdleRunJump_t572119292::get_offset_of_ApplyGravity_4(),
	IdleRunJump_t572119292::get_offset_of_SynchronizedMaxSpeed_5(),
	IdleRunJump_t572119292::get_offset_of_TurnSpeedModifier_6(),
	IdleRunJump_t572119292::get_offset_of_SynchronizedTurnSpeed_7(),
	IdleRunJump_t572119292::get_offset_of_SynchronizedSpeedAcceleration_8(),
	IdleRunJump_t572119292::get_offset_of_m_PhotonView_9(),
	IdleRunJump_t572119292::get_offset_of_m_TransformView_10(),
	IdleRunJump_t572119292::get_offset_of_m_SpeedModifier_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (PlayerDiamond_t4061521841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[6] = 
{
	PlayerDiamond_t4061521841::get_offset_of_HeadTransform_2(),
	PlayerDiamond_t4061521841::get_offset_of_HeightOffset_3(),
	PlayerDiamond_t4061521841::get_offset_of_m_PhotonView_4(),
	PlayerDiamond_t4061521841::get_offset_of_m_DiamondRenderer_5(),
	PlayerDiamond_t4061521841::get_offset_of_m_Rotation_6(),
	PlayerDiamond_t4061521841::get_offset_of_m_Height_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (CustomTypes_t2914914968), -1, sizeof(CustomTypes_t2914914968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2689[12] = 
{
	CustomTypes_t2914914968_StaticFields::get_offset_of_memVector3_0(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_memVector2_1(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_memQuarternion_2(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_memPlayer_3(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_6(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_7(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_8(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_9(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_10(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (PhotonNetworkingMessage_t1476457985)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2690[31] = 
{
	PhotonNetworkingMessage_t1476457985::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (PhotonLogLevel_t4226222036)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2691[4] = 
{
	PhotonLogLevel_t4226222036::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (PhotonTargets_t2730697525)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2692[8] = 
{
	PhotonTargets_t2730697525::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (CloudRegionCode_t1925019500)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2693[13] = 
{
	CloudRegionCode_t1925019500::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (CloudRegionFlag_t3756941471)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2694[12] = 
{
	CloudRegionFlag_t3756941471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (ConnectionState_t836644691)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2695[6] = 
{
	ConnectionState_t836644691::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (EncryptionMode_t4213192103)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2696[3] = 
{
	EncryptionMode_t4213192103::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (EncryptionDataParameters_t25380775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (Extensions_t2612146612), -1, sizeof(Extensions_t2612146612_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2698[1] = 
{
	Extensions_t2612146612_StaticFields::get_offset_of_ParametersOfMethods_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (GameObjectExtensions_t182180175), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
