﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Func`1<System.Boolean>
struct Func_1_t3822001908;
// System.Random
struct Random_t108471755;
// System.Collections.Generic.List`1<System.Threading.Thread>
struct List_1_t3772910811;
// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate
struct IntegerMillisecondsDelegate_t651311252;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// Photon.SocketServer.Numeric.BigInteger
struct BigInteger_t956758543;
// System.Security.Cryptography.Rijndael
struct Rijndael_t2986313634;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t701940803;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t1884415901;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t768590915;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t3913627115;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t2311174851;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t3841783507;
// ExitGames.Client.Photon.NCommand
struct NCommand_t1230688399;
// ExitGames.Client.Photon.EnetPeer
struct EnetPeer_t430442630;
// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1
struct U3CU3Ec__DisplayClass56_1_t3313335349;
// ExitGames.Client.Photon.TPeer
struct TPeer_t1497954812;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1405253484;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1273022909;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.String,Google.ProtocolBuffers.IGeneratedExtensionLite>>
struct Dictionary_2_t3107168633;
// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>
struct Dictionary_2_t294577696;
// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.NCommand>
struct Dictionary_2_t119401730;
// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>
struct Queue_1_t1076947893;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t892470886;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4209139644;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t3662770472;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t2956237011;
// ExitGames.Client.Photon.PhotonPeer
struct PhotonPeer_t1608153861;
// ExitGames.Client.Photon.IProtocol
struct IProtocol_t1394662050;
// ExitGames.Client.Photon.IPhotonSocket
struct IPhotonSocket_t2066969247;
// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>
struct Queue_1_t2309151397;
// Photon.SocketServer.Security.ICryptoProvider
struct ICryptoProvider_t1662250179;
// System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>
struct LinkedList_1_t1884284488;
// ExitGames.Client.Photon.NetworkSimulationSet
struct NetworkSimulationSet_t2000596048;
// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem>
struct Queue_1_t4063950034;
// ExitGames.Client.Photon.StreamBuffer
struct StreamBuffer_t3827669789;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type>
struct Dictionary_2_t1253839074;
// System.Type
struct Type_t;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t2581629031;
// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t1302902347;
// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t4013908777;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// ExitGames.Client.Photon.EncryptorManaged.Encryptor
struct Encryptor_t200327285;
// ExitGames.Client.Photon.EncryptorManaged.Decryptor
struct Decryptor_t2116099858;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// System.Collections.Generic.List`1<ExitGames.Client.Photon.NCommand>
struct List_1_t2702763141;
// ExitGames.Client.Photon.EnetChannel[]
struct EnetChannelU5BU5D_t2709601441;
// System.Collections.Generic.Queue`1<System.Int32>
struct Queue_1_t2797205247;
// System.Collections.Generic.Queue`1<System.Byte[]>
struct Queue_1_t3962907151;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1293755103;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef U3CU3EC_T356392828_H
#define U3CU3EC_T356392828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass/<>c
struct  U3CU3Ec_t356392828  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t356392828_StaticFields
{
public:
	// ExitGames.Client.Photon.SupportClass/<>c ExitGames.Client.Photon.SupportClass/<>c::<>9
	U3CU3Ec_t356392828 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t356392828_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t356392828 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t356392828 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t356392828 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T356392828_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T926758450_H
#define U3CU3EC__DISPLAYCLASS7_0_T926758450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t926758450  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.SupportClass/<>c__DisplayClass7_0::millisecondsInterval
	int32_t ___millisecondsInterval_0;
	// System.Func`1<System.Boolean> ExitGames.Client.Photon.SupportClass/<>c__DisplayClass7_0::myThread
	Func_1_t3822001908 * ___myThread_1;

public:
	inline static int32_t get_offset_of_millisecondsInterval_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t926758450, ___millisecondsInterval_0)); }
	inline int32_t get_millisecondsInterval_0() const { return ___millisecondsInterval_0; }
	inline int32_t* get_address_of_millisecondsInterval_0() { return &___millisecondsInterval_0; }
	inline void set_millisecondsInterval_0(int32_t value)
	{
		___millisecondsInterval_0 = value;
	}

	inline static int32_t get_offset_of_myThread_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t926758450, ___myThread_1)); }
	inline Func_1_t3822001908 * get_myThread_1() const { return ___myThread_1; }
	inline Func_1_t3822001908 ** get_address_of_myThread_1() { return &___myThread_1; }
	inline void set_myThread_1(Func_1_t3822001908 * value)
	{
		___myThread_1 = value;
		Il2CppCodeGenWriteBarrier((&___myThread_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T926758450_H
#ifndef THREADSAFERANDOM_T1204416265_H
#define THREADSAFERANDOM_T1204416265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass/ThreadSafeRandom
struct  ThreadSafeRandom_t1204416265  : public RuntimeObject
{
public:

public:
};

struct ThreadSafeRandom_t1204416265_StaticFields
{
public:
	// System.Random ExitGames.Client.Photon.SupportClass/ThreadSafeRandom::_r
	Random_t108471755 * ____r_0;

public:
	inline static int32_t get_offset_of__r_0() { return static_cast<int32_t>(offsetof(ThreadSafeRandom_t1204416265_StaticFields, ____r_0)); }
	inline Random_t108471755 * get__r_0() const { return ____r_0; }
	inline Random_t108471755 ** get_address_of__r_0() { return &____r_0; }
	inline void set__r_0(Random_t108471755 * value)
	{
		____r_0 = value;
		Il2CppCodeGenWriteBarrier((&____r_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSAFERANDOM_T1204416265_H
#ifndef SUPPORTCLASS_T2974952451_H
#define SUPPORTCLASS_T2974952451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass
struct  SupportClass_t2974952451  : public RuntimeObject
{
public:

public:
};

struct SupportClass_t2974952451_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Threading.Thread> ExitGames.Client.Photon.SupportClass::threadList
	List_1_t3772910811 * ___threadList_0;
	// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate ExitGames.Client.Photon.SupportClass::IntegerMilliseconds
	IntegerMillisecondsDelegate_t651311252 * ___IntegerMilliseconds_1;

public:
	inline static int32_t get_offset_of_threadList_0() { return static_cast<int32_t>(offsetof(SupportClass_t2974952451_StaticFields, ___threadList_0)); }
	inline List_1_t3772910811 * get_threadList_0() const { return ___threadList_0; }
	inline List_1_t3772910811 ** get_address_of_threadList_0() { return &___threadList_0; }
	inline void set_threadList_0(List_1_t3772910811 * value)
	{
		___threadList_0 = value;
		Il2CppCodeGenWriteBarrier((&___threadList_0), value);
	}

	inline static int32_t get_offset_of_IntegerMilliseconds_1() { return static_cast<int32_t>(offsetof(SupportClass_t2974952451_StaticFields, ___IntegerMilliseconds_1)); }
	inline IntegerMillisecondsDelegate_t651311252 * get_IntegerMilliseconds_1() const { return ___IntegerMilliseconds_1; }
	inline IntegerMillisecondsDelegate_t651311252 ** get_address_of_IntegerMilliseconds_1() { return &___IntegerMilliseconds_1; }
	inline void set_IntegerMilliseconds_1(IntegerMillisecondsDelegate_t651311252 * value)
	{
		___IntegerMilliseconds_1 = value;
		Il2CppCodeGenWriteBarrier((&___IntegerMilliseconds_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTCLASS_T2974952451_H
#ifndef VERSION_T2916202802_H
#define VERSION_T2916202802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Version
struct  Version_t2916202802  : public RuntimeObject
{
public:

public:
};

struct Version_t2916202802_StaticFields
{
public:
	// System.Byte[] ExitGames.Client.Photon.Version::clientVersion
	ByteU5BU5D_t4116647657* ___clientVersion_0;

public:
	inline static int32_t get_offset_of_clientVersion_0() { return static_cast<int32_t>(offsetof(Version_t2916202802_StaticFields, ___clientVersion_0)); }
	inline ByteU5BU5D_t4116647657* get_clientVersion_0() const { return ___clientVersion_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_clientVersion_0() { return &___clientVersion_0; }
	inline void set_clientVersion_0(ByteU5BU5D_t4116647657* value)
	{
		___clientVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___clientVersion_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T2916202802_H
#ifndef BIGINTEGER_T956758543_H
#define BIGINTEGER_T956758543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.SocketServer.Numeric.BigInteger
struct  BigInteger_t956758543  : public RuntimeObject
{
public:
	// System.UInt32[] Photon.SocketServer.Numeric.BigInteger::data
	UInt32U5BU5D_t2770800703* ___data_1;
	// System.Int32 Photon.SocketServer.Numeric.BigInteger::dataLength
	int32_t ___dataLength_2;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(BigInteger_t956758543, ___data_1)); }
	inline UInt32U5BU5D_t2770800703* get_data_1() const { return ___data_1; }
	inline UInt32U5BU5D_t2770800703** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(UInt32U5BU5D_t2770800703* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_dataLength_2() { return static_cast<int32_t>(offsetof(BigInteger_t956758543, ___dataLength_2)); }
	inline int32_t get_dataLength_2() const { return ___dataLength_2; }
	inline int32_t* get_address_of_dataLength_2() { return &___dataLength_2; }
	inline void set_dataLength_2(int32_t value)
	{
		___dataLength_2 = value;
	}
};

struct BigInteger_t956758543_StaticFields
{
public:
	// System.Int32[] Photon.SocketServer.Numeric.BigInteger::primesBelow2000
	Int32U5BU5D_t385246372* ___primesBelow2000_0;

public:
	inline static int32_t get_offset_of_primesBelow2000_0() { return static_cast<int32_t>(offsetof(BigInteger_t956758543_StaticFields, ___primesBelow2000_0)); }
	inline Int32U5BU5D_t385246372* get_primesBelow2000_0() const { return ___primesBelow2000_0; }
	inline Int32U5BU5D_t385246372** get_address_of_primesBelow2000_0() { return &___primesBelow2000_0; }
	inline void set_primesBelow2000_0(Int32U5BU5D_t385246372* value)
	{
		___primesBelow2000_0 = value;
		Il2CppCodeGenWriteBarrier((&___primesBelow2000_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGER_T956758543_H
#ifndef DIFFIEHELLMANCRYPTOPROVIDER_T915317458_H
#define DIFFIEHELLMANCRYPTOPROVIDER_T915317458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.SocketServer.Security.DiffieHellmanCryptoProvider
struct  DiffieHellmanCryptoProvider_t915317458  : public RuntimeObject
{
public:
	// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::prime
	BigInteger_t956758543 * ___prime_1;
	// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::secret
	BigInteger_t956758543 * ___secret_2;
	// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::publicKey
	BigInteger_t956758543 * ___publicKey_3;
	// System.Security.Cryptography.Rijndael Photon.SocketServer.Security.DiffieHellmanCryptoProvider::crypto
	Rijndael_t2986313634 * ___crypto_4;
	// System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::sharedKey
	ByteU5BU5D_t4116647657* ___sharedKey_5;

public:
	inline static int32_t get_offset_of_prime_1() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___prime_1)); }
	inline BigInteger_t956758543 * get_prime_1() const { return ___prime_1; }
	inline BigInteger_t956758543 ** get_address_of_prime_1() { return &___prime_1; }
	inline void set_prime_1(BigInteger_t956758543 * value)
	{
		___prime_1 = value;
		Il2CppCodeGenWriteBarrier((&___prime_1), value);
	}

	inline static int32_t get_offset_of_secret_2() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___secret_2)); }
	inline BigInteger_t956758543 * get_secret_2() const { return ___secret_2; }
	inline BigInteger_t956758543 ** get_address_of_secret_2() { return &___secret_2; }
	inline void set_secret_2(BigInteger_t956758543 * value)
	{
		___secret_2 = value;
		Il2CppCodeGenWriteBarrier((&___secret_2), value);
	}

	inline static int32_t get_offset_of_publicKey_3() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___publicKey_3)); }
	inline BigInteger_t956758543 * get_publicKey_3() const { return ___publicKey_3; }
	inline BigInteger_t956758543 ** get_address_of_publicKey_3() { return &___publicKey_3; }
	inline void set_publicKey_3(BigInteger_t956758543 * value)
	{
		___publicKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_3), value);
	}

	inline static int32_t get_offset_of_crypto_4() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___crypto_4)); }
	inline Rijndael_t2986313634 * get_crypto_4() const { return ___crypto_4; }
	inline Rijndael_t2986313634 ** get_address_of_crypto_4() { return &___crypto_4; }
	inline void set_crypto_4(Rijndael_t2986313634 * value)
	{
		___crypto_4 = value;
		Il2CppCodeGenWriteBarrier((&___crypto_4), value);
	}

	inline static int32_t get_offset_of_sharedKey_5() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___sharedKey_5)); }
	inline ByteU5BU5D_t4116647657* get_sharedKey_5() const { return ___sharedKey_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_sharedKey_5() { return &___sharedKey_5; }
	inline void set_sharedKey_5(ByteU5BU5D_t4116647657* value)
	{
		___sharedKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___sharedKey_5), value);
	}
};

struct DiffieHellmanCryptoProvider_t915317458_StaticFields
{
public:
	// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::primeRoot
	BigInteger_t956758543 * ___primeRoot_0;

public:
	inline static int32_t get_offset_of_primeRoot_0() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458_StaticFields, ___primeRoot_0)); }
	inline BigInteger_t956758543 * get_primeRoot_0() const { return ___primeRoot_0; }
	inline BigInteger_t956758543 ** get_address_of_primeRoot_0() { return &___primeRoot_0; }
	inline void set_primeRoot_0(BigInteger_t956758543 * value)
	{
		___primeRoot_0 = value;
		Il2CppCodeGenWriteBarrier((&___primeRoot_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIFFIEHELLMANCRYPTOPROVIDER_T915317458_H
#ifndef OAKLEYGROUPS_T1704371988_H
#define OAKLEYGROUPS_T1704371988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.SocketServer.Security.OakleyGroups
struct  OakleyGroups_t1704371988  : public RuntimeObject
{
public:

public:
};

struct OakleyGroups_t1704371988_StaticFields
{
public:
	// System.Int32 Photon.SocketServer.Security.OakleyGroups::Generator
	int32_t ___Generator_0;
	// System.Byte[] Photon.SocketServer.Security.OakleyGroups::OakleyPrime768
	ByteU5BU5D_t4116647657* ___OakleyPrime768_1;
	// System.Byte[] Photon.SocketServer.Security.OakleyGroups::OakleyPrime1024
	ByteU5BU5D_t4116647657* ___OakleyPrime1024_2;
	// System.Byte[] Photon.SocketServer.Security.OakleyGroups::OakleyPrime1536
	ByteU5BU5D_t4116647657* ___OakleyPrime1536_3;

public:
	inline static int32_t get_offset_of_Generator_0() { return static_cast<int32_t>(offsetof(OakleyGroups_t1704371988_StaticFields, ___Generator_0)); }
	inline int32_t get_Generator_0() const { return ___Generator_0; }
	inline int32_t* get_address_of_Generator_0() { return &___Generator_0; }
	inline void set_Generator_0(int32_t value)
	{
		___Generator_0 = value;
	}

	inline static int32_t get_offset_of_OakleyPrime768_1() { return static_cast<int32_t>(offsetof(OakleyGroups_t1704371988_StaticFields, ___OakleyPrime768_1)); }
	inline ByteU5BU5D_t4116647657* get_OakleyPrime768_1() const { return ___OakleyPrime768_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_OakleyPrime768_1() { return &___OakleyPrime768_1; }
	inline void set_OakleyPrime768_1(ByteU5BU5D_t4116647657* value)
	{
		___OakleyPrime768_1 = value;
		Il2CppCodeGenWriteBarrier((&___OakleyPrime768_1), value);
	}

	inline static int32_t get_offset_of_OakleyPrime1024_2() { return static_cast<int32_t>(offsetof(OakleyGroups_t1704371988_StaticFields, ___OakleyPrime1024_2)); }
	inline ByteU5BU5D_t4116647657* get_OakleyPrime1024_2() const { return ___OakleyPrime1024_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_OakleyPrime1024_2() { return &___OakleyPrime1024_2; }
	inline void set_OakleyPrime1024_2(ByteU5BU5D_t4116647657* value)
	{
		___OakleyPrime1024_2 = value;
		Il2CppCodeGenWriteBarrier((&___OakleyPrime1024_2), value);
	}

	inline static int32_t get_offset_of_OakleyPrime1536_3() { return static_cast<int32_t>(offsetof(OakleyGroups_t1704371988_StaticFields, ___OakleyPrime1536_3)); }
	inline ByteU5BU5D_t4116647657* get_OakleyPrime1536_3() const { return ___OakleyPrime1536_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_OakleyPrime1536_3() { return &___OakleyPrime1536_3; }
	inline void set_OakleyPrime1536_3(ByteU5BU5D_t4116647657* value)
	{
		___OakleyPrime1536_3 = value;
		Il2CppCodeGenWriteBarrier((&___OakleyPrime1536_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OAKLEYGROUPS_T1704371988_H
#ifndef BYTEARRAY_T1708291894_H
#define BYTEARRAY_T1708291894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ByteArray
struct  ByteArray_t1708291894  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEARRAY_T1708291894_H
#ifndef WIREFORMAT_T4196727973_H
#define WIREFORMAT_T4196727973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.WireFormat
struct  WireFormat_t4196727973  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFORMAT_T4196727973_H
#ifndef REFLECTIONMETHODSCACHE_T2103211062_H
#define REFLECTIONMETHODSCACHE_T2103211062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t2103211062  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t701940803 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t1884415901 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t768590915 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t3913627115 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t2311174851 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t3841783507 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3D_0)); }
	inline Raycast3DCallback_t701940803 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t701940803 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t701940803 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t1884415901 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t1884415901 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t1884415901 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast2D_2)); }
	inline Raycast2DCallback_t768590915 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t768590915 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t768590915 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t3913627115 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t3913627115 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t3913627115 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t2311174851 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t3841783507 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t3841783507 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t3841783507 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t2103211062_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t2103211062 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t2103211062 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t2103211062 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t2103211062 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T2103211062_H
#ifndef U3CU3EC__DISPLAYCLASS62_0_T982511824_H
#define U3CU3EC__DISPLAYCLASS62_0_T982511824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass62_0
struct  U3CU3Ec__DisplayClass62_0_t982511824  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass62_0::readCommand
	NCommand_t1230688399 * ___readCommand_0;
	// ExitGames.Client.Photon.EnetPeer ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass62_0::<>4__this
	EnetPeer_t430442630 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_readCommand_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t982511824, ___readCommand_0)); }
	inline NCommand_t1230688399 * get_readCommand_0() const { return ___readCommand_0; }
	inline NCommand_t1230688399 ** get_address_of_readCommand_0() { return &___readCommand_0; }
	inline void set_readCommand_0(NCommand_t1230688399 * value)
	{
		___readCommand_0 = value;
		Il2CppCodeGenWriteBarrier((&___readCommand_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t982511824, ___U3CU3E4__this_1)); }
	inline EnetPeer_t430442630 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline EnetPeer_t430442630 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(EnetPeer_t430442630 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS62_0_T982511824_H
#ifndef U3CU3EC__DISPLAYCLASS56_1_T3313335349_H
#define U3CU3EC__DISPLAYCLASS56_1_T3313335349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1
struct  U3CU3Ec__DisplayClass56_1_t3313335349  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1::length
	int32_t ___length_0;
	// ExitGames.Client.Photon.EnetPeer ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1::<>4__this
	EnetPeer_t430442630 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_1_t3313335349, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_1_t3313335349, ___U3CU3E4__this_1)); }
	inline EnetPeer_t430442630 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline EnetPeer_t430442630 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(EnetPeer_t430442630 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS56_1_T3313335349_H
#ifndef U3CU3EC__DISPLAYCLASS56_0_T3313269813_H
#define U3CU3EC__DISPLAYCLASS56_0_T3313269813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_0
struct  U3CU3Ec__DisplayClass56_0_t3313269813  : public RuntimeObject
{
public:
	// System.Byte[] ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_0::dataCopy
	ByteU5BU5D_t4116647657* ___dataCopy_0;
	// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1 ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_0::CS$<>8__locals1
	U3CU3Ec__DisplayClass56_1_t3313335349 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_dataCopy_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_t3313269813, ___dataCopy_0)); }
	inline ByteU5BU5D_t4116647657* get_dataCopy_0() const { return ___dataCopy_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_dataCopy_0() { return &___dataCopy_0; }
	inline void set_dataCopy_0(ByteU5BU5D_t4116647657* value)
	{
		___dataCopy_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataCopy_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_t3313269813, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass56_1_t3313335349 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass56_1_t3313335349 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass56_1_t3313335349 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS56_0_T3313269813_H
#ifndef NCOMMAND_T1230688399_H
#define NCOMMAND_T1230688399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.NCommand
struct  NCommand_t1230688399  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.NCommand::commandFlags
	uint8_t ___commandFlags_0;
	// System.Byte ExitGames.Client.Photon.NCommand::commandType
	uint8_t ___commandType_6;
	// System.Byte ExitGames.Client.Photon.NCommand::commandChannelID
	uint8_t ___commandChannelID_17;
	// System.Int32 ExitGames.Client.Photon.NCommand::reliableSequenceNumber
	int32_t ___reliableSequenceNumber_18;
	// System.Int32 ExitGames.Client.Photon.NCommand::unreliableSequenceNumber
	int32_t ___unreliableSequenceNumber_19;
	// System.Int32 ExitGames.Client.Photon.NCommand::unsequencedGroupNumber
	int32_t ___unsequencedGroupNumber_20;
	// System.Byte ExitGames.Client.Photon.NCommand::reservedByte
	uint8_t ___reservedByte_21;
	// System.Int32 ExitGames.Client.Photon.NCommand::startSequenceNumber
	int32_t ___startSequenceNumber_22;
	// System.Int32 ExitGames.Client.Photon.NCommand::fragmentCount
	int32_t ___fragmentCount_23;
	// System.Int32 ExitGames.Client.Photon.NCommand::fragmentNumber
	int32_t ___fragmentNumber_24;
	// System.Int32 ExitGames.Client.Photon.NCommand::totalLength
	int32_t ___totalLength_25;
	// System.Int32 ExitGames.Client.Photon.NCommand::fragmentOffset
	int32_t ___fragmentOffset_26;
	// System.Int32 ExitGames.Client.Photon.NCommand::fragmentsRemaining
	int32_t ___fragmentsRemaining_27;
	// System.Int32 ExitGames.Client.Photon.NCommand::commandSentTime
	int32_t ___commandSentTime_28;
	// System.Byte ExitGames.Client.Photon.NCommand::commandSentCount
	uint8_t ___commandSentCount_29;
	// System.Int32 ExitGames.Client.Photon.NCommand::roundTripTimeout
	int32_t ___roundTripTimeout_30;
	// System.Int32 ExitGames.Client.Photon.NCommand::timeoutTime
	int32_t ___timeoutTime_31;
	// System.Int32 ExitGames.Client.Photon.NCommand::ackReceivedReliableSequenceNumber
	int32_t ___ackReceivedReliableSequenceNumber_32;
	// System.Int32 ExitGames.Client.Photon.NCommand::ackReceivedSentTime
	int32_t ___ackReceivedSentTime_33;
	// System.Int32 ExitGames.Client.Photon.NCommand::Size
	int32_t ___Size_45;
	// System.Byte[] ExitGames.Client.Photon.NCommand::commandHeader
	ByteU5BU5D_t4116647657* ___commandHeader_46;
	// System.Int32 ExitGames.Client.Photon.NCommand::SizeOfHeader
	int32_t ___SizeOfHeader_47;
	// System.Byte[] ExitGames.Client.Photon.NCommand::Payload
	ByteU5BU5D_t4116647657* ___Payload_48;

public:
	inline static int32_t get_offset_of_commandFlags_0() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandFlags_0)); }
	inline uint8_t get_commandFlags_0() const { return ___commandFlags_0; }
	inline uint8_t* get_address_of_commandFlags_0() { return &___commandFlags_0; }
	inline void set_commandFlags_0(uint8_t value)
	{
		___commandFlags_0 = value;
	}

	inline static int32_t get_offset_of_commandType_6() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandType_6)); }
	inline uint8_t get_commandType_6() const { return ___commandType_6; }
	inline uint8_t* get_address_of_commandType_6() { return &___commandType_6; }
	inline void set_commandType_6(uint8_t value)
	{
		___commandType_6 = value;
	}

	inline static int32_t get_offset_of_commandChannelID_17() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandChannelID_17)); }
	inline uint8_t get_commandChannelID_17() const { return ___commandChannelID_17; }
	inline uint8_t* get_address_of_commandChannelID_17() { return &___commandChannelID_17; }
	inline void set_commandChannelID_17(uint8_t value)
	{
		___commandChannelID_17 = value;
	}

	inline static int32_t get_offset_of_reliableSequenceNumber_18() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___reliableSequenceNumber_18)); }
	inline int32_t get_reliableSequenceNumber_18() const { return ___reliableSequenceNumber_18; }
	inline int32_t* get_address_of_reliableSequenceNumber_18() { return &___reliableSequenceNumber_18; }
	inline void set_reliableSequenceNumber_18(int32_t value)
	{
		___reliableSequenceNumber_18 = value;
	}

	inline static int32_t get_offset_of_unreliableSequenceNumber_19() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___unreliableSequenceNumber_19)); }
	inline int32_t get_unreliableSequenceNumber_19() const { return ___unreliableSequenceNumber_19; }
	inline int32_t* get_address_of_unreliableSequenceNumber_19() { return &___unreliableSequenceNumber_19; }
	inline void set_unreliableSequenceNumber_19(int32_t value)
	{
		___unreliableSequenceNumber_19 = value;
	}

	inline static int32_t get_offset_of_unsequencedGroupNumber_20() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___unsequencedGroupNumber_20)); }
	inline int32_t get_unsequencedGroupNumber_20() const { return ___unsequencedGroupNumber_20; }
	inline int32_t* get_address_of_unsequencedGroupNumber_20() { return &___unsequencedGroupNumber_20; }
	inline void set_unsequencedGroupNumber_20(int32_t value)
	{
		___unsequencedGroupNumber_20 = value;
	}

	inline static int32_t get_offset_of_reservedByte_21() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___reservedByte_21)); }
	inline uint8_t get_reservedByte_21() const { return ___reservedByte_21; }
	inline uint8_t* get_address_of_reservedByte_21() { return &___reservedByte_21; }
	inline void set_reservedByte_21(uint8_t value)
	{
		___reservedByte_21 = value;
	}

	inline static int32_t get_offset_of_startSequenceNumber_22() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___startSequenceNumber_22)); }
	inline int32_t get_startSequenceNumber_22() const { return ___startSequenceNumber_22; }
	inline int32_t* get_address_of_startSequenceNumber_22() { return &___startSequenceNumber_22; }
	inline void set_startSequenceNumber_22(int32_t value)
	{
		___startSequenceNumber_22 = value;
	}

	inline static int32_t get_offset_of_fragmentCount_23() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___fragmentCount_23)); }
	inline int32_t get_fragmentCount_23() const { return ___fragmentCount_23; }
	inline int32_t* get_address_of_fragmentCount_23() { return &___fragmentCount_23; }
	inline void set_fragmentCount_23(int32_t value)
	{
		___fragmentCount_23 = value;
	}

	inline static int32_t get_offset_of_fragmentNumber_24() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___fragmentNumber_24)); }
	inline int32_t get_fragmentNumber_24() const { return ___fragmentNumber_24; }
	inline int32_t* get_address_of_fragmentNumber_24() { return &___fragmentNumber_24; }
	inline void set_fragmentNumber_24(int32_t value)
	{
		___fragmentNumber_24 = value;
	}

	inline static int32_t get_offset_of_totalLength_25() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___totalLength_25)); }
	inline int32_t get_totalLength_25() const { return ___totalLength_25; }
	inline int32_t* get_address_of_totalLength_25() { return &___totalLength_25; }
	inline void set_totalLength_25(int32_t value)
	{
		___totalLength_25 = value;
	}

	inline static int32_t get_offset_of_fragmentOffset_26() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___fragmentOffset_26)); }
	inline int32_t get_fragmentOffset_26() const { return ___fragmentOffset_26; }
	inline int32_t* get_address_of_fragmentOffset_26() { return &___fragmentOffset_26; }
	inline void set_fragmentOffset_26(int32_t value)
	{
		___fragmentOffset_26 = value;
	}

	inline static int32_t get_offset_of_fragmentsRemaining_27() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___fragmentsRemaining_27)); }
	inline int32_t get_fragmentsRemaining_27() const { return ___fragmentsRemaining_27; }
	inline int32_t* get_address_of_fragmentsRemaining_27() { return &___fragmentsRemaining_27; }
	inline void set_fragmentsRemaining_27(int32_t value)
	{
		___fragmentsRemaining_27 = value;
	}

	inline static int32_t get_offset_of_commandSentTime_28() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandSentTime_28)); }
	inline int32_t get_commandSentTime_28() const { return ___commandSentTime_28; }
	inline int32_t* get_address_of_commandSentTime_28() { return &___commandSentTime_28; }
	inline void set_commandSentTime_28(int32_t value)
	{
		___commandSentTime_28 = value;
	}

	inline static int32_t get_offset_of_commandSentCount_29() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandSentCount_29)); }
	inline uint8_t get_commandSentCount_29() const { return ___commandSentCount_29; }
	inline uint8_t* get_address_of_commandSentCount_29() { return &___commandSentCount_29; }
	inline void set_commandSentCount_29(uint8_t value)
	{
		___commandSentCount_29 = value;
	}

	inline static int32_t get_offset_of_roundTripTimeout_30() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___roundTripTimeout_30)); }
	inline int32_t get_roundTripTimeout_30() const { return ___roundTripTimeout_30; }
	inline int32_t* get_address_of_roundTripTimeout_30() { return &___roundTripTimeout_30; }
	inline void set_roundTripTimeout_30(int32_t value)
	{
		___roundTripTimeout_30 = value;
	}

	inline static int32_t get_offset_of_timeoutTime_31() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___timeoutTime_31)); }
	inline int32_t get_timeoutTime_31() const { return ___timeoutTime_31; }
	inline int32_t* get_address_of_timeoutTime_31() { return &___timeoutTime_31; }
	inline void set_timeoutTime_31(int32_t value)
	{
		___timeoutTime_31 = value;
	}

	inline static int32_t get_offset_of_ackReceivedReliableSequenceNumber_32() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___ackReceivedReliableSequenceNumber_32)); }
	inline int32_t get_ackReceivedReliableSequenceNumber_32() const { return ___ackReceivedReliableSequenceNumber_32; }
	inline int32_t* get_address_of_ackReceivedReliableSequenceNumber_32() { return &___ackReceivedReliableSequenceNumber_32; }
	inline void set_ackReceivedReliableSequenceNumber_32(int32_t value)
	{
		___ackReceivedReliableSequenceNumber_32 = value;
	}

	inline static int32_t get_offset_of_ackReceivedSentTime_33() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___ackReceivedSentTime_33)); }
	inline int32_t get_ackReceivedSentTime_33() const { return ___ackReceivedSentTime_33; }
	inline int32_t* get_address_of_ackReceivedSentTime_33() { return &___ackReceivedSentTime_33; }
	inline void set_ackReceivedSentTime_33(int32_t value)
	{
		___ackReceivedSentTime_33 = value;
	}

	inline static int32_t get_offset_of_Size_45() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___Size_45)); }
	inline int32_t get_Size_45() const { return ___Size_45; }
	inline int32_t* get_address_of_Size_45() { return &___Size_45; }
	inline void set_Size_45(int32_t value)
	{
		___Size_45 = value;
	}

	inline static int32_t get_offset_of_commandHeader_46() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandHeader_46)); }
	inline ByteU5BU5D_t4116647657* get_commandHeader_46() const { return ___commandHeader_46; }
	inline ByteU5BU5D_t4116647657** get_address_of_commandHeader_46() { return &___commandHeader_46; }
	inline void set_commandHeader_46(ByteU5BU5D_t4116647657* value)
	{
		___commandHeader_46 = value;
		Il2CppCodeGenWriteBarrier((&___commandHeader_46), value);
	}

	inline static int32_t get_offset_of_SizeOfHeader_47() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___SizeOfHeader_47)); }
	inline int32_t get_SizeOfHeader_47() const { return ___SizeOfHeader_47; }
	inline int32_t* get_address_of_SizeOfHeader_47() { return &___SizeOfHeader_47; }
	inline void set_SizeOfHeader_47(int32_t value)
	{
		___SizeOfHeader_47 = value;
	}

	inline static int32_t get_offset_of_Payload_48() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___Payload_48)); }
	inline ByteU5BU5D_t4116647657* get_Payload_48() const { return ___Payload_48; }
	inline ByteU5BU5D_t4116647657** get_address_of_Payload_48() { return &___Payload_48; }
	inline void set_Payload_48(ByteU5BU5D_t4116647657* value)
	{
		___Payload_48 = value;
		Il2CppCodeGenWriteBarrier((&___Payload_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NCOMMAND_T1230688399_H
#ifndef CMDLOGITEM_T4217690540_H
#define CMDLOGITEM_T4217690540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.CmdLogItem
struct  CmdLogItem_t4217690540  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::TimeInt
	int32_t ___TimeInt_0;
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::Channel
	int32_t ___Channel_1;
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::SequenceNumber
	int32_t ___SequenceNumber_2;
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::Rtt
	int32_t ___Rtt_3;
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::Variance
	int32_t ___Variance_4;

public:
	inline static int32_t get_offset_of_TimeInt_0() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___TimeInt_0)); }
	inline int32_t get_TimeInt_0() const { return ___TimeInt_0; }
	inline int32_t* get_address_of_TimeInt_0() { return &___TimeInt_0; }
	inline void set_TimeInt_0(int32_t value)
	{
		___TimeInt_0 = value;
	}

	inline static int32_t get_offset_of_Channel_1() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___Channel_1)); }
	inline int32_t get_Channel_1() const { return ___Channel_1; }
	inline int32_t* get_address_of_Channel_1() { return &___Channel_1; }
	inline void set_Channel_1(int32_t value)
	{
		___Channel_1 = value;
	}

	inline static int32_t get_offset_of_SequenceNumber_2() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___SequenceNumber_2)); }
	inline int32_t get_SequenceNumber_2() const { return ___SequenceNumber_2; }
	inline int32_t* get_address_of_SequenceNumber_2() { return &___SequenceNumber_2; }
	inline void set_SequenceNumber_2(int32_t value)
	{
		___SequenceNumber_2 = value;
	}

	inline static int32_t get_offset_of_Rtt_3() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___Rtt_3)); }
	inline int32_t get_Rtt_3() const { return ___Rtt_3; }
	inline int32_t* get_address_of_Rtt_3() { return &___Rtt_3; }
	inline void set_Rtt_3(int32_t value)
	{
		___Rtt_3 = value;
	}

	inline static int32_t get_offset_of_Variance_4() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___Variance_4)); }
	inline int32_t get_Variance_4() const { return ___Variance_4; }
	inline int32_t* get_address_of_Variance_4() { return &___Variance_4; }
	inline void set_Variance_4(int32_t value)
	{
		___Variance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMDLOGITEM_T4217690540_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T3066328396_H
#define U3CU3EC__DISPLAYCLASS30_0_T3066328396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.TPeer/<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t3066328396  : public RuntimeObject
{
public:
	// System.Byte[] ExitGames.Client.Photon.TPeer/<>c__DisplayClass30_0::data
	ByteU5BU5D_t4116647657* ___data_0;
	// ExitGames.Client.Photon.TPeer ExitGames.Client.Photon.TPeer/<>c__DisplayClass30_0::<>4__this
	TPeer_t1497954812 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t3066328396, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t3066328396, ___U3CU3E4__this_1)); }
	inline TPeer_t1497954812 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline TPeer_t1497954812 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(TPeer_t1497954812 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T3066328396_H
#ifndef EVENTDATA_T3728223374_H
#define EVENTDATA_T3728223374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EventData
struct  EventData_t3728223374  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.EventData::Code
	uint8_t ___Code_0;
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.EventData::Parameters
	Dictionary_2_t1405253484 * ___Parameters_1;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(EventData_t3728223374, ___Code_0)); }
	inline uint8_t get_Code_0() const { return ___Code_0; }
	inline uint8_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(uint8_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Parameters_1() { return static_cast<int32_t>(offsetof(EventData_t3728223374, ___Parameters_1)); }
	inline Dictionary_2_t1405253484 * get_Parameters_1() const { return ___Parameters_1; }
	inline Dictionary_2_t1405253484 ** get_address_of_Parameters_1() { return &___Parameters_1; }
	inline void set_Parameters_1(Dictionary_2_t1405253484 * value)
	{
		___Parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATA_T3728223374_H
#ifndef PHOTONCODES_T543425440_H
#define PHOTONCODES_T543425440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonCodes
struct  PhotonCodes_t543425440  : public RuntimeObject
{
public:

public:
};

struct PhotonCodes_t543425440_StaticFields
{
public:
	// System.Byte ExitGames.Client.Photon.PhotonCodes::ClientKey
	uint8_t ___ClientKey_0;
	// System.Byte ExitGames.Client.Photon.PhotonCodes::ModeKey
	uint8_t ___ModeKey_1;
	// System.Byte ExitGames.Client.Photon.PhotonCodes::ServerKey
	uint8_t ___ServerKey_2;
	// System.Byte ExitGames.Client.Photon.PhotonCodes::InitEncryption
	uint8_t ___InitEncryption_3;
	// System.Byte ExitGames.Client.Photon.PhotonCodes::Ping
	uint8_t ___Ping_4;

public:
	inline static int32_t get_offset_of_ClientKey_0() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___ClientKey_0)); }
	inline uint8_t get_ClientKey_0() const { return ___ClientKey_0; }
	inline uint8_t* get_address_of_ClientKey_0() { return &___ClientKey_0; }
	inline void set_ClientKey_0(uint8_t value)
	{
		___ClientKey_0 = value;
	}

	inline static int32_t get_offset_of_ModeKey_1() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___ModeKey_1)); }
	inline uint8_t get_ModeKey_1() const { return ___ModeKey_1; }
	inline uint8_t* get_address_of_ModeKey_1() { return &___ModeKey_1; }
	inline void set_ModeKey_1(uint8_t value)
	{
		___ModeKey_1 = value;
	}

	inline static int32_t get_offset_of_ServerKey_2() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___ServerKey_2)); }
	inline uint8_t get_ServerKey_2() const { return ___ServerKey_2; }
	inline uint8_t* get_address_of_ServerKey_2() { return &___ServerKey_2; }
	inline void set_ServerKey_2(uint8_t value)
	{
		___ServerKey_2 = value;
	}

	inline static int32_t get_offset_of_InitEncryption_3() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___InitEncryption_3)); }
	inline uint8_t get_InitEncryption_3() const { return ___InitEncryption_3; }
	inline uint8_t* get_address_of_InitEncryption_3() { return &___InitEncryption_3; }
	inline void set_InitEncryption_3(uint8_t value)
	{
		___InitEncryption_3 = value;
	}

	inline static int32_t get_offset_of_Ping_4() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___Ping_4)); }
	inline uint8_t get_Ping_4() const { return ___Ping_4; }
	inline uint8_t* get_address_of_Ping_4() { return &___Ping_4; }
	inline void set_Ping_4(uint8_t value)
	{
		___Ping_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONCODES_T543425440_H
#ifndef OPERATIONRESPONSE_T423627973_H
#define OPERATIONRESPONSE_T423627973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.OperationResponse
struct  OperationResponse_t423627973  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.OperationResponse::OperationCode
	uint8_t ___OperationCode_0;
	// System.Int16 ExitGames.Client.Photon.OperationResponse::ReturnCode
	int16_t ___ReturnCode_1;
	// System.String ExitGames.Client.Photon.OperationResponse::DebugMessage
	String_t* ___DebugMessage_2;
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.OperationResponse::Parameters
	Dictionary_2_t1405253484 * ___Parameters_3;

public:
	inline static int32_t get_offset_of_OperationCode_0() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___OperationCode_0)); }
	inline uint8_t get_OperationCode_0() const { return ___OperationCode_0; }
	inline uint8_t* get_address_of_OperationCode_0() { return &___OperationCode_0; }
	inline void set_OperationCode_0(uint8_t value)
	{
		___OperationCode_0 = value;
	}

	inline static int32_t get_offset_of_ReturnCode_1() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___ReturnCode_1)); }
	inline int16_t get_ReturnCode_1() const { return ___ReturnCode_1; }
	inline int16_t* get_address_of_ReturnCode_1() { return &___ReturnCode_1; }
	inline void set_ReturnCode_1(int16_t value)
	{
		___ReturnCode_1 = value;
	}

	inline static int32_t get_offset_of_DebugMessage_2() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___DebugMessage_2)); }
	inline String_t* get_DebugMessage_2() const { return ___DebugMessage_2; }
	inline String_t** get_address_of_DebugMessage_2() { return &___DebugMessage_2; }
	inline void set_DebugMessage_2(String_t* value)
	{
		___DebugMessage_2 = value;
		Il2CppCodeGenWriteBarrier((&___DebugMessage_2), value);
	}

	inline static int32_t get_offset_of_Parameters_3() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___Parameters_3)); }
	inline Dictionary_2_t1405253484 * get_Parameters_3() const { return ___Parameters_3; }
	inline Dictionary_2_t1405253484 ** get_address_of_Parameters_3() { return &___Parameters_3; }
	inline void set_Parameters_3(Dictionary_2_t1405253484 * value)
	{
		___Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONRESPONSE_T423627973_H
#ifndef OPERATIONREQUEST_T597637232_H
#define OPERATIONREQUEST_T597637232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.OperationRequest
struct  OperationRequest_t597637232  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.OperationRequest::OperationCode
	uint8_t ___OperationCode_0;
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.OperationRequest::Parameters
	Dictionary_2_t1405253484 * ___Parameters_1;

public:
	inline static int32_t get_offset_of_OperationCode_0() { return static_cast<int32_t>(offsetof(OperationRequest_t597637232, ___OperationCode_0)); }
	inline uint8_t get_OperationCode_0() const { return ___OperationCode_0; }
	inline uint8_t* get_address_of_OperationCode_0() { return &___OperationCode_0; }
	inline void set_OperationCode_0(uint8_t value)
	{
		___OperationCode_0 = value;
	}

	inline static int32_t get_offset_of_Parameters_1() { return static_cast<int32_t>(offsetof(OperationRequest_t597637232, ___Parameters_1)); }
	inline Dictionary_2_t1405253484 * get_Parameters_1() const { return ___Parameters_1; }
	inline Dictionary_2_t1405253484 ** get_address_of_Parameters_1() { return &___Parameters_1; }
	inline void set_Parameters_1(Dictionary_2_t1405253484 * value)
	{
		___Parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONREQUEST_T597637232_H
#ifndef IPROTOCOL_T1394662050_H
#define IPROTOCOL_T1394662050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.IProtocol
struct  IProtocol_t1394662050  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPROTOCOL_T1394662050_H
#ifndef SERIALIZATIONPROTOCOLFACTORY_T2539989091_H
#define SERIALIZATIONPROTOCOLFACTORY_T2539989091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializationProtocolFactory
struct  SerializationProtocolFactory_t2539989091  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONPROTOCOLFACTORY_T2539989091_H
#ifndef THROWHELPER_T3137032741_H
#define THROWHELPER_T3137032741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ThrowHelper
struct  ThrowHelper_t3137032741  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROWHELPER_T3137032741_H
#ifndef CODEDINPUTSTREAM_T2502120507_H
#define CODEDINPUTSTREAM_T2502120507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.CodedInputStream
struct  CodedInputStream_t2502120507  : public RuntimeObject
{
public:
	// System.Byte[] Google.ProtocolBuffers.CodedInputStream::buffer
	ByteU5BU5D_t4116647657* ___buffer_0;
	// System.Int32 Google.ProtocolBuffers.CodedInputStream::bufferSize
	int32_t ___bufferSize_1;
	// System.Int32 Google.ProtocolBuffers.CodedInputStream::bufferSizeAfterLimit
	int32_t ___bufferSizeAfterLimit_2;
	// System.Int32 Google.ProtocolBuffers.CodedInputStream::bufferPos
	int32_t ___bufferPos_3;
	// System.IO.Stream Google.ProtocolBuffers.CodedInputStream::input
	Stream_t1273022909 * ___input_4;
	// System.UInt32 Google.ProtocolBuffers.CodedInputStream::lastTag
	uint32_t ___lastTag_5;
	// System.UInt32 Google.ProtocolBuffers.CodedInputStream::nextTag
	uint32_t ___nextTag_6;
	// System.Boolean Google.ProtocolBuffers.CodedInputStream::hasNextTag
	bool ___hasNextTag_7;
	// System.Int32 Google.ProtocolBuffers.CodedInputStream::totalBytesRetired
	int32_t ___totalBytesRetired_8;
	// System.Int32 Google.ProtocolBuffers.CodedInputStream::currentLimit
	int32_t ___currentLimit_9;
	// System.Int32 Google.ProtocolBuffers.CodedInputStream::recursionDepth
	int32_t ___recursionDepth_10;
	// System.Int32 Google.ProtocolBuffers.CodedInputStream::recursionLimit
	int32_t ___recursionLimit_11;
	// System.Int32 Google.ProtocolBuffers.CodedInputStream::sizeLimit
	int32_t ___sizeLimit_12;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_bufferSize_1() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___bufferSize_1)); }
	inline int32_t get_bufferSize_1() const { return ___bufferSize_1; }
	inline int32_t* get_address_of_bufferSize_1() { return &___bufferSize_1; }
	inline void set_bufferSize_1(int32_t value)
	{
		___bufferSize_1 = value;
	}

	inline static int32_t get_offset_of_bufferSizeAfterLimit_2() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___bufferSizeAfterLimit_2)); }
	inline int32_t get_bufferSizeAfterLimit_2() const { return ___bufferSizeAfterLimit_2; }
	inline int32_t* get_address_of_bufferSizeAfterLimit_2() { return &___bufferSizeAfterLimit_2; }
	inline void set_bufferSizeAfterLimit_2(int32_t value)
	{
		___bufferSizeAfterLimit_2 = value;
	}

	inline static int32_t get_offset_of_bufferPos_3() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___bufferPos_3)); }
	inline int32_t get_bufferPos_3() const { return ___bufferPos_3; }
	inline int32_t* get_address_of_bufferPos_3() { return &___bufferPos_3; }
	inline void set_bufferPos_3(int32_t value)
	{
		___bufferPos_3 = value;
	}

	inline static int32_t get_offset_of_input_4() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___input_4)); }
	inline Stream_t1273022909 * get_input_4() const { return ___input_4; }
	inline Stream_t1273022909 ** get_address_of_input_4() { return &___input_4; }
	inline void set_input_4(Stream_t1273022909 * value)
	{
		___input_4 = value;
		Il2CppCodeGenWriteBarrier((&___input_4), value);
	}

	inline static int32_t get_offset_of_lastTag_5() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___lastTag_5)); }
	inline uint32_t get_lastTag_5() const { return ___lastTag_5; }
	inline uint32_t* get_address_of_lastTag_5() { return &___lastTag_5; }
	inline void set_lastTag_5(uint32_t value)
	{
		___lastTag_5 = value;
	}

	inline static int32_t get_offset_of_nextTag_6() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___nextTag_6)); }
	inline uint32_t get_nextTag_6() const { return ___nextTag_6; }
	inline uint32_t* get_address_of_nextTag_6() { return &___nextTag_6; }
	inline void set_nextTag_6(uint32_t value)
	{
		___nextTag_6 = value;
	}

	inline static int32_t get_offset_of_hasNextTag_7() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___hasNextTag_7)); }
	inline bool get_hasNextTag_7() const { return ___hasNextTag_7; }
	inline bool* get_address_of_hasNextTag_7() { return &___hasNextTag_7; }
	inline void set_hasNextTag_7(bool value)
	{
		___hasNextTag_7 = value;
	}

	inline static int32_t get_offset_of_totalBytesRetired_8() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___totalBytesRetired_8)); }
	inline int32_t get_totalBytesRetired_8() const { return ___totalBytesRetired_8; }
	inline int32_t* get_address_of_totalBytesRetired_8() { return &___totalBytesRetired_8; }
	inline void set_totalBytesRetired_8(int32_t value)
	{
		___totalBytesRetired_8 = value;
	}

	inline static int32_t get_offset_of_currentLimit_9() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___currentLimit_9)); }
	inline int32_t get_currentLimit_9() const { return ___currentLimit_9; }
	inline int32_t* get_address_of_currentLimit_9() { return &___currentLimit_9; }
	inline void set_currentLimit_9(int32_t value)
	{
		___currentLimit_9 = value;
	}

	inline static int32_t get_offset_of_recursionDepth_10() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___recursionDepth_10)); }
	inline int32_t get_recursionDepth_10() const { return ___recursionDepth_10; }
	inline int32_t* get_address_of_recursionDepth_10() { return &___recursionDepth_10; }
	inline void set_recursionDepth_10(int32_t value)
	{
		___recursionDepth_10 = value;
	}

	inline static int32_t get_offset_of_recursionLimit_11() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___recursionLimit_11)); }
	inline int32_t get_recursionLimit_11() const { return ___recursionLimit_11; }
	inline int32_t* get_address_of_recursionLimit_11() { return &___recursionLimit_11; }
	inline void set_recursionLimit_11(int32_t value)
	{
		___recursionLimit_11 = value;
	}

	inline static int32_t get_offset_of_sizeLimit_12() { return static_cast<int32_t>(offsetof(CodedInputStream_t2502120507, ___sizeLimit_12)); }
	inline int32_t get_sizeLimit_12() const { return ___sizeLimit_12; }
	inline int32_t* get_address_of_sizeLimit_12() { return &___sizeLimit_12; }
	inline void set_sizeLimit_12(int32_t value)
	{
		___sizeLimit_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEDINPUTSTREAM_T2502120507_H
#ifndef EXTENSIONREGISTRY_T4271428238_H
#define EXTENSIONREGISTRY_T4271428238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ExtensionRegistry
struct  ExtensionRegistry_t4271428238  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.String,Google.ProtocolBuffers.IGeneratedExtensionLite>> Google.ProtocolBuffers.ExtensionRegistry::extensionsByName
	Dictionary_2_t3107168633 * ___extensionsByName_1;
	// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite> Google.ProtocolBuffers.ExtensionRegistry::extensionsByNumber
	Dictionary_2_t294577696 * ___extensionsByNumber_2;
	// System.Boolean Google.ProtocolBuffers.ExtensionRegistry::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_extensionsByName_1() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4271428238, ___extensionsByName_1)); }
	inline Dictionary_2_t3107168633 * get_extensionsByName_1() const { return ___extensionsByName_1; }
	inline Dictionary_2_t3107168633 ** get_address_of_extensionsByName_1() { return &___extensionsByName_1; }
	inline void set_extensionsByName_1(Dictionary_2_t3107168633 * value)
	{
		___extensionsByName_1 = value;
		Il2CppCodeGenWriteBarrier((&___extensionsByName_1), value);
	}

	inline static int32_t get_offset_of_extensionsByNumber_2() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4271428238, ___extensionsByNumber_2)); }
	inline Dictionary_2_t294577696 * get_extensionsByNumber_2() const { return ___extensionsByNumber_2; }
	inline Dictionary_2_t294577696 ** get_address_of_extensionsByNumber_2() { return &___extensionsByNumber_2; }
	inline void set_extensionsByNumber_2(Dictionary_2_t294577696 * value)
	{
		___extensionsByNumber_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensionsByNumber_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4271428238, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct ExtensionRegistry_t4271428238_StaticFields
{
public:
	// Google.ProtocolBuffers.ExtensionRegistry Google.ProtocolBuffers.ExtensionRegistry::empty
	ExtensionRegistry_t4271428238 * ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4271428238_StaticFields, ___empty_0)); }
	inline ExtensionRegistry_t4271428238 * get_empty_0() const { return ___empty_0; }
	inline ExtensionRegistry_t4271428238 ** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(ExtensionRegistry_t4271428238 * value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONREGISTRY_T4271428238_H
#ifndef ENETCHANNEL_T2207795168_H
#define ENETCHANNEL_T2207795168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetChannel
struct  EnetChannel_t2207795168  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.EnetChannel::ChannelNumber
	uint8_t ___ChannelNumber_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetChannel::incomingReliableCommandsList
	Dictionary_2_t119401730 * ___incomingReliableCommandsList_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetChannel::incomingUnreliableCommandsList
	Dictionary_2_t119401730 * ___incomingUnreliableCommandsList_2;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetChannel::outgoingReliableCommandsList
	Queue_1_t1076947893 * ___outgoingReliableCommandsList_3;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetChannel::outgoingUnreliableCommandsList
	Queue_1_t1076947893 * ___outgoingUnreliableCommandsList_4;
	// System.Int32 ExitGames.Client.Photon.EnetChannel::incomingReliableSequenceNumber
	int32_t ___incomingReliableSequenceNumber_5;
	// System.Int32 ExitGames.Client.Photon.EnetChannel::incomingUnreliableSequenceNumber
	int32_t ___incomingUnreliableSequenceNumber_6;
	// System.Int32 ExitGames.Client.Photon.EnetChannel::outgoingReliableSequenceNumber
	int32_t ___outgoingReliableSequenceNumber_7;
	// System.Int32 ExitGames.Client.Photon.EnetChannel::outgoingUnreliableSequenceNumber
	int32_t ___outgoingUnreliableSequenceNumber_8;

public:
	inline static int32_t get_offset_of_ChannelNumber_0() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___ChannelNumber_0)); }
	inline uint8_t get_ChannelNumber_0() const { return ___ChannelNumber_0; }
	inline uint8_t* get_address_of_ChannelNumber_0() { return &___ChannelNumber_0; }
	inline void set_ChannelNumber_0(uint8_t value)
	{
		___ChannelNumber_0 = value;
	}

	inline static int32_t get_offset_of_incomingReliableCommandsList_1() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___incomingReliableCommandsList_1)); }
	inline Dictionary_2_t119401730 * get_incomingReliableCommandsList_1() const { return ___incomingReliableCommandsList_1; }
	inline Dictionary_2_t119401730 ** get_address_of_incomingReliableCommandsList_1() { return &___incomingReliableCommandsList_1; }
	inline void set_incomingReliableCommandsList_1(Dictionary_2_t119401730 * value)
	{
		___incomingReliableCommandsList_1 = value;
		Il2CppCodeGenWriteBarrier((&___incomingReliableCommandsList_1), value);
	}

	inline static int32_t get_offset_of_incomingUnreliableCommandsList_2() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___incomingUnreliableCommandsList_2)); }
	inline Dictionary_2_t119401730 * get_incomingUnreliableCommandsList_2() const { return ___incomingUnreliableCommandsList_2; }
	inline Dictionary_2_t119401730 ** get_address_of_incomingUnreliableCommandsList_2() { return &___incomingUnreliableCommandsList_2; }
	inline void set_incomingUnreliableCommandsList_2(Dictionary_2_t119401730 * value)
	{
		___incomingUnreliableCommandsList_2 = value;
		Il2CppCodeGenWriteBarrier((&___incomingUnreliableCommandsList_2), value);
	}

	inline static int32_t get_offset_of_outgoingReliableCommandsList_3() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___outgoingReliableCommandsList_3)); }
	inline Queue_1_t1076947893 * get_outgoingReliableCommandsList_3() const { return ___outgoingReliableCommandsList_3; }
	inline Queue_1_t1076947893 ** get_address_of_outgoingReliableCommandsList_3() { return &___outgoingReliableCommandsList_3; }
	inline void set_outgoingReliableCommandsList_3(Queue_1_t1076947893 * value)
	{
		___outgoingReliableCommandsList_3 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingReliableCommandsList_3), value);
	}

	inline static int32_t get_offset_of_outgoingUnreliableCommandsList_4() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___outgoingUnreliableCommandsList_4)); }
	inline Queue_1_t1076947893 * get_outgoingUnreliableCommandsList_4() const { return ___outgoingUnreliableCommandsList_4; }
	inline Queue_1_t1076947893 ** get_address_of_outgoingUnreliableCommandsList_4() { return &___outgoingUnreliableCommandsList_4; }
	inline void set_outgoingUnreliableCommandsList_4(Queue_1_t1076947893 * value)
	{
		___outgoingUnreliableCommandsList_4 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingUnreliableCommandsList_4), value);
	}

	inline static int32_t get_offset_of_incomingReliableSequenceNumber_5() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___incomingReliableSequenceNumber_5)); }
	inline int32_t get_incomingReliableSequenceNumber_5() const { return ___incomingReliableSequenceNumber_5; }
	inline int32_t* get_address_of_incomingReliableSequenceNumber_5() { return &___incomingReliableSequenceNumber_5; }
	inline void set_incomingReliableSequenceNumber_5(int32_t value)
	{
		___incomingReliableSequenceNumber_5 = value;
	}

	inline static int32_t get_offset_of_incomingUnreliableSequenceNumber_6() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___incomingUnreliableSequenceNumber_6)); }
	inline int32_t get_incomingUnreliableSequenceNumber_6() const { return ___incomingUnreliableSequenceNumber_6; }
	inline int32_t* get_address_of_incomingUnreliableSequenceNumber_6() { return &___incomingUnreliableSequenceNumber_6; }
	inline void set_incomingUnreliableSequenceNumber_6(int32_t value)
	{
		___incomingUnreliableSequenceNumber_6 = value;
	}

	inline static int32_t get_offset_of_outgoingReliableSequenceNumber_7() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___outgoingReliableSequenceNumber_7)); }
	inline int32_t get_outgoingReliableSequenceNumber_7() const { return ___outgoingReliableSequenceNumber_7; }
	inline int32_t* get_address_of_outgoingReliableSequenceNumber_7() { return &___outgoingReliableSequenceNumber_7; }
	inline void set_outgoingReliableSequenceNumber_7(int32_t value)
	{
		___outgoingReliableSequenceNumber_7 = value;
	}

	inline static int32_t get_offset_of_outgoingUnreliableSequenceNumber_8() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___outgoingUnreliableSequenceNumber_8)); }
	inline int32_t get_outgoingUnreliableSequenceNumber_8() const { return ___outgoingUnreliableSequenceNumber_8; }
	inline int32_t* get_address_of_outgoingUnreliableSequenceNumber_8() { return &___outgoingUnreliableSequenceNumber_8; }
	inline void set_outgoingUnreliableSequenceNumber_8(int32_t value)
	{
		___outgoingUnreliableSequenceNumber_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENETCHANNEL_T2207795168_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef DICTIONARY_2_T132545152_H
#define DICTIONARY_2_T132545152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct  Dictionary_2_t132545152  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	ObjectU5BU5D_t2843939325* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___keySlots_6)); }
	inline ObjectU5BU5D_t2843939325* get_keySlots_6() const { return ___keySlots_6; }
	inline ObjectU5BU5D_t2843939325** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(ObjectU5BU5D_t2843939325* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t132545152_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4209139644 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4209139644 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4209139644 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4209139644 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T132545152_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef LISTS_T2963336001_H
#define LISTS_T2963336001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.Collections.Lists
struct  Lists_t2963336001  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTS_T2963336001_H
#ifndef BYTESTRING_T35393593_H
#define BYTESTRING_T35393593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ByteString
struct  ByteString_t35393593  : public RuntimeObject
{
public:
	// System.Byte[] Google.ProtocolBuffers.ByteString::bytes
	ByteU5BU5D_t4116647657* ___bytes_1;

public:
	inline static int32_t get_offset_of_bytes_1() { return static_cast<int32_t>(offsetof(ByteString_t35393593, ___bytes_1)); }
	inline ByteU5BU5D_t4116647657* get_bytes_1() const { return ___bytes_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytes_1() { return &___bytes_1; }
	inline void set_bytes_1(ByteU5BU5D_t4116647657* value)
	{
		___bytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_1), value);
	}
};

struct ByteString_t35393593_StaticFields
{
public:
	// Google.ProtocolBuffers.ByteString Google.ProtocolBuffers.ByteString::empty
	ByteString_t35393593 * ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(ByteString_t35393593_StaticFields, ___empty_0)); }
	inline ByteString_t35393593 * get_empty_0() const { return ___empty_0; }
	inline ByteString_t35393593 ** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(ByteString_t35393593 * value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTESTRING_T35393593_H
#ifndef CODEDOUTPUTSTREAM_T1787628118_H
#define CODEDOUTPUTSTREAM_T1787628118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.CodedOutputStream
struct  CodedOutputStream_t1787628118  : public RuntimeObject
{
public:
	// System.Byte[] Google.ProtocolBuffers.CodedOutputStream::buffer
	ByteU5BU5D_t4116647657* ___buffer_1;
	// System.Int32 Google.ProtocolBuffers.CodedOutputStream::limit
	int32_t ___limit_2;
	// System.Int32 Google.ProtocolBuffers.CodedOutputStream::position
	int32_t ___position_3;
	// System.IO.Stream Google.ProtocolBuffers.CodedOutputStream::output
	Stream_t1273022909 * ___output_4;

public:
	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(CodedOutputStream_t1787628118, ___buffer_1)); }
	inline ByteU5BU5D_t4116647657* get_buffer_1() const { return ___buffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(ByteU5BU5D_t4116647657* value)
	{
		___buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_1), value);
	}

	inline static int32_t get_offset_of_limit_2() { return static_cast<int32_t>(offsetof(CodedOutputStream_t1787628118, ___limit_2)); }
	inline int32_t get_limit_2() const { return ___limit_2; }
	inline int32_t* get_address_of_limit_2() { return &___limit_2; }
	inline void set_limit_2(int32_t value)
	{
		___limit_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(CodedOutputStream_t1787628118, ___position_3)); }
	inline int32_t get_position_3() const { return ___position_3; }
	inline int32_t* get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(int32_t value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_output_4() { return static_cast<int32_t>(offsetof(CodedOutputStream_t1787628118, ___output_4)); }
	inline Stream_t1273022909 * get_output_4() const { return ___output_4; }
	inline Stream_t1273022909 ** get_address_of_output_4() { return &___output_4; }
	inline void set_output_4(Stream_t1273022909 * value)
	{
		___output_4 = value;
		Il2CppCodeGenWriteBarrier((&___output_4), value);
	}
};

struct CodedOutputStream_t1787628118_StaticFields
{
public:
	// System.Int32 Google.ProtocolBuffers.CodedOutputStream::DefaultBufferSize
	int32_t ___DefaultBufferSize_0;

public:
	inline static int32_t get_offset_of_DefaultBufferSize_0() { return static_cast<int32_t>(offsetof(CodedOutputStream_t1787628118_StaticFields, ___DefaultBufferSize_0)); }
	inline int32_t get_DefaultBufferSize_0() const { return ___DefaultBufferSize_0; }
	inline int32_t* get_address_of_DefaultBufferSize_0() { return &___DefaultBufferSize_0; }
	inline void set_DefaultBufferSize_0(int32_t value)
	{
		___DefaultBufferSize_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEDOUTPUTSTREAM_T1787628118_H
#ifndef FRAMEWORKPORTABILITY_T3105963803_H
#define FRAMEWORKPORTABILITY_T3105963803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.FrameworkPortability
struct  FrameworkPortability_t3105963803  : public RuntimeObject
{
public:

public:
};

struct FrameworkPortability_t3105963803_StaticFields
{
public:
	// System.String Google.ProtocolBuffers.FrameworkPortability::NewLine
	String_t* ___NewLine_0;

public:
	inline static int32_t get_offset_of_NewLine_0() { return static_cast<int32_t>(offsetof(FrameworkPortability_t3105963803_StaticFields, ___NewLine_0)); }
	inline String_t* get_NewLine_0() const { return ___NewLine_0; }
	inline String_t** get_address_of_NewLine_0() { return &___NewLine_0; }
	inline void set_NewLine_0(String_t* value)
	{
		___NewLine_0 = value;
		Il2CppCodeGenWriteBarrier((&___NewLine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEWORKPORTABILITY_T3105963803_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef CMDLOGRECEIVEDRELIABLE_T4090183889_H
#define CMDLOGRECEIVEDRELIABLE_T4090183889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.CmdLogReceivedReliable
struct  CmdLogReceivedReliable_t4090183889  : public CmdLogItem_t4217690540
{
public:
	// System.Int32 ExitGames.Client.Photon.CmdLogReceivedReliable::TimeSinceLastSend
	int32_t ___TimeSinceLastSend_5;
	// System.Int32 ExitGames.Client.Photon.CmdLogReceivedReliable::TimeSinceLastSendAck
	int32_t ___TimeSinceLastSendAck_6;

public:
	inline static int32_t get_offset_of_TimeSinceLastSend_5() { return static_cast<int32_t>(offsetof(CmdLogReceivedReliable_t4090183889, ___TimeSinceLastSend_5)); }
	inline int32_t get_TimeSinceLastSend_5() const { return ___TimeSinceLastSend_5; }
	inline int32_t* get_address_of_TimeSinceLastSend_5() { return &___TimeSinceLastSend_5; }
	inline void set_TimeSinceLastSend_5(int32_t value)
	{
		___TimeSinceLastSend_5 = value;
	}

	inline static int32_t get_offset_of_TimeSinceLastSendAck_6() { return static_cast<int32_t>(offsetof(CmdLogReceivedReliable_t4090183889, ___TimeSinceLastSendAck_6)); }
	inline int32_t get_TimeSinceLastSendAck_6() const { return ___TimeSinceLastSendAck_6; }
	inline int32_t* get_address_of_TimeSinceLastSendAck_6() { return &___TimeSinceLastSendAck_6; }
	inline void set_TimeSinceLastSendAck_6(int32_t value)
	{
		___TimeSinceLastSendAck_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMDLOGRECEIVEDRELIABLE_T4090183889_H
#ifndef CMDLOGRECEIVEDACK_T580412049_H
#define CMDLOGRECEIVEDACK_T580412049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.CmdLogReceivedAck
struct  CmdLogReceivedAck_t580412049  : public CmdLogItem_t4217690540
{
public:
	// System.Int32 ExitGames.Client.Photon.CmdLogReceivedAck::ReceivedSentTime
	int32_t ___ReceivedSentTime_5;

public:
	inline static int32_t get_offset_of_ReceivedSentTime_5() { return static_cast<int32_t>(offsetof(CmdLogReceivedAck_t580412049, ___ReceivedSentTime_5)); }
	inline int32_t get_ReceivedSentTime_5() const { return ___ReceivedSentTime_5; }
	inline int32_t* get_address_of_ReceivedSentTime_5() { return &___ReceivedSentTime_5; }
	inline void set_ReceivedSentTime_5(int32_t value)
	{
		___ReceivedSentTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMDLOGRECEIVEDACK_T580412049_H
#ifndef CMDLOGSENTRELIABLE_T3437548410_H
#define CMDLOGSENTRELIABLE_T3437548410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.CmdLogSentReliable
struct  CmdLogSentReliable_t3437548410  : public CmdLogItem_t4217690540
{
public:
	// System.Int32 ExitGames.Client.Photon.CmdLogSentReliable::Resend
	int32_t ___Resend_5;
	// System.Int32 ExitGames.Client.Photon.CmdLogSentReliable::RoundtripTimeout
	int32_t ___RoundtripTimeout_6;
	// System.Int32 ExitGames.Client.Photon.CmdLogSentReliable::Timeout
	int32_t ___Timeout_7;
	// System.Boolean ExitGames.Client.Photon.CmdLogSentReliable::TriggeredTimeout
	bool ___TriggeredTimeout_8;

public:
	inline static int32_t get_offset_of_Resend_5() { return static_cast<int32_t>(offsetof(CmdLogSentReliable_t3437548410, ___Resend_5)); }
	inline int32_t get_Resend_5() const { return ___Resend_5; }
	inline int32_t* get_address_of_Resend_5() { return &___Resend_5; }
	inline void set_Resend_5(int32_t value)
	{
		___Resend_5 = value;
	}

	inline static int32_t get_offset_of_RoundtripTimeout_6() { return static_cast<int32_t>(offsetof(CmdLogSentReliable_t3437548410, ___RoundtripTimeout_6)); }
	inline int32_t get_RoundtripTimeout_6() const { return ___RoundtripTimeout_6; }
	inline int32_t* get_address_of_RoundtripTimeout_6() { return &___RoundtripTimeout_6; }
	inline void set_RoundtripTimeout_6(int32_t value)
	{
		___RoundtripTimeout_6 = value;
	}

	inline static int32_t get_offset_of_Timeout_7() { return static_cast<int32_t>(offsetof(CmdLogSentReliable_t3437548410, ___Timeout_7)); }
	inline int32_t get_Timeout_7() const { return ___Timeout_7; }
	inline int32_t* get_address_of_Timeout_7() { return &___Timeout_7; }
	inline void set_Timeout_7(int32_t value)
	{
		___Timeout_7 = value;
	}

	inline static int32_t get_offset_of_TriggeredTimeout_8() { return static_cast<int32_t>(offsetof(CmdLogSentReliable_t3437548410, ___TriggeredTimeout_8)); }
	inline bool get_TriggeredTimeout_8() const { return ___TriggeredTimeout_8; }
	inline bool* get_address_of_TriggeredTimeout_8() { return &___TriggeredTimeout_8; }
	inline void set_TriggeredTimeout_8(bool value)
	{
		___TriggeredTimeout_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMDLOGSENTRELIABLE_T3437548410_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef HASHTABLE_T1048209202_H
#define HASHTABLE_T1048209202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Hashtable
struct  Hashtable_t1048209202  : public Dictionary_2_t132545152
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHTABLE_T1048209202_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef STREAMBUFFER_T3827669789_H
#define STREAMBUFFER_T3827669789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.StreamBuffer
struct  StreamBuffer_t3827669789  : public Stream_t1273022909
{
public:
	// System.Int32 ExitGames.Client.Photon.StreamBuffer::pos
	int32_t ___pos_2;
	// System.Int32 ExitGames.Client.Photon.StreamBuffer::len
	int32_t ___len_3;
	// System.Byte[] ExitGames.Client.Photon.StreamBuffer::buf
	ByteU5BU5D_t4116647657* ___buf_4;

public:
	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(StreamBuffer_t3827669789, ___pos_2)); }
	inline int32_t get_pos_2() const { return ___pos_2; }
	inline int32_t* get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(int32_t value)
	{
		___pos_2 = value;
	}

	inline static int32_t get_offset_of_len_3() { return static_cast<int32_t>(offsetof(StreamBuffer_t3827669789, ___len_3)); }
	inline int32_t get_len_3() const { return ___len_3; }
	inline int32_t* get_address_of_len_3() { return &___len_3; }
	inline void set_len_3(int32_t value)
	{
		___len_3 = value;
	}

	inline static int32_t get_offset_of_buf_4() { return static_cast<int32_t>(offsetof(StreamBuffer_t3827669789, ___buf_4)); }
	inline ByteU5BU5D_t4116647657* get_buf_4() const { return ___buf_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_buf_4() { return &___buf_4; }
	inline void set_buf_4(ByteU5BU5D_t4116647657* value)
	{
		___buf_4 = value;
		Il2CppCodeGenWriteBarrier((&___buf_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMBUFFER_T3827669789_H
#ifndef UNINITIALIZEDMESSAGEEXCEPTION_T3402409807_H
#define UNINITIALIZEDMESSAGEEXCEPTION_T3402409807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.UninitializedMessageException
struct  UninitializedMessageException_t3402409807  : public Exception_t
{
public:
	// System.Collections.Generic.IList`1<System.String> Google.ProtocolBuffers.UninitializedMessageException::missingFields
	RuntimeObject* ___missingFields_11;

public:
	inline static int32_t get_offset_of_missingFields_11() { return static_cast<int32_t>(offsetof(UninitializedMessageException_t3402409807, ___missingFields_11)); }
	inline RuntimeObject* get_missingFields_11() const { return ___missingFields_11; }
	inline RuntimeObject** get_address_of_missingFields_11() { return &___missingFields_11; }
	inline void set_missingFields_11(RuntimeObject* value)
	{
		___missingFields_11 = value;
		Il2CppCodeGenWriteBarrier((&___missingFields_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNINITIALIZEDMESSAGEEXCEPTION_T3402409807_H
#ifndef EXTENSIONINTPAIR_T1343559306_H
#define EXTENSIONINTPAIR_T1343559306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair
struct  ExtensionIntPair_t1343559306 
{
public:
	// System.Object Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair::msgType
	RuntimeObject * ___msgType_0;
	// System.Int32 Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair::number
	int32_t ___number_1;

public:
	inline static int32_t get_offset_of_msgType_0() { return static_cast<int32_t>(offsetof(ExtensionIntPair_t1343559306, ___msgType_0)); }
	inline RuntimeObject * get_msgType_0() const { return ___msgType_0; }
	inline RuntimeObject ** get_address_of_msgType_0() { return &___msgType_0; }
	inline void set_msgType_0(RuntimeObject * value)
	{
		___msgType_0 = value;
		Il2CppCodeGenWriteBarrier((&___msgType_0), value);
	}

	inline static int32_t get_offset_of_number_1() { return static_cast<int32_t>(offsetof(ExtensionIntPair_t1343559306, ___number_1)); }
	inline int32_t get_number_1() const { return ___number_1; }
	inline int32_t* get_address_of_number_1() { return &___number_1; }
	inline void set_number_1(int32_t value)
	{
		___number_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair
struct ExtensionIntPair_t1343559306_marshaled_pinvoke
{
	Il2CppIUnknown* ___msgType_0;
	int32_t ___number_1;
};
// Native definition for COM marshalling of Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair
struct ExtensionIntPair_t1343559306_marshaled_com
{
	Il2CppIUnknown* ___msgType_0;
	int32_t ___number_1;
};
#endif // EXTENSIONINTPAIR_T1343559306_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t2806799626 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline Collider2D_t2806799626 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t2806799626 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_pinvoke
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_com
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef SERIALIZATIONPROTOCOL_T4091957412_H
#define SERIALIZATIONPROTOCOL_T4091957412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializationProtocol
struct  SerializationProtocol_t4091957412 
{
public:
	// System.Int32 ExitGames.Client.Photon.SerializationProtocol::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SerializationProtocol_t4091957412, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONPROTOCOL_T4091957412_H
#ifndef IOEXCEPTION_T4088381929_H
#define IOEXCEPTION_T4088381929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t4088381929  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T4088381929_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef DEBUGLEVEL_T3671880145_H
#define DEBUGLEVEL_T3671880145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.DebugLevel
struct  DebugLevel_t3671880145 
{
public:
	// System.Byte ExitGames.Client.Photon.DebugLevel::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugLevel_t3671880145, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVEL_T3671880145_H
#ifndef CONNECTIONPROTOCOL_T2586603950_H
#define CONNECTIONPROTOCOL_T2586603950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.ConnectionProtocol
struct  ConnectionProtocol_t2586603950 
{
public:
	// System.Byte ExitGames.Client.Photon.ConnectionProtocol::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionProtocol_t2586603950, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONPROTOCOL_T2586603950_H
#ifndef PEERSTATEVALUE_T1289417078_H
#define PEERSTATEVALUE_T1289417078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerStateValue
struct  PeerStateValue_t1289417078 
{
public:
	// System.Byte ExitGames.Client.Photon.PeerStateValue::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PeerStateValue_t1289417078, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEERSTATEVALUE_T1289417078_H
#ifndef STATUSCODE_T823606708_H
#define STATUSCODE_T823606708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.StatusCode
struct  StatusCode_t823606708 
{
public:
	// System.Int32 ExitGames.Client.Photon.StatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StatusCode_t823606708, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSCODE_T823606708_H
#ifndef WIRETYPE_T2332021402_H
#define WIRETYPE_T2332021402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.WireFormat/WireType
struct  WireType_t2332021402 
{
public:
	// System.UInt32 Google.ProtocolBuffers.WireFormat/WireType::value__
	uint32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WireType_t2332021402, ___value___1)); }
	inline uint32_t get_value___1() const { return ___value___1; }
	inline uint32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIRETYPE_T2332021402_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255365  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef EGMESSAGETYPE_T1130059189_H
#define EGMESSAGETYPE_T1130059189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/EgMessageType
struct  EgMessageType_t1130059189 
{
public:
	// System.Byte ExitGames.Client.Photon.PeerBase/EgMessageType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EgMessageType_t1130059189, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EGMESSAGETYPE_T1130059189_H
#ifndef CONNECTIONSTATEVALUE_T1954099360_H
#define CONNECTIONSTATEVALUE_T1954099360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/ConnectionStateValue
struct  ConnectionStateValue_t1954099360 
{
public:
	// System.Byte ExitGames.Client.Photon.PeerBase/ConnectionStateValue::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionStateValue_t1954099360, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONSTATEVALUE_T1954099360_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CU3EC__DISPLAYCLASS145_0_T1573695289_H
#define U3CU3EC__DISPLAYCLASS145_0_T1573695289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/<>c__DisplayClass145_0
struct  U3CU3Ec__DisplayClass145_0_t1573695289  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PeerBase/<>c__DisplayClass145_0::level
	uint8_t ___level_0;
	// System.String ExitGames.Client.Photon.PeerBase/<>c__DisplayClass145_0::debugReturn
	String_t* ___debugReturn_1;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PeerBase/<>c__DisplayClass145_0::<>4__this
	PeerBase_t2956237011 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass145_0_t1573695289, ___level_0)); }
	inline uint8_t get_level_0() const { return ___level_0; }
	inline uint8_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(uint8_t value)
	{
		___level_0 = value;
	}

	inline static int32_t get_offset_of_debugReturn_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass145_0_t1573695289, ___debugReturn_1)); }
	inline String_t* get_debugReturn_1() const { return ___debugReturn_1; }
	inline String_t** get_address_of_debugReturn_1() { return &___debugReturn_1; }
	inline void set_debugReturn_1(String_t* value)
	{
		___debugReturn_1 = value;
		Il2CppCodeGenWriteBarrier((&___debugReturn_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass145_0_t1573695289, ___U3CU3E4__this_2)); }
	inline PeerBase_t2956237011 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PeerBase_t2956237011 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PeerBase_t2956237011 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS145_0_T1573695289_H
#ifndef U3CU3EC__DISPLAYCLASS146_0_T1573695292_H
#define U3CU3EC__DISPLAYCLASS146_0_T1573695292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/<>c__DisplayClass146_0
struct  U3CU3Ec__DisplayClass146_0_t1573695292  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.StatusCode ExitGames.Client.Photon.PeerBase/<>c__DisplayClass146_0::statusValue
	int32_t ___statusValue_0;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PeerBase/<>c__DisplayClass146_0::<>4__this
	PeerBase_t2956237011 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_statusValue_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass146_0_t1573695292, ___statusValue_0)); }
	inline int32_t get_statusValue_0() const { return ___statusValue_0; }
	inline int32_t* get_address_of_statusValue_0() { return &___statusValue_0; }
	inline void set_statusValue_0(int32_t value)
	{
		___statusValue_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass146_0_t1573695292, ___U3CU3E4__this_1)); }
	inline PeerBase_t2956237011 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PeerBase_t2956237011 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PeerBase_t2956237011 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS146_0_T1573695292_H
#ifndef INVALIDPROTOCOLBUFFEREXCEPTION_T2498581859_H
#define INVALIDPROTOCOLBUFFEREXCEPTION_T2498581859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.InvalidProtocolBufferException
struct  InvalidProtocolBufferException_t2498581859  : public IOException_t4088381929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDPROTOCOLBUFFEREXCEPTION_T2498581859_H
#ifndef OUTOFSPACEEXCEPTION_T1185332314_H
#define OUTOFSPACEEXCEPTION_T1185332314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.CodedOutputStream/OutOfSpaceException
struct  OutOfSpaceException_t1185332314  : public IOException_t4088381929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTOFSPACEEXCEPTION_T1185332314_H
#ifndef PEERBASE_T2956237011_H
#define PEERBASE_T2956237011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase
struct  PeerBase_t2956237011  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.PhotonPeer ExitGames.Client.Photon.PeerBase::ppeer
	PhotonPeer_t1608153861 * ___ppeer_0;
	// ExitGames.Client.Photon.IProtocol ExitGames.Client.Photon.PeerBase::protocol
	IProtocol_t1394662050 * ___protocol_1;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PeerBase::usedProtocol
	uint8_t ___usedProtocol_2;
	// ExitGames.Client.Photon.IPhotonSocket ExitGames.Client.Photon.PeerBase::rt
	IPhotonSocket_t2066969247 * ___rt_3;
	// System.String ExitGames.Client.Photon.PeerBase::<ServerAddress>k__BackingField
	String_t* ___U3CServerAddressU3Ek__BackingField_4;
	// System.String ExitGames.Client.Photon.PeerBase::<HttpUrlParameters>k__BackingField
	String_t* ___U3CHttpUrlParametersU3Ek__BackingField_5;
	// System.Int32 ExitGames.Client.Photon.PeerBase::ByteCountLastOperation
	int32_t ___ByteCountLastOperation_6;
	// System.Int32 ExitGames.Client.Photon.PeerBase::ByteCountCurrentDispatch
	int32_t ___ByteCountCurrentDispatch_7;
	// ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.PeerBase::CommandInCurrentDispatch
	NCommand_t1230688399 * ___CommandInCurrentDispatch_8;
	// System.Int32 ExitGames.Client.Photon.PeerBase::TrafficPackageHeaderSize
	int32_t ___TrafficPackageHeaderSize_9;
	// System.Int32 ExitGames.Client.Photon.PeerBase::packetLossByCrc
	int32_t ___packetLossByCrc_10;
	// System.Int32 ExitGames.Client.Photon.PeerBase::packetLossByChallenge
	int32_t ___packetLossByChallenge_11;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction> ExitGames.Client.Photon.PeerBase::ActionQueue
	Queue_1_t2309151397 * ___ActionQueue_12;
	// System.Int16 ExitGames.Client.Photon.PeerBase::peerID
	int16_t ___peerID_13;
	// ExitGames.Client.Photon.PeerBase/ConnectionStateValue ExitGames.Client.Photon.PeerBase::peerConnectionState
	uint8_t ___peerConnectionState_14;
	// System.Int32 ExitGames.Client.Photon.PeerBase::serverTimeOffset
	int32_t ___serverTimeOffset_15;
	// System.Boolean ExitGames.Client.Photon.PeerBase::serverTimeOffsetIsAvailable
	bool ___serverTimeOffsetIsAvailable_16;
	// System.Int32 ExitGames.Client.Photon.PeerBase::roundTripTime
	int32_t ___roundTripTime_17;
	// System.Int32 ExitGames.Client.Photon.PeerBase::roundTripTimeVariance
	int32_t ___roundTripTimeVariance_18;
	// System.Int32 ExitGames.Client.Photon.PeerBase::lastRoundTripTime
	int32_t ___lastRoundTripTime_19;
	// System.Int32 ExitGames.Client.Photon.PeerBase::lowestRoundTripTime
	int32_t ___lowestRoundTripTime_20;
	// System.Int32 ExitGames.Client.Photon.PeerBase::lastRoundTripTimeVariance
	int32_t ___lastRoundTripTimeVariance_21;
	// System.Int32 ExitGames.Client.Photon.PeerBase::highestRoundTripTimeVariance
	int32_t ___highestRoundTripTimeVariance_22;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timestampOfLastReceive
	int32_t ___timestampOfLastReceive_23;
	// System.Int32 ExitGames.Client.Photon.PeerBase::packetThrottleInterval
	int32_t ___packetThrottleInterval_24;
	// System.Int64 ExitGames.Client.Photon.PeerBase::bytesOut
	int64_t ___bytesOut_26;
	// System.Int64 ExitGames.Client.Photon.PeerBase::bytesIn
	int64_t ___bytesIn_27;
	// System.Int32 ExitGames.Client.Photon.PeerBase::commandBufferSize
	int32_t ___commandBufferSize_28;
	// Photon.SocketServer.Security.ICryptoProvider ExitGames.Client.Photon.PeerBase::CryptoProvider
	RuntimeObject* ___CryptoProvider_29;
	// System.Random ExitGames.Client.Photon.PeerBase::lagRandomizer
	Random_t108471755 * ___lagRandomizer_30;
	// System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem> ExitGames.Client.Photon.PeerBase::NetSimListOutgoing
	LinkedList_1_t1884284488 * ___NetSimListOutgoing_31;
	// System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem> ExitGames.Client.Photon.PeerBase::NetSimListIncoming
	LinkedList_1_t1884284488 * ___NetSimListIncoming_32;
	// ExitGames.Client.Photon.NetworkSimulationSet ExitGames.Client.Photon.PeerBase::networkSimulationSettings
	NetworkSimulationSet_t2000596048 * ___networkSimulationSettings_33;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem> ExitGames.Client.Photon.PeerBase::CommandLog
	Queue_1_t4063950034 * ___CommandLog_34;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem> ExitGames.Client.Photon.PeerBase::InReliableLog
	Queue_1_t4063950034 * ___InReliableLog_35;
	// System.Object ExitGames.Client.Photon.PeerBase::CustomInitData
	RuntimeObject * ___CustomInitData_36;
	// System.String ExitGames.Client.Photon.PeerBase::AppId
	String_t* ___AppId_37;
	// System.Byte[] ExitGames.Client.Photon.PeerBase::<TcpConnectionPrefix>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CTcpConnectionPrefixU3Ek__BackingField_38;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeBase
	int32_t ___timeBase_39;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeInt
	int32_t ___timeInt_40;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeoutInt
	int32_t ___timeoutInt_41;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeLastAckReceive
	int32_t ___timeLastAckReceive_42;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeLastSendAck
	int32_t ___timeLastSendAck_43;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeLastSendOutgoing
	int32_t ___timeLastSendOutgoing_44;
	// System.Boolean ExitGames.Client.Photon.PeerBase::ApplicationIsInitialized
	bool ___ApplicationIsInitialized_48;
	// System.Boolean ExitGames.Client.Photon.PeerBase::isEncryptionAvailable
	bool ___isEncryptionAvailable_49;
	// System.Int32 ExitGames.Client.Photon.PeerBase::outgoingCommandsInStream
	int32_t ___outgoingCommandsInStream_50;
	// ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.PeerBase::SerializeMemStream
	StreamBuffer_t3827669789 * ___SerializeMemStream_51;

public:
	inline static int32_t get_offset_of_ppeer_0() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ppeer_0)); }
	inline PhotonPeer_t1608153861 * get_ppeer_0() const { return ___ppeer_0; }
	inline PhotonPeer_t1608153861 ** get_address_of_ppeer_0() { return &___ppeer_0; }
	inline void set_ppeer_0(PhotonPeer_t1608153861 * value)
	{
		___ppeer_0 = value;
		Il2CppCodeGenWriteBarrier((&___ppeer_0), value);
	}

	inline static int32_t get_offset_of_protocol_1() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___protocol_1)); }
	inline IProtocol_t1394662050 * get_protocol_1() const { return ___protocol_1; }
	inline IProtocol_t1394662050 ** get_address_of_protocol_1() { return &___protocol_1; }
	inline void set_protocol_1(IProtocol_t1394662050 * value)
	{
		___protocol_1 = value;
		Il2CppCodeGenWriteBarrier((&___protocol_1), value);
	}

	inline static int32_t get_offset_of_usedProtocol_2() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___usedProtocol_2)); }
	inline uint8_t get_usedProtocol_2() const { return ___usedProtocol_2; }
	inline uint8_t* get_address_of_usedProtocol_2() { return &___usedProtocol_2; }
	inline void set_usedProtocol_2(uint8_t value)
	{
		___usedProtocol_2 = value;
	}

	inline static int32_t get_offset_of_rt_3() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___rt_3)); }
	inline IPhotonSocket_t2066969247 * get_rt_3() const { return ___rt_3; }
	inline IPhotonSocket_t2066969247 ** get_address_of_rt_3() { return &___rt_3; }
	inline void set_rt_3(IPhotonSocket_t2066969247 * value)
	{
		___rt_3 = value;
		Il2CppCodeGenWriteBarrier((&___rt_3), value);
	}

	inline static int32_t get_offset_of_U3CServerAddressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___U3CServerAddressU3Ek__BackingField_4)); }
	inline String_t* get_U3CServerAddressU3Ek__BackingField_4() const { return ___U3CServerAddressU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CServerAddressU3Ek__BackingField_4() { return &___U3CServerAddressU3Ek__BackingField_4; }
	inline void set_U3CServerAddressU3Ek__BackingField_4(String_t* value)
	{
		___U3CServerAddressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServerAddressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHttpUrlParametersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___U3CHttpUrlParametersU3Ek__BackingField_5)); }
	inline String_t* get_U3CHttpUrlParametersU3Ek__BackingField_5() const { return ___U3CHttpUrlParametersU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CHttpUrlParametersU3Ek__BackingField_5() { return &___U3CHttpUrlParametersU3Ek__BackingField_5; }
	inline void set_U3CHttpUrlParametersU3Ek__BackingField_5(String_t* value)
	{
		___U3CHttpUrlParametersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHttpUrlParametersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_ByteCountLastOperation_6() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ByteCountLastOperation_6)); }
	inline int32_t get_ByteCountLastOperation_6() const { return ___ByteCountLastOperation_6; }
	inline int32_t* get_address_of_ByteCountLastOperation_6() { return &___ByteCountLastOperation_6; }
	inline void set_ByteCountLastOperation_6(int32_t value)
	{
		___ByteCountLastOperation_6 = value;
	}

	inline static int32_t get_offset_of_ByteCountCurrentDispatch_7() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ByteCountCurrentDispatch_7)); }
	inline int32_t get_ByteCountCurrentDispatch_7() const { return ___ByteCountCurrentDispatch_7; }
	inline int32_t* get_address_of_ByteCountCurrentDispatch_7() { return &___ByteCountCurrentDispatch_7; }
	inline void set_ByteCountCurrentDispatch_7(int32_t value)
	{
		___ByteCountCurrentDispatch_7 = value;
	}

	inline static int32_t get_offset_of_CommandInCurrentDispatch_8() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___CommandInCurrentDispatch_8)); }
	inline NCommand_t1230688399 * get_CommandInCurrentDispatch_8() const { return ___CommandInCurrentDispatch_8; }
	inline NCommand_t1230688399 ** get_address_of_CommandInCurrentDispatch_8() { return &___CommandInCurrentDispatch_8; }
	inline void set_CommandInCurrentDispatch_8(NCommand_t1230688399 * value)
	{
		___CommandInCurrentDispatch_8 = value;
		Il2CppCodeGenWriteBarrier((&___CommandInCurrentDispatch_8), value);
	}

	inline static int32_t get_offset_of_TrafficPackageHeaderSize_9() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___TrafficPackageHeaderSize_9)); }
	inline int32_t get_TrafficPackageHeaderSize_9() const { return ___TrafficPackageHeaderSize_9; }
	inline int32_t* get_address_of_TrafficPackageHeaderSize_9() { return &___TrafficPackageHeaderSize_9; }
	inline void set_TrafficPackageHeaderSize_9(int32_t value)
	{
		___TrafficPackageHeaderSize_9 = value;
	}

	inline static int32_t get_offset_of_packetLossByCrc_10() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___packetLossByCrc_10)); }
	inline int32_t get_packetLossByCrc_10() const { return ___packetLossByCrc_10; }
	inline int32_t* get_address_of_packetLossByCrc_10() { return &___packetLossByCrc_10; }
	inline void set_packetLossByCrc_10(int32_t value)
	{
		___packetLossByCrc_10 = value;
	}

	inline static int32_t get_offset_of_packetLossByChallenge_11() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___packetLossByChallenge_11)); }
	inline int32_t get_packetLossByChallenge_11() const { return ___packetLossByChallenge_11; }
	inline int32_t* get_address_of_packetLossByChallenge_11() { return &___packetLossByChallenge_11; }
	inline void set_packetLossByChallenge_11(int32_t value)
	{
		___packetLossByChallenge_11 = value;
	}

	inline static int32_t get_offset_of_ActionQueue_12() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ActionQueue_12)); }
	inline Queue_1_t2309151397 * get_ActionQueue_12() const { return ___ActionQueue_12; }
	inline Queue_1_t2309151397 ** get_address_of_ActionQueue_12() { return &___ActionQueue_12; }
	inline void set_ActionQueue_12(Queue_1_t2309151397 * value)
	{
		___ActionQueue_12 = value;
		Il2CppCodeGenWriteBarrier((&___ActionQueue_12), value);
	}

	inline static int32_t get_offset_of_peerID_13() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___peerID_13)); }
	inline int16_t get_peerID_13() const { return ___peerID_13; }
	inline int16_t* get_address_of_peerID_13() { return &___peerID_13; }
	inline void set_peerID_13(int16_t value)
	{
		___peerID_13 = value;
	}

	inline static int32_t get_offset_of_peerConnectionState_14() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___peerConnectionState_14)); }
	inline uint8_t get_peerConnectionState_14() const { return ___peerConnectionState_14; }
	inline uint8_t* get_address_of_peerConnectionState_14() { return &___peerConnectionState_14; }
	inline void set_peerConnectionState_14(uint8_t value)
	{
		___peerConnectionState_14 = value;
	}

	inline static int32_t get_offset_of_serverTimeOffset_15() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___serverTimeOffset_15)); }
	inline int32_t get_serverTimeOffset_15() const { return ___serverTimeOffset_15; }
	inline int32_t* get_address_of_serverTimeOffset_15() { return &___serverTimeOffset_15; }
	inline void set_serverTimeOffset_15(int32_t value)
	{
		___serverTimeOffset_15 = value;
	}

	inline static int32_t get_offset_of_serverTimeOffsetIsAvailable_16() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___serverTimeOffsetIsAvailable_16)); }
	inline bool get_serverTimeOffsetIsAvailable_16() const { return ___serverTimeOffsetIsAvailable_16; }
	inline bool* get_address_of_serverTimeOffsetIsAvailable_16() { return &___serverTimeOffsetIsAvailable_16; }
	inline void set_serverTimeOffsetIsAvailable_16(bool value)
	{
		___serverTimeOffsetIsAvailable_16 = value;
	}

	inline static int32_t get_offset_of_roundTripTime_17() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___roundTripTime_17)); }
	inline int32_t get_roundTripTime_17() const { return ___roundTripTime_17; }
	inline int32_t* get_address_of_roundTripTime_17() { return &___roundTripTime_17; }
	inline void set_roundTripTime_17(int32_t value)
	{
		___roundTripTime_17 = value;
	}

	inline static int32_t get_offset_of_roundTripTimeVariance_18() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___roundTripTimeVariance_18)); }
	inline int32_t get_roundTripTimeVariance_18() const { return ___roundTripTimeVariance_18; }
	inline int32_t* get_address_of_roundTripTimeVariance_18() { return &___roundTripTimeVariance_18; }
	inline void set_roundTripTimeVariance_18(int32_t value)
	{
		___roundTripTimeVariance_18 = value;
	}

	inline static int32_t get_offset_of_lastRoundTripTime_19() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___lastRoundTripTime_19)); }
	inline int32_t get_lastRoundTripTime_19() const { return ___lastRoundTripTime_19; }
	inline int32_t* get_address_of_lastRoundTripTime_19() { return &___lastRoundTripTime_19; }
	inline void set_lastRoundTripTime_19(int32_t value)
	{
		___lastRoundTripTime_19 = value;
	}

	inline static int32_t get_offset_of_lowestRoundTripTime_20() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___lowestRoundTripTime_20)); }
	inline int32_t get_lowestRoundTripTime_20() const { return ___lowestRoundTripTime_20; }
	inline int32_t* get_address_of_lowestRoundTripTime_20() { return &___lowestRoundTripTime_20; }
	inline void set_lowestRoundTripTime_20(int32_t value)
	{
		___lowestRoundTripTime_20 = value;
	}

	inline static int32_t get_offset_of_lastRoundTripTimeVariance_21() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___lastRoundTripTimeVariance_21)); }
	inline int32_t get_lastRoundTripTimeVariance_21() const { return ___lastRoundTripTimeVariance_21; }
	inline int32_t* get_address_of_lastRoundTripTimeVariance_21() { return &___lastRoundTripTimeVariance_21; }
	inline void set_lastRoundTripTimeVariance_21(int32_t value)
	{
		___lastRoundTripTimeVariance_21 = value;
	}

	inline static int32_t get_offset_of_highestRoundTripTimeVariance_22() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___highestRoundTripTimeVariance_22)); }
	inline int32_t get_highestRoundTripTimeVariance_22() const { return ___highestRoundTripTimeVariance_22; }
	inline int32_t* get_address_of_highestRoundTripTimeVariance_22() { return &___highestRoundTripTimeVariance_22; }
	inline void set_highestRoundTripTimeVariance_22(int32_t value)
	{
		___highestRoundTripTimeVariance_22 = value;
	}

	inline static int32_t get_offset_of_timestampOfLastReceive_23() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timestampOfLastReceive_23)); }
	inline int32_t get_timestampOfLastReceive_23() const { return ___timestampOfLastReceive_23; }
	inline int32_t* get_address_of_timestampOfLastReceive_23() { return &___timestampOfLastReceive_23; }
	inline void set_timestampOfLastReceive_23(int32_t value)
	{
		___timestampOfLastReceive_23 = value;
	}

	inline static int32_t get_offset_of_packetThrottleInterval_24() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___packetThrottleInterval_24)); }
	inline int32_t get_packetThrottleInterval_24() const { return ___packetThrottleInterval_24; }
	inline int32_t* get_address_of_packetThrottleInterval_24() { return &___packetThrottleInterval_24; }
	inline void set_packetThrottleInterval_24(int32_t value)
	{
		___packetThrottleInterval_24 = value;
	}

	inline static int32_t get_offset_of_bytesOut_26() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___bytesOut_26)); }
	inline int64_t get_bytesOut_26() const { return ___bytesOut_26; }
	inline int64_t* get_address_of_bytesOut_26() { return &___bytesOut_26; }
	inline void set_bytesOut_26(int64_t value)
	{
		___bytesOut_26 = value;
	}

	inline static int32_t get_offset_of_bytesIn_27() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___bytesIn_27)); }
	inline int64_t get_bytesIn_27() const { return ___bytesIn_27; }
	inline int64_t* get_address_of_bytesIn_27() { return &___bytesIn_27; }
	inline void set_bytesIn_27(int64_t value)
	{
		___bytesIn_27 = value;
	}

	inline static int32_t get_offset_of_commandBufferSize_28() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___commandBufferSize_28)); }
	inline int32_t get_commandBufferSize_28() const { return ___commandBufferSize_28; }
	inline int32_t* get_address_of_commandBufferSize_28() { return &___commandBufferSize_28; }
	inline void set_commandBufferSize_28(int32_t value)
	{
		___commandBufferSize_28 = value;
	}

	inline static int32_t get_offset_of_CryptoProvider_29() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___CryptoProvider_29)); }
	inline RuntimeObject* get_CryptoProvider_29() const { return ___CryptoProvider_29; }
	inline RuntimeObject** get_address_of_CryptoProvider_29() { return &___CryptoProvider_29; }
	inline void set_CryptoProvider_29(RuntimeObject* value)
	{
		___CryptoProvider_29 = value;
		Il2CppCodeGenWriteBarrier((&___CryptoProvider_29), value);
	}

	inline static int32_t get_offset_of_lagRandomizer_30() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___lagRandomizer_30)); }
	inline Random_t108471755 * get_lagRandomizer_30() const { return ___lagRandomizer_30; }
	inline Random_t108471755 ** get_address_of_lagRandomizer_30() { return &___lagRandomizer_30; }
	inline void set_lagRandomizer_30(Random_t108471755 * value)
	{
		___lagRandomizer_30 = value;
		Il2CppCodeGenWriteBarrier((&___lagRandomizer_30), value);
	}

	inline static int32_t get_offset_of_NetSimListOutgoing_31() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___NetSimListOutgoing_31)); }
	inline LinkedList_1_t1884284488 * get_NetSimListOutgoing_31() const { return ___NetSimListOutgoing_31; }
	inline LinkedList_1_t1884284488 ** get_address_of_NetSimListOutgoing_31() { return &___NetSimListOutgoing_31; }
	inline void set_NetSimListOutgoing_31(LinkedList_1_t1884284488 * value)
	{
		___NetSimListOutgoing_31 = value;
		Il2CppCodeGenWriteBarrier((&___NetSimListOutgoing_31), value);
	}

	inline static int32_t get_offset_of_NetSimListIncoming_32() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___NetSimListIncoming_32)); }
	inline LinkedList_1_t1884284488 * get_NetSimListIncoming_32() const { return ___NetSimListIncoming_32; }
	inline LinkedList_1_t1884284488 ** get_address_of_NetSimListIncoming_32() { return &___NetSimListIncoming_32; }
	inline void set_NetSimListIncoming_32(LinkedList_1_t1884284488 * value)
	{
		___NetSimListIncoming_32 = value;
		Il2CppCodeGenWriteBarrier((&___NetSimListIncoming_32), value);
	}

	inline static int32_t get_offset_of_networkSimulationSettings_33() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___networkSimulationSettings_33)); }
	inline NetworkSimulationSet_t2000596048 * get_networkSimulationSettings_33() const { return ___networkSimulationSettings_33; }
	inline NetworkSimulationSet_t2000596048 ** get_address_of_networkSimulationSettings_33() { return &___networkSimulationSettings_33; }
	inline void set_networkSimulationSettings_33(NetworkSimulationSet_t2000596048 * value)
	{
		___networkSimulationSettings_33 = value;
		Il2CppCodeGenWriteBarrier((&___networkSimulationSettings_33), value);
	}

	inline static int32_t get_offset_of_CommandLog_34() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___CommandLog_34)); }
	inline Queue_1_t4063950034 * get_CommandLog_34() const { return ___CommandLog_34; }
	inline Queue_1_t4063950034 ** get_address_of_CommandLog_34() { return &___CommandLog_34; }
	inline void set_CommandLog_34(Queue_1_t4063950034 * value)
	{
		___CommandLog_34 = value;
		Il2CppCodeGenWriteBarrier((&___CommandLog_34), value);
	}

	inline static int32_t get_offset_of_InReliableLog_35() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___InReliableLog_35)); }
	inline Queue_1_t4063950034 * get_InReliableLog_35() const { return ___InReliableLog_35; }
	inline Queue_1_t4063950034 ** get_address_of_InReliableLog_35() { return &___InReliableLog_35; }
	inline void set_InReliableLog_35(Queue_1_t4063950034 * value)
	{
		___InReliableLog_35 = value;
		Il2CppCodeGenWriteBarrier((&___InReliableLog_35), value);
	}

	inline static int32_t get_offset_of_CustomInitData_36() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___CustomInitData_36)); }
	inline RuntimeObject * get_CustomInitData_36() const { return ___CustomInitData_36; }
	inline RuntimeObject ** get_address_of_CustomInitData_36() { return &___CustomInitData_36; }
	inline void set_CustomInitData_36(RuntimeObject * value)
	{
		___CustomInitData_36 = value;
		Il2CppCodeGenWriteBarrier((&___CustomInitData_36), value);
	}

	inline static int32_t get_offset_of_AppId_37() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___AppId_37)); }
	inline String_t* get_AppId_37() const { return ___AppId_37; }
	inline String_t** get_address_of_AppId_37() { return &___AppId_37; }
	inline void set_AppId_37(String_t* value)
	{
		___AppId_37 = value;
		Il2CppCodeGenWriteBarrier((&___AppId_37), value);
	}

	inline static int32_t get_offset_of_U3CTcpConnectionPrefixU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___U3CTcpConnectionPrefixU3Ek__BackingField_38)); }
	inline ByteU5BU5D_t4116647657* get_U3CTcpConnectionPrefixU3Ek__BackingField_38() const { return ___U3CTcpConnectionPrefixU3Ek__BackingField_38; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CTcpConnectionPrefixU3Ek__BackingField_38() { return &___U3CTcpConnectionPrefixU3Ek__BackingField_38; }
	inline void set_U3CTcpConnectionPrefixU3Ek__BackingField_38(ByteU5BU5D_t4116647657* value)
	{
		___U3CTcpConnectionPrefixU3Ek__BackingField_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTcpConnectionPrefixU3Ek__BackingField_38), value);
	}

	inline static int32_t get_offset_of_timeBase_39() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeBase_39)); }
	inline int32_t get_timeBase_39() const { return ___timeBase_39; }
	inline int32_t* get_address_of_timeBase_39() { return &___timeBase_39; }
	inline void set_timeBase_39(int32_t value)
	{
		___timeBase_39 = value;
	}

	inline static int32_t get_offset_of_timeInt_40() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeInt_40)); }
	inline int32_t get_timeInt_40() const { return ___timeInt_40; }
	inline int32_t* get_address_of_timeInt_40() { return &___timeInt_40; }
	inline void set_timeInt_40(int32_t value)
	{
		___timeInt_40 = value;
	}

	inline static int32_t get_offset_of_timeoutInt_41() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeoutInt_41)); }
	inline int32_t get_timeoutInt_41() const { return ___timeoutInt_41; }
	inline int32_t* get_address_of_timeoutInt_41() { return &___timeoutInt_41; }
	inline void set_timeoutInt_41(int32_t value)
	{
		___timeoutInt_41 = value;
	}

	inline static int32_t get_offset_of_timeLastAckReceive_42() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeLastAckReceive_42)); }
	inline int32_t get_timeLastAckReceive_42() const { return ___timeLastAckReceive_42; }
	inline int32_t* get_address_of_timeLastAckReceive_42() { return &___timeLastAckReceive_42; }
	inline void set_timeLastAckReceive_42(int32_t value)
	{
		___timeLastAckReceive_42 = value;
	}

	inline static int32_t get_offset_of_timeLastSendAck_43() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeLastSendAck_43)); }
	inline int32_t get_timeLastSendAck_43() const { return ___timeLastSendAck_43; }
	inline int32_t* get_address_of_timeLastSendAck_43() { return &___timeLastSendAck_43; }
	inline void set_timeLastSendAck_43(int32_t value)
	{
		___timeLastSendAck_43 = value;
	}

	inline static int32_t get_offset_of_timeLastSendOutgoing_44() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeLastSendOutgoing_44)); }
	inline int32_t get_timeLastSendOutgoing_44() const { return ___timeLastSendOutgoing_44; }
	inline int32_t* get_address_of_timeLastSendOutgoing_44() { return &___timeLastSendOutgoing_44; }
	inline void set_timeLastSendOutgoing_44(int32_t value)
	{
		___timeLastSendOutgoing_44 = value;
	}

	inline static int32_t get_offset_of_ApplicationIsInitialized_48() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ApplicationIsInitialized_48)); }
	inline bool get_ApplicationIsInitialized_48() const { return ___ApplicationIsInitialized_48; }
	inline bool* get_address_of_ApplicationIsInitialized_48() { return &___ApplicationIsInitialized_48; }
	inline void set_ApplicationIsInitialized_48(bool value)
	{
		___ApplicationIsInitialized_48 = value;
	}

	inline static int32_t get_offset_of_isEncryptionAvailable_49() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___isEncryptionAvailable_49)); }
	inline bool get_isEncryptionAvailable_49() const { return ___isEncryptionAvailable_49; }
	inline bool* get_address_of_isEncryptionAvailable_49() { return &___isEncryptionAvailable_49; }
	inline void set_isEncryptionAvailable_49(bool value)
	{
		___isEncryptionAvailable_49 = value;
	}

	inline static int32_t get_offset_of_outgoingCommandsInStream_50() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___outgoingCommandsInStream_50)); }
	inline int32_t get_outgoingCommandsInStream_50() const { return ___outgoingCommandsInStream_50; }
	inline int32_t* get_address_of_outgoingCommandsInStream_50() { return &___outgoingCommandsInStream_50; }
	inline void set_outgoingCommandsInStream_50(int32_t value)
	{
		___outgoingCommandsInStream_50 = value;
	}

	inline static int32_t get_offset_of_SerializeMemStream_51() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___SerializeMemStream_51)); }
	inline StreamBuffer_t3827669789 * get_SerializeMemStream_51() const { return ___SerializeMemStream_51; }
	inline StreamBuffer_t3827669789 ** get_address_of_SerializeMemStream_51() { return &___SerializeMemStream_51; }
	inline void set_SerializeMemStream_51(StreamBuffer_t3827669789 * value)
	{
		___SerializeMemStream_51 = value;
		Il2CppCodeGenWriteBarrier((&___SerializeMemStream_51), value);
	}
};

struct PeerBase_t2956237011_StaticFields
{
public:
	// System.Int16 ExitGames.Client.Photon.PeerBase::peerCount
	int16_t ___peerCount_25;

public:
	inline static int32_t get_offset_of_peerCount_25() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011_StaticFields, ___peerCount_25)); }
	inline int16_t get_peerCount_25() const { return ___peerCount_25; }
	inline int16_t* get_address_of_peerCount_25() { return &___peerCount_25; }
	inline void set_peerCount_25(int16_t value)
	{
		___peerCount_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEERBASE_T2956237011_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef PHOTONPEER_T1608153861_H
#define PHOTONPEER_T1608153861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonPeer
struct  PhotonPeer_t1608153861  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ClientSdkId
	uint8_t ___ClientSdkId_3;
	// System.String ExitGames.Client.Photon.PhotonPeer::clientVersion
	String_t* ___clientVersion_5;
	// ExitGames.Client.Photon.SerializationProtocol ExitGames.Client.Photon.PhotonPeer::<SerializationProtocolType>k__BackingField
	int32_t ___U3CSerializationProtocolTypeU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type> ExitGames.Client.Photon.PhotonPeer::SocketImplementationConfig
	Dictionary_2_t1253839074 * ___SocketImplementationConfig_7;
	// System.Type ExitGames.Client.Photon.PhotonPeer::<SocketImplementation>k__BackingField
	Type_t * ___U3CSocketImplementationU3Ek__BackingField_8;
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PhotonPeer::DebugOut
	uint8_t ___DebugOut_9;
	// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::<Listener>k__BackingField
	RuntimeObject* ___U3CListenerU3Ek__BackingField_10;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsIncoming>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsIncomingU3Ek__BackingField_11;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsOutgoing>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsOutgoingU3Ek__BackingField_12;
	// ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::<TrafficStatsGameLevel>k__BackingField
	TrafficStatsGameLevel_t4013908777 * ___U3CTrafficStatsGameLevelU3Ek__BackingField_13;
	// System.Diagnostics.Stopwatch ExitGames.Client.Photon.PhotonPeer::trafficStatsStopwatch
	Stopwatch_t305734070 * ___trafficStatsStopwatch_14;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::trafficStatsEnabled
	bool ___trafficStatsEnabled_15;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::commandLogSize
	int32_t ___commandLogSize_16;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<EnableServerTracing>k__BackingField
	bool ___U3CEnableServerTracingU3Ek__BackingField_17;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::quickResendAttempts
	uint8_t ___quickResendAttempts_18;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMinConnections
	int32_t ___RhttpMinConnections_19;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMaxConnections
	int32_t ___RhttpMaxConnections_20;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::<LimitOfUnreliableCommands>k__BackingField
	int32_t ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ChannelCount
	uint8_t ___ChannelCount_22;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::crcEnabled
	bool ___crcEnabled_23;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::WarningSize
	int32_t ___WarningSize_24;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::SentCountAllowance
	int32_t ___SentCountAllowance_25;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::TimePingInterval
	int32_t ___TimePingInterval_26;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::DisconnectTimeout
	int32_t ___DisconnectTimeout_27;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::<TransportProtocol>k__BackingField
	uint8_t ___U3CTransportProtocolU3Ek__BackingField_28;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::mtu
	int32_t ___mtu_30;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<IsSendingOnlyAcks>k__BackingField
	bool ___U3CIsSendingOnlyAcksU3Ek__BackingField_31;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PhotonPeer::peerBase
	PeerBase_t2956237011 * ___peerBase_32;
	// System.Object ExitGames.Client.Photon.PhotonPeer::SendOutgoingLockObject
	RuntimeObject * ___SendOutgoingLockObject_33;
	// System.Object ExitGames.Client.Photon.PhotonPeer::DispatchLockObject
	RuntimeObject * ___DispatchLockObject_34;
	// System.Object ExitGames.Client.Photon.PhotonPeer::EnqueueLock
	RuntimeObject * ___EnqueueLock_35;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::PayloadEncryptionSecret
	ByteU5BU5D_t4116647657* ___PayloadEncryptionSecret_36;
	// ExitGames.Client.Photon.EncryptorManaged.Encryptor ExitGames.Client.Photon.PhotonPeer::encryptor
	Encryptor_t200327285 * ___encryptor_37;
	// ExitGames.Client.Photon.EncryptorManaged.Decryptor ExitGames.Client.Photon.PhotonPeer::decryptor
	Decryptor_t2116099858 * ___decryptor_38;

public:
	inline static int32_t get_offset_of_ClientSdkId_3() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ClientSdkId_3)); }
	inline uint8_t get_ClientSdkId_3() const { return ___ClientSdkId_3; }
	inline uint8_t* get_address_of_ClientSdkId_3() { return &___ClientSdkId_3; }
	inline void set_ClientSdkId_3(uint8_t value)
	{
		___ClientSdkId_3 = value;
	}

	inline static int32_t get_offset_of_clientVersion_5() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___clientVersion_5)); }
	inline String_t* get_clientVersion_5() const { return ___clientVersion_5; }
	inline String_t** get_address_of_clientVersion_5() { return &___clientVersion_5; }
	inline void set_clientVersion_5(String_t* value)
	{
		___clientVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___clientVersion_5), value);
	}

	inline static int32_t get_offset_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSerializationProtocolTypeU3Ek__BackingField_6)); }
	inline int32_t get_U3CSerializationProtocolTypeU3Ek__BackingField_6() const { return ___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return &___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline void set_U3CSerializationProtocolTypeU3Ek__BackingField_6(int32_t value)
	{
		___U3CSerializationProtocolTypeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_SocketImplementationConfig_7() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SocketImplementationConfig_7)); }
	inline Dictionary_2_t1253839074 * get_SocketImplementationConfig_7() const { return ___SocketImplementationConfig_7; }
	inline Dictionary_2_t1253839074 ** get_address_of_SocketImplementationConfig_7() { return &___SocketImplementationConfig_7; }
	inline void set_SocketImplementationConfig_7(Dictionary_2_t1253839074 * value)
	{
		___SocketImplementationConfig_7 = value;
		Il2CppCodeGenWriteBarrier((&___SocketImplementationConfig_7), value);
	}

	inline static int32_t get_offset_of_U3CSocketImplementationU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSocketImplementationU3Ek__BackingField_8)); }
	inline Type_t * get_U3CSocketImplementationU3Ek__BackingField_8() const { return ___U3CSocketImplementationU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CSocketImplementationU3Ek__BackingField_8() { return &___U3CSocketImplementationU3Ek__BackingField_8; }
	inline void set_U3CSocketImplementationU3Ek__BackingField_8(Type_t * value)
	{
		___U3CSocketImplementationU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketImplementationU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_DebugOut_9() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DebugOut_9)); }
	inline uint8_t get_DebugOut_9() const { return ___DebugOut_9; }
	inline uint8_t* get_address_of_DebugOut_9() { return &___DebugOut_9; }
	inline void set_DebugOut_9(uint8_t value)
	{
		___DebugOut_9 = value;
	}

	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CListenerU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CListenerU3Ek__BackingField_10() const { return ___U3CListenerU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CListenerU3Ek__BackingField_10() { return &___U3CListenerU3Ek__BackingField_10; }
	inline void set_U3CListenerU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CListenerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListenerU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsIncomingU3Ek__BackingField_11)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsIncomingU3Ek__BackingField_11() const { return ___U3CTrafficStatsIncomingU3Ek__BackingField_11; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsIncomingU3Ek__BackingField_11() { return &___U3CTrafficStatsIncomingU3Ek__BackingField_11; }
	inline void set_U3CTrafficStatsIncomingU3Ek__BackingField_11(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsIncomingU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsIncomingU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsOutgoingU3Ek__BackingField_12)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsOutgoingU3Ek__BackingField_12() const { return ___U3CTrafficStatsOutgoingU3Ek__BackingField_12; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsOutgoingU3Ek__BackingField_12() { return &___U3CTrafficStatsOutgoingU3Ek__BackingField_12; }
	inline void set_U3CTrafficStatsOutgoingU3Ek__BackingField_12(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsOutgoingU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsOutgoingU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsGameLevelU3Ek__BackingField_13)); }
	inline TrafficStatsGameLevel_t4013908777 * get_U3CTrafficStatsGameLevelU3Ek__BackingField_13() const { return ___U3CTrafficStatsGameLevelU3Ek__BackingField_13; }
	inline TrafficStatsGameLevel_t4013908777 ** get_address_of_U3CTrafficStatsGameLevelU3Ek__BackingField_13() { return &___U3CTrafficStatsGameLevelU3Ek__BackingField_13; }
	inline void set_U3CTrafficStatsGameLevelU3Ek__BackingField_13(TrafficStatsGameLevel_t4013908777 * value)
	{
		___U3CTrafficStatsGameLevelU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsGameLevelU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_trafficStatsStopwatch_14() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsStopwatch_14)); }
	inline Stopwatch_t305734070 * get_trafficStatsStopwatch_14() const { return ___trafficStatsStopwatch_14; }
	inline Stopwatch_t305734070 ** get_address_of_trafficStatsStopwatch_14() { return &___trafficStatsStopwatch_14; }
	inline void set_trafficStatsStopwatch_14(Stopwatch_t305734070 * value)
	{
		___trafficStatsStopwatch_14 = value;
		Il2CppCodeGenWriteBarrier((&___trafficStatsStopwatch_14), value);
	}

	inline static int32_t get_offset_of_trafficStatsEnabled_15() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsEnabled_15)); }
	inline bool get_trafficStatsEnabled_15() const { return ___trafficStatsEnabled_15; }
	inline bool* get_address_of_trafficStatsEnabled_15() { return &___trafficStatsEnabled_15; }
	inline void set_trafficStatsEnabled_15(bool value)
	{
		___trafficStatsEnabled_15 = value;
	}

	inline static int32_t get_offset_of_commandLogSize_16() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___commandLogSize_16)); }
	inline int32_t get_commandLogSize_16() const { return ___commandLogSize_16; }
	inline int32_t* get_address_of_commandLogSize_16() { return &___commandLogSize_16; }
	inline void set_commandLogSize_16(int32_t value)
	{
		___commandLogSize_16 = value;
	}

	inline static int32_t get_offset_of_U3CEnableServerTracingU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CEnableServerTracingU3Ek__BackingField_17)); }
	inline bool get_U3CEnableServerTracingU3Ek__BackingField_17() const { return ___U3CEnableServerTracingU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CEnableServerTracingU3Ek__BackingField_17() { return &___U3CEnableServerTracingU3Ek__BackingField_17; }
	inline void set_U3CEnableServerTracingU3Ek__BackingField_17(bool value)
	{
		___U3CEnableServerTracingU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_quickResendAttempts_18() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___quickResendAttempts_18)); }
	inline uint8_t get_quickResendAttempts_18() const { return ___quickResendAttempts_18; }
	inline uint8_t* get_address_of_quickResendAttempts_18() { return &___quickResendAttempts_18; }
	inline void set_quickResendAttempts_18(uint8_t value)
	{
		___quickResendAttempts_18 = value;
	}

	inline static int32_t get_offset_of_RhttpMinConnections_19() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMinConnections_19)); }
	inline int32_t get_RhttpMinConnections_19() const { return ___RhttpMinConnections_19; }
	inline int32_t* get_address_of_RhttpMinConnections_19() { return &___RhttpMinConnections_19; }
	inline void set_RhttpMinConnections_19(int32_t value)
	{
		___RhttpMinConnections_19 = value;
	}

	inline static int32_t get_offset_of_RhttpMaxConnections_20() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMaxConnections_20)); }
	inline int32_t get_RhttpMaxConnections_20() const { return ___RhttpMaxConnections_20; }
	inline int32_t* get_address_of_RhttpMaxConnections_20() { return &___RhttpMaxConnections_20; }
	inline void set_RhttpMaxConnections_20(int32_t value)
	{
		___RhttpMaxConnections_20 = value;
	}

	inline static int32_t get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21)); }
	inline int32_t get_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21() const { return ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21() { return &___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21; }
	inline void set_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21(int32_t value)
	{
		___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_ChannelCount_22() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ChannelCount_22)); }
	inline uint8_t get_ChannelCount_22() const { return ___ChannelCount_22; }
	inline uint8_t* get_address_of_ChannelCount_22() { return &___ChannelCount_22; }
	inline void set_ChannelCount_22(uint8_t value)
	{
		___ChannelCount_22 = value;
	}

	inline static int32_t get_offset_of_crcEnabled_23() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___crcEnabled_23)); }
	inline bool get_crcEnabled_23() const { return ___crcEnabled_23; }
	inline bool* get_address_of_crcEnabled_23() { return &___crcEnabled_23; }
	inline void set_crcEnabled_23(bool value)
	{
		___crcEnabled_23 = value;
	}

	inline static int32_t get_offset_of_WarningSize_24() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___WarningSize_24)); }
	inline int32_t get_WarningSize_24() const { return ___WarningSize_24; }
	inline int32_t* get_address_of_WarningSize_24() { return &___WarningSize_24; }
	inline void set_WarningSize_24(int32_t value)
	{
		___WarningSize_24 = value;
	}

	inline static int32_t get_offset_of_SentCountAllowance_25() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SentCountAllowance_25)); }
	inline int32_t get_SentCountAllowance_25() const { return ___SentCountAllowance_25; }
	inline int32_t* get_address_of_SentCountAllowance_25() { return &___SentCountAllowance_25; }
	inline void set_SentCountAllowance_25(int32_t value)
	{
		___SentCountAllowance_25 = value;
	}

	inline static int32_t get_offset_of_TimePingInterval_26() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___TimePingInterval_26)); }
	inline int32_t get_TimePingInterval_26() const { return ___TimePingInterval_26; }
	inline int32_t* get_address_of_TimePingInterval_26() { return &___TimePingInterval_26; }
	inline void set_TimePingInterval_26(int32_t value)
	{
		___TimePingInterval_26 = value;
	}

	inline static int32_t get_offset_of_DisconnectTimeout_27() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DisconnectTimeout_27)); }
	inline int32_t get_DisconnectTimeout_27() const { return ___DisconnectTimeout_27; }
	inline int32_t* get_address_of_DisconnectTimeout_27() { return &___DisconnectTimeout_27; }
	inline void set_DisconnectTimeout_27(int32_t value)
	{
		___DisconnectTimeout_27 = value;
	}

	inline static int32_t get_offset_of_U3CTransportProtocolU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTransportProtocolU3Ek__BackingField_28)); }
	inline uint8_t get_U3CTransportProtocolU3Ek__BackingField_28() const { return ___U3CTransportProtocolU3Ek__BackingField_28; }
	inline uint8_t* get_address_of_U3CTransportProtocolU3Ek__BackingField_28() { return &___U3CTransportProtocolU3Ek__BackingField_28; }
	inline void set_U3CTransportProtocolU3Ek__BackingField_28(uint8_t value)
	{
		___U3CTransportProtocolU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_mtu_30() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___mtu_30)); }
	inline int32_t get_mtu_30() const { return ___mtu_30; }
	inline int32_t* get_address_of_mtu_30() { return &___mtu_30; }
	inline void set_mtu_30(int32_t value)
	{
		___mtu_30 = value;
	}

	inline static int32_t get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CIsSendingOnlyAcksU3Ek__BackingField_31)); }
	inline bool get_U3CIsSendingOnlyAcksU3Ek__BackingField_31() const { return ___U3CIsSendingOnlyAcksU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CIsSendingOnlyAcksU3Ek__BackingField_31() { return &___U3CIsSendingOnlyAcksU3Ek__BackingField_31; }
	inline void set_U3CIsSendingOnlyAcksU3Ek__BackingField_31(bool value)
	{
		___U3CIsSendingOnlyAcksU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_peerBase_32() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___peerBase_32)); }
	inline PeerBase_t2956237011 * get_peerBase_32() const { return ___peerBase_32; }
	inline PeerBase_t2956237011 ** get_address_of_peerBase_32() { return &___peerBase_32; }
	inline void set_peerBase_32(PeerBase_t2956237011 * value)
	{
		___peerBase_32 = value;
		Il2CppCodeGenWriteBarrier((&___peerBase_32), value);
	}

	inline static int32_t get_offset_of_SendOutgoingLockObject_33() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SendOutgoingLockObject_33)); }
	inline RuntimeObject * get_SendOutgoingLockObject_33() const { return ___SendOutgoingLockObject_33; }
	inline RuntimeObject ** get_address_of_SendOutgoingLockObject_33() { return &___SendOutgoingLockObject_33; }
	inline void set_SendOutgoingLockObject_33(RuntimeObject * value)
	{
		___SendOutgoingLockObject_33 = value;
		Il2CppCodeGenWriteBarrier((&___SendOutgoingLockObject_33), value);
	}

	inline static int32_t get_offset_of_DispatchLockObject_34() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DispatchLockObject_34)); }
	inline RuntimeObject * get_DispatchLockObject_34() const { return ___DispatchLockObject_34; }
	inline RuntimeObject ** get_address_of_DispatchLockObject_34() { return &___DispatchLockObject_34; }
	inline void set_DispatchLockObject_34(RuntimeObject * value)
	{
		___DispatchLockObject_34 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchLockObject_34), value);
	}

	inline static int32_t get_offset_of_EnqueueLock_35() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___EnqueueLock_35)); }
	inline RuntimeObject * get_EnqueueLock_35() const { return ___EnqueueLock_35; }
	inline RuntimeObject ** get_address_of_EnqueueLock_35() { return &___EnqueueLock_35; }
	inline void set_EnqueueLock_35(RuntimeObject * value)
	{
		___EnqueueLock_35 = value;
		Il2CppCodeGenWriteBarrier((&___EnqueueLock_35), value);
	}

	inline static int32_t get_offset_of_PayloadEncryptionSecret_36() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___PayloadEncryptionSecret_36)); }
	inline ByteU5BU5D_t4116647657* get_PayloadEncryptionSecret_36() const { return ___PayloadEncryptionSecret_36; }
	inline ByteU5BU5D_t4116647657** get_address_of_PayloadEncryptionSecret_36() { return &___PayloadEncryptionSecret_36; }
	inline void set_PayloadEncryptionSecret_36(ByteU5BU5D_t4116647657* value)
	{
		___PayloadEncryptionSecret_36 = value;
		Il2CppCodeGenWriteBarrier((&___PayloadEncryptionSecret_36), value);
	}

	inline static int32_t get_offset_of_encryptor_37() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___encryptor_37)); }
	inline Encryptor_t200327285 * get_encryptor_37() const { return ___encryptor_37; }
	inline Encryptor_t200327285 ** get_address_of_encryptor_37() { return &___encryptor_37; }
	inline void set_encryptor_37(Encryptor_t200327285 * value)
	{
		___encryptor_37 = value;
		Il2CppCodeGenWriteBarrier((&___encryptor_37), value);
	}

	inline static int32_t get_offset_of_decryptor_38() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___decryptor_38)); }
	inline Decryptor_t2116099858 * get_decryptor_38() const { return ___decryptor_38; }
	inline Decryptor_t2116099858 ** get_address_of_decryptor_38() { return &___decryptor_38; }
	inline void set_decryptor_38(Decryptor_t2116099858 * value)
	{
		___decryptor_38 = value;
		Il2CppCodeGenWriteBarrier((&___decryptor_38), value);
	}
};

struct PhotonPeer_t1608153861_StaticFields
{
public:
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::AsyncKeyExchange
	bool ___AsyncKeyExchange_4;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::OutgoingStreamBufferSize
	int32_t ___OutgoingStreamBufferSize_29;

public:
	inline static int32_t get_offset_of_AsyncKeyExchange_4() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___AsyncKeyExchange_4)); }
	inline bool get_AsyncKeyExchange_4() const { return ___AsyncKeyExchange_4; }
	inline bool* get_address_of_AsyncKeyExchange_4() { return &___AsyncKeyExchange_4; }
	inline void set_AsyncKeyExchange_4(bool value)
	{
		___AsyncKeyExchange_4 = value;
	}

	inline static int32_t get_offset_of_OutgoingStreamBufferSize_29() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___OutgoingStreamBufferSize_29)); }
	inline int32_t get_OutgoingStreamBufferSize_29() const { return ___OutgoingStreamBufferSize_29; }
	inline int32_t* get_address_of_OutgoingStreamBufferSize_29() { return &___OutgoingStreamBufferSize_29; }
	inline void set_OutgoingStreamBufferSize_29(int32_t value)
	{
		___OutgoingStreamBufferSize_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPEER_T1608153861_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#define GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t3913627115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef RAYCASTALLCALLBACK_T1884415901_H
#define RAYCASTALLCALLBACK_T1884415901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t1884415901  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T1884415901_H
#ifndef RAYCAST2DCALLBACK_T768590915_H
#define RAYCAST2DCALLBACK_T768590915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t768590915  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T768590915_H
#ifndef RAYCAST3DCALLBACK_T701940803_H
#define RAYCAST3DCALLBACK_T701940803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t701940803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T701940803_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ENETPEER_T430442630_H
#define ENETPEER_T430442630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetPeer
struct  EnetPeer_t430442630  : public PeerBase_t2956237011
{
public:
	// System.Collections.Generic.List`1<ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetPeer::sentReliableCommands
	List_1_t2702763141 * ___sentReliableCommands_55;
	// ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.EnetPeer::outgoingAcknowledgementsPool
	StreamBuffer_t3827669789 * ___outgoingAcknowledgementsPool_56;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::windowSize
	int32_t ___windowSize_57;
	// System.Byte ExitGames.Client.Photon.EnetPeer::udpCommandCount
	uint8_t ___udpCommandCount_58;
	// System.Byte[] ExitGames.Client.Photon.EnetPeer::udpBuffer
	ByteU5BU5D_t4116647657* ___udpBuffer_59;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::udpBufferIndex
	int32_t ___udpBufferIndex_60;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::udpBufferLength
	int32_t ___udpBufferLength_61;
	// System.Byte[] ExitGames.Client.Photon.EnetPeer::bufferForEncryption
	ByteU5BU5D_t4116647657* ___bufferForEncryption_62;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::challenge
	int32_t ___challenge_63;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::reliableCommandsRepeated
	int32_t ___reliableCommandsRepeated_64;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::reliableCommandsSent
	int32_t ___reliableCommandsSent_65;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::serverSentTime
	int32_t ___serverSentTime_66;
	// System.Boolean ExitGames.Client.Photon.EnetPeer::datagramEncryptedConnection
	bool ___datagramEncryptedConnection_69;
	// ExitGames.Client.Photon.EnetChannel[] ExitGames.Client.Photon.EnetPeer::channelArray
	EnetChannelU5BU5D_t2709601441* ___channelArray_70;
	// System.Collections.Generic.Queue`1<System.Int32> ExitGames.Client.Photon.EnetPeer::commandsToRemove
	Queue_1_t2797205247 * ___commandsToRemove_71;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetPeer::commandsToResend
	Queue_1_t1076947893 * ___commandsToResend_72;

public:
	inline static int32_t get_offset_of_sentReliableCommands_55() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___sentReliableCommands_55)); }
	inline List_1_t2702763141 * get_sentReliableCommands_55() const { return ___sentReliableCommands_55; }
	inline List_1_t2702763141 ** get_address_of_sentReliableCommands_55() { return &___sentReliableCommands_55; }
	inline void set_sentReliableCommands_55(List_1_t2702763141 * value)
	{
		___sentReliableCommands_55 = value;
		Il2CppCodeGenWriteBarrier((&___sentReliableCommands_55), value);
	}

	inline static int32_t get_offset_of_outgoingAcknowledgementsPool_56() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___outgoingAcknowledgementsPool_56)); }
	inline StreamBuffer_t3827669789 * get_outgoingAcknowledgementsPool_56() const { return ___outgoingAcknowledgementsPool_56; }
	inline StreamBuffer_t3827669789 ** get_address_of_outgoingAcknowledgementsPool_56() { return &___outgoingAcknowledgementsPool_56; }
	inline void set_outgoingAcknowledgementsPool_56(StreamBuffer_t3827669789 * value)
	{
		___outgoingAcknowledgementsPool_56 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingAcknowledgementsPool_56), value);
	}

	inline static int32_t get_offset_of_windowSize_57() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___windowSize_57)); }
	inline int32_t get_windowSize_57() const { return ___windowSize_57; }
	inline int32_t* get_address_of_windowSize_57() { return &___windowSize_57; }
	inline void set_windowSize_57(int32_t value)
	{
		___windowSize_57 = value;
	}

	inline static int32_t get_offset_of_udpCommandCount_58() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___udpCommandCount_58)); }
	inline uint8_t get_udpCommandCount_58() const { return ___udpCommandCount_58; }
	inline uint8_t* get_address_of_udpCommandCount_58() { return &___udpCommandCount_58; }
	inline void set_udpCommandCount_58(uint8_t value)
	{
		___udpCommandCount_58 = value;
	}

	inline static int32_t get_offset_of_udpBuffer_59() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___udpBuffer_59)); }
	inline ByteU5BU5D_t4116647657* get_udpBuffer_59() const { return ___udpBuffer_59; }
	inline ByteU5BU5D_t4116647657** get_address_of_udpBuffer_59() { return &___udpBuffer_59; }
	inline void set_udpBuffer_59(ByteU5BU5D_t4116647657* value)
	{
		___udpBuffer_59 = value;
		Il2CppCodeGenWriteBarrier((&___udpBuffer_59), value);
	}

	inline static int32_t get_offset_of_udpBufferIndex_60() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___udpBufferIndex_60)); }
	inline int32_t get_udpBufferIndex_60() const { return ___udpBufferIndex_60; }
	inline int32_t* get_address_of_udpBufferIndex_60() { return &___udpBufferIndex_60; }
	inline void set_udpBufferIndex_60(int32_t value)
	{
		___udpBufferIndex_60 = value;
	}

	inline static int32_t get_offset_of_udpBufferLength_61() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___udpBufferLength_61)); }
	inline int32_t get_udpBufferLength_61() const { return ___udpBufferLength_61; }
	inline int32_t* get_address_of_udpBufferLength_61() { return &___udpBufferLength_61; }
	inline void set_udpBufferLength_61(int32_t value)
	{
		___udpBufferLength_61 = value;
	}

	inline static int32_t get_offset_of_bufferForEncryption_62() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___bufferForEncryption_62)); }
	inline ByteU5BU5D_t4116647657* get_bufferForEncryption_62() const { return ___bufferForEncryption_62; }
	inline ByteU5BU5D_t4116647657** get_address_of_bufferForEncryption_62() { return &___bufferForEncryption_62; }
	inline void set_bufferForEncryption_62(ByteU5BU5D_t4116647657* value)
	{
		___bufferForEncryption_62 = value;
		Il2CppCodeGenWriteBarrier((&___bufferForEncryption_62), value);
	}

	inline static int32_t get_offset_of_challenge_63() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___challenge_63)); }
	inline int32_t get_challenge_63() const { return ___challenge_63; }
	inline int32_t* get_address_of_challenge_63() { return &___challenge_63; }
	inline void set_challenge_63(int32_t value)
	{
		___challenge_63 = value;
	}

	inline static int32_t get_offset_of_reliableCommandsRepeated_64() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___reliableCommandsRepeated_64)); }
	inline int32_t get_reliableCommandsRepeated_64() const { return ___reliableCommandsRepeated_64; }
	inline int32_t* get_address_of_reliableCommandsRepeated_64() { return &___reliableCommandsRepeated_64; }
	inline void set_reliableCommandsRepeated_64(int32_t value)
	{
		___reliableCommandsRepeated_64 = value;
	}

	inline static int32_t get_offset_of_reliableCommandsSent_65() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___reliableCommandsSent_65)); }
	inline int32_t get_reliableCommandsSent_65() const { return ___reliableCommandsSent_65; }
	inline int32_t* get_address_of_reliableCommandsSent_65() { return &___reliableCommandsSent_65; }
	inline void set_reliableCommandsSent_65(int32_t value)
	{
		___reliableCommandsSent_65 = value;
	}

	inline static int32_t get_offset_of_serverSentTime_66() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___serverSentTime_66)); }
	inline int32_t get_serverSentTime_66() const { return ___serverSentTime_66; }
	inline int32_t* get_address_of_serverSentTime_66() { return &___serverSentTime_66; }
	inline void set_serverSentTime_66(int32_t value)
	{
		___serverSentTime_66 = value;
	}

	inline static int32_t get_offset_of_datagramEncryptedConnection_69() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___datagramEncryptedConnection_69)); }
	inline bool get_datagramEncryptedConnection_69() const { return ___datagramEncryptedConnection_69; }
	inline bool* get_address_of_datagramEncryptedConnection_69() { return &___datagramEncryptedConnection_69; }
	inline void set_datagramEncryptedConnection_69(bool value)
	{
		___datagramEncryptedConnection_69 = value;
	}

	inline static int32_t get_offset_of_channelArray_70() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___channelArray_70)); }
	inline EnetChannelU5BU5D_t2709601441* get_channelArray_70() const { return ___channelArray_70; }
	inline EnetChannelU5BU5D_t2709601441** get_address_of_channelArray_70() { return &___channelArray_70; }
	inline void set_channelArray_70(EnetChannelU5BU5D_t2709601441* value)
	{
		___channelArray_70 = value;
		Il2CppCodeGenWriteBarrier((&___channelArray_70), value);
	}

	inline static int32_t get_offset_of_commandsToRemove_71() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___commandsToRemove_71)); }
	inline Queue_1_t2797205247 * get_commandsToRemove_71() const { return ___commandsToRemove_71; }
	inline Queue_1_t2797205247 ** get_address_of_commandsToRemove_71() { return &___commandsToRemove_71; }
	inline void set_commandsToRemove_71(Queue_1_t2797205247 * value)
	{
		___commandsToRemove_71 = value;
		Il2CppCodeGenWriteBarrier((&___commandsToRemove_71), value);
	}

	inline static int32_t get_offset_of_commandsToResend_72() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___commandsToResend_72)); }
	inline Queue_1_t1076947893 * get_commandsToResend_72() const { return ___commandsToResend_72; }
	inline Queue_1_t1076947893 ** get_address_of_commandsToResend_72() { return &___commandsToResend_72; }
	inline void set_commandsToResend_72(Queue_1_t1076947893 * value)
	{
		___commandsToResend_72 = value;
		Il2CppCodeGenWriteBarrier((&___commandsToResend_72), value);
	}
};

struct EnetPeer_t430442630_StaticFields
{
public:
	// System.Int32 ExitGames.Client.Photon.EnetPeer::HMAC_SIZE
	int32_t ___HMAC_SIZE_52;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::BLOCK_SIZE
	int32_t ___BLOCK_SIZE_53;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::IV_SIZE
	int32_t ___IV_SIZE_54;
	// System.Byte[] ExitGames.Client.Photon.EnetPeer::udpHeader0xF3
	ByteU5BU5D_t4116647657* ___udpHeader0xF3_67;
	// System.Byte[] ExitGames.Client.Photon.EnetPeer::messageHeader
	ByteU5BU5D_t4116647657* ___messageHeader_68;

public:
	inline static int32_t get_offset_of_HMAC_SIZE_52() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___HMAC_SIZE_52)); }
	inline int32_t get_HMAC_SIZE_52() const { return ___HMAC_SIZE_52; }
	inline int32_t* get_address_of_HMAC_SIZE_52() { return &___HMAC_SIZE_52; }
	inline void set_HMAC_SIZE_52(int32_t value)
	{
		___HMAC_SIZE_52 = value;
	}

	inline static int32_t get_offset_of_BLOCK_SIZE_53() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___BLOCK_SIZE_53)); }
	inline int32_t get_BLOCK_SIZE_53() const { return ___BLOCK_SIZE_53; }
	inline int32_t* get_address_of_BLOCK_SIZE_53() { return &___BLOCK_SIZE_53; }
	inline void set_BLOCK_SIZE_53(int32_t value)
	{
		___BLOCK_SIZE_53 = value;
	}

	inline static int32_t get_offset_of_IV_SIZE_54() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___IV_SIZE_54)); }
	inline int32_t get_IV_SIZE_54() const { return ___IV_SIZE_54; }
	inline int32_t* get_address_of_IV_SIZE_54() { return &___IV_SIZE_54; }
	inline void set_IV_SIZE_54(int32_t value)
	{
		___IV_SIZE_54 = value;
	}

	inline static int32_t get_offset_of_udpHeader0xF3_67() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___udpHeader0xF3_67)); }
	inline ByteU5BU5D_t4116647657* get_udpHeader0xF3_67() const { return ___udpHeader0xF3_67; }
	inline ByteU5BU5D_t4116647657** get_address_of_udpHeader0xF3_67() { return &___udpHeader0xF3_67; }
	inline void set_udpHeader0xF3_67(ByteU5BU5D_t4116647657* value)
	{
		___udpHeader0xF3_67 = value;
		Il2CppCodeGenWriteBarrier((&___udpHeader0xF3_67), value);
	}

	inline static int32_t get_offset_of_messageHeader_68() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___messageHeader_68)); }
	inline ByteU5BU5D_t4116647657* get_messageHeader_68() const { return ___messageHeader_68; }
	inline ByteU5BU5D_t4116647657** get_address_of_messageHeader_68() { return &___messageHeader_68; }
	inline void set_messageHeader_68(ByteU5BU5D_t4116647657* value)
	{
		___messageHeader_68 = value;
		Il2CppCodeGenWriteBarrier((&___messageHeader_68), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENETPEER_T430442630_H
#ifndef SERIALIZEMETHOD_T1264674278_H
#define SERIALIZEMETHOD_T1264674278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializeMethod
struct  SerializeMethod_t1264674278  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEMETHOD_T1264674278_H
#ifndef SERIALIZESTREAMMETHOD_T2169445464_H
#define SERIALIZESTREAMMETHOD_T2169445464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializeStreamMethod
struct  SerializeStreamMethod_t2169445464  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZESTREAMMETHOD_T2169445464_H
#ifndef INTEGERMILLISECONDSDELEGATE_T651311252_H
#define INTEGERMILLISECONDSDELEGATE_T651311252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate
struct  IntegerMillisecondsDelegate_t651311252  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGERMILLISECONDSDELEGATE_T651311252_H
#ifndef TPEER_T1497954812_H
#define TPEER_T1497954812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.TPeer
struct  TPeer_t1497954812  : public PeerBase_t2956237011
{
public:
	// System.Collections.Generic.Queue`1<System.Byte[]> ExitGames.Client.Photon.TPeer::incomingList
	Queue_1_t3962907151 * ___incomingList_52;
	// System.Collections.Generic.List`1<System.Byte[]> ExitGames.Client.Photon.TPeer::outgoingStream
	List_1_t1293755103 * ___outgoingStream_53;
	// System.Int32 ExitGames.Client.Photon.TPeer::lastPingResult
	int32_t ___lastPingResult_54;
	// System.Byte[] ExitGames.Client.Photon.TPeer::pingRequest
	ByteU5BU5D_t4116647657* ___pingRequest_55;
	// System.Byte[] ExitGames.Client.Photon.TPeer::messageHeader
	ByteU5BU5D_t4116647657* ___messageHeader_58;
	// System.Boolean ExitGames.Client.Photon.TPeer::DoFraming
	bool ___DoFraming_59;

public:
	inline static int32_t get_offset_of_incomingList_52() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___incomingList_52)); }
	inline Queue_1_t3962907151 * get_incomingList_52() const { return ___incomingList_52; }
	inline Queue_1_t3962907151 ** get_address_of_incomingList_52() { return &___incomingList_52; }
	inline void set_incomingList_52(Queue_1_t3962907151 * value)
	{
		___incomingList_52 = value;
		Il2CppCodeGenWriteBarrier((&___incomingList_52), value);
	}

	inline static int32_t get_offset_of_outgoingStream_53() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___outgoingStream_53)); }
	inline List_1_t1293755103 * get_outgoingStream_53() const { return ___outgoingStream_53; }
	inline List_1_t1293755103 ** get_address_of_outgoingStream_53() { return &___outgoingStream_53; }
	inline void set_outgoingStream_53(List_1_t1293755103 * value)
	{
		___outgoingStream_53 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingStream_53), value);
	}

	inline static int32_t get_offset_of_lastPingResult_54() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___lastPingResult_54)); }
	inline int32_t get_lastPingResult_54() const { return ___lastPingResult_54; }
	inline int32_t* get_address_of_lastPingResult_54() { return &___lastPingResult_54; }
	inline void set_lastPingResult_54(int32_t value)
	{
		___lastPingResult_54 = value;
	}

	inline static int32_t get_offset_of_pingRequest_55() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___pingRequest_55)); }
	inline ByteU5BU5D_t4116647657* get_pingRequest_55() const { return ___pingRequest_55; }
	inline ByteU5BU5D_t4116647657** get_address_of_pingRequest_55() { return &___pingRequest_55; }
	inline void set_pingRequest_55(ByteU5BU5D_t4116647657* value)
	{
		___pingRequest_55 = value;
		Il2CppCodeGenWriteBarrier((&___pingRequest_55), value);
	}

	inline static int32_t get_offset_of_messageHeader_58() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___messageHeader_58)); }
	inline ByteU5BU5D_t4116647657* get_messageHeader_58() const { return ___messageHeader_58; }
	inline ByteU5BU5D_t4116647657** get_address_of_messageHeader_58() { return &___messageHeader_58; }
	inline void set_messageHeader_58(ByteU5BU5D_t4116647657* value)
	{
		___messageHeader_58 = value;
		Il2CppCodeGenWriteBarrier((&___messageHeader_58), value);
	}

	inline static int32_t get_offset_of_DoFraming_59() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___DoFraming_59)); }
	inline bool get_DoFraming_59() const { return ___DoFraming_59; }
	inline bool* get_address_of_DoFraming_59() { return &___DoFraming_59; }
	inline void set_DoFraming_59(bool value)
	{
		___DoFraming_59 = value;
	}
};

struct TPeer_t1497954812_StaticFields
{
public:
	// System.Byte[] ExitGames.Client.Photon.TPeer::tcpFramedMessageHead
	ByteU5BU5D_t4116647657* ___tcpFramedMessageHead_56;
	// System.Byte[] ExitGames.Client.Photon.TPeer::tcpMsgHead
	ByteU5BU5D_t4116647657* ___tcpMsgHead_57;

public:
	inline static int32_t get_offset_of_tcpFramedMessageHead_56() { return static_cast<int32_t>(offsetof(TPeer_t1497954812_StaticFields, ___tcpFramedMessageHead_56)); }
	inline ByteU5BU5D_t4116647657* get_tcpFramedMessageHead_56() const { return ___tcpFramedMessageHead_56; }
	inline ByteU5BU5D_t4116647657** get_address_of_tcpFramedMessageHead_56() { return &___tcpFramedMessageHead_56; }
	inline void set_tcpFramedMessageHead_56(ByteU5BU5D_t4116647657* value)
	{
		___tcpFramedMessageHead_56 = value;
		Il2CppCodeGenWriteBarrier((&___tcpFramedMessageHead_56), value);
	}

	inline static int32_t get_offset_of_tcpMsgHead_57() { return static_cast<int32_t>(offsetof(TPeer_t1497954812_StaticFields, ___tcpMsgHead_57)); }
	inline ByteU5BU5D_t4116647657* get_tcpMsgHead_57() const { return ___tcpMsgHead_57; }
	inline ByteU5BU5D_t4116647657** get_address_of_tcpMsgHead_57() { return &___tcpMsgHead_57; }
	inline void set_tcpMsgHead_57(ByteU5BU5D_t4116647657* value)
	{
		___tcpMsgHead_57 = value;
		Il2CppCodeGenWriteBarrier((&___tcpMsgHead_57), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TPEER_T1497954812_H
#ifndef MYACTION_T2462891903_H
#define MYACTION_T2462891903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/MyAction
struct  MyAction_t2462891903  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYACTION_T2462891903_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_2)); }
	inline Graphic_t1660335611 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t1660335611 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_4)); }
	inline Vector2_t2156229523  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2156229523  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2103[7] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2110[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2118[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (ExtensionRegistry_t4271428238), -1, sizeof(ExtensionRegistry_t4271428238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2121[4] = 
{
	ExtensionRegistry_t4271428238_StaticFields::get_offset_of_empty_0(),
	ExtensionRegistry_t4271428238::get_offset_of_extensionsByName_1(),
	ExtensionRegistry_t4271428238::get_offset_of_extensionsByNumber_2(),
	ExtensionRegistry_t4271428238::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (ExtensionIntPair_t1343559306)+ sizeof (RuntimeObject), sizeof(ExtensionIntPair_t1343559306_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2122[2] = 
{
	ExtensionIntPair_t1343559306::get_offset_of_msgType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ExtensionIntPair_t1343559306::get_offset_of_number_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (CodedInputStream_t2502120507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[13] = 
{
	CodedInputStream_t2502120507::get_offset_of_buffer_0(),
	CodedInputStream_t2502120507::get_offset_of_bufferSize_1(),
	CodedInputStream_t2502120507::get_offset_of_bufferSizeAfterLimit_2(),
	CodedInputStream_t2502120507::get_offset_of_bufferPos_3(),
	CodedInputStream_t2502120507::get_offset_of_input_4(),
	CodedInputStream_t2502120507::get_offset_of_lastTag_5(),
	CodedInputStream_t2502120507::get_offset_of_nextTag_6(),
	CodedInputStream_t2502120507::get_offset_of_hasNextTag_7(),
	CodedInputStream_t2502120507::get_offset_of_totalBytesRetired_8(),
	CodedInputStream_t2502120507::get_offset_of_currentLimit_9(),
	CodedInputStream_t2502120507::get_offset_of_recursionDepth_10(),
	CodedInputStream_t2502120507::get_offset_of_recursionLimit_11(),
	CodedInputStream_t2502120507::get_offset_of_sizeLimit_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (ByteString_t35393593), -1, sizeof(ByteString_t35393593_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2124[2] = 
{
	ByteString_t35393593_StaticFields::get_offset_of_empty_0(),
	ByteString_t35393593::get_offset_of_bytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (UninitializedMessageException_t3402409807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[1] = 
{
	UninitializedMessageException_t3402409807::get_offset_of_missingFields_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (CodedOutputStream_t1787628118), -1, sizeof(CodedOutputStream_t1787628118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2128[5] = 
{
	CodedOutputStream_t1787628118_StaticFields::get_offset_of_DefaultBufferSize_0(),
	CodedOutputStream_t1787628118::get_offset_of_buffer_1(),
	CodedOutputStream_t1787628118::get_offset_of_limit_2(),
	CodedOutputStream_t1787628118::get_offset_of_position_3(),
	CodedOutputStream_t1787628118::get_offset_of_output_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (OutOfSpaceException_t1185332314), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (Lists_t2963336001), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (InvalidProtocolBufferException_t2498581859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (FrameworkPortability_t3105963803), -1, sizeof(FrameworkPortability_t3105963803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2140[1] = 
{
	FrameworkPortability_t3105963803_StaticFields::get_offset_of_NewLine_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (ThrowHelper_t3137032741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (WireFormat_t4196727973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (WireType_t2332021402)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2144[7] = 
{
	WireType_t2332021402::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (ByteArray_t1708291894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (OakleyGroups_t1704371988), -1, sizeof(OakleyGroups_t1704371988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2155[4] = 
{
	OakleyGroups_t1704371988_StaticFields::get_offset_of_Generator_0(),
	OakleyGroups_t1704371988_StaticFields::get_offset_of_OakleyPrime768_1(),
	OakleyGroups_t1704371988_StaticFields::get_offset_of_OakleyPrime1024_2(),
	OakleyGroups_t1704371988_StaticFields::get_offset_of_OakleyPrime1536_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (DiffieHellmanCryptoProvider_t915317458), -1, sizeof(DiffieHellmanCryptoProvider_t915317458_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2157[6] = 
{
	DiffieHellmanCryptoProvider_t915317458_StaticFields::get_offset_of_primeRoot_0(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_prime_1(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_secret_2(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_publicKey_3(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_crypto_4(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_sharedKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (BigInteger_t956758543), -1, sizeof(BigInteger_t956758543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2158[3] = 
{
	BigInteger_t956758543_StaticFields::get_offset_of_primesBelow2000_0(),
	BigInteger_t956758543::get_offset_of_data_1(),
	BigInteger_t956758543::get_offset_of_dataLength_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (Version_t2916202802), -1, sizeof(Version_t2916202802_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2159[1] = 
{
	Version_t2916202802_StaticFields::get_offset_of_clientVersion_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (Hashtable_t1048209202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (SupportClass_t2974952451), -1, sizeof(SupportClass_t2974952451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2161[2] = 
{
	SupportClass_t2974952451_StaticFields::get_offset_of_threadList_0(),
	SupportClass_t2974952451_StaticFields::get_offset_of_IntegerMilliseconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (IntegerMillisecondsDelegate_t651311252), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (ThreadSafeRandom_t1204416265), -1, sizeof(ThreadSafeRandom_t1204416265_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2163[1] = 
{
	ThreadSafeRandom_t1204416265_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (U3CU3Ec__DisplayClass7_0_t926758450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[2] = 
{
	U3CU3Ec__DisplayClass7_0_t926758450::get_offset_of_millisecondsInterval_0(),
	U3CU3Ec__DisplayClass7_0_t926758450::get_offset_of_myThread_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (U3CU3Ec_t356392828), -1, sizeof(U3CU3Ec_t356392828_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2165[1] = 
{
	U3CU3Ec_t356392828_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (StatusCode_t823606708)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2166[20] = 
{
	StatusCode_t823606708::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (StreamBuffer_t3827669789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[4] = 
{
	0,
	StreamBuffer_t3827669789::get_offset_of_pos_2(),
	StreamBuffer_t3827669789::get_offset_of_len_3(),
	StreamBuffer_t3827669789::get_offset_of_buf_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (PeerStateValue_t1289417078)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2169[6] = 
{
	PeerStateValue_t1289417078::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (ConnectionProtocol_t2586603950)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2170[5] = 
{
	ConnectionProtocol_t2586603950::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (DebugLevel_t3671880145)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2171[6] = 
{
	DebugLevel_t3671880145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (PhotonPeer_t1608153861), -1, sizeof(PhotonPeer_t1608153861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2172[39] = 
{
	0,
	0,
	0,
	PhotonPeer_t1608153861::get_offset_of_ClientSdkId_3(),
	PhotonPeer_t1608153861_StaticFields::get_offset_of_AsyncKeyExchange_4(),
	PhotonPeer_t1608153861::get_offset_of_clientVersion_5(),
	PhotonPeer_t1608153861::get_offset_of_U3CSerializationProtocolTypeU3Ek__BackingField_6(),
	PhotonPeer_t1608153861::get_offset_of_SocketImplementationConfig_7(),
	PhotonPeer_t1608153861::get_offset_of_U3CSocketImplementationU3Ek__BackingField_8(),
	PhotonPeer_t1608153861::get_offset_of_DebugOut_9(),
	PhotonPeer_t1608153861::get_offset_of_U3CListenerU3Ek__BackingField_10(),
	PhotonPeer_t1608153861::get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_11(),
	PhotonPeer_t1608153861::get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_12(),
	PhotonPeer_t1608153861::get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_13(),
	PhotonPeer_t1608153861::get_offset_of_trafficStatsStopwatch_14(),
	PhotonPeer_t1608153861::get_offset_of_trafficStatsEnabled_15(),
	PhotonPeer_t1608153861::get_offset_of_commandLogSize_16(),
	PhotonPeer_t1608153861::get_offset_of_U3CEnableServerTracingU3Ek__BackingField_17(),
	PhotonPeer_t1608153861::get_offset_of_quickResendAttempts_18(),
	PhotonPeer_t1608153861::get_offset_of_RhttpMinConnections_19(),
	PhotonPeer_t1608153861::get_offset_of_RhttpMaxConnections_20(),
	PhotonPeer_t1608153861::get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21(),
	PhotonPeer_t1608153861::get_offset_of_ChannelCount_22(),
	PhotonPeer_t1608153861::get_offset_of_crcEnabled_23(),
	PhotonPeer_t1608153861::get_offset_of_WarningSize_24(),
	PhotonPeer_t1608153861::get_offset_of_SentCountAllowance_25(),
	PhotonPeer_t1608153861::get_offset_of_TimePingInterval_26(),
	PhotonPeer_t1608153861::get_offset_of_DisconnectTimeout_27(),
	PhotonPeer_t1608153861::get_offset_of_U3CTransportProtocolU3Ek__BackingField_28(),
	PhotonPeer_t1608153861_StaticFields::get_offset_of_OutgoingStreamBufferSize_29(),
	PhotonPeer_t1608153861::get_offset_of_mtu_30(),
	PhotonPeer_t1608153861::get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_31(),
	PhotonPeer_t1608153861::get_offset_of_peerBase_32(),
	PhotonPeer_t1608153861::get_offset_of_SendOutgoingLockObject_33(),
	PhotonPeer_t1608153861::get_offset_of_DispatchLockObject_34(),
	PhotonPeer_t1608153861::get_offset_of_EnqueueLock_35(),
	PhotonPeer_t1608153861::get_offset_of_PayloadEncryptionSecret_36(),
	PhotonPeer_t1608153861::get_offset_of_encryptor_37(),
	PhotonPeer_t1608153861::get_offset_of_decryptor_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (PhotonCodes_t543425440), -1, sizeof(PhotonCodes_t543425440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2173[5] = 
{
	PhotonCodes_t543425440_StaticFields::get_offset_of_ClientKey_0(),
	PhotonCodes_t543425440_StaticFields::get_offset_of_ModeKey_1(),
	PhotonCodes_t543425440_StaticFields::get_offset_of_ServerKey_2(),
	PhotonCodes_t543425440_StaticFields::get_offset_of_InitEncryption_3(),
	PhotonCodes_t543425440_StaticFields::get_offset_of_Ping_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (PeerBase_t2956237011), -1, sizeof(PeerBase_t2956237011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2174[52] = 
{
	PeerBase_t2956237011::get_offset_of_ppeer_0(),
	PeerBase_t2956237011::get_offset_of_protocol_1(),
	PeerBase_t2956237011::get_offset_of_usedProtocol_2(),
	PeerBase_t2956237011::get_offset_of_rt_3(),
	PeerBase_t2956237011::get_offset_of_U3CServerAddressU3Ek__BackingField_4(),
	PeerBase_t2956237011::get_offset_of_U3CHttpUrlParametersU3Ek__BackingField_5(),
	PeerBase_t2956237011::get_offset_of_ByteCountLastOperation_6(),
	PeerBase_t2956237011::get_offset_of_ByteCountCurrentDispatch_7(),
	PeerBase_t2956237011::get_offset_of_CommandInCurrentDispatch_8(),
	PeerBase_t2956237011::get_offset_of_TrafficPackageHeaderSize_9(),
	PeerBase_t2956237011::get_offset_of_packetLossByCrc_10(),
	PeerBase_t2956237011::get_offset_of_packetLossByChallenge_11(),
	PeerBase_t2956237011::get_offset_of_ActionQueue_12(),
	PeerBase_t2956237011::get_offset_of_peerID_13(),
	PeerBase_t2956237011::get_offset_of_peerConnectionState_14(),
	PeerBase_t2956237011::get_offset_of_serverTimeOffset_15(),
	PeerBase_t2956237011::get_offset_of_serverTimeOffsetIsAvailable_16(),
	PeerBase_t2956237011::get_offset_of_roundTripTime_17(),
	PeerBase_t2956237011::get_offset_of_roundTripTimeVariance_18(),
	PeerBase_t2956237011::get_offset_of_lastRoundTripTime_19(),
	PeerBase_t2956237011::get_offset_of_lowestRoundTripTime_20(),
	PeerBase_t2956237011::get_offset_of_lastRoundTripTimeVariance_21(),
	PeerBase_t2956237011::get_offset_of_highestRoundTripTimeVariance_22(),
	PeerBase_t2956237011::get_offset_of_timestampOfLastReceive_23(),
	PeerBase_t2956237011::get_offset_of_packetThrottleInterval_24(),
	PeerBase_t2956237011_StaticFields::get_offset_of_peerCount_25(),
	PeerBase_t2956237011::get_offset_of_bytesOut_26(),
	PeerBase_t2956237011::get_offset_of_bytesIn_27(),
	PeerBase_t2956237011::get_offset_of_commandBufferSize_28(),
	PeerBase_t2956237011::get_offset_of_CryptoProvider_29(),
	PeerBase_t2956237011::get_offset_of_lagRandomizer_30(),
	PeerBase_t2956237011::get_offset_of_NetSimListOutgoing_31(),
	PeerBase_t2956237011::get_offset_of_NetSimListIncoming_32(),
	PeerBase_t2956237011::get_offset_of_networkSimulationSettings_33(),
	PeerBase_t2956237011::get_offset_of_CommandLog_34(),
	PeerBase_t2956237011::get_offset_of_InReliableLog_35(),
	PeerBase_t2956237011::get_offset_of_CustomInitData_36(),
	PeerBase_t2956237011::get_offset_of_AppId_37(),
	PeerBase_t2956237011::get_offset_of_U3CTcpConnectionPrefixU3Ek__BackingField_38(),
	PeerBase_t2956237011::get_offset_of_timeBase_39(),
	PeerBase_t2956237011::get_offset_of_timeInt_40(),
	PeerBase_t2956237011::get_offset_of_timeoutInt_41(),
	PeerBase_t2956237011::get_offset_of_timeLastAckReceive_42(),
	PeerBase_t2956237011::get_offset_of_timeLastSendAck_43(),
	PeerBase_t2956237011::get_offset_of_timeLastSendOutgoing_44(),
	0,
	0,
	0,
	PeerBase_t2956237011::get_offset_of_ApplicationIsInitialized_48(),
	PeerBase_t2956237011::get_offset_of_isEncryptionAvailable_49(),
	PeerBase_t2956237011::get_offset_of_outgoingCommandsInStream_50(),
	PeerBase_t2956237011::get_offset_of_SerializeMemStream_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (MyAction_t2462891903), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (ConnectionStateValue_t1954099360)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2176[7] = 
{
	ConnectionStateValue_t1954099360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (EgMessageType_t1130059189)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2177[10] = 
{
	EgMessageType_t1130059189::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (U3CU3Ec__DisplayClass145_0_t1573695289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[3] = 
{
	U3CU3Ec__DisplayClass145_0_t1573695289::get_offset_of_level_0(),
	U3CU3Ec__DisplayClass145_0_t1573695289::get_offset_of_debugReturn_1(),
	U3CU3Ec__DisplayClass145_0_t1573695289::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (U3CU3Ec__DisplayClass146_0_t1573695292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[2] = 
{
	U3CU3Ec__DisplayClass146_0_t1573695292::get_offset_of_statusValue_0(),
	U3CU3Ec__DisplayClass146_0_t1573695292::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (CmdLogItem_t4217690540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[5] = 
{
	CmdLogItem_t4217690540::get_offset_of_TimeInt_0(),
	CmdLogItem_t4217690540::get_offset_of_Channel_1(),
	CmdLogItem_t4217690540::get_offset_of_SequenceNumber_2(),
	CmdLogItem_t4217690540::get_offset_of_Rtt_3(),
	CmdLogItem_t4217690540::get_offset_of_Variance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (CmdLogReceivedReliable_t4090183889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[2] = 
{
	CmdLogReceivedReliable_t4090183889::get_offset_of_TimeSinceLastSend_5(),
	CmdLogReceivedReliable_t4090183889::get_offset_of_TimeSinceLastSendAck_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (CmdLogReceivedAck_t580412049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[1] = 
{
	CmdLogReceivedAck_t580412049::get_offset_of_ReceivedSentTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (CmdLogSentReliable_t3437548410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[4] = 
{
	CmdLogSentReliable_t3437548410::get_offset_of_Resend_5(),
	CmdLogSentReliable_t3437548410::get_offset_of_RoundtripTimeout_6(),
	CmdLogSentReliable_t3437548410::get_offset_of_Timeout_7(),
	CmdLogSentReliable_t3437548410::get_offset_of_TriggeredTimeout_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (EnetPeer_t430442630), -1, sizeof(EnetPeer_t430442630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2184[21] = 
{
	EnetPeer_t430442630_StaticFields::get_offset_of_HMAC_SIZE_52(),
	EnetPeer_t430442630_StaticFields::get_offset_of_BLOCK_SIZE_53(),
	EnetPeer_t430442630_StaticFields::get_offset_of_IV_SIZE_54(),
	EnetPeer_t430442630::get_offset_of_sentReliableCommands_55(),
	EnetPeer_t430442630::get_offset_of_outgoingAcknowledgementsPool_56(),
	EnetPeer_t430442630::get_offset_of_windowSize_57(),
	EnetPeer_t430442630::get_offset_of_udpCommandCount_58(),
	EnetPeer_t430442630::get_offset_of_udpBuffer_59(),
	EnetPeer_t430442630::get_offset_of_udpBufferIndex_60(),
	EnetPeer_t430442630::get_offset_of_udpBufferLength_61(),
	EnetPeer_t430442630::get_offset_of_bufferForEncryption_62(),
	EnetPeer_t430442630::get_offset_of_challenge_63(),
	EnetPeer_t430442630::get_offset_of_reliableCommandsRepeated_64(),
	EnetPeer_t430442630::get_offset_of_reliableCommandsSent_65(),
	EnetPeer_t430442630::get_offset_of_serverSentTime_66(),
	EnetPeer_t430442630_StaticFields::get_offset_of_udpHeader0xF3_67(),
	EnetPeer_t430442630_StaticFields::get_offset_of_messageHeader_68(),
	EnetPeer_t430442630::get_offset_of_datagramEncryptedConnection_69(),
	EnetPeer_t430442630::get_offset_of_channelArray_70(),
	EnetPeer_t430442630::get_offset_of_commandsToRemove_71(),
	EnetPeer_t430442630::get_offset_of_commandsToResend_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (U3CU3Ec__DisplayClass56_0_t3313269813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[2] = 
{
	U3CU3Ec__DisplayClass56_0_t3313269813::get_offset_of_dataCopy_0(),
	U3CU3Ec__DisplayClass56_0_t3313269813::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (U3CU3Ec__DisplayClass56_1_t3313335349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[2] = 
{
	U3CU3Ec__DisplayClass56_1_t3313335349::get_offset_of_length_0(),
	U3CU3Ec__DisplayClass56_1_t3313335349::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (U3CU3Ec__DisplayClass62_0_t982511824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[2] = 
{
	U3CU3Ec__DisplayClass62_0_t982511824::get_offset_of_readCommand_0(),
	U3CU3Ec__DisplayClass62_0_t982511824::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (EnetChannel_t2207795168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[9] = 
{
	EnetChannel_t2207795168::get_offset_of_ChannelNumber_0(),
	EnetChannel_t2207795168::get_offset_of_incomingReliableCommandsList_1(),
	EnetChannel_t2207795168::get_offset_of_incomingUnreliableCommandsList_2(),
	EnetChannel_t2207795168::get_offset_of_outgoingReliableCommandsList_3(),
	EnetChannel_t2207795168::get_offset_of_outgoingUnreliableCommandsList_4(),
	EnetChannel_t2207795168::get_offset_of_incomingReliableSequenceNumber_5(),
	EnetChannel_t2207795168::get_offset_of_incomingUnreliableSequenceNumber_6(),
	EnetChannel_t2207795168::get_offset_of_outgoingReliableSequenceNumber_7(),
	EnetChannel_t2207795168::get_offset_of_outgoingUnreliableSequenceNumber_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (NCommand_t1230688399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[49] = 
{
	NCommand_t1230688399::get_offset_of_commandFlags_0(),
	0,
	0,
	0,
	0,
	0,
	NCommand_t1230688399::get_offset_of_commandType_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	NCommand_t1230688399::get_offset_of_commandChannelID_17(),
	NCommand_t1230688399::get_offset_of_reliableSequenceNumber_18(),
	NCommand_t1230688399::get_offset_of_unreliableSequenceNumber_19(),
	NCommand_t1230688399::get_offset_of_unsequencedGroupNumber_20(),
	NCommand_t1230688399::get_offset_of_reservedByte_21(),
	NCommand_t1230688399::get_offset_of_startSequenceNumber_22(),
	NCommand_t1230688399::get_offset_of_fragmentCount_23(),
	NCommand_t1230688399::get_offset_of_fragmentNumber_24(),
	NCommand_t1230688399::get_offset_of_totalLength_25(),
	NCommand_t1230688399::get_offset_of_fragmentOffset_26(),
	NCommand_t1230688399::get_offset_of_fragmentsRemaining_27(),
	NCommand_t1230688399::get_offset_of_commandSentTime_28(),
	NCommand_t1230688399::get_offset_of_commandSentCount_29(),
	NCommand_t1230688399::get_offset_of_roundTripTimeout_30(),
	NCommand_t1230688399::get_offset_of_timeoutTime_31(),
	NCommand_t1230688399::get_offset_of_ackReceivedReliableSequenceNumber_32(),
	NCommand_t1230688399::get_offset_of_ackReceivedSentTime_33(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	NCommand_t1230688399::get_offset_of_Size_45(),
	NCommand_t1230688399::get_offset_of_commandHeader_46(),
	NCommand_t1230688399::get_offset_of_SizeOfHeader_47(),
	NCommand_t1230688399::get_offset_of_Payload_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (TPeer_t1497954812), -1, sizeof(TPeer_t1497954812_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2190[8] = 
{
	TPeer_t1497954812::get_offset_of_incomingList_52(),
	TPeer_t1497954812::get_offset_of_outgoingStream_53(),
	TPeer_t1497954812::get_offset_of_lastPingResult_54(),
	TPeer_t1497954812::get_offset_of_pingRequest_55(),
	TPeer_t1497954812_StaticFields::get_offset_of_tcpFramedMessageHead_56(),
	TPeer_t1497954812_StaticFields::get_offset_of_tcpMsgHead_57(),
	TPeer_t1497954812::get_offset_of_messageHeader_58(),
	TPeer_t1497954812::get_offset_of_DoFraming_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (U3CU3Ec__DisplayClass30_0_t3066328396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[2] = 
{
	U3CU3Ec__DisplayClass30_0_t3066328396::get_offset_of_data_0(),
	U3CU3Ec__DisplayClass30_0_t3066328396::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (SerializationProtocol_t4091957412)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2192[2] = 
{
	SerializationProtocol_t4091957412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (SerializationProtocolFactory_t2539989091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (IProtocol_t1394662050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (OperationRequest_t597637232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[2] = 
{
	OperationRequest_t597637232::get_offset_of_OperationCode_0(),
	OperationRequest_t597637232::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (OperationResponse_t423627973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[4] = 
{
	OperationResponse_t423627973::get_offset_of_OperationCode_0(),
	OperationResponse_t423627973::get_offset_of_ReturnCode_1(),
	OperationResponse_t423627973::get_offset_of_DebugMessage_2(),
	OperationResponse_t423627973::get_offset_of_Parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (EventData_t3728223374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[2] = 
{
	EventData_t3728223374::get_offset_of_Code_0(),
	EventData_t3728223374::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (SerializeMethod_t1264674278), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (SerializeStreamMethod_t2169445464), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
