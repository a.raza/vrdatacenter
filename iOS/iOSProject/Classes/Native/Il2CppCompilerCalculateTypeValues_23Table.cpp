﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_t3593973608;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// PagedScrollRect
struct PagedScrollRect_t3286523126;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Char>
struct Dictionary_2_t2523173801;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData>
struct List_1_t225505033;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// HeadsetDemoManager
struct HeadsetDemoManager_t1859624338;
// System.Collections.Generic.List`1<TMPro.TMP_Text>
struct List_1_t4071693616;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/MaskingMaterial>
struct List_1_t1488971017;
// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_MaterialManager/FallbackMaterial>
struct Dictionary_2_t939051307;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int64>
struct Dictionary_2_t2625280635;
// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/FallbackMaterial>
struct List_1_t1348486265;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;
// System.Void
struct Void_t1185182177;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2496920137;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t3678055768;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t648826345;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t1930184704;
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t3766301798;
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t3558768157;
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t4120149533;
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_t2463031060;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3365986247;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// TMPro.TMP_TextElement
struct TMP_TextElement_t129727469;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t3552942253;
// System.Collections.Generic.List`1<TMPro.TMP_Style>
struct List_1_t656488210;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style>
struct Dictionary_2_t2368094095;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2836635477;
// System.Collections.Generic.List`1<TMPro.TMP_FontAsset>
struct List_1_t1836456368;
// TMPro.TMP_StyleSheet
struct TMP_StyleSheet_t917564226;
// UnityEngine.TextAsset
struct TextAsset_t3022178571;
// TMPro.TMP_Settings/LineBreakingTable
struct LineBreakingTable_t1457697482;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Collections.Generic.List`1<TMPro.TMP_Sprite>
struct List_1_t2026141888;
// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset>
struct List_1_t1956895375;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t3280968592;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// PagedScrollRect/ActivePageChangedDelegate
struct ActivePageChangedDelegate_t1951257476;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// IPageProvider
struct IPageProvider_t3246281069;
// BaseScrollEffect[]
struct BaseScrollEffectU5BU5D_t2685664919;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.RectTransform>
struct Dictionary_2_t2593370356;
// System.Collections.Generic.Dictionary`2<UnityEngine.RectTransform,System.Int32>
struct Dictionary_2_t1561542660;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// DemoInputManager
struct DemoInputManager_t35471296;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// UnityEngine.Transform
struct Transform_t3600365921;
// TiledPage
struct TiledPage_t1725039945;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Predicate`1<System.String>
struct Predicate_1_t2672744813;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Transform>>
struct List_1_t2249548109;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// System.EventHandler
struct EventHandler_t1348719766;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.Image
struct Image_t2670269651;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_t149898510;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t284240280;
// TMPro.TMP_Glyph
struct TMP_Glyph_t581847833;

struct Vector3_t3722313464 ;
struct Vector4_t3319028937 ;
struct Vector2_t2156229523 ;
struct Color32_t2600501292 ;



#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TEXTUREPACKER_T3148178657_H
#define TEXTUREPACKER_T3148178657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker
struct  TexturePacker_t3148178657  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPACKER_T3148178657_H
#ifndef TMP_UPDATEREGISTRY_T461608481_H
#define TMP_UPDATEREGISTRY_T461608481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateRegistry
struct  TMP_UpdateRegistry_t461608481  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_LayoutRebuildQueue
	List_1_t3593973608 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_GraphicRebuildQueue
	List_1_t3593973608 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t3593973608 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t3593973608 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t3593973608 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t3593973608 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t3593973608 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t3593973608 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateRegistry_t461608481_StaticFields
{
public:
	// TMPro.TMP_UpdateRegistry TMPro.TMP_UpdateRegistry::s_Instance
	TMP_UpdateRegistry_t461608481 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateRegistry_t461608481 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateRegistry_t461608481 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateRegistry_t461608481 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEREGISTRY_T461608481_H
#ifndef U3CSNAPTOPAGECOROUTINEU3EC__ITERATOR0_T3387229272_H
#define U3CSNAPTOPAGECOROUTINEU3EC__ITERATOR0_T3387229272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PagedScrollRect/<SnapToPageCoroutine>c__Iterator0
struct  U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272  : public RuntimeObject
{
public:
	// System.Int32 PagedScrollRect/<SnapToPageCoroutine>c__Iterator0::index
	int32_t ___index_0;
	// PagedScrollRect PagedScrollRect/<SnapToPageCoroutine>c__Iterator0::$this
	PagedScrollRect_t3286523126 * ___U24this_1;
	// System.Object PagedScrollRect/<SnapToPageCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean PagedScrollRect/<SnapToPageCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 PagedScrollRect/<SnapToPageCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272, ___U24this_1)); }
	inline PagedScrollRect_t3286523126 * get_U24this_1() const { return ___U24this_1; }
	inline PagedScrollRect_t3286523126 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PagedScrollRect_t3286523126 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSNAPTOPAGECOROUTINEU3EC__ITERATOR0_T3387229272_H
#ifndef LINEBREAKINGTABLE_T1457697482_H
#define LINEBREAKINGTABLE_T1457697482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Settings/LineBreakingTable
struct  LineBreakingTable_t1457697482  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Char> TMPro.TMP_Settings/LineBreakingTable::leadingCharacters
	Dictionary_2_t2523173801 * ___leadingCharacters_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Char> TMPro.TMP_Settings/LineBreakingTable::followingCharacters
	Dictionary_2_t2523173801 * ___followingCharacters_1;

public:
	inline static int32_t get_offset_of_leadingCharacters_0() { return static_cast<int32_t>(offsetof(LineBreakingTable_t1457697482, ___leadingCharacters_0)); }
	inline Dictionary_2_t2523173801 * get_leadingCharacters_0() const { return ___leadingCharacters_0; }
	inline Dictionary_2_t2523173801 ** get_address_of_leadingCharacters_0() { return &___leadingCharacters_0; }
	inline void set_leadingCharacters_0(Dictionary_2_t2523173801 * value)
	{
		___leadingCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___leadingCharacters_0), value);
	}

	inline static int32_t get_offset_of_followingCharacters_1() { return static_cast<int32_t>(offsetof(LineBreakingTable_t1457697482, ___followingCharacters_1)); }
	inline Dictionary_2_t2523173801 * get_followingCharacters_1() const { return ___followingCharacters_1; }
	inline Dictionary_2_t2523173801 ** get_address_of_followingCharacters_1() { return &___followingCharacters_1; }
	inline void set_followingCharacters_1(Dictionary_2_t2523173801 * value)
	{
		___followingCharacters_1 = value;
		Il2CppCodeGenWriteBarrier((&___followingCharacters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEBREAKINGTABLE_T1457697482_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U3CRELEASEBASEMATERIALU3EC__ANONSTOREY3_T317586538_H
#define U3CRELEASEBASEMATERIALU3EC__ANONSTOREY3_T317586538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<ReleaseBaseMaterial>c__AnonStorey3
struct  U3CReleaseBaseMaterialU3Ec__AnonStorey3_t317586538  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<ReleaseBaseMaterial>c__AnonStorey3::baseMaterial
	Material_t340375123 * ___baseMaterial_0;

public:
	inline static int32_t get_offset_of_baseMaterial_0() { return static_cast<int32_t>(offsetof(U3CReleaseBaseMaterialU3Ec__AnonStorey3_t317586538, ___baseMaterial_0)); }
	inline Material_t340375123 * get_baseMaterial_0() const { return ___baseMaterial_0; }
	inline Material_t340375123 ** get_address_of_baseMaterial_0() { return &___baseMaterial_0; }
	inline void set_baseMaterial_0(Material_t340375123 * value)
	{
		___baseMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELEASEBASEMATERIALU3EC__ANONSTOREY3_T317586538_H
#ifndef SPRITEDATAOBJECT_T308163541_H
#define SPRITEDATAOBJECT_T308163541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject
struct  SpriteDataObject_t308163541  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData> TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject::frames
	List_1_t225505033 * ___frames_0;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(SpriteDataObject_t308163541, ___frames_0)); }
	inline List_1_t225505033 * get_frames_0() const { return ___frames_0; }
	inline List_1_t225505033 ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_t225505033 * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEDATAOBJECT_T308163541_H
#ifndef TMP_TEXTELEMENT_T129727469_H
#define TMP_TEXTELEMENT_T129727469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_t129727469  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_T129727469_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TMP_TEXTUTILITIES_T2105690005_H
#define TMP_TEXTUTILITIES_T2105690005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities
struct  TMP_TextUtilities_t2105690005  : public RuntimeObject
{
public:

public:
};

struct TMP_TextUtilities_t2105690005_StaticFields
{
public:
	// UnityEngine.Vector3[] TMPro.TMP_TextUtilities::m_rectWorldCorners
	Vector3U5BU5D_t1718750761* ___m_rectWorldCorners_0;

public:
	inline static int32_t get_offset_of_m_rectWorldCorners_0() { return static_cast<int32_t>(offsetof(TMP_TextUtilities_t2105690005_StaticFields, ___m_rectWorldCorners_0)); }
	inline Vector3U5BU5D_t1718750761* get_m_rectWorldCorners_0() const { return ___m_rectWorldCorners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_rectWorldCorners_0() { return &___m_rectWorldCorners_0; }
	inline void set_m_rectWorldCorners_0(Vector3U5BU5D_t1718750761* value)
	{
		___m_rectWorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectWorldCorners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTUTILITIES_T2105690005_H
#ifndef U3CSTATUSUPDATELOOPU3EC__ITERATOR0_T858545123_H
#define U3CSTATUSUPDATELOOPU3EC__ITERATOR0_T858545123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0
struct  U3CStatusUpdateLoopU3Ec__Iterator0_t858545123  : public RuntimeObject
{
public:
	// HeadsetDemoManager HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0::$this
	HeadsetDemoManager_t1859624338 * ___U24this_0;
	// System.Object HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ec__Iterator0_t858545123, ___U24this_0)); }
	inline HeadsetDemoManager_t1859624338 * get_U24this_0() const { return ___U24this_0; }
	inline HeadsetDemoManager_t1859624338 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(HeadsetDemoManager_t1859624338 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ec__Iterator0_t858545123, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ec__Iterator0_t858545123, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ec__Iterator0_t858545123, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTATUSUPDATELOOPU3EC__ITERATOR0_T858545123_H
#ifndef TMP_UPDATEMANAGER_T4114267509_H
#define TMP_UPDATEMANAGER_T4114267509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateManager
struct  TMP_UpdateManager_t4114267509  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_LayoutRebuildQueue
	List_1_t4071693616 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_GraphicRebuildQueue
	List_1_t4071693616 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t4071693616 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t4071693616 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t4071693616 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t4071693616 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t4071693616 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t4071693616 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateManager_t4114267509_StaticFields
{
public:
	// TMPro.TMP_UpdateManager TMPro.TMP_UpdateManager::s_Instance
	TMP_UpdateManager_t4114267509 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateManager_t4114267509 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateManager_t4114267509 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateManager_t4114267509 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEMANAGER_T4114267509_H
#ifndef TMP_STYLE_T3479380764_H
#define TMP_STYLE_T3479380764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Style
struct  TMP_Style_t3479380764  : public RuntimeObject
{
public:
	// System.String TMPro.TMP_Style::m_Name
	String_t* ___m_Name_0;
	// System.Int32 TMPro.TMP_Style::m_HashCode
	int32_t ___m_HashCode_1;
	// System.String TMPro.TMP_Style::m_OpeningDefinition
	String_t* ___m_OpeningDefinition_2;
	// System.String TMPro.TMP_Style::m_ClosingDefinition
	String_t* ___m_ClosingDefinition_3;
	// System.Int32[] TMPro.TMP_Style::m_OpeningTagArray
	Int32U5BU5D_t385246372* ___m_OpeningTagArray_4;
	// System.Int32[] TMPro.TMP_Style::m_ClosingTagArray
	Int32U5BU5D_t385246372* ___m_ClosingTagArray_5;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_0), value);
	}

	inline static int32_t get_offset_of_m_HashCode_1() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_HashCode_1)); }
	inline int32_t get_m_HashCode_1() const { return ___m_HashCode_1; }
	inline int32_t* get_address_of_m_HashCode_1() { return &___m_HashCode_1; }
	inline void set_m_HashCode_1(int32_t value)
	{
		___m_HashCode_1 = value;
	}

	inline static int32_t get_offset_of_m_OpeningDefinition_2() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_OpeningDefinition_2)); }
	inline String_t* get_m_OpeningDefinition_2() const { return ___m_OpeningDefinition_2; }
	inline String_t** get_address_of_m_OpeningDefinition_2() { return &___m_OpeningDefinition_2; }
	inline void set_m_OpeningDefinition_2(String_t* value)
	{
		___m_OpeningDefinition_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningDefinition_2), value);
	}

	inline static int32_t get_offset_of_m_ClosingDefinition_3() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_ClosingDefinition_3)); }
	inline String_t* get_m_ClosingDefinition_3() const { return ___m_ClosingDefinition_3; }
	inline String_t** get_address_of_m_ClosingDefinition_3() { return &___m_ClosingDefinition_3; }
	inline void set_m_ClosingDefinition_3(String_t* value)
	{
		___m_ClosingDefinition_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingDefinition_3), value);
	}

	inline static int32_t get_offset_of_m_OpeningTagArray_4() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_OpeningTagArray_4)); }
	inline Int32U5BU5D_t385246372* get_m_OpeningTagArray_4() const { return ___m_OpeningTagArray_4; }
	inline Int32U5BU5D_t385246372** get_address_of_m_OpeningTagArray_4() { return &___m_OpeningTagArray_4; }
	inline void set_m_OpeningTagArray_4(Int32U5BU5D_t385246372* value)
	{
		___m_OpeningTagArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningTagArray_4), value);
	}

	inline static int32_t get_offset_of_m_ClosingTagArray_5() { return static_cast<int32_t>(offsetof(TMP_Style_t3479380764, ___m_ClosingTagArray_5)); }
	inline Int32U5BU5D_t385246372* get_m_ClosingTagArray_5() const { return ___m_ClosingTagArray_5; }
	inline Int32U5BU5D_t385246372** get_address_of_m_ClosingTagArray_5() { return &___m_ClosingTagArray_5; }
	inline void set_m_ClosingTagArray_5(Int32U5BU5D_t385246372* value)
	{
		___m_ClosingTagArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingTagArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLE_T3479380764_H
#ifndef U3CREMOVESTENCILMATERIALU3EC__ANONSTOREY2_T2443477048_H
#define U3CREMOVESTENCILMATERIALU3EC__ANONSTOREY2_T2443477048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<RemoveStencilMaterial>c__AnonStorey2
struct  U3CRemoveStencilMaterialU3Ec__AnonStorey2_t2443477048  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<RemoveStencilMaterial>c__AnonStorey2::stencilMaterial
	Material_t340375123 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CRemoveStencilMaterialU3Ec__AnonStorey2_t2443477048, ___stencilMaterial_0)); }
	inline Material_t340375123 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t340375123 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t340375123 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVESTENCILMATERIALU3EC__ANONSTOREY2_T2443477048_H
#ifndef APPINFO_T2433711276_H
#define APPINFO_T2433711276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Store.AppInfo
struct  AppInfo_t2433711276  : public RuntimeObject
{
public:
	// System.String UnityEngine.Store.AppInfo::<appId>k__BackingField
	String_t* ___U3CappIdU3Ek__BackingField_0;
	// System.String UnityEngine.Store.AppInfo::<appKey>k__BackingField
	String_t* ___U3CappKeyU3Ek__BackingField_1;
	// System.String UnityEngine.Store.AppInfo::<clientId>k__BackingField
	String_t* ___U3CclientIdU3Ek__BackingField_2;
	// System.String UnityEngine.Store.AppInfo::<clientKey>k__BackingField
	String_t* ___U3CclientKeyU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Store.AppInfo::<debug>k__BackingField
	bool ___U3CdebugU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CappIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CappIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CappIdU3Ek__BackingField_0() const { return ___U3CappIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CappIdU3Ek__BackingField_0() { return &___U3CappIdU3Ek__BackingField_0; }
	inline void set_U3CappIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CappIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CappIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CappKeyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CappKeyU3Ek__BackingField_1)); }
	inline String_t* get_U3CappKeyU3Ek__BackingField_1() const { return ___U3CappKeyU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CappKeyU3Ek__BackingField_1() { return &___U3CappKeyU3Ek__BackingField_1; }
	inline void set_U3CappKeyU3Ek__BackingField_1(String_t* value)
	{
		___U3CappKeyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CappKeyU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CclientIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CclientIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CclientIdU3Ek__BackingField_2() const { return ___U3CclientIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CclientIdU3Ek__BackingField_2() { return &___U3CclientIdU3Ek__BackingField_2; }
	inline void set_U3CclientIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CclientIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclientIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CclientKeyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CclientKeyU3Ek__BackingField_3)); }
	inline String_t* get_U3CclientKeyU3Ek__BackingField_3() const { return ___U3CclientKeyU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CclientKeyU3Ek__BackingField_3() { return &___U3CclientKeyU3Ek__BackingField_3; }
	inline void set_U3CclientKeyU3Ek__BackingField_3(String_t* value)
	{
		___U3CclientKeyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclientKeyU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdebugU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CdebugU3Ek__BackingField_4)); }
	inline bool get_U3CdebugU3Ek__BackingField_4() const { return ___U3CdebugU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CdebugU3Ek__BackingField_4() { return &___U3CdebugU3Ek__BackingField_4; }
	inline void set_U3CdebugU3Ek__BackingField_4(bool value)
	{
		___U3CdebugU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINFO_T2433711276_H
#ifndef TMP_MATERIALMANAGER_T2944071966_H
#define TMP_MATERIALMANAGER_T2944071966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager
struct  TMP_MaterialManager_t2944071966  : public RuntimeObject
{
public:

public:
};

struct TMP_MaterialManager_t2944071966_StaticFields
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/MaskingMaterial> TMPro.TMP_MaterialManager::m_materialList
	List_1_t1488971017 * ___m_materialList_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_MaterialManager/FallbackMaterial> TMPro.TMP_MaterialManager::m_fallbackMaterials
	Dictionary_2_t939051307 * ___m_fallbackMaterials_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int64> TMPro.TMP_MaterialManager::m_fallbackMaterialLookup
	Dictionary_2_t2625280635 * ___m_fallbackMaterialLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/FallbackMaterial> TMPro.TMP_MaterialManager::m_fallbackCleanupList
	List_1_t1348486265 * ___m_fallbackCleanupList_3;
	// System.Boolean TMPro.TMP_MaterialManager::isFallbackListDirty
	bool ___isFallbackListDirty_4;

public:
	inline static int32_t get_offset_of_m_materialList_0() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___m_materialList_0)); }
	inline List_1_t1488971017 * get_m_materialList_0() const { return ___m_materialList_0; }
	inline List_1_t1488971017 ** get_address_of_m_materialList_0() { return &___m_materialList_0; }
	inline void set_m_materialList_0(List_1_t1488971017 * value)
	{
		___m_materialList_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialList_0), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterials_1() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___m_fallbackMaterials_1)); }
	inline Dictionary_2_t939051307 * get_m_fallbackMaterials_1() const { return ___m_fallbackMaterials_1; }
	inline Dictionary_2_t939051307 ** get_address_of_m_fallbackMaterials_1() { return &___m_fallbackMaterials_1; }
	inline void set_m_fallbackMaterials_1(Dictionary_2_t939051307 * value)
	{
		___m_fallbackMaterials_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterials_1), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterialLookup_2() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___m_fallbackMaterialLookup_2)); }
	inline Dictionary_2_t2625280635 * get_m_fallbackMaterialLookup_2() const { return ___m_fallbackMaterialLookup_2; }
	inline Dictionary_2_t2625280635 ** get_address_of_m_fallbackMaterialLookup_2() { return &___m_fallbackMaterialLookup_2; }
	inline void set_m_fallbackMaterialLookup_2(Dictionary_2_t2625280635 * value)
	{
		___m_fallbackMaterialLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterialLookup_2), value);
	}

	inline static int32_t get_offset_of_m_fallbackCleanupList_3() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___m_fallbackCleanupList_3)); }
	inline List_1_t1348486265 * get_m_fallbackCleanupList_3() const { return ___m_fallbackCleanupList_3; }
	inline List_1_t1348486265 ** get_address_of_m_fallbackCleanupList_3() { return &___m_fallbackCleanupList_3; }
	inline void set_m_fallbackCleanupList_3(List_1_t1348486265 * value)
	{
		___m_fallbackCleanupList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackCleanupList_3), value);
	}

	inline static int32_t get_offset_of_isFallbackListDirty_4() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t2944071966_StaticFields, ___isFallbackListDirty_4)); }
	inline bool get_isFallbackListDirty_4() const { return ___isFallbackListDirty_4; }
	inline bool* get_address_of_isFallbackListDirty_4() { return &___isFallbackListDirty_4; }
	inline void set_isFallbackListDirty_4(bool value)
	{
		___isFallbackListDirty_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATERIALMANAGER_T2944071966_H
#ifndef FALLBACKMATERIAL_T4171378819_H
#define FALLBACKMATERIAL_T4171378819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/FallbackMaterial
struct  FallbackMaterial_t4171378819  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_MaterialManager/FallbackMaterial::baseID
	int32_t ___baseID_0;
	// UnityEngine.Material TMPro.TMP_MaterialManager/FallbackMaterial::baseMaterial
	Material_t340375123 * ___baseMaterial_1;
	// System.Int64 TMPro.TMP_MaterialManager/FallbackMaterial::fallbackID
	int64_t ___fallbackID_2;
	// UnityEngine.Material TMPro.TMP_MaterialManager/FallbackMaterial::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_3;
	// System.Int32 TMPro.TMP_MaterialManager/FallbackMaterial::count
	int32_t ___count_4;

public:
	inline static int32_t get_offset_of_baseID_0() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___baseID_0)); }
	inline int32_t get_baseID_0() const { return ___baseID_0; }
	inline int32_t* get_address_of_baseID_0() { return &___baseID_0; }
	inline void set_baseID_0(int32_t value)
	{
		___baseID_0 = value;
	}

	inline static int32_t get_offset_of_baseMaterial_1() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___baseMaterial_1)); }
	inline Material_t340375123 * get_baseMaterial_1() const { return ___baseMaterial_1; }
	inline Material_t340375123 ** get_address_of_baseMaterial_1() { return &___baseMaterial_1; }
	inline void set_baseMaterial_1(Material_t340375123 * value)
	{
		___baseMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_1), value);
	}

	inline static int32_t get_offset_of_fallbackID_2() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___fallbackID_2)); }
	inline int64_t get_fallbackID_2() const { return ___fallbackID_2; }
	inline int64_t* get_address_of_fallbackID_2() { return &___fallbackID_2; }
	inline void set_fallbackID_2(int64_t value)
	{
		___fallbackID_2 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_3() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___fallbackMaterial_3)); }
	inline Material_t340375123 * get_fallbackMaterial_3() const { return ___fallbackMaterial_3; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_3() { return &___fallbackMaterial_3; }
	inline void set_fallbackMaterial_3(Material_t340375123 * value)
	{
		___fallbackMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_3), value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(FallbackMaterial_t4171378819, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FALLBACKMATERIAL_T4171378819_H
#ifndef SETPROPERTYUTILITY_T2931591262_H
#define SETPROPERTYUTILITY_T2931591262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SetPropertyUtility
struct  SetPropertyUtility_t2931591262  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROPERTYUTILITY_T2931591262_H
#ifndef U3CCARETBLINKU3EC__ITERATOR0_T4293627739_H
#define U3CCARETBLINKU3EC__ITERATOR0_T4293627739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/<CaretBlink>c__Iterator0
struct  U3CCaretBlinkU3Ec__Iterator0_t4293627739  : public RuntimeObject
{
public:
	// System.Single TMPro.TMP_InputField/<CaretBlink>c__Iterator0::<blinkPeriod>__1
	float ___U3CblinkPeriodU3E__1_0;
	// System.Boolean TMPro.TMP_InputField/<CaretBlink>c__Iterator0::<blinkState>__1
	bool ___U3CblinkStateU3E__1_1;
	// TMPro.TMP_InputField TMPro.TMP_InputField/<CaretBlink>c__Iterator0::$this
	TMP_InputField_t1099764886 * ___U24this_2;
	// System.Object TMPro.TMP_InputField/<CaretBlink>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean TMPro.TMP_InputField/<CaretBlink>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 TMPro.TMP_InputField/<CaretBlink>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CblinkPeriodU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t4293627739, ___U3CblinkPeriodU3E__1_0)); }
	inline float get_U3CblinkPeriodU3E__1_0() const { return ___U3CblinkPeriodU3E__1_0; }
	inline float* get_address_of_U3CblinkPeriodU3E__1_0() { return &___U3CblinkPeriodU3E__1_0; }
	inline void set_U3CblinkPeriodU3E__1_0(float value)
	{
		___U3CblinkPeriodU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CblinkStateU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t4293627739, ___U3CblinkStateU3E__1_1)); }
	inline bool get_U3CblinkStateU3E__1_1() const { return ___U3CblinkStateU3E__1_1; }
	inline bool* get_address_of_U3CblinkStateU3E__1_1() { return &___U3CblinkStateU3E__1_1; }
	inline void set_U3CblinkStateU3E__1_1(bool value)
	{
		___U3CblinkStateU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t4293627739, ___U24this_2)); }
	inline TMP_InputField_t1099764886 * get_U24this_2() const { return ___U24this_2; }
	inline TMP_InputField_t1099764886 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TMP_InputField_t1099764886 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t4293627739, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t4293627739, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t4293627739, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCARETBLINKU3EC__ITERATOR0_T4293627739_H
#ifndef U3CADDMASKINGMATERIALU3EC__ANONSTOREY1_T3195963584_H
#define U3CADDMASKINGMATERIALU3EC__ANONSTOREY1_T3195963584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<AddMaskingMaterial>c__AnonStorey1
struct  U3CAddMaskingMaterialU3Ec__AnonStorey1_t3195963584  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<AddMaskingMaterial>c__AnonStorey1::stencilMaterial
	Material_t340375123 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CAddMaskingMaterialU3Ec__AnonStorey1_t3195963584, ___stencilMaterial_0)); }
	inline Material_t340375123 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t340375123 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t340375123 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDMASKINGMATERIALU3EC__ANONSTOREY1_T3195963584_H
#ifndef MASKINGMATERIAL_T16896275_H
#define MASKINGMATERIAL_T16896275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/MaskingMaterial
struct  MaskingMaterial_t16896275  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/MaskingMaterial::baseMaterial
	Material_t340375123 * ___baseMaterial_0;
	// UnityEngine.Material TMPro.TMP_MaterialManager/MaskingMaterial::stencilMaterial
	Material_t340375123 * ___stencilMaterial_1;
	// System.Int32 TMPro.TMP_MaterialManager/MaskingMaterial::count
	int32_t ___count_2;
	// System.Int32 TMPro.TMP_MaterialManager/MaskingMaterial::stencilID
	int32_t ___stencilID_3;

public:
	inline static int32_t get_offset_of_baseMaterial_0() { return static_cast<int32_t>(offsetof(MaskingMaterial_t16896275, ___baseMaterial_0)); }
	inline Material_t340375123 * get_baseMaterial_0() const { return ___baseMaterial_0; }
	inline Material_t340375123 ** get_address_of_baseMaterial_0() { return &___baseMaterial_0; }
	inline void set_baseMaterial_0(Material_t340375123 * value)
	{
		___baseMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_0), value);
	}

	inline static int32_t get_offset_of_stencilMaterial_1() { return static_cast<int32_t>(offsetof(MaskingMaterial_t16896275, ___stencilMaterial_1)); }
	inline Material_t340375123 * get_stencilMaterial_1() const { return ___stencilMaterial_1; }
	inline Material_t340375123 ** get_address_of_stencilMaterial_1() { return &___stencilMaterial_1; }
	inline void set_stencilMaterial_1(Material_t340375123 * value)
	{
		___stencilMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(MaskingMaterial_t16896275, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_stencilID_3() { return static_cast<int32_t>(offsetof(MaskingMaterial_t16896275, ___stencilID_3)); }
	inline int32_t get_stencilID_3() const { return ___stencilID_3; }
	inline int32_t* get_address_of_stencilID_3() { return &___stencilID_3; }
	inline void set_stencilID_3(int32_t value)
	{
		___stencilID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGMATERIAL_T16896275_H
#ifndef U3CGETBASEMATERIALU3EC__ANONSTOREY0_T3090515672_H
#define U3CGETBASEMATERIALU3EC__ANONSTOREY0_T3090515672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<GetBaseMaterial>c__AnonStorey0
struct  U3CGetBaseMaterialU3Ec__AnonStorey0_t3090515672  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<GetBaseMaterial>c__AnonStorey0::stencilMaterial
	Material_t340375123 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CGetBaseMaterialU3Ec__AnonStorey0_t3090515672, ___stencilMaterial_0)); }
	inline Material_t340375123 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t340375123 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t340375123 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBASEMATERIALU3EC__ANONSTOREY0_T3090515672_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef UPDATEDATA_T722198904_H
#define UPDATEDATA_T722198904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseScrollEffect/UpdateData
struct  UpdateData_t722198904 
{
public:
	// UnityEngine.RectTransform BaseScrollEffect/UpdateData::page
	RectTransform_t3704657025 * ___page_0;
	// System.Int32 BaseScrollEffect/UpdateData::pageIndex
	int32_t ___pageIndex_1;
	// System.Int32 BaseScrollEffect/UpdateData::pageCount
	int32_t ___pageCount_2;
	// System.Single BaseScrollEffect/UpdateData::pageOffset
	float ___pageOffset_3;
	// System.Single BaseScrollEffect/UpdateData::scrollOffset
	float ___scrollOffset_4;
	// System.Single BaseScrollEffect/UpdateData::spacing
	float ___spacing_5;
	// System.Boolean BaseScrollEffect/UpdateData::looping
	bool ___looping_6;
	// System.Boolean BaseScrollEffect/UpdateData::isInteractable
	bool ___isInteractable_7;
	// System.Single BaseScrollEffect/UpdateData::moveDistance
	float ___moveDistance_8;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(UpdateData_t722198904, ___page_0)); }
	inline RectTransform_t3704657025 * get_page_0() const { return ___page_0; }
	inline RectTransform_t3704657025 ** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(RectTransform_t3704657025 * value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_pageIndex_1() { return static_cast<int32_t>(offsetof(UpdateData_t722198904, ___pageIndex_1)); }
	inline int32_t get_pageIndex_1() const { return ___pageIndex_1; }
	inline int32_t* get_address_of_pageIndex_1() { return &___pageIndex_1; }
	inline void set_pageIndex_1(int32_t value)
	{
		___pageIndex_1 = value;
	}

	inline static int32_t get_offset_of_pageCount_2() { return static_cast<int32_t>(offsetof(UpdateData_t722198904, ___pageCount_2)); }
	inline int32_t get_pageCount_2() const { return ___pageCount_2; }
	inline int32_t* get_address_of_pageCount_2() { return &___pageCount_2; }
	inline void set_pageCount_2(int32_t value)
	{
		___pageCount_2 = value;
	}

	inline static int32_t get_offset_of_pageOffset_3() { return static_cast<int32_t>(offsetof(UpdateData_t722198904, ___pageOffset_3)); }
	inline float get_pageOffset_3() const { return ___pageOffset_3; }
	inline float* get_address_of_pageOffset_3() { return &___pageOffset_3; }
	inline void set_pageOffset_3(float value)
	{
		___pageOffset_3 = value;
	}

	inline static int32_t get_offset_of_scrollOffset_4() { return static_cast<int32_t>(offsetof(UpdateData_t722198904, ___scrollOffset_4)); }
	inline float get_scrollOffset_4() const { return ___scrollOffset_4; }
	inline float* get_address_of_scrollOffset_4() { return &___scrollOffset_4; }
	inline void set_scrollOffset_4(float value)
	{
		___scrollOffset_4 = value;
	}

	inline static int32_t get_offset_of_spacing_5() { return static_cast<int32_t>(offsetof(UpdateData_t722198904, ___spacing_5)); }
	inline float get_spacing_5() const { return ___spacing_5; }
	inline float* get_address_of_spacing_5() { return &___spacing_5; }
	inline void set_spacing_5(float value)
	{
		___spacing_5 = value;
	}

	inline static int32_t get_offset_of_looping_6() { return static_cast<int32_t>(offsetof(UpdateData_t722198904, ___looping_6)); }
	inline bool get_looping_6() const { return ___looping_6; }
	inline bool* get_address_of_looping_6() { return &___looping_6; }
	inline void set_looping_6(bool value)
	{
		___looping_6 = value;
	}

	inline static int32_t get_offset_of_isInteractable_7() { return static_cast<int32_t>(offsetof(UpdateData_t722198904, ___isInteractable_7)); }
	inline bool get_isInteractable_7() const { return ___isInteractable_7; }
	inline bool* get_address_of_isInteractable_7() { return &___isInteractable_7; }
	inline void set_isInteractable_7(bool value)
	{
		___isInteractable_7 = value;
	}

	inline static int32_t get_offset_of_moveDistance_8() { return static_cast<int32_t>(offsetof(UpdateData_t722198904, ___moveDistance_8)); }
	inline float get_moveDistance_8() const { return ___moveDistance_8; }
	inline float* get_address_of_moveDistance_8() { return &___moveDistance_8; }
	inline void set_moveDistance_8(float value)
	{
		___moveDistance_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BaseScrollEffect/UpdateData
struct UpdateData_t722198904_marshaled_pinvoke
{
	RectTransform_t3704657025 * ___page_0;
	int32_t ___pageIndex_1;
	int32_t ___pageCount_2;
	float ___pageOffset_3;
	float ___scrollOffset_4;
	float ___spacing_5;
	int32_t ___looping_6;
	int32_t ___isInteractable_7;
	float ___moveDistance_8;
};
// Native definition for COM marshalling of BaseScrollEffect/UpdateData
struct UpdateData_t722198904_marshaled_com
{
	RectTransform_t3704657025 * ___page_0;
	int32_t ___pageIndex_1;
	int32_t ___pageCount_2;
	float ___pageOffset_3;
	float ___scrollOffset_4;
	float ___spacing_5;
	int32_t ___looping_6;
	int32_t ___isInteractable_7;
	float ___moveDistance_8;
};
#endif // UPDATEDATA_T722198904_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef UNITYEVENT_1_T2729110193_H
#define UNITYEVENT_1_T2729110193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2729110193  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2729110193, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2729110193_H
#ifndef UNITYEVENT_3_T1597070127_H
#define UNITYEVENT_3_T1597070127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t1597070127  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1597070127, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1597070127_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef U24ARRAYTYPEU3D40_T2865632059_H
#define U24ARRAYTYPEU3D40_T2865632059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D40_t2865632059 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D40_t2865632059__padding[40];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D40_T2865632059_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef NULLABLE_1_T3119828856_H
#define NULLABLE_1_T3119828856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t3119828856 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3119828856_H
#ifndef TMP_BASICXMLTAGSTACK_T2962628096_H
#define TMP_BASICXMLTAGSTACK_T2962628096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t2962628096 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T2962628096_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef TMP_XMLTAGSTACK_1_T960921318_H
#define TMP_XMLTAGSTACK_1_T960921318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t960921318 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t1444911251* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___itemStack_0)); }
	inline SingleU5BU5D_t1444911251* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t1444911251* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T960921318_H
#ifndef TMP_XMLTAGSTACK_1_T2514600297_H
#define TMP_XMLTAGSTACK_1_T2514600297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2514600297 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t385246372* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___itemStack_0)); }
	inline Int32U5BU5D_t385246372* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t385246372* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2514600297_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef SPRITESIZE_T3355290999_H
#define SPRITESIZE_T3355290999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize
struct  SpriteSize_t3355290999 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::w
	float ___w_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::h
	float ___h_1;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(SpriteSize_t3355290999, ___w_0)); }
	inline float get_w_0() const { return ___w_0; }
	inline float* get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(float value)
	{
		___w_0 = value;
	}

	inline static int32_t get_offset_of_h_1() { return static_cast<int32_t>(offsetof(SpriteSize_t3355290999, ___h_1)); }
	inline float get_h_1() const { return ___h_1; }
	inline float* get_address_of_h_1() { return &___h_1; }
	inline void set_h_1(float value)
	{
		___h_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESIZE_T3355290999_H
#ifndef SPRITEFRAME_T3912389194_H
#define SPRITEFRAME_T3912389194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame
struct  SpriteFrame_t3912389194 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::x
	float ___x_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::y
	float ___y_1;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::w
	float ___w_2;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::h
	float ___h_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEFRAME_T3912389194_H
#ifndef TMP_XMLTAGSTACK_1_T3241710312_H
#define TMP_XMLTAGSTACK_1_T3241710312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t3241710312 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2496920137* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_t3678055768 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t2496920137* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t2496920137** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t2496920137* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_t3678055768 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_t3678055768 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3241710312_H
#ifndef SNAPDIRECTION_T167415511_H
#define SNAPDIRECTION_T167415511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PagedScrollRect/SnapDirection
struct  SnapDirection_t167415511 
{
public:
	// System.Int32 PagedScrollRect/SnapDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SnapDirection_t167415511, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNAPDIRECTION_T167415511_H
#ifndef EMULATEDPLATFORMTYPE_T3351326478_H
#define EMULATEDPLATFORMTYPE_T3351326478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoInputManager/EmulatedPlatformType
struct  EmulatedPlatformType_t3351326478 
{
public:
	// System.Int32 DemoInputManager/EmulatedPlatformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EmulatedPlatformType_t3351326478, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATEDPLATFORMTYPE_T3351326478_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TMP_VERTEX_T2404176824_H
#define TMP_VERTEX_T2404176824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t2404176824 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t2156229523  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t2156229523  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t2156229523  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t2600501292  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv_1)); }
	inline Vector2_t2156229523  get_uv_1() const { return ___uv_1; }
	inline Vector2_t2156229523 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t2156229523  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv2_2)); }
	inline Vector2_t2156229523  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t2156229523 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t2156229523  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv4_3)); }
	inline Vector2_t2156229523  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t2156229523 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t2156229523  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___color_4)); }
	inline Color32_t2600501292  get_color_4() const { return ___color_4; }
	inline Color32_t2600501292 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t2600501292  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T2404176824_H
#ifndef DIRECTION_T3470714353_H
#define DIRECTION_T3470714353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar/Direction
struct  Direction_t3470714353 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t3470714353, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T3470714353_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef EXTENTS_T3837212874_H
#define EXTENTS_T3837212874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3837212874 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3837212874_H
#ifndef TMP_XMLTAGSTACK_1_T1515999176_H
#define TMP_XMLTAGSTACK_1_T1515999176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t1515999176 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t648826345* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t1952344632  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t648826345* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t648826345* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_defaultItem_3)); }
	inline MaterialReference_t1952344632  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t1952344632 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t1952344632  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1515999176_H
#ifndef VERTEXGRADIENT_T345148380_H
#define VERTEXGRADIENT_T345148380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t345148380 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2555686324  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2555686324  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2555686324  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2555686324  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topLeft_0)); }
	inline Color_t2555686324  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2555686324 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2555686324  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topRight_1)); }
	inline Color_t2555686324  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2555686324 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2555686324  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomLeft_2)); }
	inline Color_t2555686324  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2555686324 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2555686324  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomRight_3)); }
	inline Color_t2555686324  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2555686324 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2555686324  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T345148380_H
#ifndef LINETYPE_T2817627283_H
#define LINETYPE_T2817627283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/LineType
struct  LineType_t2817627283 
{
public:
	// System.Int32 TMPro.TMP_InputField/LineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineType_t2817627283, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T2817627283_H
#ifndef TMP_XMLTAGSTACK_1_T2164155836_H
#define TMP_XMLTAGSTACK_1_T2164155836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t2164155836 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t3850468773* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t2600501292  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___itemStack_0)); }
	inline Color32U5BU5D_t3850468773* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t3850468773* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_defaultItem_3)); }
	inline Color32_t2600501292  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t2600501292 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t2600501292  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2164155836_H
#ifndef TILEORDERBY_T2063207047_H
#define TILEORDERBY_T2063207047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiledPage/TileOrderBy
struct  TileOrderBy_t2063207047 
{
public:
	// System.Int32 TiledPage/TileOrderBy::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TileOrderBy_t2063207047, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEORDERBY_T2063207047_H
#ifndef _VERTICALALIGNMENTOPTIONS_T2825528260_H
#define _VERTICALALIGNMENTOPTIONS_T2825528260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._VerticalAlignmentOptions
struct  _VerticalAlignmentOptions_t2825528260 
{
public:
	// System.Int32 TMPro._VerticalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_VerticalAlignmentOptions_t2825528260, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _VERTICALALIGNMENTOPTIONS_T2825528260_H
#ifndef FONTSTYLES_T3828945032_H
#define FONTSTYLES_T3828945032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3828945032 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3828945032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3828945032_H
#ifndef FONTWEIGHTS_T3122883458_H
#define FONTWEIGHTS_T3122883458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeights
struct  FontWeights_t3122883458 
{
public:
	// System.Int32 TMPro.FontWeights::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontWeights_t3122883458, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHTS_T3122883458_H
#ifndef TAGUNITS_T1169424683_H
#define TAGUNITS_T1169424683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnits
struct  TagUnits_t1169424683 
{
public:
	// System.Int32 TMPro.TagUnits::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagUnits_t1169424683, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITS_T1169424683_H
#ifndef TAGTYPE_T123236451_H
#define TAGTYPE_T123236451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t123236451 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagType_t123236451, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T123236451_H
#ifndef TEXTINPUTSOURCES_T1522115805_H
#define TEXTINPUTSOURCES_T1522115805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text/TextInputSources
struct  TextInputSources_t1522115805 
{
public:
	// System.Int32 TMPro.TMP_Text/TextInputSources::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextInputSources_t1522115805, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T1522115805_H
#ifndef TMP_SPRITE_T554067146_H
#define TMP_SPRITE_T554067146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Sprite
struct  TMP_Sprite_t554067146  : public TMP_TextElement_t129727469
{
public:
	// System.String TMPro.TMP_Sprite::name
	String_t* ___name_9;
	// System.Int32 TMPro.TMP_Sprite::hashCode
	int32_t ___hashCode_10;
	// System.Int32 TMPro.TMP_Sprite::unicode
	int32_t ___unicode_11;
	// UnityEngine.Vector2 TMPro.TMP_Sprite::pivot
	Vector2_t2156229523  ___pivot_12;
	// UnityEngine.Sprite TMPro.TMP_Sprite::sprite
	Sprite_t280657092 * ___sprite_13;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_hashCode_10() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___hashCode_10)); }
	inline int32_t get_hashCode_10() const { return ___hashCode_10; }
	inline int32_t* get_address_of_hashCode_10() { return &___hashCode_10; }
	inline void set_hashCode_10(int32_t value)
	{
		___hashCode_10 = value;
	}

	inline static int32_t get_offset_of_unicode_11() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___unicode_11)); }
	inline int32_t get_unicode_11() const { return ___unicode_11; }
	inline int32_t* get_address_of_unicode_11() { return &___unicode_11; }
	inline void set_unicode_11(int32_t value)
	{
		___unicode_11 = value;
	}

	inline static int32_t get_offset_of_pivot_12() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___pivot_12)); }
	inline Vector2_t2156229523  get_pivot_12() const { return ___pivot_12; }
	inline Vector2_t2156229523 * get_address_of_pivot_12() { return &___pivot_12; }
	inline void set_pivot_12(Vector2_t2156229523  value)
	{
		___pivot_12 = value;
	}

	inline static int32_t get_offset_of_sprite_13() { return static_cast<int32_t>(offsetof(TMP_Sprite_t554067146, ___sprite_13)); }
	inline Sprite_t280657092 * get_sprite_13() const { return ___sprite_13; }
	inline Sprite_t280657092 ** get_address_of_sprite_13() { return &___sprite_13; }
	inline void set_sprite_13(Sprite_t280657092 * value)
	{
		___sprite_13 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITE_T554067146_H
#ifndef TMP_MESHINFO_T2771747634_H
#define TMP_MESHINFO_T2771747634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MeshInfo
struct  TMP_MeshInfo_t2771747634 
{
public:
	// UnityEngine.Mesh TMPro.TMP_MeshInfo::mesh
	Mesh_t3648964284 * ___mesh_3;
	// System.Int32 TMPro.TMP_MeshInfo::vertexCount
	int32_t ___vertexCount_4;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::vertices
	Vector3U5BU5D_t1718750761* ___vertices_5;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::normals
	Vector3U5BU5D_t1718750761* ___normals_6;
	// UnityEngine.Vector4[] TMPro.TMP_MeshInfo::tangents
	Vector4U5BU5D_t934056436* ___tangents_7;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs0
	Vector2U5BU5D_t1457185986* ___uvs0_8;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs2
	Vector2U5BU5D_t1457185986* ___uvs2_9;
	// UnityEngine.Color32[] TMPro.TMP_MeshInfo::colors32
	Color32U5BU5D_t3850468773* ___colors32_10;
	// System.Int32[] TMPro.TMP_MeshInfo::triangles
	Int32U5BU5D_t385246372* ___triangles_11;

public:
	inline static int32_t get_offset_of_mesh_3() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___mesh_3)); }
	inline Mesh_t3648964284 * get_mesh_3() const { return ___mesh_3; }
	inline Mesh_t3648964284 ** get_address_of_mesh_3() { return &___mesh_3; }
	inline void set_mesh_3(Mesh_t3648964284 * value)
	{
		___mesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_3), value);
	}

	inline static int32_t get_offset_of_vertexCount_4() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___vertexCount_4)); }
	inline int32_t get_vertexCount_4() const { return ___vertexCount_4; }
	inline int32_t* get_address_of_vertexCount_4() { return &___vertexCount_4; }
	inline void set_vertexCount_4(int32_t value)
	{
		___vertexCount_4 = value;
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___vertices_5)); }
	inline Vector3U5BU5D_t1718750761* get_vertices_5() const { return ___vertices_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(Vector3U5BU5D_t1718750761* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_normals_6() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___normals_6)); }
	inline Vector3U5BU5D_t1718750761* get_normals_6() const { return ___normals_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_normals_6() { return &___normals_6; }
	inline void set_normals_6(Vector3U5BU5D_t1718750761* value)
	{
		___normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___normals_6), value);
	}

	inline static int32_t get_offset_of_tangents_7() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___tangents_7)); }
	inline Vector4U5BU5D_t934056436* get_tangents_7() const { return ___tangents_7; }
	inline Vector4U5BU5D_t934056436** get_address_of_tangents_7() { return &___tangents_7; }
	inline void set_tangents_7(Vector4U5BU5D_t934056436* value)
	{
		___tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_7), value);
	}

	inline static int32_t get_offset_of_uvs0_8() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___uvs0_8)); }
	inline Vector2U5BU5D_t1457185986* get_uvs0_8() const { return ___uvs0_8; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvs0_8() { return &___uvs0_8; }
	inline void set_uvs0_8(Vector2U5BU5D_t1457185986* value)
	{
		___uvs0_8 = value;
		Il2CppCodeGenWriteBarrier((&___uvs0_8), value);
	}

	inline static int32_t get_offset_of_uvs2_9() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___uvs2_9)); }
	inline Vector2U5BU5D_t1457185986* get_uvs2_9() const { return ___uvs2_9; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvs2_9() { return &___uvs2_9; }
	inline void set_uvs2_9(Vector2U5BU5D_t1457185986* value)
	{
		___uvs2_9 = value;
		Il2CppCodeGenWriteBarrier((&___uvs2_9), value);
	}

	inline static int32_t get_offset_of_colors32_10() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___colors32_10)); }
	inline Color32U5BU5D_t3850468773* get_colors32_10() const { return ___colors32_10; }
	inline Color32U5BU5D_t3850468773** get_address_of_colors32_10() { return &___colors32_10; }
	inline void set_colors32_10(Color32U5BU5D_t3850468773* value)
	{
		___colors32_10 = value;
		Il2CppCodeGenWriteBarrier((&___colors32_10), value);
	}

	inline static int32_t get_offset_of_triangles_11() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634, ___triangles_11)); }
	inline Int32U5BU5D_t385246372* get_triangles_11() const { return ___triangles_11; }
	inline Int32U5BU5D_t385246372** get_address_of_triangles_11() { return &___triangles_11; }
	inline void set_triangles_11(Int32U5BU5D_t385246372* value)
	{
		___triangles_11 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_11), value);
	}
};

struct TMP_MeshInfo_t2771747634_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_MeshInfo::s_DefaultColor
	Color32_t2600501292  ___s_DefaultColor_0;
	// UnityEngine.Vector3 TMPro.TMP_MeshInfo::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_1;
	// UnityEngine.Vector4 TMPro.TMP_MeshInfo::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_2;

public:
	inline static int32_t get_offset_of_s_DefaultColor_0() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634_StaticFields, ___s_DefaultColor_0)); }
	inline Color32_t2600501292  get_s_DefaultColor_0() const { return ___s_DefaultColor_0; }
	inline Color32_t2600501292 * get_address_of_s_DefaultColor_0() { return &___s_DefaultColor_0; }
	inline void set_s_DefaultColor_0(Color32_t2600501292  value)
	{
		___s_DefaultColor_0 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_1() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634_StaticFields, ___s_DefaultNormal_1)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_1() const { return ___s_DefaultNormal_1; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_1() { return &___s_DefaultNormal_1; }
	inline void set_s_DefaultNormal_1(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_1 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_2() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t2771747634_StaticFields, ___s_DefaultTangent_2)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_2() const { return ___s_DefaultTangent_2; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_2() { return &___s_DefaultTangent_2; }
	inline void set_s_DefaultTangent_2(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t2771747634_marshaled_pinvoke
{
	Mesh_t3648964284 * ___mesh_3;
	int32_t ___vertexCount_4;
	Vector3_t3722313464 * ___vertices_5;
	Vector3_t3722313464 * ___normals_6;
	Vector4_t3319028937 * ___tangents_7;
	Vector2_t2156229523 * ___uvs0_8;
	Vector2_t2156229523 * ___uvs2_9;
	Color32_t2600501292 * ___colors32_10;
	int32_t* ___triangles_11;
};
// Native definition for COM marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t2771747634_marshaled_com
{
	Mesh_t3648964284 * ___mesh_3;
	int32_t ___vertexCount_4;
	Vector3_t3722313464 * ___vertices_5;
	Vector3_t3722313464 * ___normals_6;
	Vector4_t3319028937 * ___tangents_7;
	Vector2_t2156229523 * ___uvs0_8;
	Vector2_t2156229523 * ___uvs2_9;
	Color32_t2600501292 * ___colors32_10;
	int32_t* ___triangles_11;
};
#endif // TMP_MESHINFO_T2771747634_H
#ifndef VERTEXSORTINGORDER_T2659893934_H
#define VERTEXSORTINGORDER_T2659893934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2659893934 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2659893934, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2659893934_H
#ifndef TEXTUREMAPPINGOPTIONS_T270963663_H
#define TEXTUREMAPPINGOPTIONS_T270963663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_t270963663 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t270963663, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_T270963663_H
#ifndef SPRITEDATA_T3048397587_H
#define SPRITEDATA_T3048397587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct  SpriteData_t3048397587 
{
public:
	// System.String TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::filename
	String_t* ___filename_0;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::frame
	SpriteFrame_t3912389194  ___frame_1;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::rotated
	bool ___rotated_2;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::trimmed
	bool ___trimmed_3;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::spriteSourceSize
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::sourceSize
	SpriteSize_t3355290999  ___sourceSize_5;
	// UnityEngine.Vector2 TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::pivot
	Vector2_t2156229523  ___pivot_6;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___frame_1)); }
	inline SpriteFrame_t3912389194  get_frame_1() const { return ___frame_1; }
	inline SpriteFrame_t3912389194 * get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(SpriteFrame_t3912389194  value)
	{
		___frame_1 = value;
	}

	inline static int32_t get_offset_of_rotated_2() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___rotated_2)); }
	inline bool get_rotated_2() const { return ___rotated_2; }
	inline bool* get_address_of_rotated_2() { return &___rotated_2; }
	inline void set_rotated_2(bool value)
	{
		___rotated_2 = value;
	}

	inline static int32_t get_offset_of_trimmed_3() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___trimmed_3)); }
	inline bool get_trimmed_3() const { return ___trimmed_3; }
	inline bool* get_address_of_trimmed_3() { return &___trimmed_3; }
	inline void set_trimmed_3(bool value)
	{
		___trimmed_3 = value;
	}

	inline static int32_t get_offset_of_spriteSourceSize_4() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___spriteSourceSize_4)); }
	inline SpriteFrame_t3912389194  get_spriteSourceSize_4() const { return ___spriteSourceSize_4; }
	inline SpriteFrame_t3912389194 * get_address_of_spriteSourceSize_4() { return &___spriteSourceSize_4; }
	inline void set_spriteSourceSize_4(SpriteFrame_t3912389194  value)
	{
		___spriteSourceSize_4 = value;
	}

	inline static int32_t get_offset_of_sourceSize_5() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___sourceSize_5)); }
	inline SpriteSize_t3355290999  get_sourceSize_5() const { return ___sourceSize_5; }
	inline SpriteSize_t3355290999 * get_address_of_sourceSize_5() { return &___sourceSize_5; }
	inline void set_sourceSize_5(SpriteSize_t3355290999  value)
	{
		___sourceSize_5 = value;
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___pivot_6)); }
	inline Vector2_t2156229523  get_pivot_6() const { return ___pivot_6; }
	inline Vector2_t2156229523 * get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Vector2_t2156229523  value)
	{
		___pivot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t3048397587_marshaled_pinvoke
{
	char* ___filename_0;
	SpriteFrame_t3912389194  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	SpriteSize_t3355290999  ___sourceSize_5;
	Vector2_t2156229523  ___pivot_6;
};
// Native definition for COM marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t3048397587_marshaled_com
{
	Il2CppChar* ___filename_0;
	SpriteFrame_t3912389194  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	SpriteSize_t3355290999  ___sourceSize_5;
	Vector2_t2156229523  ___pivot_6;
};
#endif // SPRITEDATA_T3048397587_H
#ifndef SPRITEASSETIMPORTFORMATS_T116390639_H
#define SPRITEASSETIMPORTFORMATS_T116390639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.SpriteAssetImportFormats
struct  SpriteAssetImportFormats_t116390639 
{
public:
	// System.Int32 TMPro.SpriteAssetUtilities.SpriteAssetImportFormats::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpriteAssetImportFormats_t116390639, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEASSETIMPORTFORMATS_T116390639_H
#ifndef TEXTALIGNMENTOPTIONS_T4036791236_H
#define TEXTALIGNMENTOPTIONS_T4036791236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4036791236 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4036791236, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4036791236_H
#ifndef _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#define _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._HorizontalAlignmentOptions
struct  _HorizontalAlignmentOptions_t2379014378 
{
public:
	// System.Int32 TMPro._HorizontalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_HorizontalAlignmentOptions_t2379014378, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#ifndef TEXTRENDERFLAGS_T2418684345_H
#define TEXTRENDERFLAGS_T2418684345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t2418684345 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextRenderFlags_t2418684345, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T2418684345_H
#ifndef TMP_TEXTELEMENTTYPE_T1276645592_H
#define TMP_TEXTELEMENTTYPE_T1276645592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1276645592 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1276645592, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1276645592_H
#ifndef MASKINGTYPES_T3687969768_H
#define MASKINGTYPES_T3687969768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t3687969768 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingTypes_t3687969768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T3687969768_H
#ifndef TEXTOVERFLOWMODES_T1430035314_H
#define TEXTOVERFLOWMODES_T1430035314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_t1430035314 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextOverflowModes_t1430035314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_T1430035314_H
#ifndef MASKINGOFFSETMODE_T2266644590_H
#define MASKINGOFFSETMODE_T2266644590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingOffsetMode
struct  MaskingOffsetMode_t2266644590 
{
public:
	// System.Int32 TMPro.MaskingOffsetMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingOffsetMode_t2266644590, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGOFFSETMODE_T2266644590_H
#ifndef EDITSTATE_T2966235475_H
#define EDITSTATE_T2966235475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/EditState
struct  EditState_t2966235475 
{
public:
	// System.Int32 TMPro.TMP_InputField/EditState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EditState_t2966235475, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITSTATE_T2966235475_H
#ifndef SELECTIONEVENT_T4268942288_H
#define SELECTIONEVENT_T4268942288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/SelectionEvent
struct  SelectionEvent_t4268942288  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONEVENT_T4268942288_H
#ifndef CARETPOSITION_T3997512201_H
#define CARETPOSITION_T3997512201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretPosition
struct  CaretPosition_t3997512201 
{
public:
	// System.Int32 TMPro.CaretPosition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaretPosition_t3997512201, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETPOSITION_T3997512201_H
#ifndef LINESEGMENT_T1526544958_H
#define LINESEGMENT_T1526544958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities/LineSegment
struct  LineSegment_t1526544958 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point1
	Vector3_t3722313464  ___Point1_0;
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point2
	Vector3_t3722313464  ___Point2_1;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point1_0)); }
	inline Vector3_t3722313464  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_t3722313464 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_t3722313464  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point2_1)); }
	inline Vector3_t3722313464  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_t3722313464 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_t3722313464  value)
	{
		___Point2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESEGMENT_T1526544958_H
#ifndef ONCHANGEEVENT_T4257774360_H
#define ONCHANGEEVENT_T4257774360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/OnChangeEvent
struct  OnChangeEvent_t4257774360  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCHANGEEVENT_T4257774360_H
#ifndef TEXTSELECTIONEVENT_T1023421581_H
#define TEXTSELECTIONEVENT_T1023421581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/TextSelectionEvent
struct  TextSelectionEvent_t1023421581  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTSELECTIONEVENT_T1023421581_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-9E6378168821DBABB7EE3D0154346480FAC8AEF1
	U24ArrayTypeU3D40_t2865632059  ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline U24ArrayTypeU3D40_t2865632059  get_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline U24ArrayTypeU3D40_t2865632059 * get_address_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(U24ArrayTypeU3D40_t2865632059  value)
	{
		___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef SUBMITEVENT_T1343580625_H
#define SUBMITEVENT_T1343580625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/SubmitEvent
struct  SubmitEvent_t1343580625  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMITEVENT_T1343580625_H
#ifndef U3CMOUSEDRAGOUTSIDERECTU3EC__ITERATOR1_T2283961069_H
#define U3CMOUSEDRAGOUTSIDERECTU3EC__ITERATOR1_T2283961069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1
struct  U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::eventData
	PointerEventData_t3807901092 * ___eventData_0;
	// UnityEngine.Vector2 TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::<localMousePos>__1
	Vector2_t2156229523  ___U3ClocalMousePosU3E__1_1;
	// UnityEngine.Rect TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::<rect>__1
	Rect_t2360479859  ___U3CrectU3E__1_2;
	// System.Single TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::<delay>__1
	float ___U3CdelayU3E__1_3;
	// TMPro.TMP_InputField TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::$this
	TMP_InputField_t1099764886 * ___U24this_4;
	// System.Object TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069, ___eventData_0)); }
	inline PointerEventData_t3807901092 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t3807901092 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t3807901092 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}

	inline static int32_t get_offset_of_U3ClocalMousePosU3E__1_1() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069, ___U3ClocalMousePosU3E__1_1)); }
	inline Vector2_t2156229523  get_U3ClocalMousePosU3E__1_1() const { return ___U3ClocalMousePosU3E__1_1; }
	inline Vector2_t2156229523 * get_address_of_U3ClocalMousePosU3E__1_1() { return &___U3ClocalMousePosU3E__1_1; }
	inline void set_U3ClocalMousePosU3E__1_1(Vector2_t2156229523  value)
	{
		___U3ClocalMousePosU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CrectU3E__1_2() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069, ___U3CrectU3E__1_2)); }
	inline Rect_t2360479859  get_U3CrectU3E__1_2() const { return ___U3CrectU3E__1_2; }
	inline Rect_t2360479859 * get_address_of_U3CrectU3E__1_2() { return &___U3CrectU3E__1_2; }
	inline void set_U3CrectU3E__1_2(Rect_t2360479859  value)
	{
		___U3CrectU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CdelayU3E__1_3() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069, ___U3CdelayU3E__1_3)); }
	inline float get_U3CdelayU3E__1_3() const { return ___U3CdelayU3E__1_3; }
	inline float* get_address_of_U3CdelayU3E__1_3() { return &___U3CdelayU3E__1_3; }
	inline void set_U3CdelayU3E__1_3(float value)
	{
		___U3CdelayU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069, ___U24this_4)); }
	inline TMP_InputField_t1099764886 * get_U24this_4() const { return ___U24this_4; }
	inline TMP_InputField_t1099764886 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TMP_InputField_t1099764886 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOUSEDRAGOUTSIDERECTU3EC__ITERATOR1_T2283961069_H
#ifndef TMP_TEXTINFO_T3598145122_H
#define TMP_TEXTINFO_T3598145122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextInfo
struct  TMP_TextInfo_t3598145122  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.TMP_TextInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_2;
	// System.Int32 TMPro.TMP_TextInfo::characterCount
	int32_t ___characterCount_3;
	// System.Int32 TMPro.TMP_TextInfo::spriteCount
	int32_t ___spriteCount_4;
	// System.Int32 TMPro.TMP_TextInfo::spaceCount
	int32_t ___spaceCount_5;
	// System.Int32 TMPro.TMP_TextInfo::wordCount
	int32_t ___wordCount_6;
	// System.Int32 TMPro.TMP_TextInfo::linkCount
	int32_t ___linkCount_7;
	// System.Int32 TMPro.TMP_TextInfo::lineCount
	int32_t ___lineCount_8;
	// System.Int32 TMPro.TMP_TextInfo::pageCount
	int32_t ___pageCount_9;
	// System.Int32 TMPro.TMP_TextInfo::materialCount
	int32_t ___materialCount_10;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_TextInfo::characterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___characterInfo_11;
	// TMPro.TMP_WordInfo[] TMPro.TMP_TextInfo::wordInfo
	TMP_WordInfoU5BU5D_t3766301798* ___wordInfo_12;
	// TMPro.TMP_LinkInfo[] TMPro.TMP_TextInfo::linkInfo
	TMP_LinkInfoU5BU5D_t3558768157* ___linkInfo_13;
	// TMPro.TMP_LineInfo[] TMPro.TMP_TextInfo::lineInfo
	TMP_LineInfoU5BU5D_t4120149533* ___lineInfo_14;
	// TMPro.TMP_PageInfo[] TMPro.TMP_TextInfo::pageInfo
	TMP_PageInfoU5BU5D_t2463031060* ___pageInfo_15;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::meshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___meshInfo_16;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::m_CachedMeshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___m_CachedMeshInfo_17;

public:
	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___textComponent_2)); }
	inline TMP_Text_t2599618874 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t2599618874 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_2), value);
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}

	inline static int32_t get_offset_of_spriteCount_4() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spriteCount_4)); }
	inline int32_t get_spriteCount_4() const { return ___spriteCount_4; }
	inline int32_t* get_address_of_spriteCount_4() { return &___spriteCount_4; }
	inline void set_spriteCount_4(int32_t value)
	{
		___spriteCount_4 = value;
	}

	inline static int32_t get_offset_of_spaceCount_5() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spaceCount_5)); }
	inline int32_t get_spaceCount_5() const { return ___spaceCount_5; }
	inline int32_t* get_address_of_spaceCount_5() { return &___spaceCount_5; }
	inline void set_spaceCount_5(int32_t value)
	{
		___spaceCount_5 = value;
	}

	inline static int32_t get_offset_of_wordCount_6() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordCount_6)); }
	inline int32_t get_wordCount_6() const { return ___wordCount_6; }
	inline int32_t* get_address_of_wordCount_6() { return &___wordCount_6; }
	inline void set_wordCount_6(int32_t value)
	{
		___wordCount_6 = value;
	}

	inline static int32_t get_offset_of_linkCount_7() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkCount_7)); }
	inline int32_t get_linkCount_7() const { return ___linkCount_7; }
	inline int32_t* get_address_of_linkCount_7() { return &___linkCount_7; }
	inline void set_linkCount_7(int32_t value)
	{
		___linkCount_7 = value;
	}

	inline static int32_t get_offset_of_lineCount_8() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineCount_8)); }
	inline int32_t get_lineCount_8() const { return ___lineCount_8; }
	inline int32_t* get_address_of_lineCount_8() { return &___lineCount_8; }
	inline void set_lineCount_8(int32_t value)
	{
		___lineCount_8 = value;
	}

	inline static int32_t get_offset_of_pageCount_9() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageCount_9)); }
	inline int32_t get_pageCount_9() const { return ___pageCount_9; }
	inline int32_t* get_address_of_pageCount_9() { return &___pageCount_9; }
	inline void set_pageCount_9(int32_t value)
	{
		___pageCount_9 = value;
	}

	inline static int32_t get_offset_of_materialCount_10() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___materialCount_10)); }
	inline int32_t get_materialCount_10() const { return ___materialCount_10; }
	inline int32_t* get_address_of_materialCount_10() { return &___materialCount_10; }
	inline void set_materialCount_10(int32_t value)
	{
		___materialCount_10 = value;
	}

	inline static int32_t get_offset_of_characterInfo_11() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterInfo_11)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_characterInfo_11() const { return ___characterInfo_11; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_characterInfo_11() { return &___characterInfo_11; }
	inline void set_characterInfo_11(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___characterInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterInfo_11), value);
	}

	inline static int32_t get_offset_of_wordInfo_12() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordInfo_12)); }
	inline TMP_WordInfoU5BU5D_t3766301798* get_wordInfo_12() const { return ___wordInfo_12; }
	inline TMP_WordInfoU5BU5D_t3766301798** get_address_of_wordInfo_12() { return &___wordInfo_12; }
	inline void set_wordInfo_12(TMP_WordInfoU5BU5D_t3766301798* value)
	{
		___wordInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___wordInfo_12), value);
	}

	inline static int32_t get_offset_of_linkInfo_13() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkInfo_13)); }
	inline TMP_LinkInfoU5BU5D_t3558768157* get_linkInfo_13() const { return ___linkInfo_13; }
	inline TMP_LinkInfoU5BU5D_t3558768157** get_address_of_linkInfo_13() { return &___linkInfo_13; }
	inline void set_linkInfo_13(TMP_LinkInfoU5BU5D_t3558768157* value)
	{
		___linkInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___linkInfo_13), value);
	}

	inline static int32_t get_offset_of_lineInfo_14() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineInfo_14)); }
	inline TMP_LineInfoU5BU5D_t4120149533* get_lineInfo_14() const { return ___lineInfo_14; }
	inline TMP_LineInfoU5BU5D_t4120149533** get_address_of_lineInfo_14() { return &___lineInfo_14; }
	inline void set_lineInfo_14(TMP_LineInfoU5BU5D_t4120149533* value)
	{
		___lineInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_14), value);
	}

	inline static int32_t get_offset_of_pageInfo_15() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageInfo_15)); }
	inline TMP_PageInfoU5BU5D_t2463031060* get_pageInfo_15() const { return ___pageInfo_15; }
	inline TMP_PageInfoU5BU5D_t2463031060** get_address_of_pageInfo_15() { return &___pageInfo_15; }
	inline void set_pageInfo_15(TMP_PageInfoU5BU5D_t2463031060* value)
	{
		___pageInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_15), value);
	}

	inline static int32_t get_offset_of_meshInfo_16() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___meshInfo_16)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_meshInfo_16() const { return ___meshInfo_16; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_meshInfo_16() { return &___meshInfo_16; }
	inline void set_meshInfo_16(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___meshInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshInfo_16), value);
	}

	inline static int32_t get_offset_of_m_CachedMeshInfo_17() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___m_CachedMeshInfo_17)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_m_CachedMeshInfo_17() const { return ___m_CachedMeshInfo_17; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_m_CachedMeshInfo_17() { return &___m_CachedMeshInfo_17; }
	inline void set_m_CachedMeshInfo_17(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___m_CachedMeshInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedMeshInfo_17), value);
	}
};

struct TMP_TextInfo_t3598145122_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorPositive
	Vector2_t2156229523  ___k_InfinityVectorPositive_0;
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorNegative
	Vector2_t2156229523  ___k_InfinityVectorNegative_1;

public:
	inline static int32_t get_offset_of_k_InfinityVectorPositive_0() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorPositive_0)); }
	inline Vector2_t2156229523  get_k_InfinityVectorPositive_0() const { return ___k_InfinityVectorPositive_0; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorPositive_0() { return &___k_InfinityVectorPositive_0; }
	inline void set_k_InfinityVectorPositive_0(Vector2_t2156229523  value)
	{
		___k_InfinityVectorPositive_0 = value;
	}

	inline static int32_t get_offset_of_k_InfinityVectorNegative_1() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorNegative_1)); }
	inline Vector2_t2156229523  get_k_InfinityVectorNegative_1() const { return ___k_InfinityVectorNegative_1; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorNegative_1() { return &___k_InfinityVectorNegative_1; }
	inline void set_k_InfinityVectorNegative_1(Vector2_t2156229523  value)
	{
		___k_InfinityVectorNegative_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFO_T3598145122_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TMP_LINEINFO_T1079631636_H
#define TMP_LINEINFO_T1079631636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t1079631636 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3837212874  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineExtents_19)); }
	inline Extents_t3837212874  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_t3837212874 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_t3837212874  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T1079631636_H
#ifndef TMP_CHARACTERINFO_T3185626797_H
#define TMP_CHARACTERINFO_T3185626797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t3185626797 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_t129727469 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_t340375123 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_t3722313464  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_t3722313464  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_t3722313464  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_t3722313464  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t2600501292  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___textElement_3)); }
	inline TMP_TextElement_t129727469 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_t129727469 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_t129727469 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___fontAsset_4)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___material_7)); }
	inline Material_t340375123 * get_material_7() const { return ___material_7; }
	inline Material_t340375123 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t340375123 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pageNumber_12)); }
	inline int32_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int32_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int32_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TL_14)); }
	inline TMP_Vertex_t2404176824  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t2404176824  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BL_15)); }
	inline TMP_Vertex_t2404176824  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t2404176824  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TR_16)); }
	inline TMP_Vertex_t2404176824  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t2404176824  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BR_17)); }
	inline TMP_Vertex_t2404176824  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t2404176824  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topLeft_18)); }
	inline Vector3_t3722313464  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_t3722313464 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_t3722313464  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomLeft_19)); }
	inline Vector3_t3722313464  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_t3722313464 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_t3722313464  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topRight_20)); }
	inline Vector3_t3722313464  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_t3722313464 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_t3722313464  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomRight_21)); }
	inline Vector3_t3722313464  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_t3722313464 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_t3722313464  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___color_29)); }
	inline Color32_t2600501292  get_color_29() const { return ___color_29; }
	inline Color32_t2600501292 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t2600501292  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T3185626797_H
#ifndef TMP_XMLTAGSTACK_1_T3600445780_H
#define TMP_XMLTAGSTACK_1_T3600445780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t3600445780 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t3552942253* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t3552942253* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t3552942253** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t3552942253* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3600445780_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef CARETINFO_T841780893_H
#define CARETINFO_T841780893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretInfo
struct  CaretInfo_t841780893 
{
public:
	// System.Int32 TMPro.CaretInfo::index
	int32_t ___index_0;
	// TMPro.CaretPosition TMPro.CaretInfo::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETINFO_T841780893_H
#ifndef TMP_ASSET_T2469957285_H
#define TMP_ASSET_T2469957285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Asset
struct  TMP_Asset_t2469957285  : public ScriptableObject_t2528358522
{
public:
	// System.Int32 TMPro.TMP_Asset::hashCode
	int32_t ___hashCode_2;
	// UnityEngine.Material TMPro.TMP_Asset::material
	Material_t340375123 * ___material_3;
	// System.Int32 TMPro.TMP_Asset::materialHashCode
	int32_t ___materialHashCode_4;

public:
	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TMP_Asset_t2469957285, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(TMP_Asset_t2469957285, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_materialHashCode_4() { return static_cast<int32_t>(offsetof(TMP_Asset_t2469957285, ___materialHashCode_4)); }
	inline int32_t get_materialHashCode_4() const { return ___materialHashCode_4; }
	inline int32_t* get_address_of_materialHashCode_4() { return &___materialHashCode_4; }
	inline void set_materialHashCode_4(int32_t value)
	{
		___materialHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_ASSET_T2469957285_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TMP_STYLESHEET_T917564226_H
#define TMP_STYLESHEET_T917564226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_StyleSheet
struct  TMP_StyleSheet_t917564226  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleList
	List_1_t656488210 * ___m_StyleList_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleDictionary
	Dictionary_2_t2368094095 * ___m_StyleDictionary_4;

public:
	inline static int32_t get_offset_of_m_StyleList_3() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t917564226, ___m_StyleList_3)); }
	inline List_1_t656488210 * get_m_StyleList_3() const { return ___m_StyleList_3; }
	inline List_1_t656488210 ** get_address_of_m_StyleList_3() { return &___m_StyleList_3; }
	inline void set_m_StyleList_3(List_1_t656488210 * value)
	{
		___m_StyleList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleList_3), value);
	}

	inline static int32_t get_offset_of_m_StyleDictionary_4() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t917564226, ___m_StyleDictionary_4)); }
	inline Dictionary_2_t2368094095 * get_m_StyleDictionary_4() const { return ___m_StyleDictionary_4; }
	inline Dictionary_2_t2368094095 ** get_address_of_m_StyleDictionary_4() { return &___m_StyleDictionary_4; }
	inline void set_m_StyleDictionary_4(Dictionary_2_t2368094095 * value)
	{
		___m_StyleDictionary_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleDictionary_4), value);
	}
};

struct TMP_StyleSheet_t917564226_StaticFields
{
public:
	// TMPro.TMP_StyleSheet TMPro.TMP_StyleSheet::s_Instance
	TMP_StyleSheet_t917564226 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t917564226_StaticFields, ___s_Instance_2)); }
	inline TMP_StyleSheet_t917564226 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline TMP_StyleSheet_t917564226 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(TMP_StyleSheet_t917564226 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLESHEET_T917564226_H
#ifndef ONVALIDATEINPUT_T373909109_H
#define ONVALIDATEINPUT_T373909109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/OnValidateInput
struct  OnValidateInput_t373909109  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVALIDATEINPUT_T373909109_H
#ifndef U3CDOSPRITEANIMATIONINTERNALU3EC__ITERATOR0_T827549206_H
#define U3CDOSPRITEANIMATIONINTERNALU3EC__ITERATOR0_T827549206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0
struct  U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::start
	int32_t ___start_0;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<currentFrame>__0
	int32_t ___U3CcurrentFrameU3E__0_1;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::end
	int32_t ___end_2;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_3;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::currentCharacter
	int32_t ___currentCharacter_4;
	// TMPro.TMP_CharacterInfo TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<charInfo>__0
	TMP_CharacterInfo_t3185626797  ___U3CcharInfoU3E__0_5;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<materialIndex>__0
	int32_t ___U3CmaterialIndexU3E__0_6;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<vertexIndex>__0
	int32_t ___U3CvertexIndexU3E__0_7;
	// TMPro.TMP_MeshInfo TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<meshInfo>__0
	TMP_MeshInfo_t2771747634  ___U3CmeshInfoU3E__0_8;
	// System.Single TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_9;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::framerate
	int32_t ___framerate_10;
	// System.Single TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<targetTime>__0
	float ___U3CtargetTimeU3E__0_11;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::$this
	TMP_SpriteAnimator_t2836635477 * ___U24this_12;
	// System.Object TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::$current
	RuntimeObject * ___U24current_13;
	// System.Boolean TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::$disposing
	bool ___U24disposing_14;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::$PC
	int32_t ___U24PC_15;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentFrameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U3CcurrentFrameU3E__0_1)); }
	inline int32_t get_U3CcurrentFrameU3E__0_1() const { return ___U3CcurrentFrameU3E__0_1; }
	inline int32_t* get_address_of_U3CcurrentFrameU3E__0_1() { return &___U3CcurrentFrameU3E__0_1; }
	inline void set_U3CcurrentFrameU3E__0_1(int32_t value)
	{
		___U3CcurrentFrameU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_end_2() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___end_2)); }
	inline int32_t get_end_2() const { return ___end_2; }
	inline int32_t* get_address_of_end_2() { return &___end_2; }
	inline void set_end_2(int32_t value)
	{
		___end_2 = value;
	}

	inline static int32_t get_offset_of_spriteAsset_3() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___spriteAsset_3)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_3() const { return ___spriteAsset_3; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_3() { return &___spriteAsset_3; }
	inline void set_spriteAsset_3(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_3), value);
	}

	inline static int32_t get_offset_of_currentCharacter_4() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___currentCharacter_4)); }
	inline int32_t get_currentCharacter_4() const { return ___currentCharacter_4; }
	inline int32_t* get_address_of_currentCharacter_4() { return &___currentCharacter_4; }
	inline void set_currentCharacter_4(int32_t value)
	{
		___currentCharacter_4 = value;
	}

	inline static int32_t get_offset_of_U3CcharInfoU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U3CcharInfoU3E__0_5)); }
	inline TMP_CharacterInfo_t3185626797  get_U3CcharInfoU3E__0_5() const { return ___U3CcharInfoU3E__0_5; }
	inline TMP_CharacterInfo_t3185626797 * get_address_of_U3CcharInfoU3E__0_5() { return &___U3CcharInfoU3E__0_5; }
	inline void set_U3CcharInfoU3E__0_5(TMP_CharacterInfo_t3185626797  value)
	{
		___U3CcharInfoU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CmaterialIndexU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U3CmaterialIndexU3E__0_6)); }
	inline int32_t get_U3CmaterialIndexU3E__0_6() const { return ___U3CmaterialIndexU3E__0_6; }
	inline int32_t* get_address_of_U3CmaterialIndexU3E__0_6() { return &___U3CmaterialIndexU3E__0_6; }
	inline void set_U3CmaterialIndexU3E__0_6(int32_t value)
	{
		___U3CmaterialIndexU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CvertexIndexU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U3CvertexIndexU3E__0_7)); }
	inline int32_t get_U3CvertexIndexU3E__0_7() const { return ___U3CvertexIndexU3E__0_7; }
	inline int32_t* get_address_of_U3CvertexIndexU3E__0_7() { return &___U3CvertexIndexU3E__0_7; }
	inline void set_U3CvertexIndexU3E__0_7(int32_t value)
	{
		___U3CvertexIndexU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U3CmeshInfoU3E__0_8() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U3CmeshInfoU3E__0_8)); }
	inline TMP_MeshInfo_t2771747634  get_U3CmeshInfoU3E__0_8() const { return ___U3CmeshInfoU3E__0_8; }
	inline TMP_MeshInfo_t2771747634 * get_address_of_U3CmeshInfoU3E__0_8() { return &___U3CmeshInfoU3E__0_8; }
	inline void set_U3CmeshInfoU3E__0_8(TMP_MeshInfo_t2771747634  value)
	{
		___U3CmeshInfoU3E__0_8 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_9() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U3CelapsedTimeU3E__0_9)); }
	inline float get_U3CelapsedTimeU3E__0_9() const { return ___U3CelapsedTimeU3E__0_9; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_9() { return &___U3CelapsedTimeU3E__0_9; }
	inline void set_U3CelapsedTimeU3E__0_9(float value)
	{
		___U3CelapsedTimeU3E__0_9 = value;
	}

	inline static int32_t get_offset_of_framerate_10() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___framerate_10)); }
	inline int32_t get_framerate_10() const { return ___framerate_10; }
	inline int32_t* get_address_of_framerate_10() { return &___framerate_10; }
	inline void set_framerate_10(int32_t value)
	{
		___framerate_10 = value;
	}

	inline static int32_t get_offset_of_U3CtargetTimeU3E__0_11() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U3CtargetTimeU3E__0_11)); }
	inline float get_U3CtargetTimeU3E__0_11() const { return ___U3CtargetTimeU3E__0_11; }
	inline float* get_address_of_U3CtargetTimeU3E__0_11() { return &___U3CtargetTimeU3E__0_11; }
	inline void set_U3CtargetTimeU3E__0_11(float value)
	{
		___U3CtargetTimeU3E__0_11 = value;
	}

	inline static int32_t get_offset_of_U24this_12() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U24this_12)); }
	inline TMP_SpriteAnimator_t2836635477 * get_U24this_12() const { return ___U24this_12; }
	inline TMP_SpriteAnimator_t2836635477 ** get_address_of_U24this_12() { return &___U24this_12; }
	inline void set_U24this_12(TMP_SpriteAnimator_t2836635477 * value)
	{
		___U24this_12 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_12), value);
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U24current_13)); }
	inline RuntimeObject * get_U24current_13() const { return ___U24current_13; }
	inline RuntimeObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(RuntimeObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_13), value);
	}

	inline static int32_t get_offset_of_U24disposing_14() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U24disposing_14)); }
	inline bool get_U24disposing_14() const { return ___U24disposing_14; }
	inline bool* get_address_of_U24disposing_14() { return &___U24disposing_14; }
	inline void set_U24disposing_14(bool value)
	{
		___U24disposing_14 = value;
	}

	inline static int32_t get_offset_of_U24PC_15() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206, ___U24PC_15)); }
	inline int32_t get_U24PC_15() const { return ___U24PC_15; }
	inline int32_t* get_address_of_U24PC_15() { return &___U24PC_15; }
	inline void set_U24PC_15(int32_t value)
	{
		___U24PC_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSPRITEANIMATIONINTERNALU3EC__ITERATOR0_T827549206_H
#ifndef TMP_SETTINGS_T2071156274_H
#define TMP_SETTINGS_T2071156274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Settings
struct  TMP_Settings_t2071156274  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean TMPro.TMP_Settings::m_enableWordWrapping
	bool ___m_enableWordWrapping_3;
	// System.Boolean TMPro.TMP_Settings::m_enableKerning
	bool ___m_enableKerning_4;
	// System.Boolean TMPro.TMP_Settings::m_enableExtraPadding
	bool ___m_enableExtraPadding_5;
	// System.Boolean TMPro.TMP_Settings::m_enableTintAllSprites
	bool ___m_enableTintAllSprites_6;
	// System.Boolean TMPro.TMP_Settings::m_enableParseEscapeCharacters
	bool ___m_enableParseEscapeCharacters_7;
	// System.Int32 TMPro.TMP_Settings::m_missingGlyphCharacter
	int32_t ___m_missingGlyphCharacter_8;
	// System.Boolean TMPro.TMP_Settings::m_warningsDisabled
	bool ___m_warningsDisabled_9;
	// TMPro.TMP_FontAsset TMPro.TMP_Settings::m_defaultFontAsset
	TMP_FontAsset_t364381626 * ___m_defaultFontAsset_10;
	// System.String TMPro.TMP_Settings::m_defaultFontAssetPath
	String_t* ___m_defaultFontAssetPath_11;
	// System.Single TMPro.TMP_Settings::m_defaultFontSize
	float ___m_defaultFontSize_12;
	// System.Single TMPro.TMP_Settings::m_defaultAutoSizeMinRatio
	float ___m_defaultAutoSizeMinRatio_13;
	// System.Single TMPro.TMP_Settings::m_defaultAutoSizeMaxRatio
	float ___m_defaultAutoSizeMaxRatio_14;
	// UnityEngine.Vector2 TMPro.TMP_Settings::m_defaultTextMeshProTextContainerSize
	Vector2_t2156229523  ___m_defaultTextMeshProTextContainerSize_15;
	// UnityEngine.Vector2 TMPro.TMP_Settings::m_defaultTextMeshProUITextContainerSize
	Vector2_t2156229523  ___m_defaultTextMeshProUITextContainerSize_16;
	// System.Boolean TMPro.TMP_Settings::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_17;
	// System.Collections.Generic.List`1<TMPro.TMP_FontAsset> TMPro.TMP_Settings::m_fallbackFontAssets
	List_1_t1836456368 * ___m_fallbackFontAssets_18;
	// System.Boolean TMPro.TMP_Settings::m_matchMaterialPreset
	bool ___m_matchMaterialPreset_19;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Settings::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_20;
	// System.String TMPro.TMP_Settings::m_defaultSpriteAssetPath
	String_t* ___m_defaultSpriteAssetPath_21;
	// System.String TMPro.TMP_Settings::m_defaultColorGradientPresetsPath
	String_t* ___m_defaultColorGradientPresetsPath_22;
	// System.Boolean TMPro.TMP_Settings::m_enableEmojiSupport
	bool ___m_enableEmojiSupport_23;
	// TMPro.TMP_StyleSheet TMPro.TMP_Settings::m_defaultStyleSheet
	TMP_StyleSheet_t917564226 * ___m_defaultStyleSheet_24;
	// UnityEngine.TextAsset TMPro.TMP_Settings::m_leadingCharacters
	TextAsset_t3022178571 * ___m_leadingCharacters_25;
	// UnityEngine.TextAsset TMPro.TMP_Settings::m_followingCharacters
	TextAsset_t3022178571 * ___m_followingCharacters_26;
	// TMPro.TMP_Settings/LineBreakingTable TMPro.TMP_Settings::m_linebreakingRules
	LineBreakingTable_t1457697482 * ___m_linebreakingRules_27;

public:
	inline static int32_t get_offset_of_m_enableWordWrapping_3() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableWordWrapping_3)); }
	inline bool get_m_enableWordWrapping_3() const { return ___m_enableWordWrapping_3; }
	inline bool* get_address_of_m_enableWordWrapping_3() { return &___m_enableWordWrapping_3; }
	inline void set_m_enableWordWrapping_3(bool value)
	{
		___m_enableWordWrapping_3 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_4() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableKerning_4)); }
	inline bool get_m_enableKerning_4() const { return ___m_enableKerning_4; }
	inline bool* get_address_of_m_enableKerning_4() { return &___m_enableKerning_4; }
	inline void set_m_enableKerning_4(bool value)
	{
		___m_enableKerning_4 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_5() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableExtraPadding_5)); }
	inline bool get_m_enableExtraPadding_5() const { return ___m_enableExtraPadding_5; }
	inline bool* get_address_of_m_enableExtraPadding_5() { return &___m_enableExtraPadding_5; }
	inline void set_m_enableExtraPadding_5(bool value)
	{
		___m_enableExtraPadding_5 = value;
	}

	inline static int32_t get_offset_of_m_enableTintAllSprites_6() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableTintAllSprites_6)); }
	inline bool get_m_enableTintAllSprites_6() const { return ___m_enableTintAllSprites_6; }
	inline bool* get_address_of_m_enableTintAllSprites_6() { return &___m_enableTintAllSprites_6; }
	inline void set_m_enableTintAllSprites_6(bool value)
	{
		___m_enableTintAllSprites_6 = value;
	}

	inline static int32_t get_offset_of_m_enableParseEscapeCharacters_7() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableParseEscapeCharacters_7)); }
	inline bool get_m_enableParseEscapeCharacters_7() const { return ___m_enableParseEscapeCharacters_7; }
	inline bool* get_address_of_m_enableParseEscapeCharacters_7() { return &___m_enableParseEscapeCharacters_7; }
	inline void set_m_enableParseEscapeCharacters_7(bool value)
	{
		___m_enableParseEscapeCharacters_7 = value;
	}

	inline static int32_t get_offset_of_m_missingGlyphCharacter_8() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_missingGlyphCharacter_8)); }
	inline int32_t get_m_missingGlyphCharacter_8() const { return ___m_missingGlyphCharacter_8; }
	inline int32_t* get_address_of_m_missingGlyphCharacter_8() { return &___m_missingGlyphCharacter_8; }
	inline void set_m_missingGlyphCharacter_8(int32_t value)
	{
		___m_missingGlyphCharacter_8 = value;
	}

	inline static int32_t get_offset_of_m_warningsDisabled_9() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_warningsDisabled_9)); }
	inline bool get_m_warningsDisabled_9() const { return ___m_warningsDisabled_9; }
	inline bool* get_address_of_m_warningsDisabled_9() { return &___m_warningsDisabled_9; }
	inline void set_m_warningsDisabled_9(bool value)
	{
		___m_warningsDisabled_9 = value;
	}

	inline static int32_t get_offset_of_m_defaultFontAsset_10() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultFontAsset_10)); }
	inline TMP_FontAsset_t364381626 * get_m_defaultFontAsset_10() const { return ___m_defaultFontAsset_10; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_defaultFontAsset_10() { return &___m_defaultFontAsset_10; }
	inline void set_m_defaultFontAsset_10(TMP_FontAsset_t364381626 * value)
	{
		___m_defaultFontAsset_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultFontAsset_10), value);
	}

	inline static int32_t get_offset_of_m_defaultFontAssetPath_11() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultFontAssetPath_11)); }
	inline String_t* get_m_defaultFontAssetPath_11() const { return ___m_defaultFontAssetPath_11; }
	inline String_t** get_address_of_m_defaultFontAssetPath_11() { return &___m_defaultFontAssetPath_11; }
	inline void set_m_defaultFontAssetPath_11(String_t* value)
	{
		___m_defaultFontAssetPath_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultFontAssetPath_11), value);
	}

	inline static int32_t get_offset_of_m_defaultFontSize_12() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultFontSize_12)); }
	inline float get_m_defaultFontSize_12() const { return ___m_defaultFontSize_12; }
	inline float* get_address_of_m_defaultFontSize_12() { return &___m_defaultFontSize_12; }
	inline void set_m_defaultFontSize_12(float value)
	{
		___m_defaultFontSize_12 = value;
	}

	inline static int32_t get_offset_of_m_defaultAutoSizeMinRatio_13() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultAutoSizeMinRatio_13)); }
	inline float get_m_defaultAutoSizeMinRatio_13() const { return ___m_defaultAutoSizeMinRatio_13; }
	inline float* get_address_of_m_defaultAutoSizeMinRatio_13() { return &___m_defaultAutoSizeMinRatio_13; }
	inline void set_m_defaultAutoSizeMinRatio_13(float value)
	{
		___m_defaultAutoSizeMinRatio_13 = value;
	}

	inline static int32_t get_offset_of_m_defaultAutoSizeMaxRatio_14() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultAutoSizeMaxRatio_14)); }
	inline float get_m_defaultAutoSizeMaxRatio_14() const { return ___m_defaultAutoSizeMaxRatio_14; }
	inline float* get_address_of_m_defaultAutoSizeMaxRatio_14() { return &___m_defaultAutoSizeMaxRatio_14; }
	inline void set_m_defaultAutoSizeMaxRatio_14(float value)
	{
		___m_defaultAutoSizeMaxRatio_14 = value;
	}

	inline static int32_t get_offset_of_m_defaultTextMeshProTextContainerSize_15() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultTextMeshProTextContainerSize_15)); }
	inline Vector2_t2156229523  get_m_defaultTextMeshProTextContainerSize_15() const { return ___m_defaultTextMeshProTextContainerSize_15; }
	inline Vector2_t2156229523 * get_address_of_m_defaultTextMeshProTextContainerSize_15() { return &___m_defaultTextMeshProTextContainerSize_15; }
	inline void set_m_defaultTextMeshProTextContainerSize_15(Vector2_t2156229523  value)
	{
		___m_defaultTextMeshProTextContainerSize_15 = value;
	}

	inline static int32_t get_offset_of_m_defaultTextMeshProUITextContainerSize_16() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultTextMeshProUITextContainerSize_16)); }
	inline Vector2_t2156229523  get_m_defaultTextMeshProUITextContainerSize_16() const { return ___m_defaultTextMeshProUITextContainerSize_16; }
	inline Vector2_t2156229523 * get_address_of_m_defaultTextMeshProUITextContainerSize_16() { return &___m_defaultTextMeshProUITextContainerSize_16; }
	inline void set_m_defaultTextMeshProUITextContainerSize_16(Vector2_t2156229523  value)
	{
		___m_defaultTextMeshProUITextContainerSize_16 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_17() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_autoSizeTextContainer_17)); }
	inline bool get_m_autoSizeTextContainer_17() const { return ___m_autoSizeTextContainer_17; }
	inline bool* get_address_of_m_autoSizeTextContainer_17() { return &___m_autoSizeTextContainer_17; }
	inline void set_m_autoSizeTextContainer_17(bool value)
	{
		___m_autoSizeTextContainer_17 = value;
	}

	inline static int32_t get_offset_of_m_fallbackFontAssets_18() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_fallbackFontAssets_18)); }
	inline List_1_t1836456368 * get_m_fallbackFontAssets_18() const { return ___m_fallbackFontAssets_18; }
	inline List_1_t1836456368 ** get_address_of_m_fallbackFontAssets_18() { return &___m_fallbackFontAssets_18; }
	inline void set_m_fallbackFontAssets_18(List_1_t1836456368 * value)
	{
		___m_fallbackFontAssets_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackFontAssets_18), value);
	}

	inline static int32_t get_offset_of_m_matchMaterialPreset_19() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_matchMaterialPreset_19)); }
	inline bool get_m_matchMaterialPreset_19() const { return ___m_matchMaterialPreset_19; }
	inline bool* get_address_of_m_matchMaterialPreset_19() { return &___m_matchMaterialPreset_19; }
	inline void set_m_matchMaterialPreset_19(bool value)
	{
		___m_matchMaterialPreset_19 = value;
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_20() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultSpriteAsset_20)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_20() const { return ___m_defaultSpriteAsset_20; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_20() { return &___m_defaultSpriteAsset_20; }
	inline void set_m_defaultSpriteAsset_20(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_20), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAssetPath_21() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultSpriteAssetPath_21)); }
	inline String_t* get_m_defaultSpriteAssetPath_21() const { return ___m_defaultSpriteAssetPath_21; }
	inline String_t** get_address_of_m_defaultSpriteAssetPath_21() { return &___m_defaultSpriteAssetPath_21; }
	inline void set_m_defaultSpriteAssetPath_21(String_t* value)
	{
		___m_defaultSpriteAssetPath_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAssetPath_21), value);
	}

	inline static int32_t get_offset_of_m_defaultColorGradientPresetsPath_22() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultColorGradientPresetsPath_22)); }
	inline String_t* get_m_defaultColorGradientPresetsPath_22() const { return ___m_defaultColorGradientPresetsPath_22; }
	inline String_t** get_address_of_m_defaultColorGradientPresetsPath_22() { return &___m_defaultColorGradientPresetsPath_22; }
	inline void set_m_defaultColorGradientPresetsPath_22(String_t* value)
	{
		___m_defaultColorGradientPresetsPath_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultColorGradientPresetsPath_22), value);
	}

	inline static int32_t get_offset_of_m_enableEmojiSupport_23() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_enableEmojiSupport_23)); }
	inline bool get_m_enableEmojiSupport_23() const { return ___m_enableEmojiSupport_23; }
	inline bool* get_address_of_m_enableEmojiSupport_23() { return &___m_enableEmojiSupport_23; }
	inline void set_m_enableEmojiSupport_23(bool value)
	{
		___m_enableEmojiSupport_23 = value;
	}

	inline static int32_t get_offset_of_m_defaultStyleSheet_24() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_defaultStyleSheet_24)); }
	inline TMP_StyleSheet_t917564226 * get_m_defaultStyleSheet_24() const { return ___m_defaultStyleSheet_24; }
	inline TMP_StyleSheet_t917564226 ** get_address_of_m_defaultStyleSheet_24() { return &___m_defaultStyleSheet_24; }
	inline void set_m_defaultStyleSheet_24(TMP_StyleSheet_t917564226 * value)
	{
		___m_defaultStyleSheet_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultStyleSheet_24), value);
	}

	inline static int32_t get_offset_of_m_leadingCharacters_25() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_leadingCharacters_25)); }
	inline TextAsset_t3022178571 * get_m_leadingCharacters_25() const { return ___m_leadingCharacters_25; }
	inline TextAsset_t3022178571 ** get_address_of_m_leadingCharacters_25() { return &___m_leadingCharacters_25; }
	inline void set_m_leadingCharacters_25(TextAsset_t3022178571 * value)
	{
		___m_leadingCharacters_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_leadingCharacters_25), value);
	}

	inline static int32_t get_offset_of_m_followingCharacters_26() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_followingCharacters_26)); }
	inline TextAsset_t3022178571 * get_m_followingCharacters_26() const { return ___m_followingCharacters_26; }
	inline TextAsset_t3022178571 ** get_address_of_m_followingCharacters_26() { return &___m_followingCharacters_26; }
	inline void set_m_followingCharacters_26(TextAsset_t3022178571 * value)
	{
		___m_followingCharacters_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_followingCharacters_26), value);
	}

	inline static int32_t get_offset_of_m_linebreakingRules_27() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274, ___m_linebreakingRules_27)); }
	inline LineBreakingTable_t1457697482 * get_m_linebreakingRules_27() const { return ___m_linebreakingRules_27; }
	inline LineBreakingTable_t1457697482 ** get_address_of_m_linebreakingRules_27() { return &___m_linebreakingRules_27; }
	inline void set_m_linebreakingRules_27(LineBreakingTable_t1457697482 * value)
	{
		___m_linebreakingRules_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_linebreakingRules_27), value);
	}
};

struct TMP_Settings_t2071156274_StaticFields
{
public:
	// TMPro.TMP_Settings TMPro.TMP_Settings::s_Instance
	TMP_Settings_t2071156274 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(TMP_Settings_t2071156274_StaticFields, ___s_Instance_2)); }
	inline TMP_Settings_t2071156274 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline TMP_Settings_t2071156274 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(TMP_Settings_t2071156274 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SETTINGS_T2071156274_H
#ifndef TMP_INPUTVALIDATOR_T1385053824_H
#define TMP_INPUTVALIDATOR_T1385053824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t1385053824  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T1385053824_H
#ifndef WORDWRAPSTATE_T341939652_H
#define WORDWRAPSTATE_T341939652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t341939652 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t2600501292  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t340375123 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3837212874  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___textInfo_27)); }
	inline TMP_TextInfo_t3598145122 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t3598145122 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineInfo_28)); }
	inline TMP_LineInfo_t1079631636  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t1079631636 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t1079631636  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___vertexColor_29)); }
	inline Color32_t2600501292  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t2600501292 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t2600501292  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t2164155836  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t2164155836  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t2164155836  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t2164155836  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t2164155836  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t2164155836  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t2164155836  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t2164155836  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t3241710312  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t3241710312  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t960921318  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t960921318  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t960921318  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t960921318  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2514600297  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2514600297  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2514600297  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2514600297  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t960921318  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t960921318  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2514600297  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2514600297  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_t1515999176  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_t1515999176  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t3600445780  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t3600445780  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t364381626 * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t364381626 ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t364381626 * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_t484820633 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_t484820633 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterial_50)); }
	inline Material_t340375123 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_t340375123 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_t340375123 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___meshExtents_52)); }
	inline Extents_t3837212874  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_t3837212874 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_t3837212874  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T341939652_H
#ifndef ACTIVEPAGECHANGEDDELEGATE_T1951257476_H
#define ACTIVEPAGECHANGEDDELEGATE_T1951257476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PagedScrollRect/ActivePageChangedDelegate
struct  ActivePageChangedDelegate_t1951257476  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEPAGECHANGEDDELEGATE_T1951257476_H
#ifndef TMP_SPRITEASSET_T484820633_H
#define TMP_SPRITEASSET_T484820633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAsset
struct  TMP_SpriteAsset_t484820633  : public TMP_Asset_t2469957285
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_UnicodeLookup
	Dictionary_2_t1839659084 * ___m_UnicodeLookup_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_NameLookup
	Dictionary_2_t1839659084 * ___m_NameLookup_6;
	// UnityEngine.Texture TMPro.TMP_SpriteAsset::spriteSheet
	Texture_t3661962703 * ___spriteSheet_8;
	// System.Collections.Generic.List`1<TMPro.TMP_Sprite> TMPro.TMP_SpriteAsset::spriteInfoList
	List_1_t2026141888 * ___spriteInfoList_9;
	// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset> TMPro.TMP_SpriteAsset::fallbackSpriteAssets
	List_1_t1956895375 * ___fallbackSpriteAssets_10;

public:
	inline static int32_t get_offset_of_m_UnicodeLookup_5() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___m_UnicodeLookup_5)); }
	inline Dictionary_2_t1839659084 * get_m_UnicodeLookup_5() const { return ___m_UnicodeLookup_5; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_UnicodeLookup_5() { return &___m_UnicodeLookup_5; }
	inline void set_m_UnicodeLookup_5(Dictionary_2_t1839659084 * value)
	{
		___m_UnicodeLookup_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnicodeLookup_5), value);
	}

	inline static int32_t get_offset_of_m_NameLookup_6() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___m_NameLookup_6)); }
	inline Dictionary_2_t1839659084 * get_m_NameLookup_6() const { return ___m_NameLookup_6; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_NameLookup_6() { return &___m_NameLookup_6; }
	inline void set_m_NameLookup_6(Dictionary_2_t1839659084 * value)
	{
		___m_NameLookup_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_NameLookup_6), value);
	}

	inline static int32_t get_offset_of_spriteSheet_8() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___spriteSheet_8)); }
	inline Texture_t3661962703 * get_spriteSheet_8() const { return ___spriteSheet_8; }
	inline Texture_t3661962703 ** get_address_of_spriteSheet_8() { return &___spriteSheet_8; }
	inline void set_spriteSheet_8(Texture_t3661962703 * value)
	{
		___spriteSheet_8 = value;
		Il2CppCodeGenWriteBarrier((&___spriteSheet_8), value);
	}

	inline static int32_t get_offset_of_spriteInfoList_9() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___spriteInfoList_9)); }
	inline List_1_t2026141888 * get_spriteInfoList_9() const { return ___spriteInfoList_9; }
	inline List_1_t2026141888 ** get_address_of_spriteInfoList_9() { return &___spriteInfoList_9; }
	inline void set_spriteInfoList_9(List_1_t2026141888 * value)
	{
		___spriteInfoList_9 = value;
		Il2CppCodeGenWriteBarrier((&___spriteInfoList_9), value);
	}

	inline static int32_t get_offset_of_fallbackSpriteAssets_10() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___fallbackSpriteAssets_10)); }
	inline List_1_t1956895375 * get_fallbackSpriteAssets_10() const { return ___fallbackSpriteAssets_10; }
	inline List_1_t1956895375 ** get_address_of_fallbackSpriteAssets_10() { return &___fallbackSpriteAssets_10; }
	inline void set_fallbackSpriteAssets_10(List_1_t1956895375 * value)
	{
		___fallbackSpriteAssets_10 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackSpriteAssets_10), value);
	}
};

struct TMP_SpriteAsset_t484820633_StaticFields
{
public:
	// TMPro.TMP_SpriteAsset TMPro.TMP_SpriteAsset::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_7;
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_SpriteAsset::k_searchedSpriteAssets
	List_1_t128053199 * ___k_searchedSpriteAssets_11;

public:
	inline static int32_t get_offset_of_m_defaultSpriteAsset_7() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633_StaticFields, ___m_defaultSpriteAsset_7)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_7() const { return ___m_defaultSpriteAsset_7; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_7() { return &___m_defaultSpriteAsset_7; }
	inline void set_m_defaultSpriteAsset_7(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_7), value);
	}

	inline static int32_t get_offset_of_k_searchedSpriteAssets_11() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633_StaticFields, ___k_searchedSpriteAssets_11)); }
	inline List_1_t128053199 * get_k_searchedSpriteAssets_11() const { return ___k_searchedSpriteAssets_11; }
	inline List_1_t128053199 ** get_address_of_k_searchedSpriteAssets_11() { return &___k_searchedSpriteAssets_11; }
	inline void set_k_searchedSpriteAssets_11(List_1_t128053199 * value)
	{
		___k_searchedSpriteAssets_11 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedSpriteAssets_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEASSET_T484820633_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TMP_SPRITEANIMATOR_T2836635477_H
#define TMP_SPRITEANIMATOR_T2836635477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAnimator
struct  TMP_SpriteAnimator_t2836635477  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> TMPro.TMP_SpriteAnimator::m_animations
	Dictionary_2_t3280968592 * ___m_animations_2;
	// TMPro.TMP_Text TMPro.TMP_SpriteAnimator::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_3;

public:
	inline static int32_t get_offset_of_m_animations_2() { return static_cast<int32_t>(offsetof(TMP_SpriteAnimator_t2836635477, ___m_animations_2)); }
	inline Dictionary_2_t3280968592 * get_m_animations_2() const { return ___m_animations_2; }
	inline Dictionary_2_t3280968592 ** get_address_of_m_animations_2() { return &___m_animations_2; }
	inline void set_m_animations_2(Dictionary_2_t3280968592 * value)
	{
		___m_animations_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_animations_2), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_3() { return static_cast<int32_t>(offsetof(TMP_SpriteAnimator_t2836635477, ___m_TextComponent_3)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_3() const { return ___m_TextComponent_3; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_3() { return &___m_TextComponent_3; }
	inline void set_m_TextComponent_3(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEANIMATOR_T2836635477_H
#ifndef GVRKEYBOARDDELEGATEBASE_T30895224_H
#define GVRKEYBOARDDELEGATEBASE_T30895224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardDelegateBase
struct  GvrKeyboardDelegateBase_t30895224  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDDELEGATEBASE_T30895224_H
#ifndef TMP_SCROLLBAREVENTHANDLER_T374199898_H
#define TMP_SCROLLBAREVENTHANDLER_T374199898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_ScrollbarEventHandler
struct  TMP_ScrollbarEventHandler_t374199898  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TMPro.TMP_ScrollbarEventHandler::isSelected
	bool ___isSelected_2;

public:
	inline static int32_t get_offset_of_isSelected_2() { return static_cast<int32_t>(offsetof(TMP_ScrollbarEventHandler_t374199898, ___isSelected_2)); }
	inline bool get_isSelected_2() const { return ___isSelected_2; }
	inline bool* get_address_of_isSelected_2() { return &___isSelected_2; }
	inline void set_isSelected_2(bool value)
	{
		___isSelected_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SCROLLBAREVENTHANDLER_T374199898_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef HEADSETDEMOMANAGER_T1859624338_H
#define HEADSETDEMOMANAGER_T1859624338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeadsetDemoManager
struct  HeadsetDemoManager_t1859624338  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HeadsetDemoManager::enableDebugLog
	bool ___enableDebugLog_2;
	// UnityEngine.WaitForSeconds HeadsetDemoManager::waitFourSeconds
	WaitForSeconds_t1699091251 * ___waitFourSeconds_3;

public:
	inline static int32_t get_offset_of_enableDebugLog_2() { return static_cast<int32_t>(offsetof(HeadsetDemoManager_t1859624338, ___enableDebugLog_2)); }
	inline bool get_enableDebugLog_2() const { return ___enableDebugLog_2; }
	inline bool* get_address_of_enableDebugLog_2() { return &___enableDebugLog_2; }
	inline void set_enableDebugLog_2(bool value)
	{
		___enableDebugLog_2 = value;
	}

	inline static int32_t get_offset_of_waitFourSeconds_3() { return static_cast<int32_t>(offsetof(HeadsetDemoManager_t1859624338, ___waitFourSeconds_3)); }
	inline WaitForSeconds_t1699091251 * get_waitFourSeconds_3() const { return ___waitFourSeconds_3; }
	inline WaitForSeconds_t1699091251 ** get_address_of_waitFourSeconds_3() { return &___waitFourSeconds_3; }
	inline void set_waitFourSeconds_3(WaitForSeconds_t1699091251 * value)
	{
		___waitFourSeconds_3 = value;
		Il2CppCodeGenWriteBarrier((&___waitFourSeconds_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETDEMOMANAGER_T1859624338_H
#ifndef BASESCROLLEFFECT_T1789251746_H
#define BASESCROLLEFFECT_T1789251746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseScrollEffect
struct  BaseScrollEffect_t1789251746  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESCROLLEFFECT_T1789251746_H
#ifndef PREFABPAGEPROVIDER_T1618903157_H
#define PREFABPAGEPROVIDER_T1618903157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrefabPageProvider
struct  PrefabPageProvider_t1618903157  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] PrefabPageProvider::prefabs
	GameObjectU5BU5D_t3328599146* ___prefabs_2;
	// System.Single PrefabPageProvider::spacing
	float ___spacing_3;

public:
	inline static int32_t get_offset_of_prefabs_2() { return static_cast<int32_t>(offsetof(PrefabPageProvider_t1618903157, ___prefabs_2)); }
	inline GameObjectU5BU5D_t3328599146* get_prefabs_2() const { return ___prefabs_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_prefabs_2() { return &___prefabs_2; }
	inline void set_prefabs_2(GameObjectU5BU5D_t3328599146* value)
	{
		___prefabs_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefabs_2), value);
	}

	inline static int32_t get_offset_of_spacing_3() { return static_cast<int32_t>(offsetof(PrefabPageProvider_t1618903157, ___spacing_3)); }
	inline float get_spacing_3() const { return ___spacing_3; }
	inline float* get_address_of_spacing_3() { return &___spacing_3; }
	inline void set_spacing_3(float value)
	{
		___spacing_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABPAGEPROVIDER_T1618903157_H
#ifndef POOLEDPAGEPROVIDER_T4250155112_H
#define POOLEDPAGEPROVIDER_T4250155112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PooledPageProvider
struct  PooledPageProvider_t4250155112  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PooledPageProvider::pagePrefab
	GameObject_t1113636619 * ___pagePrefab_2;
	// System.Single PooledPageProvider::spacing
	float ___spacing_3;
	// System.Int32 PooledPageProvider::NumPages
	int32_t ___NumPages_4;
	// System.String PooledPageProvider::prefabName
	String_t* ___prefabName_5;

public:
	inline static int32_t get_offset_of_pagePrefab_2() { return static_cast<int32_t>(offsetof(PooledPageProvider_t4250155112, ___pagePrefab_2)); }
	inline GameObject_t1113636619 * get_pagePrefab_2() const { return ___pagePrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_pagePrefab_2() { return &___pagePrefab_2; }
	inline void set_pagePrefab_2(GameObject_t1113636619 * value)
	{
		___pagePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___pagePrefab_2), value);
	}

	inline static int32_t get_offset_of_spacing_3() { return static_cast<int32_t>(offsetof(PooledPageProvider_t4250155112, ___spacing_3)); }
	inline float get_spacing_3() const { return ___spacing_3; }
	inline float* get_address_of_spacing_3() { return &___spacing_3; }
	inline void set_spacing_3(float value)
	{
		___spacing_3 = value;
	}

	inline static int32_t get_offset_of_NumPages_4() { return static_cast<int32_t>(offsetof(PooledPageProvider_t4250155112, ___NumPages_4)); }
	inline int32_t get_NumPages_4() const { return ___NumPages_4; }
	inline int32_t* get_address_of_NumPages_4() { return &___NumPages_4; }
	inline void set_NumPages_4(int32_t value)
	{
		___NumPages_4 = value;
	}

	inline static int32_t get_offset_of_prefabName_5() { return static_cast<int32_t>(offsetof(PooledPageProvider_t4250155112, ___prefabName_5)); }
	inline String_t* get_prefabName_5() const { return ___prefabName_5; }
	inline String_t** get_address_of_prefabName_5() { return &___prefabName_5; }
	inline void set_prefabName_5(String_t* value)
	{
		___prefabName_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefabName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEDPAGEPROVIDER_T4250155112_H
#ifndef CHILDRENPAGEPROVIDER_T2451340698_H
#define CHILDRENPAGEPROVIDER_T2451340698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChildrenPageProvider
struct  ChildrenPageProvider_t2451340698  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> ChildrenPageProvider::pages
	List_1_t777473367 * ___pages_2;
	// System.Single ChildrenPageProvider::spacing
	float ___spacing_3;

public:
	inline static int32_t get_offset_of_pages_2() { return static_cast<int32_t>(offsetof(ChildrenPageProvider_t2451340698, ___pages_2)); }
	inline List_1_t777473367 * get_pages_2() const { return ___pages_2; }
	inline List_1_t777473367 ** get_address_of_pages_2() { return &___pages_2; }
	inline void set_pages_2(List_1_t777473367 * value)
	{
		___pages_2 = value;
		Il2CppCodeGenWriteBarrier((&___pages_2), value);
	}

	inline static int32_t get_offset_of_spacing_3() { return static_cast<int32_t>(offsetof(ChildrenPageProvider_t2451340698, ___spacing_3)); }
	inline float get_spacing_3() const { return ___spacing_3; }
	inline float* get_address_of_spacing_3() { return &___spacing_3; }
	inline void set_spacing_3(float value)
	{
		___spacing_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHILDRENPAGEPROVIDER_T2451340698_H
#ifndef PAGEDSCROLLRECT_T3286523126_H
#define PAGEDSCROLLRECT_T3286523126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PagedScrollRect
struct  PagedScrollRect_t3286523126  : public MonoBehaviour_t3962482529
{
public:
	// System.Single PagedScrollRect::ScrollSensitivity
	float ___ScrollSensitivity_2;
	// System.Single PagedScrollRect::SnapSpeed
	float ___SnapSpeed_3;
	// System.Int32 PagedScrollRect::StartPage
	int32_t ___StartPage_4;
	// System.Boolean PagedScrollRect::loop
	bool ___loop_5;
	// System.Boolean PagedScrollRect::onlyScrollWhenPointing
	bool ___onlyScrollWhenPointing_6;
	// System.Int32 PagedScrollRect::numExtraPagesShown
	int32_t ___numExtraPagesShown_7;
	// System.Boolean PagedScrollRect::showNextPagesAtRest
	bool ___showNextPagesAtRest_8;
	// System.Boolean PagedScrollRect::shouldTilesAlwaysBeInteractable
	bool ___shouldTilesAlwaysBeInteractable_9;
	// System.Boolean PagedScrollRect::scrollingEnabled
	bool ___scrollingEnabled_10;
	// PagedScrollRect/ActivePageChangedDelegate PagedScrollRect::OnActivePageChanged
	ActivePageChangedDelegate_t1951257476 * ___OnActivePageChanged_11;
	// UnityEngine.Events.UnityEvent PagedScrollRect::OnSwipeLeft
	UnityEvent_t2581268647 * ___OnSwipeLeft_12;
	// UnityEngine.Events.UnityEvent PagedScrollRect::OnSwipeRight
	UnityEvent_t2581268647 * ___OnSwipeRight_13;
	// UnityEngine.Events.UnityEvent PagedScrollRect::OnSnapClosest
	UnityEvent_t2581268647 * ___OnSnapClosest_14;
	// IPageProvider PagedScrollRect::pageProvider
	RuntimeObject* ___pageProvider_15;
	// BaseScrollEffect[] PagedScrollRect::scrollEffects
	BaseScrollEffectU5BU5D_t2685664919* ___scrollEffects_16;
	// System.Boolean PagedScrollRect::isTrackingTouches
	bool ___isTrackingTouches_17;
	// UnityEngine.Vector2 PagedScrollRect::initialTouchPos
	Vector2_t2156229523  ___initialTouchPos_18;
	// UnityEngine.Vector2 PagedScrollRect::previousTouchPos
	Vector2_t2156229523  ___previousTouchPos_19;
	// System.Single PagedScrollRect::previousTouchTimestamp
	float ___previousTouchTimestamp_20;
	// UnityEngine.Vector2 PagedScrollRect::overallVelocity
	Vector2_t2156229523  ___overallVelocity_21;
	// System.Boolean PagedScrollRect::isScrolling
	bool ___isScrolling_22;
	// System.Boolean PagedScrollRect::isPointerHovering
	bool ___isPointerHovering_23;
	// System.Single PagedScrollRect::scrollOffset
	float ___scrollOffset_24;
	// System.Single PagedScrollRect::targetScrollOffset
	float ___targetScrollOffset_25;
	// System.Boolean PagedScrollRect::isScrollOffsetOverridden
	bool ___isScrollOffsetOverridden_26;
	// UnityEngine.RectTransform PagedScrollRect::activePage
	RectTransform_t3704657025 * ___activePage_27;
	// UnityEngine.Coroutine PagedScrollRect::activeSnapCoroutine
	Coroutine_t3829159415 * ___activeSnapCoroutine_28;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.RectTransform> PagedScrollRect::indexToVisiblePage
	Dictionary_2_t2593370356 * ___indexToVisiblePage_29;
	// System.Collections.Generic.Dictionary`2<UnityEngine.RectTransform,System.Int32> PagedScrollRect::visiblePageToIndex
	Dictionary_2_t1561542660 * ___visiblePageToIndex_30;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> PagedScrollRect::visiblePages
	List_1_t881764471 * ___visiblePages_31;

public:
	inline static int32_t get_offset_of_ScrollSensitivity_2() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___ScrollSensitivity_2)); }
	inline float get_ScrollSensitivity_2() const { return ___ScrollSensitivity_2; }
	inline float* get_address_of_ScrollSensitivity_2() { return &___ScrollSensitivity_2; }
	inline void set_ScrollSensitivity_2(float value)
	{
		___ScrollSensitivity_2 = value;
	}

	inline static int32_t get_offset_of_SnapSpeed_3() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___SnapSpeed_3)); }
	inline float get_SnapSpeed_3() const { return ___SnapSpeed_3; }
	inline float* get_address_of_SnapSpeed_3() { return &___SnapSpeed_3; }
	inline void set_SnapSpeed_3(float value)
	{
		___SnapSpeed_3 = value;
	}

	inline static int32_t get_offset_of_StartPage_4() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___StartPage_4)); }
	inline int32_t get_StartPage_4() const { return ___StartPage_4; }
	inline int32_t* get_address_of_StartPage_4() { return &___StartPage_4; }
	inline void set_StartPage_4(int32_t value)
	{
		___StartPage_4 = value;
	}

	inline static int32_t get_offset_of_loop_5() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___loop_5)); }
	inline bool get_loop_5() const { return ___loop_5; }
	inline bool* get_address_of_loop_5() { return &___loop_5; }
	inline void set_loop_5(bool value)
	{
		___loop_5 = value;
	}

	inline static int32_t get_offset_of_onlyScrollWhenPointing_6() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___onlyScrollWhenPointing_6)); }
	inline bool get_onlyScrollWhenPointing_6() const { return ___onlyScrollWhenPointing_6; }
	inline bool* get_address_of_onlyScrollWhenPointing_6() { return &___onlyScrollWhenPointing_6; }
	inline void set_onlyScrollWhenPointing_6(bool value)
	{
		___onlyScrollWhenPointing_6 = value;
	}

	inline static int32_t get_offset_of_numExtraPagesShown_7() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___numExtraPagesShown_7)); }
	inline int32_t get_numExtraPagesShown_7() const { return ___numExtraPagesShown_7; }
	inline int32_t* get_address_of_numExtraPagesShown_7() { return &___numExtraPagesShown_7; }
	inline void set_numExtraPagesShown_7(int32_t value)
	{
		___numExtraPagesShown_7 = value;
	}

	inline static int32_t get_offset_of_showNextPagesAtRest_8() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___showNextPagesAtRest_8)); }
	inline bool get_showNextPagesAtRest_8() const { return ___showNextPagesAtRest_8; }
	inline bool* get_address_of_showNextPagesAtRest_8() { return &___showNextPagesAtRest_8; }
	inline void set_showNextPagesAtRest_8(bool value)
	{
		___showNextPagesAtRest_8 = value;
	}

	inline static int32_t get_offset_of_shouldTilesAlwaysBeInteractable_9() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___shouldTilesAlwaysBeInteractable_9)); }
	inline bool get_shouldTilesAlwaysBeInteractable_9() const { return ___shouldTilesAlwaysBeInteractable_9; }
	inline bool* get_address_of_shouldTilesAlwaysBeInteractable_9() { return &___shouldTilesAlwaysBeInteractable_9; }
	inline void set_shouldTilesAlwaysBeInteractable_9(bool value)
	{
		___shouldTilesAlwaysBeInteractable_9 = value;
	}

	inline static int32_t get_offset_of_scrollingEnabled_10() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___scrollingEnabled_10)); }
	inline bool get_scrollingEnabled_10() const { return ___scrollingEnabled_10; }
	inline bool* get_address_of_scrollingEnabled_10() { return &___scrollingEnabled_10; }
	inline void set_scrollingEnabled_10(bool value)
	{
		___scrollingEnabled_10 = value;
	}

	inline static int32_t get_offset_of_OnActivePageChanged_11() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___OnActivePageChanged_11)); }
	inline ActivePageChangedDelegate_t1951257476 * get_OnActivePageChanged_11() const { return ___OnActivePageChanged_11; }
	inline ActivePageChangedDelegate_t1951257476 ** get_address_of_OnActivePageChanged_11() { return &___OnActivePageChanged_11; }
	inline void set_OnActivePageChanged_11(ActivePageChangedDelegate_t1951257476 * value)
	{
		___OnActivePageChanged_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnActivePageChanged_11), value);
	}

	inline static int32_t get_offset_of_OnSwipeLeft_12() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___OnSwipeLeft_12)); }
	inline UnityEvent_t2581268647 * get_OnSwipeLeft_12() const { return ___OnSwipeLeft_12; }
	inline UnityEvent_t2581268647 ** get_address_of_OnSwipeLeft_12() { return &___OnSwipeLeft_12; }
	inline void set_OnSwipeLeft_12(UnityEvent_t2581268647 * value)
	{
		___OnSwipeLeft_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnSwipeLeft_12), value);
	}

	inline static int32_t get_offset_of_OnSwipeRight_13() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___OnSwipeRight_13)); }
	inline UnityEvent_t2581268647 * get_OnSwipeRight_13() const { return ___OnSwipeRight_13; }
	inline UnityEvent_t2581268647 ** get_address_of_OnSwipeRight_13() { return &___OnSwipeRight_13; }
	inline void set_OnSwipeRight_13(UnityEvent_t2581268647 * value)
	{
		___OnSwipeRight_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnSwipeRight_13), value);
	}

	inline static int32_t get_offset_of_OnSnapClosest_14() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___OnSnapClosest_14)); }
	inline UnityEvent_t2581268647 * get_OnSnapClosest_14() const { return ___OnSnapClosest_14; }
	inline UnityEvent_t2581268647 ** get_address_of_OnSnapClosest_14() { return &___OnSnapClosest_14; }
	inline void set_OnSnapClosest_14(UnityEvent_t2581268647 * value)
	{
		___OnSnapClosest_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnSnapClosest_14), value);
	}

	inline static int32_t get_offset_of_pageProvider_15() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___pageProvider_15)); }
	inline RuntimeObject* get_pageProvider_15() const { return ___pageProvider_15; }
	inline RuntimeObject** get_address_of_pageProvider_15() { return &___pageProvider_15; }
	inline void set_pageProvider_15(RuntimeObject* value)
	{
		___pageProvider_15 = value;
		Il2CppCodeGenWriteBarrier((&___pageProvider_15), value);
	}

	inline static int32_t get_offset_of_scrollEffects_16() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___scrollEffects_16)); }
	inline BaseScrollEffectU5BU5D_t2685664919* get_scrollEffects_16() const { return ___scrollEffects_16; }
	inline BaseScrollEffectU5BU5D_t2685664919** get_address_of_scrollEffects_16() { return &___scrollEffects_16; }
	inline void set_scrollEffects_16(BaseScrollEffectU5BU5D_t2685664919* value)
	{
		___scrollEffects_16 = value;
		Il2CppCodeGenWriteBarrier((&___scrollEffects_16), value);
	}

	inline static int32_t get_offset_of_isTrackingTouches_17() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___isTrackingTouches_17)); }
	inline bool get_isTrackingTouches_17() const { return ___isTrackingTouches_17; }
	inline bool* get_address_of_isTrackingTouches_17() { return &___isTrackingTouches_17; }
	inline void set_isTrackingTouches_17(bool value)
	{
		___isTrackingTouches_17 = value;
	}

	inline static int32_t get_offset_of_initialTouchPos_18() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___initialTouchPos_18)); }
	inline Vector2_t2156229523  get_initialTouchPos_18() const { return ___initialTouchPos_18; }
	inline Vector2_t2156229523 * get_address_of_initialTouchPos_18() { return &___initialTouchPos_18; }
	inline void set_initialTouchPos_18(Vector2_t2156229523  value)
	{
		___initialTouchPos_18 = value;
	}

	inline static int32_t get_offset_of_previousTouchPos_19() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___previousTouchPos_19)); }
	inline Vector2_t2156229523  get_previousTouchPos_19() const { return ___previousTouchPos_19; }
	inline Vector2_t2156229523 * get_address_of_previousTouchPos_19() { return &___previousTouchPos_19; }
	inline void set_previousTouchPos_19(Vector2_t2156229523  value)
	{
		___previousTouchPos_19 = value;
	}

	inline static int32_t get_offset_of_previousTouchTimestamp_20() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___previousTouchTimestamp_20)); }
	inline float get_previousTouchTimestamp_20() const { return ___previousTouchTimestamp_20; }
	inline float* get_address_of_previousTouchTimestamp_20() { return &___previousTouchTimestamp_20; }
	inline void set_previousTouchTimestamp_20(float value)
	{
		___previousTouchTimestamp_20 = value;
	}

	inline static int32_t get_offset_of_overallVelocity_21() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___overallVelocity_21)); }
	inline Vector2_t2156229523  get_overallVelocity_21() const { return ___overallVelocity_21; }
	inline Vector2_t2156229523 * get_address_of_overallVelocity_21() { return &___overallVelocity_21; }
	inline void set_overallVelocity_21(Vector2_t2156229523  value)
	{
		___overallVelocity_21 = value;
	}

	inline static int32_t get_offset_of_isScrolling_22() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___isScrolling_22)); }
	inline bool get_isScrolling_22() const { return ___isScrolling_22; }
	inline bool* get_address_of_isScrolling_22() { return &___isScrolling_22; }
	inline void set_isScrolling_22(bool value)
	{
		___isScrolling_22 = value;
	}

	inline static int32_t get_offset_of_isPointerHovering_23() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___isPointerHovering_23)); }
	inline bool get_isPointerHovering_23() const { return ___isPointerHovering_23; }
	inline bool* get_address_of_isPointerHovering_23() { return &___isPointerHovering_23; }
	inline void set_isPointerHovering_23(bool value)
	{
		___isPointerHovering_23 = value;
	}

	inline static int32_t get_offset_of_scrollOffset_24() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___scrollOffset_24)); }
	inline float get_scrollOffset_24() const { return ___scrollOffset_24; }
	inline float* get_address_of_scrollOffset_24() { return &___scrollOffset_24; }
	inline void set_scrollOffset_24(float value)
	{
		___scrollOffset_24 = value;
	}

	inline static int32_t get_offset_of_targetScrollOffset_25() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___targetScrollOffset_25)); }
	inline float get_targetScrollOffset_25() const { return ___targetScrollOffset_25; }
	inline float* get_address_of_targetScrollOffset_25() { return &___targetScrollOffset_25; }
	inline void set_targetScrollOffset_25(float value)
	{
		___targetScrollOffset_25 = value;
	}

	inline static int32_t get_offset_of_isScrollOffsetOverridden_26() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___isScrollOffsetOverridden_26)); }
	inline bool get_isScrollOffsetOverridden_26() const { return ___isScrollOffsetOverridden_26; }
	inline bool* get_address_of_isScrollOffsetOverridden_26() { return &___isScrollOffsetOverridden_26; }
	inline void set_isScrollOffsetOverridden_26(bool value)
	{
		___isScrollOffsetOverridden_26 = value;
	}

	inline static int32_t get_offset_of_activePage_27() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___activePage_27)); }
	inline RectTransform_t3704657025 * get_activePage_27() const { return ___activePage_27; }
	inline RectTransform_t3704657025 ** get_address_of_activePage_27() { return &___activePage_27; }
	inline void set_activePage_27(RectTransform_t3704657025 * value)
	{
		___activePage_27 = value;
		Il2CppCodeGenWriteBarrier((&___activePage_27), value);
	}

	inline static int32_t get_offset_of_activeSnapCoroutine_28() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___activeSnapCoroutine_28)); }
	inline Coroutine_t3829159415 * get_activeSnapCoroutine_28() const { return ___activeSnapCoroutine_28; }
	inline Coroutine_t3829159415 ** get_address_of_activeSnapCoroutine_28() { return &___activeSnapCoroutine_28; }
	inline void set_activeSnapCoroutine_28(Coroutine_t3829159415 * value)
	{
		___activeSnapCoroutine_28 = value;
		Il2CppCodeGenWriteBarrier((&___activeSnapCoroutine_28), value);
	}

	inline static int32_t get_offset_of_indexToVisiblePage_29() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___indexToVisiblePage_29)); }
	inline Dictionary_2_t2593370356 * get_indexToVisiblePage_29() const { return ___indexToVisiblePage_29; }
	inline Dictionary_2_t2593370356 ** get_address_of_indexToVisiblePage_29() { return &___indexToVisiblePage_29; }
	inline void set_indexToVisiblePage_29(Dictionary_2_t2593370356 * value)
	{
		___indexToVisiblePage_29 = value;
		Il2CppCodeGenWriteBarrier((&___indexToVisiblePage_29), value);
	}

	inline static int32_t get_offset_of_visiblePageToIndex_30() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___visiblePageToIndex_30)); }
	inline Dictionary_2_t1561542660 * get_visiblePageToIndex_30() const { return ___visiblePageToIndex_30; }
	inline Dictionary_2_t1561542660 ** get_address_of_visiblePageToIndex_30() { return &___visiblePageToIndex_30; }
	inline void set_visiblePageToIndex_30(Dictionary_2_t1561542660 * value)
	{
		___visiblePageToIndex_30 = value;
		Il2CppCodeGenWriteBarrier((&___visiblePageToIndex_30), value);
	}

	inline static int32_t get_offset_of_visiblePages_31() { return static_cast<int32_t>(offsetof(PagedScrollRect_t3286523126, ___visiblePages_31)); }
	inline List_1_t881764471 * get_visiblePages_31() const { return ___visiblePages_31; }
	inline List_1_t881764471 ** get_address_of_visiblePages_31() { return &___visiblePages_31; }
	inline void set_visiblePages_31(List_1_t881764471 * value)
	{
		___visiblePages_31 = value;
		Il2CppCodeGenWriteBarrier((&___visiblePages_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGEDSCROLLRECT_T3286523126_H
#ifndef JUMPTOPAGE_T3212685436_H
#define JUMPTOPAGE_T3212685436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JumpToPage
struct  JumpToPage_t3212685436  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform JumpToPage::page
	RectTransform_t3704657025 * ___page_2;
	// UnityEngine.RectTransform JumpToPage::hoverTransform
	RectTransform_t3704657025 * ___hoverTransform_3;
	// System.Single JumpToPage::hoverPositionZMeters
	float ___hoverPositionZMeters_4;
	// System.Single JumpToPage::interpolationSpeed
	float ___interpolationSpeed_5;
	// UnityEngine.UI.Graphic JumpToPage::graphic
	Graphic_t1660335611 * ___graphic_6;
	// System.Single JumpToPage::desiredPositionZ
	float ___desiredPositionZ_7;
	// PagedScrollRect JumpToPage::cachedPagedScrollRect
	PagedScrollRect_t3286523126 * ___cachedPagedScrollRect_8;

public:
	inline static int32_t get_offset_of_page_2() { return static_cast<int32_t>(offsetof(JumpToPage_t3212685436, ___page_2)); }
	inline RectTransform_t3704657025 * get_page_2() const { return ___page_2; }
	inline RectTransform_t3704657025 ** get_address_of_page_2() { return &___page_2; }
	inline void set_page_2(RectTransform_t3704657025 * value)
	{
		___page_2 = value;
		Il2CppCodeGenWriteBarrier((&___page_2), value);
	}

	inline static int32_t get_offset_of_hoverTransform_3() { return static_cast<int32_t>(offsetof(JumpToPage_t3212685436, ___hoverTransform_3)); }
	inline RectTransform_t3704657025 * get_hoverTransform_3() const { return ___hoverTransform_3; }
	inline RectTransform_t3704657025 ** get_address_of_hoverTransform_3() { return &___hoverTransform_3; }
	inline void set_hoverTransform_3(RectTransform_t3704657025 * value)
	{
		___hoverTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___hoverTransform_3), value);
	}

	inline static int32_t get_offset_of_hoverPositionZMeters_4() { return static_cast<int32_t>(offsetof(JumpToPage_t3212685436, ___hoverPositionZMeters_4)); }
	inline float get_hoverPositionZMeters_4() const { return ___hoverPositionZMeters_4; }
	inline float* get_address_of_hoverPositionZMeters_4() { return &___hoverPositionZMeters_4; }
	inline void set_hoverPositionZMeters_4(float value)
	{
		___hoverPositionZMeters_4 = value;
	}

	inline static int32_t get_offset_of_interpolationSpeed_5() { return static_cast<int32_t>(offsetof(JumpToPage_t3212685436, ___interpolationSpeed_5)); }
	inline float get_interpolationSpeed_5() const { return ___interpolationSpeed_5; }
	inline float* get_address_of_interpolationSpeed_5() { return &___interpolationSpeed_5; }
	inline void set_interpolationSpeed_5(float value)
	{
		___interpolationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_graphic_6() { return static_cast<int32_t>(offsetof(JumpToPage_t3212685436, ___graphic_6)); }
	inline Graphic_t1660335611 * get_graphic_6() const { return ___graphic_6; }
	inline Graphic_t1660335611 ** get_address_of_graphic_6() { return &___graphic_6; }
	inline void set_graphic_6(Graphic_t1660335611 * value)
	{
		___graphic_6 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_6), value);
	}

	inline static int32_t get_offset_of_desiredPositionZ_7() { return static_cast<int32_t>(offsetof(JumpToPage_t3212685436, ___desiredPositionZ_7)); }
	inline float get_desiredPositionZ_7() const { return ___desiredPositionZ_7; }
	inline float* get_address_of_desiredPositionZ_7() { return &___desiredPositionZ_7; }
	inline void set_desiredPositionZ_7(float value)
	{
		___desiredPositionZ_7 = value;
	}

	inline static int32_t get_offset_of_cachedPagedScrollRect_8() { return static_cast<int32_t>(offsetof(JumpToPage_t3212685436, ___cachedPagedScrollRect_8)); }
	inline PagedScrollRect_t3286523126 * get_cachedPagedScrollRect_8() const { return ___cachedPagedScrollRect_8; }
	inline PagedScrollRect_t3286523126 ** get_address_of_cachedPagedScrollRect_8() { return &___cachedPagedScrollRect_8; }
	inline void set_cachedPagedScrollRect_8(PagedScrollRect_t3286523126 * value)
	{
		___cachedPagedScrollRect_8 = value;
		Il2CppCodeGenWriteBarrier((&___cachedPagedScrollRect_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMPTOPAGE_T3212685436_H
#ifndef TELEPORT_T310499394_H
#define TELEPORT_T310499394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Teleport
struct  Teleport_t310499394  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 Teleport::startingPosition
	Vector3_t3722313464  ___startingPosition_2;
	// UnityEngine.Material Teleport::inactiveMaterial
	Material_t340375123 * ___inactiveMaterial_3;
	// UnityEngine.Material Teleport::gazedAtMaterial
	Material_t340375123 * ___gazedAtMaterial_4;

public:
	inline static int32_t get_offset_of_startingPosition_2() { return static_cast<int32_t>(offsetof(Teleport_t310499394, ___startingPosition_2)); }
	inline Vector3_t3722313464  get_startingPosition_2() const { return ___startingPosition_2; }
	inline Vector3_t3722313464 * get_address_of_startingPosition_2() { return &___startingPosition_2; }
	inline void set_startingPosition_2(Vector3_t3722313464  value)
	{
		___startingPosition_2 = value;
	}

	inline static int32_t get_offset_of_inactiveMaterial_3() { return static_cast<int32_t>(offsetof(Teleport_t310499394, ___inactiveMaterial_3)); }
	inline Material_t340375123 * get_inactiveMaterial_3() const { return ___inactiveMaterial_3; }
	inline Material_t340375123 ** get_address_of_inactiveMaterial_3() { return &___inactiveMaterial_3; }
	inline void set_inactiveMaterial_3(Material_t340375123 * value)
	{
		___inactiveMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___inactiveMaterial_3), value);
	}

	inline static int32_t get_offset_of_gazedAtMaterial_4() { return static_cast<int32_t>(offsetof(Teleport_t310499394, ___gazedAtMaterial_4)); }
	inline Material_t340375123 * get_gazedAtMaterial_4() const { return ___gazedAtMaterial_4; }
	inline Material_t340375123 ** get_address_of_gazedAtMaterial_4() { return &___gazedAtMaterial_4; }
	inline void set_gazedAtMaterial_4(Material_t340375123 * value)
	{
		___gazedAtMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___gazedAtMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELEPORT_T310499394_H
#ifndef GVRDEMOMANAGER_T3575522595_H
#define GVRDEMOMANAGER_T3575522595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GVRDemoManager
struct  GVRDemoManager_t3575522595  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GVRDemoManager::m_launchVrHomeButton
	GameObject_t1113636619 * ___m_launchVrHomeButton_2;
	// DemoInputManager GVRDemoManager::m_demoInputManager
	DemoInputManager_t35471296 * ___m_demoInputManager_3;

public:
	inline static int32_t get_offset_of_m_launchVrHomeButton_2() { return static_cast<int32_t>(offsetof(GVRDemoManager_t3575522595, ___m_launchVrHomeButton_2)); }
	inline GameObject_t1113636619 * get_m_launchVrHomeButton_2() const { return ___m_launchVrHomeButton_2; }
	inline GameObject_t1113636619 ** get_address_of_m_launchVrHomeButton_2() { return &___m_launchVrHomeButton_2; }
	inline void set_m_launchVrHomeButton_2(GameObject_t1113636619 * value)
	{
		___m_launchVrHomeButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_launchVrHomeButton_2), value);
	}

	inline static int32_t get_offset_of_m_demoInputManager_3() { return static_cast<int32_t>(offsetof(GVRDemoManager_t3575522595, ___m_demoInputManager_3)); }
	inline DemoInputManager_t35471296 * get_m_demoInputManager_3() const { return ___m_demoInputManager_3; }
	inline DemoInputManager_t35471296 ** get_address_of_m_demoInputManager_3() { return &___m_demoInputManager_3; }
	inline void set_m_demoInputManager_3(DemoInputManager_t35471296 * value)
	{
		___m_demoInputManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_demoInputManager_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRDEMOMANAGER_T3575522595_H
#ifndef DEMOSCENEMANAGER_T570878136_H
#define DEMOSCENEMANAGER_T570878136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoSceneManager
struct  DemoSceneManager_t570878136  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCENEMANAGER_T570878136_H
#ifndef MAINTHREADDISPATCHER_T3211665238_H
#define MAINTHREADDISPATCHER_T3211665238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Store.MainThreadDispatcher
struct  MainThreadDispatcher_t3211665238  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct MainThreadDispatcher_t3211665238_StaticFields
{
public:
	// System.String UnityEngine.Store.MainThreadDispatcher::OBJECT_NAME
	String_t* ___OBJECT_NAME_2;
	// System.Collections.Generic.List`1<System.Action> UnityEngine.Store.MainThreadDispatcher::s_Callbacks
	List_1_t2736452219 * ___s_Callbacks_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngine.Store.MainThreadDispatcher::s_CallbacksPending
	bool ___s_CallbacksPending_4;

public:
	inline static int32_t get_offset_of_OBJECT_NAME_2() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3211665238_StaticFields, ___OBJECT_NAME_2)); }
	inline String_t* get_OBJECT_NAME_2() const { return ___OBJECT_NAME_2; }
	inline String_t** get_address_of_OBJECT_NAME_2() { return &___OBJECT_NAME_2; }
	inline void set_OBJECT_NAME_2(String_t* value)
	{
		___OBJECT_NAME_2 = value;
		Il2CppCodeGenWriteBarrier((&___OBJECT_NAME_2), value);
	}

	inline static int32_t get_offset_of_s_Callbacks_3() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3211665238_StaticFields, ___s_Callbacks_3)); }
	inline List_1_t2736452219 * get_s_Callbacks_3() const { return ___s_Callbacks_3; }
	inline List_1_t2736452219 ** get_address_of_s_Callbacks_3() { return &___s_Callbacks_3; }
	inline void set_s_Callbacks_3(List_1_t2736452219 * value)
	{
		___s_Callbacks_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Callbacks_3), value);
	}

	inline static int32_t get_offset_of_s_CallbacksPending_4() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3211665238_StaticFields, ___s_CallbacksPending_4)); }
	inline bool get_s_CallbacksPending_4() const { return ___s_CallbacksPending_4; }
	inline bool* get_address_of_s_CallbacksPending_4() { return &___s_CallbacksPending_4; }
	inline void set_s_CallbacksPending_4(bool value)
	{
		___s_CallbacksPending_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINTHREADDISPATCHER_T3211665238_H
#ifndef BASETILE_T770030199_H
#define BASETILE_T770030199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseTile
struct  BaseTile_t770030199  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform BaseTile::originalParent
	Transform_t3600365921 * ___originalParent_2;
	// UnityEngine.Quaternion BaseTile::originalRotation
	Quaternion_t2301928331  ___originalRotation_3;
	// UnityEngine.Vector3 BaseTile::originalPosition
	Vector3_t3722313464  ___originalPosition_4;
	// UnityEngine.Vector3 BaseTile::originalScale
	Vector3_t3722313464  ___originalScale_5;
	// TiledPage BaseTile::page
	TiledPage_t1725039945 * ___page_6;
	// System.Boolean BaseTile::isInteractable
	bool ___isInteractable_7;
	// System.Boolean BaseTile::isHovering
	bool ___isHovering_8;
	// System.Nullable`1<System.Single> BaseTile::metersToCanvasScale
	Nullable_1_t3119828856  ___metersToCanvasScale_9;

public:
	inline static int32_t get_offset_of_originalParent_2() { return static_cast<int32_t>(offsetof(BaseTile_t770030199, ___originalParent_2)); }
	inline Transform_t3600365921 * get_originalParent_2() const { return ___originalParent_2; }
	inline Transform_t3600365921 ** get_address_of_originalParent_2() { return &___originalParent_2; }
	inline void set_originalParent_2(Transform_t3600365921 * value)
	{
		___originalParent_2 = value;
		Il2CppCodeGenWriteBarrier((&___originalParent_2), value);
	}

	inline static int32_t get_offset_of_originalRotation_3() { return static_cast<int32_t>(offsetof(BaseTile_t770030199, ___originalRotation_3)); }
	inline Quaternion_t2301928331  get_originalRotation_3() const { return ___originalRotation_3; }
	inline Quaternion_t2301928331 * get_address_of_originalRotation_3() { return &___originalRotation_3; }
	inline void set_originalRotation_3(Quaternion_t2301928331  value)
	{
		___originalRotation_3 = value;
	}

	inline static int32_t get_offset_of_originalPosition_4() { return static_cast<int32_t>(offsetof(BaseTile_t770030199, ___originalPosition_4)); }
	inline Vector3_t3722313464  get_originalPosition_4() const { return ___originalPosition_4; }
	inline Vector3_t3722313464 * get_address_of_originalPosition_4() { return &___originalPosition_4; }
	inline void set_originalPosition_4(Vector3_t3722313464  value)
	{
		___originalPosition_4 = value;
	}

	inline static int32_t get_offset_of_originalScale_5() { return static_cast<int32_t>(offsetof(BaseTile_t770030199, ___originalScale_5)); }
	inline Vector3_t3722313464  get_originalScale_5() const { return ___originalScale_5; }
	inline Vector3_t3722313464 * get_address_of_originalScale_5() { return &___originalScale_5; }
	inline void set_originalScale_5(Vector3_t3722313464  value)
	{
		___originalScale_5 = value;
	}

	inline static int32_t get_offset_of_page_6() { return static_cast<int32_t>(offsetof(BaseTile_t770030199, ___page_6)); }
	inline TiledPage_t1725039945 * get_page_6() const { return ___page_6; }
	inline TiledPage_t1725039945 ** get_address_of_page_6() { return &___page_6; }
	inline void set_page_6(TiledPage_t1725039945 * value)
	{
		___page_6 = value;
		Il2CppCodeGenWriteBarrier((&___page_6), value);
	}

	inline static int32_t get_offset_of_isInteractable_7() { return static_cast<int32_t>(offsetof(BaseTile_t770030199, ___isInteractable_7)); }
	inline bool get_isInteractable_7() const { return ___isInteractable_7; }
	inline bool* get_address_of_isInteractable_7() { return &___isInteractable_7; }
	inline void set_isInteractable_7(bool value)
	{
		___isInteractable_7 = value;
	}

	inline static int32_t get_offset_of_isHovering_8() { return static_cast<int32_t>(offsetof(BaseTile_t770030199, ___isHovering_8)); }
	inline bool get_isHovering_8() const { return ___isHovering_8; }
	inline bool* get_address_of_isHovering_8() { return &___isHovering_8; }
	inline void set_isHovering_8(bool value)
	{
		___isHovering_8 = value;
	}

	inline static int32_t get_offset_of_metersToCanvasScale_9() { return static_cast<int32_t>(offsetof(BaseTile_t770030199, ___metersToCanvasScale_9)); }
	inline Nullable_1_t3119828856  get_metersToCanvasScale_9() const { return ___metersToCanvasScale_9; }
	inline Nullable_1_t3119828856 * get_address_of_metersToCanvasScale_9() { return &___metersToCanvasScale_9; }
	inline void set_metersToCanvasScale_9(Nullable_1_t3119828856  value)
	{
		___metersToCanvasScale_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASETILE_T770030199_H
#ifndef TMP_SUBMESH_T2613037997_H
#define TMP_SUBMESH_T2613037997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMesh
struct  TMP_SubMesh_t2613037997  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMesh::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_2;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMesh::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_3;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_material
	Material_t340375123 * ___m_material_4;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_5;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackMaterial
	Material_t340375123 * ___m_fallbackMaterial_6;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackSourceMaterial
	Material_t340375123 * ___m_fallbackSourceMaterial_7;
	// System.Boolean TMPro.TMP_SubMesh::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_8;
	// System.Single TMPro.TMP_SubMesh::m_padding
	float ___m_padding_9;
	// UnityEngine.Renderer TMPro.TMP_SubMesh::m_renderer
	Renderer_t2627027031 * ___m_renderer_10;
	// UnityEngine.MeshFilter TMPro.TMP_SubMesh::m_meshFilter
	MeshFilter_t3523625662 * ___m_meshFilter_11;
	// UnityEngine.Mesh TMPro.TMP_SubMesh::m_mesh
	Mesh_t3648964284 * ___m_mesh_12;
	// UnityEngine.BoxCollider TMPro.TMP_SubMesh::m_boxCollider
	BoxCollider_t1640800422 * ___m_boxCollider_13;
	// TMPro.TextMeshPro TMPro.TMP_SubMesh::m_TextComponent
	TextMeshPro_t2393593166 * ___m_TextComponent_14;
	// System.Boolean TMPro.TMP_SubMesh::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_15;

public:
	inline static int32_t get_offset_of_m_fontAsset_2() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_fontAsset_2)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_2() const { return ___m_fontAsset_2; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_2() { return &___m_fontAsset_2; }
	inline void set_m_fontAsset_2(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_2), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_3() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_spriteAsset_3)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_3() const { return ___m_spriteAsset_3; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_3() { return &___m_spriteAsset_3; }
	inline void set_m_spriteAsset_3(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_3), value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_material_4)); }
	inline Material_t340375123 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t340375123 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t340375123 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_4), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_5() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_sharedMaterial_5)); }
	inline Material_t340375123 * get_m_sharedMaterial_5() const { return ___m_sharedMaterial_5; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_5() { return &___m_sharedMaterial_5; }
	inline void set_m_sharedMaterial_5(Material_t340375123 * value)
	{
		___m_sharedMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_5), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_fallbackMaterial_6)); }
	inline Material_t340375123 * get_m_fallbackMaterial_6() const { return ___m_fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_m_fallbackMaterial_6() { return &___m_fallbackMaterial_6; }
	inline void set_m_fallbackMaterial_6(Material_t340375123 * value)
	{
		___m_fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_7() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_fallbackSourceMaterial_7)); }
	inline Material_t340375123 * get_m_fallbackSourceMaterial_7() const { return ___m_fallbackSourceMaterial_7; }
	inline Material_t340375123 ** get_address_of_m_fallbackSourceMaterial_7() { return &___m_fallbackSourceMaterial_7; }
	inline void set_m_fallbackSourceMaterial_7(Material_t340375123 * value)
	{
		___m_fallbackSourceMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_7), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_8() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_isDefaultMaterial_8)); }
	inline bool get_m_isDefaultMaterial_8() const { return ___m_isDefaultMaterial_8; }
	inline bool* get_address_of_m_isDefaultMaterial_8() { return &___m_isDefaultMaterial_8; }
	inline void set_m_isDefaultMaterial_8(bool value)
	{
		___m_isDefaultMaterial_8 = value;
	}

	inline static int32_t get_offset_of_m_padding_9() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_padding_9)); }
	inline float get_m_padding_9() const { return ___m_padding_9; }
	inline float* get_address_of_m_padding_9() { return &___m_padding_9; }
	inline void set_m_padding_9(float value)
	{
		___m_padding_9 = value;
	}

	inline static int32_t get_offset_of_m_renderer_10() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_renderer_10)); }
	inline Renderer_t2627027031 * get_m_renderer_10() const { return ___m_renderer_10; }
	inline Renderer_t2627027031 ** get_address_of_m_renderer_10() { return &___m_renderer_10; }
	inline void set_m_renderer_10(Renderer_t2627027031 * value)
	{
		___m_renderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_10), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_11() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_meshFilter_11)); }
	inline MeshFilter_t3523625662 * get_m_meshFilter_11() const { return ___m_meshFilter_11; }
	inline MeshFilter_t3523625662 ** get_address_of_m_meshFilter_11() { return &___m_meshFilter_11; }
	inline void set_m_meshFilter_11(MeshFilter_t3523625662 * value)
	{
		___m_meshFilter_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_11), value);
	}

	inline static int32_t get_offset_of_m_mesh_12() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_mesh_12)); }
	inline Mesh_t3648964284 * get_m_mesh_12() const { return ___m_mesh_12; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_12() { return &___m_mesh_12; }
	inline void set_m_mesh_12(Mesh_t3648964284 * value)
	{
		___m_mesh_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_12), value);
	}

	inline static int32_t get_offset_of_m_boxCollider_13() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_boxCollider_13)); }
	inline BoxCollider_t1640800422 * get_m_boxCollider_13() const { return ___m_boxCollider_13; }
	inline BoxCollider_t1640800422 ** get_address_of_m_boxCollider_13() { return &___m_boxCollider_13; }
	inline void set_m_boxCollider_13(BoxCollider_t1640800422 * value)
	{
		___m_boxCollider_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_boxCollider_13), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_14() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_TextComponent_14)); }
	inline TextMeshPro_t2393593166 * get_m_TextComponent_14() const { return ___m_TextComponent_14; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextComponent_14() { return &___m_TextComponent_14; }
	inline void set_m_TextComponent_14(TextMeshPro_t2393593166 * value)
	{
		___m_TextComponent_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_14), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_15() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t2613037997, ___m_isRegisteredForEvents_15)); }
	inline bool get_m_isRegisteredForEvents_15() const { return ___m_isRegisteredForEvents_15; }
	inline bool* get_address_of_m_isRegisteredForEvents_15() { return &___m_isRegisteredForEvents_15; }
	inline void set_m_isRegisteredForEvents_15(bool value)
	{
		___m_isRegisteredForEvents_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESH_T2613037997_H
#ifndef DEMOINPUTMANAGER_T35471296_H
#define DEMOINPUTMANAGER_T35471296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoInputManager
struct  DemoInputManager_t35471296  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean DemoInputManager::isDaydream
	bool ___isDaydream_16;
	// UnityEngine.GameObject DemoInputManager::controllerMain
	GameObject_t1113636619 * ___controllerMain_19;
	// UnityEngine.GameObject DemoInputManager::controllerPointer
	GameObject_t1113636619 * ___controllerPointer_21;
	// UnityEngine.GameObject DemoInputManager::reticlePointer
	GameObject_t1113636619 * ___reticlePointer_23;
	// UnityEngine.GameObject DemoInputManager::messageCanvas
	GameObject_t1113636619 * ___messageCanvas_25;
	// UnityEngine.UI.Text DemoInputManager::messageText
	Text_t1901882714 * ___messageText_26;
	// DemoInputManager/EmulatedPlatformType DemoInputManager::gvrEmulatedPlatformType
	int32_t ___gvrEmulatedPlatformType_27;

public:
	inline static int32_t get_offset_of_isDaydream_16() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296, ___isDaydream_16)); }
	inline bool get_isDaydream_16() const { return ___isDaydream_16; }
	inline bool* get_address_of_isDaydream_16() { return &___isDaydream_16; }
	inline void set_isDaydream_16(bool value)
	{
		___isDaydream_16 = value;
	}

	inline static int32_t get_offset_of_controllerMain_19() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296, ___controllerMain_19)); }
	inline GameObject_t1113636619 * get_controllerMain_19() const { return ___controllerMain_19; }
	inline GameObject_t1113636619 ** get_address_of_controllerMain_19() { return &___controllerMain_19; }
	inline void set_controllerMain_19(GameObject_t1113636619 * value)
	{
		___controllerMain_19 = value;
		Il2CppCodeGenWriteBarrier((&___controllerMain_19), value);
	}

	inline static int32_t get_offset_of_controllerPointer_21() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296, ___controllerPointer_21)); }
	inline GameObject_t1113636619 * get_controllerPointer_21() const { return ___controllerPointer_21; }
	inline GameObject_t1113636619 ** get_address_of_controllerPointer_21() { return &___controllerPointer_21; }
	inline void set_controllerPointer_21(GameObject_t1113636619 * value)
	{
		___controllerPointer_21 = value;
		Il2CppCodeGenWriteBarrier((&___controllerPointer_21), value);
	}

	inline static int32_t get_offset_of_reticlePointer_23() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296, ___reticlePointer_23)); }
	inline GameObject_t1113636619 * get_reticlePointer_23() const { return ___reticlePointer_23; }
	inline GameObject_t1113636619 ** get_address_of_reticlePointer_23() { return &___reticlePointer_23; }
	inline void set_reticlePointer_23(GameObject_t1113636619 * value)
	{
		___reticlePointer_23 = value;
		Il2CppCodeGenWriteBarrier((&___reticlePointer_23), value);
	}

	inline static int32_t get_offset_of_messageCanvas_25() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296, ___messageCanvas_25)); }
	inline GameObject_t1113636619 * get_messageCanvas_25() const { return ___messageCanvas_25; }
	inline GameObject_t1113636619 ** get_address_of_messageCanvas_25() { return &___messageCanvas_25; }
	inline void set_messageCanvas_25(GameObject_t1113636619 * value)
	{
		___messageCanvas_25 = value;
		Il2CppCodeGenWriteBarrier((&___messageCanvas_25), value);
	}

	inline static int32_t get_offset_of_messageText_26() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296, ___messageText_26)); }
	inline Text_t1901882714 * get_messageText_26() const { return ___messageText_26; }
	inline Text_t1901882714 ** get_address_of_messageText_26() { return &___messageText_26; }
	inline void set_messageText_26(Text_t1901882714 * value)
	{
		___messageText_26 = value;
		Il2CppCodeGenWriteBarrier((&___messageText_26), value);
	}

	inline static int32_t get_offset_of_gvrEmulatedPlatformType_27() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296, ___gvrEmulatedPlatformType_27)); }
	inline int32_t get_gvrEmulatedPlatformType_27() const { return ___gvrEmulatedPlatformType_27; }
	inline int32_t* get_address_of_gvrEmulatedPlatformType_27() { return &___gvrEmulatedPlatformType_27; }
	inline void set_gvrEmulatedPlatformType_27(int32_t value)
	{
		___gvrEmulatedPlatformType_27 = value;
	}
};

struct DemoInputManager_t35471296_StaticFields
{
public:
	// System.String DemoInputManager::CARDBOARD_DEVICE_NAME
	String_t* ___CARDBOARD_DEVICE_NAME_17;
	// System.String DemoInputManager::DAYDREAM_DEVICE_NAME
	String_t* ___DAYDREAM_DEVICE_NAME_18;
	// System.String DemoInputManager::CONTROLLER_MAIN_PROP_NAME
	String_t* ___CONTROLLER_MAIN_PROP_NAME_20;
	// System.String DemoInputManager::CONTROLLER_POINTER_PROP_NAME
	String_t* ___CONTROLLER_POINTER_PROP_NAME_22;
	// System.String DemoInputManager::RETICLE_POINTER_PROP_NAME
	String_t* ___RETICLE_POINTER_PROP_NAME_24;
	// System.String DemoInputManager::EMULATED_PLATFORM_PROP_NAME
	String_t* ___EMULATED_PLATFORM_PROP_NAME_28;
	// System.Predicate`1<System.String> DemoInputManager::<>f__am$cache0
	Predicate_1_t2672744813 * ___U3CU3Ef__amU24cache0_29;
	// System.Predicate`1<System.String> DemoInputManager::<>f__am$cache1
	Predicate_1_t2672744813 * ___U3CU3Ef__amU24cache1_30;

public:
	inline static int32_t get_offset_of_CARDBOARD_DEVICE_NAME_17() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296_StaticFields, ___CARDBOARD_DEVICE_NAME_17)); }
	inline String_t* get_CARDBOARD_DEVICE_NAME_17() const { return ___CARDBOARD_DEVICE_NAME_17; }
	inline String_t** get_address_of_CARDBOARD_DEVICE_NAME_17() { return &___CARDBOARD_DEVICE_NAME_17; }
	inline void set_CARDBOARD_DEVICE_NAME_17(String_t* value)
	{
		___CARDBOARD_DEVICE_NAME_17 = value;
		Il2CppCodeGenWriteBarrier((&___CARDBOARD_DEVICE_NAME_17), value);
	}

	inline static int32_t get_offset_of_DAYDREAM_DEVICE_NAME_18() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296_StaticFields, ___DAYDREAM_DEVICE_NAME_18)); }
	inline String_t* get_DAYDREAM_DEVICE_NAME_18() const { return ___DAYDREAM_DEVICE_NAME_18; }
	inline String_t** get_address_of_DAYDREAM_DEVICE_NAME_18() { return &___DAYDREAM_DEVICE_NAME_18; }
	inline void set_DAYDREAM_DEVICE_NAME_18(String_t* value)
	{
		___DAYDREAM_DEVICE_NAME_18 = value;
		Il2CppCodeGenWriteBarrier((&___DAYDREAM_DEVICE_NAME_18), value);
	}

	inline static int32_t get_offset_of_CONTROLLER_MAIN_PROP_NAME_20() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296_StaticFields, ___CONTROLLER_MAIN_PROP_NAME_20)); }
	inline String_t* get_CONTROLLER_MAIN_PROP_NAME_20() const { return ___CONTROLLER_MAIN_PROP_NAME_20; }
	inline String_t** get_address_of_CONTROLLER_MAIN_PROP_NAME_20() { return &___CONTROLLER_MAIN_PROP_NAME_20; }
	inline void set_CONTROLLER_MAIN_PROP_NAME_20(String_t* value)
	{
		___CONTROLLER_MAIN_PROP_NAME_20 = value;
		Il2CppCodeGenWriteBarrier((&___CONTROLLER_MAIN_PROP_NAME_20), value);
	}

	inline static int32_t get_offset_of_CONTROLLER_POINTER_PROP_NAME_22() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296_StaticFields, ___CONTROLLER_POINTER_PROP_NAME_22)); }
	inline String_t* get_CONTROLLER_POINTER_PROP_NAME_22() const { return ___CONTROLLER_POINTER_PROP_NAME_22; }
	inline String_t** get_address_of_CONTROLLER_POINTER_PROP_NAME_22() { return &___CONTROLLER_POINTER_PROP_NAME_22; }
	inline void set_CONTROLLER_POINTER_PROP_NAME_22(String_t* value)
	{
		___CONTROLLER_POINTER_PROP_NAME_22 = value;
		Il2CppCodeGenWriteBarrier((&___CONTROLLER_POINTER_PROP_NAME_22), value);
	}

	inline static int32_t get_offset_of_RETICLE_POINTER_PROP_NAME_24() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296_StaticFields, ___RETICLE_POINTER_PROP_NAME_24)); }
	inline String_t* get_RETICLE_POINTER_PROP_NAME_24() const { return ___RETICLE_POINTER_PROP_NAME_24; }
	inline String_t** get_address_of_RETICLE_POINTER_PROP_NAME_24() { return &___RETICLE_POINTER_PROP_NAME_24; }
	inline void set_RETICLE_POINTER_PROP_NAME_24(String_t* value)
	{
		___RETICLE_POINTER_PROP_NAME_24 = value;
		Il2CppCodeGenWriteBarrier((&___RETICLE_POINTER_PROP_NAME_24), value);
	}

	inline static int32_t get_offset_of_EMULATED_PLATFORM_PROP_NAME_28() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296_StaticFields, ___EMULATED_PLATFORM_PROP_NAME_28)); }
	inline String_t* get_EMULATED_PLATFORM_PROP_NAME_28() const { return ___EMULATED_PLATFORM_PROP_NAME_28; }
	inline String_t** get_address_of_EMULATED_PLATFORM_PROP_NAME_28() { return &___EMULATED_PLATFORM_PROP_NAME_28; }
	inline void set_EMULATED_PLATFORM_PROP_NAME_28(String_t* value)
	{
		___EMULATED_PLATFORM_PROP_NAME_28 = value;
		Il2CppCodeGenWriteBarrier((&___EMULATED_PLATFORM_PROP_NAME_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_29() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296_StaticFields, ___U3CU3Ef__amU24cache0_29)); }
	inline Predicate_1_t2672744813 * get_U3CU3Ef__amU24cache0_29() const { return ___U3CU3Ef__amU24cache0_29; }
	inline Predicate_1_t2672744813 ** get_address_of_U3CU3Ef__amU24cache0_29() { return &___U3CU3Ef__amU24cache0_29; }
	inline void set_U3CU3Ef__amU24cache0_29(Predicate_1_t2672744813 * value)
	{
		___U3CU3Ef__amU24cache0_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_30() { return static_cast<int32_t>(offsetof(DemoInputManager_t35471296_StaticFields, ___U3CU3Ef__amU24cache1_30)); }
	inline Predicate_1_t2672744813 * get_U3CU3Ef__amU24cache1_30() const { return ___U3CU3Ef__amU24cache1_30; }
	inline Predicate_1_t2672744813 ** get_address_of_U3CU3Ef__amU24cache1_30() { return &___U3CU3Ef__amU24cache1_30; }
	inline void set_U3CU3Ef__amU24cache1_30(Predicate_1_t2672744813 * value)
	{
		___U3CU3Ef__amU24cache1_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOINPUTMANAGER_T35471296_H
#ifndef TILEDPAGE_T1725039945_H
#define TILEDPAGE_T1725039945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiledPage
struct  TiledPage_t1725039945  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform[] TiledPage::tiles
	TransformU5BU5D_t807237628* ___tiles_2;
	// UnityEngine.RectTransform TiledPage::layoutTransform
	RectTransform_t3704657025 * ___layoutTransform_3;
	// System.Single TiledPage::staggerAnimationIntensity
	float ___staggerAnimationIntensity_4;
	// TiledPage/TileOrderBy TiledPage::tileOrderBy
	int32_t ___tileOrderBy_5;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Transform>> TiledPage::tilesByDistanceFromLeft
	List_1_t2249548109 * ___tilesByDistanceFromLeft_6;

public:
	inline static int32_t get_offset_of_tiles_2() { return static_cast<int32_t>(offsetof(TiledPage_t1725039945, ___tiles_2)); }
	inline TransformU5BU5D_t807237628* get_tiles_2() const { return ___tiles_2; }
	inline TransformU5BU5D_t807237628** get_address_of_tiles_2() { return &___tiles_2; }
	inline void set_tiles_2(TransformU5BU5D_t807237628* value)
	{
		___tiles_2 = value;
		Il2CppCodeGenWriteBarrier((&___tiles_2), value);
	}

	inline static int32_t get_offset_of_layoutTransform_3() { return static_cast<int32_t>(offsetof(TiledPage_t1725039945, ___layoutTransform_3)); }
	inline RectTransform_t3704657025 * get_layoutTransform_3() const { return ___layoutTransform_3; }
	inline RectTransform_t3704657025 ** get_address_of_layoutTransform_3() { return &___layoutTransform_3; }
	inline void set_layoutTransform_3(RectTransform_t3704657025 * value)
	{
		___layoutTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___layoutTransform_3), value);
	}

	inline static int32_t get_offset_of_staggerAnimationIntensity_4() { return static_cast<int32_t>(offsetof(TiledPage_t1725039945, ___staggerAnimationIntensity_4)); }
	inline float get_staggerAnimationIntensity_4() const { return ___staggerAnimationIntensity_4; }
	inline float* get_address_of_staggerAnimationIntensity_4() { return &___staggerAnimationIntensity_4; }
	inline void set_staggerAnimationIntensity_4(float value)
	{
		___staggerAnimationIntensity_4 = value;
	}

	inline static int32_t get_offset_of_tileOrderBy_5() { return static_cast<int32_t>(offsetof(TiledPage_t1725039945, ___tileOrderBy_5)); }
	inline int32_t get_tileOrderBy_5() const { return ___tileOrderBy_5; }
	inline int32_t* get_address_of_tileOrderBy_5() { return &___tileOrderBy_5; }
	inline void set_tileOrderBy_5(int32_t value)
	{
		___tileOrderBy_5 = value;
	}

	inline static int32_t get_offset_of_tilesByDistanceFromLeft_6() { return static_cast<int32_t>(offsetof(TiledPage_t1725039945, ___tilesByDistanceFromLeft_6)); }
	inline List_1_t2249548109 * get_tilesByDistanceFromLeft_6() const { return ___tilesByDistanceFromLeft_6; }
	inline List_1_t2249548109 ** get_address_of_tilesByDistanceFromLeft_6() { return &___tilesByDistanceFromLeft_6; }
	inline void set_tilesByDistanceFromLeft_6(List_1_t2249548109 * value)
	{
		___tilesByDistanceFromLeft_6 = value;
		Il2CppCodeGenWriteBarrier((&___tilesByDistanceFromLeft_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEDPAGE_T1725039945_H
#ifndef KEYBOARDDELEGATEEXAMPLE_T150747960_H
#define KEYBOARDDELEGATEEXAMPLE_T150747960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KeyboardDelegateExample
struct  KeyboardDelegateExample_t150747960  : public GvrKeyboardDelegateBase_t30895224
{
public:
	// UnityEngine.UI.Text KeyboardDelegateExample::KeyboardText
	Text_t1901882714 * ___KeyboardText_2;
	// UnityEngine.Canvas KeyboardDelegateExample::UpdateCanvas
	Canvas_t3310196443 * ___UpdateCanvas_3;
	// System.EventHandler KeyboardDelegateExample::KeyboardHidden
	EventHandler_t1348719766 * ___KeyboardHidden_4;
	// System.EventHandler KeyboardDelegateExample::KeyboardShown
	EventHandler_t1348719766 * ___KeyboardShown_5;

public:
	inline static int32_t get_offset_of_KeyboardText_2() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t150747960, ___KeyboardText_2)); }
	inline Text_t1901882714 * get_KeyboardText_2() const { return ___KeyboardText_2; }
	inline Text_t1901882714 ** get_address_of_KeyboardText_2() { return &___KeyboardText_2; }
	inline void set_KeyboardText_2(Text_t1901882714 * value)
	{
		___KeyboardText_2 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardText_2), value);
	}

	inline static int32_t get_offset_of_UpdateCanvas_3() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t150747960, ___UpdateCanvas_3)); }
	inline Canvas_t3310196443 * get_UpdateCanvas_3() const { return ___UpdateCanvas_3; }
	inline Canvas_t3310196443 ** get_address_of_UpdateCanvas_3() { return &___UpdateCanvas_3; }
	inline void set_UpdateCanvas_3(Canvas_t3310196443 * value)
	{
		___UpdateCanvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateCanvas_3), value);
	}

	inline static int32_t get_offset_of_KeyboardHidden_4() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t150747960, ___KeyboardHidden_4)); }
	inline EventHandler_t1348719766 * get_KeyboardHidden_4() const { return ___KeyboardHidden_4; }
	inline EventHandler_t1348719766 ** get_address_of_KeyboardHidden_4() { return &___KeyboardHidden_4; }
	inline void set_KeyboardHidden_4(EventHandler_t1348719766 * value)
	{
		___KeyboardHidden_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardHidden_4), value);
	}

	inline static int32_t get_offset_of_KeyboardShown_5() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t150747960, ___KeyboardShown_5)); }
	inline EventHandler_t1348719766 * get_KeyboardShown_5() const { return ___KeyboardShown_5; }
	inline EventHandler_t1348719766 ** get_address_of_KeyboardShown_5() { return &___KeyboardShown_5; }
	inline void set_KeyboardShown_5(EventHandler_t1348719766 * value)
	{
		___KeyboardShown_5 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardShown_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDDELEGATEEXAMPLE_T150747960_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_8() const { return ___m_CanvasRenderer_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_8() { return &___m_CanvasRenderer_8; }
	inline void set_m_CanvasRenderer_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SCALESCROLLEFFECT_T2628707146_H
#define SCALESCROLLEFFECT_T2628707146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScaleScrollEffect
struct  ScaleScrollEffect_t2628707146  : public BaseScrollEffect_t1789251746
{
public:
	// System.Single ScaleScrollEffect::minScale
	float ___minScale_2;

public:
	inline static int32_t get_offset_of_minScale_2() { return static_cast<int32_t>(offsetof(ScaleScrollEffect_t2628707146, ___minScale_2)); }
	inline float get_minScale_2() const { return ___minScale_2; }
	inline float* get_address_of_minScale_2() { return &___minScale_2; }
	inline void set_minScale_2(float value)
	{
		___minScale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALESCROLLEFFECT_T2628707146_H
#ifndef FADESCROLLEFFECT_T2917998861_H
#define FADESCROLLEFFECT_T2917998861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeScrollEffect
struct  FadeScrollEffect_t2917998861  : public BaseScrollEffect_t1789251746
{
public:
	// System.Single FadeScrollEffect::minAlpha
	float ___minAlpha_2;

public:
	inline static int32_t get_offset_of_minAlpha_2() { return static_cast<int32_t>(offsetof(FadeScrollEffect_t2917998861, ___minAlpha_2)); }
	inline float get_minAlpha_2() const { return ___minAlpha_2; }
	inline float* get_address_of_minAlpha_2() { return &___minAlpha_2; }
	inline void set_minAlpha_2(float value)
	{
		___minAlpha_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADESCROLLEFFECT_T2917998861_H
#ifndef FLOATTILE_T2669707442_H
#define FLOATTILE_T2669707442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatTile
struct  FloatTile_t2669707442  : public BaseTile_t770030199
{
public:
	// UnityEngine.Quaternion FloatTile::desiredRotation
	Quaternion_t2301928331  ___desiredRotation_13;
	// System.Single FloatTile::desiredPositionZ
	float ___desiredPositionZ_14;
	// UnityEngine.Vector3 FloatTile::desiredScale
	Vector3_t3722313464  ___desiredScale_15;
	// System.Single FloatTile::hoverScale
	float ___hoverScale_16;
	// System.Single FloatTile::hoverPositionZMeters
	float ___hoverPositionZMeters_17;
	// System.Single FloatTile::maximumRotationDegreesCamera
	float ___maximumRotationDegreesCamera_18;
	// System.Single FloatTile::maximumRotationDegreesPointer
	float ___maximumRotationDegreesPointer_19;
	// System.Single FloatTile::interpolationSpeed
	float ___interpolationSpeed_20;

public:
	inline static int32_t get_offset_of_desiredRotation_13() { return static_cast<int32_t>(offsetof(FloatTile_t2669707442, ___desiredRotation_13)); }
	inline Quaternion_t2301928331  get_desiredRotation_13() const { return ___desiredRotation_13; }
	inline Quaternion_t2301928331 * get_address_of_desiredRotation_13() { return &___desiredRotation_13; }
	inline void set_desiredRotation_13(Quaternion_t2301928331  value)
	{
		___desiredRotation_13 = value;
	}

	inline static int32_t get_offset_of_desiredPositionZ_14() { return static_cast<int32_t>(offsetof(FloatTile_t2669707442, ___desiredPositionZ_14)); }
	inline float get_desiredPositionZ_14() const { return ___desiredPositionZ_14; }
	inline float* get_address_of_desiredPositionZ_14() { return &___desiredPositionZ_14; }
	inline void set_desiredPositionZ_14(float value)
	{
		___desiredPositionZ_14 = value;
	}

	inline static int32_t get_offset_of_desiredScale_15() { return static_cast<int32_t>(offsetof(FloatTile_t2669707442, ___desiredScale_15)); }
	inline Vector3_t3722313464  get_desiredScale_15() const { return ___desiredScale_15; }
	inline Vector3_t3722313464 * get_address_of_desiredScale_15() { return &___desiredScale_15; }
	inline void set_desiredScale_15(Vector3_t3722313464  value)
	{
		___desiredScale_15 = value;
	}

	inline static int32_t get_offset_of_hoverScale_16() { return static_cast<int32_t>(offsetof(FloatTile_t2669707442, ___hoverScale_16)); }
	inline float get_hoverScale_16() const { return ___hoverScale_16; }
	inline float* get_address_of_hoverScale_16() { return &___hoverScale_16; }
	inline void set_hoverScale_16(float value)
	{
		___hoverScale_16 = value;
	}

	inline static int32_t get_offset_of_hoverPositionZMeters_17() { return static_cast<int32_t>(offsetof(FloatTile_t2669707442, ___hoverPositionZMeters_17)); }
	inline float get_hoverPositionZMeters_17() const { return ___hoverPositionZMeters_17; }
	inline float* get_address_of_hoverPositionZMeters_17() { return &___hoverPositionZMeters_17; }
	inline void set_hoverPositionZMeters_17(float value)
	{
		___hoverPositionZMeters_17 = value;
	}

	inline static int32_t get_offset_of_maximumRotationDegreesCamera_18() { return static_cast<int32_t>(offsetof(FloatTile_t2669707442, ___maximumRotationDegreesCamera_18)); }
	inline float get_maximumRotationDegreesCamera_18() const { return ___maximumRotationDegreesCamera_18; }
	inline float* get_address_of_maximumRotationDegreesCamera_18() { return &___maximumRotationDegreesCamera_18; }
	inline void set_maximumRotationDegreesCamera_18(float value)
	{
		___maximumRotationDegreesCamera_18 = value;
	}

	inline static int32_t get_offset_of_maximumRotationDegreesPointer_19() { return static_cast<int32_t>(offsetof(FloatTile_t2669707442, ___maximumRotationDegreesPointer_19)); }
	inline float get_maximumRotationDegreesPointer_19() const { return ___maximumRotationDegreesPointer_19; }
	inline float* get_address_of_maximumRotationDegreesPointer_19() { return &___maximumRotationDegreesPointer_19; }
	inline void set_maximumRotationDegreesPointer_19(float value)
	{
		___maximumRotationDegreesPointer_19 = value;
	}

	inline static int32_t get_offset_of_interpolationSpeed_20() { return static_cast<int32_t>(offsetof(FloatTile_t2669707442, ___interpolationSpeed_20)); }
	inline float get_interpolationSpeed_20() const { return ___interpolationSpeed_20; }
	inline float* get_address_of_interpolationSpeed_20() { return &___interpolationSpeed_20; }
	inline void set_interpolationSpeed_20(float value)
	{
		___interpolationSpeed_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATTILE_T2669707442_H
#ifndef MASKEDTILE_T1953876158_H
#define MASKEDTILE_T1953876158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaskedTile
struct  MaskedTile_t1953876158  : public BaseTile_t770030199
{
public:
	// UnityEngine.UI.Image MaskedTile::maskedImage
	Image_t2670269651 * ___maskedImage_12;
	// UnityEngine.GameObject MaskedTile::maskedImageObject
	GameObject_t1113636619 * ___maskedImageObject_13;
	// UnityEngine.Vector3 MaskedTile::originalMaskedPosition
	Vector3_t3722313464  ___originalMaskedPosition_14;
	// UnityEngine.Vector3 MaskedTile::maskedScrollOffset
	Vector3_t3722313464  ___maskedScrollOffset_15;
	// UnityEngine.Vector2 MaskedTile::originalImageSize
	Vector2_t2156229523  ___originalImageSize_16;
	// UnityEngine.Vector2 MaskedTile::enlargedImageSize
	Vector2_t2156229523  ___enlargedImageSize_17;
	// System.Single MaskedTile::desiredPositionZ
	float ___desiredPositionZ_18;
	// System.Single MaskedTile::movementWeight
	float ___movementWeight_19;
	// System.Single MaskedTile::scaleWeight
	float ___scaleWeight_20;
	// System.Single MaskedTile::hoverPositionZMeters
	float ___hoverPositionZMeters_21;
	// System.Single MaskedTile::interpolationSpeed
	float ___interpolationSpeed_22;

public:
	inline static int32_t get_offset_of_maskedImage_12() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___maskedImage_12)); }
	inline Image_t2670269651 * get_maskedImage_12() const { return ___maskedImage_12; }
	inline Image_t2670269651 ** get_address_of_maskedImage_12() { return &___maskedImage_12; }
	inline void set_maskedImage_12(Image_t2670269651 * value)
	{
		___maskedImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___maskedImage_12), value);
	}

	inline static int32_t get_offset_of_maskedImageObject_13() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___maskedImageObject_13)); }
	inline GameObject_t1113636619 * get_maskedImageObject_13() const { return ___maskedImageObject_13; }
	inline GameObject_t1113636619 ** get_address_of_maskedImageObject_13() { return &___maskedImageObject_13; }
	inline void set_maskedImageObject_13(GameObject_t1113636619 * value)
	{
		___maskedImageObject_13 = value;
		Il2CppCodeGenWriteBarrier((&___maskedImageObject_13), value);
	}

	inline static int32_t get_offset_of_originalMaskedPosition_14() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___originalMaskedPosition_14)); }
	inline Vector3_t3722313464  get_originalMaskedPosition_14() const { return ___originalMaskedPosition_14; }
	inline Vector3_t3722313464 * get_address_of_originalMaskedPosition_14() { return &___originalMaskedPosition_14; }
	inline void set_originalMaskedPosition_14(Vector3_t3722313464  value)
	{
		___originalMaskedPosition_14 = value;
	}

	inline static int32_t get_offset_of_maskedScrollOffset_15() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___maskedScrollOffset_15)); }
	inline Vector3_t3722313464  get_maskedScrollOffset_15() const { return ___maskedScrollOffset_15; }
	inline Vector3_t3722313464 * get_address_of_maskedScrollOffset_15() { return &___maskedScrollOffset_15; }
	inline void set_maskedScrollOffset_15(Vector3_t3722313464  value)
	{
		___maskedScrollOffset_15 = value;
	}

	inline static int32_t get_offset_of_originalImageSize_16() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___originalImageSize_16)); }
	inline Vector2_t2156229523  get_originalImageSize_16() const { return ___originalImageSize_16; }
	inline Vector2_t2156229523 * get_address_of_originalImageSize_16() { return &___originalImageSize_16; }
	inline void set_originalImageSize_16(Vector2_t2156229523  value)
	{
		___originalImageSize_16 = value;
	}

	inline static int32_t get_offset_of_enlargedImageSize_17() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___enlargedImageSize_17)); }
	inline Vector2_t2156229523  get_enlargedImageSize_17() const { return ___enlargedImageSize_17; }
	inline Vector2_t2156229523 * get_address_of_enlargedImageSize_17() { return &___enlargedImageSize_17; }
	inline void set_enlargedImageSize_17(Vector2_t2156229523  value)
	{
		___enlargedImageSize_17 = value;
	}

	inline static int32_t get_offset_of_desiredPositionZ_18() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___desiredPositionZ_18)); }
	inline float get_desiredPositionZ_18() const { return ___desiredPositionZ_18; }
	inline float* get_address_of_desiredPositionZ_18() { return &___desiredPositionZ_18; }
	inline void set_desiredPositionZ_18(float value)
	{
		___desiredPositionZ_18 = value;
	}

	inline static int32_t get_offset_of_movementWeight_19() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___movementWeight_19)); }
	inline float get_movementWeight_19() const { return ___movementWeight_19; }
	inline float* get_address_of_movementWeight_19() { return &___movementWeight_19; }
	inline void set_movementWeight_19(float value)
	{
		___movementWeight_19 = value;
	}

	inline static int32_t get_offset_of_scaleWeight_20() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___scaleWeight_20)); }
	inline float get_scaleWeight_20() const { return ___scaleWeight_20; }
	inline float* get_address_of_scaleWeight_20() { return &___scaleWeight_20; }
	inline void set_scaleWeight_20(float value)
	{
		___scaleWeight_20 = value;
	}

	inline static int32_t get_offset_of_hoverPositionZMeters_21() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___hoverPositionZMeters_21)); }
	inline float get_hoverPositionZMeters_21() const { return ___hoverPositionZMeters_21; }
	inline float* get_address_of_hoverPositionZMeters_21() { return &___hoverPositionZMeters_21; }
	inline void set_hoverPositionZMeters_21(float value)
	{
		___hoverPositionZMeters_21 = value;
	}

	inline static int32_t get_offset_of_interpolationSpeed_22() { return static_cast<int32_t>(offsetof(MaskedTile_t1953876158, ___interpolationSpeed_22)); }
	inline float get_interpolationSpeed_22() const { return ___interpolationSpeed_22; }
	inline float* get_address_of_interpolationSpeed_22() { return &___interpolationSpeed_22; }
	inline void set_interpolationSpeed_22(float value)
	{
		___interpolationSpeed_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKEDTILE_T1953876158_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef SCROLLBAR_T1494447233_H
#define SCROLLBAR_T1494447233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar
struct  Scrollbar_t1494447233  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_HandleRect
	RectTransform_t3704657025 * ___m_HandleRect_16;
	// UnityEngine.UI.Scrollbar/Direction UnityEngine.UI.Scrollbar::m_Direction
	int32_t ___m_Direction_17;
	// System.Single UnityEngine.UI.Scrollbar::m_Value
	float ___m_Value_18;
	// System.Single UnityEngine.UI.Scrollbar::m_Size
	float ___m_Size_19;
	// System.Int32 UnityEngine.UI.Scrollbar::m_NumberOfSteps
	int32_t ___m_NumberOfSteps_20;
	// UnityEngine.UI.Scrollbar/ScrollEvent UnityEngine.UI.Scrollbar::m_OnValueChanged
	ScrollEvent_t149898510 * ___m_OnValueChanged_21;
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_ContainerRect
	RectTransform_t3704657025 * ___m_ContainerRect_22;
	// UnityEngine.Vector2 UnityEngine.UI.Scrollbar::m_Offset
	Vector2_t2156229523  ___m_Offset_23;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Scrollbar::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_24;
	// UnityEngine.Coroutine UnityEngine.UI.Scrollbar::m_PointerDownRepeat
	Coroutine_t3829159415 * ___m_PointerDownRepeat_25;
	// System.Boolean UnityEngine.UI.Scrollbar::isPointerDownAndNotDragging
	bool ___isPointerDownAndNotDragging_26;

public:
	inline static int32_t get_offset_of_m_HandleRect_16() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_HandleRect_16)); }
	inline RectTransform_t3704657025 * get_m_HandleRect_16() const { return ___m_HandleRect_16; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleRect_16() { return &___m_HandleRect_16; }
	inline void set_m_HandleRect_16(RectTransform_t3704657025 * value)
	{
		___m_HandleRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_16), value);
	}

	inline static int32_t get_offset_of_m_Direction_17() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Direction_17)); }
	inline int32_t get_m_Direction_17() const { return ___m_Direction_17; }
	inline int32_t* get_address_of_m_Direction_17() { return &___m_Direction_17; }
	inline void set_m_Direction_17(int32_t value)
	{
		___m_Direction_17 = value;
	}

	inline static int32_t get_offset_of_m_Value_18() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Value_18)); }
	inline float get_m_Value_18() const { return ___m_Value_18; }
	inline float* get_address_of_m_Value_18() { return &___m_Value_18; }
	inline void set_m_Value_18(float value)
	{
		___m_Value_18 = value;
	}

	inline static int32_t get_offset_of_m_Size_19() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Size_19)); }
	inline float get_m_Size_19() const { return ___m_Size_19; }
	inline float* get_address_of_m_Size_19() { return &___m_Size_19; }
	inline void set_m_Size_19(float value)
	{
		___m_Size_19 = value;
	}

	inline static int32_t get_offset_of_m_NumberOfSteps_20() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_NumberOfSteps_20)); }
	inline int32_t get_m_NumberOfSteps_20() const { return ___m_NumberOfSteps_20; }
	inline int32_t* get_address_of_m_NumberOfSteps_20() { return &___m_NumberOfSteps_20; }
	inline void set_m_NumberOfSteps_20(int32_t value)
	{
		___m_NumberOfSteps_20 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_21() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_OnValueChanged_21)); }
	inline ScrollEvent_t149898510 * get_m_OnValueChanged_21() const { return ___m_OnValueChanged_21; }
	inline ScrollEvent_t149898510 ** get_address_of_m_OnValueChanged_21() { return &___m_OnValueChanged_21; }
	inline void set_m_OnValueChanged_21(ScrollEvent_t149898510 * value)
	{
		___m_OnValueChanged_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_21), value);
	}

	inline static int32_t get_offset_of_m_ContainerRect_22() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_ContainerRect_22)); }
	inline RectTransform_t3704657025 * get_m_ContainerRect_22() const { return ___m_ContainerRect_22; }
	inline RectTransform_t3704657025 ** get_address_of_m_ContainerRect_22() { return &___m_ContainerRect_22; }
	inline void set_m_ContainerRect_22(RectTransform_t3704657025 * value)
	{
		___m_ContainerRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ContainerRect_22), value);
	}

	inline static int32_t get_offset_of_m_Offset_23() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Offset_23)); }
	inline Vector2_t2156229523  get_m_Offset_23() const { return ___m_Offset_23; }
	inline Vector2_t2156229523 * get_address_of_m_Offset_23() { return &___m_Offset_23; }
	inline void set_m_Offset_23(Vector2_t2156229523  value)
	{
		___m_Offset_23 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_24() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Tracker_24)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_24() const { return ___m_Tracker_24; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_24() { return &___m_Tracker_24; }
	inline void set_m_Tracker_24(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_24 = value;
	}

	inline static int32_t get_offset_of_m_PointerDownRepeat_25() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_PointerDownRepeat_25)); }
	inline Coroutine_t3829159415 * get_m_PointerDownRepeat_25() const { return ___m_PointerDownRepeat_25; }
	inline Coroutine_t3829159415 ** get_address_of_m_PointerDownRepeat_25() { return &___m_PointerDownRepeat_25; }
	inline void set_m_PointerDownRepeat_25(Coroutine_t3829159415 * value)
	{
		___m_PointerDownRepeat_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerDownRepeat_25), value);
	}

	inline static int32_t get_offset_of_isPointerDownAndNotDragging_26() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___isPointerDownAndNotDragging_26)); }
	inline bool get_isPointerDownAndNotDragging_26() const { return ___isPointerDownAndNotDragging_26; }
	inline bool* get_address_of_isPointerDownAndNotDragging_26() { return &___isPointerDownAndNotDragging_26; }
	inline void set_isPointerDownAndNotDragging_26(bool value)
	{
		___isPointerDownAndNotDragging_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBAR_T1494447233_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef PAGEDSCROLLBAR_T4001695667_H
#define PAGEDSCROLLBAR_T4001695667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PagedScrollBar
struct  PagedScrollBar_t4001695667  : public Scrollbar_t1494447233
{
public:
	// PagedScrollRect PagedScrollBar::pagedScrollRect
	PagedScrollRect_t3286523126 * ___pagedScrollRect_28;
	// System.Boolean PagedScrollBar::isDragging
	bool ___isDragging_29;

public:
	inline static int32_t get_offset_of_pagedScrollRect_28() { return static_cast<int32_t>(offsetof(PagedScrollBar_t4001695667, ___pagedScrollRect_28)); }
	inline PagedScrollRect_t3286523126 * get_pagedScrollRect_28() const { return ___pagedScrollRect_28; }
	inline PagedScrollRect_t3286523126 ** get_address_of_pagedScrollRect_28() { return &___pagedScrollRect_28; }
	inline void set_pagedScrollRect_28(PagedScrollRect_t3286523126 * value)
	{
		___pagedScrollRect_28 = value;
		Il2CppCodeGenWriteBarrier((&___pagedScrollRect_28), value);
	}

	inline static int32_t get_offset_of_isDragging_29() { return static_cast<int32_t>(offsetof(PagedScrollBar_t4001695667, ___isDragging_29)); }
	inline bool get_isDragging_29() const { return ___isDragging_29; }
	inline bool* get_address_of_isDragging_29() { return &___isDragging_29; }
	inline void set_isDragging_29(bool value)
	{
		___isDragging_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGEDSCROLLBAR_T4001695667_H
#ifndef TMP_SELECTIONCARET_T407257285_H
#define TMP_SELECTIONCARET_T407257285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SelectionCaret
struct  TMP_SelectionCaret_t407257285  : public MaskableGraphic_t3839221559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SELECTIONCARET_T407257285_H
#ifndef TMP_SUBMESHUI_T1578871311_H
#define TMP_SUBMESHUI_T1578871311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMeshUI
struct  TMP_SubMeshUI_t1578871311  : public MaskableGraphic_t3839221559
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMeshUI::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_28;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMeshUI::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_29;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_material
	Material_t340375123 * ___m_material_30;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_31;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackMaterial
	Material_t340375123 * ___m_fallbackMaterial_32;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackSourceMaterial
	Material_t340375123 * ___m_fallbackSourceMaterial_33;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_34;
	// System.Single TMPro.TMP_SubMeshUI::m_padding
	float ___m_padding_35;
	// UnityEngine.CanvasRenderer TMPro.TMP_SubMeshUI::m_canvasRenderer
	CanvasRenderer_t2598313366 * ___m_canvasRenderer_36;
	// UnityEngine.Mesh TMPro.TMP_SubMeshUI::m_mesh
	Mesh_t3648964284 * ___m_mesh_37;
	// TMPro.TextMeshProUGUI TMPro.TMP_SubMeshUI::m_TextComponent
	TextMeshProUGUI_t529313277 * ___m_TextComponent_38;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_39;
	// System.Boolean TMPro.TMP_SubMeshUI::m_materialDirty
	bool ___m_materialDirty_40;
	// System.Int32 TMPro.TMP_SubMeshUI::m_materialReferenceIndex
	int32_t ___m_materialReferenceIndex_41;

public:
	inline static int32_t get_offset_of_m_fontAsset_28() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_fontAsset_28)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_28() const { return ___m_fontAsset_28; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_28() { return &___m_fontAsset_28; }
	inline void set_m_fontAsset_28(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_28), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_29() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_spriteAsset_29)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_29() const { return ___m_spriteAsset_29; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_29() { return &___m_spriteAsset_29; }
	inline void set_m_spriteAsset_29(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_29), value);
	}

	inline static int32_t get_offset_of_m_material_30() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_material_30)); }
	inline Material_t340375123 * get_m_material_30() const { return ___m_material_30; }
	inline Material_t340375123 ** get_address_of_m_material_30() { return &___m_material_30; }
	inline void set_m_material_30(Material_t340375123 * value)
	{
		___m_material_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_30), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_31() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_sharedMaterial_31)); }
	inline Material_t340375123 * get_m_sharedMaterial_31() const { return ___m_sharedMaterial_31; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_31() { return &___m_sharedMaterial_31; }
	inline void set_m_sharedMaterial_31(Material_t340375123 * value)
	{
		___m_sharedMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_31), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_32() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_fallbackMaterial_32)); }
	inline Material_t340375123 * get_m_fallbackMaterial_32() const { return ___m_fallbackMaterial_32; }
	inline Material_t340375123 ** get_address_of_m_fallbackMaterial_32() { return &___m_fallbackMaterial_32; }
	inline void set_m_fallbackMaterial_32(Material_t340375123 * value)
	{
		___m_fallbackMaterial_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_32), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_33() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_fallbackSourceMaterial_33)); }
	inline Material_t340375123 * get_m_fallbackSourceMaterial_33() const { return ___m_fallbackSourceMaterial_33; }
	inline Material_t340375123 ** get_address_of_m_fallbackSourceMaterial_33() { return &___m_fallbackSourceMaterial_33; }
	inline void set_m_fallbackSourceMaterial_33(Material_t340375123 * value)
	{
		___m_fallbackSourceMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_34() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_isDefaultMaterial_34)); }
	inline bool get_m_isDefaultMaterial_34() const { return ___m_isDefaultMaterial_34; }
	inline bool* get_address_of_m_isDefaultMaterial_34() { return &___m_isDefaultMaterial_34; }
	inline void set_m_isDefaultMaterial_34(bool value)
	{
		___m_isDefaultMaterial_34 = value;
	}

	inline static int32_t get_offset_of_m_padding_35() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_padding_35)); }
	inline float get_m_padding_35() const { return ___m_padding_35; }
	inline float* get_address_of_m_padding_35() { return &___m_padding_35; }
	inline void set_m_padding_35(float value)
	{
		___m_padding_35 = value;
	}

	inline static int32_t get_offset_of_m_canvasRenderer_36() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_canvasRenderer_36)); }
	inline CanvasRenderer_t2598313366 * get_m_canvasRenderer_36() const { return ___m_canvasRenderer_36; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_canvasRenderer_36() { return &___m_canvasRenderer_36; }
	inline void set_m_canvasRenderer_36(CanvasRenderer_t2598313366 * value)
	{
		___m_canvasRenderer_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_36), value);
	}

	inline static int32_t get_offset_of_m_mesh_37() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_mesh_37)); }
	inline Mesh_t3648964284 * get_m_mesh_37() const { return ___m_mesh_37; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_37() { return &___m_mesh_37; }
	inline void set_m_mesh_37(Mesh_t3648964284 * value)
	{
		___m_mesh_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_37), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_38() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_TextComponent_38)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextComponent_38() const { return ___m_TextComponent_38; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextComponent_38() { return &___m_TextComponent_38; }
	inline void set_m_TextComponent_38(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextComponent_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_38), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_39() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_isRegisteredForEvents_39)); }
	inline bool get_m_isRegisteredForEvents_39() const { return ___m_isRegisteredForEvents_39; }
	inline bool* get_address_of_m_isRegisteredForEvents_39() { return &___m_isRegisteredForEvents_39; }
	inline void set_m_isRegisteredForEvents_39(bool value)
	{
		___m_isRegisteredForEvents_39 = value;
	}

	inline static int32_t get_offset_of_m_materialDirty_40() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_materialDirty_40)); }
	inline bool get_m_materialDirty_40() const { return ___m_materialDirty_40; }
	inline bool* get_address_of_m_materialDirty_40() { return &___m_materialDirty_40; }
	inline void set_m_materialDirty_40(bool value)
	{
		___m_materialDirty_40 = value;
	}

	inline static int32_t get_offset_of_m_materialReferenceIndex_41() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1578871311, ___m_materialReferenceIndex_41)); }
	inline int32_t get_m_materialReferenceIndex_41() const { return ___m_materialReferenceIndex_41; }
	inline int32_t* get_address_of_m_materialReferenceIndex_41() { return &___m_materialReferenceIndex_41; }
	inline void set_m_materialReferenceIndex_41(int32_t value)
	{
		___m_materialReferenceIndex_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESHUI_T1578871311_H
#ifndef TMP_TEXT_T2599618874_H
#define TMP_TEXT_T2599618874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t2599618874  : public MaskableGraphic_t3839221559
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_28;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_29;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_30;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t364381626 * ___m_currentFontAsset_31;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_32;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_33;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t340375123 * ___m_currentMaterial_34;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t648826345* ___m_materialReferences_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t1839659084 * ___m_materialReferenceIndexLookup_36;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___m_materialReferenceStack_37;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_38;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t561872642* ___m_fontSharedMaterials_39;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t340375123 * ___m_fontMaterial_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t561872642* ___m_fontMaterials_41;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_42;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t2600501292  ___m_fontColor32_43;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t2555686324  ___m_fontColor_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t2600501292  ___m_underlineColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t2600501292  ___m_strikethroughColor_47;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t2600501292  ___m_highlightColor_48;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_49;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t345148380  ___m_fontColorGradient_50;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_fontColorGradientPreset_51;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_52;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_53;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_54;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t2600501292  ___m_spriteColor_55;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t2600501292  ___m_faceColor_57;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t2600501292  ___m_outlineColor_58;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_59;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_60;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_61;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_62;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t960921318  ___m_sizeStack_63;
	// System.Int32 TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_64;
	// System.Int32 TMPro.TMP_Text::m_fontWeightInternal
	int32_t ___m_fontWeightInternal_65;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___m_fontWeightStack_66;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_67;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_68;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_69;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_70;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_71;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_72;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_73;
	// TMPro.TMP_BasicXmlTagStack TMPro.TMP_Text::m_fontStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___m_fontStyleStack_74;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_75;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_76;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_77;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___m_lineJustificationStack_78;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t1718750761* ___m_textContainerLocalCorners_79;
	// System.Boolean TMPro.TMP_Text::m_isAlignmentEnumConverted
	bool ___m_isAlignmentEnumConverted_80;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_81;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_82;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_83;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_84;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_85;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_86;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_87;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_88;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_89;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_90;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_91;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_92;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_93;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_94;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_95;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_96;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_97;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_98;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t2599618874 * ___m_linkedTextComponent_99;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_100;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_101;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_102;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_103;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_104;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_105;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_106;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_107;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_108;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_109;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_110;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_111;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_112;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_113;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_114;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_115;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_116;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_117;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_118;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_119;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_120;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_121;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_122;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_123;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t3319028937  ___m_margin_124;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_125;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_126;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_127;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_128;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_129;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t3598145122 * ___m_textInfo_130;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_131;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_132;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_t3600365921 * ___m_transform_133;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t3704657025 * ___m_rectTransform_134;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_135;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_136;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t3648964284 * ___m_mesh_137;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_138;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2836635477 * ___m_spriteAnimator_139;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_140;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_141;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_142;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_143;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_144;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_145;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_t1785403678 * ___m_LayoutElement_146;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_147;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_148;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_149;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_150;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_151;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_152;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_153;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_154;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_155;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_156;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_157;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_158;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_159;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_160;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_161;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_162;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_163;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_164;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_165;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t3528271667* ___m_htmlTag_166;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_t284240280* ___m_xmlAttribute_167;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t1444911251* ___m_attributeParameterValues_168;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_169;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_170;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t960921318  ___m_indentStack_171;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_172;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_173;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t1817901843  ___m_FXMatrix_174;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_175;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t385246372* ___m_char_buffer_176;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___m_internalCharacterInfo_177;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t3528271667* ___m_input_CharArray_178;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_179;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_180;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t341939652  ___m_SavedWordWrapState_181;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t341939652  ___m_SavedLineState_182;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_183;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_184;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_185;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_186;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_187;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_188;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_189;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_190;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_191;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_192;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_193;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_194;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_195;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_196;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_197;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_t3837212874  ___m_meshExtents_198;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t2600501292  ___m_htmlColor_199;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t2164155836  ___m_colorStack_200;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_underlineColorStack_201;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_strikethroughColorStack_202;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_highlightColorStack_203;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_colorGradientPreset_204;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___m_colorGradientStack_205;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_206;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_207;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2514600297  ___m_styleStack_208;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_XmlTagStack_1_t2514600297  ___m_actionStack_209;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_210;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_211;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_XmlTagStack_1_t960921318  ___m_baselineOffsetStack_212;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_213;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_214;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t129727469 * ___m_cached_TextElement_215;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Underline_GlyphInfo_216;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Ellipsis_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Ellipsis_GlyphInfo_217;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_218;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_currentSpriteAsset_219;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_220;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_221;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_222;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_223;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_t1444911251* ___k_Power_224;

public:
	inline static int32_t get_offset_of_m_text_28() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_text_28)); }
	inline String_t* get_m_text_28() const { return ___m_text_28; }
	inline String_t** get_address_of_m_text_28() { return &___m_text_28; }
	inline void set_m_text_28(String_t* value)
	{
		___m_text_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_28), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_29() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRightToLeft_29)); }
	inline bool get_m_isRightToLeft_29() const { return ___m_isRightToLeft_29; }
	inline bool* get_address_of_m_isRightToLeft_29() { return &___m_isRightToLeft_29; }
	inline void set_m_isRightToLeft_29(bool value)
	{
		___m_isRightToLeft_29 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_30() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontAsset_30)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_30() const { return ___m_fontAsset_30; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_30() { return &___m_fontAsset_30; }
	inline void set_m_fontAsset_30(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_30), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_31() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontAsset_31)); }
	inline TMP_FontAsset_t364381626 * get_m_currentFontAsset_31() const { return ___m_currentFontAsset_31; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_currentFontAsset_31() { return &___m_currentFontAsset_31; }
	inline void set_m_currentFontAsset_31(TMP_FontAsset_t364381626 * value)
	{
		___m_currentFontAsset_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_31), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_32() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isSDFShader_32)); }
	inline bool get_m_isSDFShader_32() const { return ___m_isSDFShader_32; }
	inline bool* get_address_of_m_isSDFShader_32() { return &___m_isSDFShader_32; }
	inline void set_m_isSDFShader_32(bool value)
	{
		___m_isSDFShader_32 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_33() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sharedMaterial_33)); }
	inline Material_t340375123 * get_m_sharedMaterial_33() const { return ___m_sharedMaterial_33; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_33() { return &___m_sharedMaterial_33; }
	inline void set_m_sharedMaterial_33(Material_t340375123 * value)
	{
		___m_sharedMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_34() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterial_34)); }
	inline Material_t340375123 * get_m_currentMaterial_34() const { return ___m_currentMaterial_34; }
	inline Material_t340375123 ** get_address_of_m_currentMaterial_34() { return &___m_currentMaterial_34; }
	inline void set_m_currentMaterial_34(Material_t340375123 * value)
	{
		___m_currentMaterial_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_34), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_35() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferences_35)); }
	inline MaterialReferenceU5BU5D_t648826345* get_m_materialReferences_35() const { return ___m_materialReferences_35; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_m_materialReferences_35() { return &___m_materialReferences_35; }
	inline void set_m_materialReferences_35(MaterialReferenceU5BU5D_t648826345* value)
	{
		___m_materialReferences_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_35), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_36() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceIndexLookup_36)); }
	inline Dictionary_2_t1839659084 * get_m_materialReferenceIndexLookup_36() const { return ___m_materialReferenceIndexLookup_36; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_materialReferenceIndexLookup_36() { return &___m_materialReferenceIndexLookup_36; }
	inline void set_m_materialReferenceIndexLookup_36(Dictionary_2_t1839659084 * value)
	{
		___m_materialReferenceIndexLookup_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_37() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceStack_37)); }
	inline TMP_XmlTagStack_1_t1515999176  get_m_materialReferenceStack_37() const { return ___m_materialReferenceStack_37; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_m_materialReferenceStack_37() { return &___m_materialReferenceStack_37; }
	inline void set_m_materialReferenceStack_37(TMP_XmlTagStack_1_t1515999176  value)
	{
		___m_materialReferenceStack_37 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_38() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterialIndex_38)); }
	inline int32_t get_m_currentMaterialIndex_38() const { return ___m_currentMaterialIndex_38; }
	inline int32_t* get_address_of_m_currentMaterialIndex_38() { return &___m_currentMaterialIndex_38; }
	inline void set_m_currentMaterialIndex_38(int32_t value)
	{
		___m_currentMaterialIndex_38 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_39() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSharedMaterials_39)); }
	inline MaterialU5BU5D_t561872642* get_m_fontSharedMaterials_39() const { return ___m_fontSharedMaterials_39; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontSharedMaterials_39() { return &___m_fontSharedMaterials_39; }
	inline void set_m_fontSharedMaterials_39(MaterialU5BU5D_t561872642* value)
	{
		___m_fontSharedMaterials_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_39), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_40() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterial_40)); }
	inline Material_t340375123 * get_m_fontMaterial_40() const { return ___m_fontMaterial_40; }
	inline Material_t340375123 ** get_address_of_m_fontMaterial_40() { return &___m_fontMaterial_40; }
	inline void set_m_fontMaterial_40(Material_t340375123 * value)
	{
		___m_fontMaterial_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_40), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterials_41)); }
	inline MaterialU5BU5D_t561872642* get_m_fontMaterials_41() const { return ___m_fontMaterials_41; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontMaterials_41() { return &___m_fontMaterials_41; }
	inline void set_m_fontMaterials_41(MaterialU5BU5D_t561872642* value)
	{
		___m_fontMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_42() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isMaterialDirty_42)); }
	inline bool get_m_isMaterialDirty_42() const { return ___m_isMaterialDirty_42; }
	inline bool* get_address_of_m_isMaterialDirty_42() { return &___m_isMaterialDirty_42; }
	inline void set_m_isMaterialDirty_42(bool value)
	{
		___m_isMaterialDirty_42 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_43() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor32_43)); }
	inline Color32_t2600501292  get_m_fontColor32_43() const { return ___m_fontColor32_43; }
	inline Color32_t2600501292 * get_address_of_m_fontColor32_43() { return &___m_fontColor32_43; }
	inline void set_m_fontColor32_43(Color32_t2600501292  value)
	{
		___m_fontColor32_43 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_44() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor_44)); }
	inline Color_t2555686324  get_m_fontColor_44() const { return ___m_fontColor_44; }
	inline Color_t2555686324 * get_address_of_m_fontColor_44() { return &___m_fontColor_44; }
	inline void set_m_fontColor_44(Color_t2555686324  value)
	{
		___m_fontColor_44 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColor_46)); }
	inline Color32_t2600501292  get_m_underlineColor_46() const { return ___m_underlineColor_46; }
	inline Color32_t2600501292 * get_address_of_m_underlineColor_46() { return &___m_underlineColor_46; }
	inline void set_m_underlineColor_46(Color32_t2600501292  value)
	{
		___m_underlineColor_46 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_47() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColor_47)); }
	inline Color32_t2600501292  get_m_strikethroughColor_47() const { return ___m_strikethroughColor_47; }
	inline Color32_t2600501292 * get_address_of_m_strikethroughColor_47() { return &___m_strikethroughColor_47; }
	inline void set_m_strikethroughColor_47(Color32_t2600501292  value)
	{
		___m_strikethroughColor_47 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColor_48)); }
	inline Color32_t2600501292  get_m_highlightColor_48() const { return ___m_highlightColor_48; }
	inline Color32_t2600501292 * get_address_of_m_highlightColor_48() { return &___m_highlightColor_48; }
	inline void set_m_highlightColor_48(Color32_t2600501292  value)
	{
		___m_highlightColor_48 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_49() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableVertexGradient_49)); }
	inline bool get_m_enableVertexGradient_49() const { return ___m_enableVertexGradient_49; }
	inline bool* get_address_of_m_enableVertexGradient_49() { return &___m_enableVertexGradient_49; }
	inline void set_m_enableVertexGradient_49(bool value)
	{
		___m_enableVertexGradient_49 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_50() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradient_50)); }
	inline VertexGradient_t345148380  get_m_fontColorGradient_50() const { return ___m_fontColorGradient_50; }
	inline VertexGradient_t345148380 * get_address_of_m_fontColorGradient_50() { return &___m_fontColorGradient_50; }
	inline void set_m_fontColorGradient_50(VertexGradient_t345148380  value)
	{
		___m_fontColorGradient_50 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_51() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradientPreset_51)); }
	inline TMP_ColorGradient_t3678055768 * get_m_fontColorGradientPreset_51() const { return ___m_fontColorGradientPreset_51; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_fontColorGradientPreset_51() { return &___m_fontColorGradientPreset_51; }
	inline void set_m_fontColorGradientPreset_51(TMP_ColorGradient_t3678055768 * value)
	{
		___m_fontColorGradientPreset_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_51), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_52() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAsset_52)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_52() const { return ___m_spriteAsset_52; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_52() { return &___m_spriteAsset_52; }
	inline void set_m_spriteAsset_52(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_52), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_53() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintAllSprites_53)); }
	inline bool get_m_tintAllSprites_53() const { return ___m_tintAllSprites_53; }
	inline bool* get_address_of_m_tintAllSprites_53() { return &___m_tintAllSprites_53; }
	inline void set_m_tintAllSprites_53(bool value)
	{
		___m_tintAllSprites_53 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_54() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintSprite_54)); }
	inline bool get_m_tintSprite_54() const { return ___m_tintSprite_54; }
	inline bool* get_address_of_m_tintSprite_54() { return &___m_tintSprite_54; }
	inline void set_m_tintSprite_54(bool value)
	{
		___m_tintSprite_54 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_55() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteColor_55)); }
	inline Color32_t2600501292  get_m_spriteColor_55() const { return ___m_spriteColor_55; }
	inline Color32_t2600501292 * get_address_of_m_spriteColor_55() { return &___m_spriteColor_55; }
	inline void set_m_spriteColor_55(Color32_t2600501292  value)
	{
		___m_spriteColor_55 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_56() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overrideHtmlColors_56)); }
	inline bool get_m_overrideHtmlColors_56() const { return ___m_overrideHtmlColors_56; }
	inline bool* get_address_of_m_overrideHtmlColors_56() { return &___m_overrideHtmlColors_56; }
	inline void set_m_overrideHtmlColors_56(bool value)
	{
		___m_overrideHtmlColors_56 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_57() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_faceColor_57)); }
	inline Color32_t2600501292  get_m_faceColor_57() const { return ___m_faceColor_57; }
	inline Color32_t2600501292 * get_address_of_m_faceColor_57() { return &___m_faceColor_57; }
	inline void set_m_faceColor_57(Color32_t2600501292  value)
	{
		___m_faceColor_57 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_58() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineColor_58)); }
	inline Color32_t2600501292  get_m_outlineColor_58() const { return ___m_outlineColor_58; }
	inline Color32_t2600501292 * get_address_of_m_outlineColor_58() { return &___m_outlineColor_58; }
	inline void set_m_outlineColor_58(Color32_t2600501292  value)
	{
		___m_outlineColor_58 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_59() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineWidth_59)); }
	inline float get_m_outlineWidth_59() const { return ___m_outlineWidth_59; }
	inline float* get_address_of_m_outlineWidth_59() { return &___m_outlineWidth_59; }
	inline void set_m_outlineWidth_59(float value)
	{
		___m_outlineWidth_59 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_60() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSize_60)); }
	inline float get_m_fontSize_60() const { return ___m_fontSize_60; }
	inline float* get_address_of_m_fontSize_60() { return &___m_fontSize_60; }
	inline void set_m_fontSize_60(float value)
	{
		___m_fontSize_60 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_61() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontSize_61)); }
	inline float get_m_currentFontSize_61() const { return ___m_currentFontSize_61; }
	inline float* get_address_of_m_currentFontSize_61() { return &___m_currentFontSize_61; }
	inline void set_m_currentFontSize_61(float value)
	{
		___m_currentFontSize_61 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_62() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeBase_62)); }
	inline float get_m_fontSizeBase_62() const { return ___m_fontSizeBase_62; }
	inline float* get_address_of_m_fontSizeBase_62() { return &___m_fontSizeBase_62; }
	inline void set_m_fontSizeBase_62(float value)
	{
		___m_fontSizeBase_62 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_63() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sizeStack_63)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_sizeStack_63() const { return ___m_sizeStack_63; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_sizeStack_63() { return &___m_sizeStack_63; }
	inline void set_m_sizeStack_63(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_sizeStack_63 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_64() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeight_64)); }
	inline int32_t get_m_fontWeight_64() const { return ___m_fontWeight_64; }
	inline int32_t* get_address_of_m_fontWeight_64() { return &___m_fontWeight_64; }
	inline void set_m_fontWeight_64(int32_t value)
	{
		___m_fontWeight_64 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightInternal_65() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightInternal_65)); }
	inline int32_t get_m_fontWeightInternal_65() const { return ___m_fontWeightInternal_65; }
	inline int32_t* get_address_of_m_fontWeightInternal_65() { return &___m_fontWeightInternal_65; }
	inline void set_m_fontWeightInternal_65(int32_t value)
	{
		___m_fontWeightInternal_65 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightStack_66() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightStack_66)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_fontWeightStack_66() const { return ___m_fontWeightStack_66; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_fontWeightStack_66() { return &___m_fontWeightStack_66; }
	inline void set_m_fontWeightStack_66(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_fontWeightStack_66 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_67() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableAutoSizing_67)); }
	inline bool get_m_enableAutoSizing_67() const { return ___m_enableAutoSizing_67; }
	inline bool* get_address_of_m_enableAutoSizing_67() { return &___m_enableAutoSizing_67; }
	inline void set_m_enableAutoSizing_67(bool value)
	{
		___m_enableAutoSizing_67 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_68() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxFontSize_68)); }
	inline float get_m_maxFontSize_68() const { return ___m_maxFontSize_68; }
	inline float* get_address_of_m_maxFontSize_68() { return &___m_maxFontSize_68; }
	inline void set_m_maxFontSize_68(float value)
	{
		___m_maxFontSize_68 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_69() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minFontSize_69)); }
	inline float get_m_minFontSize_69() const { return ___m_minFontSize_69; }
	inline float* get_address_of_m_minFontSize_69() { return &___m_minFontSize_69; }
	inline void set_m_minFontSize_69(float value)
	{
		___m_minFontSize_69 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_70() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMin_70)); }
	inline float get_m_fontSizeMin_70() const { return ___m_fontSizeMin_70; }
	inline float* get_address_of_m_fontSizeMin_70() { return &___m_fontSizeMin_70; }
	inline void set_m_fontSizeMin_70(float value)
	{
		___m_fontSizeMin_70 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_71() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMax_71)); }
	inline float get_m_fontSizeMax_71() const { return ___m_fontSizeMax_71; }
	inline float* get_address_of_m_fontSizeMax_71() { return &___m_fontSizeMax_71; }
	inline void set_m_fontSizeMax_71(float value)
	{
		___m_fontSizeMax_71 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_72() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyle_72)); }
	inline int32_t get_m_fontStyle_72() const { return ___m_fontStyle_72; }
	inline int32_t* get_address_of_m_fontStyle_72() { return &___m_fontStyle_72; }
	inline void set_m_fontStyle_72(int32_t value)
	{
		___m_fontStyle_72 = value;
	}

	inline static int32_t get_offset_of_m_style_73() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_style_73)); }
	inline int32_t get_m_style_73() const { return ___m_style_73; }
	inline int32_t* get_address_of_m_style_73() { return &___m_style_73; }
	inline void set_m_style_73(int32_t value)
	{
		___m_style_73 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_74() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyleStack_74)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_m_fontStyleStack_74() const { return ___m_fontStyleStack_74; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_m_fontStyleStack_74() { return &___m_fontStyleStack_74; }
	inline void set_m_fontStyleStack_74(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___m_fontStyleStack_74 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_75() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingBold_75)); }
	inline bool get_m_isUsingBold_75() const { return ___m_isUsingBold_75; }
	inline bool* get_address_of_m_isUsingBold_75() { return &___m_isUsingBold_75; }
	inline void set_m_isUsingBold_75(bool value)
	{
		___m_isUsingBold_75 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_76() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textAlignment_76)); }
	inline int32_t get_m_textAlignment_76() const { return ___m_textAlignment_76; }
	inline int32_t* get_address_of_m_textAlignment_76() { return &___m_textAlignment_76; }
	inline void set_m_textAlignment_76(int32_t value)
	{
		___m_textAlignment_76 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_77() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustification_77)); }
	inline int32_t get_m_lineJustification_77() const { return ___m_lineJustification_77; }
	inline int32_t* get_address_of_m_lineJustification_77() { return &___m_lineJustification_77; }
	inline void set_m_lineJustification_77(int32_t value)
	{
		___m_lineJustification_77 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_78() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustificationStack_78)); }
	inline TMP_XmlTagStack_1_t3600445780  get_m_lineJustificationStack_78() const { return ___m_lineJustificationStack_78; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_m_lineJustificationStack_78() { return &___m_lineJustificationStack_78; }
	inline void set_m_lineJustificationStack_78(TMP_XmlTagStack_1_t3600445780  value)
	{
		___m_lineJustificationStack_78 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_79() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textContainerLocalCorners_79)); }
	inline Vector3U5BU5D_t1718750761* get_m_textContainerLocalCorners_79() const { return ___m_textContainerLocalCorners_79; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_textContainerLocalCorners_79() { return &___m_textContainerLocalCorners_79; }
	inline void set_m_textContainerLocalCorners_79(Vector3U5BU5D_t1718750761* value)
	{
		___m_textContainerLocalCorners_79 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_79), value);
	}

	inline static int32_t get_offset_of_m_isAlignmentEnumConverted_80() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAlignmentEnumConverted_80)); }
	inline bool get_m_isAlignmentEnumConverted_80() const { return ___m_isAlignmentEnumConverted_80; }
	inline bool* get_address_of_m_isAlignmentEnumConverted_80() { return &___m_isAlignmentEnumConverted_80; }
	inline void set_m_isAlignmentEnumConverted_80(bool value)
	{
		___m_isAlignmentEnumConverted_80 = value;
	}

	inline static int32_t get_offset_of_m_characterSpacing_81() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterSpacing_81)); }
	inline float get_m_characterSpacing_81() const { return ___m_characterSpacing_81; }
	inline float* get_address_of_m_characterSpacing_81() { return &___m_characterSpacing_81; }
	inline void set_m_characterSpacing_81(float value)
	{
		___m_characterSpacing_81 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_82() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cSpacing_82)); }
	inline float get_m_cSpacing_82() const { return ___m_cSpacing_82; }
	inline float* get_address_of_m_cSpacing_82() { return &___m_cSpacing_82; }
	inline void set_m_cSpacing_82(float value)
	{
		___m_cSpacing_82 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_83() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_monoSpacing_83)); }
	inline float get_m_monoSpacing_83() const { return ___m_monoSpacing_83; }
	inline float* get_address_of_m_monoSpacing_83() { return &___m_monoSpacing_83; }
	inline void set_m_monoSpacing_83(float value)
	{
		___m_monoSpacing_83 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordSpacing_84)); }
	inline float get_m_wordSpacing_84() const { return ___m_wordSpacing_84; }
	inline float* get_address_of_m_wordSpacing_84() { return &___m_wordSpacing_84; }
	inline void set_m_wordSpacing_84(float value)
	{
		___m_wordSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacing_85)); }
	inline float get_m_lineSpacing_85() const { return ___m_lineSpacing_85; }
	inline float* get_address_of_m_lineSpacing_85() { return &___m_lineSpacing_85; }
	inline void set_m_lineSpacing_85(float value)
	{
		___m_lineSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_86() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingDelta_86)); }
	inline float get_m_lineSpacingDelta_86() const { return ___m_lineSpacingDelta_86; }
	inline float* get_address_of_m_lineSpacingDelta_86() { return &___m_lineSpacingDelta_86; }
	inline void set_m_lineSpacingDelta_86(float value)
	{
		___m_lineSpacingDelta_86 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_87() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineHeight_87)); }
	inline float get_m_lineHeight_87() const { return ___m_lineHeight_87; }
	inline float* get_address_of_m_lineHeight_87() { return &___m_lineHeight_87; }
	inline void set_m_lineHeight_87(float value)
	{
		___m_lineHeight_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_88() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingMax_88)); }
	inline float get_m_lineSpacingMax_88() const { return ___m_lineSpacingMax_88; }
	inline float* get_address_of_m_lineSpacingMax_88() { return &___m_lineSpacingMax_88; }
	inline void set_m_lineSpacingMax_88(float value)
	{
		___m_lineSpacingMax_88 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_89() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_paragraphSpacing_89)); }
	inline float get_m_paragraphSpacing_89() const { return ___m_paragraphSpacing_89; }
	inline float* get_address_of_m_paragraphSpacing_89() { return &___m_paragraphSpacing_89; }
	inline void set_m_paragraphSpacing_89(float value)
	{
		___m_paragraphSpacing_89 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_90() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthMaxAdj_90)); }
	inline float get_m_charWidthMaxAdj_90() const { return ___m_charWidthMaxAdj_90; }
	inline float* get_address_of_m_charWidthMaxAdj_90() { return &___m_charWidthMaxAdj_90; }
	inline void set_m_charWidthMaxAdj_90(float value)
	{
		___m_charWidthMaxAdj_90 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_91() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthAdjDelta_91)); }
	inline float get_m_charWidthAdjDelta_91() const { return ___m_charWidthAdjDelta_91; }
	inline float* get_address_of_m_charWidthAdjDelta_91() { return &___m_charWidthAdjDelta_91; }
	inline void set_m_charWidthAdjDelta_91(float value)
	{
		___m_charWidthAdjDelta_91 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_92() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableWordWrapping_92)); }
	inline bool get_m_enableWordWrapping_92() const { return ___m_enableWordWrapping_92; }
	inline bool* get_address_of_m_enableWordWrapping_92() { return &___m_enableWordWrapping_92; }
	inline void set_m_enableWordWrapping_92(bool value)
	{
		___m_enableWordWrapping_92 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_93() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCharacterWrappingEnabled_93)); }
	inline bool get_m_isCharacterWrappingEnabled_93() const { return ___m_isCharacterWrappingEnabled_93; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_93() { return &___m_isCharacterWrappingEnabled_93; }
	inline void set_m_isCharacterWrappingEnabled_93(bool value)
	{
		___m_isCharacterWrappingEnabled_93 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_94() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNonBreakingSpace_94)); }
	inline bool get_m_isNonBreakingSpace_94() const { return ___m_isNonBreakingSpace_94; }
	inline bool* get_address_of_m_isNonBreakingSpace_94() { return &___m_isNonBreakingSpace_94; }
	inline void set_m_isNonBreakingSpace_94(bool value)
	{
		___m_isNonBreakingSpace_94 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_95() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isIgnoringAlignment_95)); }
	inline bool get_m_isIgnoringAlignment_95() const { return ___m_isIgnoringAlignment_95; }
	inline bool* get_address_of_m_isIgnoringAlignment_95() { return &___m_isIgnoringAlignment_95; }
	inline void set_m_isIgnoringAlignment_95(bool value)
	{
		___m_isIgnoringAlignment_95 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_96() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordWrappingRatios_96)); }
	inline float get_m_wordWrappingRatios_96() const { return ___m_wordWrappingRatios_96; }
	inline float* get_address_of_m_wordWrappingRatios_96() { return &___m_wordWrappingRatios_96; }
	inline void set_m_wordWrappingRatios_96(float value)
	{
		___m_wordWrappingRatios_96 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_97() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overflowMode_97)); }
	inline int32_t get_m_overflowMode_97() const { return ___m_overflowMode_97; }
	inline int32_t* get_address_of_m_overflowMode_97() { return &___m_overflowMode_97; }
	inline void set_m_overflowMode_97(int32_t value)
	{
		___m_overflowMode_97 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_98() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstOverflowCharacterIndex_98)); }
	inline int32_t get_m_firstOverflowCharacterIndex_98() const { return ___m_firstOverflowCharacterIndex_98; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_98() { return &___m_firstOverflowCharacterIndex_98; }
	inline void set_m_firstOverflowCharacterIndex_98(int32_t value)
	{
		___m_firstOverflowCharacterIndex_98 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_99() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_linkedTextComponent_99)); }
	inline TMP_Text_t2599618874 * get_m_linkedTextComponent_99() const { return ___m_linkedTextComponent_99; }
	inline TMP_Text_t2599618874 ** get_address_of_m_linkedTextComponent_99() { return &___m_linkedTextComponent_99; }
	inline void set_m_linkedTextComponent_99(TMP_Text_t2599618874 * value)
	{
		___m_linkedTextComponent_99 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_99), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_100() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLinkedTextComponent_100)); }
	inline bool get_m_isLinkedTextComponent_100() const { return ___m_isLinkedTextComponent_100; }
	inline bool* get_address_of_m_isLinkedTextComponent_100() { return &___m_isLinkedTextComponent_100; }
	inline void set_m_isLinkedTextComponent_100(bool value)
	{
		___m_isLinkedTextComponent_100 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_101() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isTextTruncated_101)); }
	inline bool get_m_isTextTruncated_101() const { return ___m_isTextTruncated_101; }
	inline bool* get_address_of_m_isTextTruncated_101() { return &___m_isTextTruncated_101; }
	inline void set_m_isTextTruncated_101(bool value)
	{
		___m_isTextTruncated_101 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_102() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableKerning_102)); }
	inline bool get_m_enableKerning_102() const { return ___m_enableKerning_102; }
	inline bool* get_address_of_m_enableKerning_102() { return &___m_enableKerning_102; }
	inline void set_m_enableKerning_102(bool value)
	{
		___m_enableKerning_102 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_103() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableExtraPadding_103)); }
	inline bool get_m_enableExtraPadding_103() const { return ___m_enableExtraPadding_103; }
	inline bool* get_address_of_m_enableExtraPadding_103() { return &___m_enableExtraPadding_103; }
	inline void set_m_enableExtraPadding_103(bool value)
	{
		___m_enableExtraPadding_103 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_104() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___checkPaddingRequired_104)); }
	inline bool get_checkPaddingRequired_104() const { return ___checkPaddingRequired_104; }
	inline bool* get_address_of_checkPaddingRequired_104() { return &___checkPaddingRequired_104; }
	inline void set_checkPaddingRequired_104(bool value)
	{
		___checkPaddingRequired_104 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_105() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRichText_105)); }
	inline bool get_m_isRichText_105() const { return ___m_isRichText_105; }
	inline bool* get_address_of_m_isRichText_105() { return &___m_isRichText_105; }
	inline void set_m_isRichText_105(bool value)
	{
		___m_isRichText_105 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_106() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_parseCtrlCharacters_106)); }
	inline bool get_m_parseCtrlCharacters_106() const { return ___m_parseCtrlCharacters_106; }
	inline bool* get_address_of_m_parseCtrlCharacters_106() { return &___m_parseCtrlCharacters_106; }
	inline void set_m_parseCtrlCharacters_106(bool value)
	{
		___m_parseCtrlCharacters_106 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_107() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOverlay_107)); }
	inline bool get_m_isOverlay_107() const { return ___m_isOverlay_107; }
	inline bool* get_address_of_m_isOverlay_107() { return &___m_isOverlay_107; }
	inline void set_m_isOverlay_107(bool value)
	{
		___m_isOverlay_107 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_108() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOrthographic_108)); }
	inline bool get_m_isOrthographic_108() const { return ___m_isOrthographic_108; }
	inline bool* get_address_of_m_isOrthographic_108() { return &___m_isOrthographic_108; }
	inline void set_m_isOrthographic_108(bool value)
	{
		___m_isOrthographic_108 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_109() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCullingEnabled_109)); }
	inline bool get_m_isCullingEnabled_109() const { return ___m_isCullingEnabled_109; }
	inline bool* get_address_of_m_isCullingEnabled_109() { return &___m_isCullingEnabled_109; }
	inline void set_m_isCullingEnabled_109(bool value)
	{
		___m_isCullingEnabled_109 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_110() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreRectMaskCulling_110)); }
	inline bool get_m_ignoreRectMaskCulling_110() const { return ___m_ignoreRectMaskCulling_110; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_110() { return &___m_ignoreRectMaskCulling_110; }
	inline void set_m_ignoreRectMaskCulling_110(bool value)
	{
		___m_ignoreRectMaskCulling_110 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_111() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreCulling_111)); }
	inline bool get_m_ignoreCulling_111() const { return ___m_ignoreCulling_111; }
	inline bool* get_address_of_m_ignoreCulling_111() { return &___m_ignoreCulling_111; }
	inline void set_m_ignoreCulling_111(bool value)
	{
		___m_ignoreCulling_111 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_112() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_horizontalMapping_112)); }
	inline int32_t get_m_horizontalMapping_112() const { return ___m_horizontalMapping_112; }
	inline int32_t* get_address_of_m_horizontalMapping_112() { return &___m_horizontalMapping_112; }
	inline void set_m_horizontalMapping_112(int32_t value)
	{
		___m_horizontalMapping_112 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_113() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticalMapping_113)); }
	inline int32_t get_m_verticalMapping_113() const { return ___m_verticalMapping_113; }
	inline int32_t* get_address_of_m_verticalMapping_113() { return &___m_verticalMapping_113; }
	inline void set_m_verticalMapping_113(int32_t value)
	{
		___m_verticalMapping_113 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_114() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_uvLineOffset_114)); }
	inline float get_m_uvLineOffset_114() const { return ___m_uvLineOffset_114; }
	inline float* get_address_of_m_uvLineOffset_114() { return &___m_uvLineOffset_114; }
	inline void set_m_uvLineOffset_114(float value)
	{
		___m_uvLineOffset_114 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_115() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderMode_115)); }
	inline int32_t get_m_renderMode_115() const { return ___m_renderMode_115; }
	inline int32_t* get_address_of_m_renderMode_115() { return &___m_renderMode_115; }
	inline void set_m_renderMode_115(int32_t value)
	{
		___m_renderMode_115 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_116() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_geometrySortingOrder_116)); }
	inline int32_t get_m_geometrySortingOrder_116() const { return ___m_geometrySortingOrder_116; }
	inline int32_t* get_address_of_m_geometrySortingOrder_116() { return &___m_geometrySortingOrder_116; }
	inline void set_m_geometrySortingOrder_116(int32_t value)
	{
		___m_geometrySortingOrder_116 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_117() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacter_117)); }
	inline int32_t get_m_firstVisibleCharacter_117() const { return ___m_firstVisibleCharacter_117; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_117() { return &___m_firstVisibleCharacter_117; }
	inline void set_m_firstVisibleCharacter_117(int32_t value)
	{
		___m_firstVisibleCharacter_117 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_118() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleCharacters_118)); }
	inline int32_t get_m_maxVisibleCharacters_118() const { return ___m_maxVisibleCharacters_118; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_118() { return &___m_maxVisibleCharacters_118; }
	inline void set_m_maxVisibleCharacters_118(int32_t value)
	{
		___m_maxVisibleCharacters_118 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_119() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleWords_119)); }
	inline int32_t get_m_maxVisibleWords_119() const { return ___m_maxVisibleWords_119; }
	inline int32_t* get_address_of_m_maxVisibleWords_119() { return &___m_maxVisibleWords_119; }
	inline void set_m_maxVisibleWords_119(int32_t value)
	{
		___m_maxVisibleWords_119 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_120() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleLines_120)); }
	inline int32_t get_m_maxVisibleLines_120() const { return ___m_maxVisibleLines_120; }
	inline int32_t* get_address_of_m_maxVisibleLines_120() { return &___m_maxVisibleLines_120; }
	inline void set_m_maxVisibleLines_120(int32_t value)
	{
		___m_maxVisibleLines_120 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_121() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_useMaxVisibleDescender_121)); }
	inline bool get_m_useMaxVisibleDescender_121() const { return ___m_useMaxVisibleDescender_121; }
	inline bool* get_address_of_m_useMaxVisibleDescender_121() { return &___m_useMaxVisibleDescender_121; }
	inline void set_m_useMaxVisibleDescender_121(bool value)
	{
		___m_useMaxVisibleDescender_121 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_122() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageToDisplay_122)); }
	inline int32_t get_m_pageToDisplay_122() const { return ___m_pageToDisplay_122; }
	inline int32_t* get_address_of_m_pageToDisplay_122() { return &___m_pageToDisplay_122; }
	inline void set_m_pageToDisplay_122(int32_t value)
	{
		___m_pageToDisplay_122 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_123() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNewPage_123)); }
	inline bool get_m_isNewPage_123() const { return ___m_isNewPage_123; }
	inline bool* get_address_of_m_isNewPage_123() { return &___m_isNewPage_123; }
	inline void set_m_isNewPage_123(bool value)
	{
		___m_isNewPage_123 = value;
	}

	inline static int32_t get_offset_of_m_margin_124() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_margin_124)); }
	inline Vector4_t3319028937  get_m_margin_124() const { return ___m_margin_124; }
	inline Vector4_t3319028937 * get_address_of_m_margin_124() { return &___m_margin_124; }
	inline void set_m_margin_124(Vector4_t3319028937  value)
	{
		___m_margin_124 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_125() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginLeft_125)); }
	inline float get_m_marginLeft_125() const { return ___m_marginLeft_125; }
	inline float* get_address_of_m_marginLeft_125() { return &___m_marginLeft_125; }
	inline void set_m_marginLeft_125(float value)
	{
		___m_marginLeft_125 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_126() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginRight_126)); }
	inline float get_m_marginRight_126() const { return ___m_marginRight_126; }
	inline float* get_address_of_m_marginRight_126() { return &___m_marginRight_126; }
	inline void set_m_marginRight_126(float value)
	{
		___m_marginRight_126 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_127() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginWidth_127)); }
	inline float get_m_marginWidth_127() const { return ___m_marginWidth_127; }
	inline float* get_address_of_m_marginWidth_127() { return &___m_marginWidth_127; }
	inline void set_m_marginWidth_127(float value)
	{
		___m_marginWidth_127 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_128() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginHeight_128)); }
	inline float get_m_marginHeight_128() const { return ___m_marginHeight_128; }
	inline float* get_address_of_m_marginHeight_128() { return &___m_marginHeight_128; }
	inline void set_m_marginHeight_128(float value)
	{
		___m_marginHeight_128 = value;
	}

	inline static int32_t get_offset_of_m_width_129() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_width_129)); }
	inline float get_m_width_129() const { return ___m_width_129; }
	inline float* get_address_of_m_width_129() { return &___m_width_129; }
	inline void set_m_width_129(float value)
	{
		___m_width_129 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_130() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textInfo_130)); }
	inline TMP_TextInfo_t3598145122 * get_m_textInfo_130() const { return ___m_textInfo_130; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_m_textInfo_130() { return &___m_textInfo_130; }
	inline void set_m_textInfo_130(TMP_TextInfo_t3598145122 * value)
	{
		___m_textInfo_130 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_130), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_131() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_havePropertiesChanged_131)); }
	inline bool get_m_havePropertiesChanged_131() const { return ___m_havePropertiesChanged_131; }
	inline bool* get_address_of_m_havePropertiesChanged_131() { return &___m_havePropertiesChanged_131; }
	inline void set_m_havePropertiesChanged_131(bool value)
	{
		___m_havePropertiesChanged_131 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_132() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingLegacyAnimationComponent_132)); }
	inline bool get_m_isUsingLegacyAnimationComponent_132() const { return ___m_isUsingLegacyAnimationComponent_132; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_132() { return &___m_isUsingLegacyAnimationComponent_132; }
	inline void set_m_isUsingLegacyAnimationComponent_132(bool value)
	{
		___m_isUsingLegacyAnimationComponent_132 = value;
	}

	inline static int32_t get_offset_of_m_transform_133() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_transform_133)); }
	inline Transform_t3600365921 * get_m_transform_133() const { return ___m_transform_133; }
	inline Transform_t3600365921 ** get_address_of_m_transform_133() { return &___m_transform_133; }
	inline void set_m_transform_133(Transform_t3600365921 * value)
	{
		___m_transform_133 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_133), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_134() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_rectTransform_134)); }
	inline RectTransform_t3704657025 * get_m_rectTransform_134() const { return ___m_rectTransform_134; }
	inline RectTransform_t3704657025 ** get_address_of_m_rectTransform_134() { return &___m_rectTransform_134; }
	inline void set_m_rectTransform_134(RectTransform_t3704657025 * value)
	{
		___m_rectTransform_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_134), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___U3CautoSizeTextContainerU3Ek__BackingField_135)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_135() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return &___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_135(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_135 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_136() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_autoSizeTextContainer_136)); }
	inline bool get_m_autoSizeTextContainer_136() const { return ___m_autoSizeTextContainer_136; }
	inline bool* get_address_of_m_autoSizeTextContainer_136() { return &___m_autoSizeTextContainer_136; }
	inline void set_m_autoSizeTextContainer_136(bool value)
	{
		___m_autoSizeTextContainer_136 = value;
	}

	inline static int32_t get_offset_of_m_mesh_137() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_mesh_137)); }
	inline Mesh_t3648964284 * get_m_mesh_137() const { return ___m_mesh_137; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_137() { return &___m_mesh_137; }
	inline void set_m_mesh_137(Mesh_t3648964284 * value)
	{
		___m_mesh_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_137), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_138() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isVolumetricText_138)); }
	inline bool get_m_isVolumetricText_138() const { return ___m_isVolumetricText_138; }
	inline bool* get_address_of_m_isVolumetricText_138() { return &___m_isVolumetricText_138; }
	inline void set_m_isVolumetricText_138(bool value)
	{
		___m_isVolumetricText_138 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_139() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimator_139)); }
	inline TMP_SpriteAnimator_t2836635477 * get_m_spriteAnimator_139() const { return ___m_spriteAnimator_139; }
	inline TMP_SpriteAnimator_t2836635477 ** get_address_of_m_spriteAnimator_139() { return &___m_spriteAnimator_139; }
	inline void set_m_spriteAnimator_139(TMP_SpriteAnimator_t2836635477 * value)
	{
		___m_spriteAnimator_139 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_139), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_140() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleHeight_140)); }
	inline float get_m_flexibleHeight_140() const { return ___m_flexibleHeight_140; }
	inline float* get_address_of_m_flexibleHeight_140() { return &___m_flexibleHeight_140; }
	inline void set_m_flexibleHeight_140(float value)
	{
		___m_flexibleHeight_140 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_141() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleWidth_141)); }
	inline float get_m_flexibleWidth_141() const { return ___m_flexibleWidth_141; }
	inline float* get_address_of_m_flexibleWidth_141() { return &___m_flexibleWidth_141; }
	inline void set_m_flexibleWidth_141(float value)
	{
		___m_flexibleWidth_141 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_142() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minWidth_142)); }
	inline float get_m_minWidth_142() const { return ___m_minWidth_142; }
	inline float* get_address_of_m_minWidth_142() { return &___m_minWidth_142; }
	inline void set_m_minWidth_142(float value)
	{
		___m_minWidth_142 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_143() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minHeight_143)); }
	inline float get_m_minHeight_143() const { return ___m_minHeight_143; }
	inline float* get_address_of_m_minHeight_143() { return &___m_minHeight_143; }
	inline void set_m_minHeight_143(float value)
	{
		___m_minHeight_143 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_144() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxWidth_144)); }
	inline float get_m_maxWidth_144() const { return ___m_maxWidth_144; }
	inline float* get_address_of_m_maxWidth_144() { return &___m_maxWidth_144; }
	inline void set_m_maxWidth_144(float value)
	{
		___m_maxWidth_144 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_145() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxHeight_145)); }
	inline float get_m_maxHeight_145() const { return ___m_maxHeight_145; }
	inline float* get_address_of_m_maxHeight_145() { return &___m_maxHeight_145; }
	inline void set_m_maxHeight_145(float value)
	{
		___m_maxHeight_145 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_146() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_LayoutElement_146)); }
	inline LayoutElement_t1785403678 * get_m_LayoutElement_146() const { return ___m_LayoutElement_146; }
	inline LayoutElement_t1785403678 ** get_address_of_m_LayoutElement_146() { return &___m_LayoutElement_146; }
	inline void set_m_LayoutElement_146(LayoutElement_t1785403678 * value)
	{
		___m_LayoutElement_146 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_146), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_147() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredWidth_147)); }
	inline float get_m_preferredWidth_147() const { return ___m_preferredWidth_147; }
	inline float* get_address_of_m_preferredWidth_147() { return &___m_preferredWidth_147; }
	inline void set_m_preferredWidth_147(float value)
	{
		___m_preferredWidth_147 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_148() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedWidth_148)); }
	inline float get_m_renderedWidth_148() const { return ___m_renderedWidth_148; }
	inline float* get_address_of_m_renderedWidth_148() { return &___m_renderedWidth_148; }
	inline void set_m_renderedWidth_148(float value)
	{
		___m_renderedWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_149() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredWidthDirty_149)); }
	inline bool get_m_isPreferredWidthDirty_149() const { return ___m_isPreferredWidthDirty_149; }
	inline bool* get_address_of_m_isPreferredWidthDirty_149() { return &___m_isPreferredWidthDirty_149; }
	inline void set_m_isPreferredWidthDirty_149(bool value)
	{
		___m_isPreferredWidthDirty_149 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_150() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredHeight_150)); }
	inline float get_m_preferredHeight_150() const { return ___m_preferredHeight_150; }
	inline float* get_address_of_m_preferredHeight_150() { return &___m_preferredHeight_150; }
	inline void set_m_preferredHeight_150(float value)
	{
		___m_preferredHeight_150 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_151() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedHeight_151)); }
	inline float get_m_renderedHeight_151() const { return ___m_renderedHeight_151; }
	inline float* get_address_of_m_renderedHeight_151() { return &___m_renderedHeight_151; }
	inline void set_m_renderedHeight_151(float value)
	{
		___m_renderedHeight_151 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_152() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredHeightDirty_152)); }
	inline bool get_m_isPreferredHeightDirty_152() const { return ___m_isPreferredHeightDirty_152; }
	inline bool* get_address_of_m_isPreferredHeightDirty_152() { return &___m_isPreferredHeightDirty_152; }
	inline void set_m_isPreferredHeightDirty_152(bool value)
	{
		___m_isPreferredHeightDirty_152 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_153() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculatingPreferredValues_153)); }
	inline bool get_m_isCalculatingPreferredValues_153() const { return ___m_isCalculatingPreferredValues_153; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_153() { return &___m_isCalculatingPreferredValues_153; }
	inline void set_m_isCalculatingPreferredValues_153(bool value)
	{
		___m_isCalculatingPreferredValues_153 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_154() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_recursiveCount_154)); }
	inline int32_t get_m_recursiveCount_154() const { return ___m_recursiveCount_154; }
	inline int32_t* get_address_of_m_recursiveCount_154() { return &___m_recursiveCount_154; }
	inline void set_m_recursiveCount_154(int32_t value)
	{
		___m_recursiveCount_154 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_155() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutPriority_155)); }
	inline int32_t get_m_layoutPriority_155() const { return ___m_layoutPriority_155; }
	inline int32_t* get_address_of_m_layoutPriority_155() { return &___m_layoutPriority_155; }
	inline void set_m_layoutPriority_155(int32_t value)
	{
		___m_layoutPriority_155 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_156() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculateSizeRequired_156)); }
	inline bool get_m_isCalculateSizeRequired_156() const { return ___m_isCalculateSizeRequired_156; }
	inline bool* get_address_of_m_isCalculateSizeRequired_156() { return &___m_isCalculateSizeRequired_156; }
	inline void set_m_isCalculateSizeRequired_156(bool value)
	{
		___m_isCalculateSizeRequired_156 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_157() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLayoutDirty_157)); }
	inline bool get_m_isLayoutDirty_157() const { return ___m_isLayoutDirty_157; }
	inline bool* get_address_of_m_isLayoutDirty_157() { return &___m_isLayoutDirty_157; }
	inline void set_m_isLayoutDirty_157(bool value)
	{
		___m_isLayoutDirty_157 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_158() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticesAlreadyDirty_158)); }
	inline bool get_m_verticesAlreadyDirty_158() const { return ___m_verticesAlreadyDirty_158; }
	inline bool* get_address_of_m_verticesAlreadyDirty_158() { return &___m_verticesAlreadyDirty_158; }
	inline void set_m_verticesAlreadyDirty_158(bool value)
	{
		___m_verticesAlreadyDirty_158 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_159() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutAlreadyDirty_159)); }
	inline bool get_m_layoutAlreadyDirty_159() const { return ___m_layoutAlreadyDirty_159; }
	inline bool* get_address_of_m_layoutAlreadyDirty_159() { return &___m_layoutAlreadyDirty_159; }
	inline void set_m_layoutAlreadyDirty_159(bool value)
	{
		___m_layoutAlreadyDirty_159 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_160() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAwake_160)); }
	inline bool get_m_isAwake_160() const { return ___m_isAwake_160; }
	inline bool* get_address_of_m_isAwake_160() { return &___m_isAwake_160; }
	inline void set_m_isAwake_160(bool value)
	{
		___m_isAwake_160 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_161() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isInputParsingRequired_161)); }
	inline bool get_m_isInputParsingRequired_161() const { return ___m_isInputParsingRequired_161; }
	inline bool* get_address_of_m_isInputParsingRequired_161() { return &___m_isInputParsingRequired_161; }
	inline void set_m_isInputParsingRequired_161(bool value)
	{
		___m_isInputParsingRequired_161 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_162() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_inputSource_162)); }
	inline int32_t get_m_inputSource_162() const { return ___m_inputSource_162; }
	inline int32_t* get_address_of_m_inputSource_162() { return &___m_inputSource_162; }
	inline void set_m_inputSource_162(int32_t value)
	{
		___m_inputSource_162 = value;
	}

	inline static int32_t get_offset_of_old_text_163() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___old_text_163)); }
	inline String_t* get_old_text_163() const { return ___old_text_163; }
	inline String_t** get_address_of_old_text_163() { return &___old_text_163; }
	inline void set_old_text_163(String_t* value)
	{
		___old_text_163 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_163), value);
	}

	inline static int32_t get_offset_of_m_fontScale_164() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScale_164)); }
	inline float get_m_fontScale_164() const { return ___m_fontScale_164; }
	inline float* get_address_of_m_fontScale_164() { return &___m_fontScale_164; }
	inline void set_m_fontScale_164(float value)
	{
		___m_fontScale_164 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_165() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScaleMultiplier_165)); }
	inline float get_m_fontScaleMultiplier_165() const { return ___m_fontScaleMultiplier_165; }
	inline float* get_address_of_m_fontScaleMultiplier_165() { return &___m_fontScaleMultiplier_165; }
	inline void set_m_fontScaleMultiplier_165(float value)
	{
		___m_fontScaleMultiplier_165 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_166() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlTag_166)); }
	inline CharU5BU5D_t3528271667* get_m_htmlTag_166() const { return ___m_htmlTag_166; }
	inline CharU5BU5D_t3528271667** get_address_of_m_htmlTag_166() { return &___m_htmlTag_166; }
	inline void set_m_htmlTag_166(CharU5BU5D_t3528271667* value)
	{
		___m_htmlTag_166 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_166), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_167() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xmlAttribute_167)); }
	inline XML_TagAttributeU5BU5D_t284240280* get_m_xmlAttribute_167() const { return ___m_xmlAttribute_167; }
	inline XML_TagAttributeU5BU5D_t284240280** get_address_of_m_xmlAttribute_167() { return &___m_xmlAttribute_167; }
	inline void set_m_xmlAttribute_167(XML_TagAttributeU5BU5D_t284240280* value)
	{
		___m_xmlAttribute_167 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_167), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_168() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_attributeParameterValues_168)); }
	inline SingleU5BU5D_t1444911251* get_m_attributeParameterValues_168() const { return ___m_attributeParameterValues_168; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_attributeParameterValues_168() { return &___m_attributeParameterValues_168; }
	inline void set_m_attributeParameterValues_168(SingleU5BU5D_t1444911251* value)
	{
		___m_attributeParameterValues_168 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_168), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_169() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_LineIndent_169)); }
	inline float get_tag_LineIndent_169() const { return ___tag_LineIndent_169; }
	inline float* get_address_of_tag_LineIndent_169() { return &___tag_LineIndent_169; }
	inline void set_tag_LineIndent_169(float value)
	{
		___tag_LineIndent_169 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_170() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_Indent_170)); }
	inline float get_tag_Indent_170() const { return ___tag_Indent_170; }
	inline float* get_address_of_tag_Indent_170() { return &___tag_Indent_170; }
	inline void set_tag_Indent_170(float value)
	{
		___tag_Indent_170 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_171() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_indentStack_171)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_indentStack_171() const { return ___m_indentStack_171; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_indentStack_171() { return &___m_indentStack_171; }
	inline void set_m_indentStack_171(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_indentStack_171 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_172() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_NoParsing_172)); }
	inline bool get_tag_NoParsing_172() const { return ___tag_NoParsing_172; }
	inline bool* get_address_of_tag_NoParsing_172() { return &___tag_NoParsing_172; }
	inline void set_tag_NoParsing_172(bool value)
	{
		___tag_NoParsing_172 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_173() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isParsingText_173)); }
	inline bool get_m_isParsingText_173() const { return ___m_isParsingText_173; }
	inline bool* get_address_of_m_isParsingText_173() { return &___m_isParsingText_173; }
	inline void set_m_isParsingText_173(bool value)
	{
		___m_isParsingText_173 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_174() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_FXMatrix_174)); }
	inline Matrix4x4_t1817901843  get_m_FXMatrix_174() const { return ___m_FXMatrix_174; }
	inline Matrix4x4_t1817901843 * get_address_of_m_FXMatrix_174() { return &___m_FXMatrix_174; }
	inline void set_m_FXMatrix_174(Matrix4x4_t1817901843  value)
	{
		___m_FXMatrix_174 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_175() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isFXMatrixSet_175)); }
	inline bool get_m_isFXMatrixSet_175() const { return ___m_isFXMatrixSet_175; }
	inline bool* get_address_of_m_isFXMatrixSet_175() { return &___m_isFXMatrixSet_175; }
	inline void set_m_isFXMatrixSet_175(bool value)
	{
		___m_isFXMatrixSet_175 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_176() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_char_buffer_176)); }
	inline Int32U5BU5D_t385246372* get_m_char_buffer_176() const { return ___m_char_buffer_176; }
	inline Int32U5BU5D_t385246372** get_address_of_m_char_buffer_176() { return &___m_char_buffer_176; }
	inline void set_m_char_buffer_176(Int32U5BU5D_t385246372* value)
	{
		___m_char_buffer_176 = value;
		Il2CppCodeGenWriteBarrier((&___m_char_buffer_176), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_177() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_internalCharacterInfo_177)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_m_internalCharacterInfo_177() const { return ___m_internalCharacterInfo_177; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_m_internalCharacterInfo_177() { return &___m_internalCharacterInfo_177; }
	inline void set_m_internalCharacterInfo_177(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___m_internalCharacterInfo_177 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_177), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_178() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_input_CharArray_178)); }
	inline CharU5BU5D_t3528271667* get_m_input_CharArray_178() const { return ___m_input_CharArray_178; }
	inline CharU5BU5D_t3528271667** get_address_of_m_input_CharArray_178() { return &___m_input_CharArray_178; }
	inline void set_m_input_CharArray_178(CharU5BU5D_t3528271667* value)
	{
		___m_input_CharArray_178 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_178), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_179() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charArray_Length_179)); }
	inline int32_t get_m_charArray_Length_179() const { return ___m_charArray_Length_179; }
	inline int32_t* get_address_of_m_charArray_Length_179() { return &___m_charArray_Length_179; }
	inline void set_m_charArray_Length_179(int32_t value)
	{
		___m_charArray_Length_179 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_180() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_totalCharacterCount_180)); }
	inline int32_t get_m_totalCharacterCount_180() const { return ___m_totalCharacterCount_180; }
	inline int32_t* get_address_of_m_totalCharacterCount_180() { return &___m_totalCharacterCount_180; }
	inline void set_m_totalCharacterCount_180(int32_t value)
	{
		___m_totalCharacterCount_180 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_181() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedWordWrapState_181)); }
	inline WordWrapState_t341939652  get_m_SavedWordWrapState_181() const { return ___m_SavedWordWrapState_181; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedWordWrapState_181() { return &___m_SavedWordWrapState_181; }
	inline void set_m_SavedWordWrapState_181(WordWrapState_t341939652  value)
	{
		___m_SavedWordWrapState_181 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_182() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedLineState_182)); }
	inline WordWrapState_t341939652  get_m_SavedLineState_182() const { return ___m_SavedLineState_182; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedLineState_182() { return &___m_SavedLineState_182; }
	inline void set_m_SavedLineState_182(WordWrapState_t341939652  value)
	{
		___m_SavedLineState_182 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_183() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterCount_183)); }
	inline int32_t get_m_characterCount_183() const { return ___m_characterCount_183; }
	inline int32_t* get_address_of_m_characterCount_183() { return &___m_characterCount_183; }
	inline void set_m_characterCount_183(int32_t value)
	{
		___m_characterCount_183 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_184() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstCharacterOfLine_184)); }
	inline int32_t get_m_firstCharacterOfLine_184() const { return ___m_firstCharacterOfLine_184; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_184() { return &___m_firstCharacterOfLine_184; }
	inline void set_m_firstCharacterOfLine_184(int32_t value)
	{
		___m_firstCharacterOfLine_184 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_185() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacterOfLine_185)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_185() const { return ___m_firstVisibleCharacterOfLine_185; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_185() { return &___m_firstVisibleCharacterOfLine_185; }
	inline void set_m_firstVisibleCharacterOfLine_185(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_185 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_186() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastCharacterOfLine_186)); }
	inline int32_t get_m_lastCharacterOfLine_186() const { return ___m_lastCharacterOfLine_186; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_186() { return &___m_lastCharacterOfLine_186; }
	inline void set_m_lastCharacterOfLine_186(int32_t value)
	{
		___m_lastCharacterOfLine_186 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_187() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastVisibleCharacterOfLine_187)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_187() const { return ___m_lastVisibleCharacterOfLine_187; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_187() { return &___m_lastVisibleCharacterOfLine_187; }
	inline void set_m_lastVisibleCharacterOfLine_187(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_187 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_188() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineNumber_188)); }
	inline int32_t get_m_lineNumber_188() const { return ___m_lineNumber_188; }
	inline int32_t* get_address_of_m_lineNumber_188() { return &___m_lineNumber_188; }
	inline void set_m_lineNumber_188(int32_t value)
	{
		___m_lineNumber_188 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_189() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineVisibleCharacterCount_189)); }
	inline int32_t get_m_lineVisibleCharacterCount_189() const { return ___m_lineVisibleCharacterCount_189; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_189() { return &___m_lineVisibleCharacterCount_189; }
	inline void set_m_lineVisibleCharacterCount_189(int32_t value)
	{
		___m_lineVisibleCharacterCount_189 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_190() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageNumber_190)); }
	inline int32_t get_m_pageNumber_190() const { return ___m_pageNumber_190; }
	inline int32_t* get_address_of_m_pageNumber_190() { return &___m_pageNumber_190; }
	inline void set_m_pageNumber_190(int32_t value)
	{
		___m_pageNumber_190 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_191() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxAscender_191)); }
	inline float get_m_maxAscender_191() const { return ___m_maxAscender_191; }
	inline float* get_address_of_m_maxAscender_191() { return &___m_maxAscender_191; }
	inline void set_m_maxAscender_191(float value)
	{
		___m_maxAscender_191 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_192() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxCapHeight_192)); }
	inline float get_m_maxCapHeight_192() const { return ___m_maxCapHeight_192; }
	inline float* get_address_of_m_maxCapHeight_192() { return &___m_maxCapHeight_192; }
	inline void set_m_maxCapHeight_192(float value)
	{
		___m_maxCapHeight_192 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_193() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxDescender_193)); }
	inline float get_m_maxDescender_193() const { return ___m_maxDescender_193; }
	inline float* get_address_of_m_maxDescender_193() { return &___m_maxDescender_193; }
	inline void set_m_maxDescender_193(float value)
	{
		___m_maxDescender_193 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_194() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineAscender_194)); }
	inline float get_m_maxLineAscender_194() const { return ___m_maxLineAscender_194; }
	inline float* get_address_of_m_maxLineAscender_194() { return &___m_maxLineAscender_194; }
	inline void set_m_maxLineAscender_194(float value)
	{
		___m_maxLineAscender_194 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_195() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineDescender_195)); }
	inline float get_m_maxLineDescender_195() const { return ___m_maxLineDescender_195; }
	inline float* get_address_of_m_maxLineDescender_195() { return &___m_maxLineDescender_195; }
	inline void set_m_maxLineDescender_195(float value)
	{
		___m_maxLineDescender_195 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_startOfLineAscender_196)); }
	inline float get_m_startOfLineAscender_196() const { return ___m_startOfLineAscender_196; }
	inline float* get_address_of_m_startOfLineAscender_196() { return &___m_startOfLineAscender_196; }
	inline void set_m_startOfLineAscender_196(float value)
	{
		___m_startOfLineAscender_196 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_197() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineOffset_197)); }
	inline float get_m_lineOffset_197() const { return ___m_lineOffset_197; }
	inline float* get_address_of_m_lineOffset_197() { return &___m_lineOffset_197; }
	inline void set_m_lineOffset_197(float value)
	{
		___m_lineOffset_197 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_198() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_meshExtents_198)); }
	inline Extents_t3837212874  get_m_meshExtents_198() const { return ___m_meshExtents_198; }
	inline Extents_t3837212874 * get_address_of_m_meshExtents_198() { return &___m_meshExtents_198; }
	inline void set_m_meshExtents_198(Extents_t3837212874  value)
	{
		___m_meshExtents_198 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_199() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlColor_199)); }
	inline Color32_t2600501292  get_m_htmlColor_199() const { return ___m_htmlColor_199; }
	inline Color32_t2600501292 * get_address_of_m_htmlColor_199() { return &___m_htmlColor_199; }
	inline void set_m_htmlColor_199(Color32_t2600501292  value)
	{
		___m_htmlColor_199 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_200() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorStack_200)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_colorStack_200() const { return ___m_colorStack_200; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_colorStack_200() { return &___m_colorStack_200; }
	inline void set_m_colorStack_200(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_colorStack_200 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_201() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColorStack_201)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_underlineColorStack_201() const { return ___m_underlineColorStack_201; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_underlineColorStack_201() { return &___m_underlineColorStack_201; }
	inline void set_m_underlineColorStack_201(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_underlineColorStack_201 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_202() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColorStack_202)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_strikethroughColorStack_202() const { return ___m_strikethroughColorStack_202; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_strikethroughColorStack_202() { return &___m_strikethroughColorStack_202; }
	inline void set_m_strikethroughColorStack_202(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_strikethroughColorStack_202 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_203() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColorStack_203)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_highlightColorStack_203() const { return ___m_highlightColorStack_203; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_highlightColorStack_203() { return &___m_highlightColorStack_203; }
	inline void set_m_highlightColorStack_203(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_highlightColorStack_203 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_204() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientPreset_204)); }
	inline TMP_ColorGradient_t3678055768 * get_m_colorGradientPreset_204() const { return ___m_colorGradientPreset_204; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_colorGradientPreset_204() { return &___m_colorGradientPreset_204; }
	inline void set_m_colorGradientPreset_204(TMP_ColorGradient_t3678055768 * value)
	{
		___m_colorGradientPreset_204 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_204), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientStack_205)); }
	inline TMP_XmlTagStack_1_t3241710312  get_m_colorGradientStack_205() const { return ___m_colorGradientStack_205; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_m_colorGradientStack_205() { return &___m_colorGradientStack_205; }
	inline void set_m_colorGradientStack_205(TMP_XmlTagStack_1_t3241710312  value)
	{
		___m_colorGradientStack_205 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_206() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tabSpacing_206)); }
	inline float get_m_tabSpacing_206() const { return ___m_tabSpacing_206; }
	inline float* get_address_of_m_tabSpacing_206() { return &___m_tabSpacing_206; }
	inline void set_m_tabSpacing_206(float value)
	{
		___m_tabSpacing_206 = value;
	}

	inline static int32_t get_offset_of_m_spacing_207() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spacing_207)); }
	inline float get_m_spacing_207() const { return ___m_spacing_207; }
	inline float* get_address_of_m_spacing_207() { return &___m_spacing_207; }
	inline void set_m_spacing_207(float value)
	{
		___m_spacing_207 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_styleStack_208)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_styleStack_208() const { return ___m_styleStack_208; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_styleStack_208() { return &___m_styleStack_208; }
	inline void set_m_styleStack_208(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_styleStack_208 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_209() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_actionStack_209)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_actionStack_209() const { return ___m_actionStack_209; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_actionStack_209() { return &___m_actionStack_209; }
	inline void set_m_actionStack_209(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_actionStack_209 = value;
	}

	inline static int32_t get_offset_of_m_padding_210() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_padding_210)); }
	inline float get_m_padding_210() const { return ___m_padding_210; }
	inline float* get_address_of_m_padding_210() { return &___m_padding_210; }
	inline void set_m_padding_210(float value)
	{
		___m_padding_210 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_211() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffset_211)); }
	inline float get_m_baselineOffset_211() const { return ___m_baselineOffset_211; }
	inline float* get_address_of_m_baselineOffset_211() { return &___m_baselineOffset_211; }
	inline void set_m_baselineOffset_211(float value)
	{
		___m_baselineOffset_211 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_212() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffsetStack_212)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_baselineOffsetStack_212() const { return ___m_baselineOffsetStack_212; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_baselineOffsetStack_212() { return &___m_baselineOffsetStack_212; }
	inline void set_m_baselineOffsetStack_212(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_baselineOffsetStack_212 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_213() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xAdvance_213)); }
	inline float get_m_xAdvance_213() const { return ___m_xAdvance_213; }
	inline float* get_address_of_m_xAdvance_213() { return &___m_xAdvance_213; }
	inline void set_m_xAdvance_213(float value)
	{
		___m_xAdvance_213 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_214() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textElementType_214)); }
	inline int32_t get_m_textElementType_214() const { return ___m_textElementType_214; }
	inline int32_t* get_address_of_m_textElementType_214() { return &___m_textElementType_214; }
	inline void set_m_textElementType_214(int32_t value)
	{
		___m_textElementType_214 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_215() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_TextElement_215)); }
	inline TMP_TextElement_t129727469 * get_m_cached_TextElement_215() const { return ___m_cached_TextElement_215; }
	inline TMP_TextElement_t129727469 ** get_address_of_m_cached_TextElement_215() { return &___m_cached_TextElement_215; }
	inline void set_m_cached_TextElement_215(TMP_TextElement_t129727469 * value)
	{
		___m_cached_TextElement_215 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_215), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_216() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Underline_GlyphInfo_216)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Underline_GlyphInfo_216() const { return ___m_cached_Underline_GlyphInfo_216; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Underline_GlyphInfo_216() { return &___m_cached_Underline_GlyphInfo_216; }
	inline void set_m_cached_Underline_GlyphInfo_216(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Underline_GlyphInfo_216 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_GlyphInfo_216), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_GlyphInfo_217() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Ellipsis_GlyphInfo_217)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Ellipsis_GlyphInfo_217() const { return ___m_cached_Ellipsis_GlyphInfo_217; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Ellipsis_GlyphInfo_217() { return &___m_cached_Ellipsis_GlyphInfo_217; }
	inline void set_m_cached_Ellipsis_GlyphInfo_217(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Ellipsis_GlyphInfo_217 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_GlyphInfo_217), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_218() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_defaultSpriteAsset_218)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_218() const { return ___m_defaultSpriteAsset_218; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_218() { return &___m_defaultSpriteAsset_218; }
	inline void set_m_defaultSpriteAsset_218(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_218 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_218), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_219() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentSpriteAsset_219)); }
	inline TMP_SpriteAsset_t484820633 * get_m_currentSpriteAsset_219() const { return ___m_currentSpriteAsset_219; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_currentSpriteAsset_219() { return &___m_currentSpriteAsset_219; }
	inline void set_m_currentSpriteAsset_219(TMP_SpriteAsset_t484820633 * value)
	{
		___m_currentSpriteAsset_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_219), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_220() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteCount_220)); }
	inline int32_t get_m_spriteCount_220() const { return ___m_spriteCount_220; }
	inline int32_t* get_address_of_m_spriteCount_220() { return &___m_spriteCount_220; }
	inline void set_m_spriteCount_220(int32_t value)
	{
		___m_spriteCount_220 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_221() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteIndex_221)); }
	inline int32_t get_m_spriteIndex_221() const { return ___m_spriteIndex_221; }
	inline int32_t* get_address_of_m_spriteIndex_221() { return &___m_spriteIndex_221; }
	inline void set_m_spriteIndex_221(int32_t value)
	{
		___m_spriteIndex_221 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_222() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimationID_222)); }
	inline int32_t get_m_spriteAnimationID_222() const { return ___m_spriteAnimationID_222; }
	inline int32_t* get_address_of_m_spriteAnimationID_222() { return &___m_spriteAnimationID_222; }
	inline void set_m_spriteAnimationID_222(int32_t value)
	{
		___m_spriteAnimationID_222 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_223() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreActiveState_223)); }
	inline bool get_m_ignoreActiveState_223() const { return ___m_ignoreActiveState_223; }
	inline bool* get_address_of_m_ignoreActiveState_223() { return &___m_ignoreActiveState_223; }
	inline void set_m_ignoreActiveState_223(bool value)
	{
		___m_ignoreActiveState_223 = value;
	}

	inline static int32_t get_offset_of_k_Power_224() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___k_Power_224)); }
	inline SingleU5BU5D_t1444911251* get_k_Power_224() const { return ___k_Power_224; }
	inline SingleU5BU5D_t1444911251** get_address_of_k_Power_224() { return &___k_Power_224; }
	inline void set_k_Power_224(SingleU5BU5D_t1444911251* value)
	{
		___k_Power_224 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_224), value);
	}
};

struct TMP_Text_t2599618874_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t2600501292  ___s_colorWhite_45;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t2156229523  ___k_LargePositiveVector2_225;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t2156229523  ___k_LargeNegativeVector2_226;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_227;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_228;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_229;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_230;

public:
	inline static int32_t get_offset_of_s_colorWhite_45() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___s_colorWhite_45)); }
	inline Color32_t2600501292  get_s_colorWhite_45() const { return ___s_colorWhite_45; }
	inline Color32_t2600501292 * get_address_of_s_colorWhite_45() { return &___s_colorWhite_45; }
	inline void set_s_colorWhite_45(Color32_t2600501292  value)
	{
		___s_colorWhite_45 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_225() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveVector2_225)); }
	inline Vector2_t2156229523  get_k_LargePositiveVector2_225() const { return ___k_LargePositiveVector2_225; }
	inline Vector2_t2156229523 * get_address_of_k_LargePositiveVector2_225() { return &___k_LargePositiveVector2_225; }
	inline void set_k_LargePositiveVector2_225(Vector2_t2156229523  value)
	{
		___k_LargePositiveVector2_225 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_226() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeVector2_226)); }
	inline Vector2_t2156229523  get_k_LargeNegativeVector2_226() const { return ___k_LargeNegativeVector2_226; }
	inline Vector2_t2156229523 * get_address_of_k_LargeNegativeVector2_226() { return &___k_LargeNegativeVector2_226; }
	inline void set_k_LargeNegativeVector2_226(Vector2_t2156229523  value)
	{
		___k_LargeNegativeVector2_226 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_227() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveFloat_227)); }
	inline float get_k_LargePositiveFloat_227() const { return ___k_LargePositiveFloat_227; }
	inline float* get_address_of_k_LargePositiveFloat_227() { return &___k_LargePositiveFloat_227; }
	inline void set_k_LargePositiveFloat_227(float value)
	{
		___k_LargePositiveFloat_227 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_228() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeFloat_228)); }
	inline float get_k_LargeNegativeFloat_228() const { return ___k_LargeNegativeFloat_228; }
	inline float* get_address_of_k_LargeNegativeFloat_228() { return &___k_LargeNegativeFloat_228; }
	inline void set_k_LargeNegativeFloat_228(float value)
	{
		___k_LargeNegativeFloat_228 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_229() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveInt_229)); }
	inline int32_t get_k_LargePositiveInt_229() const { return ___k_LargePositiveInt_229; }
	inline int32_t* get_address_of_k_LargePositiveInt_229() { return &___k_LargePositiveInt_229; }
	inline void set_k_LargePositiveInt_229(int32_t value)
	{
		___k_LargePositiveInt_229 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_230() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeInt_230)); }
	inline int32_t get_k_LargeNegativeInt_230() const { return ___k_LargeNegativeInt_230; }
	inline int32_t* get_address_of_k_LargeNegativeInt_230() { return &___k_LargeNegativeInt_230; }
	inline void set_k_LargeNegativeInt_230(int32_t value)
	{
		___k_LargeNegativeInt_230 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T2599618874_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (LineType_t2817627283)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2300[4] = 
{
	LineType_t2817627283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (OnValidateInput_t373909109), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (SubmitEvent_t1343580625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (OnChangeEvent_t4257774360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (SelectionEvent_t4268942288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (TextSelectionEvent_t1023421581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (EditState_t2966235475)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2306[3] = 
{
	EditState_t2966235475::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (U3CCaretBlinkU3Ec__Iterator0_t4293627739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[6] = 
{
	U3CCaretBlinkU3Ec__Iterator0_t4293627739::get_offset_of_U3CblinkPeriodU3E__1_0(),
	U3CCaretBlinkU3Ec__Iterator0_t4293627739::get_offset_of_U3CblinkStateU3E__1_1(),
	U3CCaretBlinkU3Ec__Iterator0_t4293627739::get_offset_of_U24this_2(),
	U3CCaretBlinkU3Ec__Iterator0_t4293627739::get_offset_of_U24current_3(),
	U3CCaretBlinkU3Ec__Iterator0_t4293627739::get_offset_of_U24disposing_4(),
	U3CCaretBlinkU3Ec__Iterator0_t4293627739::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[8] = 
{
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069::get_offset_of_eventData_0(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069::get_offset_of_U3ClocalMousePosU3E__1_1(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069::get_offset_of_U3CrectU3E__1_2(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069::get_offset_of_U3CdelayU3E__1_3(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069::get_offset_of_U24this_4(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069::get_offset_of_U24current_5(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069::get_offset_of_U24disposing_6(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2283961069::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (SetPropertyUtility_t2931591262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (TMP_InputValidator_t1385053824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (TMP_LineInfo_t1079631636)+ sizeof (RuntimeObject), sizeof(TMP_LineInfo_t1079631636 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2311[20] = 
{
	TMP_LineInfo_t1079631636::get_offset_of_controlCharacterCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_characterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_visibleCharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_spaceCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_wordCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lastVisibleCharacterIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_length_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lineHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_ascender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_baseline_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_descender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_maxAdvance_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_width_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_marginLeft_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_marginRight_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_alignment_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lineExtents_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (TMP_MaterialManager_t2944071966), -1, sizeof(TMP_MaterialManager_t2944071966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2313[5] = 
{
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_materialList_0(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_fallbackMaterials_1(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_fallbackMaterialLookup_2(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_fallbackCleanupList_3(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_isFallbackListDirty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (FallbackMaterial_t4171378819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[5] = 
{
	FallbackMaterial_t4171378819::get_offset_of_baseID_0(),
	FallbackMaterial_t4171378819::get_offset_of_baseMaterial_1(),
	FallbackMaterial_t4171378819::get_offset_of_fallbackID_2(),
	FallbackMaterial_t4171378819::get_offset_of_fallbackMaterial_3(),
	FallbackMaterial_t4171378819::get_offset_of_count_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (MaskingMaterial_t16896275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[4] = 
{
	MaskingMaterial_t16896275::get_offset_of_baseMaterial_0(),
	MaskingMaterial_t16896275::get_offset_of_stencilMaterial_1(),
	MaskingMaterial_t16896275::get_offset_of_count_2(),
	MaskingMaterial_t16896275::get_offset_of_stencilID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (U3CGetBaseMaterialU3Ec__AnonStorey0_t3090515672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[1] = 
{
	U3CGetBaseMaterialU3Ec__AnonStorey0_t3090515672::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (U3CAddMaskingMaterialU3Ec__AnonStorey1_t3195963584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[1] = 
{
	U3CAddMaskingMaterialU3Ec__AnonStorey1_t3195963584::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (U3CRemoveStencilMaterialU3Ec__AnonStorey2_t2443477048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[1] = 
{
	U3CRemoveStencilMaterialU3Ec__AnonStorey2_t2443477048::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (U3CReleaseBaseMaterialU3Ec__AnonStorey3_t317586538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[1] = 
{
	U3CReleaseBaseMaterialU3Ec__AnonStorey3_t317586538::get_offset_of_baseMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (VertexSortingOrder_t2659893934)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2320[3] = 
{
	VertexSortingOrder_t2659893934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (TMP_MeshInfo_t2771747634)+ sizeof (RuntimeObject), -1, sizeof(TMP_MeshInfo_t2771747634_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2321[12] = 
{
	TMP_MeshInfo_t2771747634_StaticFields::get_offset_of_s_DefaultColor_0(),
	TMP_MeshInfo_t2771747634_StaticFields::get_offset_of_s_DefaultNormal_1(),
	TMP_MeshInfo_t2771747634_StaticFields::get_offset_of_s_DefaultTangent_2(),
	TMP_MeshInfo_t2771747634::get_offset_of_mesh_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_vertexCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_vertices_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_normals_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_tangents_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_uvs0_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_uvs2_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_colors32_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_triangles_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (TMP_ScrollbarEventHandler_t374199898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[1] = 
{
	TMP_ScrollbarEventHandler_t374199898::get_offset_of_isSelected_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (TMP_SelectionCaret_t407257285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (TMP_Settings_t2071156274), -1, sizeof(TMP_Settings_t2071156274_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2325[26] = 
{
	TMP_Settings_t2071156274_StaticFields::get_offset_of_s_Instance_2(),
	TMP_Settings_t2071156274::get_offset_of_m_enableWordWrapping_3(),
	TMP_Settings_t2071156274::get_offset_of_m_enableKerning_4(),
	TMP_Settings_t2071156274::get_offset_of_m_enableExtraPadding_5(),
	TMP_Settings_t2071156274::get_offset_of_m_enableTintAllSprites_6(),
	TMP_Settings_t2071156274::get_offset_of_m_enableParseEscapeCharacters_7(),
	TMP_Settings_t2071156274::get_offset_of_m_missingGlyphCharacter_8(),
	TMP_Settings_t2071156274::get_offset_of_m_warningsDisabled_9(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultFontAsset_10(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultFontAssetPath_11(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultFontSize_12(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultAutoSizeMinRatio_13(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultAutoSizeMaxRatio_14(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultTextMeshProTextContainerSize_15(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultTextMeshProUITextContainerSize_16(),
	TMP_Settings_t2071156274::get_offset_of_m_autoSizeTextContainer_17(),
	TMP_Settings_t2071156274::get_offset_of_m_fallbackFontAssets_18(),
	TMP_Settings_t2071156274::get_offset_of_m_matchMaterialPreset_19(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultSpriteAsset_20(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultSpriteAssetPath_21(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultColorGradientPresetsPath_22(),
	TMP_Settings_t2071156274::get_offset_of_m_enableEmojiSupport_23(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultStyleSheet_24(),
	TMP_Settings_t2071156274::get_offset_of_m_leadingCharacters_25(),
	TMP_Settings_t2071156274::get_offset_of_m_followingCharacters_26(),
	TMP_Settings_t2071156274::get_offset_of_m_linebreakingRules_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (LineBreakingTable_t1457697482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[2] = 
{
	LineBreakingTable_t1457697482::get_offset_of_leadingCharacters_0(),
	LineBreakingTable_t1457697482::get_offset_of_followingCharacters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (TMP_Sprite_t554067146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[5] = 
{
	TMP_Sprite_t554067146::get_offset_of_name_9(),
	TMP_Sprite_t554067146::get_offset_of_hashCode_10(),
	TMP_Sprite_t554067146::get_offset_of_unicode_11(),
	TMP_Sprite_t554067146::get_offset_of_pivot_12(),
	TMP_Sprite_t554067146::get_offset_of_sprite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (TMP_SpriteAnimator_t2836635477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[2] = 
{
	TMP_SpriteAnimator_t2836635477::get_offset_of_m_animations_2(),
	TMP_SpriteAnimator_t2836635477::get_offset_of_m_TextComponent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[16] = 
{
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_start_0(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U3CcurrentFrameU3E__0_1(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_end_2(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_spriteAsset_3(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_currentCharacter_4(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U3CcharInfoU3E__0_5(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U3CmaterialIndexU3E__0_6(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U3CvertexIndexU3E__0_7(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U3CmeshInfoU3E__0_8(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U3CelapsedTimeU3E__0_9(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_framerate_10(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U3CtargetTimeU3E__0_11(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U24this_12(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U24current_13(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U24disposing_14(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t827549206::get_offset_of_U24PC_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (TMP_SpriteAsset_t484820633), -1, sizeof(TMP_SpriteAsset_t484820633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2330[7] = 
{
	TMP_SpriteAsset_t484820633::get_offset_of_m_UnicodeLookup_5(),
	TMP_SpriteAsset_t484820633::get_offset_of_m_NameLookup_6(),
	TMP_SpriteAsset_t484820633_StaticFields::get_offset_of_m_defaultSpriteAsset_7(),
	TMP_SpriteAsset_t484820633::get_offset_of_spriteSheet_8(),
	TMP_SpriteAsset_t484820633::get_offset_of_spriteInfoList_9(),
	TMP_SpriteAsset_t484820633::get_offset_of_fallbackSpriteAssets_10(),
	TMP_SpriteAsset_t484820633_StaticFields::get_offset_of_k_searchedSpriteAssets_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (SpriteAssetImportFormats_t116390639)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2331[3] = 
{
	SpriteAssetImportFormats_t116390639::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (TexturePacker_t3148178657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (SpriteFrame_t3912389194)+ sizeof (RuntimeObject), sizeof(SpriteFrame_t3912389194 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2333[4] = 
{
	SpriteFrame_t3912389194::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_w_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_h_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (SpriteSize_t3355290999)+ sizeof (RuntimeObject), sizeof(SpriteSize_t3355290999 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2334[2] = 
{
	SpriteSize_t3355290999::get_offset_of_w_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteSize_t3355290999::get_offset_of_h_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (SpriteData_t3048397587)+ sizeof (RuntimeObject), sizeof(SpriteData_t3048397587_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2335[7] = 
{
	SpriteData_t3048397587::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (SpriteDataObject_t308163541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[1] = 
{
	SpriteDataObject_t308163541::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (TMP_Style_t3479380764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[6] = 
{
	TMP_Style_t3479380764::get_offset_of_m_Name_0(),
	TMP_Style_t3479380764::get_offset_of_m_HashCode_1(),
	TMP_Style_t3479380764::get_offset_of_m_OpeningDefinition_2(),
	TMP_Style_t3479380764::get_offset_of_m_ClosingDefinition_3(),
	TMP_Style_t3479380764::get_offset_of_m_OpeningTagArray_4(),
	TMP_Style_t3479380764::get_offset_of_m_ClosingTagArray_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (TMP_StyleSheet_t917564226), -1, sizeof(TMP_StyleSheet_t917564226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2338[3] = 
{
	TMP_StyleSheet_t917564226_StaticFields::get_offset_of_s_Instance_2(),
	TMP_StyleSheet_t917564226::get_offset_of_m_StyleList_3(),
	TMP_StyleSheet_t917564226::get_offset_of_m_StyleDictionary_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (TMP_SubMesh_t2613037997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[14] = 
{
	TMP_SubMesh_t2613037997::get_offset_of_m_fontAsset_2(),
	TMP_SubMesh_t2613037997::get_offset_of_m_spriteAsset_3(),
	TMP_SubMesh_t2613037997::get_offset_of_m_material_4(),
	TMP_SubMesh_t2613037997::get_offset_of_m_sharedMaterial_5(),
	TMP_SubMesh_t2613037997::get_offset_of_m_fallbackMaterial_6(),
	TMP_SubMesh_t2613037997::get_offset_of_m_fallbackSourceMaterial_7(),
	TMP_SubMesh_t2613037997::get_offset_of_m_isDefaultMaterial_8(),
	TMP_SubMesh_t2613037997::get_offset_of_m_padding_9(),
	TMP_SubMesh_t2613037997::get_offset_of_m_renderer_10(),
	TMP_SubMesh_t2613037997::get_offset_of_m_meshFilter_11(),
	TMP_SubMesh_t2613037997::get_offset_of_m_mesh_12(),
	TMP_SubMesh_t2613037997::get_offset_of_m_boxCollider_13(),
	TMP_SubMesh_t2613037997::get_offset_of_m_TextComponent_14(),
	TMP_SubMesh_t2613037997::get_offset_of_m_isRegisteredForEvents_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (TMP_SubMeshUI_t1578871311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[14] = 
{
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fontAsset_28(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_spriteAsset_29(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_material_30(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_sharedMaterial_31(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fallbackMaterial_32(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fallbackSourceMaterial_33(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_isDefaultMaterial_34(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_padding_35(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_canvasRenderer_36(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_mesh_37(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_TextComponent_38(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_isRegisteredForEvents_39(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_materialDirty_40(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_materialReferenceIndex_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (TextAlignmentOptions_t4036791236)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2342[37] = 
{
	TextAlignmentOptions_t4036791236::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (_HorizontalAlignmentOptions_t2379014378)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2343[7] = 
{
	_HorizontalAlignmentOptions_t2379014378::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (_VerticalAlignmentOptions_t2825528260)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2344[7] = 
{
	_VerticalAlignmentOptions_t2825528260::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (TextRenderFlags_t2418684345)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2345[3] = 
{
	TextRenderFlags_t2418684345::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (TMP_TextElementType_t1276645592)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2346[3] = 
{
	TMP_TextElementType_t1276645592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (MaskingTypes_t3687969768)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2347[4] = 
{
	MaskingTypes_t3687969768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (TextOverflowModes_t1430035314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2348[8] = 
{
	TextOverflowModes_t1430035314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (MaskingOffsetMode_t2266644590)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2349[3] = 
{
	MaskingOffsetMode_t2266644590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (TextureMappingOptions_t270963663)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2350[5] = 
{
	TextureMappingOptions_t270963663::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (FontStyles_t3828945032)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2351[12] = 
{
	FontStyles_t3828945032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (FontWeights_t3122883458)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2352[10] = 
{
	FontWeights_t3122883458::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (TagUnits_t1169424683)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2353[4] = 
{
	TagUnits_t1169424683::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (TagType_t123236451)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[5] = 
{
	TagType_t123236451::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (TMP_Text_t2599618874), -1, sizeof(TMP_Text_t2599618874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2355[203] = 
{
	TMP_Text_t2599618874::get_offset_of_m_text_28(),
	TMP_Text_t2599618874::get_offset_of_m_isRightToLeft_29(),
	TMP_Text_t2599618874::get_offset_of_m_fontAsset_30(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontAsset_31(),
	TMP_Text_t2599618874::get_offset_of_m_isSDFShader_32(),
	TMP_Text_t2599618874::get_offset_of_m_sharedMaterial_33(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterial_34(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferences_35(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceIndexLookup_36(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceStack_37(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterialIndex_38(),
	TMP_Text_t2599618874::get_offset_of_m_fontSharedMaterials_39(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterial_40(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterials_41(),
	TMP_Text_t2599618874::get_offset_of_m_isMaterialDirty_42(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor32_43(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor_44(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_s_colorWhite_45(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColor_46(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColor_47(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColor_48(),
	TMP_Text_t2599618874::get_offset_of_m_enableVertexGradient_49(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradient_50(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradientPreset_51(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAsset_52(),
	TMP_Text_t2599618874::get_offset_of_m_tintAllSprites_53(),
	TMP_Text_t2599618874::get_offset_of_m_tintSprite_54(),
	TMP_Text_t2599618874::get_offset_of_m_spriteColor_55(),
	TMP_Text_t2599618874::get_offset_of_m_overrideHtmlColors_56(),
	TMP_Text_t2599618874::get_offset_of_m_faceColor_57(),
	TMP_Text_t2599618874::get_offset_of_m_outlineColor_58(),
	TMP_Text_t2599618874::get_offset_of_m_outlineWidth_59(),
	TMP_Text_t2599618874::get_offset_of_m_fontSize_60(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontSize_61(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeBase_62(),
	TMP_Text_t2599618874::get_offset_of_m_sizeStack_63(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeight_64(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightInternal_65(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightStack_66(),
	TMP_Text_t2599618874::get_offset_of_m_enableAutoSizing_67(),
	TMP_Text_t2599618874::get_offset_of_m_maxFontSize_68(),
	TMP_Text_t2599618874::get_offset_of_m_minFontSize_69(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMin_70(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMax_71(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyle_72(),
	TMP_Text_t2599618874::get_offset_of_m_style_73(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyleStack_74(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingBold_75(),
	TMP_Text_t2599618874::get_offset_of_m_textAlignment_76(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustification_77(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustificationStack_78(),
	TMP_Text_t2599618874::get_offset_of_m_textContainerLocalCorners_79(),
	TMP_Text_t2599618874::get_offset_of_m_isAlignmentEnumConverted_80(),
	TMP_Text_t2599618874::get_offset_of_m_characterSpacing_81(),
	TMP_Text_t2599618874::get_offset_of_m_cSpacing_82(),
	TMP_Text_t2599618874::get_offset_of_m_monoSpacing_83(),
	TMP_Text_t2599618874::get_offset_of_m_wordSpacing_84(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacing_85(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingDelta_86(),
	TMP_Text_t2599618874::get_offset_of_m_lineHeight_87(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingMax_88(),
	TMP_Text_t2599618874::get_offset_of_m_paragraphSpacing_89(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthMaxAdj_90(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthAdjDelta_91(),
	TMP_Text_t2599618874::get_offset_of_m_enableWordWrapping_92(),
	TMP_Text_t2599618874::get_offset_of_m_isCharacterWrappingEnabled_93(),
	TMP_Text_t2599618874::get_offset_of_m_isNonBreakingSpace_94(),
	TMP_Text_t2599618874::get_offset_of_m_isIgnoringAlignment_95(),
	TMP_Text_t2599618874::get_offset_of_m_wordWrappingRatios_96(),
	TMP_Text_t2599618874::get_offset_of_m_overflowMode_97(),
	TMP_Text_t2599618874::get_offset_of_m_firstOverflowCharacterIndex_98(),
	TMP_Text_t2599618874::get_offset_of_m_linkedTextComponent_99(),
	TMP_Text_t2599618874::get_offset_of_m_isLinkedTextComponent_100(),
	TMP_Text_t2599618874::get_offset_of_m_isTextTruncated_101(),
	TMP_Text_t2599618874::get_offset_of_m_enableKerning_102(),
	TMP_Text_t2599618874::get_offset_of_m_enableExtraPadding_103(),
	TMP_Text_t2599618874::get_offset_of_checkPaddingRequired_104(),
	TMP_Text_t2599618874::get_offset_of_m_isRichText_105(),
	TMP_Text_t2599618874::get_offset_of_m_parseCtrlCharacters_106(),
	TMP_Text_t2599618874::get_offset_of_m_isOverlay_107(),
	TMP_Text_t2599618874::get_offset_of_m_isOrthographic_108(),
	TMP_Text_t2599618874::get_offset_of_m_isCullingEnabled_109(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreRectMaskCulling_110(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreCulling_111(),
	TMP_Text_t2599618874::get_offset_of_m_horizontalMapping_112(),
	TMP_Text_t2599618874::get_offset_of_m_verticalMapping_113(),
	TMP_Text_t2599618874::get_offset_of_m_uvLineOffset_114(),
	TMP_Text_t2599618874::get_offset_of_m_renderMode_115(),
	TMP_Text_t2599618874::get_offset_of_m_geometrySortingOrder_116(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacter_117(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleCharacters_118(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleWords_119(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleLines_120(),
	TMP_Text_t2599618874::get_offset_of_m_useMaxVisibleDescender_121(),
	TMP_Text_t2599618874::get_offset_of_m_pageToDisplay_122(),
	TMP_Text_t2599618874::get_offset_of_m_isNewPage_123(),
	TMP_Text_t2599618874::get_offset_of_m_margin_124(),
	TMP_Text_t2599618874::get_offset_of_m_marginLeft_125(),
	TMP_Text_t2599618874::get_offset_of_m_marginRight_126(),
	TMP_Text_t2599618874::get_offset_of_m_marginWidth_127(),
	TMP_Text_t2599618874::get_offset_of_m_marginHeight_128(),
	TMP_Text_t2599618874::get_offset_of_m_width_129(),
	TMP_Text_t2599618874::get_offset_of_m_textInfo_130(),
	TMP_Text_t2599618874::get_offset_of_m_havePropertiesChanged_131(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingLegacyAnimationComponent_132(),
	TMP_Text_t2599618874::get_offset_of_m_transform_133(),
	TMP_Text_t2599618874::get_offset_of_m_rectTransform_134(),
	TMP_Text_t2599618874::get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135(),
	TMP_Text_t2599618874::get_offset_of_m_autoSizeTextContainer_136(),
	TMP_Text_t2599618874::get_offset_of_m_mesh_137(),
	TMP_Text_t2599618874::get_offset_of_m_isVolumetricText_138(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimator_139(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleHeight_140(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleWidth_141(),
	TMP_Text_t2599618874::get_offset_of_m_minWidth_142(),
	TMP_Text_t2599618874::get_offset_of_m_minHeight_143(),
	TMP_Text_t2599618874::get_offset_of_m_maxWidth_144(),
	TMP_Text_t2599618874::get_offset_of_m_maxHeight_145(),
	TMP_Text_t2599618874::get_offset_of_m_LayoutElement_146(),
	TMP_Text_t2599618874::get_offset_of_m_preferredWidth_147(),
	TMP_Text_t2599618874::get_offset_of_m_renderedWidth_148(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredWidthDirty_149(),
	TMP_Text_t2599618874::get_offset_of_m_preferredHeight_150(),
	TMP_Text_t2599618874::get_offset_of_m_renderedHeight_151(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredHeightDirty_152(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculatingPreferredValues_153(),
	TMP_Text_t2599618874::get_offset_of_m_recursiveCount_154(),
	TMP_Text_t2599618874::get_offset_of_m_layoutPriority_155(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculateSizeRequired_156(),
	TMP_Text_t2599618874::get_offset_of_m_isLayoutDirty_157(),
	TMP_Text_t2599618874::get_offset_of_m_verticesAlreadyDirty_158(),
	TMP_Text_t2599618874::get_offset_of_m_layoutAlreadyDirty_159(),
	TMP_Text_t2599618874::get_offset_of_m_isAwake_160(),
	TMP_Text_t2599618874::get_offset_of_m_isInputParsingRequired_161(),
	TMP_Text_t2599618874::get_offset_of_m_inputSource_162(),
	TMP_Text_t2599618874::get_offset_of_old_text_163(),
	TMP_Text_t2599618874::get_offset_of_m_fontScale_164(),
	TMP_Text_t2599618874::get_offset_of_m_fontScaleMultiplier_165(),
	TMP_Text_t2599618874::get_offset_of_m_htmlTag_166(),
	TMP_Text_t2599618874::get_offset_of_m_xmlAttribute_167(),
	TMP_Text_t2599618874::get_offset_of_m_attributeParameterValues_168(),
	TMP_Text_t2599618874::get_offset_of_tag_LineIndent_169(),
	TMP_Text_t2599618874::get_offset_of_tag_Indent_170(),
	TMP_Text_t2599618874::get_offset_of_m_indentStack_171(),
	TMP_Text_t2599618874::get_offset_of_tag_NoParsing_172(),
	TMP_Text_t2599618874::get_offset_of_m_isParsingText_173(),
	TMP_Text_t2599618874::get_offset_of_m_FXMatrix_174(),
	TMP_Text_t2599618874::get_offset_of_m_isFXMatrixSet_175(),
	TMP_Text_t2599618874::get_offset_of_m_char_buffer_176(),
	TMP_Text_t2599618874::get_offset_of_m_internalCharacterInfo_177(),
	TMP_Text_t2599618874::get_offset_of_m_input_CharArray_178(),
	TMP_Text_t2599618874::get_offset_of_m_charArray_Length_179(),
	TMP_Text_t2599618874::get_offset_of_m_totalCharacterCount_180(),
	TMP_Text_t2599618874::get_offset_of_m_SavedWordWrapState_181(),
	TMP_Text_t2599618874::get_offset_of_m_SavedLineState_182(),
	TMP_Text_t2599618874::get_offset_of_m_characterCount_183(),
	TMP_Text_t2599618874::get_offset_of_m_firstCharacterOfLine_184(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacterOfLine_185(),
	TMP_Text_t2599618874::get_offset_of_m_lastCharacterOfLine_186(),
	TMP_Text_t2599618874::get_offset_of_m_lastVisibleCharacterOfLine_187(),
	TMP_Text_t2599618874::get_offset_of_m_lineNumber_188(),
	TMP_Text_t2599618874::get_offset_of_m_lineVisibleCharacterCount_189(),
	TMP_Text_t2599618874::get_offset_of_m_pageNumber_190(),
	TMP_Text_t2599618874::get_offset_of_m_maxAscender_191(),
	TMP_Text_t2599618874::get_offset_of_m_maxCapHeight_192(),
	TMP_Text_t2599618874::get_offset_of_m_maxDescender_193(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineAscender_194(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineDescender_195(),
	TMP_Text_t2599618874::get_offset_of_m_startOfLineAscender_196(),
	TMP_Text_t2599618874::get_offset_of_m_lineOffset_197(),
	TMP_Text_t2599618874::get_offset_of_m_meshExtents_198(),
	TMP_Text_t2599618874::get_offset_of_m_htmlColor_199(),
	TMP_Text_t2599618874::get_offset_of_m_colorStack_200(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColorStack_201(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColorStack_202(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColorStack_203(),
	TMP_Text_t2599618874::get_offset_of_m_colorGradientPreset_204(),
	TMP_Text_t2599618874::get_offset_of_m_colorGradientStack_205(),
	TMP_Text_t2599618874::get_offset_of_m_tabSpacing_206(),
	TMP_Text_t2599618874::get_offset_of_m_spacing_207(),
	TMP_Text_t2599618874::get_offset_of_m_styleStack_208(),
	TMP_Text_t2599618874::get_offset_of_m_actionStack_209(),
	TMP_Text_t2599618874::get_offset_of_m_padding_210(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffset_211(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffsetStack_212(),
	TMP_Text_t2599618874::get_offset_of_m_xAdvance_213(),
	TMP_Text_t2599618874::get_offset_of_m_textElementType_214(),
	TMP_Text_t2599618874::get_offset_of_m_cached_TextElement_215(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Underline_GlyphInfo_216(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Ellipsis_GlyphInfo_217(),
	TMP_Text_t2599618874::get_offset_of_m_defaultSpriteAsset_218(),
	TMP_Text_t2599618874::get_offset_of_m_currentSpriteAsset_219(),
	TMP_Text_t2599618874::get_offset_of_m_spriteCount_220(),
	TMP_Text_t2599618874::get_offset_of_m_spriteIndex_221(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimationID_222(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreActiveState_223(),
	TMP_Text_t2599618874::get_offset_of_k_Power_224(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveVector2_225(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeVector2_226(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveFloat_227(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeFloat_228(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveInt_229(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeInt_230(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (TextInputSources_t1522115805)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2356[5] = 
{
	TextInputSources_t1522115805::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (TMP_TextElement_t129727469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[9] = 
{
	TMP_TextElement_t129727469::get_offset_of_id_0(),
	TMP_TextElement_t129727469::get_offset_of_x_1(),
	TMP_TextElement_t129727469::get_offset_of_y_2(),
	TMP_TextElement_t129727469::get_offset_of_width_3(),
	TMP_TextElement_t129727469::get_offset_of_height_4(),
	TMP_TextElement_t129727469::get_offset_of_xOffset_5(),
	TMP_TextElement_t129727469::get_offset_of_yOffset_6(),
	TMP_TextElement_t129727469::get_offset_of_xAdvance_7(),
	TMP_TextElement_t129727469::get_offset_of_scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (TMP_TextInfo_t3598145122), -1, sizeof(TMP_TextInfo_t3598145122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2358[18] = 
{
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorPositive_0(),
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorNegative_1(),
	TMP_TextInfo_t3598145122::get_offset_of_textComponent_2(),
	TMP_TextInfo_t3598145122::get_offset_of_characterCount_3(),
	TMP_TextInfo_t3598145122::get_offset_of_spriteCount_4(),
	TMP_TextInfo_t3598145122::get_offset_of_spaceCount_5(),
	TMP_TextInfo_t3598145122::get_offset_of_wordCount_6(),
	TMP_TextInfo_t3598145122::get_offset_of_linkCount_7(),
	TMP_TextInfo_t3598145122::get_offset_of_lineCount_8(),
	TMP_TextInfo_t3598145122::get_offset_of_pageCount_9(),
	TMP_TextInfo_t3598145122::get_offset_of_materialCount_10(),
	TMP_TextInfo_t3598145122::get_offset_of_characterInfo_11(),
	TMP_TextInfo_t3598145122::get_offset_of_wordInfo_12(),
	TMP_TextInfo_t3598145122::get_offset_of_linkInfo_13(),
	TMP_TextInfo_t3598145122::get_offset_of_lineInfo_14(),
	TMP_TextInfo_t3598145122::get_offset_of_pageInfo_15(),
	TMP_TextInfo_t3598145122::get_offset_of_meshInfo_16(),
	TMP_TextInfo_t3598145122::get_offset_of_m_CachedMeshInfo_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (CaretPosition_t3997512201)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2359[4] = 
{
	CaretPosition_t3997512201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (CaretInfo_t841780893)+ sizeof (RuntimeObject), sizeof(CaretInfo_t841780893 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2360[2] = 
{
	CaretInfo_t841780893::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CaretInfo_t841780893::get_offset_of_position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (TMP_TextUtilities_t2105690005), -1, sizeof(TMP_TextUtilities_t2105690005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2361[3] = 
{
	TMP_TextUtilities_t2105690005_StaticFields::get_offset_of_m_rectWorldCorners_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (LineSegment_t1526544958)+ sizeof (RuntimeObject), sizeof(LineSegment_t1526544958 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2362[2] = 
{
	LineSegment_t1526544958::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LineSegment_t1526544958::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (TMP_UpdateManager_t4114267509), -1, sizeof(TMP_UpdateManager_t4114267509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2363[5] = 
{
	TMP_UpdateManager_t4114267509_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (TMP_UpdateRegistry_t461608481), -1, sizeof(TMP_UpdateRegistry_t461608481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2364[5] = 
{
	TMP_UpdateRegistry_t461608481_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (TMP_BasicXmlTagStack_t2962628096)+ sizeof (RuntimeObject), sizeof(TMP_BasicXmlTagStack_t2962628096 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2365[10] = 
{
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_bold_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_italic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_underline_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_strikethrough_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_highlight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_superscript_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_subscript_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_uppercase_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_lowercase_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_smallcaps_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2367[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (U24ArrayTypeU3D40_t2865632059)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D40_t2865632059 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (MainThreadDispatcher_t3211665238), -1, sizeof(MainThreadDispatcher_t3211665238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2371[3] = 
{
	MainThreadDispatcher_t3211665238_StaticFields::get_offset_of_OBJECT_NAME_2(),
	MainThreadDispatcher_t3211665238_StaticFields::get_offset_of_s_Callbacks_3(),
	MainThreadDispatcher_t3211665238_StaticFields::get_offset_of_s_CallbacksPending_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (AppInfo_t2433711276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[5] = 
{
	AppInfo_t2433711276::get_offset_of_U3CappIdU3Ek__BackingField_0(),
	AppInfo_t2433711276::get_offset_of_U3CappKeyU3Ek__BackingField_1(),
	AppInfo_t2433711276::get_offset_of_U3CclientIdU3Ek__BackingField_2(),
	AppInfo_t2433711276::get_offset_of_U3CclientKeyU3Ek__BackingField_3(),
	AppInfo_t2433711276::get_offset_of_U3CdebugU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (DemoInputManager_t35471296), -1, sizeof(DemoInputManager_t35471296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2374[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DemoInputManager_t35471296::get_offset_of_isDaydream_16(),
	DemoInputManager_t35471296_StaticFields::get_offset_of_CARDBOARD_DEVICE_NAME_17(),
	DemoInputManager_t35471296_StaticFields::get_offset_of_DAYDREAM_DEVICE_NAME_18(),
	DemoInputManager_t35471296::get_offset_of_controllerMain_19(),
	DemoInputManager_t35471296_StaticFields::get_offset_of_CONTROLLER_MAIN_PROP_NAME_20(),
	DemoInputManager_t35471296::get_offset_of_controllerPointer_21(),
	DemoInputManager_t35471296_StaticFields::get_offset_of_CONTROLLER_POINTER_PROP_NAME_22(),
	DemoInputManager_t35471296::get_offset_of_reticlePointer_23(),
	DemoInputManager_t35471296_StaticFields::get_offset_of_RETICLE_POINTER_PROP_NAME_24(),
	DemoInputManager_t35471296::get_offset_of_messageCanvas_25(),
	DemoInputManager_t35471296::get_offset_of_messageText_26(),
	DemoInputManager_t35471296::get_offset_of_gvrEmulatedPlatformType_27(),
	DemoInputManager_t35471296_StaticFields::get_offset_of_EMULATED_PLATFORM_PROP_NAME_28(),
	DemoInputManager_t35471296_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_29(),
	DemoInputManager_t35471296_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (EmulatedPlatformType_t3351326478)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2375[3] = 
{
	EmulatedPlatformType_t3351326478::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (DemoSceneManager_t570878136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (GVRDemoManager_t3575522595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[2] = 
{
	GVRDemoManager_t3575522595::get_offset_of_m_launchVrHomeButton_2(),
	GVRDemoManager_t3575522595::get_offset_of_m_demoInputManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (HeadsetDemoManager_t1859624338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[2] = 
{
	HeadsetDemoManager_t1859624338::get_offset_of_enableDebugLog_2(),
	HeadsetDemoManager_t1859624338::get_offset_of_waitFourSeconds_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (U3CStatusUpdateLoopU3Ec__Iterator0_t858545123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[4] = 
{
	U3CStatusUpdateLoopU3Ec__Iterator0_t858545123::get_offset_of_U24this_0(),
	U3CStatusUpdateLoopU3Ec__Iterator0_t858545123::get_offset_of_U24current_1(),
	U3CStatusUpdateLoopU3Ec__Iterator0_t858545123::get_offset_of_U24disposing_2(),
	U3CStatusUpdateLoopU3Ec__Iterator0_t858545123::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (Teleport_t310499394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[3] = 
{
	Teleport_t310499394::get_offset_of_startingPosition_2(),
	Teleport_t310499394::get_offset_of_inactiveMaterial_3(),
	Teleport_t310499394::get_offset_of_gazedAtMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (KeyboardDelegateExample_t150747960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[5] = 
{
	KeyboardDelegateExample_t150747960::get_offset_of_KeyboardText_2(),
	KeyboardDelegateExample_t150747960::get_offset_of_UpdateCanvas_3(),
	KeyboardDelegateExample_t150747960::get_offset_of_KeyboardHidden_4(),
	KeyboardDelegateExample_t150747960::get_offset_of_KeyboardShown_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (JumpToPage_t3212685436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[7] = 
{
	JumpToPage_t3212685436::get_offset_of_page_2(),
	JumpToPage_t3212685436::get_offset_of_hoverTransform_3(),
	JumpToPage_t3212685436::get_offset_of_hoverPositionZMeters_4(),
	JumpToPage_t3212685436::get_offset_of_interpolationSpeed_5(),
	JumpToPage_t3212685436::get_offset_of_graphic_6(),
	JumpToPage_t3212685436::get_offset_of_desiredPositionZ_7(),
	JumpToPage_t3212685436::get_offset_of_cachedPagedScrollRect_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (PagedScrollBar_t4001695667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[4] = 
{
	0,
	PagedScrollBar_t4001695667::get_offset_of_pagedScrollRect_28(),
	PagedScrollBar_t4001695667::get_offset_of_isDragging_29(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (PagedScrollRect_t3286523126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[37] = 
{
	PagedScrollRect_t3286523126::get_offset_of_ScrollSensitivity_2(),
	PagedScrollRect_t3286523126::get_offset_of_SnapSpeed_3(),
	PagedScrollRect_t3286523126::get_offset_of_StartPage_4(),
	PagedScrollRect_t3286523126::get_offset_of_loop_5(),
	PagedScrollRect_t3286523126::get_offset_of_onlyScrollWhenPointing_6(),
	PagedScrollRect_t3286523126::get_offset_of_numExtraPagesShown_7(),
	PagedScrollRect_t3286523126::get_offset_of_showNextPagesAtRest_8(),
	PagedScrollRect_t3286523126::get_offset_of_shouldTilesAlwaysBeInteractable_9(),
	PagedScrollRect_t3286523126::get_offset_of_scrollingEnabled_10(),
	PagedScrollRect_t3286523126::get_offset_of_OnActivePageChanged_11(),
	PagedScrollRect_t3286523126::get_offset_of_OnSwipeLeft_12(),
	PagedScrollRect_t3286523126::get_offset_of_OnSwipeRight_13(),
	PagedScrollRect_t3286523126::get_offset_of_OnSnapClosest_14(),
	PagedScrollRect_t3286523126::get_offset_of_pageProvider_15(),
	PagedScrollRect_t3286523126::get_offset_of_scrollEffects_16(),
	PagedScrollRect_t3286523126::get_offset_of_isTrackingTouches_17(),
	PagedScrollRect_t3286523126::get_offset_of_initialTouchPos_18(),
	PagedScrollRect_t3286523126::get_offset_of_previousTouchPos_19(),
	PagedScrollRect_t3286523126::get_offset_of_previousTouchTimestamp_20(),
	PagedScrollRect_t3286523126::get_offset_of_overallVelocity_21(),
	PagedScrollRect_t3286523126::get_offset_of_isScrolling_22(),
	PagedScrollRect_t3286523126::get_offset_of_isPointerHovering_23(),
	PagedScrollRect_t3286523126::get_offset_of_scrollOffset_24(),
	PagedScrollRect_t3286523126::get_offset_of_targetScrollOffset_25(),
	PagedScrollRect_t3286523126::get_offset_of_isScrollOffsetOverridden_26(),
	PagedScrollRect_t3286523126::get_offset_of_activePage_27(),
	PagedScrollRect_t3286523126::get_offset_of_activeSnapCoroutine_28(),
	PagedScrollRect_t3286523126::get_offset_of_indexToVisiblePage_29(),
	PagedScrollRect_t3286523126::get_offset_of_visiblePageToIndex_30(),
	PagedScrollRect_t3286523126::get_offset_of_visiblePages_31(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (ActivePageChangedDelegate_t1951257476), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (SnapDirection_t167415511)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2386[4] = 
{
	SnapDirection_t167415511::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[5] = 
{
	U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272::get_offset_of_index_0(),
	U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272::get_offset_of_U24this_1(),
	U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272::get_offset_of_U24current_2(),
	U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272::get_offset_of_U24disposing_3(),
	U3CSnapToPageCoroutineU3Ec__Iterator0_t3387229272::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (ChildrenPageProvider_t2451340698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[2] = 
{
	ChildrenPageProvider_t2451340698::get_offset_of_pages_2(),
	ChildrenPageProvider_t2451340698::get_offset_of_spacing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (PooledPageProvider_t4250155112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[4] = 
{
	PooledPageProvider_t4250155112::get_offset_of_pagePrefab_2(),
	PooledPageProvider_t4250155112::get_offset_of_spacing_3(),
	PooledPageProvider_t4250155112::get_offset_of_NumPages_4(),
	PooledPageProvider_t4250155112::get_offset_of_prefabName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (PrefabPageProvider_t1618903157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[2] = 
{
	PrefabPageProvider_t1618903157::get_offset_of_prefabs_2(),
	PrefabPageProvider_t1618903157::get_offset_of_spacing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (BaseScrollEffect_t1789251746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (UpdateData_t722198904)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[9] = 
{
	UpdateData_t722198904::get_offset_of_page_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateData_t722198904::get_offset_of_pageIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateData_t722198904::get_offset_of_pageCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateData_t722198904::get_offset_of_pageOffset_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateData_t722198904::get_offset_of_scrollOffset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateData_t722198904::get_offset_of_spacing_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateData_t722198904::get_offset_of_looping_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateData_t722198904::get_offset_of_isInteractable_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UpdateData_t722198904::get_offset_of_moveDistance_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (BaseTile_t770030199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[8] = 
{
	BaseTile_t770030199::get_offset_of_originalParent_2(),
	BaseTile_t770030199::get_offset_of_originalRotation_3(),
	BaseTile_t770030199::get_offset_of_originalPosition_4(),
	BaseTile_t770030199::get_offset_of_originalScale_5(),
	BaseTile_t770030199::get_offset_of_page_6(),
	BaseTile_t770030199::get_offset_of_isInteractable_7(),
	BaseTile_t770030199::get_offset_of_isHovering_8(),
	BaseTile_t770030199::get_offset_of_metersToCanvasScale_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (FadeScrollEffect_t2917998861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[1] = 
{
	FadeScrollEffect_t2917998861::get_offset_of_minAlpha_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (FloatTile_t2669707442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[11] = 
{
	0,
	0,
	0,
	FloatTile_t2669707442::get_offset_of_desiredRotation_13(),
	FloatTile_t2669707442::get_offset_of_desiredPositionZ_14(),
	FloatTile_t2669707442::get_offset_of_desiredScale_15(),
	FloatTile_t2669707442::get_offset_of_hoverScale_16(),
	FloatTile_t2669707442::get_offset_of_hoverPositionZMeters_17(),
	FloatTile_t2669707442::get_offset_of_maximumRotationDegreesCamera_18(),
	FloatTile_t2669707442::get_offset_of_maximumRotationDegreesPointer_19(),
	FloatTile_t2669707442::get_offset_of_interpolationSpeed_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (MaskedTile_t1953876158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[13] = 
{
	0,
	0,
	MaskedTile_t1953876158::get_offset_of_maskedImage_12(),
	MaskedTile_t1953876158::get_offset_of_maskedImageObject_13(),
	MaskedTile_t1953876158::get_offset_of_originalMaskedPosition_14(),
	MaskedTile_t1953876158::get_offset_of_maskedScrollOffset_15(),
	MaskedTile_t1953876158::get_offset_of_originalImageSize_16(),
	MaskedTile_t1953876158::get_offset_of_enlargedImageSize_17(),
	MaskedTile_t1953876158::get_offset_of_desiredPositionZ_18(),
	MaskedTile_t1953876158::get_offset_of_movementWeight_19(),
	MaskedTile_t1953876158::get_offset_of_scaleWeight_20(),
	MaskedTile_t1953876158::get_offset_of_hoverPositionZMeters_21(),
	MaskedTile_t1953876158::get_offset_of_interpolationSpeed_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (ScaleScrollEffect_t2628707146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[1] = 
{
	ScaleScrollEffect_t2628707146::get_offset_of_minScale_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (TiledPage_t1725039945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[6] = 
{
	TiledPage_t1725039945::get_offset_of_tiles_2(),
	TiledPage_t1725039945::get_offset_of_layoutTransform_3(),
	TiledPage_t1725039945::get_offset_of_staggerAnimationIntensity_4(),
	TiledPage_t1725039945::get_offset_of_tileOrderBy_5(),
	TiledPage_t1725039945::get_offset_of_tilesByDistanceFromLeft_6(),
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
