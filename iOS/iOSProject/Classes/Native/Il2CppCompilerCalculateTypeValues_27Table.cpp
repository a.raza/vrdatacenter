﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// CellTreeNode
struct CellTreeNode_t2932145224;
// OnClickDestroy
struct OnClickDestroy_t2392776543;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t1048209202;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// Region
struct Region_t3684225262;
// ExitGames.Client.Photon.PhotonPing
struct PhotonPing_t2371975946;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// PhotonPingManager
struct PhotonPingManager_t630892274;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2926365658;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.String[]
struct StringU5BU5D_t1281789340;
// RoomOptions
struct RoomOptions_t1787645948;
// TypedLobby
struct TypedLobby_t3336582029;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<Region>
struct List_1_t861332708;
// System.Void
struct Void_t1185182177;
// PhotonPlayer
struct PhotonPlayer_t3305149557;
// PhotonView
struct PhotonView_t2207721820;
// PhotonTransformViewPositionModel
struct PhotonTransformViewPositionModel_t2500134640;
// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct Queue_1_t3568572958;
// PhotonTransformViewScaleModel
struct PhotonTransformViewScaleModel_t763003770;
// PhotonTransformViewRotationModel
struct PhotonTransformViewRotationModel_t1080899250;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.List`1<CellTreeNode>
struct List_1_t109252670;
// PhotonHandler
struct PhotonHandler_t2139970417;
// NetworkingPeer
struct NetworkingPeer_t264212356;
// ServerSettings
struct ServerSettings_t2755303613;
// System.Collections.Generic.List`1<FriendInfo>
struct List_1_t2005371586;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t898892918;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_t3973553389;
// System.Type
struct Type_t;
// Room
struct Room_t3759828263;
// PhotonNetwork/EventCallback
struct EventCallback_t1220598991;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type>
struct Dictionary_2_t1253839074;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t2581629031;
// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t1302902347;
// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t4013908777;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t2956237011;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// ExitGames.Client.Photon.EncryptorManaged.Encryptor
struct Encryptor_t200327285;
// ExitGames.Client.Photon.EncryptorManaged.Decryptor
struct Decryptor_t2116099858;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1405253484;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// AuthenticationValues
struct AuthenticationValues_t660572511;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t1720840067;
// System.Collections.Generic.List`1<TypedLobbyInfo>
struct List_1_t3976582791;
// System.Collections.Generic.Dictionary`2<System.String,RoomInfo>
struct Dictionary_2_t2955551919;
// RoomInfo[]
struct RoomInfoU5BU5D_t1491207981;
// EnterRoomParams
struct EnterRoomParams_t3960472384;
// System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer>
struct Dictionary_2_t2193862888;
// PhotonPlayer[]
struct PhotonPlayerU5BU5D_t2880637464;
// System.Collections.Generic.HashSet`1<System.Byte>
struct HashSet_1_t3994213146;
// System.Collections.Generic.Dictionary`2<System.Int32,PhotonView>
struct Dictionary_2_t1096435151;
// PhotonStream
struct PhotonStream_t1003850889;
// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.Hashtable>
struct Dictionary_2_t4231889829;
// IPunPrefabPool
struct IPunPrefabPool_t3054155687;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>
struct Dictionary_2_t1499080758;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object[]>
struct Dictionary_2_t1732652656;
// RaiseEventOptions
struct RaiseEventOptions_t1229553678;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Animator
struct Animator_t434523843;
// PhotonStreamQueue
struct PhotonStreamQueue_t3244431384;
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>
struct List_1_t3272742856;
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>
struct List_1_t662835721;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2165061829;
// System.Func`1<System.Boolean>
struct Func_1_t3822001908;
// ExitGames.Client.Photon.PhotonPeer
struct PhotonPeer_t1608153861;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// CullArea
struct CullArea_t3053759289;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t2606371118;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// CellTree
struct CellTree_t3785927468;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t2349950;
// PhotonTransformViewPositionControl
struct PhotonTransformViewPositionControl_t619346209;
// PhotonTransformViewRotationControl
struct PhotonTransformViewRotationControl_t2679094986;
// PhotonTransformViewScaleControl
struct PhotonTransformViewScaleControl_t2271393751;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3395709193;
// System.Collections.Generic.Dictionary`2<UnityEngine.Component,System.Reflection.MethodInfo>
struct Dictionary_2_t3676033689;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CDOESPARAMETERSYNCHRONIZETYPEEXISTU3EC__ANONSTOREY1_T2105236361_H
#define U3CDOESPARAMETERSYNCHRONIZETYPEEXISTU3EC__ANONSTOREY1_T2105236361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/<DoesParameterSynchronizeTypeExist>c__AnonStorey1
struct  U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey1_t2105236361  : public RuntimeObject
{
public:
	// System.String PhotonAnimatorView/<DoesParameterSynchronizeTypeExist>c__AnonStorey1::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey1_t2105236361, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOESPARAMETERSYNCHRONIZETYPEEXISTU3EC__ANONSTOREY1_T2105236361_H
#ifndef U3CGETLAYERSYNCHRONIZETYPEU3EC__ANONSTOREY2_T4244813680_H
#define U3CGETLAYERSYNCHRONIZETYPEU3EC__ANONSTOREY2_T4244813680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/<GetLayerSynchronizeType>c__AnonStorey2
struct  U3CGetLayerSynchronizeTypeU3Ec__AnonStorey2_t4244813680  : public RuntimeObject
{
public:
	// System.Int32 PhotonAnimatorView/<GetLayerSynchronizeType>c__AnonStorey2::layerIndex
	int32_t ___layerIndex_0;

public:
	inline static int32_t get_offset_of_layerIndex_0() { return static_cast<int32_t>(offsetof(U3CGetLayerSynchronizeTypeU3Ec__AnonStorey2_t4244813680, ___layerIndex_0)); }
	inline int32_t get_layerIndex_0() const { return ___layerIndex_0; }
	inline int32_t* get_address_of_layerIndex_0() { return &___layerIndex_0; }
	inline void set_layerIndex_0(int32_t value)
	{
		___layerIndex_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETLAYERSYNCHRONIZETYPEU3EC__ANONSTOREY2_T4244813680_H
#ifndef U3CGETPARAMETERSYNCHRONIZETYPEU3EC__ANONSTOREY3_T204833724_H
#define U3CGETPARAMETERSYNCHRONIZETYPEU3EC__ANONSTOREY3_T204833724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/<GetParameterSynchronizeType>c__AnonStorey3
struct  U3CGetParameterSynchronizeTypeU3Ec__AnonStorey3_t204833724  : public RuntimeObject
{
public:
	// System.String PhotonAnimatorView/<GetParameterSynchronizeType>c__AnonStorey3::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CGetParameterSynchronizeTypeU3Ec__AnonStorey3_t204833724, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARAMETERSYNCHRONIZETYPEU3EC__ANONSTOREY3_T204833724_H
#ifndef U3CSETLAYERSYNCHRONIZEDU3EC__ANONSTOREY4_T2749014471_H
#define U3CSETLAYERSYNCHRONIZEDU3EC__ANONSTOREY4_T2749014471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/<SetLayerSynchronized>c__AnonStorey4
struct  U3CSetLayerSynchronizedU3Ec__AnonStorey4_t2749014471  : public RuntimeObject
{
public:
	// System.Int32 PhotonAnimatorView/<SetLayerSynchronized>c__AnonStorey4::layerIndex
	int32_t ___layerIndex_0;

public:
	inline static int32_t get_offset_of_layerIndex_0() { return static_cast<int32_t>(offsetof(U3CSetLayerSynchronizedU3Ec__AnonStorey4_t2749014471, ___layerIndex_0)); }
	inline int32_t get_layerIndex_0() const { return ___layerIndex_0; }
	inline int32_t* get_address_of_layerIndex_0() { return &___layerIndex_0; }
	inline void set_layerIndex_0(int32_t value)
	{
		___layerIndex_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETLAYERSYNCHRONIZEDU3EC__ANONSTOREY4_T2749014471_H
#ifndef U3CSETPARAMETERSYNCHRONIZEDU3EC__ANONSTOREY5_T3597370861_H
#define U3CSETPARAMETERSYNCHRONIZEDU3EC__ANONSTOREY5_T3597370861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/<SetParameterSynchronized>c__AnonStorey5
struct  U3CSetParameterSynchronizedU3Ec__AnonStorey5_t3597370861  : public RuntimeObject
{
public:
	// System.String PhotonAnimatorView/<SetParameterSynchronized>c__AnonStorey5::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CSetParameterSynchronizedU3Ec__AnonStorey5_t3597370861, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETPARAMETERSYNCHRONIZEDU3EC__ANONSTOREY5_T3597370861_H
#ifndef CELLTREE_T3785927468_H
#define CELLTREE_T3785927468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellTree
struct  CellTree_t3785927468  : public RuntimeObject
{
public:
	// CellTreeNode CellTree::<RootNode>k__BackingField
	CellTreeNode_t2932145224 * ___U3CRootNodeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CRootNodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CellTree_t3785927468, ___U3CRootNodeU3Ek__BackingField_0)); }
	inline CellTreeNode_t2932145224 * get_U3CRootNodeU3Ek__BackingField_0() const { return ___U3CRootNodeU3Ek__BackingField_0; }
	inline CellTreeNode_t2932145224 ** get_address_of_U3CRootNodeU3Ek__BackingField_0() { return &___U3CRootNodeU3Ek__BackingField_0; }
	inline void set_U3CRootNodeU3Ek__BackingField_0(CellTreeNode_t2932145224 * value)
	{
		___U3CRootNodeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootNodeU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CELLTREE_T3785927468_H
#ifndef U3CDESTROYRPCU3EC__ITERATOR0_T2038103911_H
#define U3CDESTROYRPCU3EC__ITERATOR0_T2038103911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickDestroy/<DestroyRpc>c__Iterator0
struct  U3CDestroyRpcU3Ec__Iterator0_t2038103911  : public RuntimeObject
{
public:
	// OnClickDestroy OnClickDestroy/<DestroyRpc>c__Iterator0::$this
	OnClickDestroy_t2392776543 * ___U24this_0;
	// System.Object OnClickDestroy/<DestroyRpc>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean OnClickDestroy/<DestroyRpc>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 OnClickDestroy/<DestroyRpc>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDestroyRpcU3Ec__Iterator0_t2038103911, ___U24this_0)); }
	inline OnClickDestroy_t2392776543 * get_U24this_0() const { return ___U24this_0; }
	inline OnClickDestroy_t2392776543 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(OnClickDestroy_t2392776543 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDestroyRpcU3Ec__Iterator0_t2038103911, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDestroyRpcU3Ec__Iterator0_t2038103911, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDestroyRpcU3Ec__Iterator0_t2038103911, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYRPCU3EC__ITERATOR0_T2038103911_H
#ifndef FRIENDINFO_T533296844_H
#define FRIENDINFO_T533296844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendInfo
struct  FriendInfo_t533296844  : public RuntimeObject
{
public:
	// System.String FriendInfo::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean FriendInfo::<IsOnline>k__BackingField
	bool ___U3CIsOnlineU3Ek__BackingField_1;
	// System.String FriendInfo::<Room>k__BackingField
	String_t* ___U3CRoomU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FriendInfo_t533296844, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIsOnlineU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FriendInfo_t533296844, ___U3CIsOnlineU3Ek__BackingField_1)); }
	inline bool get_U3CIsOnlineU3Ek__BackingField_1() const { return ___U3CIsOnlineU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsOnlineU3Ek__BackingField_1() { return &___U3CIsOnlineU3Ek__BackingField_1; }
	inline void set_U3CIsOnlineU3Ek__BackingField_1(bool value)
	{
		___U3CIsOnlineU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRoomU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FriendInfo_t533296844, ___U3CRoomU3Ek__BackingField_2)); }
	inline String_t* get_U3CRoomU3Ek__BackingField_2() const { return ___U3CRoomU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRoomU3Ek__BackingField_2() { return &___U3CRoomU3Ek__BackingField_2; }
	inline void set_U3CRoomU3Ek__BackingField_2(String_t* value)
	{
		___U3CRoomU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRoomU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRIENDINFO_T533296844_H
#ifndef U3CDOESLAYERSYNCHRONIZETYPEEXISTU3EC__ANONSTOREY0_T2411181974_H
#define U3CDOESLAYERSYNCHRONIZETYPEEXISTU3EC__ANONSTOREY0_T2411181974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/<DoesLayerSynchronizeTypeExist>c__AnonStorey0
struct  U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey0_t2411181974  : public RuntimeObject
{
public:
	// System.Int32 PhotonAnimatorView/<DoesLayerSynchronizeTypeExist>c__AnonStorey0::layerIndex
	int32_t ___layerIndex_0;

public:
	inline static int32_t get_offset_of_layerIndex_0() { return static_cast<int32_t>(offsetof(U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey0_t2411181974, ___layerIndex_0)); }
	inline int32_t get_layerIndex_0() const { return ___layerIndex_0; }
	inline int32_t* get_address_of_layerIndex_0() { return &___layerIndex_0; }
	inline void set_layerIndex_0(int32_t value)
	{
		___layerIndex_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOESLAYERSYNCHRONIZETYPEEXISTU3EC__ANONSTOREY0_T2411181974_H
#ifndef PHOTONPLAYER_T3305149557_H
#define PHOTONPLAYER_T3305149557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonPlayer
struct  PhotonPlayer_t3305149557  : public RuntimeObject
{
public:
	// System.Int32 PhotonPlayer::actorID
	int32_t ___actorID_0;
	// System.String PhotonPlayer::nameField
	String_t* ___nameField_1;
	// System.String PhotonPlayer::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_2;
	// System.Boolean PhotonPlayer::IsLocal
	bool ___IsLocal_3;
	// System.Boolean PhotonPlayer::<IsInactive>k__BackingField
	bool ___U3CIsInactiveU3Ek__BackingField_4;
	// ExitGames.Client.Photon.Hashtable PhotonPlayer::<CustomProperties>k__BackingField
	Hashtable_t1048209202 * ___U3CCustomPropertiesU3Ek__BackingField_5;
	// System.Object PhotonPlayer::TagObject
	RuntimeObject * ___TagObject_6;

public:
	inline static int32_t get_offset_of_actorID_0() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___actorID_0)); }
	inline int32_t get_actorID_0() const { return ___actorID_0; }
	inline int32_t* get_address_of_actorID_0() { return &___actorID_0; }
	inline void set_actorID_0(int32_t value)
	{
		___actorID_0 = value;
	}

	inline static int32_t get_offset_of_nameField_1() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___nameField_1)); }
	inline String_t* get_nameField_1() const { return ___nameField_1; }
	inline String_t** get_address_of_nameField_1() { return &___nameField_1; }
	inline void set_nameField_1(String_t* value)
	{
		___nameField_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameField_1), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___U3CUserIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_2() const { return ___U3CUserIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_2() { return &___U3CUserIdU3Ek__BackingField_2; }
	inline void set_U3CUserIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_IsLocal_3() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___IsLocal_3)); }
	inline bool get_IsLocal_3() const { return ___IsLocal_3; }
	inline bool* get_address_of_IsLocal_3() { return &___IsLocal_3; }
	inline void set_IsLocal_3(bool value)
	{
		___IsLocal_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsInactiveU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___U3CIsInactiveU3Ek__BackingField_4)); }
	inline bool get_U3CIsInactiveU3Ek__BackingField_4() const { return ___U3CIsInactiveU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsInactiveU3Ek__BackingField_4() { return &___U3CIsInactiveU3Ek__BackingField_4; }
	inline void set_U3CIsInactiveU3Ek__BackingField_4(bool value)
	{
		___U3CIsInactiveU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCustomPropertiesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___U3CCustomPropertiesU3Ek__BackingField_5)); }
	inline Hashtable_t1048209202 * get_U3CCustomPropertiesU3Ek__BackingField_5() const { return ___U3CCustomPropertiesU3Ek__BackingField_5; }
	inline Hashtable_t1048209202 ** get_address_of_U3CCustomPropertiesU3Ek__BackingField_5() { return &___U3CCustomPropertiesU3Ek__BackingField_5; }
	inline void set_U3CCustomPropertiesU3Ek__BackingField_5(Hashtable_t1048209202 * value)
	{
		___U3CCustomPropertiesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomPropertiesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_TagObject_6() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___TagObject_6)); }
	inline RuntimeObject * get_TagObject_6() const { return ___TagObject_6; }
	inline RuntimeObject ** get_address_of_TagObject_6() { return &___TagObject_6; }
	inline void set_TagObject_6(RuntimeObject * value)
	{
		___TagObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___TagObject_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPLAYER_T3305149557_H
#ifndef PHOTONSTREAMQUEUE_T3244431384_H
#define PHOTONSTREAMQUEUE_T3244431384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonStreamQueue
struct  PhotonStreamQueue_t3244431384  : public RuntimeObject
{
public:
	// System.Int32 PhotonStreamQueue::m_SampleRate
	int32_t ___m_SampleRate_0;
	// System.Int32 PhotonStreamQueue::m_SampleCount
	int32_t ___m_SampleCount_1;
	// System.Int32 PhotonStreamQueue::m_ObjectsPerSample
	int32_t ___m_ObjectsPerSample_2;
	// System.Single PhotonStreamQueue::m_LastSampleTime
	float ___m_LastSampleTime_3;
	// System.Int32 PhotonStreamQueue::m_LastFrameCount
	int32_t ___m_LastFrameCount_4;
	// System.Int32 PhotonStreamQueue::m_NextObjectIndex
	int32_t ___m_NextObjectIndex_5;
	// System.Collections.Generic.List`1<System.Object> PhotonStreamQueue::m_Objects
	List_1_t257213610 * ___m_Objects_6;
	// System.Boolean PhotonStreamQueue::m_IsWriting
	bool ___m_IsWriting_7;

public:
	inline static int32_t get_offset_of_m_SampleRate_0() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_t3244431384, ___m_SampleRate_0)); }
	inline int32_t get_m_SampleRate_0() const { return ___m_SampleRate_0; }
	inline int32_t* get_address_of_m_SampleRate_0() { return &___m_SampleRate_0; }
	inline void set_m_SampleRate_0(int32_t value)
	{
		___m_SampleRate_0 = value;
	}

	inline static int32_t get_offset_of_m_SampleCount_1() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_t3244431384, ___m_SampleCount_1)); }
	inline int32_t get_m_SampleCount_1() const { return ___m_SampleCount_1; }
	inline int32_t* get_address_of_m_SampleCount_1() { return &___m_SampleCount_1; }
	inline void set_m_SampleCount_1(int32_t value)
	{
		___m_SampleCount_1 = value;
	}

	inline static int32_t get_offset_of_m_ObjectsPerSample_2() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_t3244431384, ___m_ObjectsPerSample_2)); }
	inline int32_t get_m_ObjectsPerSample_2() const { return ___m_ObjectsPerSample_2; }
	inline int32_t* get_address_of_m_ObjectsPerSample_2() { return &___m_ObjectsPerSample_2; }
	inline void set_m_ObjectsPerSample_2(int32_t value)
	{
		___m_ObjectsPerSample_2 = value;
	}

	inline static int32_t get_offset_of_m_LastSampleTime_3() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_t3244431384, ___m_LastSampleTime_3)); }
	inline float get_m_LastSampleTime_3() const { return ___m_LastSampleTime_3; }
	inline float* get_address_of_m_LastSampleTime_3() { return &___m_LastSampleTime_3; }
	inline void set_m_LastSampleTime_3(float value)
	{
		___m_LastSampleTime_3 = value;
	}

	inline static int32_t get_offset_of_m_LastFrameCount_4() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_t3244431384, ___m_LastFrameCount_4)); }
	inline int32_t get_m_LastFrameCount_4() const { return ___m_LastFrameCount_4; }
	inline int32_t* get_address_of_m_LastFrameCount_4() { return &___m_LastFrameCount_4; }
	inline void set_m_LastFrameCount_4(int32_t value)
	{
		___m_LastFrameCount_4 = value;
	}

	inline static int32_t get_offset_of_m_NextObjectIndex_5() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_t3244431384, ___m_NextObjectIndex_5)); }
	inline int32_t get_m_NextObjectIndex_5() const { return ___m_NextObjectIndex_5; }
	inline int32_t* get_address_of_m_NextObjectIndex_5() { return &___m_NextObjectIndex_5; }
	inline void set_m_NextObjectIndex_5(int32_t value)
	{
		___m_NextObjectIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_Objects_6() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_t3244431384, ___m_Objects_6)); }
	inline List_1_t257213610 * get_m_Objects_6() const { return ___m_Objects_6; }
	inline List_1_t257213610 ** get_address_of_m_Objects_6() { return &___m_Objects_6; }
	inline void set_m_Objects_6(List_1_t257213610 * value)
	{
		___m_Objects_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Objects_6), value);
	}

	inline static int32_t get_offset_of_m_IsWriting_7() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_t3244431384, ___m_IsWriting_7)); }
	inline bool get_m_IsWriting_7() const { return ___m_IsWriting_7; }
	inline bool* get_address_of_m_IsWriting_7() { return &___m_IsWriting_7; }
	inline void set_m_IsWriting_7(bool value)
	{
		___m_IsWriting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONSTREAMQUEUE_T3244431384_H
#ifndef PHOTONPINGMANAGER_T630892274_H
#define PHOTONPINGMANAGER_T630892274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonPingManager
struct  PhotonPingManager_t630892274  : public RuntimeObject
{
public:
	// System.Boolean PhotonPingManager::UseNative
	bool ___UseNative_0;
	// System.Int32 PhotonPingManager::PingsRunning
	int32_t ___PingsRunning_5;

public:
	inline static int32_t get_offset_of_UseNative_0() { return static_cast<int32_t>(offsetof(PhotonPingManager_t630892274, ___UseNative_0)); }
	inline bool get_UseNative_0() const { return ___UseNative_0; }
	inline bool* get_address_of_UseNative_0() { return &___UseNative_0; }
	inline void set_UseNative_0(bool value)
	{
		___UseNative_0 = value;
	}

	inline static int32_t get_offset_of_PingsRunning_5() { return static_cast<int32_t>(offsetof(PhotonPingManager_t630892274, ___PingsRunning_5)); }
	inline int32_t get_PingsRunning_5() const { return ___PingsRunning_5; }
	inline int32_t* get_address_of_PingsRunning_5() { return &___PingsRunning_5; }
	inline void set_PingsRunning_5(int32_t value)
	{
		___PingsRunning_5 = value;
	}
};

struct PhotonPingManager_t630892274_StaticFields
{
public:
	// System.Int32 PhotonPingManager::Attempts
	int32_t ___Attempts_1;
	// System.Boolean PhotonPingManager::IgnoreInitialAttempt
	bool ___IgnoreInitialAttempt_2;
	// System.Int32 PhotonPingManager::MaxMilliseconsPerPing
	int32_t ___MaxMilliseconsPerPing_3;

public:
	inline static int32_t get_offset_of_Attempts_1() { return static_cast<int32_t>(offsetof(PhotonPingManager_t630892274_StaticFields, ___Attempts_1)); }
	inline int32_t get_Attempts_1() const { return ___Attempts_1; }
	inline int32_t* get_address_of_Attempts_1() { return &___Attempts_1; }
	inline void set_Attempts_1(int32_t value)
	{
		___Attempts_1 = value;
	}

	inline static int32_t get_offset_of_IgnoreInitialAttempt_2() { return static_cast<int32_t>(offsetof(PhotonPingManager_t630892274_StaticFields, ___IgnoreInitialAttempt_2)); }
	inline bool get_IgnoreInitialAttempt_2() const { return ___IgnoreInitialAttempt_2; }
	inline bool* get_address_of_IgnoreInitialAttempt_2() { return &___IgnoreInitialAttempt_2; }
	inline void set_IgnoreInitialAttempt_2(bool value)
	{
		___IgnoreInitialAttempt_2 = value;
	}

	inline static int32_t get_offset_of_MaxMilliseconsPerPing_3() { return static_cast<int32_t>(offsetof(PhotonPingManager_t630892274_StaticFields, ___MaxMilliseconsPerPing_3)); }
	inline int32_t get_MaxMilliseconsPerPing_3() const { return ___MaxMilliseconsPerPing_3; }
	inline int32_t* get_address_of_MaxMilliseconsPerPing_3() { return &___MaxMilliseconsPerPing_3; }
	inline void set_MaxMilliseconsPerPing_3(int32_t value)
	{
		___MaxMilliseconsPerPing_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPINGMANAGER_T630892274_H
#ifndef U3CPINGSOCKETU3EC__ITERATOR0_T2858604848_H
#define U3CPINGSOCKETU3EC__ITERATOR0_T2858604848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonPingManager/<PingSocket>c__Iterator0
struct  U3CPingSocketU3Ec__Iterator0_t2858604848  : public RuntimeObject
{
public:
	// Region PhotonPingManager/<PingSocket>c__Iterator0::region
	Region_t3684225262 * ___region_0;
	// ExitGames.Client.Photon.PhotonPing PhotonPingManager/<PingSocket>c__Iterator0::<ping>__1
	PhotonPing_t2371975946 * ___U3CpingU3E__1_1;
	// System.Single PhotonPingManager/<PingSocket>c__Iterator0::<rttSum>__0
	float ___U3CrttSumU3E__0_2;
	// System.Int32 PhotonPingManager/<PingSocket>c__Iterator0::<replyCount>__0
	int32_t ___U3CreplyCountU3E__0_3;
	// System.String PhotonPingManager/<PingSocket>c__Iterator0::<regionAddress>__0
	String_t* ___U3CregionAddressU3E__0_4;
	// System.Int32 PhotonPingManager/<PingSocket>c__Iterator0::<indexOfColon>__0
	int32_t ___U3CindexOfColonU3E__0_5;
	// System.Int32 PhotonPingManager/<PingSocket>c__Iterator0::<indexOfProtocol>__0
	int32_t ___U3CindexOfProtocolU3E__0_6;
	// System.Int32 PhotonPingManager/<PingSocket>c__Iterator0::<i>__2
	int32_t ___U3CiU3E__2_7;
	// System.Boolean PhotonPingManager/<PingSocket>c__Iterator0::<overtime>__3
	bool ___U3CovertimeU3E__3_8;
	// System.Diagnostics.Stopwatch PhotonPingManager/<PingSocket>c__Iterator0::<sw>__3
	Stopwatch_t305734070 * ___U3CswU3E__3_9;
	// System.Int32 PhotonPingManager/<PingSocket>c__Iterator0::<rtt>__3
	int32_t ___U3CrttU3E__3_10;
	// PhotonPingManager PhotonPingManager/<PingSocket>c__Iterator0::$this
	PhotonPingManager_t630892274 * ___U24this_11;
	// System.Object PhotonPingManager/<PingSocket>c__Iterator0::$current
	RuntimeObject * ___U24current_12;
	// System.Boolean PhotonPingManager/<PingSocket>c__Iterator0::$disposing
	bool ___U24disposing_13;
	// System.Int32 PhotonPingManager/<PingSocket>c__Iterator0::$PC
	int32_t ___U24PC_14;

public:
	inline static int32_t get_offset_of_region_0() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___region_0)); }
	inline Region_t3684225262 * get_region_0() const { return ___region_0; }
	inline Region_t3684225262 ** get_address_of_region_0() { return &___region_0; }
	inline void set_region_0(Region_t3684225262 * value)
	{
		___region_0 = value;
		Il2CppCodeGenWriteBarrier((&___region_0), value);
	}

	inline static int32_t get_offset_of_U3CpingU3E__1_1() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CpingU3E__1_1)); }
	inline PhotonPing_t2371975946 * get_U3CpingU3E__1_1() const { return ___U3CpingU3E__1_1; }
	inline PhotonPing_t2371975946 ** get_address_of_U3CpingU3E__1_1() { return &___U3CpingU3E__1_1; }
	inline void set_U3CpingU3E__1_1(PhotonPing_t2371975946 * value)
	{
		___U3CpingU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpingU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CrttSumU3E__0_2() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CrttSumU3E__0_2)); }
	inline float get_U3CrttSumU3E__0_2() const { return ___U3CrttSumU3E__0_2; }
	inline float* get_address_of_U3CrttSumU3E__0_2() { return &___U3CrttSumU3E__0_2; }
	inline void set_U3CrttSumU3E__0_2(float value)
	{
		___U3CrttSumU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CreplyCountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CreplyCountU3E__0_3)); }
	inline int32_t get_U3CreplyCountU3E__0_3() const { return ___U3CreplyCountU3E__0_3; }
	inline int32_t* get_address_of_U3CreplyCountU3E__0_3() { return &___U3CreplyCountU3E__0_3; }
	inline void set_U3CreplyCountU3E__0_3(int32_t value)
	{
		___U3CreplyCountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CregionAddressU3E__0_4() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CregionAddressU3E__0_4)); }
	inline String_t* get_U3CregionAddressU3E__0_4() const { return ___U3CregionAddressU3E__0_4; }
	inline String_t** get_address_of_U3CregionAddressU3E__0_4() { return &___U3CregionAddressU3E__0_4; }
	inline void set_U3CregionAddressU3E__0_4(String_t* value)
	{
		___U3CregionAddressU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CregionAddressU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CindexOfColonU3E__0_5() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CindexOfColonU3E__0_5)); }
	inline int32_t get_U3CindexOfColonU3E__0_5() const { return ___U3CindexOfColonU3E__0_5; }
	inline int32_t* get_address_of_U3CindexOfColonU3E__0_5() { return &___U3CindexOfColonU3E__0_5; }
	inline void set_U3CindexOfColonU3E__0_5(int32_t value)
	{
		___U3CindexOfColonU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CindexOfProtocolU3E__0_6() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CindexOfProtocolU3E__0_6)); }
	inline int32_t get_U3CindexOfProtocolU3E__0_6() const { return ___U3CindexOfProtocolU3E__0_6; }
	inline int32_t* get_address_of_U3CindexOfProtocolU3E__0_6() { return &___U3CindexOfProtocolU3E__0_6; }
	inline void set_U3CindexOfProtocolU3E__0_6(int32_t value)
	{
		___U3CindexOfProtocolU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__2_7() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CiU3E__2_7)); }
	inline int32_t get_U3CiU3E__2_7() const { return ___U3CiU3E__2_7; }
	inline int32_t* get_address_of_U3CiU3E__2_7() { return &___U3CiU3E__2_7; }
	inline void set_U3CiU3E__2_7(int32_t value)
	{
		___U3CiU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CovertimeU3E__3_8() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CovertimeU3E__3_8)); }
	inline bool get_U3CovertimeU3E__3_8() const { return ___U3CovertimeU3E__3_8; }
	inline bool* get_address_of_U3CovertimeU3E__3_8() { return &___U3CovertimeU3E__3_8; }
	inline void set_U3CovertimeU3E__3_8(bool value)
	{
		___U3CovertimeU3E__3_8 = value;
	}

	inline static int32_t get_offset_of_U3CswU3E__3_9() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CswU3E__3_9)); }
	inline Stopwatch_t305734070 * get_U3CswU3E__3_9() const { return ___U3CswU3E__3_9; }
	inline Stopwatch_t305734070 ** get_address_of_U3CswU3E__3_9() { return &___U3CswU3E__3_9; }
	inline void set_U3CswU3E__3_9(Stopwatch_t305734070 * value)
	{
		___U3CswU3E__3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CswU3E__3_9), value);
	}

	inline static int32_t get_offset_of_U3CrttU3E__3_10() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U3CrttU3E__3_10)); }
	inline int32_t get_U3CrttU3E__3_10() const { return ___U3CrttU3E__3_10; }
	inline int32_t* get_address_of_U3CrttU3E__3_10() { return &___U3CrttU3E__3_10; }
	inline void set_U3CrttU3E__3_10(int32_t value)
	{
		___U3CrttU3E__3_10 = value;
	}

	inline static int32_t get_offset_of_U24this_11() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U24this_11)); }
	inline PhotonPingManager_t630892274 * get_U24this_11() const { return ___U24this_11; }
	inline PhotonPingManager_t630892274 ** get_address_of_U24this_11() { return &___U24this_11; }
	inline void set_U24this_11(PhotonPingManager_t630892274 * value)
	{
		___U24this_11 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_11), value);
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U24current_12)); }
	inline RuntimeObject * get_U24current_12() const { return ___U24current_12; }
	inline RuntimeObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(RuntimeObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_12), value);
	}

	inline static int32_t get_offset_of_U24disposing_13() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U24disposing_13)); }
	inline bool get_U24disposing_13() const { return ___U24disposing_13; }
	inline bool* get_address_of_U24disposing_13() { return &___U24disposing_13; }
	inline void set_U24disposing_13(bool value)
	{
		___U24disposing_13 = value;
	}

	inline static int32_t get_offset_of_U24PC_14() { return static_cast<int32_t>(offsetof(U3CPingSocketU3Ec__Iterator0_t2858604848, ___U24PC_14)); }
	inline int32_t get_U24PC_14() const { return ___U24PC_14; }
	inline int32_t* get_address_of_U24PC_14() { return &___U24PC_14; }
	inline void set_U24PC_14(int32_t value)
	{
		___U24PC_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPINGSOCKETU3EC__ITERATOR0_T2858604848_H
#ifndef WEBRPCRESPONSE_T4177102182_H
#define WEBRPCRESPONSE_T4177102182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebRpcResponse
struct  WebRpcResponse_t4177102182  : public RuntimeObject
{
public:
	// System.String WebRpcResponse::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 WebRpcResponse::<ReturnCode>k__BackingField
	int32_t ___U3CReturnCodeU3Ek__BackingField_1;
	// System.String WebRpcResponse::<DebugMessage>k__BackingField
	String_t* ___U3CDebugMessageU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> WebRpcResponse::<Parameters>k__BackingField
	Dictionary_2_t2865362463 * ___U3CParametersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebRpcResponse_t4177102182, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CReturnCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebRpcResponse_t4177102182, ___U3CReturnCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CReturnCodeU3Ek__BackingField_1() const { return ___U3CReturnCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CReturnCodeU3Ek__BackingField_1() { return &___U3CReturnCodeU3Ek__BackingField_1; }
	inline void set_U3CReturnCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CReturnCodeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDebugMessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebRpcResponse_t4177102182, ___U3CDebugMessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CDebugMessageU3Ek__BackingField_2() const { return ___U3CDebugMessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CDebugMessageU3Ek__BackingField_2() { return &___U3CDebugMessageU3Ek__BackingField_2; }
	inline void set_U3CDebugMessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CDebugMessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDebugMessageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebRpcResponse_t4177102182, ___U3CParametersU3Ek__BackingField_3)); }
	inline Dictionary_2_t2865362463 * get_U3CParametersU3Ek__BackingField_3() const { return ___U3CParametersU3Ek__BackingField_3; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CParametersU3Ek__BackingField_3() { return &___U3CParametersU3Ek__BackingField_3; }
	inline void set_U3CParametersU3Ek__BackingField_3(Dictionary_2_t2865362463 * value)
	{
		___U3CParametersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRPCRESPONSE_T4177102182_H
#ifndef SCENEMANAGERHELPER_T3665721098_H
#define SCENEMANAGERHELPER_T3665721098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneManagerHelper
struct  SceneManagerHelper_t3665721098  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMANAGERHELPER_T3665721098_H
#ifndef PHOTONSTREAM_T1003850889_H
#define PHOTONSTREAM_T1003850889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonStream
struct  PhotonStream_t1003850889  : public RuntimeObject
{
public:
	// System.Boolean PhotonStream::write
	bool ___write_0;
	// System.Collections.Generic.Queue`1<System.Object> PhotonStream::writeData
	Queue_1_t2926365658 * ___writeData_1;
	// System.Object[] PhotonStream::readData
	ObjectU5BU5D_t2843939325* ___readData_2;
	// System.Byte PhotonStream::currentItem
	uint8_t ___currentItem_3;

public:
	inline static int32_t get_offset_of_write_0() { return static_cast<int32_t>(offsetof(PhotonStream_t1003850889, ___write_0)); }
	inline bool get_write_0() const { return ___write_0; }
	inline bool* get_address_of_write_0() { return &___write_0; }
	inline void set_write_0(bool value)
	{
		___write_0 = value;
	}

	inline static int32_t get_offset_of_writeData_1() { return static_cast<int32_t>(offsetof(PhotonStream_t1003850889, ___writeData_1)); }
	inline Queue_1_t2926365658 * get_writeData_1() const { return ___writeData_1; }
	inline Queue_1_t2926365658 ** get_address_of_writeData_1() { return &___writeData_1; }
	inline void set_writeData_1(Queue_1_t2926365658 * value)
	{
		___writeData_1 = value;
		Il2CppCodeGenWriteBarrier((&___writeData_1), value);
	}

	inline static int32_t get_offset_of_readData_2() { return static_cast<int32_t>(offsetof(PhotonStream_t1003850889, ___readData_2)); }
	inline ObjectU5BU5D_t2843939325* get_readData_2() const { return ___readData_2; }
	inline ObjectU5BU5D_t2843939325** get_address_of_readData_2() { return &___readData_2; }
	inline void set_readData_2(ObjectU5BU5D_t2843939325* value)
	{
		___readData_2 = value;
		Il2CppCodeGenWriteBarrier((&___readData_2), value);
	}

	inline static int32_t get_offset_of_currentItem_3() { return static_cast<int32_t>(offsetof(PhotonStream_t1003850889, ___currentItem_3)); }
	inline uint8_t get_currentItem_3() const { return ___currentItem_3; }
	inline uint8_t* get_address_of_currentItem_3() { return &___currentItem_3; }
	inline void set_currentItem_3(uint8_t value)
	{
		___currentItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONSTREAM_T1003850889_H
#ifndef PUNEVENT_T699376346_H
#define PUNEVENT_T699376346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PunEvent
struct  PunEvent_t699376346  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNEVENT_T699376346_H
#ifndef ROOMINFO_T3170295620_H
#define ROOMINFO_T3170295620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomInfo
struct  RoomInfo_t3170295620  : public RuntimeObject
{
public:
	// System.Boolean RoomInfo::<removedFromList>k__BackingField
	bool ___U3CremovedFromListU3Ek__BackingField_0;
	// ExitGames.Client.Photon.Hashtable RoomInfo::customPropertiesField
	Hashtable_t1048209202 * ___customPropertiesField_1;
	// System.Byte RoomInfo::maxPlayersField
	uint8_t ___maxPlayersField_2;
	// System.String[] RoomInfo::expectedUsersField
	StringU5BU5D_t1281789340* ___expectedUsersField_3;
	// System.Boolean RoomInfo::openField
	bool ___openField_4;
	// System.Boolean RoomInfo::visibleField
	bool ___visibleField_5;
	// System.Boolean RoomInfo::autoCleanUpField
	bool ___autoCleanUpField_6;
	// System.String RoomInfo::nameField
	String_t* ___nameField_7;
	// System.Int32 RoomInfo::masterClientIdField
	int32_t ___masterClientIdField_8;
	// System.Boolean RoomInfo::<serverSideMasterClient>k__BackingField
	bool ___U3CserverSideMasterClientU3Ek__BackingField_9;
	// System.Int32 RoomInfo::<PlayerCount>k__BackingField
	int32_t ___U3CPlayerCountU3Ek__BackingField_10;
	// System.Boolean RoomInfo::<IsLocalClientInside>k__BackingField
	bool ___U3CIsLocalClientInsideU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CremovedFromListU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___U3CremovedFromListU3Ek__BackingField_0)); }
	inline bool get_U3CremovedFromListU3Ek__BackingField_0() const { return ___U3CremovedFromListU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CremovedFromListU3Ek__BackingField_0() { return &___U3CremovedFromListU3Ek__BackingField_0; }
	inline void set_U3CremovedFromListU3Ek__BackingField_0(bool value)
	{
		___U3CremovedFromListU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_customPropertiesField_1() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___customPropertiesField_1)); }
	inline Hashtable_t1048209202 * get_customPropertiesField_1() const { return ___customPropertiesField_1; }
	inline Hashtable_t1048209202 ** get_address_of_customPropertiesField_1() { return &___customPropertiesField_1; }
	inline void set_customPropertiesField_1(Hashtable_t1048209202 * value)
	{
		___customPropertiesField_1 = value;
		Il2CppCodeGenWriteBarrier((&___customPropertiesField_1), value);
	}

	inline static int32_t get_offset_of_maxPlayersField_2() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___maxPlayersField_2)); }
	inline uint8_t get_maxPlayersField_2() const { return ___maxPlayersField_2; }
	inline uint8_t* get_address_of_maxPlayersField_2() { return &___maxPlayersField_2; }
	inline void set_maxPlayersField_2(uint8_t value)
	{
		___maxPlayersField_2 = value;
	}

	inline static int32_t get_offset_of_expectedUsersField_3() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___expectedUsersField_3)); }
	inline StringU5BU5D_t1281789340* get_expectedUsersField_3() const { return ___expectedUsersField_3; }
	inline StringU5BU5D_t1281789340** get_address_of_expectedUsersField_3() { return &___expectedUsersField_3; }
	inline void set_expectedUsersField_3(StringU5BU5D_t1281789340* value)
	{
		___expectedUsersField_3 = value;
		Il2CppCodeGenWriteBarrier((&___expectedUsersField_3), value);
	}

	inline static int32_t get_offset_of_openField_4() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___openField_4)); }
	inline bool get_openField_4() const { return ___openField_4; }
	inline bool* get_address_of_openField_4() { return &___openField_4; }
	inline void set_openField_4(bool value)
	{
		___openField_4 = value;
	}

	inline static int32_t get_offset_of_visibleField_5() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___visibleField_5)); }
	inline bool get_visibleField_5() const { return ___visibleField_5; }
	inline bool* get_address_of_visibleField_5() { return &___visibleField_5; }
	inline void set_visibleField_5(bool value)
	{
		___visibleField_5 = value;
	}

	inline static int32_t get_offset_of_autoCleanUpField_6() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___autoCleanUpField_6)); }
	inline bool get_autoCleanUpField_6() const { return ___autoCleanUpField_6; }
	inline bool* get_address_of_autoCleanUpField_6() { return &___autoCleanUpField_6; }
	inline void set_autoCleanUpField_6(bool value)
	{
		___autoCleanUpField_6 = value;
	}

	inline static int32_t get_offset_of_nameField_7() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___nameField_7)); }
	inline String_t* get_nameField_7() const { return ___nameField_7; }
	inline String_t** get_address_of_nameField_7() { return &___nameField_7; }
	inline void set_nameField_7(String_t* value)
	{
		___nameField_7 = value;
		Il2CppCodeGenWriteBarrier((&___nameField_7), value);
	}

	inline static int32_t get_offset_of_masterClientIdField_8() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___masterClientIdField_8)); }
	inline int32_t get_masterClientIdField_8() const { return ___masterClientIdField_8; }
	inline int32_t* get_address_of_masterClientIdField_8() { return &___masterClientIdField_8; }
	inline void set_masterClientIdField_8(int32_t value)
	{
		___masterClientIdField_8 = value;
	}

	inline static int32_t get_offset_of_U3CserverSideMasterClientU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___U3CserverSideMasterClientU3Ek__BackingField_9)); }
	inline bool get_U3CserverSideMasterClientU3Ek__BackingField_9() const { return ___U3CserverSideMasterClientU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CserverSideMasterClientU3Ek__BackingField_9() { return &___U3CserverSideMasterClientU3Ek__BackingField_9; }
	inline void set_U3CserverSideMasterClientU3Ek__BackingField_9(bool value)
	{
		___U3CserverSideMasterClientU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CPlayerCountU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___U3CPlayerCountU3Ek__BackingField_10)); }
	inline int32_t get_U3CPlayerCountU3Ek__BackingField_10() const { return ___U3CPlayerCountU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CPlayerCountU3Ek__BackingField_10() { return &___U3CPlayerCountU3Ek__BackingField_10; }
	inline void set_U3CPlayerCountU3Ek__BackingField_10(int32_t value)
	{
		___U3CPlayerCountU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CIsLocalClientInsideU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___U3CIsLocalClientInsideU3Ek__BackingField_11)); }
	inline bool get_U3CIsLocalClientInsideU3Ek__BackingField_11() const { return ___U3CIsLocalClientInsideU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsLocalClientInsideU3Ek__BackingField_11() { return &___U3CIsLocalClientInsideU3Ek__BackingField_11; }
	inline void set_U3CIsLocalClientInsideU3Ek__BackingField_11(bool value)
	{
		___U3CIsLocalClientInsideU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMINFO_T3170295620_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef OPERATIONCODE_T3680831435_H
#define OPERATIONCODE_T3680831435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OperationCode
struct  OperationCode_t3680831435  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONCODE_T3680831435_H
#ifndef PARAMETERCODE_T2914727942_H
#define PARAMETERCODE_T2914727942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParameterCode
struct  ParameterCode_t2914727942  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERCODE_T2914727942_H
#ifndef EVENTCODE_T3064356988_H
#define EVENTCODE_T3064356988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventCode
struct  EventCode_t3064356988  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTCODE_T3064356988_H
#ifndef ERRORCODE_T835159227_H
#define ERRORCODE_T835159227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorCode
struct  ErrorCode_t835159227  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCODE_T835159227_H
#ifndef ACTORPROPERTIES_T3254499384_H
#define ACTORPROPERTIES_T3254499384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActorProperties
struct  ActorProperties_t3254499384  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTORPROPERTIES_T3254499384_H
#ifndef GAMEPROPERTYKEY_T1877143020_H
#define GAMEPROPERTYKEY_T1877143020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePropertyKey
struct  GamePropertyKey_t1877143020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPROPERTYKEY_T1877143020_H
#ifndef ENTERROOMPARAMS_T3960472384_H
#define ENTERROOMPARAMS_T3960472384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnterRoomParams
struct  EnterRoomParams_t3960472384  : public RuntimeObject
{
public:
	// System.String EnterRoomParams::RoomName
	String_t* ___RoomName_0;
	// RoomOptions EnterRoomParams::RoomOptions
	RoomOptions_t1787645948 * ___RoomOptions_1;
	// TypedLobby EnterRoomParams::Lobby
	TypedLobby_t3336582029 * ___Lobby_2;
	// ExitGames.Client.Photon.Hashtable EnterRoomParams::PlayerProperties
	Hashtable_t1048209202 * ___PlayerProperties_3;
	// System.Boolean EnterRoomParams::OnGameServer
	bool ___OnGameServer_4;
	// System.Boolean EnterRoomParams::CreateIfNotExists
	bool ___CreateIfNotExists_5;
	// System.Boolean EnterRoomParams::RejoinOnly
	bool ___RejoinOnly_6;
	// System.String[] EnterRoomParams::ExpectedUsers
	StringU5BU5D_t1281789340* ___ExpectedUsers_7;

public:
	inline static int32_t get_offset_of_RoomName_0() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___RoomName_0)); }
	inline String_t* get_RoomName_0() const { return ___RoomName_0; }
	inline String_t** get_address_of_RoomName_0() { return &___RoomName_0; }
	inline void set_RoomName_0(String_t* value)
	{
		___RoomName_0 = value;
		Il2CppCodeGenWriteBarrier((&___RoomName_0), value);
	}

	inline static int32_t get_offset_of_RoomOptions_1() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___RoomOptions_1)); }
	inline RoomOptions_t1787645948 * get_RoomOptions_1() const { return ___RoomOptions_1; }
	inline RoomOptions_t1787645948 ** get_address_of_RoomOptions_1() { return &___RoomOptions_1; }
	inline void set_RoomOptions_1(RoomOptions_t1787645948 * value)
	{
		___RoomOptions_1 = value;
		Il2CppCodeGenWriteBarrier((&___RoomOptions_1), value);
	}

	inline static int32_t get_offset_of_Lobby_2() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___Lobby_2)); }
	inline TypedLobby_t3336582029 * get_Lobby_2() const { return ___Lobby_2; }
	inline TypedLobby_t3336582029 ** get_address_of_Lobby_2() { return &___Lobby_2; }
	inline void set_Lobby_2(TypedLobby_t3336582029 * value)
	{
		___Lobby_2 = value;
		Il2CppCodeGenWriteBarrier((&___Lobby_2), value);
	}

	inline static int32_t get_offset_of_PlayerProperties_3() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___PlayerProperties_3)); }
	inline Hashtable_t1048209202 * get_PlayerProperties_3() const { return ___PlayerProperties_3; }
	inline Hashtable_t1048209202 ** get_address_of_PlayerProperties_3() { return &___PlayerProperties_3; }
	inline void set_PlayerProperties_3(Hashtable_t1048209202 * value)
	{
		___PlayerProperties_3 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerProperties_3), value);
	}

	inline static int32_t get_offset_of_OnGameServer_4() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___OnGameServer_4)); }
	inline bool get_OnGameServer_4() const { return ___OnGameServer_4; }
	inline bool* get_address_of_OnGameServer_4() { return &___OnGameServer_4; }
	inline void set_OnGameServer_4(bool value)
	{
		___OnGameServer_4 = value;
	}

	inline static int32_t get_offset_of_CreateIfNotExists_5() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___CreateIfNotExists_5)); }
	inline bool get_CreateIfNotExists_5() const { return ___CreateIfNotExists_5; }
	inline bool* get_address_of_CreateIfNotExists_5() { return &___CreateIfNotExists_5; }
	inline void set_CreateIfNotExists_5(bool value)
	{
		___CreateIfNotExists_5 = value;
	}

	inline static int32_t get_offset_of_RejoinOnly_6() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___RejoinOnly_6)); }
	inline bool get_RejoinOnly_6() const { return ___RejoinOnly_6; }
	inline bool* get_address_of_RejoinOnly_6() { return &___RejoinOnly_6; }
	inline void set_RejoinOnly_6(bool value)
	{
		___RejoinOnly_6 = value;
	}

	inline static int32_t get_offset_of_ExpectedUsers_7() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___ExpectedUsers_7)); }
	inline StringU5BU5D_t1281789340* get_ExpectedUsers_7() const { return ___ExpectedUsers_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ExpectedUsers_7() { return &___ExpectedUsers_7; }
	inline void set_ExpectedUsers_7(StringU5BU5D_t1281789340* value)
	{
		___ExpectedUsers_7 = value;
		Il2CppCodeGenWriteBarrier((&___ExpectedUsers_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTERROOMPARAMS_T3960472384_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef ROOMOPTIONS_T1787645948_H
#define ROOMOPTIONS_T1787645948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomOptions
struct  RoomOptions_t1787645948  : public RuntimeObject
{
public:
	// System.Boolean RoomOptions::isVisibleField
	bool ___isVisibleField_0;
	// System.Boolean RoomOptions::isOpenField
	bool ___isOpenField_1;
	// System.Byte RoomOptions::MaxPlayers
	uint8_t ___MaxPlayers_2;
	// System.Int32 RoomOptions::PlayerTtl
	int32_t ___PlayerTtl_3;
	// System.Int32 RoomOptions::EmptyRoomTtl
	int32_t ___EmptyRoomTtl_4;
	// System.Boolean RoomOptions::cleanupCacheOnLeaveField
	bool ___cleanupCacheOnLeaveField_5;
	// ExitGames.Client.Photon.Hashtable RoomOptions::CustomRoomProperties
	Hashtable_t1048209202 * ___CustomRoomProperties_6;
	// System.String[] RoomOptions::CustomRoomPropertiesForLobby
	StringU5BU5D_t1281789340* ___CustomRoomPropertiesForLobby_7;
	// System.String[] RoomOptions::Plugins
	StringU5BU5D_t1281789340* ___Plugins_8;
	// System.Boolean RoomOptions::suppressRoomEventsField
	bool ___suppressRoomEventsField_9;
	// System.Boolean RoomOptions::publishUserIdField
	bool ___publishUserIdField_10;
	// System.Boolean RoomOptions::deleteNullPropertiesField
	bool ___deleteNullPropertiesField_11;

public:
	inline static int32_t get_offset_of_isVisibleField_0() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___isVisibleField_0)); }
	inline bool get_isVisibleField_0() const { return ___isVisibleField_0; }
	inline bool* get_address_of_isVisibleField_0() { return &___isVisibleField_0; }
	inline void set_isVisibleField_0(bool value)
	{
		___isVisibleField_0 = value;
	}

	inline static int32_t get_offset_of_isOpenField_1() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___isOpenField_1)); }
	inline bool get_isOpenField_1() const { return ___isOpenField_1; }
	inline bool* get_address_of_isOpenField_1() { return &___isOpenField_1; }
	inline void set_isOpenField_1(bool value)
	{
		___isOpenField_1 = value;
	}

	inline static int32_t get_offset_of_MaxPlayers_2() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___MaxPlayers_2)); }
	inline uint8_t get_MaxPlayers_2() const { return ___MaxPlayers_2; }
	inline uint8_t* get_address_of_MaxPlayers_2() { return &___MaxPlayers_2; }
	inline void set_MaxPlayers_2(uint8_t value)
	{
		___MaxPlayers_2 = value;
	}

	inline static int32_t get_offset_of_PlayerTtl_3() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___PlayerTtl_3)); }
	inline int32_t get_PlayerTtl_3() const { return ___PlayerTtl_3; }
	inline int32_t* get_address_of_PlayerTtl_3() { return &___PlayerTtl_3; }
	inline void set_PlayerTtl_3(int32_t value)
	{
		___PlayerTtl_3 = value;
	}

	inline static int32_t get_offset_of_EmptyRoomTtl_4() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___EmptyRoomTtl_4)); }
	inline int32_t get_EmptyRoomTtl_4() const { return ___EmptyRoomTtl_4; }
	inline int32_t* get_address_of_EmptyRoomTtl_4() { return &___EmptyRoomTtl_4; }
	inline void set_EmptyRoomTtl_4(int32_t value)
	{
		___EmptyRoomTtl_4 = value;
	}

	inline static int32_t get_offset_of_cleanupCacheOnLeaveField_5() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___cleanupCacheOnLeaveField_5)); }
	inline bool get_cleanupCacheOnLeaveField_5() const { return ___cleanupCacheOnLeaveField_5; }
	inline bool* get_address_of_cleanupCacheOnLeaveField_5() { return &___cleanupCacheOnLeaveField_5; }
	inline void set_cleanupCacheOnLeaveField_5(bool value)
	{
		___cleanupCacheOnLeaveField_5 = value;
	}

	inline static int32_t get_offset_of_CustomRoomProperties_6() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___CustomRoomProperties_6)); }
	inline Hashtable_t1048209202 * get_CustomRoomProperties_6() const { return ___CustomRoomProperties_6; }
	inline Hashtable_t1048209202 ** get_address_of_CustomRoomProperties_6() { return &___CustomRoomProperties_6; }
	inline void set_CustomRoomProperties_6(Hashtable_t1048209202 * value)
	{
		___CustomRoomProperties_6 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRoomProperties_6), value);
	}

	inline static int32_t get_offset_of_CustomRoomPropertiesForLobby_7() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___CustomRoomPropertiesForLobby_7)); }
	inline StringU5BU5D_t1281789340* get_CustomRoomPropertiesForLobby_7() const { return ___CustomRoomPropertiesForLobby_7; }
	inline StringU5BU5D_t1281789340** get_address_of_CustomRoomPropertiesForLobby_7() { return &___CustomRoomPropertiesForLobby_7; }
	inline void set_CustomRoomPropertiesForLobby_7(StringU5BU5D_t1281789340* value)
	{
		___CustomRoomPropertiesForLobby_7 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRoomPropertiesForLobby_7), value);
	}

	inline static int32_t get_offset_of_Plugins_8() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___Plugins_8)); }
	inline StringU5BU5D_t1281789340* get_Plugins_8() const { return ___Plugins_8; }
	inline StringU5BU5D_t1281789340** get_address_of_Plugins_8() { return &___Plugins_8; }
	inline void set_Plugins_8(StringU5BU5D_t1281789340* value)
	{
		___Plugins_8 = value;
		Il2CppCodeGenWriteBarrier((&___Plugins_8), value);
	}

	inline static int32_t get_offset_of_suppressRoomEventsField_9() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___suppressRoomEventsField_9)); }
	inline bool get_suppressRoomEventsField_9() const { return ___suppressRoomEventsField_9; }
	inline bool* get_address_of_suppressRoomEventsField_9() { return &___suppressRoomEventsField_9; }
	inline void set_suppressRoomEventsField_9(bool value)
	{
		___suppressRoomEventsField_9 = value;
	}

	inline static int32_t get_offset_of_publishUserIdField_10() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___publishUserIdField_10)); }
	inline bool get_publishUserIdField_10() const { return ___publishUserIdField_10; }
	inline bool* get_address_of_publishUserIdField_10() { return &___publishUserIdField_10; }
	inline void set_publishUserIdField_10(bool value)
	{
		___publishUserIdField_10 = value;
	}

	inline static int32_t get_offset_of_deleteNullPropertiesField_11() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___deleteNullPropertiesField_11)); }
	inline bool get_deleteNullPropertiesField_11() const { return ___deleteNullPropertiesField_11; }
	inline bool* get_address_of_deleteNullPropertiesField_11() { return &___deleteNullPropertiesField_11; }
	inline void set_deleteNullPropertiesField_11(bool value)
	{
		___deleteNullPropertiesField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMOPTIONS_T1787645948_H
#ifndef GIZMOTYPEDRAWER_T184707570_H
#define GIZMOTYPEDRAWER_T184707570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.GUI.GizmoTypeDrawer
struct  GizmoTypeDrawer_t184707570  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIZMOTYPEDRAWER_T184707570_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ROOM_T3759828263_H
#define ROOM_T3759828263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Room
struct  Room_t3759828263  : public RoomInfo_t3170295620
{
public:
	// System.String[] Room::<PropertiesListedInLobby>k__BackingField
	StringU5BU5D_t1281789340* ___U3CPropertiesListedInLobbyU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CPropertiesListedInLobbyU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Room_t3759828263, ___U3CPropertiesListedInLobbyU3Ek__BackingField_12)); }
	inline StringU5BU5D_t1281789340* get_U3CPropertiesListedInLobbyU3Ek__BackingField_12() const { return ___U3CPropertiesListedInLobbyU3Ek__BackingField_12; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CPropertiesListedInLobbyU3Ek__BackingField_12() { return &___U3CPropertiesListedInLobbyU3Ek__BackingField_12; }
	inline void set_U3CPropertiesListedInLobbyU3Ek__BackingField_12(StringU5BU5D_t1281789340* value)
	{
		___U3CPropertiesListedInLobbyU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesListedInLobbyU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOM_T3759828263_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef ENUMERATOR_T2750576585_H
#define ENUMERATOR_T2750576585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Region>
struct  Enumerator_t2750576585 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t861332708 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Region_t3684225262 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2750576585, ___l_0)); }
	inline List_1_t861332708 * get_l_0() const { return ___l_0; }
	inline List_1_t861332708 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t861332708 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2750576585, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2750576585, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2750576585, ___current_3)); }
	inline Region_t3684225262 * get_current_3() const { return ___current_3; }
	inline Region_t3684225262 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Region_t3684225262 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2750576585_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef PUNRPC_T1644934964_H
#define PUNRPC_T1644934964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PunRPC
struct  PunRPC_t1644934964  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNRPC_T1644934964_H
#ifndef PHOTONMESSAGEINFO_T3855471533_H
#define PHOTONMESSAGEINFO_T3855471533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonMessageInfo
struct  PhotonMessageInfo_t3855471533 
{
public:
	// System.Int32 PhotonMessageInfo::timeInt
	int32_t ___timeInt_0;
	// PhotonPlayer PhotonMessageInfo::sender
	PhotonPlayer_t3305149557 * ___sender_1;
	// PhotonView PhotonMessageInfo::photonView
	PhotonView_t2207721820 * ___photonView_2;

public:
	inline static int32_t get_offset_of_timeInt_0() { return static_cast<int32_t>(offsetof(PhotonMessageInfo_t3855471533, ___timeInt_0)); }
	inline int32_t get_timeInt_0() const { return ___timeInt_0; }
	inline int32_t* get_address_of_timeInt_0() { return &___timeInt_0; }
	inline void set_timeInt_0(int32_t value)
	{
		___timeInt_0 = value;
	}

	inline static int32_t get_offset_of_sender_1() { return static_cast<int32_t>(offsetof(PhotonMessageInfo_t3855471533, ___sender_1)); }
	inline PhotonPlayer_t3305149557 * get_sender_1() const { return ___sender_1; }
	inline PhotonPlayer_t3305149557 ** get_address_of_sender_1() { return &___sender_1; }
	inline void set_sender_1(PhotonPlayer_t3305149557 * value)
	{
		___sender_1 = value;
		Il2CppCodeGenWriteBarrier((&___sender_1), value);
	}

	inline static int32_t get_offset_of_photonView_2() { return static_cast<int32_t>(offsetof(PhotonMessageInfo_t3855471533, ___photonView_2)); }
	inline PhotonView_t2207721820 * get_photonView_2() const { return ___photonView_2; }
	inline PhotonView_t2207721820 ** get_address_of_photonView_2() { return &___photonView_2; }
	inline void set_photonView_2(PhotonView_t2207721820 * value)
	{
		___photonView_2 = value;
		Il2CppCodeGenWriteBarrier((&___photonView_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PhotonMessageInfo
struct PhotonMessageInfo_t3855471533_marshaled_pinvoke
{
	int32_t ___timeInt_0;
	PhotonPlayer_t3305149557 * ___sender_1;
	PhotonView_t2207721820 * ___photonView_2;
};
// Native definition for COM marshalling of PhotonMessageInfo
struct PhotonMessageInfo_t3855471533_marshaled_com
{
	int32_t ___timeInt_0;
	PhotonPlayer_t3305149557 * ___sender_1;
	PhotonView_t2207721820 * ___photonView_2;
};
#endif // PHOTONMESSAGEINFO_T3855471533_H
#ifndef PHOTONTRANSFORMVIEWPOSITIONCONTROL_T619346209_H
#define PHOTONTRANSFORMVIEWPOSITIONCONTROL_T619346209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewPositionControl
struct  PhotonTransformViewPositionControl_t619346209  : public RuntimeObject
{
public:
	// PhotonTransformViewPositionModel PhotonTransformViewPositionControl::m_Model
	PhotonTransformViewPositionModel_t2500134640 * ___m_Model_0;
	// System.Single PhotonTransformViewPositionControl::m_CurrentSpeed
	float ___m_CurrentSpeed_1;
	// System.Double PhotonTransformViewPositionControl::m_LastSerializeTime
	double ___m_LastSerializeTime_2;
	// UnityEngine.Vector3 PhotonTransformViewPositionControl::m_SynchronizedSpeed
	Vector3_t3722313464  ___m_SynchronizedSpeed_3;
	// System.Single PhotonTransformViewPositionControl::m_SynchronizedTurnSpeed
	float ___m_SynchronizedTurnSpeed_4;
	// UnityEngine.Vector3 PhotonTransformViewPositionControl::m_NetworkPosition
	Vector3_t3722313464  ___m_NetworkPosition_5;
	// System.Collections.Generic.Queue`1<UnityEngine.Vector3> PhotonTransformViewPositionControl::m_OldNetworkPositions
	Queue_1_t3568572958 * ___m_OldNetworkPositions_6;
	// System.Boolean PhotonTransformViewPositionControl::m_UpdatedPositionAfterOnSerialize
	bool ___m_UpdatedPositionAfterOnSerialize_7;

public:
	inline static int32_t get_offset_of_m_Model_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t619346209, ___m_Model_0)); }
	inline PhotonTransformViewPositionModel_t2500134640 * get_m_Model_0() const { return ___m_Model_0; }
	inline PhotonTransformViewPositionModel_t2500134640 ** get_address_of_m_Model_0() { return &___m_Model_0; }
	inline void set_m_Model_0(PhotonTransformViewPositionModel_t2500134640 * value)
	{
		___m_Model_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Model_0), value);
	}

	inline static int32_t get_offset_of_m_CurrentSpeed_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t619346209, ___m_CurrentSpeed_1)); }
	inline float get_m_CurrentSpeed_1() const { return ___m_CurrentSpeed_1; }
	inline float* get_address_of_m_CurrentSpeed_1() { return &___m_CurrentSpeed_1; }
	inline void set_m_CurrentSpeed_1(float value)
	{
		___m_CurrentSpeed_1 = value;
	}

	inline static int32_t get_offset_of_m_LastSerializeTime_2() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t619346209, ___m_LastSerializeTime_2)); }
	inline double get_m_LastSerializeTime_2() const { return ___m_LastSerializeTime_2; }
	inline double* get_address_of_m_LastSerializeTime_2() { return &___m_LastSerializeTime_2; }
	inline void set_m_LastSerializeTime_2(double value)
	{
		___m_LastSerializeTime_2 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizedSpeed_3() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t619346209, ___m_SynchronizedSpeed_3)); }
	inline Vector3_t3722313464  get_m_SynchronizedSpeed_3() const { return ___m_SynchronizedSpeed_3; }
	inline Vector3_t3722313464 * get_address_of_m_SynchronizedSpeed_3() { return &___m_SynchronizedSpeed_3; }
	inline void set_m_SynchronizedSpeed_3(Vector3_t3722313464  value)
	{
		___m_SynchronizedSpeed_3 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizedTurnSpeed_4() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t619346209, ___m_SynchronizedTurnSpeed_4)); }
	inline float get_m_SynchronizedTurnSpeed_4() const { return ___m_SynchronizedTurnSpeed_4; }
	inline float* get_address_of_m_SynchronizedTurnSpeed_4() { return &___m_SynchronizedTurnSpeed_4; }
	inline void set_m_SynchronizedTurnSpeed_4(float value)
	{
		___m_SynchronizedTurnSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_NetworkPosition_5() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t619346209, ___m_NetworkPosition_5)); }
	inline Vector3_t3722313464  get_m_NetworkPosition_5() const { return ___m_NetworkPosition_5; }
	inline Vector3_t3722313464 * get_address_of_m_NetworkPosition_5() { return &___m_NetworkPosition_5; }
	inline void set_m_NetworkPosition_5(Vector3_t3722313464  value)
	{
		___m_NetworkPosition_5 = value;
	}

	inline static int32_t get_offset_of_m_OldNetworkPositions_6() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t619346209, ___m_OldNetworkPositions_6)); }
	inline Queue_1_t3568572958 * get_m_OldNetworkPositions_6() const { return ___m_OldNetworkPositions_6; }
	inline Queue_1_t3568572958 ** get_address_of_m_OldNetworkPositions_6() { return &___m_OldNetworkPositions_6; }
	inline void set_m_OldNetworkPositions_6(Queue_1_t3568572958 * value)
	{
		___m_OldNetworkPositions_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OldNetworkPositions_6), value);
	}

	inline static int32_t get_offset_of_m_UpdatedPositionAfterOnSerialize_7() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t619346209, ___m_UpdatedPositionAfterOnSerialize_7)); }
	inline bool get_m_UpdatedPositionAfterOnSerialize_7() const { return ___m_UpdatedPositionAfterOnSerialize_7; }
	inline bool* get_address_of_m_UpdatedPositionAfterOnSerialize_7() { return &___m_UpdatedPositionAfterOnSerialize_7; }
	inline void set_m_UpdatedPositionAfterOnSerialize_7(bool value)
	{
		___m_UpdatedPositionAfterOnSerialize_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONTRANSFORMVIEWPOSITIONCONTROL_T619346209_H
#ifndef CLOUDREGIONFLAG_T3756941471_H
#define CLOUDREGIONFLAG_T3756941471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRegionFlag
struct  CloudRegionFlag_t3756941471 
{
public:
	// System.Int32 CloudRegionFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudRegionFlag_t3756941471, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDREGIONFLAG_T3756941471_H
#ifndef PHOTONLOGLEVEL_T4226222036_H
#define PHOTONLOGLEVEL_T4226222036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonLogLevel
struct  PhotonLogLevel_t4226222036 
{
public:
	// System.Int32 PhotonLogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonLogLevel_t4226222036, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONLOGLEVEL_T4226222036_H
#ifndef CUSTOMAUTHENTICATIONTYPE_T302987107_H
#define CUSTOMAUTHENTICATIONTYPE_T302987107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomAuthenticationType
struct  CustomAuthenticationType_t302987107 
{
public:
	// System.Byte CustomAuthenticationType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CustomAuthenticationType_t302987107, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMAUTHENTICATIONTYPE_T302987107_H
#ifndef CONNECTIONPROTOCOL_T2586603950_H
#define CONNECTIONPROTOCOL_T2586603950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.ConnectionProtocol
struct  ConnectionProtocol_t2586603950 
{
public:
	// System.Byte ExitGames.Client.Photon.ConnectionProtocol::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionProtocol_t2586603950, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONPROTOCOL_T2586603950_H
#ifndef CLIENTSTATE_T1348705391_H
#define CLIENTSTATE_T1348705391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClientState
struct  ClientState_t1348705391 
{
public:
	// System.Int32 ClientState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClientState_t1348705391, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSTATE_T1348705391_H
#ifndef JOINTYPE_T3510207077_H
#define JOINTYPE_T3510207077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoinType
struct  JoinType_t3510207077 
{
public:
	// System.Int32 JoinType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoinType_t3510207077, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTYPE_T3510207077_H
#ifndef DISCONNECTCAUSE_T501870387_H
#define DISCONNECTCAUSE_T501870387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisconnectCause
struct  DisconnectCause_t501870387 
{
public:
	// System.Int32 DisconnectCause::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DisconnectCause_t501870387, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCONNECTCAUSE_T501870387_H
#ifndef SERVERCONNECTION_T867335480_H
#define SERVERCONNECTION_T867335480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerConnection
struct  ServerConnection_t867335480 
{
public:
	// System.Int32 ServerConnection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ServerConnection_t867335480, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCONNECTION_T867335480_H
#ifndef EVENTCACHING_T168509732_H
#define EVENTCACHING_T168509732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventCaching
struct  EventCaching_t168509732 
{
public:
	// System.Byte EventCaching::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventCaching_t168509732, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTCACHING_T168509732_H
#ifndef JOINMODE_T2253954680_H
#define JOINMODE_T2253954680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoinMode
struct  JoinMode_t2253954680 
{
public:
	// System.Byte JoinMode::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoinMode_t2253954680, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINMODE_T2253954680_H
#ifndef INTERPOLATEOPTIONS_T1056869502_H
#define INTERPOLATEOPTIONS_T1056869502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewScaleModel/InterpolateOptions
struct  InterpolateOptions_t1056869502 
{
public:
	// System.Int32 PhotonTransformViewScaleModel/InterpolateOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InterpolateOptions_t1056869502, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEOPTIONS_T1056869502_H
#ifndef ENODETYPE_T1612415973_H
#define ENODETYPE_T1612415973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellTreeNode/ENodeType
struct  ENodeType_t1612415973 
{
public:
	// System.Int32 CellTreeNode/ENodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ENodeType_t1612415973, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENODETYPE_T1612415973_H
#ifndef PHOTONTRANSFORMVIEWSCALECONTROL_T2271393751_H
#define PHOTONTRANSFORMVIEWSCALECONTROL_T2271393751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewScaleControl
struct  PhotonTransformViewScaleControl_t2271393751  : public RuntimeObject
{
public:
	// PhotonTransformViewScaleModel PhotonTransformViewScaleControl::m_Model
	PhotonTransformViewScaleModel_t763003770 * ___m_Model_0;
	// UnityEngine.Vector3 PhotonTransformViewScaleControl::m_NetworkScale
	Vector3_t3722313464  ___m_NetworkScale_1;

public:
	inline static int32_t get_offset_of_m_Model_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleControl_t2271393751, ___m_Model_0)); }
	inline PhotonTransformViewScaleModel_t763003770 * get_m_Model_0() const { return ___m_Model_0; }
	inline PhotonTransformViewScaleModel_t763003770 ** get_address_of_m_Model_0() { return &___m_Model_0; }
	inline void set_m_Model_0(PhotonTransformViewScaleModel_t763003770 * value)
	{
		___m_Model_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Model_0), value);
	}

	inline static int32_t get_offset_of_m_NetworkScale_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleControl_t2271393751, ___m_NetworkScale_1)); }
	inline Vector3_t3722313464  get_m_NetworkScale_1() const { return ___m_NetworkScale_1; }
	inline Vector3_t3722313464 * get_address_of_m_NetworkScale_1() { return &___m_NetworkScale_1; }
	inline void set_m_NetworkScale_1(Vector3_t3722313464  value)
	{
		___m_NetworkScale_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONTRANSFORMVIEWSCALECONTROL_T2271393751_H
#ifndef INTERPOLATEOPTIONS_T364195466_H
#define INTERPOLATEOPTIONS_T364195466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewRotationModel/InterpolateOptions
struct  InterpolateOptions_t364195466 
{
public:
	// System.Int32 PhotonTransformViewRotationModel/InterpolateOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InterpolateOptions_t364195466, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEOPTIONS_T364195466_H
#ifndef MATCHMAKINGMODE_T1165119589_H
#define MATCHMAKINGMODE_T1165119589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MatchmakingMode
struct  MatchmakingMode_t1165119589 
{
public:
	// System.Byte MatchmakingMode::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MatchmakingMode_t1165119589, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHMAKINGMODE_T1165119589_H
#ifndef PHOTONTRANSFORMVIEWROTATIONCONTROL_T2679094986_H
#define PHOTONTRANSFORMVIEWROTATIONCONTROL_T2679094986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewRotationControl
struct  PhotonTransformViewRotationControl_t2679094986  : public RuntimeObject
{
public:
	// PhotonTransformViewRotationModel PhotonTransformViewRotationControl::m_Model
	PhotonTransformViewRotationModel_t1080899250 * ___m_Model_0;
	// UnityEngine.Quaternion PhotonTransformViewRotationControl::m_NetworkRotation
	Quaternion_t2301928331  ___m_NetworkRotation_1;

public:
	inline static int32_t get_offset_of_m_Model_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationControl_t2679094986, ___m_Model_0)); }
	inline PhotonTransformViewRotationModel_t1080899250 * get_m_Model_0() const { return ___m_Model_0; }
	inline PhotonTransformViewRotationModel_t1080899250 ** get_address_of_m_Model_0() { return &___m_Model_0; }
	inline void set_m_Model_0(PhotonTransformViewRotationModel_t1080899250 * value)
	{
		___m_Model_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Model_0), value);
	}

	inline static int32_t get_offset_of_m_NetworkRotation_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationControl_t2679094986, ___m_NetworkRotation_1)); }
	inline Quaternion_t2301928331  get_m_NetworkRotation_1() const { return ___m_NetworkRotation_1; }
	inline Quaternion_t2301928331 * get_address_of_m_NetworkRotation_1() { return &___m_NetworkRotation_1; }
	inline void set_m_NetworkRotation_1(Quaternion_t2301928331  value)
	{
		___m_NetworkRotation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONTRANSFORMVIEWROTATIONCONTROL_T2679094986_H
#ifndef EXTRAPOLATEOPTIONS_T2438484843_H
#define EXTRAPOLATEOPTIONS_T2438484843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewPositionModel/ExtrapolateOptions
struct  ExtrapolateOptions_t2438484843 
{
public:
	// System.Int32 PhotonTransformViewPositionModel/ExtrapolateOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ExtrapolateOptions_t2438484843, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTRAPOLATEOPTIONS_T2438484843_H
#ifndef INTERPOLATEOPTIONS_T1912251329_H
#define INTERPOLATEOPTIONS_T1912251329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewPositionModel/InterpolateOptions
struct  InterpolateOptions_t1912251329 
{
public:
	// System.Int32 PhotonTransformViewPositionModel/InterpolateOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InterpolateOptions_t1912251329, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEOPTIONS_T1912251329_H
#ifndef RECEIVERGROUP_T3206452792_H
#define RECEIVERGROUP_T3206452792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReceiverGroup
struct  ReceiverGroup_t3206452792 
{
public:
	// System.Byte ReceiverGroup::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReceiverGroup_t3206452792, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIVERGROUP_T3206452792_H
#ifndef PROPERTYTYPEFLAG_T3957538841_H
#define PROPERTYTYPEFLAG_T3957538841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PropertyTypeFlag
struct  PropertyTypeFlag_t3957538841 
{
public:
	// System.Byte PropertyTypeFlag::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyTypeFlag_t3957538841, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPEFLAG_T3957538841_H
#ifndef ENCRYPTIONMODE_T4213192103_H
#define ENCRYPTIONMODE_T4213192103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EncryptionMode
struct  EncryptionMode_t4213192103 
{
public:
	// System.Int32 EncryptionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EncryptionMode_t4213192103, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTIONMODE_T4213192103_H
#ifndef U3CPINGAVAILABLEREGIONSCOROUTINEU3EC__ITERATOR0_T2850618202_H
#define U3CPINGAVAILABLEREGIONSCOROUTINEU3EC__ITERATOR0_T2850618202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0
struct  U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202  : public RuntimeObject
{
public:
	// PhotonPingManager PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::<pingManager>__0
	PhotonPingManager_t630892274 * ___U3CpingManagerU3E__0_0;
	// System.Collections.Generic.List`1/Enumerator<Region> PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::$locvar0
	Enumerator_t2750576585  ___U24locvar0_1;
	// Region PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::<best>__0
	Region_t3684225262 * ___U3CbestU3E__0_2;
	// System.Boolean PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::connectToBest
	bool ___connectToBest_3;
	// System.Object PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CpingManagerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202, ___U3CpingManagerU3E__0_0)); }
	inline PhotonPingManager_t630892274 * get_U3CpingManagerU3E__0_0() const { return ___U3CpingManagerU3E__0_0; }
	inline PhotonPingManager_t630892274 ** get_address_of_U3CpingManagerU3E__0_0() { return &___U3CpingManagerU3E__0_0; }
	inline void set_U3CpingManagerU3E__0_0(PhotonPingManager_t630892274 * value)
	{
		___U3CpingManagerU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpingManagerU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202, ___U24locvar0_1)); }
	inline Enumerator_t2750576585  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t2750576585 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t2750576585  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3CbestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202, ___U3CbestU3E__0_2)); }
	inline Region_t3684225262 * get_U3CbestU3E__0_2() const { return ___U3CbestU3E__0_2; }
	inline Region_t3684225262 ** get_address_of_U3CbestU3E__0_2() { return &___U3CbestU3E__0_2; }
	inline void set_U3CbestU3E__0_2(Region_t3684225262 * value)
	{
		___U3CbestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_connectToBest_3() { return static_cast<int32_t>(offsetof(U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202, ___connectToBest_3)); }
	inline bool get_connectToBest_3() const { return ___connectToBest_3; }
	inline bool* get_address_of_connectToBest_3() { return &___connectToBest_3; }
	inline void set_connectToBest_3(bool value)
	{
		___connectToBest_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPINGAVAILABLEREGIONSCOROUTINEU3EC__ITERATOR0_T2850618202_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OWNERSHIPOPTION_T37885007_H
#define OWNERSHIPOPTION_T37885007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OwnershipOption
struct  OwnershipOption_t37885007 
{
public:
	// System.Int32 OwnershipOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OwnershipOption_t37885007, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OWNERSHIPOPTION_T37885007_H
#ifndef ONSERIALIZERIGIDBODY_T385167779_H
#define ONSERIALIZERIGIDBODY_T385167779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnSerializeRigidBody
struct  OnSerializeRigidBody_t385167779 
{
public:
	// System.Int32 OnSerializeRigidBody::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OnSerializeRigidBody_t385167779, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSERIALIZERIGIDBODY_T385167779_H
#ifndef ONSERIALIZETRANSFORM_T1364648257_H
#define ONSERIALIZETRANSFORM_T1364648257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnSerializeTransform
struct  OnSerializeTransform_t1364648257 
{
public:
	// System.Int32 OnSerializeTransform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OnSerializeTransform_t1364648257, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSERIALIZETRANSFORM_T1364648257_H
#ifndef VIEWSYNCHRONIZATION_T3183556584_H
#define VIEWSYNCHRONIZATION_T3183556584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewSynchronization
struct  ViewSynchronization_t3183556584 
{
public:
	// System.Int32 ViewSynchronization::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ViewSynchronization_t3183556584, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWSYNCHRONIZATION_T3183556584_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef LOBBYTYPE_T3695323860_H
#define LOBBYTYPE_T3695323860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyType
struct  LobbyType_t3695323860 
{
public:
	// System.Byte LobbyType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LobbyType_t3695323860, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOBBYTYPE_T3695323860_H
#ifndef SERIALIZATIONPROTOCOL_T4091957412_H
#define SERIALIZATIONPROTOCOL_T4091957412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializationProtocol
struct  SerializationProtocol_t4091957412 
{
public:
	// System.Int32 ExitGames.Client.Photon.SerializationProtocol::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SerializationProtocol_t4091957412, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONPROTOCOL_T4091957412_H
#ifndef DEBUGLEVEL_T3671880145_H
#define DEBUGLEVEL_T3671880145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.DebugLevel
struct  DebugLevel_t3671880145 
{
public:
	// System.Byte ExitGames.Client.Photon.DebugLevel::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugLevel_t3671880145, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVEL_T3671880145_H
#ifndef SYNCHRONIZETYPE_T4108284517_H
#define SYNCHRONIZETYPE_T4108284517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/SynchronizeType
struct  SynchronizeType_t4108284517 
{
public:
	// System.Int32 PhotonAnimatorView/SynchronizeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SynchronizeType_t4108284517, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCHRONIZETYPE_T4108284517_H
#ifndef PARAMETERTYPE_T1940879453_H
#define PARAMETERTYPE_T1940879453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/ParameterType
struct  ParameterType_t1940879453 
{
public:
	// System.Int32 PhotonAnimatorView/ParameterType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterType_t1940879453, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERTYPE_T1940879453_H
#ifndef ROOMOPTIONBIT_T4007729377_H
#define ROOMOPTIONBIT_T4007729377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadBalancingPeer/RoomOptionBit
struct  RoomOptionBit_t4007729377 
{
public:
	// System.Int32 LoadBalancingPeer/RoomOptionBit::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RoomOptionBit_t4007729377, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMOPTIONBIT_T4007729377_H
#ifndef HOSTINGOPTION_T2949276063_H
#define HOSTINGOPTION_T2949276063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerSettings/HostingOption
struct  HostingOption_t2949276063 
{
public:
	// System.Int32 ServerSettings/HostingOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HostingOption_t2949276063, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTINGOPTION_T2949276063_H
#ifndef GIZMOTYPE_T3704257101_H
#define GIZMOTYPE_T3704257101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.GUI.GizmoType
struct  GizmoType_t3704257101 
{
public:
	// System.Int32 ExitGames.Client.GUI.GizmoType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GizmoType_t3704257101, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIZMOTYPE_T3704257101_H
#ifndef CLOUDREGIONCODE_T1925019500_H
#define CLOUDREGIONCODE_T1925019500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRegionCode
struct  CloudRegionCode_t1925019500 
{
public:
	// System.Int32 CloudRegionCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudRegionCode_t1925019500, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDREGIONCODE_T1925019500_H
#ifndef AUTHMODEOPTION_T1305270560_H
#define AUTHMODEOPTION_T1305270560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AuthModeOption
struct  AuthModeOption_t1305270560 
{
public:
	// System.Int32 AuthModeOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthModeOption_t1305270560, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHMODEOPTION_T1305270560_H
#ifndef RAISEEVENTOPTIONS_T1229553678_H
#define RAISEEVENTOPTIONS_T1229553678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RaiseEventOptions
struct  RaiseEventOptions_t1229553678  : public RuntimeObject
{
public:
	// EventCaching RaiseEventOptions::CachingOption
	uint8_t ___CachingOption_1;
	// System.Byte RaiseEventOptions::InterestGroup
	uint8_t ___InterestGroup_2;
	// System.Int32[] RaiseEventOptions::TargetActors
	Int32U5BU5D_t385246372* ___TargetActors_3;
	// ReceiverGroup RaiseEventOptions::Receivers
	uint8_t ___Receivers_4;
	// System.Byte RaiseEventOptions::SequenceChannel
	uint8_t ___SequenceChannel_5;
	// System.Boolean RaiseEventOptions::ForwardToWebhook
	bool ___ForwardToWebhook_6;
	// System.Boolean RaiseEventOptions::Encrypt
	bool ___Encrypt_7;

public:
	inline static int32_t get_offset_of_CachingOption_1() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___CachingOption_1)); }
	inline uint8_t get_CachingOption_1() const { return ___CachingOption_1; }
	inline uint8_t* get_address_of_CachingOption_1() { return &___CachingOption_1; }
	inline void set_CachingOption_1(uint8_t value)
	{
		___CachingOption_1 = value;
	}

	inline static int32_t get_offset_of_InterestGroup_2() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___InterestGroup_2)); }
	inline uint8_t get_InterestGroup_2() const { return ___InterestGroup_2; }
	inline uint8_t* get_address_of_InterestGroup_2() { return &___InterestGroup_2; }
	inline void set_InterestGroup_2(uint8_t value)
	{
		___InterestGroup_2 = value;
	}

	inline static int32_t get_offset_of_TargetActors_3() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___TargetActors_3)); }
	inline Int32U5BU5D_t385246372* get_TargetActors_3() const { return ___TargetActors_3; }
	inline Int32U5BU5D_t385246372** get_address_of_TargetActors_3() { return &___TargetActors_3; }
	inline void set_TargetActors_3(Int32U5BU5D_t385246372* value)
	{
		___TargetActors_3 = value;
		Il2CppCodeGenWriteBarrier((&___TargetActors_3), value);
	}

	inline static int32_t get_offset_of_Receivers_4() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___Receivers_4)); }
	inline uint8_t get_Receivers_4() const { return ___Receivers_4; }
	inline uint8_t* get_address_of_Receivers_4() { return &___Receivers_4; }
	inline void set_Receivers_4(uint8_t value)
	{
		___Receivers_4 = value;
	}

	inline static int32_t get_offset_of_SequenceChannel_5() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___SequenceChannel_5)); }
	inline uint8_t get_SequenceChannel_5() const { return ___SequenceChannel_5; }
	inline uint8_t* get_address_of_SequenceChannel_5() { return &___SequenceChannel_5; }
	inline void set_SequenceChannel_5(uint8_t value)
	{
		___SequenceChannel_5 = value;
	}

	inline static int32_t get_offset_of_ForwardToWebhook_6() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___ForwardToWebhook_6)); }
	inline bool get_ForwardToWebhook_6() const { return ___ForwardToWebhook_6; }
	inline bool* get_address_of_ForwardToWebhook_6() { return &___ForwardToWebhook_6; }
	inline void set_ForwardToWebhook_6(bool value)
	{
		___ForwardToWebhook_6 = value;
	}

	inline static int32_t get_offset_of_Encrypt_7() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___Encrypt_7)); }
	inline bool get_Encrypt_7() const { return ___Encrypt_7; }
	inline bool* get_address_of_Encrypt_7() { return &___Encrypt_7; }
	inline void set_Encrypt_7(bool value)
	{
		___Encrypt_7 = value;
	}
};

struct RaiseEventOptions_t1229553678_StaticFields
{
public:
	// RaiseEventOptions RaiseEventOptions::Default
	RaiseEventOptions_t1229553678 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678_StaticFields, ___Default_0)); }
	inline RaiseEventOptions_t1229553678 * get_Default_0() const { return ___Default_0; }
	inline RaiseEventOptions_t1229553678 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(RaiseEventOptions_t1229553678 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAISEEVENTOPTIONS_T1229553678_H
#ifndef TYPEDLOBBY_T3336582029_H
#define TYPEDLOBBY_T3336582029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TypedLobby
struct  TypedLobby_t3336582029  : public RuntimeObject
{
public:
	// System.String TypedLobby::Name
	String_t* ___Name_0;
	// LobbyType TypedLobby::Type
	uint8_t ___Type_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TypedLobby_t3336582029, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(TypedLobby_t3336582029, ___Type_1)); }
	inline uint8_t get_Type_1() const { return ___Type_1; }
	inline uint8_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(uint8_t value)
	{
		___Type_1 = value;
	}
};

struct TypedLobby_t3336582029_StaticFields
{
public:
	// TypedLobby TypedLobby::Default
	TypedLobby_t3336582029 * ___Default_2;

public:
	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(TypedLobby_t3336582029_StaticFields, ___Default_2)); }
	inline TypedLobby_t3336582029 * get_Default_2() const { return ___Default_2; }
	inline TypedLobby_t3336582029 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(TypedLobby_t3336582029 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDLOBBY_T3336582029_H
#ifndef CELLTREENODE_T2932145224_H
#define CELLTREENODE_T2932145224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellTreeNode
struct  CellTreeNode_t2932145224  : public RuntimeObject
{
public:
	// System.Byte CellTreeNode::Id
	uint8_t ___Id_0;
	// UnityEngine.Vector3 CellTreeNode::Center
	Vector3_t3722313464  ___Center_1;
	// UnityEngine.Vector3 CellTreeNode::Size
	Vector3_t3722313464  ___Size_2;
	// UnityEngine.Vector3 CellTreeNode::TopLeft
	Vector3_t3722313464  ___TopLeft_3;
	// UnityEngine.Vector3 CellTreeNode::BottomRight
	Vector3_t3722313464  ___BottomRight_4;
	// CellTreeNode/ENodeType CellTreeNode::NodeType
	int32_t ___NodeType_5;
	// CellTreeNode CellTreeNode::Parent
	CellTreeNode_t2932145224 * ___Parent_6;
	// System.Collections.Generic.List`1<CellTreeNode> CellTreeNode::Childs
	List_1_t109252670 * ___Childs_7;
	// System.Single CellTreeNode::maxDistance
	float ___maxDistance_8;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(CellTreeNode_t2932145224, ___Id_0)); }
	inline uint8_t get_Id_0() const { return ___Id_0; }
	inline uint8_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(uint8_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Center_1() { return static_cast<int32_t>(offsetof(CellTreeNode_t2932145224, ___Center_1)); }
	inline Vector3_t3722313464  get_Center_1() const { return ___Center_1; }
	inline Vector3_t3722313464 * get_address_of_Center_1() { return &___Center_1; }
	inline void set_Center_1(Vector3_t3722313464  value)
	{
		___Center_1 = value;
	}

	inline static int32_t get_offset_of_Size_2() { return static_cast<int32_t>(offsetof(CellTreeNode_t2932145224, ___Size_2)); }
	inline Vector3_t3722313464  get_Size_2() const { return ___Size_2; }
	inline Vector3_t3722313464 * get_address_of_Size_2() { return &___Size_2; }
	inline void set_Size_2(Vector3_t3722313464  value)
	{
		___Size_2 = value;
	}

	inline static int32_t get_offset_of_TopLeft_3() { return static_cast<int32_t>(offsetof(CellTreeNode_t2932145224, ___TopLeft_3)); }
	inline Vector3_t3722313464  get_TopLeft_3() const { return ___TopLeft_3; }
	inline Vector3_t3722313464 * get_address_of_TopLeft_3() { return &___TopLeft_3; }
	inline void set_TopLeft_3(Vector3_t3722313464  value)
	{
		___TopLeft_3 = value;
	}

	inline static int32_t get_offset_of_BottomRight_4() { return static_cast<int32_t>(offsetof(CellTreeNode_t2932145224, ___BottomRight_4)); }
	inline Vector3_t3722313464  get_BottomRight_4() const { return ___BottomRight_4; }
	inline Vector3_t3722313464 * get_address_of_BottomRight_4() { return &___BottomRight_4; }
	inline void set_BottomRight_4(Vector3_t3722313464  value)
	{
		___BottomRight_4 = value;
	}

	inline static int32_t get_offset_of_NodeType_5() { return static_cast<int32_t>(offsetof(CellTreeNode_t2932145224, ___NodeType_5)); }
	inline int32_t get_NodeType_5() const { return ___NodeType_5; }
	inline int32_t* get_address_of_NodeType_5() { return &___NodeType_5; }
	inline void set_NodeType_5(int32_t value)
	{
		___NodeType_5 = value;
	}

	inline static int32_t get_offset_of_Parent_6() { return static_cast<int32_t>(offsetof(CellTreeNode_t2932145224, ___Parent_6)); }
	inline CellTreeNode_t2932145224 * get_Parent_6() const { return ___Parent_6; }
	inline CellTreeNode_t2932145224 ** get_address_of_Parent_6() { return &___Parent_6; }
	inline void set_Parent_6(CellTreeNode_t2932145224 * value)
	{
		___Parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_6), value);
	}

	inline static int32_t get_offset_of_Childs_7() { return static_cast<int32_t>(offsetof(CellTreeNode_t2932145224, ___Childs_7)); }
	inline List_1_t109252670 * get_Childs_7() const { return ___Childs_7; }
	inline List_1_t109252670 ** get_address_of_Childs_7() { return &___Childs_7; }
	inline void set_Childs_7(List_1_t109252670 * value)
	{
		___Childs_7 = value;
		Il2CppCodeGenWriteBarrier((&___Childs_7), value);
	}

	inline static int32_t get_offset_of_maxDistance_8() { return static_cast<int32_t>(offsetof(CellTreeNode_t2932145224, ___maxDistance_8)); }
	inline float get_maxDistance_8() const { return ___maxDistance_8; }
	inline float* get_address_of_maxDistance_8() { return &___maxDistance_8; }
	inline void set_maxDistance_8(float value)
	{
		___maxDistance_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CELLTREENODE_T2932145224_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef PHOTONNETWORK_T1610183659_H
#define PHOTONNETWORK_T1610183659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonNetwork
struct  PhotonNetwork_t1610183659  : public RuntimeObject
{
public:

public:
};

struct PhotonNetwork_t1610183659_StaticFields
{
public:
	// System.String PhotonNetwork::<gameVersion>k__BackingField
	String_t* ___U3CgameVersionU3Ek__BackingField_1;
	// PhotonHandler PhotonNetwork::photonMono
	PhotonHandler_t2139970417 * ___photonMono_2;
	// NetworkingPeer PhotonNetwork::networkingPeer
	NetworkingPeer_t264212356 * ___networkingPeer_3;
	// System.Int32 PhotonNetwork::MAX_VIEW_IDS
	int32_t ___MAX_VIEW_IDS_4;
	// ServerSettings PhotonNetwork::PhotonServerSettings
	ServerSettings_t2755303613 * ___PhotonServerSettings_6;
	// System.Boolean PhotonNetwork::InstantiateInRoomOnly
	bool ___InstantiateInRoomOnly_7;
	// PhotonLogLevel PhotonNetwork::logLevel
	int32_t ___logLevel_8;
	// System.Collections.Generic.List`1<FriendInfo> PhotonNetwork::<Friends>k__BackingField
	List_1_t2005371586 * ___U3CFriendsU3Ek__BackingField_9;
	// System.Single PhotonNetwork::precisionForVectorSynchronization
	float ___precisionForVectorSynchronization_10;
	// System.Single PhotonNetwork::precisionForQuaternionSynchronization
	float ___precisionForQuaternionSynchronization_11;
	// System.Single PhotonNetwork::precisionForFloatSynchronization
	float ___precisionForFloatSynchronization_12;
	// System.Boolean PhotonNetwork::UseRpcMonoBehaviourCache
	bool ___UseRpcMonoBehaviourCache_13;
	// System.Boolean PhotonNetwork::UsePrefabCache
	bool ___UsePrefabCache_14;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> PhotonNetwork::PrefabCache
	Dictionary_2_t898892918 * ___PrefabCache_15;
	// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> PhotonNetwork::SendMonoMessageTargets
	HashSet_1_t3973553389 * ___SendMonoMessageTargets_16;
	// System.Type PhotonNetwork::SendMonoMessageTargetType
	Type_t * ___SendMonoMessageTargetType_17;
	// System.Boolean PhotonNetwork::StartRpcsAsCoroutine
	bool ___StartRpcsAsCoroutine_18;
	// System.Boolean PhotonNetwork::isOfflineMode
	bool ___isOfflineMode_19;
	// Room PhotonNetwork::offlineModeRoom
	Room_t3759828263 * ___offlineModeRoom_20;
	// System.Int32 PhotonNetwork::maxConnections
	int32_t ___maxConnections_21;
	// System.Boolean PhotonNetwork::_mAutomaticallySyncScene
	bool ____mAutomaticallySyncScene_22;
	// System.Boolean PhotonNetwork::m_autoCleanUpPlayerObjects
	bool ___m_autoCleanUpPlayerObjects_23;
	// System.Int32 PhotonNetwork::sendInterval
	int32_t ___sendInterval_24;
	// System.Int32 PhotonNetwork::sendIntervalOnSerialize
	int32_t ___sendIntervalOnSerialize_25;
	// System.Boolean PhotonNetwork::m_isMessageQueueRunning
	bool ___m_isMessageQueueRunning_26;
	// System.Boolean PhotonNetwork::UsePreciseTimer
	bool ___UsePreciseTimer_27;
	// System.Diagnostics.Stopwatch PhotonNetwork::startupStopwatch
	Stopwatch_t305734070 * ___startupStopwatch_28;
	// System.Single PhotonNetwork::BackgroundTimeout
	float ___BackgroundTimeout_29;
	// PhotonNetwork/EventCallback PhotonNetwork::OnEventCall
	EventCallback_t1220598991 * ___OnEventCall_30;
	// System.Int32 PhotonNetwork::lastUsedViewSubId
	int32_t ___lastUsedViewSubId_31;
	// System.Int32 PhotonNetwork::lastUsedViewSubIdStatic
	int32_t ___lastUsedViewSubIdStatic_32;
	// System.Collections.Generic.List`1<System.Int32> PhotonNetwork::manuallyAllocatedViewIds
	List_1_t128053199 * ___manuallyAllocatedViewIds_33;

public:
	inline static int32_t get_offset_of_U3CgameVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___U3CgameVersionU3Ek__BackingField_1)); }
	inline String_t* get_U3CgameVersionU3Ek__BackingField_1() const { return ___U3CgameVersionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CgameVersionU3Ek__BackingField_1() { return &___U3CgameVersionU3Ek__BackingField_1; }
	inline void set_U3CgameVersionU3Ek__BackingField_1(String_t* value)
	{
		___U3CgameVersionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameVersionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_photonMono_2() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___photonMono_2)); }
	inline PhotonHandler_t2139970417 * get_photonMono_2() const { return ___photonMono_2; }
	inline PhotonHandler_t2139970417 ** get_address_of_photonMono_2() { return &___photonMono_2; }
	inline void set_photonMono_2(PhotonHandler_t2139970417 * value)
	{
		___photonMono_2 = value;
		Il2CppCodeGenWriteBarrier((&___photonMono_2), value);
	}

	inline static int32_t get_offset_of_networkingPeer_3() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___networkingPeer_3)); }
	inline NetworkingPeer_t264212356 * get_networkingPeer_3() const { return ___networkingPeer_3; }
	inline NetworkingPeer_t264212356 ** get_address_of_networkingPeer_3() { return &___networkingPeer_3; }
	inline void set_networkingPeer_3(NetworkingPeer_t264212356 * value)
	{
		___networkingPeer_3 = value;
		Il2CppCodeGenWriteBarrier((&___networkingPeer_3), value);
	}

	inline static int32_t get_offset_of_MAX_VIEW_IDS_4() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___MAX_VIEW_IDS_4)); }
	inline int32_t get_MAX_VIEW_IDS_4() const { return ___MAX_VIEW_IDS_4; }
	inline int32_t* get_address_of_MAX_VIEW_IDS_4() { return &___MAX_VIEW_IDS_4; }
	inline void set_MAX_VIEW_IDS_4(int32_t value)
	{
		___MAX_VIEW_IDS_4 = value;
	}

	inline static int32_t get_offset_of_PhotonServerSettings_6() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___PhotonServerSettings_6)); }
	inline ServerSettings_t2755303613 * get_PhotonServerSettings_6() const { return ___PhotonServerSettings_6; }
	inline ServerSettings_t2755303613 ** get_address_of_PhotonServerSettings_6() { return &___PhotonServerSettings_6; }
	inline void set_PhotonServerSettings_6(ServerSettings_t2755303613 * value)
	{
		___PhotonServerSettings_6 = value;
		Il2CppCodeGenWriteBarrier((&___PhotonServerSettings_6), value);
	}

	inline static int32_t get_offset_of_InstantiateInRoomOnly_7() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___InstantiateInRoomOnly_7)); }
	inline bool get_InstantiateInRoomOnly_7() const { return ___InstantiateInRoomOnly_7; }
	inline bool* get_address_of_InstantiateInRoomOnly_7() { return &___InstantiateInRoomOnly_7; }
	inline void set_InstantiateInRoomOnly_7(bool value)
	{
		___InstantiateInRoomOnly_7 = value;
	}

	inline static int32_t get_offset_of_logLevel_8() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___logLevel_8)); }
	inline int32_t get_logLevel_8() const { return ___logLevel_8; }
	inline int32_t* get_address_of_logLevel_8() { return &___logLevel_8; }
	inline void set_logLevel_8(int32_t value)
	{
		___logLevel_8 = value;
	}

	inline static int32_t get_offset_of_U3CFriendsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___U3CFriendsU3Ek__BackingField_9)); }
	inline List_1_t2005371586 * get_U3CFriendsU3Ek__BackingField_9() const { return ___U3CFriendsU3Ek__BackingField_9; }
	inline List_1_t2005371586 ** get_address_of_U3CFriendsU3Ek__BackingField_9() { return &___U3CFriendsU3Ek__BackingField_9; }
	inline void set_U3CFriendsU3Ek__BackingField_9(List_1_t2005371586 * value)
	{
		___U3CFriendsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFriendsU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_precisionForVectorSynchronization_10() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___precisionForVectorSynchronization_10)); }
	inline float get_precisionForVectorSynchronization_10() const { return ___precisionForVectorSynchronization_10; }
	inline float* get_address_of_precisionForVectorSynchronization_10() { return &___precisionForVectorSynchronization_10; }
	inline void set_precisionForVectorSynchronization_10(float value)
	{
		___precisionForVectorSynchronization_10 = value;
	}

	inline static int32_t get_offset_of_precisionForQuaternionSynchronization_11() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___precisionForQuaternionSynchronization_11)); }
	inline float get_precisionForQuaternionSynchronization_11() const { return ___precisionForQuaternionSynchronization_11; }
	inline float* get_address_of_precisionForQuaternionSynchronization_11() { return &___precisionForQuaternionSynchronization_11; }
	inline void set_precisionForQuaternionSynchronization_11(float value)
	{
		___precisionForQuaternionSynchronization_11 = value;
	}

	inline static int32_t get_offset_of_precisionForFloatSynchronization_12() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___precisionForFloatSynchronization_12)); }
	inline float get_precisionForFloatSynchronization_12() const { return ___precisionForFloatSynchronization_12; }
	inline float* get_address_of_precisionForFloatSynchronization_12() { return &___precisionForFloatSynchronization_12; }
	inline void set_precisionForFloatSynchronization_12(float value)
	{
		___precisionForFloatSynchronization_12 = value;
	}

	inline static int32_t get_offset_of_UseRpcMonoBehaviourCache_13() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___UseRpcMonoBehaviourCache_13)); }
	inline bool get_UseRpcMonoBehaviourCache_13() const { return ___UseRpcMonoBehaviourCache_13; }
	inline bool* get_address_of_UseRpcMonoBehaviourCache_13() { return &___UseRpcMonoBehaviourCache_13; }
	inline void set_UseRpcMonoBehaviourCache_13(bool value)
	{
		___UseRpcMonoBehaviourCache_13 = value;
	}

	inline static int32_t get_offset_of_UsePrefabCache_14() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___UsePrefabCache_14)); }
	inline bool get_UsePrefabCache_14() const { return ___UsePrefabCache_14; }
	inline bool* get_address_of_UsePrefabCache_14() { return &___UsePrefabCache_14; }
	inline void set_UsePrefabCache_14(bool value)
	{
		___UsePrefabCache_14 = value;
	}

	inline static int32_t get_offset_of_PrefabCache_15() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___PrefabCache_15)); }
	inline Dictionary_2_t898892918 * get_PrefabCache_15() const { return ___PrefabCache_15; }
	inline Dictionary_2_t898892918 ** get_address_of_PrefabCache_15() { return &___PrefabCache_15; }
	inline void set_PrefabCache_15(Dictionary_2_t898892918 * value)
	{
		___PrefabCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabCache_15), value);
	}

	inline static int32_t get_offset_of_SendMonoMessageTargets_16() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___SendMonoMessageTargets_16)); }
	inline HashSet_1_t3973553389 * get_SendMonoMessageTargets_16() const { return ___SendMonoMessageTargets_16; }
	inline HashSet_1_t3973553389 ** get_address_of_SendMonoMessageTargets_16() { return &___SendMonoMessageTargets_16; }
	inline void set_SendMonoMessageTargets_16(HashSet_1_t3973553389 * value)
	{
		___SendMonoMessageTargets_16 = value;
		Il2CppCodeGenWriteBarrier((&___SendMonoMessageTargets_16), value);
	}

	inline static int32_t get_offset_of_SendMonoMessageTargetType_17() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___SendMonoMessageTargetType_17)); }
	inline Type_t * get_SendMonoMessageTargetType_17() const { return ___SendMonoMessageTargetType_17; }
	inline Type_t ** get_address_of_SendMonoMessageTargetType_17() { return &___SendMonoMessageTargetType_17; }
	inline void set_SendMonoMessageTargetType_17(Type_t * value)
	{
		___SendMonoMessageTargetType_17 = value;
		Il2CppCodeGenWriteBarrier((&___SendMonoMessageTargetType_17), value);
	}

	inline static int32_t get_offset_of_StartRpcsAsCoroutine_18() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___StartRpcsAsCoroutine_18)); }
	inline bool get_StartRpcsAsCoroutine_18() const { return ___StartRpcsAsCoroutine_18; }
	inline bool* get_address_of_StartRpcsAsCoroutine_18() { return &___StartRpcsAsCoroutine_18; }
	inline void set_StartRpcsAsCoroutine_18(bool value)
	{
		___StartRpcsAsCoroutine_18 = value;
	}

	inline static int32_t get_offset_of_isOfflineMode_19() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___isOfflineMode_19)); }
	inline bool get_isOfflineMode_19() const { return ___isOfflineMode_19; }
	inline bool* get_address_of_isOfflineMode_19() { return &___isOfflineMode_19; }
	inline void set_isOfflineMode_19(bool value)
	{
		___isOfflineMode_19 = value;
	}

	inline static int32_t get_offset_of_offlineModeRoom_20() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___offlineModeRoom_20)); }
	inline Room_t3759828263 * get_offlineModeRoom_20() const { return ___offlineModeRoom_20; }
	inline Room_t3759828263 ** get_address_of_offlineModeRoom_20() { return &___offlineModeRoom_20; }
	inline void set_offlineModeRoom_20(Room_t3759828263 * value)
	{
		___offlineModeRoom_20 = value;
		Il2CppCodeGenWriteBarrier((&___offlineModeRoom_20), value);
	}

	inline static int32_t get_offset_of_maxConnections_21() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___maxConnections_21)); }
	inline int32_t get_maxConnections_21() const { return ___maxConnections_21; }
	inline int32_t* get_address_of_maxConnections_21() { return &___maxConnections_21; }
	inline void set_maxConnections_21(int32_t value)
	{
		___maxConnections_21 = value;
	}

	inline static int32_t get_offset_of__mAutomaticallySyncScene_22() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ____mAutomaticallySyncScene_22)); }
	inline bool get__mAutomaticallySyncScene_22() const { return ____mAutomaticallySyncScene_22; }
	inline bool* get_address_of__mAutomaticallySyncScene_22() { return &____mAutomaticallySyncScene_22; }
	inline void set__mAutomaticallySyncScene_22(bool value)
	{
		____mAutomaticallySyncScene_22 = value;
	}

	inline static int32_t get_offset_of_m_autoCleanUpPlayerObjects_23() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___m_autoCleanUpPlayerObjects_23)); }
	inline bool get_m_autoCleanUpPlayerObjects_23() const { return ___m_autoCleanUpPlayerObjects_23; }
	inline bool* get_address_of_m_autoCleanUpPlayerObjects_23() { return &___m_autoCleanUpPlayerObjects_23; }
	inline void set_m_autoCleanUpPlayerObjects_23(bool value)
	{
		___m_autoCleanUpPlayerObjects_23 = value;
	}

	inline static int32_t get_offset_of_sendInterval_24() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___sendInterval_24)); }
	inline int32_t get_sendInterval_24() const { return ___sendInterval_24; }
	inline int32_t* get_address_of_sendInterval_24() { return &___sendInterval_24; }
	inline void set_sendInterval_24(int32_t value)
	{
		___sendInterval_24 = value;
	}

	inline static int32_t get_offset_of_sendIntervalOnSerialize_25() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___sendIntervalOnSerialize_25)); }
	inline int32_t get_sendIntervalOnSerialize_25() const { return ___sendIntervalOnSerialize_25; }
	inline int32_t* get_address_of_sendIntervalOnSerialize_25() { return &___sendIntervalOnSerialize_25; }
	inline void set_sendIntervalOnSerialize_25(int32_t value)
	{
		___sendIntervalOnSerialize_25 = value;
	}

	inline static int32_t get_offset_of_m_isMessageQueueRunning_26() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___m_isMessageQueueRunning_26)); }
	inline bool get_m_isMessageQueueRunning_26() const { return ___m_isMessageQueueRunning_26; }
	inline bool* get_address_of_m_isMessageQueueRunning_26() { return &___m_isMessageQueueRunning_26; }
	inline void set_m_isMessageQueueRunning_26(bool value)
	{
		___m_isMessageQueueRunning_26 = value;
	}

	inline static int32_t get_offset_of_UsePreciseTimer_27() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___UsePreciseTimer_27)); }
	inline bool get_UsePreciseTimer_27() const { return ___UsePreciseTimer_27; }
	inline bool* get_address_of_UsePreciseTimer_27() { return &___UsePreciseTimer_27; }
	inline void set_UsePreciseTimer_27(bool value)
	{
		___UsePreciseTimer_27 = value;
	}

	inline static int32_t get_offset_of_startupStopwatch_28() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___startupStopwatch_28)); }
	inline Stopwatch_t305734070 * get_startupStopwatch_28() const { return ___startupStopwatch_28; }
	inline Stopwatch_t305734070 ** get_address_of_startupStopwatch_28() { return &___startupStopwatch_28; }
	inline void set_startupStopwatch_28(Stopwatch_t305734070 * value)
	{
		___startupStopwatch_28 = value;
		Il2CppCodeGenWriteBarrier((&___startupStopwatch_28), value);
	}

	inline static int32_t get_offset_of_BackgroundTimeout_29() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___BackgroundTimeout_29)); }
	inline float get_BackgroundTimeout_29() const { return ___BackgroundTimeout_29; }
	inline float* get_address_of_BackgroundTimeout_29() { return &___BackgroundTimeout_29; }
	inline void set_BackgroundTimeout_29(float value)
	{
		___BackgroundTimeout_29 = value;
	}

	inline static int32_t get_offset_of_OnEventCall_30() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___OnEventCall_30)); }
	inline EventCallback_t1220598991 * get_OnEventCall_30() const { return ___OnEventCall_30; }
	inline EventCallback_t1220598991 ** get_address_of_OnEventCall_30() { return &___OnEventCall_30; }
	inline void set_OnEventCall_30(EventCallback_t1220598991 * value)
	{
		___OnEventCall_30 = value;
		Il2CppCodeGenWriteBarrier((&___OnEventCall_30), value);
	}

	inline static int32_t get_offset_of_lastUsedViewSubId_31() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___lastUsedViewSubId_31)); }
	inline int32_t get_lastUsedViewSubId_31() const { return ___lastUsedViewSubId_31; }
	inline int32_t* get_address_of_lastUsedViewSubId_31() { return &___lastUsedViewSubId_31; }
	inline void set_lastUsedViewSubId_31(int32_t value)
	{
		___lastUsedViewSubId_31 = value;
	}

	inline static int32_t get_offset_of_lastUsedViewSubIdStatic_32() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___lastUsedViewSubIdStatic_32)); }
	inline int32_t get_lastUsedViewSubIdStatic_32() const { return ___lastUsedViewSubIdStatic_32; }
	inline int32_t* get_address_of_lastUsedViewSubIdStatic_32() { return &___lastUsedViewSubIdStatic_32; }
	inline void set_lastUsedViewSubIdStatic_32(int32_t value)
	{
		___lastUsedViewSubIdStatic_32 = value;
	}

	inline static int32_t get_offset_of_manuallyAllocatedViewIds_33() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___manuallyAllocatedViewIds_33)); }
	inline List_1_t128053199 * get_manuallyAllocatedViewIds_33() const { return ___manuallyAllocatedViewIds_33; }
	inline List_1_t128053199 ** get_address_of_manuallyAllocatedViewIds_33() { return &___manuallyAllocatedViewIds_33; }
	inline void set_manuallyAllocatedViewIds_33(List_1_t128053199 * value)
	{
		___manuallyAllocatedViewIds_33 = value;
		Il2CppCodeGenWriteBarrier((&___manuallyAllocatedViewIds_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONNETWORK_T1610183659_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef REGION_T3684225262_H
#define REGION_T3684225262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Region
struct  Region_t3684225262  : public RuntimeObject
{
public:
	// CloudRegionCode Region::Code
	int32_t ___Code_0;
	// System.String Region::Cluster
	String_t* ___Cluster_1;
	// System.String Region::HostAndPort
	String_t* ___HostAndPort_2;
	// System.Int32 Region::Ping
	int32_t ___Ping_3;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(Region_t3684225262, ___Code_0)); }
	inline int32_t get_Code_0() const { return ___Code_0; }
	inline int32_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(int32_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Cluster_1() { return static_cast<int32_t>(offsetof(Region_t3684225262, ___Cluster_1)); }
	inline String_t* get_Cluster_1() const { return ___Cluster_1; }
	inline String_t** get_address_of_Cluster_1() { return &___Cluster_1; }
	inline void set_Cluster_1(String_t* value)
	{
		___Cluster_1 = value;
		Il2CppCodeGenWriteBarrier((&___Cluster_1), value);
	}

	inline static int32_t get_offset_of_HostAndPort_2() { return static_cast<int32_t>(offsetof(Region_t3684225262, ___HostAndPort_2)); }
	inline String_t* get_HostAndPort_2() const { return ___HostAndPort_2; }
	inline String_t** get_address_of_HostAndPort_2() { return &___HostAndPort_2; }
	inline void set_HostAndPort_2(String_t* value)
	{
		___HostAndPort_2 = value;
		Il2CppCodeGenWriteBarrier((&___HostAndPort_2), value);
	}

	inline static int32_t get_offset_of_Ping_3() { return static_cast<int32_t>(offsetof(Region_t3684225262, ___Ping_3)); }
	inline int32_t get_Ping_3() const { return ___Ping_3; }
	inline int32_t* get_address_of_Ping_3() { return &___Ping_3; }
	inline void set_Ping_3(int32_t value)
	{
		___Ping_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGION_T3684225262_H
#ifndef SYNCHRONIZEDLAYER_T3485728275_H
#define SYNCHRONIZEDLAYER_T3485728275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/SynchronizedLayer
struct  SynchronizedLayer_t3485728275  : public RuntimeObject
{
public:
	// PhotonAnimatorView/SynchronizeType PhotonAnimatorView/SynchronizedLayer::SynchronizeType
	int32_t ___SynchronizeType_0;
	// System.Int32 PhotonAnimatorView/SynchronizedLayer::LayerIndex
	int32_t ___LayerIndex_1;

public:
	inline static int32_t get_offset_of_SynchronizeType_0() { return static_cast<int32_t>(offsetof(SynchronizedLayer_t3485728275, ___SynchronizeType_0)); }
	inline int32_t get_SynchronizeType_0() const { return ___SynchronizeType_0; }
	inline int32_t* get_address_of_SynchronizeType_0() { return &___SynchronizeType_0; }
	inline void set_SynchronizeType_0(int32_t value)
	{
		___SynchronizeType_0 = value;
	}

	inline static int32_t get_offset_of_LayerIndex_1() { return static_cast<int32_t>(offsetof(SynchronizedLayer_t3485728275, ___LayerIndex_1)); }
	inline int32_t get_LayerIndex_1() const { return ___LayerIndex_1; }
	inline int32_t* get_address_of_LayerIndex_1() { return &___LayerIndex_1; }
	inline void set_LayerIndex_1(int32_t value)
	{
		___LayerIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCHRONIZEDLAYER_T3485728275_H
#ifndef AUTHENTICATIONVALUES_T660572511_H
#define AUTHENTICATIONVALUES_T660572511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AuthenticationValues
struct  AuthenticationValues_t660572511  : public RuntimeObject
{
public:
	// CustomAuthenticationType AuthenticationValues::authType
	uint8_t ___authType_0;
	// System.String AuthenticationValues::<AuthGetParameters>k__BackingField
	String_t* ___U3CAuthGetParametersU3Ek__BackingField_1;
	// System.Object AuthenticationValues::<AuthPostData>k__BackingField
	RuntimeObject * ___U3CAuthPostDataU3Ek__BackingField_2;
	// System.String AuthenticationValues::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_3;
	// System.String AuthenticationValues::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_authType_0() { return static_cast<int32_t>(offsetof(AuthenticationValues_t660572511, ___authType_0)); }
	inline uint8_t get_authType_0() const { return ___authType_0; }
	inline uint8_t* get_address_of_authType_0() { return &___authType_0; }
	inline void set_authType_0(uint8_t value)
	{
		___authType_0 = value;
	}

	inline static int32_t get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AuthenticationValues_t660572511, ___U3CAuthGetParametersU3Ek__BackingField_1)); }
	inline String_t* get_U3CAuthGetParametersU3Ek__BackingField_1() const { return ___U3CAuthGetParametersU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAuthGetParametersU3Ek__BackingField_1() { return &___U3CAuthGetParametersU3Ek__BackingField_1; }
	inline void set_U3CAuthGetParametersU3Ek__BackingField_1(String_t* value)
	{
		___U3CAuthGetParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthGetParametersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAuthPostDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AuthenticationValues_t660572511, ___U3CAuthPostDataU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CAuthPostDataU3Ek__BackingField_2() const { return ___U3CAuthPostDataU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CAuthPostDataU3Ek__BackingField_2() { return &___U3CAuthPostDataU3Ek__BackingField_2; }
	inline void set_U3CAuthPostDataU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CAuthPostDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthPostDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AuthenticationValues_t660572511, ___U3CTokenU3Ek__BackingField_3)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_3() const { return ___U3CTokenU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_3() { return &___U3CTokenU3Ek__BackingField_3; }
	inline void set_U3CTokenU3Ek__BackingField_3(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AuthenticationValues_t660572511, ___U3CUserIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_4() const { return ___U3CUserIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_4() { return &___U3CUserIdU3Ek__BackingField_4; }
	inline void set_U3CUserIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONVALUES_T660572511_H
#ifndef SYNCHRONIZEDPARAMETER_T1800668114_H
#define SYNCHRONIZEDPARAMETER_T1800668114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView/SynchronizedParameter
struct  SynchronizedParameter_t1800668114  : public RuntimeObject
{
public:
	// PhotonAnimatorView/ParameterType PhotonAnimatorView/SynchronizedParameter::Type
	int32_t ___Type_0;
	// PhotonAnimatorView/SynchronizeType PhotonAnimatorView/SynchronizedParameter::SynchronizeType
	int32_t ___SynchronizeType_1;
	// System.String PhotonAnimatorView/SynchronizedParameter::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(SynchronizedParameter_t1800668114, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_SynchronizeType_1() { return static_cast<int32_t>(offsetof(SynchronizedParameter_t1800668114, ___SynchronizeType_1)); }
	inline int32_t get_SynchronizeType_1() const { return ___SynchronizeType_1; }
	inline int32_t* get_address_of_SynchronizeType_1() { return &___SynchronizeType_1; }
	inline void set_SynchronizeType_1(int32_t value)
	{
		___SynchronizeType_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(SynchronizedParameter_t1800668114, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCHRONIZEDPARAMETER_T1800668114_H
#ifndef OPJOINRANDOMROOMPARAMS_T491090087_H
#define OPJOINRANDOMROOMPARAMS_T491090087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpJoinRandomRoomParams
struct  OpJoinRandomRoomParams_t491090087  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.Hashtable OpJoinRandomRoomParams::ExpectedCustomRoomProperties
	Hashtable_t1048209202 * ___ExpectedCustomRoomProperties_0;
	// System.Byte OpJoinRandomRoomParams::ExpectedMaxPlayers
	uint8_t ___ExpectedMaxPlayers_1;
	// MatchmakingMode OpJoinRandomRoomParams::MatchingType
	uint8_t ___MatchingType_2;
	// TypedLobby OpJoinRandomRoomParams::TypedLobby
	TypedLobby_t3336582029 * ___TypedLobby_3;
	// System.String OpJoinRandomRoomParams::SqlLobbyFilter
	String_t* ___SqlLobbyFilter_4;
	// System.String[] OpJoinRandomRoomParams::ExpectedUsers
	StringU5BU5D_t1281789340* ___ExpectedUsers_5;

public:
	inline static int32_t get_offset_of_ExpectedCustomRoomProperties_0() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___ExpectedCustomRoomProperties_0)); }
	inline Hashtable_t1048209202 * get_ExpectedCustomRoomProperties_0() const { return ___ExpectedCustomRoomProperties_0; }
	inline Hashtable_t1048209202 ** get_address_of_ExpectedCustomRoomProperties_0() { return &___ExpectedCustomRoomProperties_0; }
	inline void set_ExpectedCustomRoomProperties_0(Hashtable_t1048209202 * value)
	{
		___ExpectedCustomRoomProperties_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExpectedCustomRoomProperties_0), value);
	}

	inline static int32_t get_offset_of_ExpectedMaxPlayers_1() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___ExpectedMaxPlayers_1)); }
	inline uint8_t get_ExpectedMaxPlayers_1() const { return ___ExpectedMaxPlayers_1; }
	inline uint8_t* get_address_of_ExpectedMaxPlayers_1() { return &___ExpectedMaxPlayers_1; }
	inline void set_ExpectedMaxPlayers_1(uint8_t value)
	{
		___ExpectedMaxPlayers_1 = value;
	}

	inline static int32_t get_offset_of_MatchingType_2() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___MatchingType_2)); }
	inline uint8_t get_MatchingType_2() const { return ___MatchingType_2; }
	inline uint8_t* get_address_of_MatchingType_2() { return &___MatchingType_2; }
	inline void set_MatchingType_2(uint8_t value)
	{
		___MatchingType_2 = value;
	}

	inline static int32_t get_offset_of_TypedLobby_3() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___TypedLobby_3)); }
	inline TypedLobby_t3336582029 * get_TypedLobby_3() const { return ___TypedLobby_3; }
	inline TypedLobby_t3336582029 ** get_address_of_TypedLobby_3() { return &___TypedLobby_3; }
	inline void set_TypedLobby_3(TypedLobby_t3336582029 * value)
	{
		___TypedLobby_3 = value;
		Il2CppCodeGenWriteBarrier((&___TypedLobby_3), value);
	}

	inline static int32_t get_offset_of_SqlLobbyFilter_4() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___SqlLobbyFilter_4)); }
	inline String_t* get_SqlLobbyFilter_4() const { return ___SqlLobbyFilter_4; }
	inline String_t** get_address_of_SqlLobbyFilter_4() { return &___SqlLobbyFilter_4; }
	inline void set_SqlLobbyFilter_4(String_t* value)
	{
		___SqlLobbyFilter_4 = value;
		Il2CppCodeGenWriteBarrier((&___SqlLobbyFilter_4), value);
	}

	inline static int32_t get_offset_of_ExpectedUsers_5() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___ExpectedUsers_5)); }
	inline StringU5BU5D_t1281789340* get_ExpectedUsers_5() const { return ___ExpectedUsers_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ExpectedUsers_5() { return &___ExpectedUsers_5; }
	inline void set_ExpectedUsers_5(StringU5BU5D_t1281789340* value)
	{
		___ExpectedUsers_5 = value;
		Il2CppCodeGenWriteBarrier((&___ExpectedUsers_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPJOINRANDOMROOMPARAMS_T491090087_H
#ifndef PHOTONTRANSFORMVIEWSCALEMODEL_T763003770_H
#define PHOTONTRANSFORMVIEWSCALEMODEL_T763003770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewScaleModel
struct  PhotonTransformViewScaleModel_t763003770  : public RuntimeObject
{
public:
	// System.Boolean PhotonTransformViewScaleModel::SynchronizeEnabled
	bool ___SynchronizeEnabled_0;
	// PhotonTransformViewScaleModel/InterpolateOptions PhotonTransformViewScaleModel::InterpolateOption
	int32_t ___InterpolateOption_1;
	// System.Single PhotonTransformViewScaleModel::InterpolateMoveTowardsSpeed
	float ___InterpolateMoveTowardsSpeed_2;
	// System.Single PhotonTransformViewScaleModel::InterpolateLerpSpeed
	float ___InterpolateLerpSpeed_3;

public:
	inline static int32_t get_offset_of_SynchronizeEnabled_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleModel_t763003770, ___SynchronizeEnabled_0)); }
	inline bool get_SynchronizeEnabled_0() const { return ___SynchronizeEnabled_0; }
	inline bool* get_address_of_SynchronizeEnabled_0() { return &___SynchronizeEnabled_0; }
	inline void set_SynchronizeEnabled_0(bool value)
	{
		___SynchronizeEnabled_0 = value;
	}

	inline static int32_t get_offset_of_InterpolateOption_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleModel_t763003770, ___InterpolateOption_1)); }
	inline int32_t get_InterpolateOption_1() const { return ___InterpolateOption_1; }
	inline int32_t* get_address_of_InterpolateOption_1() { return &___InterpolateOption_1; }
	inline void set_InterpolateOption_1(int32_t value)
	{
		___InterpolateOption_1 = value;
	}

	inline static int32_t get_offset_of_InterpolateMoveTowardsSpeed_2() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleModel_t763003770, ___InterpolateMoveTowardsSpeed_2)); }
	inline float get_InterpolateMoveTowardsSpeed_2() const { return ___InterpolateMoveTowardsSpeed_2; }
	inline float* get_address_of_InterpolateMoveTowardsSpeed_2() { return &___InterpolateMoveTowardsSpeed_2; }
	inline void set_InterpolateMoveTowardsSpeed_2(float value)
	{
		___InterpolateMoveTowardsSpeed_2 = value;
	}

	inline static int32_t get_offset_of_InterpolateLerpSpeed_3() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleModel_t763003770, ___InterpolateLerpSpeed_3)); }
	inline float get_InterpolateLerpSpeed_3() const { return ___InterpolateLerpSpeed_3; }
	inline float* get_address_of_InterpolateLerpSpeed_3() { return &___InterpolateLerpSpeed_3; }
	inline void set_InterpolateLerpSpeed_3(float value)
	{
		___InterpolateLerpSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONTRANSFORMVIEWSCALEMODEL_T763003770_H
#ifndef PHOTONTRANSFORMVIEWROTATIONMODEL_T1080899250_H
#define PHOTONTRANSFORMVIEWROTATIONMODEL_T1080899250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewRotationModel
struct  PhotonTransformViewRotationModel_t1080899250  : public RuntimeObject
{
public:
	// System.Boolean PhotonTransformViewRotationModel::SynchronizeEnabled
	bool ___SynchronizeEnabled_0;
	// PhotonTransformViewRotationModel/InterpolateOptions PhotonTransformViewRotationModel::InterpolateOption
	int32_t ___InterpolateOption_1;
	// System.Single PhotonTransformViewRotationModel::InterpolateRotateTowardsSpeed
	float ___InterpolateRotateTowardsSpeed_2;
	// System.Single PhotonTransformViewRotationModel::InterpolateLerpSpeed
	float ___InterpolateLerpSpeed_3;

public:
	inline static int32_t get_offset_of_SynchronizeEnabled_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationModel_t1080899250, ___SynchronizeEnabled_0)); }
	inline bool get_SynchronizeEnabled_0() const { return ___SynchronizeEnabled_0; }
	inline bool* get_address_of_SynchronizeEnabled_0() { return &___SynchronizeEnabled_0; }
	inline void set_SynchronizeEnabled_0(bool value)
	{
		___SynchronizeEnabled_0 = value;
	}

	inline static int32_t get_offset_of_InterpolateOption_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationModel_t1080899250, ___InterpolateOption_1)); }
	inline int32_t get_InterpolateOption_1() const { return ___InterpolateOption_1; }
	inline int32_t* get_address_of_InterpolateOption_1() { return &___InterpolateOption_1; }
	inline void set_InterpolateOption_1(int32_t value)
	{
		___InterpolateOption_1 = value;
	}

	inline static int32_t get_offset_of_InterpolateRotateTowardsSpeed_2() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationModel_t1080899250, ___InterpolateRotateTowardsSpeed_2)); }
	inline float get_InterpolateRotateTowardsSpeed_2() const { return ___InterpolateRotateTowardsSpeed_2; }
	inline float* get_address_of_InterpolateRotateTowardsSpeed_2() { return &___InterpolateRotateTowardsSpeed_2; }
	inline void set_InterpolateRotateTowardsSpeed_2(float value)
	{
		___InterpolateRotateTowardsSpeed_2 = value;
	}

	inline static int32_t get_offset_of_InterpolateLerpSpeed_3() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationModel_t1080899250, ___InterpolateLerpSpeed_3)); }
	inline float get_InterpolateLerpSpeed_3() const { return ___InterpolateLerpSpeed_3; }
	inline float* get_address_of_InterpolateLerpSpeed_3() { return &___InterpolateLerpSpeed_3; }
	inline void set_InterpolateLerpSpeed_3(float value)
	{
		___InterpolateLerpSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONTRANSFORMVIEWROTATIONMODEL_T1080899250_H
#ifndef PHOTONTRANSFORMVIEWPOSITIONMODEL_T2500134640_H
#define PHOTONTRANSFORMVIEWPOSITIONMODEL_T2500134640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformViewPositionModel
struct  PhotonTransformViewPositionModel_t2500134640  : public RuntimeObject
{
public:
	// System.Boolean PhotonTransformViewPositionModel::SynchronizeEnabled
	bool ___SynchronizeEnabled_0;
	// System.Boolean PhotonTransformViewPositionModel::TeleportEnabled
	bool ___TeleportEnabled_1;
	// System.Single PhotonTransformViewPositionModel::TeleportIfDistanceGreaterThan
	float ___TeleportIfDistanceGreaterThan_2;
	// PhotonTransformViewPositionModel/InterpolateOptions PhotonTransformViewPositionModel::InterpolateOption
	int32_t ___InterpolateOption_3;
	// System.Single PhotonTransformViewPositionModel::InterpolateMoveTowardsSpeed
	float ___InterpolateMoveTowardsSpeed_4;
	// System.Single PhotonTransformViewPositionModel::InterpolateLerpSpeed
	float ___InterpolateLerpSpeed_5;
	// System.Single PhotonTransformViewPositionModel::InterpolateMoveTowardsAcceleration
	float ___InterpolateMoveTowardsAcceleration_6;
	// System.Single PhotonTransformViewPositionModel::InterpolateMoveTowardsDeceleration
	float ___InterpolateMoveTowardsDeceleration_7;
	// UnityEngine.AnimationCurve PhotonTransformViewPositionModel::InterpolateSpeedCurve
	AnimationCurve_t3046754366 * ___InterpolateSpeedCurve_8;
	// PhotonTransformViewPositionModel/ExtrapolateOptions PhotonTransformViewPositionModel::ExtrapolateOption
	int32_t ___ExtrapolateOption_9;
	// System.Single PhotonTransformViewPositionModel::ExtrapolateSpeed
	float ___ExtrapolateSpeed_10;
	// System.Boolean PhotonTransformViewPositionModel::ExtrapolateIncludingRoundTripTime
	bool ___ExtrapolateIncludingRoundTripTime_11;
	// System.Int32 PhotonTransformViewPositionModel::ExtrapolateNumberOfStoredPositions
	int32_t ___ExtrapolateNumberOfStoredPositions_12;
	// System.Boolean PhotonTransformViewPositionModel::DrawErrorGizmo
	bool ___DrawErrorGizmo_13;

public:
	inline static int32_t get_offset_of_SynchronizeEnabled_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___SynchronizeEnabled_0)); }
	inline bool get_SynchronizeEnabled_0() const { return ___SynchronizeEnabled_0; }
	inline bool* get_address_of_SynchronizeEnabled_0() { return &___SynchronizeEnabled_0; }
	inline void set_SynchronizeEnabled_0(bool value)
	{
		___SynchronizeEnabled_0 = value;
	}

	inline static int32_t get_offset_of_TeleportEnabled_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___TeleportEnabled_1)); }
	inline bool get_TeleportEnabled_1() const { return ___TeleportEnabled_1; }
	inline bool* get_address_of_TeleportEnabled_1() { return &___TeleportEnabled_1; }
	inline void set_TeleportEnabled_1(bool value)
	{
		___TeleportEnabled_1 = value;
	}

	inline static int32_t get_offset_of_TeleportIfDistanceGreaterThan_2() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___TeleportIfDistanceGreaterThan_2)); }
	inline float get_TeleportIfDistanceGreaterThan_2() const { return ___TeleportIfDistanceGreaterThan_2; }
	inline float* get_address_of_TeleportIfDistanceGreaterThan_2() { return &___TeleportIfDistanceGreaterThan_2; }
	inline void set_TeleportIfDistanceGreaterThan_2(float value)
	{
		___TeleportIfDistanceGreaterThan_2 = value;
	}

	inline static int32_t get_offset_of_InterpolateOption_3() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___InterpolateOption_3)); }
	inline int32_t get_InterpolateOption_3() const { return ___InterpolateOption_3; }
	inline int32_t* get_address_of_InterpolateOption_3() { return &___InterpolateOption_3; }
	inline void set_InterpolateOption_3(int32_t value)
	{
		___InterpolateOption_3 = value;
	}

	inline static int32_t get_offset_of_InterpolateMoveTowardsSpeed_4() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___InterpolateMoveTowardsSpeed_4)); }
	inline float get_InterpolateMoveTowardsSpeed_4() const { return ___InterpolateMoveTowardsSpeed_4; }
	inline float* get_address_of_InterpolateMoveTowardsSpeed_4() { return &___InterpolateMoveTowardsSpeed_4; }
	inline void set_InterpolateMoveTowardsSpeed_4(float value)
	{
		___InterpolateMoveTowardsSpeed_4 = value;
	}

	inline static int32_t get_offset_of_InterpolateLerpSpeed_5() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___InterpolateLerpSpeed_5)); }
	inline float get_InterpolateLerpSpeed_5() const { return ___InterpolateLerpSpeed_5; }
	inline float* get_address_of_InterpolateLerpSpeed_5() { return &___InterpolateLerpSpeed_5; }
	inline void set_InterpolateLerpSpeed_5(float value)
	{
		___InterpolateLerpSpeed_5 = value;
	}

	inline static int32_t get_offset_of_InterpolateMoveTowardsAcceleration_6() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___InterpolateMoveTowardsAcceleration_6)); }
	inline float get_InterpolateMoveTowardsAcceleration_6() const { return ___InterpolateMoveTowardsAcceleration_6; }
	inline float* get_address_of_InterpolateMoveTowardsAcceleration_6() { return &___InterpolateMoveTowardsAcceleration_6; }
	inline void set_InterpolateMoveTowardsAcceleration_6(float value)
	{
		___InterpolateMoveTowardsAcceleration_6 = value;
	}

	inline static int32_t get_offset_of_InterpolateMoveTowardsDeceleration_7() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___InterpolateMoveTowardsDeceleration_7)); }
	inline float get_InterpolateMoveTowardsDeceleration_7() const { return ___InterpolateMoveTowardsDeceleration_7; }
	inline float* get_address_of_InterpolateMoveTowardsDeceleration_7() { return &___InterpolateMoveTowardsDeceleration_7; }
	inline void set_InterpolateMoveTowardsDeceleration_7(float value)
	{
		___InterpolateMoveTowardsDeceleration_7 = value;
	}

	inline static int32_t get_offset_of_InterpolateSpeedCurve_8() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___InterpolateSpeedCurve_8)); }
	inline AnimationCurve_t3046754366 * get_InterpolateSpeedCurve_8() const { return ___InterpolateSpeedCurve_8; }
	inline AnimationCurve_t3046754366 ** get_address_of_InterpolateSpeedCurve_8() { return &___InterpolateSpeedCurve_8; }
	inline void set_InterpolateSpeedCurve_8(AnimationCurve_t3046754366 * value)
	{
		___InterpolateSpeedCurve_8 = value;
		Il2CppCodeGenWriteBarrier((&___InterpolateSpeedCurve_8), value);
	}

	inline static int32_t get_offset_of_ExtrapolateOption_9() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___ExtrapolateOption_9)); }
	inline int32_t get_ExtrapolateOption_9() const { return ___ExtrapolateOption_9; }
	inline int32_t* get_address_of_ExtrapolateOption_9() { return &___ExtrapolateOption_9; }
	inline void set_ExtrapolateOption_9(int32_t value)
	{
		___ExtrapolateOption_9 = value;
	}

	inline static int32_t get_offset_of_ExtrapolateSpeed_10() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___ExtrapolateSpeed_10)); }
	inline float get_ExtrapolateSpeed_10() const { return ___ExtrapolateSpeed_10; }
	inline float* get_address_of_ExtrapolateSpeed_10() { return &___ExtrapolateSpeed_10; }
	inline void set_ExtrapolateSpeed_10(float value)
	{
		___ExtrapolateSpeed_10 = value;
	}

	inline static int32_t get_offset_of_ExtrapolateIncludingRoundTripTime_11() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___ExtrapolateIncludingRoundTripTime_11)); }
	inline bool get_ExtrapolateIncludingRoundTripTime_11() const { return ___ExtrapolateIncludingRoundTripTime_11; }
	inline bool* get_address_of_ExtrapolateIncludingRoundTripTime_11() { return &___ExtrapolateIncludingRoundTripTime_11; }
	inline void set_ExtrapolateIncludingRoundTripTime_11(bool value)
	{
		___ExtrapolateIncludingRoundTripTime_11 = value;
	}

	inline static int32_t get_offset_of_ExtrapolateNumberOfStoredPositions_12() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___ExtrapolateNumberOfStoredPositions_12)); }
	inline int32_t get_ExtrapolateNumberOfStoredPositions_12() const { return ___ExtrapolateNumberOfStoredPositions_12; }
	inline int32_t* get_address_of_ExtrapolateNumberOfStoredPositions_12() { return &___ExtrapolateNumberOfStoredPositions_12; }
	inline void set_ExtrapolateNumberOfStoredPositions_12(int32_t value)
	{
		___ExtrapolateNumberOfStoredPositions_12 = value;
	}

	inline static int32_t get_offset_of_DrawErrorGizmo_13() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t2500134640, ___DrawErrorGizmo_13)); }
	inline bool get_DrawErrorGizmo_13() const { return ___DrawErrorGizmo_13; }
	inline bool* get_address_of_DrawErrorGizmo_13() { return &___DrawErrorGizmo_13; }
	inline void set_DrawErrorGizmo_13(bool value)
	{
		___DrawErrorGizmo_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONTRANSFORMVIEWPOSITIONMODEL_T2500134640_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef PHOTONPEER_T1608153861_H
#define PHOTONPEER_T1608153861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonPeer
struct  PhotonPeer_t1608153861  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ClientSdkId
	uint8_t ___ClientSdkId_3;
	// System.String ExitGames.Client.Photon.PhotonPeer::clientVersion
	String_t* ___clientVersion_5;
	// ExitGames.Client.Photon.SerializationProtocol ExitGames.Client.Photon.PhotonPeer::<SerializationProtocolType>k__BackingField
	int32_t ___U3CSerializationProtocolTypeU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type> ExitGames.Client.Photon.PhotonPeer::SocketImplementationConfig
	Dictionary_2_t1253839074 * ___SocketImplementationConfig_7;
	// System.Type ExitGames.Client.Photon.PhotonPeer::<SocketImplementation>k__BackingField
	Type_t * ___U3CSocketImplementationU3Ek__BackingField_8;
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PhotonPeer::DebugOut
	uint8_t ___DebugOut_9;
	// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::<Listener>k__BackingField
	RuntimeObject* ___U3CListenerU3Ek__BackingField_10;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsIncoming>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsIncomingU3Ek__BackingField_11;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsOutgoing>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsOutgoingU3Ek__BackingField_12;
	// ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::<TrafficStatsGameLevel>k__BackingField
	TrafficStatsGameLevel_t4013908777 * ___U3CTrafficStatsGameLevelU3Ek__BackingField_13;
	// System.Diagnostics.Stopwatch ExitGames.Client.Photon.PhotonPeer::trafficStatsStopwatch
	Stopwatch_t305734070 * ___trafficStatsStopwatch_14;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::trafficStatsEnabled
	bool ___trafficStatsEnabled_15;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::commandLogSize
	int32_t ___commandLogSize_16;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<EnableServerTracing>k__BackingField
	bool ___U3CEnableServerTracingU3Ek__BackingField_17;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::quickResendAttempts
	uint8_t ___quickResendAttempts_18;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMinConnections
	int32_t ___RhttpMinConnections_19;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMaxConnections
	int32_t ___RhttpMaxConnections_20;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::<LimitOfUnreliableCommands>k__BackingField
	int32_t ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ChannelCount
	uint8_t ___ChannelCount_22;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::crcEnabled
	bool ___crcEnabled_23;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::WarningSize
	int32_t ___WarningSize_24;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::SentCountAllowance
	int32_t ___SentCountAllowance_25;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::TimePingInterval
	int32_t ___TimePingInterval_26;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::DisconnectTimeout
	int32_t ___DisconnectTimeout_27;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::<TransportProtocol>k__BackingField
	uint8_t ___U3CTransportProtocolU3Ek__BackingField_28;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::mtu
	int32_t ___mtu_30;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<IsSendingOnlyAcks>k__BackingField
	bool ___U3CIsSendingOnlyAcksU3Ek__BackingField_31;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PhotonPeer::peerBase
	PeerBase_t2956237011 * ___peerBase_32;
	// System.Object ExitGames.Client.Photon.PhotonPeer::SendOutgoingLockObject
	RuntimeObject * ___SendOutgoingLockObject_33;
	// System.Object ExitGames.Client.Photon.PhotonPeer::DispatchLockObject
	RuntimeObject * ___DispatchLockObject_34;
	// System.Object ExitGames.Client.Photon.PhotonPeer::EnqueueLock
	RuntimeObject * ___EnqueueLock_35;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::PayloadEncryptionSecret
	ByteU5BU5D_t4116647657* ___PayloadEncryptionSecret_36;
	// ExitGames.Client.Photon.EncryptorManaged.Encryptor ExitGames.Client.Photon.PhotonPeer::encryptor
	Encryptor_t200327285 * ___encryptor_37;
	// ExitGames.Client.Photon.EncryptorManaged.Decryptor ExitGames.Client.Photon.PhotonPeer::decryptor
	Decryptor_t2116099858 * ___decryptor_38;

public:
	inline static int32_t get_offset_of_ClientSdkId_3() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ClientSdkId_3)); }
	inline uint8_t get_ClientSdkId_3() const { return ___ClientSdkId_3; }
	inline uint8_t* get_address_of_ClientSdkId_3() { return &___ClientSdkId_3; }
	inline void set_ClientSdkId_3(uint8_t value)
	{
		___ClientSdkId_3 = value;
	}

	inline static int32_t get_offset_of_clientVersion_5() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___clientVersion_5)); }
	inline String_t* get_clientVersion_5() const { return ___clientVersion_5; }
	inline String_t** get_address_of_clientVersion_5() { return &___clientVersion_5; }
	inline void set_clientVersion_5(String_t* value)
	{
		___clientVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___clientVersion_5), value);
	}

	inline static int32_t get_offset_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSerializationProtocolTypeU3Ek__BackingField_6)); }
	inline int32_t get_U3CSerializationProtocolTypeU3Ek__BackingField_6() const { return ___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return &___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline void set_U3CSerializationProtocolTypeU3Ek__BackingField_6(int32_t value)
	{
		___U3CSerializationProtocolTypeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_SocketImplementationConfig_7() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SocketImplementationConfig_7)); }
	inline Dictionary_2_t1253839074 * get_SocketImplementationConfig_7() const { return ___SocketImplementationConfig_7; }
	inline Dictionary_2_t1253839074 ** get_address_of_SocketImplementationConfig_7() { return &___SocketImplementationConfig_7; }
	inline void set_SocketImplementationConfig_7(Dictionary_2_t1253839074 * value)
	{
		___SocketImplementationConfig_7 = value;
		Il2CppCodeGenWriteBarrier((&___SocketImplementationConfig_7), value);
	}

	inline static int32_t get_offset_of_U3CSocketImplementationU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSocketImplementationU3Ek__BackingField_8)); }
	inline Type_t * get_U3CSocketImplementationU3Ek__BackingField_8() const { return ___U3CSocketImplementationU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CSocketImplementationU3Ek__BackingField_8() { return &___U3CSocketImplementationU3Ek__BackingField_8; }
	inline void set_U3CSocketImplementationU3Ek__BackingField_8(Type_t * value)
	{
		___U3CSocketImplementationU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketImplementationU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_DebugOut_9() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DebugOut_9)); }
	inline uint8_t get_DebugOut_9() const { return ___DebugOut_9; }
	inline uint8_t* get_address_of_DebugOut_9() { return &___DebugOut_9; }
	inline void set_DebugOut_9(uint8_t value)
	{
		___DebugOut_9 = value;
	}

	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CListenerU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CListenerU3Ek__BackingField_10() const { return ___U3CListenerU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CListenerU3Ek__BackingField_10() { return &___U3CListenerU3Ek__BackingField_10; }
	inline void set_U3CListenerU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CListenerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListenerU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsIncomingU3Ek__BackingField_11)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsIncomingU3Ek__BackingField_11() const { return ___U3CTrafficStatsIncomingU3Ek__BackingField_11; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsIncomingU3Ek__BackingField_11() { return &___U3CTrafficStatsIncomingU3Ek__BackingField_11; }
	inline void set_U3CTrafficStatsIncomingU3Ek__BackingField_11(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsIncomingU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsIncomingU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsOutgoingU3Ek__BackingField_12)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsOutgoingU3Ek__BackingField_12() const { return ___U3CTrafficStatsOutgoingU3Ek__BackingField_12; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsOutgoingU3Ek__BackingField_12() { return &___U3CTrafficStatsOutgoingU3Ek__BackingField_12; }
	inline void set_U3CTrafficStatsOutgoingU3Ek__BackingField_12(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsOutgoingU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsOutgoingU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsGameLevelU3Ek__BackingField_13)); }
	inline TrafficStatsGameLevel_t4013908777 * get_U3CTrafficStatsGameLevelU3Ek__BackingField_13() const { return ___U3CTrafficStatsGameLevelU3Ek__BackingField_13; }
	inline TrafficStatsGameLevel_t4013908777 ** get_address_of_U3CTrafficStatsGameLevelU3Ek__BackingField_13() { return &___U3CTrafficStatsGameLevelU3Ek__BackingField_13; }
	inline void set_U3CTrafficStatsGameLevelU3Ek__BackingField_13(TrafficStatsGameLevel_t4013908777 * value)
	{
		___U3CTrafficStatsGameLevelU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsGameLevelU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_trafficStatsStopwatch_14() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsStopwatch_14)); }
	inline Stopwatch_t305734070 * get_trafficStatsStopwatch_14() const { return ___trafficStatsStopwatch_14; }
	inline Stopwatch_t305734070 ** get_address_of_trafficStatsStopwatch_14() { return &___trafficStatsStopwatch_14; }
	inline void set_trafficStatsStopwatch_14(Stopwatch_t305734070 * value)
	{
		___trafficStatsStopwatch_14 = value;
		Il2CppCodeGenWriteBarrier((&___trafficStatsStopwatch_14), value);
	}

	inline static int32_t get_offset_of_trafficStatsEnabled_15() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsEnabled_15)); }
	inline bool get_trafficStatsEnabled_15() const { return ___trafficStatsEnabled_15; }
	inline bool* get_address_of_trafficStatsEnabled_15() { return &___trafficStatsEnabled_15; }
	inline void set_trafficStatsEnabled_15(bool value)
	{
		___trafficStatsEnabled_15 = value;
	}

	inline static int32_t get_offset_of_commandLogSize_16() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___commandLogSize_16)); }
	inline int32_t get_commandLogSize_16() const { return ___commandLogSize_16; }
	inline int32_t* get_address_of_commandLogSize_16() { return &___commandLogSize_16; }
	inline void set_commandLogSize_16(int32_t value)
	{
		___commandLogSize_16 = value;
	}

	inline static int32_t get_offset_of_U3CEnableServerTracingU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CEnableServerTracingU3Ek__BackingField_17)); }
	inline bool get_U3CEnableServerTracingU3Ek__BackingField_17() const { return ___U3CEnableServerTracingU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CEnableServerTracingU3Ek__BackingField_17() { return &___U3CEnableServerTracingU3Ek__BackingField_17; }
	inline void set_U3CEnableServerTracingU3Ek__BackingField_17(bool value)
	{
		___U3CEnableServerTracingU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_quickResendAttempts_18() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___quickResendAttempts_18)); }
	inline uint8_t get_quickResendAttempts_18() const { return ___quickResendAttempts_18; }
	inline uint8_t* get_address_of_quickResendAttempts_18() { return &___quickResendAttempts_18; }
	inline void set_quickResendAttempts_18(uint8_t value)
	{
		___quickResendAttempts_18 = value;
	}

	inline static int32_t get_offset_of_RhttpMinConnections_19() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMinConnections_19)); }
	inline int32_t get_RhttpMinConnections_19() const { return ___RhttpMinConnections_19; }
	inline int32_t* get_address_of_RhttpMinConnections_19() { return &___RhttpMinConnections_19; }
	inline void set_RhttpMinConnections_19(int32_t value)
	{
		___RhttpMinConnections_19 = value;
	}

	inline static int32_t get_offset_of_RhttpMaxConnections_20() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMaxConnections_20)); }
	inline int32_t get_RhttpMaxConnections_20() const { return ___RhttpMaxConnections_20; }
	inline int32_t* get_address_of_RhttpMaxConnections_20() { return &___RhttpMaxConnections_20; }
	inline void set_RhttpMaxConnections_20(int32_t value)
	{
		___RhttpMaxConnections_20 = value;
	}

	inline static int32_t get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21)); }
	inline int32_t get_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21() const { return ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21() { return &___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21; }
	inline void set_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21(int32_t value)
	{
		___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_ChannelCount_22() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ChannelCount_22)); }
	inline uint8_t get_ChannelCount_22() const { return ___ChannelCount_22; }
	inline uint8_t* get_address_of_ChannelCount_22() { return &___ChannelCount_22; }
	inline void set_ChannelCount_22(uint8_t value)
	{
		___ChannelCount_22 = value;
	}

	inline static int32_t get_offset_of_crcEnabled_23() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___crcEnabled_23)); }
	inline bool get_crcEnabled_23() const { return ___crcEnabled_23; }
	inline bool* get_address_of_crcEnabled_23() { return &___crcEnabled_23; }
	inline void set_crcEnabled_23(bool value)
	{
		___crcEnabled_23 = value;
	}

	inline static int32_t get_offset_of_WarningSize_24() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___WarningSize_24)); }
	inline int32_t get_WarningSize_24() const { return ___WarningSize_24; }
	inline int32_t* get_address_of_WarningSize_24() { return &___WarningSize_24; }
	inline void set_WarningSize_24(int32_t value)
	{
		___WarningSize_24 = value;
	}

	inline static int32_t get_offset_of_SentCountAllowance_25() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SentCountAllowance_25)); }
	inline int32_t get_SentCountAllowance_25() const { return ___SentCountAllowance_25; }
	inline int32_t* get_address_of_SentCountAllowance_25() { return &___SentCountAllowance_25; }
	inline void set_SentCountAllowance_25(int32_t value)
	{
		___SentCountAllowance_25 = value;
	}

	inline static int32_t get_offset_of_TimePingInterval_26() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___TimePingInterval_26)); }
	inline int32_t get_TimePingInterval_26() const { return ___TimePingInterval_26; }
	inline int32_t* get_address_of_TimePingInterval_26() { return &___TimePingInterval_26; }
	inline void set_TimePingInterval_26(int32_t value)
	{
		___TimePingInterval_26 = value;
	}

	inline static int32_t get_offset_of_DisconnectTimeout_27() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DisconnectTimeout_27)); }
	inline int32_t get_DisconnectTimeout_27() const { return ___DisconnectTimeout_27; }
	inline int32_t* get_address_of_DisconnectTimeout_27() { return &___DisconnectTimeout_27; }
	inline void set_DisconnectTimeout_27(int32_t value)
	{
		___DisconnectTimeout_27 = value;
	}

	inline static int32_t get_offset_of_U3CTransportProtocolU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTransportProtocolU3Ek__BackingField_28)); }
	inline uint8_t get_U3CTransportProtocolU3Ek__BackingField_28() const { return ___U3CTransportProtocolU3Ek__BackingField_28; }
	inline uint8_t* get_address_of_U3CTransportProtocolU3Ek__BackingField_28() { return &___U3CTransportProtocolU3Ek__BackingField_28; }
	inline void set_U3CTransportProtocolU3Ek__BackingField_28(uint8_t value)
	{
		___U3CTransportProtocolU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_mtu_30() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___mtu_30)); }
	inline int32_t get_mtu_30() const { return ___mtu_30; }
	inline int32_t* get_address_of_mtu_30() { return &___mtu_30; }
	inline void set_mtu_30(int32_t value)
	{
		___mtu_30 = value;
	}

	inline static int32_t get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CIsSendingOnlyAcksU3Ek__BackingField_31)); }
	inline bool get_U3CIsSendingOnlyAcksU3Ek__BackingField_31() const { return ___U3CIsSendingOnlyAcksU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CIsSendingOnlyAcksU3Ek__BackingField_31() { return &___U3CIsSendingOnlyAcksU3Ek__BackingField_31; }
	inline void set_U3CIsSendingOnlyAcksU3Ek__BackingField_31(bool value)
	{
		___U3CIsSendingOnlyAcksU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_peerBase_32() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___peerBase_32)); }
	inline PeerBase_t2956237011 * get_peerBase_32() const { return ___peerBase_32; }
	inline PeerBase_t2956237011 ** get_address_of_peerBase_32() { return &___peerBase_32; }
	inline void set_peerBase_32(PeerBase_t2956237011 * value)
	{
		___peerBase_32 = value;
		Il2CppCodeGenWriteBarrier((&___peerBase_32), value);
	}

	inline static int32_t get_offset_of_SendOutgoingLockObject_33() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SendOutgoingLockObject_33)); }
	inline RuntimeObject * get_SendOutgoingLockObject_33() const { return ___SendOutgoingLockObject_33; }
	inline RuntimeObject ** get_address_of_SendOutgoingLockObject_33() { return &___SendOutgoingLockObject_33; }
	inline void set_SendOutgoingLockObject_33(RuntimeObject * value)
	{
		___SendOutgoingLockObject_33 = value;
		Il2CppCodeGenWriteBarrier((&___SendOutgoingLockObject_33), value);
	}

	inline static int32_t get_offset_of_DispatchLockObject_34() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DispatchLockObject_34)); }
	inline RuntimeObject * get_DispatchLockObject_34() const { return ___DispatchLockObject_34; }
	inline RuntimeObject ** get_address_of_DispatchLockObject_34() { return &___DispatchLockObject_34; }
	inline void set_DispatchLockObject_34(RuntimeObject * value)
	{
		___DispatchLockObject_34 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchLockObject_34), value);
	}

	inline static int32_t get_offset_of_EnqueueLock_35() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___EnqueueLock_35)); }
	inline RuntimeObject * get_EnqueueLock_35() const { return ___EnqueueLock_35; }
	inline RuntimeObject ** get_address_of_EnqueueLock_35() { return &___EnqueueLock_35; }
	inline void set_EnqueueLock_35(RuntimeObject * value)
	{
		___EnqueueLock_35 = value;
		Il2CppCodeGenWriteBarrier((&___EnqueueLock_35), value);
	}

	inline static int32_t get_offset_of_PayloadEncryptionSecret_36() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___PayloadEncryptionSecret_36)); }
	inline ByteU5BU5D_t4116647657* get_PayloadEncryptionSecret_36() const { return ___PayloadEncryptionSecret_36; }
	inline ByteU5BU5D_t4116647657** get_address_of_PayloadEncryptionSecret_36() { return &___PayloadEncryptionSecret_36; }
	inline void set_PayloadEncryptionSecret_36(ByteU5BU5D_t4116647657* value)
	{
		___PayloadEncryptionSecret_36 = value;
		Il2CppCodeGenWriteBarrier((&___PayloadEncryptionSecret_36), value);
	}

	inline static int32_t get_offset_of_encryptor_37() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___encryptor_37)); }
	inline Encryptor_t200327285 * get_encryptor_37() const { return ___encryptor_37; }
	inline Encryptor_t200327285 ** get_address_of_encryptor_37() { return &___encryptor_37; }
	inline void set_encryptor_37(Encryptor_t200327285 * value)
	{
		___encryptor_37 = value;
		Il2CppCodeGenWriteBarrier((&___encryptor_37), value);
	}

	inline static int32_t get_offset_of_decryptor_38() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___decryptor_38)); }
	inline Decryptor_t2116099858 * get_decryptor_38() const { return ___decryptor_38; }
	inline Decryptor_t2116099858 ** get_address_of_decryptor_38() { return &___decryptor_38; }
	inline void set_decryptor_38(Decryptor_t2116099858 * value)
	{
		___decryptor_38 = value;
		Il2CppCodeGenWriteBarrier((&___decryptor_38), value);
	}
};

struct PhotonPeer_t1608153861_StaticFields
{
public:
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::AsyncKeyExchange
	bool ___AsyncKeyExchange_4;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::OutgoingStreamBufferSize
	int32_t ___OutgoingStreamBufferSize_29;

public:
	inline static int32_t get_offset_of_AsyncKeyExchange_4() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___AsyncKeyExchange_4)); }
	inline bool get_AsyncKeyExchange_4() const { return ___AsyncKeyExchange_4; }
	inline bool* get_address_of_AsyncKeyExchange_4() { return &___AsyncKeyExchange_4; }
	inline void set_AsyncKeyExchange_4(bool value)
	{
		___AsyncKeyExchange_4 = value;
	}

	inline static int32_t get_offset_of_OutgoingStreamBufferSize_29() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___OutgoingStreamBufferSize_29)); }
	inline int32_t get_OutgoingStreamBufferSize_29() const { return ___OutgoingStreamBufferSize_29; }
	inline int32_t* get_address_of_OutgoingStreamBufferSize_29() { return &___OutgoingStreamBufferSize_29; }
	inline void set_OutgoingStreamBufferSize_29(int32_t value)
	{
		___OutgoingStreamBufferSize_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPEER_T1608153861_H
#ifndef LOADBALANCINGPEER_T3218467959_H
#define LOADBALANCINGPEER_T3218467959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadBalancingPeer
struct  LoadBalancingPeer_t3218467959  : public PhotonPeer_t1608153861
{
public:
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> LoadBalancingPeer::opParameters
	Dictionary_2_t1405253484 * ___opParameters_39;

public:
	inline static int32_t get_offset_of_opParameters_39() { return static_cast<int32_t>(offsetof(LoadBalancingPeer_t3218467959, ___opParameters_39)); }
	inline Dictionary_2_t1405253484 * get_opParameters_39() const { return ___opParameters_39; }
	inline Dictionary_2_t1405253484 ** get_address_of_opParameters_39() { return &___opParameters_39; }
	inline void set_opParameters_39(Dictionary_2_t1405253484 * value)
	{
		___opParameters_39 = value;
		Il2CppCodeGenWriteBarrier((&___opParameters_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADBALANCINGPEER_T3218467959_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef SERVERSETTINGS_T2755303613_H
#define SERVERSETTINGS_T2755303613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerSettings
struct  ServerSettings_t2755303613  : public ScriptableObject_t2528358522
{
public:
	// System.String ServerSettings::AppID
	String_t* ___AppID_2;
	// System.String ServerSettings::VoiceAppID
	String_t* ___VoiceAppID_3;
	// System.String ServerSettings::ChatAppID
	String_t* ___ChatAppID_4;
	// ServerSettings/HostingOption ServerSettings::HostType
	int32_t ___HostType_5;
	// CloudRegionCode ServerSettings::PreferredRegion
	int32_t ___PreferredRegion_6;
	// CloudRegionFlag ServerSettings::EnabledRegions
	int32_t ___EnabledRegions_7;
	// ExitGames.Client.Photon.ConnectionProtocol ServerSettings::Protocol
	uint8_t ___Protocol_8;
	// System.String ServerSettings::ServerAddress
	String_t* ___ServerAddress_9;
	// System.Int32 ServerSettings::ServerPort
	int32_t ___ServerPort_10;
	// System.Int32 ServerSettings::VoiceServerPort
	int32_t ___VoiceServerPort_11;
	// System.Boolean ServerSettings::JoinLobby
	bool ___JoinLobby_12;
	// System.Boolean ServerSettings::EnableLobbyStatistics
	bool ___EnableLobbyStatistics_13;
	// PhotonLogLevel ServerSettings::PunLogging
	int32_t ___PunLogging_14;
	// ExitGames.Client.Photon.DebugLevel ServerSettings::NetworkLogging
	uint8_t ___NetworkLogging_15;
	// System.Boolean ServerSettings::RunInBackground
	bool ___RunInBackground_16;
	// System.Collections.Generic.List`1<System.String> ServerSettings::RpcList
	List_1_t3319525431 * ___RpcList_17;
	// System.Boolean ServerSettings::DisableAutoOpenWizard
	bool ___DisableAutoOpenWizard_18;

public:
	inline static int32_t get_offset_of_AppID_2() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___AppID_2)); }
	inline String_t* get_AppID_2() const { return ___AppID_2; }
	inline String_t** get_address_of_AppID_2() { return &___AppID_2; }
	inline void set_AppID_2(String_t* value)
	{
		___AppID_2 = value;
		Il2CppCodeGenWriteBarrier((&___AppID_2), value);
	}

	inline static int32_t get_offset_of_VoiceAppID_3() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___VoiceAppID_3)); }
	inline String_t* get_VoiceAppID_3() const { return ___VoiceAppID_3; }
	inline String_t** get_address_of_VoiceAppID_3() { return &___VoiceAppID_3; }
	inline void set_VoiceAppID_3(String_t* value)
	{
		___VoiceAppID_3 = value;
		Il2CppCodeGenWriteBarrier((&___VoiceAppID_3), value);
	}

	inline static int32_t get_offset_of_ChatAppID_4() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___ChatAppID_4)); }
	inline String_t* get_ChatAppID_4() const { return ___ChatAppID_4; }
	inline String_t** get_address_of_ChatAppID_4() { return &___ChatAppID_4; }
	inline void set_ChatAppID_4(String_t* value)
	{
		___ChatAppID_4 = value;
		Il2CppCodeGenWriteBarrier((&___ChatAppID_4), value);
	}

	inline static int32_t get_offset_of_HostType_5() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___HostType_5)); }
	inline int32_t get_HostType_5() const { return ___HostType_5; }
	inline int32_t* get_address_of_HostType_5() { return &___HostType_5; }
	inline void set_HostType_5(int32_t value)
	{
		___HostType_5 = value;
	}

	inline static int32_t get_offset_of_PreferredRegion_6() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___PreferredRegion_6)); }
	inline int32_t get_PreferredRegion_6() const { return ___PreferredRegion_6; }
	inline int32_t* get_address_of_PreferredRegion_6() { return &___PreferredRegion_6; }
	inline void set_PreferredRegion_6(int32_t value)
	{
		___PreferredRegion_6 = value;
	}

	inline static int32_t get_offset_of_EnabledRegions_7() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___EnabledRegions_7)); }
	inline int32_t get_EnabledRegions_7() const { return ___EnabledRegions_7; }
	inline int32_t* get_address_of_EnabledRegions_7() { return &___EnabledRegions_7; }
	inline void set_EnabledRegions_7(int32_t value)
	{
		___EnabledRegions_7 = value;
	}

	inline static int32_t get_offset_of_Protocol_8() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___Protocol_8)); }
	inline uint8_t get_Protocol_8() const { return ___Protocol_8; }
	inline uint8_t* get_address_of_Protocol_8() { return &___Protocol_8; }
	inline void set_Protocol_8(uint8_t value)
	{
		___Protocol_8 = value;
	}

	inline static int32_t get_offset_of_ServerAddress_9() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___ServerAddress_9)); }
	inline String_t* get_ServerAddress_9() const { return ___ServerAddress_9; }
	inline String_t** get_address_of_ServerAddress_9() { return &___ServerAddress_9; }
	inline void set_ServerAddress_9(String_t* value)
	{
		___ServerAddress_9 = value;
		Il2CppCodeGenWriteBarrier((&___ServerAddress_9), value);
	}

	inline static int32_t get_offset_of_ServerPort_10() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___ServerPort_10)); }
	inline int32_t get_ServerPort_10() const { return ___ServerPort_10; }
	inline int32_t* get_address_of_ServerPort_10() { return &___ServerPort_10; }
	inline void set_ServerPort_10(int32_t value)
	{
		___ServerPort_10 = value;
	}

	inline static int32_t get_offset_of_VoiceServerPort_11() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___VoiceServerPort_11)); }
	inline int32_t get_VoiceServerPort_11() const { return ___VoiceServerPort_11; }
	inline int32_t* get_address_of_VoiceServerPort_11() { return &___VoiceServerPort_11; }
	inline void set_VoiceServerPort_11(int32_t value)
	{
		___VoiceServerPort_11 = value;
	}

	inline static int32_t get_offset_of_JoinLobby_12() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___JoinLobby_12)); }
	inline bool get_JoinLobby_12() const { return ___JoinLobby_12; }
	inline bool* get_address_of_JoinLobby_12() { return &___JoinLobby_12; }
	inline void set_JoinLobby_12(bool value)
	{
		___JoinLobby_12 = value;
	}

	inline static int32_t get_offset_of_EnableLobbyStatistics_13() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___EnableLobbyStatistics_13)); }
	inline bool get_EnableLobbyStatistics_13() const { return ___EnableLobbyStatistics_13; }
	inline bool* get_address_of_EnableLobbyStatistics_13() { return &___EnableLobbyStatistics_13; }
	inline void set_EnableLobbyStatistics_13(bool value)
	{
		___EnableLobbyStatistics_13 = value;
	}

	inline static int32_t get_offset_of_PunLogging_14() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___PunLogging_14)); }
	inline int32_t get_PunLogging_14() const { return ___PunLogging_14; }
	inline int32_t* get_address_of_PunLogging_14() { return &___PunLogging_14; }
	inline void set_PunLogging_14(int32_t value)
	{
		___PunLogging_14 = value;
	}

	inline static int32_t get_offset_of_NetworkLogging_15() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___NetworkLogging_15)); }
	inline uint8_t get_NetworkLogging_15() const { return ___NetworkLogging_15; }
	inline uint8_t* get_address_of_NetworkLogging_15() { return &___NetworkLogging_15; }
	inline void set_NetworkLogging_15(uint8_t value)
	{
		___NetworkLogging_15 = value;
	}

	inline static int32_t get_offset_of_RunInBackground_16() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___RunInBackground_16)); }
	inline bool get_RunInBackground_16() const { return ___RunInBackground_16; }
	inline bool* get_address_of_RunInBackground_16() { return &___RunInBackground_16; }
	inline void set_RunInBackground_16(bool value)
	{
		___RunInBackground_16 = value;
	}

	inline static int32_t get_offset_of_RpcList_17() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___RpcList_17)); }
	inline List_1_t3319525431 * get_RpcList_17() const { return ___RpcList_17; }
	inline List_1_t3319525431 ** get_address_of_RpcList_17() { return &___RpcList_17; }
	inline void set_RpcList_17(List_1_t3319525431 * value)
	{
		___RpcList_17 = value;
		Il2CppCodeGenWriteBarrier((&___RpcList_17), value);
	}

	inline static int32_t get_offset_of_DisableAutoOpenWizard_18() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___DisableAutoOpenWizard_18)); }
	inline bool get_DisableAutoOpenWizard_18() const { return ___DisableAutoOpenWizard_18; }
	inline bool* get_address_of_DisableAutoOpenWizard_18() { return &___DisableAutoOpenWizard_18; }
	inline void set_DisableAutoOpenWizard_18(bool value)
	{
		___DisableAutoOpenWizard_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSETTINGS_T2755303613_H
#ifndef EVENTCALLBACK_T1220598991_H
#define EVENTCALLBACK_T1220598991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonNetwork/EventCallback
struct  EventCallback_t1220598991  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTCALLBACK_T1220598991_H
#ifndef TYPEDLOBBYINFO_T2504508049_H
#define TYPEDLOBBYINFO_T2504508049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TypedLobbyInfo
struct  TypedLobbyInfo_t2504508049  : public TypedLobby_t3336582029
{
public:
	// System.Int32 TypedLobbyInfo::PlayerCount
	int32_t ___PlayerCount_3;
	// System.Int32 TypedLobbyInfo::RoomCount
	int32_t ___RoomCount_4;

public:
	inline static int32_t get_offset_of_PlayerCount_3() { return static_cast<int32_t>(offsetof(TypedLobbyInfo_t2504508049, ___PlayerCount_3)); }
	inline int32_t get_PlayerCount_3() const { return ___PlayerCount_3; }
	inline int32_t* get_address_of_PlayerCount_3() { return &___PlayerCount_3; }
	inline void set_PlayerCount_3(int32_t value)
	{
		___PlayerCount_3 = value;
	}

	inline static int32_t get_offset_of_RoomCount_4() { return static_cast<int32_t>(offsetof(TypedLobbyInfo_t2504508049, ___RoomCount_4)); }
	inline int32_t get_RoomCount_4() const { return ___RoomCount_4; }
	inline int32_t* get_address_of_RoomCount_4() { return &___RoomCount_4; }
	inline void set_RoomCount_4(int32_t value)
	{
		___RoomCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDLOBBYINFO_T2504508049_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef NETWORKINGPEER_T264212356_H
#define NETWORKINGPEER_T264212356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkingPeer
struct  NetworkingPeer_t264212356  : public LoadBalancingPeer_t3218467959
{
public:
	// System.String NetworkingPeer::AppId
	String_t* ___AppId_40;
	// AuthenticationValues NetworkingPeer::<AuthValues>k__BackingField
	AuthenticationValues_t660572511 * ___U3CAuthValuesU3Ek__BackingField_41;
	// System.String NetworkingPeer::tokenCache
	String_t* ___tokenCache_42;
	// AuthModeOption NetworkingPeer::AuthMode
	int32_t ___AuthMode_43;
	// EncryptionMode NetworkingPeer::EncryptionMode
	int32_t ___EncryptionMode_44;
	// System.Boolean NetworkingPeer::<IsUsingNameServer>k__BackingField
	bool ___U3CIsUsingNameServerU3Ek__BackingField_45;
	// System.String NetworkingPeer::<MasterServerAddress>k__BackingField
	String_t* ___U3CMasterServerAddressU3Ek__BackingField_49;
	// System.String NetworkingPeer::<GameServerAddress>k__BackingField
	String_t* ___U3CGameServerAddressU3Ek__BackingField_50;
	// ServerConnection NetworkingPeer::<Server>k__BackingField
	int32_t ___U3CServerU3Ek__BackingField_51;
	// ClientState NetworkingPeer::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_52;
	// System.Boolean NetworkingPeer::IsInitialConnect
	bool ___IsInitialConnect_53;
	// System.Boolean NetworkingPeer::insideLobby
	bool ___insideLobby_54;
	// TypedLobby NetworkingPeer::<lobby>k__BackingField
	TypedLobby_t3336582029 * ___U3ClobbyU3Ek__BackingField_55;
	// System.Collections.Generic.List`1<TypedLobbyInfo> NetworkingPeer::LobbyStatistics
	List_1_t3976582791 * ___LobbyStatistics_56;
	// System.Collections.Generic.Dictionary`2<System.String,RoomInfo> NetworkingPeer::mGameList
	Dictionary_2_t2955551919 * ___mGameList_57;
	// RoomInfo[] NetworkingPeer::mGameListCopy
	RoomInfoU5BU5D_t1491207981* ___mGameListCopy_58;
	// System.String NetworkingPeer::playername
	String_t* ___playername_59;
	// System.Boolean NetworkingPeer::mPlayernameHasToBeUpdated
	bool ___mPlayernameHasToBeUpdated_60;
	// Room NetworkingPeer::currentRoom
	Room_t3759828263 * ___currentRoom_61;
	// PhotonPlayer NetworkingPeer::<LocalPlayer>k__BackingField
	PhotonPlayer_t3305149557 * ___U3CLocalPlayerU3Ek__BackingField_62;
	// System.Int32 NetworkingPeer::<PlayersOnMasterCount>k__BackingField
	int32_t ___U3CPlayersOnMasterCountU3Ek__BackingField_63;
	// System.Int32 NetworkingPeer::<PlayersInRoomsCount>k__BackingField
	int32_t ___U3CPlayersInRoomsCountU3Ek__BackingField_64;
	// System.Int32 NetworkingPeer::<RoomsCount>k__BackingField
	int32_t ___U3CRoomsCountU3Ek__BackingField_65;
	// JoinType NetworkingPeer::lastJoinType
	int32_t ___lastJoinType_66;
	// EnterRoomParams NetworkingPeer::enterRoomParamsCache
	EnterRoomParams_t3960472384 * ___enterRoomParamsCache_67;
	// System.Boolean NetworkingPeer::didAuthenticate
	bool ___didAuthenticate_68;
	// System.String[] NetworkingPeer::friendListRequested
	StringU5BU5D_t1281789340* ___friendListRequested_69;
	// System.Int32 NetworkingPeer::friendListTimestamp
	int32_t ___friendListTimestamp_70;
	// System.Boolean NetworkingPeer::isFetchingFriendList
	bool ___isFetchingFriendList_71;
	// System.Collections.Generic.List`1<Region> NetworkingPeer::<AvailableRegions>k__BackingField
	List_1_t861332708 * ___U3CAvailableRegionsU3Ek__BackingField_72;
	// CloudRegionCode NetworkingPeer::<CloudRegion>k__BackingField
	int32_t ___U3CCloudRegionU3Ek__BackingField_73;
	// System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer> NetworkingPeer::mActors
	Dictionary_2_t2193862888 * ___mActors_74;
	// PhotonPlayer[] NetworkingPeer::mOtherPlayerListCopy
	PhotonPlayerU5BU5D_t2880637464* ___mOtherPlayerListCopy_75;
	// PhotonPlayer[] NetworkingPeer::mPlayerListCopy
	PhotonPlayerU5BU5D_t2880637464* ___mPlayerListCopy_76;
	// System.Boolean NetworkingPeer::hasSwitchedMC
	bool ___hasSwitchedMC_77;
	// System.Collections.Generic.HashSet`1<System.Byte> NetworkingPeer::allowedReceivingGroups
	HashSet_1_t3994213146 * ___allowedReceivingGroups_78;
	// System.Collections.Generic.HashSet`1<System.Byte> NetworkingPeer::blockSendingGroups
	HashSet_1_t3994213146 * ___blockSendingGroups_79;
	// System.Collections.Generic.Dictionary`2<System.Int32,PhotonView> NetworkingPeer::photonViewList
	Dictionary_2_t1096435151 * ___photonViewList_80;
	// PhotonStream NetworkingPeer::readStream
	PhotonStream_t1003850889 * ___readStream_81;
	// PhotonStream NetworkingPeer::pStream
	PhotonStream_t1003850889 * ___pStream_82;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.Hashtable> NetworkingPeer::dataPerGroupReliable
	Dictionary_2_t4231889829 * ___dataPerGroupReliable_83;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.Hashtable> NetworkingPeer::dataPerGroupUnreliable
	Dictionary_2_t4231889829 * ___dataPerGroupUnreliable_84;
	// System.Int16 NetworkingPeer::currentLevelPrefix
	int16_t ___currentLevelPrefix_85;
	// System.Boolean NetworkingPeer::loadingLevelAndPausedNetwork
	bool ___loadingLevelAndPausedNetwork_86;
	// IPunPrefabPool NetworkingPeer::ObjectPool
	RuntimeObject* ___ObjectPool_89;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>> NetworkingPeer::monoRPCMethodsCache
	Dictionary_2_t1499080758 * ___monoRPCMethodsCache_91;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> NetworkingPeer::rpcShortcuts
	Dictionary_2_t2736202052 * ___rpcShortcuts_92;
	// System.String NetworkingPeer::cachedServerAddress
	String_t* ___cachedServerAddress_94;
	// System.String NetworkingPeer::cachedApplicationName
	String_t* ___cachedApplicationName_95;
	// ServerConnection NetworkingPeer::cachedProtocolType
	int32_t ___cachedProtocolType_96;
	// System.Boolean NetworkingPeer::_isReconnecting
	bool ____isReconnecting_97;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Object[]> NetworkingPeer::tempInstantiationData
	Dictionary_2_t1732652656 * ___tempInstantiationData_98;
	// RaiseEventOptions NetworkingPeer::options
	RaiseEventOptions_t1229553678 * ___options_100;

public:
	inline static int32_t get_offset_of_AppId_40() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___AppId_40)); }
	inline String_t* get_AppId_40() const { return ___AppId_40; }
	inline String_t** get_address_of_AppId_40() { return &___AppId_40; }
	inline void set_AppId_40(String_t* value)
	{
		___AppId_40 = value;
		Il2CppCodeGenWriteBarrier((&___AppId_40), value);
	}

	inline static int32_t get_offset_of_U3CAuthValuesU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CAuthValuesU3Ek__BackingField_41)); }
	inline AuthenticationValues_t660572511 * get_U3CAuthValuesU3Ek__BackingField_41() const { return ___U3CAuthValuesU3Ek__BackingField_41; }
	inline AuthenticationValues_t660572511 ** get_address_of_U3CAuthValuesU3Ek__BackingField_41() { return &___U3CAuthValuesU3Ek__BackingField_41; }
	inline void set_U3CAuthValuesU3Ek__BackingField_41(AuthenticationValues_t660572511 * value)
	{
		___U3CAuthValuesU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthValuesU3Ek__BackingField_41), value);
	}

	inline static int32_t get_offset_of_tokenCache_42() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___tokenCache_42)); }
	inline String_t* get_tokenCache_42() const { return ___tokenCache_42; }
	inline String_t** get_address_of_tokenCache_42() { return &___tokenCache_42; }
	inline void set_tokenCache_42(String_t* value)
	{
		___tokenCache_42 = value;
		Il2CppCodeGenWriteBarrier((&___tokenCache_42), value);
	}

	inline static int32_t get_offset_of_AuthMode_43() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___AuthMode_43)); }
	inline int32_t get_AuthMode_43() const { return ___AuthMode_43; }
	inline int32_t* get_address_of_AuthMode_43() { return &___AuthMode_43; }
	inline void set_AuthMode_43(int32_t value)
	{
		___AuthMode_43 = value;
	}

	inline static int32_t get_offset_of_EncryptionMode_44() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___EncryptionMode_44)); }
	inline int32_t get_EncryptionMode_44() const { return ___EncryptionMode_44; }
	inline int32_t* get_address_of_EncryptionMode_44() { return &___EncryptionMode_44; }
	inline void set_EncryptionMode_44(int32_t value)
	{
		___EncryptionMode_44 = value;
	}

	inline static int32_t get_offset_of_U3CIsUsingNameServerU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CIsUsingNameServerU3Ek__BackingField_45)); }
	inline bool get_U3CIsUsingNameServerU3Ek__BackingField_45() const { return ___U3CIsUsingNameServerU3Ek__BackingField_45; }
	inline bool* get_address_of_U3CIsUsingNameServerU3Ek__BackingField_45() { return &___U3CIsUsingNameServerU3Ek__BackingField_45; }
	inline void set_U3CIsUsingNameServerU3Ek__BackingField_45(bool value)
	{
		___U3CIsUsingNameServerU3Ek__BackingField_45 = value;
	}

	inline static int32_t get_offset_of_U3CMasterServerAddressU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CMasterServerAddressU3Ek__BackingField_49)); }
	inline String_t* get_U3CMasterServerAddressU3Ek__BackingField_49() const { return ___U3CMasterServerAddressU3Ek__BackingField_49; }
	inline String_t** get_address_of_U3CMasterServerAddressU3Ek__BackingField_49() { return &___U3CMasterServerAddressU3Ek__BackingField_49; }
	inline void set_U3CMasterServerAddressU3Ek__BackingField_49(String_t* value)
	{
		___U3CMasterServerAddressU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMasterServerAddressU3Ek__BackingField_49), value);
	}

	inline static int32_t get_offset_of_U3CGameServerAddressU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CGameServerAddressU3Ek__BackingField_50)); }
	inline String_t* get_U3CGameServerAddressU3Ek__BackingField_50() const { return ___U3CGameServerAddressU3Ek__BackingField_50; }
	inline String_t** get_address_of_U3CGameServerAddressU3Ek__BackingField_50() { return &___U3CGameServerAddressU3Ek__BackingField_50; }
	inline void set_U3CGameServerAddressU3Ek__BackingField_50(String_t* value)
	{
		___U3CGameServerAddressU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameServerAddressU3Ek__BackingField_50), value);
	}

	inline static int32_t get_offset_of_U3CServerU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CServerU3Ek__BackingField_51)); }
	inline int32_t get_U3CServerU3Ek__BackingField_51() const { return ___U3CServerU3Ek__BackingField_51; }
	inline int32_t* get_address_of_U3CServerU3Ek__BackingField_51() { return &___U3CServerU3Ek__BackingField_51; }
	inline void set_U3CServerU3Ek__BackingField_51(int32_t value)
	{
		___U3CServerU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CStateU3Ek__BackingField_52)); }
	inline int32_t get_U3CStateU3Ek__BackingField_52() const { return ___U3CStateU3Ek__BackingField_52; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_52() { return &___U3CStateU3Ek__BackingField_52; }
	inline void set_U3CStateU3Ek__BackingField_52(int32_t value)
	{
		___U3CStateU3Ek__BackingField_52 = value;
	}

	inline static int32_t get_offset_of_IsInitialConnect_53() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___IsInitialConnect_53)); }
	inline bool get_IsInitialConnect_53() const { return ___IsInitialConnect_53; }
	inline bool* get_address_of_IsInitialConnect_53() { return &___IsInitialConnect_53; }
	inline void set_IsInitialConnect_53(bool value)
	{
		___IsInitialConnect_53 = value;
	}

	inline static int32_t get_offset_of_insideLobby_54() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___insideLobby_54)); }
	inline bool get_insideLobby_54() const { return ___insideLobby_54; }
	inline bool* get_address_of_insideLobby_54() { return &___insideLobby_54; }
	inline void set_insideLobby_54(bool value)
	{
		___insideLobby_54 = value;
	}

	inline static int32_t get_offset_of_U3ClobbyU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3ClobbyU3Ek__BackingField_55)); }
	inline TypedLobby_t3336582029 * get_U3ClobbyU3Ek__BackingField_55() const { return ___U3ClobbyU3Ek__BackingField_55; }
	inline TypedLobby_t3336582029 ** get_address_of_U3ClobbyU3Ek__BackingField_55() { return &___U3ClobbyU3Ek__BackingField_55; }
	inline void set_U3ClobbyU3Ek__BackingField_55(TypedLobby_t3336582029 * value)
	{
		___U3ClobbyU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClobbyU3Ek__BackingField_55), value);
	}

	inline static int32_t get_offset_of_LobbyStatistics_56() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___LobbyStatistics_56)); }
	inline List_1_t3976582791 * get_LobbyStatistics_56() const { return ___LobbyStatistics_56; }
	inline List_1_t3976582791 ** get_address_of_LobbyStatistics_56() { return &___LobbyStatistics_56; }
	inline void set_LobbyStatistics_56(List_1_t3976582791 * value)
	{
		___LobbyStatistics_56 = value;
		Il2CppCodeGenWriteBarrier((&___LobbyStatistics_56), value);
	}

	inline static int32_t get_offset_of_mGameList_57() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mGameList_57)); }
	inline Dictionary_2_t2955551919 * get_mGameList_57() const { return ___mGameList_57; }
	inline Dictionary_2_t2955551919 ** get_address_of_mGameList_57() { return &___mGameList_57; }
	inline void set_mGameList_57(Dictionary_2_t2955551919 * value)
	{
		___mGameList_57 = value;
		Il2CppCodeGenWriteBarrier((&___mGameList_57), value);
	}

	inline static int32_t get_offset_of_mGameListCopy_58() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mGameListCopy_58)); }
	inline RoomInfoU5BU5D_t1491207981* get_mGameListCopy_58() const { return ___mGameListCopy_58; }
	inline RoomInfoU5BU5D_t1491207981** get_address_of_mGameListCopy_58() { return &___mGameListCopy_58; }
	inline void set_mGameListCopy_58(RoomInfoU5BU5D_t1491207981* value)
	{
		___mGameListCopy_58 = value;
		Il2CppCodeGenWriteBarrier((&___mGameListCopy_58), value);
	}

	inline static int32_t get_offset_of_playername_59() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___playername_59)); }
	inline String_t* get_playername_59() const { return ___playername_59; }
	inline String_t** get_address_of_playername_59() { return &___playername_59; }
	inline void set_playername_59(String_t* value)
	{
		___playername_59 = value;
		Il2CppCodeGenWriteBarrier((&___playername_59), value);
	}

	inline static int32_t get_offset_of_mPlayernameHasToBeUpdated_60() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mPlayernameHasToBeUpdated_60)); }
	inline bool get_mPlayernameHasToBeUpdated_60() const { return ___mPlayernameHasToBeUpdated_60; }
	inline bool* get_address_of_mPlayernameHasToBeUpdated_60() { return &___mPlayernameHasToBeUpdated_60; }
	inline void set_mPlayernameHasToBeUpdated_60(bool value)
	{
		___mPlayernameHasToBeUpdated_60 = value;
	}

	inline static int32_t get_offset_of_currentRoom_61() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___currentRoom_61)); }
	inline Room_t3759828263 * get_currentRoom_61() const { return ___currentRoom_61; }
	inline Room_t3759828263 ** get_address_of_currentRoom_61() { return &___currentRoom_61; }
	inline void set_currentRoom_61(Room_t3759828263 * value)
	{
		___currentRoom_61 = value;
		Il2CppCodeGenWriteBarrier((&___currentRoom_61), value);
	}

	inline static int32_t get_offset_of_U3CLocalPlayerU3Ek__BackingField_62() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CLocalPlayerU3Ek__BackingField_62)); }
	inline PhotonPlayer_t3305149557 * get_U3CLocalPlayerU3Ek__BackingField_62() const { return ___U3CLocalPlayerU3Ek__BackingField_62; }
	inline PhotonPlayer_t3305149557 ** get_address_of_U3CLocalPlayerU3Ek__BackingField_62() { return &___U3CLocalPlayerU3Ek__BackingField_62; }
	inline void set_U3CLocalPlayerU3Ek__BackingField_62(PhotonPlayer_t3305149557 * value)
	{
		___U3CLocalPlayerU3Ek__BackingField_62 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLocalPlayerU3Ek__BackingField_62), value);
	}

	inline static int32_t get_offset_of_U3CPlayersOnMasterCountU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CPlayersOnMasterCountU3Ek__BackingField_63)); }
	inline int32_t get_U3CPlayersOnMasterCountU3Ek__BackingField_63() const { return ___U3CPlayersOnMasterCountU3Ek__BackingField_63; }
	inline int32_t* get_address_of_U3CPlayersOnMasterCountU3Ek__BackingField_63() { return &___U3CPlayersOnMasterCountU3Ek__BackingField_63; }
	inline void set_U3CPlayersOnMasterCountU3Ek__BackingField_63(int32_t value)
	{
		___U3CPlayersOnMasterCountU3Ek__BackingField_63 = value;
	}

	inline static int32_t get_offset_of_U3CPlayersInRoomsCountU3Ek__BackingField_64() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CPlayersInRoomsCountU3Ek__BackingField_64)); }
	inline int32_t get_U3CPlayersInRoomsCountU3Ek__BackingField_64() const { return ___U3CPlayersInRoomsCountU3Ek__BackingField_64; }
	inline int32_t* get_address_of_U3CPlayersInRoomsCountU3Ek__BackingField_64() { return &___U3CPlayersInRoomsCountU3Ek__BackingField_64; }
	inline void set_U3CPlayersInRoomsCountU3Ek__BackingField_64(int32_t value)
	{
		___U3CPlayersInRoomsCountU3Ek__BackingField_64 = value;
	}

	inline static int32_t get_offset_of_U3CRoomsCountU3Ek__BackingField_65() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CRoomsCountU3Ek__BackingField_65)); }
	inline int32_t get_U3CRoomsCountU3Ek__BackingField_65() const { return ___U3CRoomsCountU3Ek__BackingField_65; }
	inline int32_t* get_address_of_U3CRoomsCountU3Ek__BackingField_65() { return &___U3CRoomsCountU3Ek__BackingField_65; }
	inline void set_U3CRoomsCountU3Ek__BackingField_65(int32_t value)
	{
		___U3CRoomsCountU3Ek__BackingField_65 = value;
	}

	inline static int32_t get_offset_of_lastJoinType_66() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___lastJoinType_66)); }
	inline int32_t get_lastJoinType_66() const { return ___lastJoinType_66; }
	inline int32_t* get_address_of_lastJoinType_66() { return &___lastJoinType_66; }
	inline void set_lastJoinType_66(int32_t value)
	{
		___lastJoinType_66 = value;
	}

	inline static int32_t get_offset_of_enterRoomParamsCache_67() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___enterRoomParamsCache_67)); }
	inline EnterRoomParams_t3960472384 * get_enterRoomParamsCache_67() const { return ___enterRoomParamsCache_67; }
	inline EnterRoomParams_t3960472384 ** get_address_of_enterRoomParamsCache_67() { return &___enterRoomParamsCache_67; }
	inline void set_enterRoomParamsCache_67(EnterRoomParams_t3960472384 * value)
	{
		___enterRoomParamsCache_67 = value;
		Il2CppCodeGenWriteBarrier((&___enterRoomParamsCache_67), value);
	}

	inline static int32_t get_offset_of_didAuthenticate_68() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___didAuthenticate_68)); }
	inline bool get_didAuthenticate_68() const { return ___didAuthenticate_68; }
	inline bool* get_address_of_didAuthenticate_68() { return &___didAuthenticate_68; }
	inline void set_didAuthenticate_68(bool value)
	{
		___didAuthenticate_68 = value;
	}

	inline static int32_t get_offset_of_friendListRequested_69() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___friendListRequested_69)); }
	inline StringU5BU5D_t1281789340* get_friendListRequested_69() const { return ___friendListRequested_69; }
	inline StringU5BU5D_t1281789340** get_address_of_friendListRequested_69() { return &___friendListRequested_69; }
	inline void set_friendListRequested_69(StringU5BU5D_t1281789340* value)
	{
		___friendListRequested_69 = value;
		Il2CppCodeGenWriteBarrier((&___friendListRequested_69), value);
	}

	inline static int32_t get_offset_of_friendListTimestamp_70() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___friendListTimestamp_70)); }
	inline int32_t get_friendListTimestamp_70() const { return ___friendListTimestamp_70; }
	inline int32_t* get_address_of_friendListTimestamp_70() { return &___friendListTimestamp_70; }
	inline void set_friendListTimestamp_70(int32_t value)
	{
		___friendListTimestamp_70 = value;
	}

	inline static int32_t get_offset_of_isFetchingFriendList_71() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___isFetchingFriendList_71)); }
	inline bool get_isFetchingFriendList_71() const { return ___isFetchingFriendList_71; }
	inline bool* get_address_of_isFetchingFriendList_71() { return &___isFetchingFriendList_71; }
	inline void set_isFetchingFriendList_71(bool value)
	{
		___isFetchingFriendList_71 = value;
	}

	inline static int32_t get_offset_of_U3CAvailableRegionsU3Ek__BackingField_72() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CAvailableRegionsU3Ek__BackingField_72)); }
	inline List_1_t861332708 * get_U3CAvailableRegionsU3Ek__BackingField_72() const { return ___U3CAvailableRegionsU3Ek__BackingField_72; }
	inline List_1_t861332708 ** get_address_of_U3CAvailableRegionsU3Ek__BackingField_72() { return &___U3CAvailableRegionsU3Ek__BackingField_72; }
	inline void set_U3CAvailableRegionsU3Ek__BackingField_72(List_1_t861332708 * value)
	{
		___U3CAvailableRegionsU3Ek__BackingField_72 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAvailableRegionsU3Ek__BackingField_72), value);
	}

	inline static int32_t get_offset_of_U3CCloudRegionU3Ek__BackingField_73() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CCloudRegionU3Ek__BackingField_73)); }
	inline int32_t get_U3CCloudRegionU3Ek__BackingField_73() const { return ___U3CCloudRegionU3Ek__BackingField_73; }
	inline int32_t* get_address_of_U3CCloudRegionU3Ek__BackingField_73() { return &___U3CCloudRegionU3Ek__BackingField_73; }
	inline void set_U3CCloudRegionU3Ek__BackingField_73(int32_t value)
	{
		___U3CCloudRegionU3Ek__BackingField_73 = value;
	}

	inline static int32_t get_offset_of_mActors_74() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mActors_74)); }
	inline Dictionary_2_t2193862888 * get_mActors_74() const { return ___mActors_74; }
	inline Dictionary_2_t2193862888 ** get_address_of_mActors_74() { return &___mActors_74; }
	inline void set_mActors_74(Dictionary_2_t2193862888 * value)
	{
		___mActors_74 = value;
		Il2CppCodeGenWriteBarrier((&___mActors_74), value);
	}

	inline static int32_t get_offset_of_mOtherPlayerListCopy_75() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mOtherPlayerListCopy_75)); }
	inline PhotonPlayerU5BU5D_t2880637464* get_mOtherPlayerListCopy_75() const { return ___mOtherPlayerListCopy_75; }
	inline PhotonPlayerU5BU5D_t2880637464** get_address_of_mOtherPlayerListCopy_75() { return &___mOtherPlayerListCopy_75; }
	inline void set_mOtherPlayerListCopy_75(PhotonPlayerU5BU5D_t2880637464* value)
	{
		___mOtherPlayerListCopy_75 = value;
		Il2CppCodeGenWriteBarrier((&___mOtherPlayerListCopy_75), value);
	}

	inline static int32_t get_offset_of_mPlayerListCopy_76() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mPlayerListCopy_76)); }
	inline PhotonPlayerU5BU5D_t2880637464* get_mPlayerListCopy_76() const { return ___mPlayerListCopy_76; }
	inline PhotonPlayerU5BU5D_t2880637464** get_address_of_mPlayerListCopy_76() { return &___mPlayerListCopy_76; }
	inline void set_mPlayerListCopy_76(PhotonPlayerU5BU5D_t2880637464* value)
	{
		___mPlayerListCopy_76 = value;
		Il2CppCodeGenWriteBarrier((&___mPlayerListCopy_76), value);
	}

	inline static int32_t get_offset_of_hasSwitchedMC_77() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___hasSwitchedMC_77)); }
	inline bool get_hasSwitchedMC_77() const { return ___hasSwitchedMC_77; }
	inline bool* get_address_of_hasSwitchedMC_77() { return &___hasSwitchedMC_77; }
	inline void set_hasSwitchedMC_77(bool value)
	{
		___hasSwitchedMC_77 = value;
	}

	inline static int32_t get_offset_of_allowedReceivingGroups_78() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___allowedReceivingGroups_78)); }
	inline HashSet_1_t3994213146 * get_allowedReceivingGroups_78() const { return ___allowedReceivingGroups_78; }
	inline HashSet_1_t3994213146 ** get_address_of_allowedReceivingGroups_78() { return &___allowedReceivingGroups_78; }
	inline void set_allowedReceivingGroups_78(HashSet_1_t3994213146 * value)
	{
		___allowedReceivingGroups_78 = value;
		Il2CppCodeGenWriteBarrier((&___allowedReceivingGroups_78), value);
	}

	inline static int32_t get_offset_of_blockSendingGroups_79() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___blockSendingGroups_79)); }
	inline HashSet_1_t3994213146 * get_blockSendingGroups_79() const { return ___blockSendingGroups_79; }
	inline HashSet_1_t3994213146 ** get_address_of_blockSendingGroups_79() { return &___blockSendingGroups_79; }
	inline void set_blockSendingGroups_79(HashSet_1_t3994213146 * value)
	{
		___blockSendingGroups_79 = value;
		Il2CppCodeGenWriteBarrier((&___blockSendingGroups_79), value);
	}

	inline static int32_t get_offset_of_photonViewList_80() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___photonViewList_80)); }
	inline Dictionary_2_t1096435151 * get_photonViewList_80() const { return ___photonViewList_80; }
	inline Dictionary_2_t1096435151 ** get_address_of_photonViewList_80() { return &___photonViewList_80; }
	inline void set_photonViewList_80(Dictionary_2_t1096435151 * value)
	{
		___photonViewList_80 = value;
		Il2CppCodeGenWriteBarrier((&___photonViewList_80), value);
	}

	inline static int32_t get_offset_of_readStream_81() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___readStream_81)); }
	inline PhotonStream_t1003850889 * get_readStream_81() const { return ___readStream_81; }
	inline PhotonStream_t1003850889 ** get_address_of_readStream_81() { return &___readStream_81; }
	inline void set_readStream_81(PhotonStream_t1003850889 * value)
	{
		___readStream_81 = value;
		Il2CppCodeGenWriteBarrier((&___readStream_81), value);
	}

	inline static int32_t get_offset_of_pStream_82() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___pStream_82)); }
	inline PhotonStream_t1003850889 * get_pStream_82() const { return ___pStream_82; }
	inline PhotonStream_t1003850889 ** get_address_of_pStream_82() { return &___pStream_82; }
	inline void set_pStream_82(PhotonStream_t1003850889 * value)
	{
		___pStream_82 = value;
		Il2CppCodeGenWriteBarrier((&___pStream_82), value);
	}

	inline static int32_t get_offset_of_dataPerGroupReliable_83() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___dataPerGroupReliable_83)); }
	inline Dictionary_2_t4231889829 * get_dataPerGroupReliable_83() const { return ___dataPerGroupReliable_83; }
	inline Dictionary_2_t4231889829 ** get_address_of_dataPerGroupReliable_83() { return &___dataPerGroupReliable_83; }
	inline void set_dataPerGroupReliable_83(Dictionary_2_t4231889829 * value)
	{
		___dataPerGroupReliable_83 = value;
		Il2CppCodeGenWriteBarrier((&___dataPerGroupReliable_83), value);
	}

	inline static int32_t get_offset_of_dataPerGroupUnreliable_84() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___dataPerGroupUnreliable_84)); }
	inline Dictionary_2_t4231889829 * get_dataPerGroupUnreliable_84() const { return ___dataPerGroupUnreliable_84; }
	inline Dictionary_2_t4231889829 ** get_address_of_dataPerGroupUnreliable_84() { return &___dataPerGroupUnreliable_84; }
	inline void set_dataPerGroupUnreliable_84(Dictionary_2_t4231889829 * value)
	{
		___dataPerGroupUnreliable_84 = value;
		Il2CppCodeGenWriteBarrier((&___dataPerGroupUnreliable_84), value);
	}

	inline static int32_t get_offset_of_currentLevelPrefix_85() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___currentLevelPrefix_85)); }
	inline int16_t get_currentLevelPrefix_85() const { return ___currentLevelPrefix_85; }
	inline int16_t* get_address_of_currentLevelPrefix_85() { return &___currentLevelPrefix_85; }
	inline void set_currentLevelPrefix_85(int16_t value)
	{
		___currentLevelPrefix_85 = value;
	}

	inline static int32_t get_offset_of_loadingLevelAndPausedNetwork_86() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___loadingLevelAndPausedNetwork_86)); }
	inline bool get_loadingLevelAndPausedNetwork_86() const { return ___loadingLevelAndPausedNetwork_86; }
	inline bool* get_address_of_loadingLevelAndPausedNetwork_86() { return &___loadingLevelAndPausedNetwork_86; }
	inline void set_loadingLevelAndPausedNetwork_86(bool value)
	{
		___loadingLevelAndPausedNetwork_86 = value;
	}

	inline static int32_t get_offset_of_ObjectPool_89() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___ObjectPool_89)); }
	inline RuntimeObject* get_ObjectPool_89() const { return ___ObjectPool_89; }
	inline RuntimeObject** get_address_of_ObjectPool_89() { return &___ObjectPool_89; }
	inline void set_ObjectPool_89(RuntimeObject* value)
	{
		___ObjectPool_89 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectPool_89), value);
	}

	inline static int32_t get_offset_of_monoRPCMethodsCache_91() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___monoRPCMethodsCache_91)); }
	inline Dictionary_2_t1499080758 * get_monoRPCMethodsCache_91() const { return ___monoRPCMethodsCache_91; }
	inline Dictionary_2_t1499080758 ** get_address_of_monoRPCMethodsCache_91() { return &___monoRPCMethodsCache_91; }
	inline void set_monoRPCMethodsCache_91(Dictionary_2_t1499080758 * value)
	{
		___monoRPCMethodsCache_91 = value;
		Il2CppCodeGenWriteBarrier((&___monoRPCMethodsCache_91), value);
	}

	inline static int32_t get_offset_of_rpcShortcuts_92() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___rpcShortcuts_92)); }
	inline Dictionary_2_t2736202052 * get_rpcShortcuts_92() const { return ___rpcShortcuts_92; }
	inline Dictionary_2_t2736202052 ** get_address_of_rpcShortcuts_92() { return &___rpcShortcuts_92; }
	inline void set_rpcShortcuts_92(Dictionary_2_t2736202052 * value)
	{
		___rpcShortcuts_92 = value;
		Il2CppCodeGenWriteBarrier((&___rpcShortcuts_92), value);
	}

	inline static int32_t get_offset_of_cachedServerAddress_94() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___cachedServerAddress_94)); }
	inline String_t* get_cachedServerAddress_94() const { return ___cachedServerAddress_94; }
	inline String_t** get_address_of_cachedServerAddress_94() { return &___cachedServerAddress_94; }
	inline void set_cachedServerAddress_94(String_t* value)
	{
		___cachedServerAddress_94 = value;
		Il2CppCodeGenWriteBarrier((&___cachedServerAddress_94), value);
	}

	inline static int32_t get_offset_of_cachedApplicationName_95() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___cachedApplicationName_95)); }
	inline String_t* get_cachedApplicationName_95() const { return ___cachedApplicationName_95; }
	inline String_t** get_address_of_cachedApplicationName_95() { return &___cachedApplicationName_95; }
	inline void set_cachedApplicationName_95(String_t* value)
	{
		___cachedApplicationName_95 = value;
		Il2CppCodeGenWriteBarrier((&___cachedApplicationName_95), value);
	}

	inline static int32_t get_offset_of_cachedProtocolType_96() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___cachedProtocolType_96)); }
	inline int32_t get_cachedProtocolType_96() const { return ___cachedProtocolType_96; }
	inline int32_t* get_address_of_cachedProtocolType_96() { return &___cachedProtocolType_96; }
	inline void set_cachedProtocolType_96(int32_t value)
	{
		___cachedProtocolType_96 = value;
	}

	inline static int32_t get_offset_of__isReconnecting_97() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ____isReconnecting_97)); }
	inline bool get__isReconnecting_97() const { return ____isReconnecting_97; }
	inline bool* get_address_of__isReconnecting_97() { return &____isReconnecting_97; }
	inline void set__isReconnecting_97(bool value)
	{
		____isReconnecting_97 = value;
	}

	inline static int32_t get_offset_of_tempInstantiationData_98() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___tempInstantiationData_98)); }
	inline Dictionary_2_t1732652656 * get_tempInstantiationData_98() const { return ___tempInstantiationData_98; }
	inline Dictionary_2_t1732652656 ** get_address_of_tempInstantiationData_98() { return &___tempInstantiationData_98; }
	inline void set_tempInstantiationData_98(Dictionary_2_t1732652656 * value)
	{
		___tempInstantiationData_98 = value;
		Il2CppCodeGenWriteBarrier((&___tempInstantiationData_98), value);
	}

	inline static int32_t get_offset_of_options_100() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___options_100)); }
	inline RaiseEventOptions_t1229553678 * get_options_100() const { return ___options_100; }
	inline RaiseEventOptions_t1229553678 ** get_address_of_options_100() { return &___options_100; }
	inline void set_options_100(RaiseEventOptions_t1229553678 * value)
	{
		___options_100 = value;
		Il2CppCodeGenWriteBarrier((&___options_100), value);
	}
};

struct NetworkingPeer_t264212356_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32> NetworkingPeer::ProtocolToNameServerPort
	Dictionary_2_t1720840067 * ___ProtocolToNameServerPort_48;
	// System.Boolean NetworkingPeer::UsePrefabCache
	bool ___UsePrefabCache_88;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> NetworkingPeer::PrefabCache
	Dictionary_2_t898892918 * ___PrefabCache_90;
	// System.String NetworkingPeer::OnPhotonInstantiateString
	String_t* ___OnPhotonInstantiateString_93;
	// System.Int32 NetworkingPeer::ObjectsInOneUpdate
	int32_t ___ObjectsInOneUpdate_99;

public:
	inline static int32_t get_offset_of_ProtocolToNameServerPort_48() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___ProtocolToNameServerPort_48)); }
	inline Dictionary_2_t1720840067 * get_ProtocolToNameServerPort_48() const { return ___ProtocolToNameServerPort_48; }
	inline Dictionary_2_t1720840067 ** get_address_of_ProtocolToNameServerPort_48() { return &___ProtocolToNameServerPort_48; }
	inline void set_ProtocolToNameServerPort_48(Dictionary_2_t1720840067 * value)
	{
		___ProtocolToNameServerPort_48 = value;
		Il2CppCodeGenWriteBarrier((&___ProtocolToNameServerPort_48), value);
	}

	inline static int32_t get_offset_of_UsePrefabCache_88() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___UsePrefabCache_88)); }
	inline bool get_UsePrefabCache_88() const { return ___UsePrefabCache_88; }
	inline bool* get_address_of_UsePrefabCache_88() { return &___UsePrefabCache_88; }
	inline void set_UsePrefabCache_88(bool value)
	{
		___UsePrefabCache_88 = value;
	}

	inline static int32_t get_offset_of_PrefabCache_90() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___PrefabCache_90)); }
	inline Dictionary_2_t898892918 * get_PrefabCache_90() const { return ___PrefabCache_90; }
	inline Dictionary_2_t898892918 ** get_address_of_PrefabCache_90() { return &___PrefabCache_90; }
	inline void set_PrefabCache_90(Dictionary_2_t898892918 * value)
	{
		___PrefabCache_90 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabCache_90), value);
	}

	inline static int32_t get_offset_of_OnPhotonInstantiateString_93() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___OnPhotonInstantiateString_93)); }
	inline String_t* get_OnPhotonInstantiateString_93() const { return ___OnPhotonInstantiateString_93; }
	inline String_t** get_address_of_OnPhotonInstantiateString_93() { return &___OnPhotonInstantiateString_93; }
	inline void set_OnPhotonInstantiateString_93(String_t* value)
	{
		___OnPhotonInstantiateString_93 = value;
		Il2CppCodeGenWriteBarrier((&___OnPhotonInstantiateString_93), value);
	}

	inline static int32_t get_offset_of_ObjectsInOneUpdate_99() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___ObjectsInOneUpdate_99)); }
	inline int32_t get_ObjectsInOneUpdate_99() const { return ___ObjectsInOneUpdate_99; }
	inline int32_t* get_address_of_ObjectsInOneUpdate_99() { return &___ObjectsInOneUpdate_99; }
	inline void set_ObjectsInOneUpdate_99(int32_t value)
	{
		___ObjectsInOneUpdate_99 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKINGPEER_T264212356_H
#ifndef PHOTONRIGIDBODY2DVIEW_T585014740_H
#define PHOTONRIGIDBODY2DVIEW_T585014740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonRigidbody2DView
struct  PhotonRigidbody2DView_t585014740  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PhotonRigidbody2DView::m_SynchronizeVelocity
	bool ___m_SynchronizeVelocity_2;
	// System.Boolean PhotonRigidbody2DView::m_SynchronizeAngularVelocity
	bool ___m_SynchronizeAngularVelocity_3;
	// UnityEngine.Rigidbody2D PhotonRigidbody2DView::m_Body
	Rigidbody2D_t939494601 * ___m_Body_4;

public:
	inline static int32_t get_offset_of_m_SynchronizeVelocity_2() { return static_cast<int32_t>(offsetof(PhotonRigidbody2DView_t585014740, ___m_SynchronizeVelocity_2)); }
	inline bool get_m_SynchronizeVelocity_2() const { return ___m_SynchronizeVelocity_2; }
	inline bool* get_address_of_m_SynchronizeVelocity_2() { return &___m_SynchronizeVelocity_2; }
	inline void set_m_SynchronizeVelocity_2(bool value)
	{
		___m_SynchronizeVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizeAngularVelocity_3() { return static_cast<int32_t>(offsetof(PhotonRigidbody2DView_t585014740, ___m_SynchronizeAngularVelocity_3)); }
	inline bool get_m_SynchronizeAngularVelocity_3() const { return ___m_SynchronizeAngularVelocity_3; }
	inline bool* get_address_of_m_SynchronizeAngularVelocity_3() { return &___m_SynchronizeAngularVelocity_3; }
	inline void set_m_SynchronizeAngularVelocity_3(bool value)
	{
		___m_SynchronizeAngularVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_Body_4() { return static_cast<int32_t>(offsetof(PhotonRigidbody2DView_t585014740, ___m_Body_4)); }
	inline Rigidbody2D_t939494601 * get_m_Body_4() const { return ___m_Body_4; }
	inline Rigidbody2D_t939494601 ** get_address_of_m_Body_4() { return &___m_Body_4; }
	inline void set_m_Body_4(Rigidbody2D_t939494601 * value)
	{
		___m_Body_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Body_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONRIGIDBODY2DVIEW_T585014740_H
#ifndef PHOTONANIMATORVIEW_T3352472062_H
#define PHOTONANIMATORVIEW_T3352472062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonAnimatorView
struct  PhotonAnimatorView_t3352472062  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator PhotonAnimatorView::m_Animator
	Animator_t434523843 * ___m_Animator_2;
	// PhotonStreamQueue PhotonAnimatorView::m_StreamQueue
	PhotonStreamQueue_t3244431384 * ___m_StreamQueue_3;
	// System.Boolean PhotonAnimatorView::ShowLayerWeightsInspector
	bool ___ShowLayerWeightsInspector_4;
	// System.Boolean PhotonAnimatorView::ShowParameterInspector
	bool ___ShowParameterInspector_5;
	// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter> PhotonAnimatorView::m_SynchronizeParameters
	List_1_t3272742856 * ___m_SynchronizeParameters_6;
	// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer> PhotonAnimatorView::m_SynchronizeLayers
	List_1_t662835721 * ___m_SynchronizeLayers_7;
	// UnityEngine.Vector3 PhotonAnimatorView::m_ReceiverPosition
	Vector3_t3722313464  ___m_ReceiverPosition_8;
	// System.Single PhotonAnimatorView::m_LastDeserializeTime
	float ___m_LastDeserializeTime_9;
	// System.Boolean PhotonAnimatorView::m_WasSynchronizeTypeChanged
	bool ___m_WasSynchronizeTypeChanged_10;
	// PhotonView PhotonAnimatorView::m_PhotonView
	PhotonView_t2207721820 * ___m_PhotonView_11;
	// System.Collections.Generic.List`1<System.String> PhotonAnimatorView::m_raisedDiscreteTriggersCache
	List_1_t3319525431 * ___m_raisedDiscreteTriggersCache_12;

public:
	inline static int32_t get_offset_of_m_Animator_2() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___m_Animator_2)); }
	inline Animator_t434523843 * get_m_Animator_2() const { return ___m_Animator_2; }
	inline Animator_t434523843 ** get_address_of_m_Animator_2() { return &___m_Animator_2; }
	inline void set_m_Animator_2(Animator_t434523843 * value)
	{
		___m_Animator_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_2), value);
	}

	inline static int32_t get_offset_of_m_StreamQueue_3() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___m_StreamQueue_3)); }
	inline PhotonStreamQueue_t3244431384 * get_m_StreamQueue_3() const { return ___m_StreamQueue_3; }
	inline PhotonStreamQueue_t3244431384 ** get_address_of_m_StreamQueue_3() { return &___m_StreamQueue_3; }
	inline void set_m_StreamQueue_3(PhotonStreamQueue_t3244431384 * value)
	{
		___m_StreamQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_StreamQueue_3), value);
	}

	inline static int32_t get_offset_of_ShowLayerWeightsInspector_4() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___ShowLayerWeightsInspector_4)); }
	inline bool get_ShowLayerWeightsInspector_4() const { return ___ShowLayerWeightsInspector_4; }
	inline bool* get_address_of_ShowLayerWeightsInspector_4() { return &___ShowLayerWeightsInspector_4; }
	inline void set_ShowLayerWeightsInspector_4(bool value)
	{
		___ShowLayerWeightsInspector_4 = value;
	}

	inline static int32_t get_offset_of_ShowParameterInspector_5() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___ShowParameterInspector_5)); }
	inline bool get_ShowParameterInspector_5() const { return ___ShowParameterInspector_5; }
	inline bool* get_address_of_ShowParameterInspector_5() { return &___ShowParameterInspector_5; }
	inline void set_ShowParameterInspector_5(bool value)
	{
		___ShowParameterInspector_5 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizeParameters_6() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___m_SynchronizeParameters_6)); }
	inline List_1_t3272742856 * get_m_SynchronizeParameters_6() const { return ___m_SynchronizeParameters_6; }
	inline List_1_t3272742856 ** get_address_of_m_SynchronizeParameters_6() { return &___m_SynchronizeParameters_6; }
	inline void set_m_SynchronizeParameters_6(List_1_t3272742856 * value)
	{
		___m_SynchronizeParameters_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SynchronizeParameters_6), value);
	}

	inline static int32_t get_offset_of_m_SynchronizeLayers_7() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___m_SynchronizeLayers_7)); }
	inline List_1_t662835721 * get_m_SynchronizeLayers_7() const { return ___m_SynchronizeLayers_7; }
	inline List_1_t662835721 ** get_address_of_m_SynchronizeLayers_7() { return &___m_SynchronizeLayers_7; }
	inline void set_m_SynchronizeLayers_7(List_1_t662835721 * value)
	{
		___m_SynchronizeLayers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SynchronizeLayers_7), value);
	}

	inline static int32_t get_offset_of_m_ReceiverPosition_8() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___m_ReceiverPosition_8)); }
	inline Vector3_t3722313464  get_m_ReceiverPosition_8() const { return ___m_ReceiverPosition_8; }
	inline Vector3_t3722313464 * get_address_of_m_ReceiverPosition_8() { return &___m_ReceiverPosition_8; }
	inline void set_m_ReceiverPosition_8(Vector3_t3722313464  value)
	{
		___m_ReceiverPosition_8 = value;
	}

	inline static int32_t get_offset_of_m_LastDeserializeTime_9() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___m_LastDeserializeTime_9)); }
	inline float get_m_LastDeserializeTime_9() const { return ___m_LastDeserializeTime_9; }
	inline float* get_address_of_m_LastDeserializeTime_9() { return &___m_LastDeserializeTime_9; }
	inline void set_m_LastDeserializeTime_9(float value)
	{
		___m_LastDeserializeTime_9 = value;
	}

	inline static int32_t get_offset_of_m_WasSynchronizeTypeChanged_10() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___m_WasSynchronizeTypeChanged_10)); }
	inline bool get_m_WasSynchronizeTypeChanged_10() const { return ___m_WasSynchronizeTypeChanged_10; }
	inline bool* get_address_of_m_WasSynchronizeTypeChanged_10() { return &___m_WasSynchronizeTypeChanged_10; }
	inline void set_m_WasSynchronizeTypeChanged_10(bool value)
	{
		___m_WasSynchronizeTypeChanged_10 = value;
	}

	inline static int32_t get_offset_of_m_PhotonView_11() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___m_PhotonView_11)); }
	inline PhotonView_t2207721820 * get_m_PhotonView_11() const { return ___m_PhotonView_11; }
	inline PhotonView_t2207721820 ** get_address_of_m_PhotonView_11() { return &___m_PhotonView_11; }
	inline void set_m_PhotonView_11(PhotonView_t2207721820 * value)
	{
		___m_PhotonView_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_PhotonView_11), value);
	}

	inline static int32_t get_offset_of_m_raisedDiscreteTriggersCache_12() { return static_cast<int32_t>(offsetof(PhotonAnimatorView_t3352472062, ___m_raisedDiscreteTriggersCache_12)); }
	inline List_1_t3319525431 * get_m_raisedDiscreteTriggersCache_12() const { return ___m_raisedDiscreteTriggersCache_12; }
	inline List_1_t3319525431 ** get_address_of_m_raisedDiscreteTriggersCache_12() { return &___m_raisedDiscreteTriggersCache_12; }
	inline void set_m_raisedDiscreteTriggersCache_12(List_1_t3319525431 * value)
	{
		___m_raisedDiscreteTriggersCache_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_raisedDiscreteTriggersCache_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONANIMATORVIEW_T3352472062_H
#ifndef ONJOINEDINSTANTIATE_T3153042345_H
#define ONJOINEDINSTANTIATE_T3153042345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnJoinedInstantiate
struct  OnJoinedInstantiate_t3153042345  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform OnJoinedInstantiate::SpawnPosition
	Transform_t3600365921 * ___SpawnPosition_2;
	// System.String OnJoinedInstantiate::m_cam
	String_t* ___m_cam_3;
	// System.String OnJoinedInstantiate::m_controller
	String_t* ___m_controller_4;

public:
	inline static int32_t get_offset_of_SpawnPosition_2() { return static_cast<int32_t>(offsetof(OnJoinedInstantiate_t3153042345, ___SpawnPosition_2)); }
	inline Transform_t3600365921 * get_SpawnPosition_2() const { return ___SpawnPosition_2; }
	inline Transform_t3600365921 ** get_address_of_SpawnPosition_2() { return &___SpawnPosition_2; }
	inline void set_SpawnPosition_2(Transform_t3600365921 * value)
	{
		___SpawnPosition_2 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnPosition_2), value);
	}

	inline static int32_t get_offset_of_m_cam_3() { return static_cast<int32_t>(offsetof(OnJoinedInstantiate_t3153042345, ___m_cam_3)); }
	inline String_t* get_m_cam_3() const { return ___m_cam_3; }
	inline String_t** get_address_of_m_cam_3() { return &___m_cam_3; }
	inline void set_m_cam_3(String_t* value)
	{
		___m_cam_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_cam_3), value);
	}

	inline static int32_t get_offset_of_m_controller_4() { return static_cast<int32_t>(offsetof(OnJoinedInstantiate_t3153042345, ___m_controller_4)); }
	inline String_t* get_m_controller_4() const { return ___m_controller_4; }
	inline String_t** get_address_of_m_controller_4() { return &___m_controller_4; }
	inline void set_m_controller_4(String_t* value)
	{
		___m_controller_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_controller_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONJOINEDINSTANTIATE_T3153042345_H
#ifndef MONOBEHAVIOUR_T3225183318_H
#define MONOBEHAVIOUR_T3225183318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.MonoBehaviour
struct  MonoBehaviour_t3225183318  : public MonoBehaviour_t3962482529
{
public:
	// PhotonView Photon.MonoBehaviour::pvCache
	PhotonView_t2207721820 * ___pvCache_2;

public:
	inline static int32_t get_offset_of_pvCache_2() { return static_cast<int32_t>(offsetof(MonoBehaviour_t3225183318, ___pvCache_2)); }
	inline PhotonView_t2207721820 * get_pvCache_2() const { return ___pvCache_2; }
	inline PhotonView_t2207721820 ** get_address_of_pvCache_2() { return &___pvCache_2; }
	inline void set_pvCache_2(PhotonView_t2207721820 * value)
	{
		___pvCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3225183318_H
#ifndef PHOTONHANDLER_T2139970417_H
#define PHOTONHANDLER_T2139970417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonHandler
struct  PhotonHandler_t2139970417  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 PhotonHandler::updateInterval
	int32_t ___updateInterval_3;
	// System.Int32 PhotonHandler::updateIntervalOnSerialize
	int32_t ___updateIntervalOnSerialize_4;
	// System.Int32 PhotonHandler::nextSendTickCount
	int32_t ___nextSendTickCount_5;
	// System.Int32 PhotonHandler::nextSendTickCountOnSerialize
	int32_t ___nextSendTickCountOnSerialize_6;

public:
	inline static int32_t get_offset_of_updateInterval_3() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417, ___updateInterval_3)); }
	inline int32_t get_updateInterval_3() const { return ___updateInterval_3; }
	inline int32_t* get_address_of_updateInterval_3() { return &___updateInterval_3; }
	inline void set_updateInterval_3(int32_t value)
	{
		___updateInterval_3 = value;
	}

	inline static int32_t get_offset_of_updateIntervalOnSerialize_4() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417, ___updateIntervalOnSerialize_4)); }
	inline int32_t get_updateIntervalOnSerialize_4() const { return ___updateIntervalOnSerialize_4; }
	inline int32_t* get_address_of_updateIntervalOnSerialize_4() { return &___updateIntervalOnSerialize_4; }
	inline void set_updateIntervalOnSerialize_4(int32_t value)
	{
		___updateIntervalOnSerialize_4 = value;
	}

	inline static int32_t get_offset_of_nextSendTickCount_5() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417, ___nextSendTickCount_5)); }
	inline int32_t get_nextSendTickCount_5() const { return ___nextSendTickCount_5; }
	inline int32_t* get_address_of_nextSendTickCount_5() { return &___nextSendTickCount_5; }
	inline void set_nextSendTickCount_5(int32_t value)
	{
		___nextSendTickCount_5 = value;
	}

	inline static int32_t get_offset_of_nextSendTickCountOnSerialize_6() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417, ___nextSendTickCountOnSerialize_6)); }
	inline int32_t get_nextSendTickCountOnSerialize_6() const { return ___nextSendTickCountOnSerialize_6; }
	inline int32_t* get_address_of_nextSendTickCountOnSerialize_6() { return &___nextSendTickCountOnSerialize_6; }
	inline void set_nextSendTickCountOnSerialize_6(int32_t value)
	{
		___nextSendTickCountOnSerialize_6 = value;
	}
};

struct PhotonHandler_t2139970417_StaticFields
{
public:
	// PhotonHandler PhotonHandler::SP
	PhotonHandler_t2139970417 * ___SP_2;
	// System.Boolean PhotonHandler::sendThreadShouldRun
	bool ___sendThreadShouldRun_7;
	// System.Diagnostics.Stopwatch PhotonHandler::timerToStopConnectionInBackground
	Stopwatch_t305734070 * ___timerToStopConnectionInBackground_8;
	// System.Boolean PhotonHandler::AppQuits
	bool ___AppQuits_9;
	// System.Type PhotonHandler::PingImplementation
	Type_t * ___PingImplementation_10;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode> PhotonHandler::<>f__am$cache0
	UnityAction_2_t2165061829 * ___U3CU3Ef__amU24cache0_12;
	// System.Func`1<System.Boolean> PhotonHandler::<>f__mg$cache0
	Func_1_t3822001908 * ___U3CU3Ef__mgU24cache0_13;

public:
	inline static int32_t get_offset_of_SP_2() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417_StaticFields, ___SP_2)); }
	inline PhotonHandler_t2139970417 * get_SP_2() const { return ___SP_2; }
	inline PhotonHandler_t2139970417 ** get_address_of_SP_2() { return &___SP_2; }
	inline void set_SP_2(PhotonHandler_t2139970417 * value)
	{
		___SP_2 = value;
		Il2CppCodeGenWriteBarrier((&___SP_2), value);
	}

	inline static int32_t get_offset_of_sendThreadShouldRun_7() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417_StaticFields, ___sendThreadShouldRun_7)); }
	inline bool get_sendThreadShouldRun_7() const { return ___sendThreadShouldRun_7; }
	inline bool* get_address_of_sendThreadShouldRun_7() { return &___sendThreadShouldRun_7; }
	inline void set_sendThreadShouldRun_7(bool value)
	{
		___sendThreadShouldRun_7 = value;
	}

	inline static int32_t get_offset_of_timerToStopConnectionInBackground_8() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417_StaticFields, ___timerToStopConnectionInBackground_8)); }
	inline Stopwatch_t305734070 * get_timerToStopConnectionInBackground_8() const { return ___timerToStopConnectionInBackground_8; }
	inline Stopwatch_t305734070 ** get_address_of_timerToStopConnectionInBackground_8() { return &___timerToStopConnectionInBackground_8; }
	inline void set_timerToStopConnectionInBackground_8(Stopwatch_t305734070 * value)
	{
		___timerToStopConnectionInBackground_8 = value;
		Il2CppCodeGenWriteBarrier((&___timerToStopConnectionInBackground_8), value);
	}

	inline static int32_t get_offset_of_AppQuits_9() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417_StaticFields, ___AppQuits_9)); }
	inline bool get_AppQuits_9() const { return ___AppQuits_9; }
	inline bool* get_address_of_AppQuits_9() { return &___AppQuits_9; }
	inline void set_AppQuits_9(bool value)
	{
		___AppQuits_9 = value;
	}

	inline static int32_t get_offset_of_PingImplementation_10() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417_StaticFields, ___PingImplementation_10)); }
	inline Type_t * get_PingImplementation_10() const { return ___PingImplementation_10; }
	inline Type_t ** get_address_of_PingImplementation_10() { return &___PingImplementation_10; }
	inline void set_PingImplementation_10(Type_t * value)
	{
		___PingImplementation_10 = value;
		Il2CppCodeGenWriteBarrier((&___PingImplementation_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline UnityAction_2_t2165061829 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline UnityAction_2_t2165061829 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(UnityAction_2_t2165061829 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(PhotonHandler_t2139970417_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Func_1_t3822001908 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Func_1_t3822001908 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Func_1_t3822001908 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONHANDLER_T2139970417_H
#ifndef PHOTONLAGSIMULATIONGUI_T3583255037_H
#define PHOTONLAGSIMULATIONGUI_T3583255037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonLagSimulationGui
struct  PhotonLagSimulationGui_t3583255037  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rect PhotonLagSimulationGui::WindowRect
	Rect_t2360479859  ___WindowRect_2;
	// System.Int32 PhotonLagSimulationGui::WindowId
	int32_t ___WindowId_3;
	// System.Boolean PhotonLagSimulationGui::Visible
	bool ___Visible_4;
	// ExitGames.Client.Photon.PhotonPeer PhotonLagSimulationGui::<Peer>k__BackingField
	PhotonPeer_t1608153861 * ___U3CPeerU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_WindowRect_2() { return static_cast<int32_t>(offsetof(PhotonLagSimulationGui_t3583255037, ___WindowRect_2)); }
	inline Rect_t2360479859  get_WindowRect_2() const { return ___WindowRect_2; }
	inline Rect_t2360479859 * get_address_of_WindowRect_2() { return &___WindowRect_2; }
	inline void set_WindowRect_2(Rect_t2360479859  value)
	{
		___WindowRect_2 = value;
	}

	inline static int32_t get_offset_of_WindowId_3() { return static_cast<int32_t>(offsetof(PhotonLagSimulationGui_t3583255037, ___WindowId_3)); }
	inline int32_t get_WindowId_3() const { return ___WindowId_3; }
	inline int32_t* get_address_of_WindowId_3() { return &___WindowId_3; }
	inline void set_WindowId_3(int32_t value)
	{
		___WindowId_3 = value;
	}

	inline static int32_t get_offset_of_Visible_4() { return static_cast<int32_t>(offsetof(PhotonLagSimulationGui_t3583255037, ___Visible_4)); }
	inline bool get_Visible_4() const { return ___Visible_4; }
	inline bool* get_address_of_Visible_4() { return &___Visible_4; }
	inline void set_Visible_4(bool value)
	{
		___Visible_4 = value;
	}

	inline static int32_t get_offset_of_U3CPeerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PhotonLagSimulationGui_t3583255037, ___U3CPeerU3Ek__BackingField_5)); }
	inline PhotonPeer_t1608153861 * get_U3CPeerU3Ek__BackingField_5() const { return ___U3CPeerU3Ek__BackingField_5; }
	inline PhotonPeer_t1608153861 ** get_address_of_U3CPeerU3Ek__BackingField_5() { return &___U3CPeerU3Ek__BackingField_5; }
	inline void set_U3CPeerU3Ek__BackingField_5(PhotonPeer_t1608153861 * value)
	{
		___U3CPeerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPeerU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONLAGSIMULATIONGUI_T3583255037_H
#ifndef PHOTONSTATSGUI_T1231606017_H
#define PHOTONSTATSGUI_T1231606017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonStatsGui
struct  PhotonStatsGui_t1231606017  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PhotonStatsGui::statsWindowOn
	bool ___statsWindowOn_2;
	// System.Boolean PhotonStatsGui::statsOn
	bool ___statsOn_3;
	// System.Boolean PhotonStatsGui::healthStatsVisible
	bool ___healthStatsVisible_4;
	// System.Boolean PhotonStatsGui::trafficStatsOn
	bool ___trafficStatsOn_5;
	// System.Boolean PhotonStatsGui::buttonsOn
	bool ___buttonsOn_6;
	// UnityEngine.Rect PhotonStatsGui::statsRect
	Rect_t2360479859  ___statsRect_7;
	// System.Int32 PhotonStatsGui::WindowId
	int32_t ___WindowId_8;

public:
	inline static int32_t get_offset_of_statsWindowOn_2() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t1231606017, ___statsWindowOn_2)); }
	inline bool get_statsWindowOn_2() const { return ___statsWindowOn_2; }
	inline bool* get_address_of_statsWindowOn_2() { return &___statsWindowOn_2; }
	inline void set_statsWindowOn_2(bool value)
	{
		___statsWindowOn_2 = value;
	}

	inline static int32_t get_offset_of_statsOn_3() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t1231606017, ___statsOn_3)); }
	inline bool get_statsOn_3() const { return ___statsOn_3; }
	inline bool* get_address_of_statsOn_3() { return &___statsOn_3; }
	inline void set_statsOn_3(bool value)
	{
		___statsOn_3 = value;
	}

	inline static int32_t get_offset_of_healthStatsVisible_4() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t1231606017, ___healthStatsVisible_4)); }
	inline bool get_healthStatsVisible_4() const { return ___healthStatsVisible_4; }
	inline bool* get_address_of_healthStatsVisible_4() { return &___healthStatsVisible_4; }
	inline void set_healthStatsVisible_4(bool value)
	{
		___healthStatsVisible_4 = value;
	}

	inline static int32_t get_offset_of_trafficStatsOn_5() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t1231606017, ___trafficStatsOn_5)); }
	inline bool get_trafficStatsOn_5() const { return ___trafficStatsOn_5; }
	inline bool* get_address_of_trafficStatsOn_5() { return &___trafficStatsOn_5; }
	inline void set_trafficStatsOn_5(bool value)
	{
		___trafficStatsOn_5 = value;
	}

	inline static int32_t get_offset_of_buttonsOn_6() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t1231606017, ___buttonsOn_6)); }
	inline bool get_buttonsOn_6() const { return ___buttonsOn_6; }
	inline bool* get_address_of_buttonsOn_6() { return &___buttonsOn_6; }
	inline void set_buttonsOn_6(bool value)
	{
		___buttonsOn_6 = value;
	}

	inline static int32_t get_offset_of_statsRect_7() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t1231606017, ___statsRect_7)); }
	inline Rect_t2360479859  get_statsRect_7() const { return ___statsRect_7; }
	inline Rect_t2360479859 * get_address_of_statsRect_7() { return &___statsRect_7; }
	inline void set_statsRect_7(Rect_t2360479859  value)
	{
		___statsRect_7 = value;
	}

	inline static int32_t get_offset_of_WindowId_8() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t1231606017, ___WindowId_8)); }
	inline int32_t get_WindowId_8() const { return ___WindowId_8; }
	inline int32_t* get_address_of_WindowId_8() { return &___WindowId_8; }
	inline void set_WindowId_8(int32_t value)
	{
		___WindowId_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONSTATSGUI_T1231606017_H
#ifndef PHOTONRIGIDBODYVIEW_T56173500_H
#define PHOTONRIGIDBODYVIEW_T56173500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonRigidbodyView
struct  PhotonRigidbodyView_t56173500  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PhotonRigidbodyView::m_SynchronizeVelocity
	bool ___m_SynchronizeVelocity_2;
	// System.Boolean PhotonRigidbodyView::m_SynchronizeAngularVelocity
	bool ___m_SynchronizeAngularVelocity_3;
	// UnityEngine.Rigidbody PhotonRigidbodyView::m_Body
	Rigidbody_t3916780224 * ___m_Body_4;

public:
	inline static int32_t get_offset_of_m_SynchronizeVelocity_2() { return static_cast<int32_t>(offsetof(PhotonRigidbodyView_t56173500, ___m_SynchronizeVelocity_2)); }
	inline bool get_m_SynchronizeVelocity_2() const { return ___m_SynchronizeVelocity_2; }
	inline bool* get_address_of_m_SynchronizeVelocity_2() { return &___m_SynchronizeVelocity_2; }
	inline void set_m_SynchronizeVelocity_2(bool value)
	{
		___m_SynchronizeVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizeAngularVelocity_3() { return static_cast<int32_t>(offsetof(PhotonRigidbodyView_t56173500, ___m_SynchronizeAngularVelocity_3)); }
	inline bool get_m_SynchronizeAngularVelocity_3() const { return ___m_SynchronizeAngularVelocity_3; }
	inline bool* get_address_of_m_SynchronizeAngularVelocity_3() { return &___m_SynchronizeAngularVelocity_3; }
	inline void set_m_SynchronizeAngularVelocity_3(bool value)
	{
		___m_SynchronizeAngularVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_Body_4() { return static_cast<int32_t>(offsetof(PhotonRigidbodyView_t56173500, ___m_Body_4)); }
	inline Rigidbody_t3916780224 * get_m_Body_4() const { return ___m_Body_4; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Body_4() { return &___m_Body_4; }
	inline void set_m_Body_4(Rigidbody_t3916780224 * value)
	{
		___m_Body_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Body_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONRIGIDBODYVIEW_T56173500_H
#ifndef NETWORKCULLINGHANDLER_T3621072727_H
#define NETWORKCULLINGHANDLER_T3621072727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkCullingHandler
struct  NetworkCullingHandler_t3621072727  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 NetworkCullingHandler::orderIndex
	int32_t ___orderIndex_2;
	// CullArea NetworkCullingHandler::cullArea
	CullArea_t3053759289 * ___cullArea_3;
	// System.Collections.Generic.List`1<System.Byte> NetworkCullingHandler::previousActiveCells
	List_1_t2606371118 * ___previousActiveCells_4;
	// System.Collections.Generic.List`1<System.Byte> NetworkCullingHandler::activeCells
	List_1_t2606371118 * ___activeCells_5;
	// PhotonView NetworkCullingHandler::pView
	PhotonView_t2207721820 * ___pView_6;
	// UnityEngine.Vector3 NetworkCullingHandler::lastPosition
	Vector3_t3722313464  ___lastPosition_7;
	// UnityEngine.Vector3 NetworkCullingHandler::currentPosition
	Vector3_t3722313464  ___currentPosition_8;

public:
	inline static int32_t get_offset_of_orderIndex_2() { return static_cast<int32_t>(offsetof(NetworkCullingHandler_t3621072727, ___orderIndex_2)); }
	inline int32_t get_orderIndex_2() const { return ___orderIndex_2; }
	inline int32_t* get_address_of_orderIndex_2() { return &___orderIndex_2; }
	inline void set_orderIndex_2(int32_t value)
	{
		___orderIndex_2 = value;
	}

	inline static int32_t get_offset_of_cullArea_3() { return static_cast<int32_t>(offsetof(NetworkCullingHandler_t3621072727, ___cullArea_3)); }
	inline CullArea_t3053759289 * get_cullArea_3() const { return ___cullArea_3; }
	inline CullArea_t3053759289 ** get_address_of_cullArea_3() { return &___cullArea_3; }
	inline void set_cullArea_3(CullArea_t3053759289 * value)
	{
		___cullArea_3 = value;
		Il2CppCodeGenWriteBarrier((&___cullArea_3), value);
	}

	inline static int32_t get_offset_of_previousActiveCells_4() { return static_cast<int32_t>(offsetof(NetworkCullingHandler_t3621072727, ___previousActiveCells_4)); }
	inline List_1_t2606371118 * get_previousActiveCells_4() const { return ___previousActiveCells_4; }
	inline List_1_t2606371118 ** get_address_of_previousActiveCells_4() { return &___previousActiveCells_4; }
	inline void set_previousActiveCells_4(List_1_t2606371118 * value)
	{
		___previousActiveCells_4 = value;
		Il2CppCodeGenWriteBarrier((&___previousActiveCells_4), value);
	}

	inline static int32_t get_offset_of_activeCells_5() { return static_cast<int32_t>(offsetof(NetworkCullingHandler_t3621072727, ___activeCells_5)); }
	inline List_1_t2606371118 * get_activeCells_5() const { return ___activeCells_5; }
	inline List_1_t2606371118 ** get_address_of_activeCells_5() { return &___activeCells_5; }
	inline void set_activeCells_5(List_1_t2606371118 * value)
	{
		___activeCells_5 = value;
		Il2CppCodeGenWriteBarrier((&___activeCells_5), value);
	}

	inline static int32_t get_offset_of_pView_6() { return static_cast<int32_t>(offsetof(NetworkCullingHandler_t3621072727, ___pView_6)); }
	inline PhotonView_t2207721820 * get_pView_6() const { return ___pView_6; }
	inline PhotonView_t2207721820 ** get_address_of_pView_6() { return &___pView_6; }
	inline void set_pView_6(PhotonView_t2207721820 * value)
	{
		___pView_6 = value;
		Il2CppCodeGenWriteBarrier((&___pView_6), value);
	}

	inline static int32_t get_offset_of_lastPosition_7() { return static_cast<int32_t>(offsetof(NetworkCullingHandler_t3621072727, ___lastPosition_7)); }
	inline Vector3_t3722313464  get_lastPosition_7() const { return ___lastPosition_7; }
	inline Vector3_t3722313464 * get_address_of_lastPosition_7() { return &___lastPosition_7; }
	inline void set_lastPosition_7(Vector3_t3722313464  value)
	{
		___lastPosition_7 = value;
	}

	inline static int32_t get_offset_of_currentPosition_8() { return static_cast<int32_t>(offsetof(NetworkCullingHandler_t3621072727, ___currentPosition_8)); }
	inline Vector3_t3722313464  get_currentPosition_8() const { return ___currentPosition_8; }
	inline Vector3_t3722313464 * get_address_of_currentPosition_8() { return &___currentPosition_8; }
	inline void set_currentPosition_8(Vector3_t3722313464  value)
	{
		___currentPosition_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCULLINGHANDLER_T3621072727_H
#ifndef MANUALPHOTONVIEWALLOCATOR_T3877483065_H
#define MANUALPHOTONVIEWALLOCATOR_T3877483065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManualPhotonViewAllocator
struct  ManualPhotonViewAllocator_t3877483065  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ManualPhotonViewAllocator::Prefab
	GameObject_t1113636619 * ___Prefab_2;

public:
	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(ManualPhotonViewAllocator_t3877483065, ___Prefab_2)); }
	inline GameObject_t1113636619 * get_Prefab_2() const { return ___Prefab_2; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(GameObject_t1113636619 * value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANUALPHOTONVIEWALLOCATOR_T3877483065_H
#ifndef INPUTTOEVENT_T2359295403_H
#define INPUTTOEVENT_T2359295403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputToEvent
struct  InputToEvent_t2359295403  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject InputToEvent::lastGo
	GameObject_t1113636619 * ___lastGo_2;
	// System.Boolean InputToEvent::DetectPointedAtGameObject
	bool ___DetectPointedAtGameObject_4;
	// UnityEngine.Vector2 InputToEvent::pressedPosition
	Vector2_t2156229523  ___pressedPosition_6;
	// UnityEngine.Vector2 InputToEvent::currentPos
	Vector2_t2156229523  ___currentPos_7;
	// System.Boolean InputToEvent::Dragging
	bool ___Dragging_8;
	// UnityEngine.Camera InputToEvent::m_Camera
	Camera_t4157153871 * ___m_Camera_9;

public:
	inline static int32_t get_offset_of_lastGo_2() { return static_cast<int32_t>(offsetof(InputToEvent_t2359295403, ___lastGo_2)); }
	inline GameObject_t1113636619 * get_lastGo_2() const { return ___lastGo_2; }
	inline GameObject_t1113636619 ** get_address_of_lastGo_2() { return &___lastGo_2; }
	inline void set_lastGo_2(GameObject_t1113636619 * value)
	{
		___lastGo_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastGo_2), value);
	}

	inline static int32_t get_offset_of_DetectPointedAtGameObject_4() { return static_cast<int32_t>(offsetof(InputToEvent_t2359295403, ___DetectPointedAtGameObject_4)); }
	inline bool get_DetectPointedAtGameObject_4() const { return ___DetectPointedAtGameObject_4; }
	inline bool* get_address_of_DetectPointedAtGameObject_4() { return &___DetectPointedAtGameObject_4; }
	inline void set_DetectPointedAtGameObject_4(bool value)
	{
		___DetectPointedAtGameObject_4 = value;
	}

	inline static int32_t get_offset_of_pressedPosition_6() { return static_cast<int32_t>(offsetof(InputToEvent_t2359295403, ___pressedPosition_6)); }
	inline Vector2_t2156229523  get_pressedPosition_6() const { return ___pressedPosition_6; }
	inline Vector2_t2156229523 * get_address_of_pressedPosition_6() { return &___pressedPosition_6; }
	inline void set_pressedPosition_6(Vector2_t2156229523  value)
	{
		___pressedPosition_6 = value;
	}

	inline static int32_t get_offset_of_currentPos_7() { return static_cast<int32_t>(offsetof(InputToEvent_t2359295403, ___currentPos_7)); }
	inline Vector2_t2156229523  get_currentPos_7() const { return ___currentPos_7; }
	inline Vector2_t2156229523 * get_address_of_currentPos_7() { return &___currentPos_7; }
	inline void set_currentPos_7(Vector2_t2156229523  value)
	{
		___currentPos_7 = value;
	}

	inline static int32_t get_offset_of_Dragging_8() { return static_cast<int32_t>(offsetof(InputToEvent_t2359295403, ___Dragging_8)); }
	inline bool get_Dragging_8() const { return ___Dragging_8; }
	inline bool* get_address_of_Dragging_8() { return &___Dragging_8; }
	inline void set_Dragging_8(bool value)
	{
		___Dragging_8 = value;
	}

	inline static int32_t get_offset_of_m_Camera_9() { return static_cast<int32_t>(offsetof(InputToEvent_t2359295403, ___m_Camera_9)); }
	inline Camera_t4157153871 * get_m_Camera_9() const { return ___m_Camera_9; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_9() { return &___m_Camera_9; }
	inline void set_m_Camera_9(Camera_t4157153871 * value)
	{
		___m_Camera_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_9), value);
	}
};

struct InputToEvent_t2359295403_StaticFields
{
public:
	// UnityEngine.Vector3 InputToEvent::inputHitPos
	Vector3_t3722313464  ___inputHitPos_3;
	// UnityEngine.GameObject InputToEvent::<goPointedAt>k__BackingField
	GameObject_t1113636619 * ___U3CgoPointedAtU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_inputHitPos_3() { return static_cast<int32_t>(offsetof(InputToEvent_t2359295403_StaticFields, ___inputHitPos_3)); }
	inline Vector3_t3722313464  get_inputHitPos_3() const { return ___inputHitPos_3; }
	inline Vector3_t3722313464 * get_address_of_inputHitPos_3() { return &___inputHitPos_3; }
	inline void set_inputHitPos_3(Vector3_t3722313464  value)
	{
		___inputHitPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CgoPointedAtU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InputToEvent_t2359295403_StaticFields, ___U3CgoPointedAtU3Ek__BackingField_5)); }
	inline GameObject_t1113636619 * get_U3CgoPointedAtU3Ek__BackingField_5() const { return ___U3CgoPointedAtU3Ek__BackingField_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CgoPointedAtU3Ek__BackingField_5() { return &___U3CgoPointedAtU3Ek__BackingField_5; }
	inline void set_U3CgoPointedAtU3Ek__BackingField_5(GameObject_t1113636619 * value)
	{
		___U3CgoPointedAtU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgoPointedAtU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTOEVENT_T2359295403_H
#ifndef CULLAREA_T3053759289_H
#define CULLAREA_T3053759289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CullArea
struct  CullArea_t3053759289  : public MonoBehaviour_t3962482529
{
public:
	// System.Byte CullArea::FIRST_GROUP_ID
	uint8_t ___FIRST_GROUP_ID_4;
	// System.Int32[] CullArea::SUBDIVISION_FIRST_LEVEL_ORDER
	Int32U5BU5D_t385246372* ___SUBDIVISION_FIRST_LEVEL_ORDER_5;
	// System.Int32[] CullArea::SUBDIVISION_SECOND_LEVEL_ORDER
	Int32U5BU5D_t385246372* ___SUBDIVISION_SECOND_LEVEL_ORDER_6;
	// System.Int32[] CullArea::SUBDIVISION_THIRD_LEVEL_ORDER
	Int32U5BU5D_t385246372* ___SUBDIVISION_THIRD_LEVEL_ORDER_7;
	// UnityEngine.Vector2 CullArea::Center
	Vector2_t2156229523  ___Center_8;
	// UnityEngine.Vector2 CullArea::Size
	Vector2_t2156229523  ___Size_9;
	// UnityEngine.Vector2[] CullArea::Subdivisions
	Vector2U5BU5D_t1457185986* ___Subdivisions_10;
	// System.Int32 CullArea::NumberOfSubdivisions
	int32_t ___NumberOfSubdivisions_11;
	// System.Int32 CullArea::<CellCount>k__BackingField
	int32_t ___U3CCellCountU3Ek__BackingField_12;
	// CellTree CullArea::<CellTree>k__BackingField
	CellTree_t3785927468 * ___U3CCellTreeU3Ek__BackingField_13;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> CullArea::<Map>k__BackingField
	Dictionary_2_t2349950 * ___U3CMapU3Ek__BackingField_14;
	// System.Boolean CullArea::YIsUpAxis
	bool ___YIsUpAxis_15;
	// System.Boolean CullArea::RecreateCellHierarchy
	bool ___RecreateCellHierarchy_16;
	// System.Byte CullArea::idCounter
	uint8_t ___idCounter_17;

public:
	inline static int32_t get_offset_of_FIRST_GROUP_ID_4() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___FIRST_GROUP_ID_4)); }
	inline uint8_t get_FIRST_GROUP_ID_4() const { return ___FIRST_GROUP_ID_4; }
	inline uint8_t* get_address_of_FIRST_GROUP_ID_4() { return &___FIRST_GROUP_ID_4; }
	inline void set_FIRST_GROUP_ID_4(uint8_t value)
	{
		___FIRST_GROUP_ID_4 = value;
	}

	inline static int32_t get_offset_of_SUBDIVISION_FIRST_LEVEL_ORDER_5() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___SUBDIVISION_FIRST_LEVEL_ORDER_5)); }
	inline Int32U5BU5D_t385246372* get_SUBDIVISION_FIRST_LEVEL_ORDER_5() const { return ___SUBDIVISION_FIRST_LEVEL_ORDER_5; }
	inline Int32U5BU5D_t385246372** get_address_of_SUBDIVISION_FIRST_LEVEL_ORDER_5() { return &___SUBDIVISION_FIRST_LEVEL_ORDER_5; }
	inline void set_SUBDIVISION_FIRST_LEVEL_ORDER_5(Int32U5BU5D_t385246372* value)
	{
		___SUBDIVISION_FIRST_LEVEL_ORDER_5 = value;
		Il2CppCodeGenWriteBarrier((&___SUBDIVISION_FIRST_LEVEL_ORDER_5), value);
	}

	inline static int32_t get_offset_of_SUBDIVISION_SECOND_LEVEL_ORDER_6() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___SUBDIVISION_SECOND_LEVEL_ORDER_6)); }
	inline Int32U5BU5D_t385246372* get_SUBDIVISION_SECOND_LEVEL_ORDER_6() const { return ___SUBDIVISION_SECOND_LEVEL_ORDER_6; }
	inline Int32U5BU5D_t385246372** get_address_of_SUBDIVISION_SECOND_LEVEL_ORDER_6() { return &___SUBDIVISION_SECOND_LEVEL_ORDER_6; }
	inline void set_SUBDIVISION_SECOND_LEVEL_ORDER_6(Int32U5BU5D_t385246372* value)
	{
		___SUBDIVISION_SECOND_LEVEL_ORDER_6 = value;
		Il2CppCodeGenWriteBarrier((&___SUBDIVISION_SECOND_LEVEL_ORDER_6), value);
	}

	inline static int32_t get_offset_of_SUBDIVISION_THIRD_LEVEL_ORDER_7() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___SUBDIVISION_THIRD_LEVEL_ORDER_7)); }
	inline Int32U5BU5D_t385246372* get_SUBDIVISION_THIRD_LEVEL_ORDER_7() const { return ___SUBDIVISION_THIRD_LEVEL_ORDER_7; }
	inline Int32U5BU5D_t385246372** get_address_of_SUBDIVISION_THIRD_LEVEL_ORDER_7() { return &___SUBDIVISION_THIRD_LEVEL_ORDER_7; }
	inline void set_SUBDIVISION_THIRD_LEVEL_ORDER_7(Int32U5BU5D_t385246372* value)
	{
		___SUBDIVISION_THIRD_LEVEL_ORDER_7 = value;
		Il2CppCodeGenWriteBarrier((&___SUBDIVISION_THIRD_LEVEL_ORDER_7), value);
	}

	inline static int32_t get_offset_of_Center_8() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___Center_8)); }
	inline Vector2_t2156229523  get_Center_8() const { return ___Center_8; }
	inline Vector2_t2156229523 * get_address_of_Center_8() { return &___Center_8; }
	inline void set_Center_8(Vector2_t2156229523  value)
	{
		___Center_8 = value;
	}

	inline static int32_t get_offset_of_Size_9() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___Size_9)); }
	inline Vector2_t2156229523  get_Size_9() const { return ___Size_9; }
	inline Vector2_t2156229523 * get_address_of_Size_9() { return &___Size_9; }
	inline void set_Size_9(Vector2_t2156229523  value)
	{
		___Size_9 = value;
	}

	inline static int32_t get_offset_of_Subdivisions_10() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___Subdivisions_10)); }
	inline Vector2U5BU5D_t1457185986* get_Subdivisions_10() const { return ___Subdivisions_10; }
	inline Vector2U5BU5D_t1457185986** get_address_of_Subdivisions_10() { return &___Subdivisions_10; }
	inline void set_Subdivisions_10(Vector2U5BU5D_t1457185986* value)
	{
		___Subdivisions_10 = value;
		Il2CppCodeGenWriteBarrier((&___Subdivisions_10), value);
	}

	inline static int32_t get_offset_of_NumberOfSubdivisions_11() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___NumberOfSubdivisions_11)); }
	inline int32_t get_NumberOfSubdivisions_11() const { return ___NumberOfSubdivisions_11; }
	inline int32_t* get_address_of_NumberOfSubdivisions_11() { return &___NumberOfSubdivisions_11; }
	inline void set_NumberOfSubdivisions_11(int32_t value)
	{
		___NumberOfSubdivisions_11 = value;
	}

	inline static int32_t get_offset_of_U3CCellCountU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___U3CCellCountU3Ek__BackingField_12)); }
	inline int32_t get_U3CCellCountU3Ek__BackingField_12() const { return ___U3CCellCountU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CCellCountU3Ek__BackingField_12() { return &___U3CCellCountU3Ek__BackingField_12; }
	inline void set_U3CCellCountU3Ek__BackingField_12(int32_t value)
	{
		___U3CCellCountU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CCellTreeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___U3CCellTreeU3Ek__BackingField_13)); }
	inline CellTree_t3785927468 * get_U3CCellTreeU3Ek__BackingField_13() const { return ___U3CCellTreeU3Ek__BackingField_13; }
	inline CellTree_t3785927468 ** get_address_of_U3CCellTreeU3Ek__BackingField_13() { return &___U3CCellTreeU3Ek__BackingField_13; }
	inline void set_U3CCellTreeU3Ek__BackingField_13(CellTree_t3785927468 * value)
	{
		___U3CCellTreeU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCellTreeU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CMapU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___U3CMapU3Ek__BackingField_14)); }
	inline Dictionary_2_t2349950 * get_U3CMapU3Ek__BackingField_14() const { return ___U3CMapU3Ek__BackingField_14; }
	inline Dictionary_2_t2349950 ** get_address_of_U3CMapU3Ek__BackingField_14() { return &___U3CMapU3Ek__BackingField_14; }
	inline void set_U3CMapU3Ek__BackingField_14(Dictionary_2_t2349950 * value)
	{
		___U3CMapU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMapU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_YIsUpAxis_15() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___YIsUpAxis_15)); }
	inline bool get_YIsUpAxis_15() const { return ___YIsUpAxis_15; }
	inline bool* get_address_of_YIsUpAxis_15() { return &___YIsUpAxis_15; }
	inline void set_YIsUpAxis_15(bool value)
	{
		___YIsUpAxis_15 = value;
	}

	inline static int32_t get_offset_of_RecreateCellHierarchy_16() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___RecreateCellHierarchy_16)); }
	inline bool get_RecreateCellHierarchy_16() const { return ___RecreateCellHierarchy_16; }
	inline bool* get_address_of_RecreateCellHierarchy_16() { return &___RecreateCellHierarchy_16; }
	inline void set_RecreateCellHierarchy_16(bool value)
	{
		___RecreateCellHierarchy_16 = value;
	}

	inline static int32_t get_offset_of_idCounter_17() { return static_cast<int32_t>(offsetof(CullArea_t3053759289, ___idCounter_17)); }
	inline uint8_t get_idCounter_17() const { return ___idCounter_17; }
	inline uint8_t* get_address_of_idCounter_17() { return &___idCounter_17; }
	inline void set_idCounter_17(uint8_t value)
	{
		___idCounter_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULLAREA_T3053759289_H
#ifndef ONCLICKINSTANTIATE_T646526299_H
#define ONCLICKINSTANTIATE_T646526299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickInstantiate
struct  OnClickInstantiate_t646526299  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject OnClickInstantiate::Prefab
	GameObject_t1113636619 * ___Prefab_2;
	// System.Int32 OnClickInstantiate::InstantiateType
	int32_t ___InstantiateType_3;
	// System.String[] OnClickInstantiate::InstantiateTypeNames
	StringU5BU5D_t1281789340* ___InstantiateTypeNames_4;
	// System.Boolean OnClickInstantiate::showGui
	bool ___showGui_5;

public:
	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(OnClickInstantiate_t646526299, ___Prefab_2)); }
	inline GameObject_t1113636619 * get_Prefab_2() const { return ___Prefab_2; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(GameObject_t1113636619 * value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_InstantiateType_3() { return static_cast<int32_t>(offsetof(OnClickInstantiate_t646526299, ___InstantiateType_3)); }
	inline int32_t get_InstantiateType_3() const { return ___InstantiateType_3; }
	inline int32_t* get_address_of_InstantiateType_3() { return &___InstantiateType_3; }
	inline void set_InstantiateType_3(int32_t value)
	{
		___InstantiateType_3 = value;
	}

	inline static int32_t get_offset_of_InstantiateTypeNames_4() { return static_cast<int32_t>(offsetof(OnClickInstantiate_t646526299, ___InstantiateTypeNames_4)); }
	inline StringU5BU5D_t1281789340* get_InstantiateTypeNames_4() const { return ___InstantiateTypeNames_4; }
	inline StringU5BU5D_t1281789340** get_address_of_InstantiateTypeNames_4() { return &___InstantiateTypeNames_4; }
	inline void set_InstantiateTypeNames_4(StringU5BU5D_t1281789340* value)
	{
		___InstantiateTypeNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___InstantiateTypeNames_4), value);
	}

	inline static int32_t get_offset_of_showGui_5() { return static_cast<int32_t>(offsetof(OnClickInstantiate_t646526299, ___showGui_5)); }
	inline bool get_showGui_5() const { return ___showGui_5; }
	inline bool* get_address_of_showGui_5() { return &___showGui_5; }
	inline void set_showGui_5(bool value)
	{
		___showGui_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKINSTANTIATE_T646526299_H
#ifndef PHOTONTRANSFORMVIEW_T372465615_H
#define PHOTONTRANSFORMVIEW_T372465615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTransformView
struct  PhotonTransformView_t372465615  : public MonoBehaviour_t3962482529
{
public:
	// PhotonTransformViewPositionModel PhotonTransformView::m_PositionModel
	PhotonTransformViewPositionModel_t2500134640 * ___m_PositionModel_2;
	// PhotonTransformViewRotationModel PhotonTransformView::m_RotationModel
	PhotonTransformViewRotationModel_t1080899250 * ___m_RotationModel_3;
	// PhotonTransformViewScaleModel PhotonTransformView::m_ScaleModel
	PhotonTransformViewScaleModel_t763003770 * ___m_ScaleModel_4;
	// PhotonTransformViewPositionControl PhotonTransformView::m_PositionControl
	PhotonTransformViewPositionControl_t619346209 * ___m_PositionControl_5;
	// PhotonTransformViewRotationControl PhotonTransformView::m_RotationControl
	PhotonTransformViewRotationControl_t2679094986 * ___m_RotationControl_6;
	// PhotonTransformViewScaleControl PhotonTransformView::m_ScaleControl
	PhotonTransformViewScaleControl_t2271393751 * ___m_ScaleControl_7;
	// PhotonView PhotonTransformView::m_PhotonView
	PhotonView_t2207721820 * ___m_PhotonView_8;
	// System.Boolean PhotonTransformView::m_ReceivedNetworkUpdate
	bool ___m_ReceivedNetworkUpdate_9;
	// System.Boolean PhotonTransformView::m_firstTake
	bool ___m_firstTake_10;

public:
	inline static int32_t get_offset_of_m_PositionModel_2() { return static_cast<int32_t>(offsetof(PhotonTransformView_t372465615, ___m_PositionModel_2)); }
	inline PhotonTransformViewPositionModel_t2500134640 * get_m_PositionModel_2() const { return ___m_PositionModel_2; }
	inline PhotonTransformViewPositionModel_t2500134640 ** get_address_of_m_PositionModel_2() { return &___m_PositionModel_2; }
	inline void set_m_PositionModel_2(PhotonTransformViewPositionModel_t2500134640 * value)
	{
		___m_PositionModel_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PositionModel_2), value);
	}

	inline static int32_t get_offset_of_m_RotationModel_3() { return static_cast<int32_t>(offsetof(PhotonTransformView_t372465615, ___m_RotationModel_3)); }
	inline PhotonTransformViewRotationModel_t1080899250 * get_m_RotationModel_3() const { return ___m_RotationModel_3; }
	inline PhotonTransformViewRotationModel_t1080899250 ** get_address_of_m_RotationModel_3() { return &___m_RotationModel_3; }
	inline void set_m_RotationModel_3(PhotonTransformViewRotationModel_t1080899250 * value)
	{
		___m_RotationModel_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_RotationModel_3), value);
	}

	inline static int32_t get_offset_of_m_ScaleModel_4() { return static_cast<int32_t>(offsetof(PhotonTransformView_t372465615, ___m_ScaleModel_4)); }
	inline PhotonTransformViewScaleModel_t763003770 * get_m_ScaleModel_4() const { return ___m_ScaleModel_4; }
	inline PhotonTransformViewScaleModel_t763003770 ** get_address_of_m_ScaleModel_4() { return &___m_ScaleModel_4; }
	inline void set_m_ScaleModel_4(PhotonTransformViewScaleModel_t763003770 * value)
	{
		___m_ScaleModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScaleModel_4), value);
	}

	inline static int32_t get_offset_of_m_PositionControl_5() { return static_cast<int32_t>(offsetof(PhotonTransformView_t372465615, ___m_PositionControl_5)); }
	inline PhotonTransformViewPositionControl_t619346209 * get_m_PositionControl_5() const { return ___m_PositionControl_5; }
	inline PhotonTransformViewPositionControl_t619346209 ** get_address_of_m_PositionControl_5() { return &___m_PositionControl_5; }
	inline void set_m_PositionControl_5(PhotonTransformViewPositionControl_t619346209 * value)
	{
		___m_PositionControl_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PositionControl_5), value);
	}

	inline static int32_t get_offset_of_m_RotationControl_6() { return static_cast<int32_t>(offsetof(PhotonTransformView_t372465615, ___m_RotationControl_6)); }
	inline PhotonTransformViewRotationControl_t2679094986 * get_m_RotationControl_6() const { return ___m_RotationControl_6; }
	inline PhotonTransformViewRotationControl_t2679094986 ** get_address_of_m_RotationControl_6() { return &___m_RotationControl_6; }
	inline void set_m_RotationControl_6(PhotonTransformViewRotationControl_t2679094986 * value)
	{
		___m_RotationControl_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_RotationControl_6), value);
	}

	inline static int32_t get_offset_of_m_ScaleControl_7() { return static_cast<int32_t>(offsetof(PhotonTransformView_t372465615, ___m_ScaleControl_7)); }
	inline PhotonTransformViewScaleControl_t2271393751 * get_m_ScaleControl_7() const { return ___m_ScaleControl_7; }
	inline PhotonTransformViewScaleControl_t2271393751 ** get_address_of_m_ScaleControl_7() { return &___m_ScaleControl_7; }
	inline void set_m_ScaleControl_7(PhotonTransformViewScaleControl_t2271393751 * value)
	{
		___m_ScaleControl_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScaleControl_7), value);
	}

	inline static int32_t get_offset_of_m_PhotonView_8() { return static_cast<int32_t>(offsetof(PhotonTransformView_t372465615, ___m_PhotonView_8)); }
	inline PhotonView_t2207721820 * get_m_PhotonView_8() const { return ___m_PhotonView_8; }
	inline PhotonView_t2207721820 ** get_address_of_m_PhotonView_8() { return &___m_PhotonView_8; }
	inline void set_m_PhotonView_8(PhotonView_t2207721820 * value)
	{
		___m_PhotonView_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_PhotonView_8), value);
	}

	inline static int32_t get_offset_of_m_ReceivedNetworkUpdate_9() { return static_cast<int32_t>(offsetof(PhotonTransformView_t372465615, ___m_ReceivedNetworkUpdate_9)); }
	inline bool get_m_ReceivedNetworkUpdate_9() const { return ___m_ReceivedNetworkUpdate_9; }
	inline bool* get_address_of_m_ReceivedNetworkUpdate_9() { return &___m_ReceivedNetworkUpdate_9; }
	inline void set_m_ReceivedNetworkUpdate_9(bool value)
	{
		___m_ReceivedNetworkUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_firstTake_10() { return static_cast<int32_t>(offsetof(PhotonTransformView_t372465615, ___m_firstTake_10)); }
	inline bool get_m_firstTake_10() const { return ___m_firstTake_10; }
	inline bool* get_address_of_m_firstTake_10() { return &___m_firstTake_10; }
	inline void set_m_firstTake_10(bool value)
	{
		___m_firstTake_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONTRANSFORMVIEW_T372465615_H
#ifndef ONCLICKDESTROY_T2392776543_H
#define ONCLICKDESTROY_T2392776543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickDestroy
struct  OnClickDestroy_t2392776543  : public MonoBehaviour_t3225183318
{
public:
	// System.Boolean OnClickDestroy::DestroyByRpc
	bool ___DestroyByRpc_3;

public:
	inline static int32_t get_offset_of_DestroyByRpc_3() { return static_cast<int32_t>(offsetof(OnClickDestroy_t2392776543, ___DestroyByRpc_3)); }
	inline bool get_DestroyByRpc_3() const { return ___DestroyByRpc_3; }
	inline bool* get_address_of_DestroyByRpc_3() { return &___DestroyByRpc_3; }
	inline void set_DestroyByRpc_3(bool value)
	{
		___DestroyByRpc_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKDESTROY_T2392776543_H
#ifndef PHOTONVIEW_T2207721820_H
#define PHOTONVIEW_T2207721820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonView
struct  PhotonView_t2207721820  : public MonoBehaviour_t3225183318
{
public:
	// System.Int32 PhotonView::ownerId
	int32_t ___ownerId_3;
	// System.Byte PhotonView::group
	uint8_t ___group_4;
	// System.Boolean PhotonView::mixedModeIsReliable
	bool ___mixedModeIsReliable_5;
	// System.Boolean PhotonView::OwnerShipWasTransfered
	bool ___OwnerShipWasTransfered_6;
	// System.Int32 PhotonView::prefixBackup
	int32_t ___prefixBackup_7;
	// System.Object[] PhotonView::instantiationDataField
	ObjectU5BU5D_t2843939325* ___instantiationDataField_8;
	// System.Object[] PhotonView::lastOnSerializeDataSent
	ObjectU5BU5D_t2843939325* ___lastOnSerializeDataSent_9;
	// System.Object[] PhotonView::lastOnSerializeDataReceived
	ObjectU5BU5D_t2843939325* ___lastOnSerializeDataReceived_10;
	// ViewSynchronization PhotonView::synchronization
	int32_t ___synchronization_11;
	// OnSerializeTransform PhotonView::onSerializeTransformOption
	int32_t ___onSerializeTransformOption_12;
	// OnSerializeRigidBody PhotonView::onSerializeRigidBodyOption
	int32_t ___onSerializeRigidBodyOption_13;
	// OwnershipOption PhotonView::ownershipTransfer
	int32_t ___ownershipTransfer_14;
	// System.Collections.Generic.List`1<UnityEngine.Component> PhotonView::ObservedComponents
	List_1_t3395709193 * ___ObservedComponents_15;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Component,System.Reflection.MethodInfo> PhotonView::m_OnSerializeMethodInfos
	Dictionary_2_t3676033689 * ___m_OnSerializeMethodInfos_16;
	// System.Int32 PhotonView::viewIdField
	int32_t ___viewIdField_17;
	// System.Int32 PhotonView::instantiationId
	int32_t ___instantiationId_18;
	// System.Int32 PhotonView::currentMasterID
	int32_t ___currentMasterID_19;
	// System.Boolean PhotonView::didAwake
	bool ___didAwake_20;
	// System.Boolean PhotonView::isRuntimeInstantiated
	bool ___isRuntimeInstantiated_21;
	// System.Boolean PhotonView::removedFromLocalViewList
	bool ___removedFromLocalViewList_22;
	// UnityEngine.MonoBehaviour[] PhotonView::RpcMonoBehaviours
	MonoBehaviourU5BU5D_t2007329276* ___RpcMonoBehaviours_23;
	// System.Reflection.MethodInfo PhotonView::OnSerializeMethodInfo
	MethodInfo_t * ___OnSerializeMethodInfo_24;
	// System.Boolean PhotonView::failedToFindOnSerialize
	bool ___failedToFindOnSerialize_25;

public:
	inline static int32_t get_offset_of_ownerId_3() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___ownerId_3)); }
	inline int32_t get_ownerId_3() const { return ___ownerId_3; }
	inline int32_t* get_address_of_ownerId_3() { return &___ownerId_3; }
	inline void set_ownerId_3(int32_t value)
	{
		___ownerId_3 = value;
	}

	inline static int32_t get_offset_of_group_4() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___group_4)); }
	inline uint8_t get_group_4() const { return ___group_4; }
	inline uint8_t* get_address_of_group_4() { return &___group_4; }
	inline void set_group_4(uint8_t value)
	{
		___group_4 = value;
	}

	inline static int32_t get_offset_of_mixedModeIsReliable_5() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___mixedModeIsReliable_5)); }
	inline bool get_mixedModeIsReliable_5() const { return ___mixedModeIsReliable_5; }
	inline bool* get_address_of_mixedModeIsReliable_5() { return &___mixedModeIsReliable_5; }
	inline void set_mixedModeIsReliable_5(bool value)
	{
		___mixedModeIsReliable_5 = value;
	}

	inline static int32_t get_offset_of_OwnerShipWasTransfered_6() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___OwnerShipWasTransfered_6)); }
	inline bool get_OwnerShipWasTransfered_6() const { return ___OwnerShipWasTransfered_6; }
	inline bool* get_address_of_OwnerShipWasTransfered_6() { return &___OwnerShipWasTransfered_6; }
	inline void set_OwnerShipWasTransfered_6(bool value)
	{
		___OwnerShipWasTransfered_6 = value;
	}

	inline static int32_t get_offset_of_prefixBackup_7() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___prefixBackup_7)); }
	inline int32_t get_prefixBackup_7() const { return ___prefixBackup_7; }
	inline int32_t* get_address_of_prefixBackup_7() { return &___prefixBackup_7; }
	inline void set_prefixBackup_7(int32_t value)
	{
		___prefixBackup_7 = value;
	}

	inline static int32_t get_offset_of_instantiationDataField_8() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___instantiationDataField_8)); }
	inline ObjectU5BU5D_t2843939325* get_instantiationDataField_8() const { return ___instantiationDataField_8; }
	inline ObjectU5BU5D_t2843939325** get_address_of_instantiationDataField_8() { return &___instantiationDataField_8; }
	inline void set_instantiationDataField_8(ObjectU5BU5D_t2843939325* value)
	{
		___instantiationDataField_8 = value;
		Il2CppCodeGenWriteBarrier((&___instantiationDataField_8), value);
	}

	inline static int32_t get_offset_of_lastOnSerializeDataSent_9() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___lastOnSerializeDataSent_9)); }
	inline ObjectU5BU5D_t2843939325* get_lastOnSerializeDataSent_9() const { return ___lastOnSerializeDataSent_9; }
	inline ObjectU5BU5D_t2843939325** get_address_of_lastOnSerializeDataSent_9() { return &___lastOnSerializeDataSent_9; }
	inline void set_lastOnSerializeDataSent_9(ObjectU5BU5D_t2843939325* value)
	{
		___lastOnSerializeDataSent_9 = value;
		Il2CppCodeGenWriteBarrier((&___lastOnSerializeDataSent_9), value);
	}

	inline static int32_t get_offset_of_lastOnSerializeDataReceived_10() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___lastOnSerializeDataReceived_10)); }
	inline ObjectU5BU5D_t2843939325* get_lastOnSerializeDataReceived_10() const { return ___lastOnSerializeDataReceived_10; }
	inline ObjectU5BU5D_t2843939325** get_address_of_lastOnSerializeDataReceived_10() { return &___lastOnSerializeDataReceived_10; }
	inline void set_lastOnSerializeDataReceived_10(ObjectU5BU5D_t2843939325* value)
	{
		___lastOnSerializeDataReceived_10 = value;
		Il2CppCodeGenWriteBarrier((&___lastOnSerializeDataReceived_10), value);
	}

	inline static int32_t get_offset_of_synchronization_11() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___synchronization_11)); }
	inline int32_t get_synchronization_11() const { return ___synchronization_11; }
	inline int32_t* get_address_of_synchronization_11() { return &___synchronization_11; }
	inline void set_synchronization_11(int32_t value)
	{
		___synchronization_11 = value;
	}

	inline static int32_t get_offset_of_onSerializeTransformOption_12() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___onSerializeTransformOption_12)); }
	inline int32_t get_onSerializeTransformOption_12() const { return ___onSerializeTransformOption_12; }
	inline int32_t* get_address_of_onSerializeTransformOption_12() { return &___onSerializeTransformOption_12; }
	inline void set_onSerializeTransformOption_12(int32_t value)
	{
		___onSerializeTransformOption_12 = value;
	}

	inline static int32_t get_offset_of_onSerializeRigidBodyOption_13() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___onSerializeRigidBodyOption_13)); }
	inline int32_t get_onSerializeRigidBodyOption_13() const { return ___onSerializeRigidBodyOption_13; }
	inline int32_t* get_address_of_onSerializeRigidBodyOption_13() { return &___onSerializeRigidBodyOption_13; }
	inline void set_onSerializeRigidBodyOption_13(int32_t value)
	{
		___onSerializeRigidBodyOption_13 = value;
	}

	inline static int32_t get_offset_of_ownershipTransfer_14() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___ownershipTransfer_14)); }
	inline int32_t get_ownershipTransfer_14() const { return ___ownershipTransfer_14; }
	inline int32_t* get_address_of_ownershipTransfer_14() { return &___ownershipTransfer_14; }
	inline void set_ownershipTransfer_14(int32_t value)
	{
		___ownershipTransfer_14 = value;
	}

	inline static int32_t get_offset_of_ObservedComponents_15() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___ObservedComponents_15)); }
	inline List_1_t3395709193 * get_ObservedComponents_15() const { return ___ObservedComponents_15; }
	inline List_1_t3395709193 ** get_address_of_ObservedComponents_15() { return &___ObservedComponents_15; }
	inline void set_ObservedComponents_15(List_1_t3395709193 * value)
	{
		___ObservedComponents_15 = value;
		Il2CppCodeGenWriteBarrier((&___ObservedComponents_15), value);
	}

	inline static int32_t get_offset_of_m_OnSerializeMethodInfos_16() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___m_OnSerializeMethodInfos_16)); }
	inline Dictionary_2_t3676033689 * get_m_OnSerializeMethodInfos_16() const { return ___m_OnSerializeMethodInfos_16; }
	inline Dictionary_2_t3676033689 ** get_address_of_m_OnSerializeMethodInfos_16() { return &___m_OnSerializeMethodInfos_16; }
	inline void set_m_OnSerializeMethodInfos_16(Dictionary_2_t3676033689 * value)
	{
		___m_OnSerializeMethodInfos_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSerializeMethodInfos_16), value);
	}

	inline static int32_t get_offset_of_viewIdField_17() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___viewIdField_17)); }
	inline int32_t get_viewIdField_17() const { return ___viewIdField_17; }
	inline int32_t* get_address_of_viewIdField_17() { return &___viewIdField_17; }
	inline void set_viewIdField_17(int32_t value)
	{
		___viewIdField_17 = value;
	}

	inline static int32_t get_offset_of_instantiationId_18() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___instantiationId_18)); }
	inline int32_t get_instantiationId_18() const { return ___instantiationId_18; }
	inline int32_t* get_address_of_instantiationId_18() { return &___instantiationId_18; }
	inline void set_instantiationId_18(int32_t value)
	{
		___instantiationId_18 = value;
	}

	inline static int32_t get_offset_of_currentMasterID_19() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___currentMasterID_19)); }
	inline int32_t get_currentMasterID_19() const { return ___currentMasterID_19; }
	inline int32_t* get_address_of_currentMasterID_19() { return &___currentMasterID_19; }
	inline void set_currentMasterID_19(int32_t value)
	{
		___currentMasterID_19 = value;
	}

	inline static int32_t get_offset_of_didAwake_20() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___didAwake_20)); }
	inline bool get_didAwake_20() const { return ___didAwake_20; }
	inline bool* get_address_of_didAwake_20() { return &___didAwake_20; }
	inline void set_didAwake_20(bool value)
	{
		___didAwake_20 = value;
	}

	inline static int32_t get_offset_of_isRuntimeInstantiated_21() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___isRuntimeInstantiated_21)); }
	inline bool get_isRuntimeInstantiated_21() const { return ___isRuntimeInstantiated_21; }
	inline bool* get_address_of_isRuntimeInstantiated_21() { return &___isRuntimeInstantiated_21; }
	inline void set_isRuntimeInstantiated_21(bool value)
	{
		___isRuntimeInstantiated_21 = value;
	}

	inline static int32_t get_offset_of_removedFromLocalViewList_22() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___removedFromLocalViewList_22)); }
	inline bool get_removedFromLocalViewList_22() const { return ___removedFromLocalViewList_22; }
	inline bool* get_address_of_removedFromLocalViewList_22() { return &___removedFromLocalViewList_22; }
	inline void set_removedFromLocalViewList_22(bool value)
	{
		___removedFromLocalViewList_22 = value;
	}

	inline static int32_t get_offset_of_RpcMonoBehaviours_23() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___RpcMonoBehaviours_23)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_RpcMonoBehaviours_23() const { return ___RpcMonoBehaviours_23; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_RpcMonoBehaviours_23() { return &___RpcMonoBehaviours_23; }
	inline void set_RpcMonoBehaviours_23(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___RpcMonoBehaviours_23 = value;
		Il2CppCodeGenWriteBarrier((&___RpcMonoBehaviours_23), value);
	}

	inline static int32_t get_offset_of_OnSerializeMethodInfo_24() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___OnSerializeMethodInfo_24)); }
	inline MethodInfo_t * get_OnSerializeMethodInfo_24() const { return ___OnSerializeMethodInfo_24; }
	inline MethodInfo_t ** get_address_of_OnSerializeMethodInfo_24() { return &___OnSerializeMethodInfo_24; }
	inline void set_OnSerializeMethodInfo_24(MethodInfo_t * value)
	{
		___OnSerializeMethodInfo_24 = value;
		Il2CppCodeGenWriteBarrier((&___OnSerializeMethodInfo_24), value);
	}

	inline static int32_t get_offset_of_failedToFindOnSerialize_25() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___failedToFindOnSerialize_25)); }
	inline bool get_failedToFindOnSerialize_25() const { return ___failedToFindOnSerialize_25; }
	inline bool* get_address_of_failedToFindOnSerialize_25() { return &___failedToFindOnSerialize_25; }
	inline void set_failedToFindOnSerialize_25(bool value)
	{
		___failedToFindOnSerialize_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONVIEW_T2207721820_H
#ifndef MOVEBYKEYS_T2244393468_H
#define MOVEBYKEYS_T2244393468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveByKeys
struct  MoveByKeys_t2244393468  : public MonoBehaviour_t3225183318
{
public:
	// System.Single MoveByKeys::Speed
	float ___Speed_3;
	// System.Single MoveByKeys::JumpForce
	float ___JumpForce_4;
	// System.Single MoveByKeys::JumpTimeout
	float ___JumpTimeout_5;
	// System.Boolean MoveByKeys::isSprite
	bool ___isSprite_6;
	// System.Single MoveByKeys::jumpingTime
	float ___jumpingTime_7;
	// UnityEngine.Rigidbody MoveByKeys::body
	Rigidbody_t3916780224 * ___body_8;
	// UnityEngine.Rigidbody2D MoveByKeys::body2d
	Rigidbody2D_t939494601 * ___body2d_9;

public:
	inline static int32_t get_offset_of_Speed_3() { return static_cast<int32_t>(offsetof(MoveByKeys_t2244393468, ___Speed_3)); }
	inline float get_Speed_3() const { return ___Speed_3; }
	inline float* get_address_of_Speed_3() { return &___Speed_3; }
	inline void set_Speed_3(float value)
	{
		___Speed_3 = value;
	}

	inline static int32_t get_offset_of_JumpForce_4() { return static_cast<int32_t>(offsetof(MoveByKeys_t2244393468, ___JumpForce_4)); }
	inline float get_JumpForce_4() const { return ___JumpForce_4; }
	inline float* get_address_of_JumpForce_4() { return &___JumpForce_4; }
	inline void set_JumpForce_4(float value)
	{
		___JumpForce_4 = value;
	}

	inline static int32_t get_offset_of_JumpTimeout_5() { return static_cast<int32_t>(offsetof(MoveByKeys_t2244393468, ___JumpTimeout_5)); }
	inline float get_JumpTimeout_5() const { return ___JumpTimeout_5; }
	inline float* get_address_of_JumpTimeout_5() { return &___JumpTimeout_5; }
	inline void set_JumpTimeout_5(float value)
	{
		___JumpTimeout_5 = value;
	}

	inline static int32_t get_offset_of_isSprite_6() { return static_cast<int32_t>(offsetof(MoveByKeys_t2244393468, ___isSprite_6)); }
	inline bool get_isSprite_6() const { return ___isSprite_6; }
	inline bool* get_address_of_isSprite_6() { return &___isSprite_6; }
	inline void set_isSprite_6(bool value)
	{
		___isSprite_6 = value;
	}

	inline static int32_t get_offset_of_jumpingTime_7() { return static_cast<int32_t>(offsetof(MoveByKeys_t2244393468, ___jumpingTime_7)); }
	inline float get_jumpingTime_7() const { return ___jumpingTime_7; }
	inline float* get_address_of_jumpingTime_7() { return &___jumpingTime_7; }
	inline void set_jumpingTime_7(float value)
	{
		___jumpingTime_7 = value;
	}

	inline static int32_t get_offset_of_body_8() { return static_cast<int32_t>(offsetof(MoveByKeys_t2244393468, ___body_8)); }
	inline Rigidbody_t3916780224 * get_body_8() const { return ___body_8; }
	inline Rigidbody_t3916780224 ** get_address_of_body_8() { return &___body_8; }
	inline void set_body_8(Rigidbody_t3916780224 * value)
	{
		___body_8 = value;
		Il2CppCodeGenWriteBarrier((&___body_8), value);
	}

	inline static int32_t get_offset_of_body2d_9() { return static_cast<int32_t>(offsetof(MoveByKeys_t2244393468, ___body2d_9)); }
	inline Rigidbody2D_t939494601 * get_body2d_9() const { return ___body2d_9; }
	inline Rigidbody2D_t939494601 ** get_address_of_body2d_9() { return &___body2d_9; }
	inline void set_body2d_9(Rigidbody2D_t939494601 * value)
	{
		___body2d_9 = value;
		Il2CppCodeGenWriteBarrier((&___body2d_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEBYKEYS_T2244393468_H
#ifndef PUNBEHAVIOUR_T987309092_H
#define PUNBEHAVIOUR_T987309092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.PunBehaviour
struct  PunBehaviour_t987309092  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNBEHAVIOUR_T987309092_H
#ifndef INROOMCHAT_T2967338891_H
#define INROOMCHAT_T2967338891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InRoomChat
struct  InRoomChat_t2967338891  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.Rect InRoomChat::GuiRect
	Rect_t2360479859  ___GuiRect_3;
	// System.Boolean InRoomChat::IsVisible
	bool ___IsVisible_4;
	// System.Boolean InRoomChat::AlignBottom
	bool ___AlignBottom_5;
	// System.Collections.Generic.List`1<System.String> InRoomChat::messages
	List_1_t3319525431 * ___messages_6;
	// System.String InRoomChat::inputLine
	String_t* ___inputLine_7;
	// UnityEngine.Vector2 InRoomChat::scrollPos
	Vector2_t2156229523  ___scrollPos_8;

public:
	inline static int32_t get_offset_of_GuiRect_3() { return static_cast<int32_t>(offsetof(InRoomChat_t2967338891, ___GuiRect_3)); }
	inline Rect_t2360479859  get_GuiRect_3() const { return ___GuiRect_3; }
	inline Rect_t2360479859 * get_address_of_GuiRect_3() { return &___GuiRect_3; }
	inline void set_GuiRect_3(Rect_t2360479859  value)
	{
		___GuiRect_3 = value;
	}

	inline static int32_t get_offset_of_IsVisible_4() { return static_cast<int32_t>(offsetof(InRoomChat_t2967338891, ___IsVisible_4)); }
	inline bool get_IsVisible_4() const { return ___IsVisible_4; }
	inline bool* get_address_of_IsVisible_4() { return &___IsVisible_4; }
	inline void set_IsVisible_4(bool value)
	{
		___IsVisible_4 = value;
	}

	inline static int32_t get_offset_of_AlignBottom_5() { return static_cast<int32_t>(offsetof(InRoomChat_t2967338891, ___AlignBottom_5)); }
	inline bool get_AlignBottom_5() const { return ___AlignBottom_5; }
	inline bool* get_address_of_AlignBottom_5() { return &___AlignBottom_5; }
	inline void set_AlignBottom_5(bool value)
	{
		___AlignBottom_5 = value;
	}

	inline static int32_t get_offset_of_messages_6() { return static_cast<int32_t>(offsetof(InRoomChat_t2967338891, ___messages_6)); }
	inline List_1_t3319525431 * get_messages_6() const { return ___messages_6; }
	inline List_1_t3319525431 ** get_address_of_messages_6() { return &___messages_6; }
	inline void set_messages_6(List_1_t3319525431 * value)
	{
		___messages_6 = value;
		Il2CppCodeGenWriteBarrier((&___messages_6), value);
	}

	inline static int32_t get_offset_of_inputLine_7() { return static_cast<int32_t>(offsetof(InRoomChat_t2967338891, ___inputLine_7)); }
	inline String_t* get_inputLine_7() const { return ___inputLine_7; }
	inline String_t** get_address_of_inputLine_7() { return &___inputLine_7; }
	inline void set_inputLine_7(String_t* value)
	{
		___inputLine_7 = value;
		Il2CppCodeGenWriteBarrier((&___inputLine_7), value);
	}

	inline static int32_t get_offset_of_scrollPos_8() { return static_cast<int32_t>(offsetof(InRoomChat_t2967338891, ___scrollPos_8)); }
	inline Vector2_t2156229523  get_scrollPos_8() const { return ___scrollPos_8; }
	inline Vector2_t2156229523 * get_address_of_scrollPos_8() { return &___scrollPos_8; }
	inline void set_scrollPos_8(Vector2_t2156229523  value)
	{
		___scrollPos_8 = value;
	}
};

struct InRoomChat_t2967338891_StaticFields
{
public:
	// System.String InRoomChat::ChatRPC
	String_t* ___ChatRPC_9;

public:
	inline static int32_t get_offset_of_ChatRPC_9() { return static_cast<int32_t>(offsetof(InRoomChat_t2967338891_StaticFields, ___ChatRPC_9)); }
	inline String_t* get_ChatRPC_9() const { return ___ChatRPC_9; }
	inline String_t** get_address_of_ChatRPC_9() { return &___ChatRPC_9; }
	inline void set_ChatRPC_9(String_t* value)
	{
		___ChatRPC_9 = value;
		Il2CppCodeGenWriteBarrier((&___ChatRPC_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INROOMCHAT_T2967338891_H
#ifndef CONNECTANDJOINRANDOM_T3582048891_H
#define CONNECTANDJOINRANDOM_T3582048891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectAndJoinRandom
struct  ConnectAndJoinRandom_t3582048891  : public MonoBehaviour_t3225183318
{
public:
	// System.Boolean ConnectAndJoinRandom::AutoConnect
	bool ___AutoConnect_3;
	// System.Byte ConnectAndJoinRandom::Version
	uint8_t ___Version_4;
	// System.Boolean ConnectAndJoinRandom::ConnectInUpdate
	bool ___ConnectInUpdate_5;

public:
	inline static int32_t get_offset_of_AutoConnect_3() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandom_t3582048891, ___AutoConnect_3)); }
	inline bool get_AutoConnect_3() const { return ___AutoConnect_3; }
	inline bool* get_address_of_AutoConnect_3() { return &___AutoConnect_3; }
	inline void set_AutoConnect_3(bool value)
	{
		___AutoConnect_3 = value;
	}

	inline static int32_t get_offset_of_Version_4() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandom_t3582048891, ___Version_4)); }
	inline uint8_t get_Version_4() const { return ___Version_4; }
	inline uint8_t* get_address_of_Version_4() { return &___Version_4; }
	inline void set_Version_4(uint8_t value)
	{
		___Version_4 = value;
	}

	inline static int32_t get_offset_of_ConnectInUpdate_5() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandom_t3582048891, ___ConnectInUpdate_5)); }
	inline bool get_ConnectInUpdate_5() const { return ___ConnectInUpdate_5; }
	inline bool* get_address_of_ConnectInUpdate_5() { return &___ConnectInUpdate_5; }
	inline void set_ConnectInUpdate_5(bool value)
	{
		___ConnectInUpdate_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTANDJOINRANDOM_T3582048891_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (FriendInfo_t533296844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[3] = 
{
	FriendInfo_t533296844::get_offset_of_U3CNameU3Ek__BackingField_0(),
	FriendInfo_t533296844::get_offset_of_U3CIsOnlineU3Ek__BackingField_1(),
	FriendInfo_t533296844::get_offset_of_U3CRoomU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (GizmoType_t3704257101)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2701[5] = 
{
	GizmoType_t3704257101::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (GizmoTypeDrawer_t184707570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (LoadBalancingPeer_t3218467959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[1] = 
{
	LoadBalancingPeer_t3218467959::get_offset_of_opParameters_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (RoomOptionBit_t4007729377)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2704[7] = 
{
	RoomOptionBit_t4007729377::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (OpJoinRandomRoomParams_t491090087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[6] = 
{
	OpJoinRandomRoomParams_t491090087::get_offset_of_ExpectedCustomRoomProperties_0(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_ExpectedMaxPlayers_1(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_MatchingType_2(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_TypedLobby_3(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_SqlLobbyFilter_4(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_ExpectedUsers_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (EnterRoomParams_t3960472384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[8] = 
{
	EnterRoomParams_t3960472384::get_offset_of_RoomName_0(),
	EnterRoomParams_t3960472384::get_offset_of_RoomOptions_1(),
	EnterRoomParams_t3960472384::get_offset_of_Lobby_2(),
	EnterRoomParams_t3960472384::get_offset_of_PlayerProperties_3(),
	EnterRoomParams_t3960472384::get_offset_of_OnGameServer_4(),
	EnterRoomParams_t3960472384::get_offset_of_CreateIfNotExists_5(),
	EnterRoomParams_t3960472384::get_offset_of_RejoinOnly_6(),
	EnterRoomParams_t3960472384::get_offset_of_ExpectedUsers_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (ErrorCode_t835159227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (ActorProperties_t3254499384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (GamePropertyKey_t1877143020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (EventCode_t3064356988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (ParameterCode_t2914727942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[67] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (OperationCode_t3680831435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[20] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (JoinMode_t2253954680)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2713[5] = 
{
	JoinMode_t2253954680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (MatchmakingMode_t1165119589)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2714[4] = 
{
	MatchmakingMode_t1165119589::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (ReceiverGroup_t3206452792)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2715[4] = 
{
	ReceiverGroup_t3206452792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (EventCaching_t168509732)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2716[13] = 
{
	EventCaching_t168509732::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (PropertyTypeFlag_t3957538841)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2717[5] = 
{
	PropertyTypeFlag_t3957538841::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (RoomOptions_t1787645948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[12] = 
{
	RoomOptions_t1787645948::get_offset_of_isVisibleField_0(),
	RoomOptions_t1787645948::get_offset_of_isOpenField_1(),
	RoomOptions_t1787645948::get_offset_of_MaxPlayers_2(),
	RoomOptions_t1787645948::get_offset_of_PlayerTtl_3(),
	RoomOptions_t1787645948::get_offset_of_EmptyRoomTtl_4(),
	RoomOptions_t1787645948::get_offset_of_cleanupCacheOnLeaveField_5(),
	RoomOptions_t1787645948::get_offset_of_CustomRoomProperties_6(),
	RoomOptions_t1787645948::get_offset_of_CustomRoomPropertiesForLobby_7(),
	RoomOptions_t1787645948::get_offset_of_Plugins_8(),
	RoomOptions_t1787645948::get_offset_of_suppressRoomEventsField_9(),
	RoomOptions_t1787645948::get_offset_of_publishUserIdField_10(),
	RoomOptions_t1787645948::get_offset_of_deleteNullPropertiesField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (RaiseEventOptions_t1229553678), -1, sizeof(RaiseEventOptions_t1229553678_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2719[8] = 
{
	RaiseEventOptions_t1229553678_StaticFields::get_offset_of_Default_0(),
	RaiseEventOptions_t1229553678::get_offset_of_CachingOption_1(),
	RaiseEventOptions_t1229553678::get_offset_of_InterestGroup_2(),
	RaiseEventOptions_t1229553678::get_offset_of_TargetActors_3(),
	RaiseEventOptions_t1229553678::get_offset_of_Receivers_4(),
	RaiseEventOptions_t1229553678::get_offset_of_SequenceChannel_5(),
	RaiseEventOptions_t1229553678::get_offset_of_ForwardToWebhook_6(),
	RaiseEventOptions_t1229553678::get_offset_of_Encrypt_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (LobbyType_t3695323860)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2720[4] = 
{
	LobbyType_t3695323860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (TypedLobby_t3336582029), -1, sizeof(TypedLobby_t3336582029_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2721[3] = 
{
	TypedLobby_t3336582029::get_offset_of_Name_0(),
	TypedLobby_t3336582029::get_offset_of_Type_1(),
	TypedLobby_t3336582029_StaticFields::get_offset_of_Default_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (TypedLobbyInfo_t2504508049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	TypedLobbyInfo_t2504508049::get_offset_of_PlayerCount_3(),
	TypedLobbyInfo_t2504508049::get_offset_of_RoomCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (AuthModeOption_t1305270560)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2723[4] = 
{
	AuthModeOption_t1305270560::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (CustomAuthenticationType_t302987107)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2724[8] = 
{
	CustomAuthenticationType_t302987107::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (AuthenticationValues_t660572511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[5] = 
{
	AuthenticationValues_t660572511::get_offset_of_authType_0(),
	AuthenticationValues_t660572511::get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1(),
	AuthenticationValues_t660572511::get_offset_of_U3CAuthPostDataU3Ek__BackingField_2(),
	AuthenticationValues_t660572511::get_offset_of_U3CTokenU3Ek__BackingField_3(),
	AuthenticationValues_t660572511::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (ClientState_t1348705391)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2726[22] = 
{
	ClientState_t1348705391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (JoinType_t3510207077)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2727[5] = 
{
	JoinType_t3510207077::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (DisconnectCause_t501870387)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2728[13] = 
{
	DisconnectCause_t501870387::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (ServerConnection_t867335480)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2729[4] = 
{
	ServerConnection_t867335480::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (NetworkingPeer_t264212356), -1, sizeof(NetworkingPeer_t264212356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2730[65] = 
{
	NetworkingPeer_t264212356::get_offset_of_AppId_40(),
	NetworkingPeer_t264212356::get_offset_of_U3CAuthValuesU3Ek__BackingField_41(),
	NetworkingPeer_t264212356::get_offset_of_tokenCache_42(),
	NetworkingPeer_t264212356::get_offset_of_AuthMode_43(),
	NetworkingPeer_t264212356::get_offset_of_EncryptionMode_44(),
	NetworkingPeer_t264212356::get_offset_of_U3CIsUsingNameServerU3Ek__BackingField_45(),
	0,
	0,
	NetworkingPeer_t264212356_StaticFields::get_offset_of_ProtocolToNameServerPort_48(),
	NetworkingPeer_t264212356::get_offset_of_U3CMasterServerAddressU3Ek__BackingField_49(),
	NetworkingPeer_t264212356::get_offset_of_U3CGameServerAddressU3Ek__BackingField_50(),
	NetworkingPeer_t264212356::get_offset_of_U3CServerU3Ek__BackingField_51(),
	NetworkingPeer_t264212356::get_offset_of_U3CStateU3Ek__BackingField_52(),
	NetworkingPeer_t264212356::get_offset_of_IsInitialConnect_53(),
	NetworkingPeer_t264212356::get_offset_of_insideLobby_54(),
	NetworkingPeer_t264212356::get_offset_of_U3ClobbyU3Ek__BackingField_55(),
	NetworkingPeer_t264212356::get_offset_of_LobbyStatistics_56(),
	NetworkingPeer_t264212356::get_offset_of_mGameList_57(),
	NetworkingPeer_t264212356::get_offset_of_mGameListCopy_58(),
	NetworkingPeer_t264212356::get_offset_of_playername_59(),
	NetworkingPeer_t264212356::get_offset_of_mPlayernameHasToBeUpdated_60(),
	NetworkingPeer_t264212356::get_offset_of_currentRoom_61(),
	NetworkingPeer_t264212356::get_offset_of_U3CLocalPlayerU3Ek__BackingField_62(),
	NetworkingPeer_t264212356::get_offset_of_U3CPlayersOnMasterCountU3Ek__BackingField_63(),
	NetworkingPeer_t264212356::get_offset_of_U3CPlayersInRoomsCountU3Ek__BackingField_64(),
	NetworkingPeer_t264212356::get_offset_of_U3CRoomsCountU3Ek__BackingField_65(),
	NetworkingPeer_t264212356::get_offset_of_lastJoinType_66(),
	NetworkingPeer_t264212356::get_offset_of_enterRoomParamsCache_67(),
	NetworkingPeer_t264212356::get_offset_of_didAuthenticate_68(),
	NetworkingPeer_t264212356::get_offset_of_friendListRequested_69(),
	NetworkingPeer_t264212356::get_offset_of_friendListTimestamp_70(),
	NetworkingPeer_t264212356::get_offset_of_isFetchingFriendList_71(),
	NetworkingPeer_t264212356::get_offset_of_U3CAvailableRegionsU3Ek__BackingField_72(),
	NetworkingPeer_t264212356::get_offset_of_U3CCloudRegionU3Ek__BackingField_73(),
	NetworkingPeer_t264212356::get_offset_of_mActors_74(),
	NetworkingPeer_t264212356::get_offset_of_mOtherPlayerListCopy_75(),
	NetworkingPeer_t264212356::get_offset_of_mPlayerListCopy_76(),
	NetworkingPeer_t264212356::get_offset_of_hasSwitchedMC_77(),
	NetworkingPeer_t264212356::get_offset_of_allowedReceivingGroups_78(),
	NetworkingPeer_t264212356::get_offset_of_blockSendingGroups_79(),
	NetworkingPeer_t264212356::get_offset_of_photonViewList_80(),
	NetworkingPeer_t264212356::get_offset_of_readStream_81(),
	NetworkingPeer_t264212356::get_offset_of_pStream_82(),
	NetworkingPeer_t264212356::get_offset_of_dataPerGroupReliable_83(),
	NetworkingPeer_t264212356::get_offset_of_dataPerGroupUnreliable_84(),
	NetworkingPeer_t264212356::get_offset_of_currentLevelPrefix_85(),
	NetworkingPeer_t264212356::get_offset_of_loadingLevelAndPausedNetwork_86(),
	0,
	NetworkingPeer_t264212356_StaticFields::get_offset_of_UsePrefabCache_88(),
	NetworkingPeer_t264212356::get_offset_of_ObjectPool_89(),
	NetworkingPeer_t264212356_StaticFields::get_offset_of_PrefabCache_90(),
	NetworkingPeer_t264212356::get_offset_of_monoRPCMethodsCache_91(),
	NetworkingPeer_t264212356::get_offset_of_rpcShortcuts_92(),
	NetworkingPeer_t264212356_StaticFields::get_offset_of_OnPhotonInstantiateString_93(),
	NetworkingPeer_t264212356::get_offset_of_cachedServerAddress_94(),
	NetworkingPeer_t264212356::get_offset_of_cachedApplicationName_95(),
	NetworkingPeer_t264212356::get_offset_of_cachedProtocolType_96(),
	NetworkingPeer_t264212356::get_offset_of__isReconnecting_97(),
	NetworkingPeer_t264212356::get_offset_of_tempInstantiationData_98(),
	NetworkingPeer_t264212356_StaticFields::get_offset_of_ObjectsInOneUpdate_99(),
	NetworkingPeer_t264212356::get_offset_of_options_100(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (MonoBehaviour_t3225183318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[1] = 
{
	MonoBehaviour_t3225183318::get_offset_of_pvCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (PunBehaviour_t987309092), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (PhotonMessageInfo_t3855471533)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[3] = 
{
	PhotonMessageInfo_t3855471533::get_offset_of_timeInt_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PhotonMessageInfo_t3855471533::get_offset_of_sender_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PhotonMessageInfo_t3855471533::get_offset_of_photonView_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (PunEvent_t699376346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (PhotonStream_t1003850889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[4] = 
{
	PhotonStream_t1003850889::get_offset_of_write_0(),
	PhotonStream_t1003850889::get_offset_of_writeData_1(),
	PhotonStream_t1003850889::get_offset_of_readData_2(),
	PhotonStream_t1003850889::get_offset_of_currentItem_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (SceneManagerHelper_t3665721098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (WebRpcResponse_t4177102182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[4] = 
{
	WebRpcResponse_t4177102182::get_offset_of_U3CNameU3Ek__BackingField_0(),
	WebRpcResponse_t4177102182::get_offset_of_U3CReturnCodeU3Ek__BackingField_1(),
	WebRpcResponse_t4177102182::get_offset_of_U3CDebugMessageU3Ek__BackingField_2(),
	WebRpcResponse_t4177102182::get_offset_of_U3CParametersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (PhotonHandler_t2139970417), -1, sizeof(PhotonHandler_t2139970417_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2741[12] = 
{
	PhotonHandler_t2139970417_StaticFields::get_offset_of_SP_2(),
	PhotonHandler_t2139970417::get_offset_of_updateInterval_3(),
	PhotonHandler_t2139970417::get_offset_of_updateIntervalOnSerialize_4(),
	PhotonHandler_t2139970417::get_offset_of_nextSendTickCount_5(),
	PhotonHandler_t2139970417::get_offset_of_nextSendTickCountOnSerialize_6(),
	PhotonHandler_t2139970417_StaticFields::get_offset_of_sendThreadShouldRun_7(),
	PhotonHandler_t2139970417_StaticFields::get_offset_of_timerToStopConnectionInBackground_8(),
	PhotonHandler_t2139970417_StaticFields::get_offset_of_AppQuits_9(),
	PhotonHandler_t2139970417_StaticFields::get_offset_of_PingImplementation_10(),
	0,
	PhotonHandler_t2139970417_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
	PhotonHandler_t2139970417_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[7] = 
{
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202::get_offset_of_U3CpingManagerU3E__0_0(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202::get_offset_of_U24locvar0_1(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202::get_offset_of_U3CbestU3E__0_2(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202::get_offset_of_connectToBest_3(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202::get_offset_of_U24current_4(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202::get_offset_of_U24disposing_5(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t2850618202::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (PhotonLagSimulationGui_t3583255037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[4] = 
{
	PhotonLagSimulationGui_t3583255037::get_offset_of_WindowRect_2(),
	PhotonLagSimulationGui_t3583255037::get_offset_of_WindowId_3(),
	PhotonLagSimulationGui_t3583255037::get_offset_of_Visible_4(),
	PhotonLagSimulationGui_t3583255037::get_offset_of_U3CPeerU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (PhotonNetwork_t1610183659), -1, sizeof(PhotonNetwork_t1610183659_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2744[34] = 
{
	0,
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_U3CgameVersionU3Ek__BackingField_1(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_photonMono_2(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_networkingPeer_3(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_MAX_VIEW_IDS_4(),
	0,
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_PhotonServerSettings_6(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_InstantiateInRoomOnly_7(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_logLevel_8(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_U3CFriendsU3Ek__BackingField_9(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_precisionForVectorSynchronization_10(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_precisionForQuaternionSynchronization_11(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_precisionForFloatSynchronization_12(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_UseRpcMonoBehaviourCache_13(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_UsePrefabCache_14(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_PrefabCache_15(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_SendMonoMessageTargets_16(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_SendMonoMessageTargetType_17(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_StartRpcsAsCoroutine_18(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_isOfflineMode_19(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_offlineModeRoom_20(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_maxConnections_21(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of__mAutomaticallySyncScene_22(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_m_autoCleanUpPlayerObjects_23(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_sendInterval_24(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_sendIntervalOnSerialize_25(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_m_isMessageQueueRunning_26(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_UsePreciseTimer_27(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_startupStopwatch_28(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_BackgroundTimeout_29(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_OnEventCall_30(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_lastUsedViewSubId_31(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_lastUsedViewSubIdStatic_32(),
	PhotonNetwork_t1610183659_StaticFields::get_offset_of_manuallyAllocatedViewIds_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (EventCallback_t1220598991), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (PhotonPlayer_t3305149557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[7] = 
{
	PhotonPlayer_t3305149557::get_offset_of_actorID_0(),
	PhotonPlayer_t3305149557::get_offset_of_nameField_1(),
	PhotonPlayer_t3305149557::get_offset_of_U3CUserIdU3Ek__BackingField_2(),
	PhotonPlayer_t3305149557::get_offset_of_IsLocal_3(),
	PhotonPlayer_t3305149557::get_offset_of_U3CIsInactiveU3Ek__BackingField_4(),
	PhotonPlayer_t3305149557::get_offset_of_U3CCustomPropertiesU3Ek__BackingField_5(),
	PhotonPlayer_t3305149557::get_offset_of_TagObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (PhotonStatsGui_t1231606017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[7] = 
{
	PhotonStatsGui_t1231606017::get_offset_of_statsWindowOn_2(),
	PhotonStatsGui_t1231606017::get_offset_of_statsOn_3(),
	PhotonStatsGui_t1231606017::get_offset_of_healthStatsVisible_4(),
	PhotonStatsGui_t1231606017::get_offset_of_trafficStatsOn_5(),
	PhotonStatsGui_t1231606017::get_offset_of_buttonsOn_6(),
	PhotonStatsGui_t1231606017::get_offset_of_statsRect_7(),
	PhotonStatsGui_t1231606017::get_offset_of_WindowId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (PhotonStreamQueue_t3244431384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[8] = 
{
	PhotonStreamQueue_t3244431384::get_offset_of_m_SampleRate_0(),
	PhotonStreamQueue_t3244431384::get_offset_of_m_SampleCount_1(),
	PhotonStreamQueue_t3244431384::get_offset_of_m_ObjectsPerSample_2(),
	PhotonStreamQueue_t3244431384::get_offset_of_m_LastSampleTime_3(),
	PhotonStreamQueue_t3244431384::get_offset_of_m_LastFrameCount_4(),
	PhotonStreamQueue_t3244431384::get_offset_of_m_NextObjectIndex_5(),
	PhotonStreamQueue_t3244431384::get_offset_of_m_Objects_6(),
	PhotonStreamQueue_t3244431384::get_offset_of_m_IsWriting_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (ViewSynchronization_t3183556584)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2749[5] = 
{
	ViewSynchronization_t3183556584::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (OnSerializeTransform_t1364648257)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2750[6] = 
{
	OnSerializeTransform_t1364648257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (OnSerializeRigidBody_t385167779)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2751[4] = 
{
	OnSerializeRigidBody_t385167779::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (OwnershipOption_t37885007)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2752[4] = 
{
	OwnershipOption_t37885007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (PhotonView_t2207721820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[23] = 
{
	PhotonView_t2207721820::get_offset_of_ownerId_3(),
	PhotonView_t2207721820::get_offset_of_group_4(),
	PhotonView_t2207721820::get_offset_of_mixedModeIsReliable_5(),
	PhotonView_t2207721820::get_offset_of_OwnerShipWasTransfered_6(),
	PhotonView_t2207721820::get_offset_of_prefixBackup_7(),
	PhotonView_t2207721820::get_offset_of_instantiationDataField_8(),
	PhotonView_t2207721820::get_offset_of_lastOnSerializeDataSent_9(),
	PhotonView_t2207721820::get_offset_of_lastOnSerializeDataReceived_10(),
	PhotonView_t2207721820::get_offset_of_synchronization_11(),
	PhotonView_t2207721820::get_offset_of_onSerializeTransformOption_12(),
	PhotonView_t2207721820::get_offset_of_onSerializeRigidBodyOption_13(),
	PhotonView_t2207721820::get_offset_of_ownershipTransfer_14(),
	PhotonView_t2207721820::get_offset_of_ObservedComponents_15(),
	PhotonView_t2207721820::get_offset_of_m_OnSerializeMethodInfos_16(),
	PhotonView_t2207721820::get_offset_of_viewIdField_17(),
	PhotonView_t2207721820::get_offset_of_instantiationId_18(),
	PhotonView_t2207721820::get_offset_of_currentMasterID_19(),
	PhotonView_t2207721820::get_offset_of_didAwake_20(),
	PhotonView_t2207721820::get_offset_of_isRuntimeInstantiated_21(),
	PhotonView_t2207721820::get_offset_of_removedFromLocalViewList_22(),
	PhotonView_t2207721820::get_offset_of_RpcMonoBehaviours_23(),
	PhotonView_t2207721820::get_offset_of_OnSerializeMethodInfo_24(),
	PhotonView_t2207721820::get_offset_of_failedToFindOnSerialize_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (PhotonPingManager_t630892274), -1, sizeof(PhotonPingManager_t630892274_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2754[6] = 
{
	PhotonPingManager_t630892274::get_offset_of_UseNative_0(),
	PhotonPingManager_t630892274_StaticFields::get_offset_of_Attempts_1(),
	PhotonPingManager_t630892274_StaticFields::get_offset_of_IgnoreInitialAttempt_2(),
	PhotonPingManager_t630892274_StaticFields::get_offset_of_MaxMilliseconsPerPing_3(),
	0,
	PhotonPingManager_t630892274::get_offset_of_PingsRunning_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (U3CPingSocketU3Ec__Iterator0_t2858604848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[15] = 
{
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_region_0(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CpingU3E__1_1(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CrttSumU3E__0_2(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CreplyCountU3E__0_3(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CregionAddressU3E__0_4(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CindexOfColonU3E__0_5(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CindexOfProtocolU3E__0_6(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CiU3E__2_7(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CovertimeU3E__3_8(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CswU3E__3_9(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U3CrttU3E__3_10(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U24this_11(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U24current_12(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U24disposing_13(),
	U3CPingSocketU3Ec__Iterator0_t2858604848::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (Room_t3759828263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[1] = 
{
	Room_t3759828263::get_offset_of_U3CPropertiesListedInLobbyU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (RoomInfo_t3170295620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[12] = 
{
	RoomInfo_t3170295620::get_offset_of_U3CremovedFromListU3Ek__BackingField_0(),
	RoomInfo_t3170295620::get_offset_of_customPropertiesField_1(),
	RoomInfo_t3170295620::get_offset_of_maxPlayersField_2(),
	RoomInfo_t3170295620::get_offset_of_expectedUsersField_3(),
	RoomInfo_t3170295620::get_offset_of_openField_4(),
	RoomInfo_t3170295620::get_offset_of_visibleField_5(),
	RoomInfo_t3170295620::get_offset_of_autoCleanUpField_6(),
	RoomInfo_t3170295620::get_offset_of_nameField_7(),
	RoomInfo_t3170295620::get_offset_of_masterClientIdField_8(),
	RoomInfo_t3170295620::get_offset_of_U3CserverSideMasterClientU3Ek__BackingField_9(),
	RoomInfo_t3170295620::get_offset_of_U3CPlayerCountU3Ek__BackingField_10(),
	RoomInfo_t3170295620::get_offset_of_U3CIsLocalClientInsideU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (PunRPC_t1644934964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (Region_t3684225262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[4] = 
{
	Region_t3684225262::get_offset_of_Code_0(),
	Region_t3684225262::get_offset_of_Cluster_1(),
	Region_t3684225262::get_offset_of_HostAndPort_2(),
	Region_t3684225262::get_offset_of_Ping_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (ServerSettings_t2755303613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[17] = 
{
	ServerSettings_t2755303613::get_offset_of_AppID_2(),
	ServerSettings_t2755303613::get_offset_of_VoiceAppID_3(),
	ServerSettings_t2755303613::get_offset_of_ChatAppID_4(),
	ServerSettings_t2755303613::get_offset_of_HostType_5(),
	ServerSettings_t2755303613::get_offset_of_PreferredRegion_6(),
	ServerSettings_t2755303613::get_offset_of_EnabledRegions_7(),
	ServerSettings_t2755303613::get_offset_of_Protocol_8(),
	ServerSettings_t2755303613::get_offset_of_ServerAddress_9(),
	ServerSettings_t2755303613::get_offset_of_ServerPort_10(),
	ServerSettings_t2755303613::get_offset_of_VoiceServerPort_11(),
	ServerSettings_t2755303613::get_offset_of_JoinLobby_12(),
	ServerSettings_t2755303613::get_offset_of_EnableLobbyStatistics_13(),
	ServerSettings_t2755303613::get_offset_of_PunLogging_14(),
	ServerSettings_t2755303613::get_offset_of_NetworkLogging_15(),
	ServerSettings_t2755303613::get_offset_of_RunInBackground_16(),
	ServerSettings_t2755303613::get_offset_of_RpcList_17(),
	ServerSettings_t2755303613::get_offset_of_DisableAutoOpenWizard_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (HostingOption_t2949276063)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2761[6] = 
{
	HostingOption_t2949276063::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (PhotonAnimatorView_t3352472062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[11] = 
{
	PhotonAnimatorView_t3352472062::get_offset_of_m_Animator_2(),
	PhotonAnimatorView_t3352472062::get_offset_of_m_StreamQueue_3(),
	PhotonAnimatorView_t3352472062::get_offset_of_ShowLayerWeightsInspector_4(),
	PhotonAnimatorView_t3352472062::get_offset_of_ShowParameterInspector_5(),
	PhotonAnimatorView_t3352472062::get_offset_of_m_SynchronizeParameters_6(),
	PhotonAnimatorView_t3352472062::get_offset_of_m_SynchronizeLayers_7(),
	PhotonAnimatorView_t3352472062::get_offset_of_m_ReceiverPosition_8(),
	PhotonAnimatorView_t3352472062::get_offset_of_m_LastDeserializeTime_9(),
	PhotonAnimatorView_t3352472062::get_offset_of_m_WasSynchronizeTypeChanged_10(),
	PhotonAnimatorView_t3352472062::get_offset_of_m_PhotonView_11(),
	PhotonAnimatorView_t3352472062::get_offset_of_m_raisedDiscreteTriggersCache_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (ParameterType_t1940879453)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2763[5] = 
{
	ParameterType_t1940879453::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (SynchronizeType_t4108284517)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2764[4] = 
{
	SynchronizeType_t4108284517::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (SynchronizedParameter_t1800668114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[3] = 
{
	SynchronizedParameter_t1800668114::get_offset_of_Type_0(),
	SynchronizedParameter_t1800668114::get_offset_of_SynchronizeType_1(),
	SynchronizedParameter_t1800668114::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (SynchronizedLayer_t3485728275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[2] = 
{
	SynchronizedLayer_t3485728275::get_offset_of_SynchronizeType_0(),
	SynchronizedLayer_t3485728275::get_offset_of_LayerIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey0_t2411181974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[1] = 
{
	U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey0_t2411181974::get_offset_of_layerIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey1_t2105236361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[1] = 
{
	U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey1_t2105236361::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (U3CGetLayerSynchronizeTypeU3Ec__AnonStorey2_t4244813680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2769[1] = 
{
	U3CGetLayerSynchronizeTypeU3Ec__AnonStorey2_t4244813680::get_offset_of_layerIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (U3CGetParameterSynchronizeTypeU3Ec__AnonStorey3_t204833724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2770[1] = 
{
	U3CGetParameterSynchronizeTypeU3Ec__AnonStorey3_t204833724::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (U3CSetLayerSynchronizedU3Ec__AnonStorey4_t2749014471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[1] = 
{
	U3CSetLayerSynchronizedU3Ec__AnonStorey4_t2749014471::get_offset_of_layerIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (U3CSetParameterSynchronizedU3Ec__AnonStorey5_t3597370861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2772[1] = 
{
	U3CSetParameterSynchronizedU3Ec__AnonStorey5_t3597370861::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (PhotonRigidbody2DView_t585014740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[3] = 
{
	PhotonRigidbody2DView_t585014740::get_offset_of_m_SynchronizeVelocity_2(),
	PhotonRigidbody2DView_t585014740::get_offset_of_m_SynchronizeAngularVelocity_3(),
	PhotonRigidbody2DView_t585014740::get_offset_of_m_Body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (PhotonRigidbodyView_t56173500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[3] = 
{
	PhotonRigidbodyView_t56173500::get_offset_of_m_SynchronizeVelocity_2(),
	PhotonRigidbodyView_t56173500::get_offset_of_m_SynchronizeAngularVelocity_3(),
	PhotonRigidbodyView_t56173500::get_offset_of_m_Body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (PhotonTransformView_t372465615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[9] = 
{
	PhotonTransformView_t372465615::get_offset_of_m_PositionModel_2(),
	PhotonTransformView_t372465615::get_offset_of_m_RotationModel_3(),
	PhotonTransformView_t372465615::get_offset_of_m_ScaleModel_4(),
	PhotonTransformView_t372465615::get_offset_of_m_PositionControl_5(),
	PhotonTransformView_t372465615::get_offset_of_m_RotationControl_6(),
	PhotonTransformView_t372465615::get_offset_of_m_ScaleControl_7(),
	PhotonTransformView_t372465615::get_offset_of_m_PhotonView_8(),
	PhotonTransformView_t372465615::get_offset_of_m_ReceivedNetworkUpdate_9(),
	PhotonTransformView_t372465615::get_offset_of_m_firstTake_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (PhotonTransformViewPositionControl_t619346209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[8] = 
{
	PhotonTransformViewPositionControl_t619346209::get_offset_of_m_Model_0(),
	PhotonTransformViewPositionControl_t619346209::get_offset_of_m_CurrentSpeed_1(),
	PhotonTransformViewPositionControl_t619346209::get_offset_of_m_LastSerializeTime_2(),
	PhotonTransformViewPositionControl_t619346209::get_offset_of_m_SynchronizedSpeed_3(),
	PhotonTransformViewPositionControl_t619346209::get_offset_of_m_SynchronizedTurnSpeed_4(),
	PhotonTransformViewPositionControl_t619346209::get_offset_of_m_NetworkPosition_5(),
	PhotonTransformViewPositionControl_t619346209::get_offset_of_m_OldNetworkPositions_6(),
	PhotonTransformViewPositionControl_t619346209::get_offset_of_m_UpdatedPositionAfterOnSerialize_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (PhotonTransformViewPositionModel_t2500134640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[14] = 
{
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_SynchronizeEnabled_0(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_TeleportEnabled_1(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_TeleportIfDistanceGreaterThan_2(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_InterpolateOption_3(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_InterpolateMoveTowardsSpeed_4(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_InterpolateLerpSpeed_5(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_InterpolateMoveTowardsAcceleration_6(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_InterpolateMoveTowardsDeceleration_7(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_InterpolateSpeedCurve_8(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_ExtrapolateOption_9(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_ExtrapolateSpeed_10(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_ExtrapolateIncludingRoundTripTime_11(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_ExtrapolateNumberOfStoredPositions_12(),
	PhotonTransformViewPositionModel_t2500134640::get_offset_of_DrawErrorGizmo_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (InterpolateOptions_t1912251329)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2778[6] = 
{
	InterpolateOptions_t1912251329::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (ExtrapolateOptions_t2438484843)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2779[5] = 
{
	ExtrapolateOptions_t2438484843::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (PhotonTransformViewRotationControl_t2679094986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[2] = 
{
	PhotonTransformViewRotationControl_t2679094986::get_offset_of_m_Model_0(),
	PhotonTransformViewRotationControl_t2679094986::get_offset_of_m_NetworkRotation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (PhotonTransformViewRotationModel_t1080899250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[4] = 
{
	PhotonTransformViewRotationModel_t1080899250::get_offset_of_SynchronizeEnabled_0(),
	PhotonTransformViewRotationModel_t1080899250::get_offset_of_InterpolateOption_1(),
	PhotonTransformViewRotationModel_t1080899250::get_offset_of_InterpolateRotateTowardsSpeed_2(),
	PhotonTransformViewRotationModel_t1080899250::get_offset_of_InterpolateLerpSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (InterpolateOptions_t364195466)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2782[4] = 
{
	InterpolateOptions_t364195466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (PhotonTransformViewScaleControl_t2271393751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[2] = 
{
	PhotonTransformViewScaleControl_t2271393751::get_offset_of_m_Model_0(),
	PhotonTransformViewScaleControl_t2271393751::get_offset_of_m_NetworkScale_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (PhotonTransformViewScaleModel_t763003770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[4] = 
{
	PhotonTransformViewScaleModel_t763003770::get_offset_of_SynchronizeEnabled_0(),
	PhotonTransformViewScaleModel_t763003770::get_offset_of_InterpolateOption_1(),
	PhotonTransformViewScaleModel_t763003770::get_offset_of_InterpolateMoveTowardsSpeed_2(),
	PhotonTransformViewScaleModel_t763003770::get_offset_of_InterpolateLerpSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (InterpolateOptions_t1056869502)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2785[4] = 
{
	InterpolateOptions_t1056869502::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (ConnectAndJoinRandom_t3582048891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[3] = 
{
	ConnectAndJoinRandom_t3582048891::get_offset_of_AutoConnect_3(),
	ConnectAndJoinRandom_t3582048891::get_offset_of_Version_4(),
	ConnectAndJoinRandom_t3582048891::get_offset_of_ConnectInUpdate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (CullArea_t3053759289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[16] = 
{
	0,
	0,
	CullArea_t3053759289::get_offset_of_FIRST_GROUP_ID_4(),
	CullArea_t3053759289::get_offset_of_SUBDIVISION_FIRST_LEVEL_ORDER_5(),
	CullArea_t3053759289::get_offset_of_SUBDIVISION_SECOND_LEVEL_ORDER_6(),
	CullArea_t3053759289::get_offset_of_SUBDIVISION_THIRD_LEVEL_ORDER_7(),
	CullArea_t3053759289::get_offset_of_Center_8(),
	CullArea_t3053759289::get_offset_of_Size_9(),
	CullArea_t3053759289::get_offset_of_Subdivisions_10(),
	CullArea_t3053759289::get_offset_of_NumberOfSubdivisions_11(),
	CullArea_t3053759289::get_offset_of_U3CCellCountU3Ek__BackingField_12(),
	CullArea_t3053759289::get_offset_of_U3CCellTreeU3Ek__BackingField_13(),
	CullArea_t3053759289::get_offset_of_U3CMapU3Ek__BackingField_14(),
	CullArea_t3053759289::get_offset_of_YIsUpAxis_15(),
	CullArea_t3053759289::get_offset_of_RecreateCellHierarchy_16(),
	CullArea_t3053759289::get_offset_of_idCounter_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (CellTree_t3785927468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2788[1] = 
{
	CellTree_t3785927468::get_offset_of_U3CRootNodeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (CellTreeNode_t2932145224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[9] = 
{
	CellTreeNode_t2932145224::get_offset_of_Id_0(),
	CellTreeNode_t2932145224::get_offset_of_Center_1(),
	CellTreeNode_t2932145224::get_offset_of_Size_2(),
	CellTreeNode_t2932145224::get_offset_of_TopLeft_3(),
	CellTreeNode_t2932145224::get_offset_of_BottomRight_4(),
	CellTreeNode_t2932145224::get_offset_of_NodeType_5(),
	CellTreeNode_t2932145224::get_offset_of_Parent_6(),
	CellTreeNode_t2932145224::get_offset_of_Childs_7(),
	CellTreeNode_t2932145224::get_offset_of_maxDistance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (ENodeType_t1612415973)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2790[4] = 
{
	ENodeType_t1612415973::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (InputToEvent_t2359295403), -1, sizeof(InputToEvent_t2359295403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2791[8] = 
{
	InputToEvent_t2359295403::get_offset_of_lastGo_2(),
	InputToEvent_t2359295403_StaticFields::get_offset_of_inputHitPos_3(),
	InputToEvent_t2359295403::get_offset_of_DetectPointedAtGameObject_4(),
	InputToEvent_t2359295403_StaticFields::get_offset_of_U3CgoPointedAtU3Ek__BackingField_5(),
	InputToEvent_t2359295403::get_offset_of_pressedPosition_6(),
	InputToEvent_t2359295403::get_offset_of_currentPos_7(),
	InputToEvent_t2359295403::get_offset_of_Dragging_8(),
	InputToEvent_t2359295403::get_offset_of_m_Camera_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (InRoomChat_t2967338891), -1, sizeof(InRoomChat_t2967338891_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2792[7] = 
{
	InRoomChat_t2967338891::get_offset_of_GuiRect_3(),
	InRoomChat_t2967338891::get_offset_of_IsVisible_4(),
	InRoomChat_t2967338891::get_offset_of_AlignBottom_5(),
	InRoomChat_t2967338891::get_offset_of_messages_6(),
	InRoomChat_t2967338891::get_offset_of_inputLine_7(),
	InRoomChat_t2967338891::get_offset_of_scrollPos_8(),
	InRoomChat_t2967338891_StaticFields::get_offset_of_ChatRPC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (ManualPhotonViewAllocator_t3877483065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[1] = 
{
	ManualPhotonViewAllocator_t3877483065::get_offset_of_Prefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (MoveByKeys_t2244393468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[7] = 
{
	MoveByKeys_t2244393468::get_offset_of_Speed_3(),
	MoveByKeys_t2244393468::get_offset_of_JumpForce_4(),
	MoveByKeys_t2244393468::get_offset_of_JumpTimeout_5(),
	MoveByKeys_t2244393468::get_offset_of_isSprite_6(),
	MoveByKeys_t2244393468::get_offset_of_jumpingTime_7(),
	MoveByKeys_t2244393468::get_offset_of_body_8(),
	MoveByKeys_t2244393468::get_offset_of_body2d_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (NetworkCullingHandler_t3621072727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[7] = 
{
	NetworkCullingHandler_t3621072727::get_offset_of_orderIndex_2(),
	NetworkCullingHandler_t3621072727::get_offset_of_cullArea_3(),
	NetworkCullingHandler_t3621072727::get_offset_of_previousActiveCells_4(),
	NetworkCullingHandler_t3621072727::get_offset_of_activeCells_5(),
	NetworkCullingHandler_t3621072727::get_offset_of_pView_6(),
	NetworkCullingHandler_t3621072727::get_offset_of_lastPosition_7(),
	NetworkCullingHandler_t3621072727::get_offset_of_currentPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (OnClickDestroy_t2392776543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[1] = 
{
	OnClickDestroy_t2392776543::get_offset_of_DestroyByRpc_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (U3CDestroyRpcU3Ec__Iterator0_t2038103911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[4] = 
{
	U3CDestroyRpcU3Ec__Iterator0_t2038103911::get_offset_of_U24this_0(),
	U3CDestroyRpcU3Ec__Iterator0_t2038103911::get_offset_of_U24current_1(),
	U3CDestroyRpcU3Ec__Iterator0_t2038103911::get_offset_of_U24disposing_2(),
	U3CDestroyRpcU3Ec__Iterator0_t2038103911::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (OnClickInstantiate_t646526299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[4] = 
{
	OnClickInstantiate_t646526299::get_offset_of_Prefab_2(),
	OnClickInstantiate_t646526299::get_offset_of_InstantiateType_3(),
	OnClickInstantiate_t646526299::get_offset_of_InstantiateTypeNames_4(),
	OnClickInstantiate_t646526299::get_offset_of_showGui_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (OnJoinedInstantiate_t3153042345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[3] = 
{
	OnJoinedInstantiate_t3153042345::get_offset_of_SpawnPosition_2(),
	OnJoinedInstantiate_t3153042345::get_offset_of_m_cam_3(),
	OnJoinedInstantiate_t3153042345::get_offset_of_m_controller_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
