﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.Dictionary`2<System.Type,GvrEventExecutor/EventDelegate>
struct Dictionary_2_t2925553320;
// System.String
struct String_t;
// KeyboardState
struct KeyboardState_t4109162649;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// GvrHeadset
struct GvrHeadset_t1874684537;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,GvrPointerScrollInput/ScrollInfo>
struct Dictionary_2_t3565263950;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// GvrKeyboard
struct GvrKeyboard_t2536560201;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<IGvrPointerHoverHandler>
struct EventFunction_1_t2876286272;
// System.Void
struct Void_t1185182177;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// proto.PhoneEvent/Types/KeyEvent
struct KeyEvent_t1937114521;
// IGvrInputModuleController
struct IGvrInputModuleController_t3115474459;
// IGvrEventExecutor
struct IGvrEventExecutor_t2029948192;
// GvrPointerScrollInput
struct GvrPointerScrollInput_t3738414627;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// GvrBasePointer
struct GvrBasePointer_t822782720;
// proto.PhoneEvent
struct PhoneEvent_t1358418708;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// IGvrScrollSettings
struct IGvrScrollSettings_t3099753915;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t32045322;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t3225273728;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// GvrKeyboard/KeyboardCallback
struct KeyboardCallback_t3330588312;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// GvrBaseArmModel
struct GvrBaseArmModel_t3382200842;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// UnityEngine.Object
struct Object_t631007953;
// Gvr.Internal.IHeadsetProvider
struct IHeadsetProvider_t3991216071;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1314943911;
// GvrHeadset/OnSafetyRegionEvent
struct OnSafetyRegionEvent_t980662704;
// GvrHeadset/OnRecenterEvent
struct OnRecenterEvent_t1755824842;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// GvrAllEventsTrigger/TriggerEvent
struct TriggerEvent_t1225966107;
// Gvr.Internal.IKeyboardProvider
struct IKeyboardProvider_t1978250355;
// GvrKeyboard/ErrorCallback
struct ErrorCallback_t2310212740;
// GvrKeyboard/StandardCallback
struct StandardCallback_t3095007891;
// GvrKeyboard/EditTextCallback
struct EditTextCallback_t1702213000;
// System.Collections.Generic.List`1<GvrKeyboardEvent>
struct List_1_t806272884;
// GvrKeyboardDelegateBase
struct GvrKeyboardDelegateBase_t30895224;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// System.Collections.Generic.List`1<System.Action`1<System.Int32>>
struct List_1_t300520794;
// System.Collections.Generic.List`1<System.Action`2<System.String,System.String>>
struct List_1_t1147278120;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t1110636971;
// System.Action`2<System.String,System.String>
struct Action_2_t3970170674;
// GvrVideoPlayerTexture/OnVideoEventCallback
struct OnVideoEventCallback_t2376626694;
// GvrVideoPlayerTexture/OnExceptionCallback
struct OnExceptionCallback_t1696428116;
// UnityEngine.Camera
struct Camera_t4157153871;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// GvrPointerInputModuleImpl
struct GvrPointerInputModuleImpl_t2260544298;
// GvrEventExecutor
struct GvrEventExecutor_t2551165091;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t1438173104;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t4040729994;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t2924027637;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t3520241082;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t3270282352;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// GvrPointerPhysicsRaycaster/HitComparer
struct HitComparer_t1984117280;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t3132410353;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t1435266790;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef GVRCURSORHELPER_T4026897861_H
#define GVRCURSORHELPER_T4026897861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.GvrCursorHelper
struct  GvrCursorHelper_t4026897861  : public RuntimeObject
{
public:

public:
};

struct GvrCursorHelper_t4026897861_StaticFields
{
public:
	// System.Boolean Gvr.Internal.GvrCursorHelper::cachedHeadEmulationActive
	bool ___cachedHeadEmulationActive_0;
	// System.Boolean Gvr.Internal.GvrCursorHelper::cachedControllerEmulationActive
	bool ___cachedControllerEmulationActive_1;

public:
	inline static int32_t get_offset_of_cachedHeadEmulationActive_0() { return static_cast<int32_t>(offsetof(GvrCursorHelper_t4026897861_StaticFields, ___cachedHeadEmulationActive_0)); }
	inline bool get_cachedHeadEmulationActive_0() const { return ___cachedHeadEmulationActive_0; }
	inline bool* get_address_of_cachedHeadEmulationActive_0() { return &___cachedHeadEmulationActive_0; }
	inline void set_cachedHeadEmulationActive_0(bool value)
	{
		___cachedHeadEmulationActive_0 = value;
	}

	inline static int32_t get_offset_of_cachedControllerEmulationActive_1() { return static_cast<int32_t>(offsetof(GvrCursorHelper_t4026897861_StaticFields, ___cachedControllerEmulationActive_1)); }
	inline bool get_cachedControllerEmulationActive_1() const { return ___cachedControllerEmulationActive_1; }
	inline bool* get_address_of_cachedControllerEmulationActive_1() { return &___cachedControllerEmulationActive_1; }
	inline void set_cachedControllerEmulationActive_1(bool value)
	{
		___cachedControllerEmulationActive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCURSORHELPER_T4026897861_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef GVREVENTEXECUTOR_T2551165091_H
#define GVREVENTEXECUTOR_T2551165091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventExecutor
struct  GvrEventExecutor_t2551165091  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,GvrEventExecutor/EventDelegate> GvrEventExecutor::eventTable
	Dictionary_2_t2925553320 * ___eventTable_0;

public:
	inline static int32_t get_offset_of_eventTable_0() { return static_cast<int32_t>(offsetof(GvrEventExecutor_t2551165091, ___eventTable_0)); }
	inline Dictionary_2_t2925553320 * get_eventTable_0() const { return ___eventTable_0; }
	inline Dictionary_2_t2925553320 ** get_address_of_eventTable_0() { return &___eventTable_0; }
	inline void set_eventTable_0(Dictionary_2_t2925553320 * value)
	{
		___eventTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREVENTEXECUTOR_T2551165091_H
#ifndef KEYBOARDPROVIDERFACTORY_T3069358895_H
#define KEYBOARDPROVIDERFACTORY_T3069358895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.KeyboardProviderFactory
struct  KeyboardProviderFactory_t3069358895  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDPROVIDERFACTORY_T3069358895_H
#ifndef GVRACTIVITYHELPER_T700161863_H
#define GVRACTIVITYHELPER_T700161863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrActivityHelper
struct  GvrActivityHelper_t700161863  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRACTIVITYHELPER_T700161863_H
#ifndef DUMMYKEYBOARDPROVIDER_T4235634217_H
#define DUMMYKEYBOARDPROVIDER_T4235634217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.DummyKeyboardProvider
struct  DummyKeyboardProvider_t4235634217  : public RuntimeObject
{
public:
	// KeyboardState Gvr.Internal.DummyKeyboardProvider::dummyState
	KeyboardState_t4109162649 * ___dummyState_0;
	// System.String Gvr.Internal.DummyKeyboardProvider::<EditorText>k__BackingField
	String_t* ___U3CEditorTextU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(DummyKeyboardProvider_t4235634217, ___dummyState_0)); }
	inline KeyboardState_t4109162649 * get_dummyState_0() const { return ___dummyState_0; }
	inline KeyboardState_t4109162649 ** get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(KeyboardState_t4109162649 * value)
	{
		___dummyState_0 = value;
		Il2CppCodeGenWriteBarrier((&___dummyState_0), value);
	}

	inline static int32_t get_offset_of_U3CEditorTextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DummyKeyboardProvider_t4235634217, ___U3CEditorTextU3Ek__BackingField_1)); }
	inline String_t* get_U3CEditorTextU3Ek__BackingField_1() const { return ___U3CEditorTextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CEditorTextU3Ek__BackingField_1() { return &___U3CEditorTextU3Ek__BackingField_1; }
	inline void set_U3CEditorTextU3Ek__BackingField_1(String_t* value)
	{
		___U3CEditorTextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEditorTextU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYKEYBOARDPROVIDER_T4235634217_H
#ifndef GVRDAYDREAMAPI_T820520409_H
#define GVRDAYDREAMAPI_T820520409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrDaydreamApi
struct  GvrDaydreamApi_t820520409  : public RuntimeObject
{
public:

public:
};

struct GvrDaydreamApi_t820520409_StaticFields
{
public:
	// GvrDaydreamApi GvrDaydreamApi::m_instance
	GvrDaydreamApi_t820520409 * ___m_instance_4;

public:
	inline static int32_t get_offset_of_m_instance_4() { return static_cast<int32_t>(offsetof(GvrDaydreamApi_t820520409_StaticFields, ___m_instance_4)); }
	inline GvrDaydreamApi_t820520409 * get_m_instance_4() const { return ___m_instance_4; }
	inline GvrDaydreamApi_t820520409 ** get_address_of_m_instance_4() { return &___m_instance_4; }
	inline void set_m_instance_4(GvrDaydreamApi_t820520409 * value)
	{
		___m_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRDAYDREAMAPI_T820520409_H
#ifndef GVRUNITYSDKVERSION_T2768813923_H
#define GVRUNITYSDKVERSION_T2768813923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrUnitySdkVersion
struct  GvrUnitySdkVersion_t2768813923  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRUNITYSDKVERSION_T2768813923_H
#ifndef GVRINTENT_T255451010_H
#define GVRINTENT_T255451010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrIntent
struct  GvrIntent_t255451010  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRINTENT_T255451010_H
#ifndef U3CLAUNCHVRHOMEASYNCU3EC__ANONSTOREY0_T1042273844_H
#define U3CLAUNCHVRHOMEASYNCU3EC__ANONSTOREY0_T1042273844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrDaydreamApi/<LaunchVrHomeAsync>c__AnonStorey0
struct  U3CLaunchVrHomeAsyncU3Ec__AnonStorey0_t1042273844  : public RuntimeObject
{
public:
	// System.Action`1<System.Boolean> GvrDaydreamApi/<LaunchVrHomeAsync>c__AnonStorey0::callback
	Action_1_t269755560 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CLaunchVrHomeAsyncU3Ec__AnonStorey0_t1042273844, ___callback_0)); }
	inline Action_1_t269755560 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t269755560 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t269755560 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLAUNCHVRHOMEASYNCU3EC__ANONSTOREY0_T1042273844_H
#ifndef GVRSETTINGS_T2768269135_H
#define GVRSETTINGS_T2768269135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings
struct  GvrSettings_t2768269135  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRSETTINGS_T2768269135_H
#ifndef U3CENDOFFRAMEU3EC__ITERATOR0_T1803228010_H
#define U3CENDOFFRAMEU3EC__ITERATOR0_T1803228010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset/<EndOfFrame>c__Iterator0
struct  U3CEndOfFrameU3Ec__Iterator0_t1803228010  : public RuntimeObject
{
public:
	// GvrHeadset GvrHeadset/<EndOfFrame>c__Iterator0::$this
	GvrHeadset_t1874684537 * ___U24this_0;
	// System.Object GvrHeadset/<EndOfFrame>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GvrHeadset/<EndOfFrame>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GvrHeadset/<EndOfFrame>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t1803228010, ___U24this_0)); }
	inline GvrHeadset_t1874684537 * get_U24this_0() const { return ___U24this_0; }
	inline GvrHeadset_t1874684537 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GvrHeadset_t1874684537 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t1803228010, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t1803228010, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t1803228010, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENDOFFRAMEU3EC__ITERATOR0_T1803228010_H
#ifndef GVRCARDBOARDHELPERS_T3063197799_H
#define GVRCARDBOARDHELPERS_T3063197799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrCardboardHelpers
struct  GvrCardboardHelpers_t3063197799  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCARDBOARDHELPERS_T3063197799_H
#ifndef GVRPOINTERSCROLLINPUT_T3738414627_H
#define GVRPOINTERSCROLLINPUT_T3738414627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerScrollInput
struct  GvrPointerScrollInput_t3738414627  : public RuntimeObject
{
public:
	// System.Boolean GvrPointerScrollInput::inertia
	bool ___inertia_2;
	// System.Single GvrPointerScrollInput::decelerationRate
	float ___decelerationRate_3;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,GvrPointerScrollInput/ScrollInfo> GvrPointerScrollInput::scrollHandlers
	Dictionary_2_t3565263950 * ___scrollHandlers_15;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GvrPointerScrollInput::scrollingObjects
	List_1_t2585711361 * ___scrollingObjects_16;

public:
	inline static int32_t get_offset_of_inertia_2() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_t3738414627, ___inertia_2)); }
	inline bool get_inertia_2() const { return ___inertia_2; }
	inline bool* get_address_of_inertia_2() { return &___inertia_2; }
	inline void set_inertia_2(bool value)
	{
		___inertia_2 = value;
	}

	inline static int32_t get_offset_of_decelerationRate_3() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_t3738414627, ___decelerationRate_3)); }
	inline float get_decelerationRate_3() const { return ___decelerationRate_3; }
	inline float* get_address_of_decelerationRate_3() { return &___decelerationRate_3; }
	inline void set_decelerationRate_3(float value)
	{
		___decelerationRate_3 = value;
	}

	inline static int32_t get_offset_of_scrollHandlers_15() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_t3738414627, ___scrollHandlers_15)); }
	inline Dictionary_2_t3565263950 * get_scrollHandlers_15() const { return ___scrollHandlers_15; }
	inline Dictionary_2_t3565263950 ** get_address_of_scrollHandlers_15() { return &___scrollHandlers_15; }
	inline void set_scrollHandlers_15(Dictionary_2_t3565263950 * value)
	{
		___scrollHandlers_15 = value;
		Il2CppCodeGenWriteBarrier((&___scrollHandlers_15), value);
	}

	inline static int32_t get_offset_of_scrollingObjects_16() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_t3738414627, ___scrollingObjects_16)); }
	inline List_1_t2585711361 * get_scrollingObjects_16() const { return ___scrollingObjects_16; }
	inline List_1_t2585711361 ** get_address_of_scrollingObjects_16() { return &___scrollingObjects_16; }
	inline void set_scrollingObjects_16(List_1_t2585711361 * value)
	{
		___scrollingObjects_16 = value;
		Il2CppCodeGenWriteBarrier((&___scrollingObjects_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERSCROLLINPUT_T3738414627_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef ABSTRACTBUILDERLITE_2_T1175377521_H
#define ABSTRACTBUILDERLITE_2_T1175377521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent,proto.PhoneEvent/Builder>
struct  AbstractBuilderLite_2_t1175377521  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T1175377521_H
#ifndef ABSTRACTBUILDERLITE_2_T4063219799_H
#define ABSTRACTBUILDERLITE_2_T4063219799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  AbstractBuilderLite_2_t4063219799  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T4063219799_H
#ifndef U3CEXECUTERU3EC__ITERATOR0_T892308174_H
#define U3CEXECUTERU3EC__ITERATOR0_T892308174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/<Executer>c__Iterator0
struct  U3CExecuterU3Ec__Iterator0_t892308174  : public RuntimeObject
{
public:
	// GvrKeyboard GvrKeyboard/<Executer>c__Iterator0::$this
	GvrKeyboard_t2536560201 * ___U24this_0;
	// System.Object GvrKeyboard/<Executer>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GvrKeyboard/<Executer>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GvrKeyboard/<Executer>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ec__Iterator0_t892308174, ___U24this_0)); }
	inline GvrKeyboard_t2536560201 * get_U24this_0() const { return ___U24this_0; }
	inline GvrKeyboard_t2536560201 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GvrKeyboard_t2536560201 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ec__Iterator0_t892308174, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ec__Iterator0_t892308174, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ec__Iterator0_t892308174, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTERU3EC__ITERATOR0_T892308174_H
#ifndef GVRKEYBOARDINTENT_T3874861606_H
#define GVRKEYBOARDINTENT_T3874861606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardIntent
struct  GvrKeyboardIntent_t3874861606  : public RuntimeObject
{
public:

public:
};

struct GvrKeyboardIntent_t3874861606_StaticFields
{
public:
	// GvrKeyboardIntent GvrKeyboardIntent::theInstance
	GvrKeyboardIntent_t3874861606 * ___theInstance_2;

public:
	inline static int32_t get_offset_of_theInstance_2() { return static_cast<int32_t>(offsetof(GvrKeyboardIntent_t3874861606_StaticFields, ___theInstance_2)); }
	inline GvrKeyboardIntent_t3874861606 * get_theInstance_2() const { return ___theInstance_2; }
	inline GvrKeyboardIntent_t3874861606 ** get_address_of_theInstance_2() { return &___theInstance_2; }
	inline void set_theInstance_2(GvrKeyboardIntent_t3874861606 * value)
	{
		___theInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___theInstance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDINTENT_T3874861606_H
#ifndef HITCOMPARER_T1984117280_H
#define HITCOMPARER_T1984117280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerPhysicsRaycaster/HitComparer
struct  HitComparer_t1984117280  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITCOMPARER_T1984117280_H
#ifndef HEADSETPROVIDERFACTORY_T520764709_H
#define HEADSETPROVIDERFACTORY_T520764709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.HeadsetProviderFactory
struct  HeadsetProviderFactory_t520764709  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETPROVIDERFACTORY_T520764709_H
#ifndef GVRUIHELPERS_T853958893_H
#define GVRUIHELPERS_T853958893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrUIHelpers
struct  GvrUIHelpers_t853958893  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRUIHELPERS_T853958893_H
#ifndef GVREXECUTEEVENTSEXTENSION_T3940832397_H
#define GVREXECUTEEVENTSEXTENSION_T3940832397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrExecuteEventsExtension
struct  GvrExecuteEventsExtension_t3940832397  : public RuntimeObject
{
public:

public:
};

struct GvrExecuteEventsExtension_t3940832397_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<IGvrPointerHoverHandler> GvrExecuteEventsExtension::s_HoverHandler
	EventFunction_1_t2876286272 * ___s_HoverHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<IGvrPointerHoverHandler> GvrExecuteEventsExtension::<>f__mg$cache0
	EventFunction_1_t2876286272 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_HoverHandler_0() { return static_cast<int32_t>(offsetof(GvrExecuteEventsExtension_t3940832397_StaticFields, ___s_HoverHandler_0)); }
	inline EventFunction_1_t2876286272 * get_s_HoverHandler_0() const { return ___s_HoverHandler_0; }
	inline EventFunction_1_t2876286272 ** get_address_of_s_HoverHandler_0() { return &___s_HoverHandler_0; }
	inline void set_s_HoverHandler_0(EventFunction_1_t2876286272 * value)
	{
		___s_HoverHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_HoverHandler_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(GvrExecuteEventsExtension_t3940832397_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline EventFunction_1_t2876286272 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline EventFunction_1_t2876286272 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(EventFunction_1_t2876286272 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREXECUTEEVENTSEXTENSION_T3940832397_H
#ifndef UNITYRECT_T2898233164_H
#define UNITYRECT_T2898233164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/UnityRect
struct  UnityRect_t2898233164 
{
public:
	// System.Single Gvr.Internal.InstantPreview/UnityRect::right
	float ___right_0;
	// System.Single Gvr.Internal.InstantPreview/UnityRect::left
	float ___left_1;
	// System.Single Gvr.Internal.InstantPreview/UnityRect::top
	float ___top_2;
	// System.Single Gvr.Internal.InstantPreview/UnityRect::bottom
	float ___bottom_3;

public:
	inline static int32_t get_offset_of_right_0() { return static_cast<int32_t>(offsetof(UnityRect_t2898233164, ___right_0)); }
	inline float get_right_0() const { return ___right_0; }
	inline float* get_address_of_right_0() { return &___right_0; }
	inline void set_right_0(float value)
	{
		___right_0 = value;
	}

	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(UnityRect_t2898233164, ___left_1)); }
	inline float get_left_1() const { return ___left_1; }
	inline float* get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(float value)
	{
		___left_1 = value;
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(UnityRect_t2898233164, ___top_2)); }
	inline float get_top_2() const { return ___top_2; }
	inline float* get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(float value)
	{
		___top_2 = value;
	}

	inline static int32_t get_offset_of_bottom_3() { return static_cast<int32_t>(offsetof(UnityRect_t2898233164, ___bottom_3)); }
	inline float get_bottom_3() const { return ___bottom_3; }
	inline float* get_address_of_bottom_3() { return &___bottom_3; }
	inline void set_bottom_3(float value)
	{
		___bottom_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYRECT_T2898233164_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef GVR_RECTI_T2249612514_H
#define GVR_RECTI_T2249612514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti
struct  gvr_recti_t2249612514 
{
public:
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti::left
	int32_t ___left_0;
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti::right
	int32_t ___right_1;
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti::bottom
	int32_t ___bottom_2;
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti::top
	int32_t ___top_3;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(gvr_recti_t2249612514, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(gvr_recti_t2249612514, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}

	inline static int32_t get_offset_of_bottom_2() { return static_cast<int32_t>(offsetof(gvr_recti_t2249612514, ___bottom_2)); }
	inline int32_t get_bottom_2() const { return ___bottom_2; }
	inline int32_t* get_address_of_bottom_2() { return &___bottom_2; }
	inline void set_bottom_2(int32_t value)
	{
		___bottom_2 = value;
	}

	inline static int32_t get_offset_of_top_3() { return static_cast<int32_t>(offsetof(gvr_recti_t2249612514, ___top_3)); }
	inline int32_t get_top_3() const { return ___top_3; }
	inline int32_t* get_address_of_top_3() { return &___top_3; }
	inline void set_top_3(int32_t value)
	{
		___top_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_RECTI_T2249612514_H
#ifndef RESOLUTIONSIZE_T3677657137_H
#define RESOLUTIONSIZE_T3677657137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/ResolutionSize
struct  ResolutionSize_t3677657137 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview/ResolutionSize::width
	int32_t ___width_0;
	// System.Int32 Gvr.Internal.InstantPreview/ResolutionSize::height
	int32_t ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ResolutionSize_t3677657137, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ResolutionSize_t3677657137, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTIONSIZE_T3677657137_H
#ifndef GVR_CLOCK_TIME_POINT_T2797008802_H
#define GVR_CLOCK_TIME_POINT_T2797008802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AndroidNativeKeyboardProvider/gvr_clock_time_point
struct  gvr_clock_time_point_t2797008802 
{
public:
	// System.Int64 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_clock_time_point::monotonic_system_time_nanos
	int64_t ___monotonic_system_time_nanos_0;

public:
	inline static int32_t get_offset_of_monotonic_system_time_nanos_0() { return static_cast<int32_t>(offsetof(gvr_clock_time_point_t2797008802, ___monotonic_system_time_nanos_0)); }
	inline int64_t get_monotonic_system_time_nanos_0() const { return ___monotonic_system_time_nanos_0; }
	inline int64_t* get_address_of_monotonic_system_time_nanos_0() { return &___monotonic_system_time_nanos_0; }
	inline void set_monotonic_system_time_nanos_0(int64_t value)
	{
		___monotonic_system_time_nanos_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_CLOCK_TIME_POINT_T2797008802_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef GENERATEDBUILDERLITE_2_T3171452996_H
#define GENERATEDBUILDERLITE_2_T3171452996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  GeneratedBuilderLite_2_t3171452996  : public AbstractBuilderLite_2_t4063219799
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T3171452996_H
#ifndef GENERATEDBUILDERLITE_2_T283610718_H
#define GENERATEDBUILDERLITE_2_T283610718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent,proto.PhoneEvent/Builder>
struct  GeneratedBuilderLite_2_t283610718  : public AbstractBuilderLite_2_t1175377521
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T283610718_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef UNITYEVENT_2_T1826723146_H
#define UNITYEVENT_2_T1826723146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData>
struct  UnityEvent_2_t1826723146  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1826723146, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1826723146_H
#ifndef VIDEOEVENTS_T3555787859_H
#define VIDEOEVENTS_T3555787859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/VideoEvents
struct  VideoEvents_t3555787859 
{
public:
	// System.Int32 GvrVideoPlayerTexture/VideoEvents::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoEvents_t3555787859, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOEVENTS_T3555787859_H
#ifndef VIDEOPLAYERSTATE_T3323603301_H
#define VIDEOPLAYERSTATE_T3323603301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/VideoPlayerState
struct  VideoPlayerState_t3323603301 
{
public:
	// System.Int32 GvrVideoPlayerTexture/VideoPlayerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoPlayerState_t3323603301, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYERSTATE_T3323603301_H
#ifndef BUILDER_T2712992173_H
#define BUILDER_T2712992173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/KeyEvent/Builder
struct  Builder_t2712992173  : public GeneratedBuilderLite_2_t3171452996
{
public:
	// System.Boolean proto.PhoneEvent/Types/KeyEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent/Builder::result
	KeyEvent_t1937114521 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t2712992173, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t2712992173, ___result_1)); }
	inline KeyEvent_t1937114521 * get_result_1() const { return ___result_1; }
	inline KeyEvent_t1937114521 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(KeyEvent_t1937114521 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2712992173_H
#ifndef STEREOMODE_T1039127149_H
#define STEREOMODE_T1039127149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/StereoMode
struct  StereoMode_t1039127149 
{
public:
	// System.Int32 GvrVideoPlayerTexture/StereoMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StereoMode_t1039127149, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOMODE_T1039127149_H
#ifndef RENDERCOMMAND_T1121160834_H
#define RENDERCOMMAND_T1121160834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/RenderCommand
struct  RenderCommand_t1121160834 
{
public:
	// System.Int32 GvrVideoPlayerTexture/RenderCommand::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderCommand_t1121160834, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERCOMMAND_T1121160834_H
#ifndef USERPREFSHANDEDNESS_T3496962503_H
#define USERPREFSHANDEDNESS_T3496962503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings/UserPrefsHandedness
struct  UserPrefsHandedness_t3496962503 
{
public:
	// System.Int32 GvrSettings/UserPrefsHandedness::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UserPrefsHandedness_t3496962503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPREFSHANDEDNESS_T3496962503_H
#ifndef VIEWERPLATFORMTYPE_T1613185256_H
#define VIEWERPLATFORMTYPE_T1613185256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings/ViewerPlatformType
struct  ViewerPlatformType_t1613185256 
{
public:
	// System.Int32 GvrSettings/ViewerPlatformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ViewerPlatformType_t1613185256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERPLATFORMTYPE_T1613185256_H
#ifndef VIDEORESOLUTION_T1062057780_H
#define VIDEORESOLUTION_T1062057780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/VideoResolution
struct  VideoResolution_t1062057780 
{
public:
	// System.Int32 GvrVideoPlayerTexture/VideoResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoResolution_t1062057780, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEORESOLUTION_T1062057780_H
#ifndef GVRINFO_T2187998870_H
#define GVRINFO_T2187998870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrInfo
struct  GvrInfo_t2187998870  : public PropertyAttribute_t3677895545
{
public:
	// System.String GvrInfo::text
	String_t* ___text_0;
	// System.Int32 GvrInfo::numLines
	int32_t ___numLines_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(GvrInfo_t2187998870, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_numLines_1() { return static_cast<int32_t>(offsetof(GvrInfo_t2187998870, ___numLines_1)); }
	inline int32_t get_numLines_1() const { return ___numLines_1; }
	inline int32_t* get_address_of_numLines_1() { return &___numLines_1; }
	inline void set_numLines_1(int32_t value)
	{
		___numLines_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRINFO_T2187998870_H
#ifndef GVRRECENTERFLAGS_T370890970_H
#define GVRRECENTERFLAGS_T370890970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterFlags
struct  GvrRecenterFlags_t370890970 
{
public:
	// System.Int32 GvrRecenterFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrRecenterFlags_t370890970, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTERFLAGS_T370890970_H
#ifndef GVRRECENTEREVENTTYPE_T513699134_H
#define GVRRECENTEREVENTTYPE_T513699134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterEventType
struct  GvrRecenterEventType_t513699134 
{
public:
	// System.Int32 GvrRecenterEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrRecenterEventType_t513699134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTEREVENTTYPE_T513699134_H
#ifndef GVREVENTTYPE_T1628138291_H
#define GVREVENTTYPE_T1628138291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventType
struct  GvrEventType_t1628138291 
{
public:
	// System.Int32 GvrEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrEventType_t1628138291, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREVENTTYPE_T1628138291_H
#ifndef GVRMATHHELPERS_T769385329_H
#define GVRMATHHELPERS_T769385329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrMathHelpers
struct  GvrMathHelpers_t769385329  : public RuntimeObject
{
public:

public:
};

struct GvrMathHelpers_t769385329_StaticFields
{
public:
	// UnityEngine.Vector2 GvrMathHelpers::sphericalCoordinatesResult
	Vector2_t2156229523  ___sphericalCoordinatesResult_0;

public:
	inline static int32_t get_offset_of_sphericalCoordinatesResult_0() { return static_cast<int32_t>(offsetof(GvrMathHelpers_t769385329_StaticFields, ___sphericalCoordinatesResult_0)); }
	inline Vector2_t2156229523  get_sphericalCoordinatesResult_0() const { return ___sphericalCoordinatesResult_0; }
	inline Vector2_t2156229523 * get_address_of_sphericalCoordinatesResult_0() { return &___sphericalCoordinatesResult_0; }
	inline void set_sphericalCoordinatesResult_0(Vector2_t2156229523  value)
	{
		___sphericalCoordinatesResult_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRMATHHELPERS_T769385329_H
#ifndef VIDEOTYPE_T2491562340_H
#define VIDEOTYPE_T2491562340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/VideoType
struct  VideoType_t2491562340 
{
public:
	// System.Int32 GvrVideoPlayerTexture/VideoType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoType_t2491562340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTYPE_T2491562340_H
#ifndef GVRPOINTERINPUTMODULEIMPL_T2260544298_H
#define GVRPOINTERINPUTMODULEIMPL_T2260544298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerInputModuleImpl
struct  GvrPointerInputModuleImpl_t2260544298  : public RuntimeObject
{
public:
	// IGvrInputModuleController GvrPointerInputModuleImpl::<ModuleController>k__BackingField
	RuntimeObject* ___U3CModuleControllerU3Ek__BackingField_0;
	// IGvrEventExecutor GvrPointerInputModuleImpl::<EventExecutor>k__BackingField
	RuntimeObject* ___U3CEventExecutorU3Ek__BackingField_1;
	// System.Boolean GvrPointerInputModuleImpl::<VrModeOnly>k__BackingField
	bool ___U3CVrModeOnlyU3Ek__BackingField_2;
	// GvrPointerScrollInput GvrPointerInputModuleImpl::<ScrollInput>k__BackingField
	GvrPointerScrollInput_t3738414627 * ___U3CScrollInputU3Ek__BackingField_3;
	// UnityEngine.EventSystems.PointerEventData GvrPointerInputModuleImpl::<CurrentEventData>k__BackingField
	PointerEventData_t3807901092 * ___U3CCurrentEventDataU3Ek__BackingField_4;
	// GvrBasePointer GvrPointerInputModuleImpl::pointer
	GvrBasePointer_t822782720 * ___pointer_5;
	// UnityEngine.Vector2 GvrPointerInputModuleImpl::lastPose
	Vector2_t2156229523  ___lastPose_6;
	// System.Boolean GvrPointerInputModuleImpl::isPointerHovering
	bool ___isPointerHovering_7;
	// System.Boolean GvrPointerInputModuleImpl::isActive
	bool ___isActive_8;

public:
	inline static int32_t get_offset_of_U3CModuleControllerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CModuleControllerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CModuleControllerU3Ek__BackingField_0() const { return ___U3CModuleControllerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CModuleControllerU3Ek__BackingField_0() { return &___U3CModuleControllerU3Ek__BackingField_0; }
	inline void set_U3CModuleControllerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CModuleControllerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CModuleControllerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CEventExecutorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CEventExecutorU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CEventExecutorU3Ek__BackingField_1() const { return ___U3CEventExecutorU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CEventExecutorU3Ek__BackingField_1() { return &___U3CEventExecutorU3Ek__BackingField_1; }
	inline void set_U3CEventExecutorU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CEventExecutorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventExecutorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CVrModeOnlyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CVrModeOnlyU3Ek__BackingField_2)); }
	inline bool get_U3CVrModeOnlyU3Ek__BackingField_2() const { return ___U3CVrModeOnlyU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CVrModeOnlyU3Ek__BackingField_2() { return &___U3CVrModeOnlyU3Ek__BackingField_2; }
	inline void set_U3CVrModeOnlyU3Ek__BackingField_2(bool value)
	{
		___U3CVrModeOnlyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CScrollInputU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CScrollInputU3Ek__BackingField_3)); }
	inline GvrPointerScrollInput_t3738414627 * get_U3CScrollInputU3Ek__BackingField_3() const { return ___U3CScrollInputU3Ek__BackingField_3; }
	inline GvrPointerScrollInput_t3738414627 ** get_address_of_U3CScrollInputU3Ek__BackingField_3() { return &___U3CScrollInputU3Ek__BackingField_3; }
	inline void set_U3CScrollInputU3Ek__BackingField_3(GvrPointerScrollInput_t3738414627 * value)
	{
		___U3CScrollInputU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScrollInputU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCurrentEventDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CCurrentEventDataU3Ek__BackingField_4)); }
	inline PointerEventData_t3807901092 * get_U3CCurrentEventDataU3Ek__BackingField_4() const { return ___U3CCurrentEventDataU3Ek__BackingField_4; }
	inline PointerEventData_t3807901092 ** get_address_of_U3CCurrentEventDataU3Ek__BackingField_4() { return &___U3CCurrentEventDataU3Ek__BackingField_4; }
	inline void set_U3CCurrentEventDataU3Ek__BackingField_4(PointerEventData_t3807901092 * value)
	{
		___U3CCurrentEventDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentEventDataU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_pointer_5() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___pointer_5)); }
	inline GvrBasePointer_t822782720 * get_pointer_5() const { return ___pointer_5; }
	inline GvrBasePointer_t822782720 ** get_address_of_pointer_5() { return &___pointer_5; }
	inline void set_pointer_5(GvrBasePointer_t822782720 * value)
	{
		___pointer_5 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_5), value);
	}

	inline static int32_t get_offset_of_lastPose_6() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___lastPose_6)); }
	inline Vector2_t2156229523  get_lastPose_6() const { return ___lastPose_6; }
	inline Vector2_t2156229523 * get_address_of_lastPose_6() { return &___lastPose_6; }
	inline void set_lastPose_6(Vector2_t2156229523  value)
	{
		___lastPose_6 = value;
	}

	inline static int32_t get_offset_of_isPointerHovering_7() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___isPointerHovering_7)); }
	inline bool get_isPointerHovering_7() const { return ___isPointerHovering_7; }
	inline bool* get_address_of_isPointerHovering_7() { return &___isPointerHovering_7; }
	inline void set_isPointerHovering_7(bool value)
	{
		___isPointerHovering_7 = value;
	}

	inline static int32_t get_offset_of_isActive_8() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___isActive_8)); }
	inline bool get_isActive_8() const { return ___isActive_8; }
	inline bool* get_address_of_isActive_8() { return &___isActive_8; }
	inline void set_isActive_8(bool value)
	{
		___isActive_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERINPUTMODULEIMPL_T2260544298_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef BUILDER_T895705486_H
#define BUILDER_T895705486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Builder
struct  Builder_t895705486  : public GeneratedBuilderLite_2_t283610718
{
public:
	// System.Boolean proto.PhoneEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent proto.PhoneEvent/Builder::result
	PhoneEvent_t1358418708 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t895705486, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t895705486, ___result_1)); }
	inline PhoneEvent_t1358418708 * get_result_1() const { return ___result_1; }
	inline PhoneEvent_t1358418708 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(PhoneEvent_t1358418708 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T895705486_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef TRIGGEREVENT_T1225966107_H
#define TRIGGEREVENT_T1225966107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAllEventsTrigger/TriggerEvent
struct  TriggerEvent_t1225966107  : public UnityEvent_2_t1826723146
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_T1225966107_H
#ifndef SCROLLINFO_T1733197845_H
#define SCROLLINFO_T1733197845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerScrollInput/ScrollInfo
struct  ScrollInfo_t1733197845  : public RuntimeObject
{
public:
	// System.Boolean GvrPointerScrollInput/ScrollInfo::isScrollingX
	bool ___isScrollingX_0;
	// System.Boolean GvrPointerScrollInput/ScrollInfo::isScrollingY
	bool ___isScrollingY_1;
	// UnityEngine.Vector2 GvrPointerScrollInput/ScrollInfo::initScroll
	Vector2_t2156229523  ___initScroll_2;
	// UnityEngine.Vector2 GvrPointerScrollInput/ScrollInfo::lastScroll
	Vector2_t2156229523  ___lastScroll_3;
	// UnityEngine.Vector2 GvrPointerScrollInput/ScrollInfo::scrollVelocity
	Vector2_t2156229523  ___scrollVelocity_4;
	// IGvrScrollSettings GvrPointerScrollInput/ScrollInfo::scrollSettings
	RuntimeObject* ___scrollSettings_5;

public:
	inline static int32_t get_offset_of_isScrollingX_0() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___isScrollingX_0)); }
	inline bool get_isScrollingX_0() const { return ___isScrollingX_0; }
	inline bool* get_address_of_isScrollingX_0() { return &___isScrollingX_0; }
	inline void set_isScrollingX_0(bool value)
	{
		___isScrollingX_0 = value;
	}

	inline static int32_t get_offset_of_isScrollingY_1() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___isScrollingY_1)); }
	inline bool get_isScrollingY_1() const { return ___isScrollingY_1; }
	inline bool* get_address_of_isScrollingY_1() { return &___isScrollingY_1; }
	inline void set_isScrollingY_1(bool value)
	{
		___isScrollingY_1 = value;
	}

	inline static int32_t get_offset_of_initScroll_2() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___initScroll_2)); }
	inline Vector2_t2156229523  get_initScroll_2() const { return ___initScroll_2; }
	inline Vector2_t2156229523 * get_address_of_initScroll_2() { return &___initScroll_2; }
	inline void set_initScroll_2(Vector2_t2156229523  value)
	{
		___initScroll_2 = value;
	}

	inline static int32_t get_offset_of_lastScroll_3() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___lastScroll_3)); }
	inline Vector2_t2156229523  get_lastScroll_3() const { return ___lastScroll_3; }
	inline Vector2_t2156229523 * get_address_of_lastScroll_3() { return &___lastScroll_3; }
	inline void set_lastScroll_3(Vector2_t2156229523  value)
	{
		___lastScroll_3 = value;
	}

	inline static int32_t get_offset_of_scrollVelocity_4() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___scrollVelocity_4)); }
	inline Vector2_t2156229523  get_scrollVelocity_4() const { return ___scrollVelocity_4; }
	inline Vector2_t2156229523 * get_address_of_scrollVelocity_4() { return &___scrollVelocity_4; }
	inline void set_scrollVelocity_4(Vector2_t2156229523  value)
	{
		___scrollVelocity_4 = value;
	}

	inline static int32_t get_offset_of_scrollSettings_5() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___scrollSettings_5)); }
	inline RuntimeObject* get_scrollSettings_5() const { return ___scrollSettings_5; }
	inline RuntimeObject** get_address_of_scrollSettings_5() { return &___scrollSettings_5; }
	inline void set_scrollSettings_5(RuntimeObject* value)
	{
		___scrollSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___scrollSettings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLINFO_T1733197845_H
#ifndef BLOCKINGOBJECTS_T813457210_H
#define BLOCKINGOBJECTS_T813457210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerGraphicRaycaster/BlockingObjects
struct  BlockingObjects_t813457210 
{
public:
	// System.Int32 GvrPointerGraphicRaycaster/BlockingObjects::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockingObjects_t813457210, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKINGOBJECTS_T813457210_H
#ifndef ANDROIDJAVAPROXY_T2835824643_H
#define ANDROIDJAVAPROXY_T2835824643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_t2835824643  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.AndroidJavaProxy::javaInterface
	AndroidJavaClass_t32045322 * ___javaInterface_0;
	// UnityEngine.AndroidJavaObject UnityEngine.AndroidJavaProxy::proxyObject
	AndroidJavaObject_t4131667876 * ___proxyObject_1;

public:
	inline static int32_t get_offset_of_javaInterface_0() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643, ___javaInterface_0)); }
	inline AndroidJavaClass_t32045322 * get_javaInterface_0() const { return ___javaInterface_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_javaInterface_0() { return &___javaInterface_0; }
	inline void set_javaInterface_0(AndroidJavaClass_t32045322 * value)
	{
		___javaInterface_0 = value;
		Il2CppCodeGenWriteBarrier((&___javaInterface_0), value);
	}

	inline static int32_t get_offset_of_proxyObject_1() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643, ___proxyObject_1)); }
	inline AndroidJavaObject_t4131667876 * get_proxyObject_1() const { return ___proxyObject_1; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_proxyObject_1() { return &___proxyObject_1; }
	inline void set_proxyObject_1(AndroidJavaObject_t4131667876 * value)
	{
		___proxyObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___proxyObject_1), value);
	}
};

struct AndroidJavaProxy_t2835824643_StaticFields
{
public:
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaProxy::s_JavaLangSystemClass
	GlobalJavaObjectRef_t3225273728 * ___s_JavaLangSystemClass_2;
	// System.IntPtr UnityEngine.AndroidJavaProxy::s_HashCodeMethodID
	intptr_t ___s_HashCodeMethodID_3;

public:
	inline static int32_t get_offset_of_s_JavaLangSystemClass_2() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643_StaticFields, ___s_JavaLangSystemClass_2)); }
	inline GlobalJavaObjectRef_t3225273728 * get_s_JavaLangSystemClass_2() const { return ___s_JavaLangSystemClass_2; }
	inline GlobalJavaObjectRef_t3225273728 ** get_address_of_s_JavaLangSystemClass_2() { return &___s_JavaLangSystemClass_2; }
	inline void set_s_JavaLangSystemClass_2(GlobalJavaObjectRef_t3225273728 * value)
	{
		___s_JavaLangSystemClass_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_JavaLangSystemClass_2), value);
	}

	inline static int32_t get_offset_of_s_HashCodeMethodID_3() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643_StaticFields, ___s_HashCodeMethodID_3)); }
	inline intptr_t get_s_HashCodeMethodID_3() const { return ___s_HashCodeMethodID_3; }
	inline intptr_t* get_address_of_s_HashCodeMethodID_3() { return &___s_HashCodeMethodID_3; }
	inline void set_s_HashCodeMethodID_3(intptr_t value)
	{
		___s_HashCodeMethodID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_T2835824643_H
#ifndef RAYCASTMODE_T1421504155_H
#define RAYCASTMODE_T1421504155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer/RaycastMode
struct  RaycastMode_t1421504155 
{
public:
	// System.Int32 GvrBasePointer/RaycastMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RaycastMode_t1421504155, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTMODE_T1421504155_H
#ifndef LOCATION_T4049147969_H
#define LOCATION_T4049147969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrTooltip/Location
struct  Location_t4049147969 
{
public:
	// System.Int32 GvrTooltip/Location::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Location_t4049147969, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATION_T4049147969_H
#ifndef GVRKEYBOARDEVENT_T3629165438_H
#define GVRKEYBOARDEVENT_T3629165438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardEvent
struct  GvrKeyboardEvent_t3629165438 
{
public:
	// System.Int32 GvrKeyboardEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrKeyboardEvent_t3629165438, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDEVENT_T3629165438_H
#ifndef GVRSAFETYREGIONTYPE_T1943469016_H
#define GVRSAFETYREGIONTYPE_T1943469016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSafetyRegionType
struct  GvrSafetyRegionType_t1943469016 
{
public:
	// System.Int32 GvrSafetyRegionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrSafetyRegionType_t1943469016, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRSAFETYREGIONTYPE_T1943469016_H
#ifndef UNITYEYEVIEWS_T678228735_H
#define UNITYEYEVIEWS_T678228735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/UnityEyeViews
struct  UnityEyeViews_t678228735 
{
public:
	// UnityEngine.Matrix4x4 Gvr.Internal.InstantPreview/UnityEyeViews::leftEyePose
	Matrix4x4_t1817901843  ___leftEyePose_0;
	// UnityEngine.Matrix4x4 Gvr.Internal.InstantPreview/UnityEyeViews::rightEyePose
	Matrix4x4_t1817901843  ___rightEyePose_1;
	// Gvr.Internal.InstantPreview/UnityRect Gvr.Internal.InstantPreview/UnityEyeViews::leftEyeViewSize
	UnityRect_t2898233164  ___leftEyeViewSize_2;
	// Gvr.Internal.InstantPreview/UnityRect Gvr.Internal.InstantPreview/UnityEyeViews::rightEyeViewSize
	UnityRect_t2898233164  ___rightEyeViewSize_3;

public:
	inline static int32_t get_offset_of_leftEyePose_0() { return static_cast<int32_t>(offsetof(UnityEyeViews_t678228735, ___leftEyePose_0)); }
	inline Matrix4x4_t1817901843  get_leftEyePose_0() const { return ___leftEyePose_0; }
	inline Matrix4x4_t1817901843 * get_address_of_leftEyePose_0() { return &___leftEyePose_0; }
	inline void set_leftEyePose_0(Matrix4x4_t1817901843  value)
	{
		___leftEyePose_0 = value;
	}

	inline static int32_t get_offset_of_rightEyePose_1() { return static_cast<int32_t>(offsetof(UnityEyeViews_t678228735, ___rightEyePose_1)); }
	inline Matrix4x4_t1817901843  get_rightEyePose_1() const { return ___rightEyePose_1; }
	inline Matrix4x4_t1817901843 * get_address_of_rightEyePose_1() { return &___rightEyePose_1; }
	inline void set_rightEyePose_1(Matrix4x4_t1817901843  value)
	{
		___rightEyePose_1 = value;
	}

	inline static int32_t get_offset_of_leftEyeViewSize_2() { return static_cast<int32_t>(offsetof(UnityEyeViews_t678228735, ___leftEyeViewSize_2)); }
	inline UnityRect_t2898233164  get_leftEyeViewSize_2() const { return ___leftEyeViewSize_2; }
	inline UnityRect_t2898233164 * get_address_of_leftEyeViewSize_2() { return &___leftEyeViewSize_2; }
	inline void set_leftEyeViewSize_2(UnityRect_t2898233164  value)
	{
		___leftEyeViewSize_2 = value;
	}

	inline static int32_t get_offset_of_rightEyeViewSize_3() { return static_cast<int32_t>(offsetof(UnityEyeViews_t678228735, ___rightEyeViewSize_3)); }
	inline UnityRect_t2898233164  get_rightEyeViewSize_3() const { return ___rightEyeViewSize_3; }
	inline UnityRect_t2898233164 * get_address_of_rightEyeViewSize_3() { return &___rightEyeViewSize_3; }
	inline void set_rightEyeViewSize_3(UnityRect_t2898233164  value)
	{
		___rightEyeViewSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEYEVIEWS_T678228735_H
#ifndef GVR_FEATURE_T1054564930_H
#define GVR_FEATURE_T1054564930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_feature
struct  gvr_feature_t1054564930 
{
public:
	// System.Int32 Gvr.Internal.gvr_feature::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(gvr_feature_t1054564930, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_FEATURE_T1054564930_H
#ifndef GVRKEYBOARDINPUTMODE_T518947509_H
#define GVRKEYBOARDINPUTMODE_T518947509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardInputMode
struct  GvrKeyboardInputMode_t518947509 
{
public:
	// System.Int32 GvrKeyboardInputMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrKeyboardInputMode_t518947509, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDINPUTMODE_T518947509_H
#ifndef GVR_PROPERTY_TYPE_T1812416635_H
#define GVR_PROPERTY_TYPE_T1812416635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_property_type
struct  gvr_property_type_t1812416635 
{
public:
	// System.Int32 Gvr.Internal.gvr_property_type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(gvr_property_type_t1812416635, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_PROPERTY_TYPE_T1812416635_H
#ifndef GVRKEYBOARDERROR_T3210682397_H
#define GVRKEYBOARDERROR_T3210682397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardError
struct  GvrKeyboardError_t3210682397 
{
public:
	// System.Int32 GvrKeyboardError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrKeyboardError_t3210682397, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDERROR_T3210682397_H
#ifndef GVR_VALUE_TYPE_T2156477760_H
#define GVR_VALUE_TYPE_T2156477760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_value_type
struct  gvr_value_type_t2156477760 
{
public:
	// System.Int32 Gvr.Internal.gvr_value_type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(gvr_value_type_t2156477760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_VALUE_TYPE_T2156477760_H
#ifndef POSE3D_T2649470188_H
#define POSE3D_T2649470188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pose3D
struct  Pose3D_t2649470188  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Pose3D::<Position>k__BackingField
	Vector3_t3722313464  ___U3CPositionU3Ek__BackingField_1;
	// UnityEngine.Quaternion Pose3D::<Orientation>k__BackingField
	Quaternion_t2301928331  ___U3COrientationU3Ek__BackingField_2;
	// UnityEngine.Matrix4x4 Pose3D::<Matrix>k__BackingField
	Matrix4x4_t1817901843  ___U3CMatrixU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188, ___U3CPositionU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CPositionU3Ek__BackingField_1() const { return ___U3CPositionU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CPositionU3Ek__BackingField_1() { return &___U3CPositionU3Ek__BackingField_1; }
	inline void set_U3CPositionU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CPositionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COrientationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188, ___U3COrientationU3Ek__BackingField_2)); }
	inline Quaternion_t2301928331  get_U3COrientationU3Ek__BackingField_2() const { return ___U3COrientationU3Ek__BackingField_2; }
	inline Quaternion_t2301928331 * get_address_of_U3COrientationU3Ek__BackingField_2() { return &___U3COrientationU3Ek__BackingField_2; }
	inline void set_U3COrientationU3Ek__BackingField_2(Quaternion_t2301928331  value)
	{
		___U3COrientationU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMatrixU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188, ___U3CMatrixU3Ek__BackingField_3)); }
	inline Matrix4x4_t1817901843  get_U3CMatrixU3Ek__BackingField_3() const { return ___U3CMatrixU3Ek__BackingField_3; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CMatrixU3Ek__BackingField_3() { return &___U3CMatrixU3Ek__BackingField_3; }
	inline void set_U3CMatrixU3Ek__BackingField_3(Matrix4x4_t1817901843  value)
	{
		___U3CMatrixU3Ek__BackingField_3 = value;
	}
};

struct Pose3D_t2649470188_StaticFields
{
public:
	// UnityEngine.Matrix4x4 Pose3D::FLIP_Z
	Matrix4x4_t1817901843  ___FLIP_Z_0;

public:
	inline static int32_t get_offset_of_FLIP_Z_0() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188_StaticFields, ___FLIP_Z_0)); }
	inline Matrix4x4_t1817901843  get_FLIP_Z_0() const { return ___FLIP_Z_0; }
	inline Matrix4x4_t1817901843 * get_address_of_FLIP_Z_0() { return &___FLIP_Z_0; }
	inline void set_FLIP_Z_0(Matrix4x4_t1817901843  value)
	{
		___FLIP_Z_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE3D_T2649470188_H
#ifndef RESOLUTIONS_T3321708501_H
#define RESOLUTIONS_T3321708501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/Resolutions
struct  Resolutions_t3321708501 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview/Resolutions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Resolutions_t3321708501, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTIONS_T3321708501_H
#ifndef MULTISAMPLECOUNTS_T552109702_H
#define MULTISAMPLECOUNTS_T552109702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/MultisampleCounts
struct  MultisampleCounts_t552109702 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview/MultisampleCounts::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MultisampleCounts_t552109702, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTISAMPLECOUNTS_T552109702_H
#ifndef BITRATES_T2681405699_H
#define BITRATES_T2681405699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/BitRates
struct  BitRates_t2681405699 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview/BitRates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BitRates_t2681405699, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITRATES_T2681405699_H
#ifndef GVRERRORTYPE_T1921600730_H
#define GVRERRORTYPE_T1921600730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrErrorType
struct  GvrErrorType_t1921600730 
{
public:
	// System.Int32 GvrErrorType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrErrorType_t1921600730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRERRORTYPE_T1921600730_H
#ifndef GVR_RECENTER_FLAGS_T2414511954_H
#define GVR_RECENTER_FLAGS_T2414511954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_recenter_flags
struct  gvr_recenter_flags_t2414511954 
{
public:
	// System.Int32 Gvr.Internal.gvr_recenter_flags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(gvr_recenter_flags_t2414511954, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_RECENTER_FLAGS_T2414511954_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef HEADSETSTATE_T378905490_H
#define HEADSETSTATE_T378905490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.HeadsetState
struct  HeadsetState_t378905490 
{
public:
	// GvrEventType Gvr.Internal.HeadsetState::eventType
	int32_t ___eventType_0;
	// System.Int32 Gvr.Internal.HeadsetState::eventFlags
	int32_t ___eventFlags_1;
	// System.Int64 Gvr.Internal.HeadsetState::eventTimestampNs
	int64_t ___eventTimestampNs_2;
	// GvrRecenterEventType Gvr.Internal.HeadsetState::recenterEventType
	int32_t ___recenterEventType_3;
	// System.UInt32 Gvr.Internal.HeadsetState::recenterEventFlags
	uint32_t ___recenterEventFlags_4;
	// UnityEngine.Vector3 Gvr.Internal.HeadsetState::recenteredPosition
	Vector3_t3722313464  ___recenteredPosition_5;
	// UnityEngine.Quaternion Gvr.Internal.HeadsetState::recenteredRotation
	Quaternion_t2301928331  ___recenteredRotation_6;

public:
	inline static int32_t get_offset_of_eventType_0() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___eventType_0)); }
	inline int32_t get_eventType_0() const { return ___eventType_0; }
	inline int32_t* get_address_of_eventType_0() { return &___eventType_0; }
	inline void set_eventType_0(int32_t value)
	{
		___eventType_0 = value;
	}

	inline static int32_t get_offset_of_eventFlags_1() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___eventFlags_1)); }
	inline int32_t get_eventFlags_1() const { return ___eventFlags_1; }
	inline int32_t* get_address_of_eventFlags_1() { return &___eventFlags_1; }
	inline void set_eventFlags_1(int32_t value)
	{
		___eventFlags_1 = value;
	}

	inline static int32_t get_offset_of_eventTimestampNs_2() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___eventTimestampNs_2)); }
	inline int64_t get_eventTimestampNs_2() const { return ___eventTimestampNs_2; }
	inline int64_t* get_address_of_eventTimestampNs_2() { return &___eventTimestampNs_2; }
	inline void set_eventTimestampNs_2(int64_t value)
	{
		___eventTimestampNs_2 = value;
	}

	inline static int32_t get_offset_of_recenterEventType_3() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenterEventType_3)); }
	inline int32_t get_recenterEventType_3() const { return ___recenterEventType_3; }
	inline int32_t* get_address_of_recenterEventType_3() { return &___recenterEventType_3; }
	inline void set_recenterEventType_3(int32_t value)
	{
		___recenterEventType_3 = value;
	}

	inline static int32_t get_offset_of_recenterEventFlags_4() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenterEventFlags_4)); }
	inline uint32_t get_recenterEventFlags_4() const { return ___recenterEventFlags_4; }
	inline uint32_t* get_address_of_recenterEventFlags_4() { return &___recenterEventFlags_4; }
	inline void set_recenterEventFlags_4(uint32_t value)
	{
		___recenterEventFlags_4 = value;
	}

	inline static int32_t get_offset_of_recenteredPosition_5() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenteredPosition_5)); }
	inline Vector3_t3722313464  get_recenteredPosition_5() const { return ___recenteredPosition_5; }
	inline Vector3_t3722313464 * get_address_of_recenteredPosition_5() { return &___recenteredPosition_5; }
	inline void set_recenteredPosition_5(Vector3_t3722313464  value)
	{
		___recenteredPosition_5 = value;
	}

	inline static int32_t get_offset_of_recenteredRotation_6() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenteredRotation_6)); }
	inline Quaternion_t2301928331  get_recenteredRotation_6() const { return ___recenteredRotation_6; }
	inline Quaternion_t2301928331 * get_address_of_recenteredRotation_6() { return &___recenteredRotation_6; }
	inline void set_recenteredRotation_6(Quaternion_t2301928331  value)
	{
		___recenteredRotation_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETSTATE_T378905490_H
#ifndef POINTERRAY_T3451016640_H
#define POINTERRAY_T3451016640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer/PointerRay
struct  PointerRay_t3451016640 
{
public:
	// UnityEngine.Ray GvrBasePointer/PointerRay::ray
	Ray_t3785851493  ___ray_0;
	// System.Single GvrBasePointer/PointerRay::distanceFromStart
	float ___distanceFromStart_1;
	// System.Single GvrBasePointer/PointerRay::distance
	float ___distance_2;

public:
	inline static int32_t get_offset_of_ray_0() { return static_cast<int32_t>(offsetof(PointerRay_t3451016640, ___ray_0)); }
	inline Ray_t3785851493  get_ray_0() const { return ___ray_0; }
	inline Ray_t3785851493 * get_address_of_ray_0() { return &___ray_0; }
	inline void set_ray_0(Ray_t3785851493  value)
	{
		___ray_0 = value;
	}

	inline static int32_t get_offset_of_distanceFromStart_1() { return static_cast<int32_t>(offsetof(PointerRay_t3451016640, ___distanceFromStart_1)); }
	inline float get_distanceFromStart_1() const { return ___distanceFromStart_1; }
	inline float* get_address_of_distanceFromStart_1() { return &___distanceFromStart_1; }
	inline void set_distanceFromStart_1(float value)
	{
		___distanceFromStart_1 = value;
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(PointerRay_t3451016640, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERRAY_T3451016640_H
#ifndef MUTABLEPOSE3D_T3352419872_H
#define MUTABLEPOSE3D_T3352419872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MutablePose3D
struct  MutablePose3D_t3352419872  : public Pose3D_t2649470188
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUTABLEPOSE3D_T3352419872_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef ANDROIDNATIVEKEYBOARDPROVIDER_T4081466012_H
#define ANDROIDNATIVEKEYBOARDPROVIDER_T4081466012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AndroidNativeKeyboardProvider
struct  AndroidNativeKeyboardProvider_t4081466012  : public RuntimeObject
{
public:
	// System.IntPtr Gvr.Internal.AndroidNativeKeyboardProvider::renderEventFunction
	intptr_t ___renderEventFunction_0;
	// System.IntPtr Gvr.Internal.AndroidNativeKeyboardProvider::keyboard_context
	intptr_t ___keyboard_context_9;
	// GvrKeyboardInputMode Gvr.Internal.AndroidNativeKeyboardProvider::mode
	int32_t ___mode_16;
	// System.String Gvr.Internal.AndroidNativeKeyboardProvider::editorText
	String_t* ___editorText_17;
	// UnityEngine.Matrix4x4 Gvr.Internal.AndroidNativeKeyboardProvider::worldMatrix
	Matrix4x4_t1817901843  ___worldMatrix_18;
	// System.Boolean Gvr.Internal.AndroidNativeKeyboardProvider::isValid
	bool ___isValid_19;
	// System.Boolean Gvr.Internal.AndroidNativeKeyboardProvider::isReady
	bool ___isReady_20;

public:
	inline static int32_t get_offset_of_renderEventFunction_0() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___renderEventFunction_0)); }
	inline intptr_t get_renderEventFunction_0() const { return ___renderEventFunction_0; }
	inline intptr_t* get_address_of_renderEventFunction_0() { return &___renderEventFunction_0; }
	inline void set_renderEventFunction_0(intptr_t value)
	{
		___renderEventFunction_0 = value;
	}

	inline static int32_t get_offset_of_keyboard_context_9() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___keyboard_context_9)); }
	inline intptr_t get_keyboard_context_9() const { return ___keyboard_context_9; }
	inline intptr_t* get_address_of_keyboard_context_9() { return &___keyboard_context_9; }
	inline void set_keyboard_context_9(intptr_t value)
	{
		___keyboard_context_9 = value;
	}

	inline static int32_t get_offset_of_mode_16() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___mode_16)); }
	inline int32_t get_mode_16() const { return ___mode_16; }
	inline int32_t* get_address_of_mode_16() { return &___mode_16; }
	inline void set_mode_16(int32_t value)
	{
		___mode_16 = value;
	}

	inline static int32_t get_offset_of_editorText_17() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___editorText_17)); }
	inline String_t* get_editorText_17() const { return ___editorText_17; }
	inline String_t** get_address_of_editorText_17() { return &___editorText_17; }
	inline void set_editorText_17(String_t* value)
	{
		___editorText_17 = value;
		Il2CppCodeGenWriteBarrier((&___editorText_17), value);
	}

	inline static int32_t get_offset_of_worldMatrix_18() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___worldMatrix_18)); }
	inline Matrix4x4_t1817901843  get_worldMatrix_18() const { return ___worldMatrix_18; }
	inline Matrix4x4_t1817901843 * get_address_of_worldMatrix_18() { return &___worldMatrix_18; }
	inline void set_worldMatrix_18(Matrix4x4_t1817901843  value)
	{
		___worldMatrix_18 = value;
	}

	inline static int32_t get_offset_of_isValid_19() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___isValid_19)); }
	inline bool get_isValid_19() const { return ___isValid_19; }
	inline bool* get_address_of_isValid_19() { return &___isValid_19; }
	inline void set_isValid_19(bool value)
	{
		___isValid_19 = value;
	}

	inline static int32_t get_offset_of_isReady_20() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___isReady_20)); }
	inline bool get_isReady_20() const { return ___isReady_20; }
	inline bool* get_address_of_isReady_20() { return &___isReady_20; }
	inline void set_isReady_20(bool value)
	{
		___isReady_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDNATIVEKEYBOARDPROVIDER_T4081466012_H
#ifndef KEYBOARDCALLBACK_T4011255843_H
#define KEYBOARDCALLBACK_T4011255843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardIntent/KeyboardCallback
struct  KeyboardCallback_t4011255843  : public AndroidJavaProxy_t2835824643
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDCALLBACK_T4011255843_H
#ifndef KEYBOARDSTATE_T4109162649_H
#define KEYBOARDSTATE_T4109162649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KeyboardState
struct  KeyboardState_t4109162649  : public RuntimeObject
{
public:
	// System.String KeyboardState::editorText
	String_t* ___editorText_0;
	// GvrKeyboardInputMode KeyboardState::mode
	int32_t ___mode_1;
	// System.Boolean KeyboardState::isValid
	bool ___isValid_2;
	// System.Boolean KeyboardState::isReady
	bool ___isReady_3;
	// UnityEngine.Matrix4x4 KeyboardState::worldMatrix
	Matrix4x4_t1817901843  ___worldMatrix_4;

public:
	inline static int32_t get_offset_of_editorText_0() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___editorText_0)); }
	inline String_t* get_editorText_0() const { return ___editorText_0; }
	inline String_t** get_address_of_editorText_0() { return &___editorText_0; }
	inline void set_editorText_0(String_t* value)
	{
		___editorText_0 = value;
		Il2CppCodeGenWriteBarrier((&___editorText_0), value);
	}

	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_isValid_2() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___isValid_2)); }
	inline bool get_isValid_2() const { return ___isValid_2; }
	inline bool* get_address_of_isValid_2() { return &___isValid_2; }
	inline void set_isValid_2(bool value)
	{
		___isValid_2 = value;
	}

	inline static int32_t get_offset_of_isReady_3() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___isReady_3)); }
	inline bool get_isReady_3() const { return ___isReady_3; }
	inline bool* get_address_of_isReady_3() { return &___isReady_3; }
	inline void set_isReady_3(bool value)
	{
		___isReady_3 = value;
	}

	inline static int32_t get_offset_of_worldMatrix_4() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___worldMatrix_4)); }
	inline Matrix4x4_t1817901843  get_worldMatrix_4() const { return ___worldMatrix_4; }
	inline Matrix4x4_t1817901843 * get_address_of_worldMatrix_4() { return &___worldMatrix_4; }
	inline void set_worldMatrix_4(Matrix4x4_t1817901843  value)
	{
		___worldMatrix_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDSTATE_T4109162649_H
#ifndef EMULATORKEYBOARDPROVIDER_T2389719130_H
#define EMULATORKEYBOARDPROVIDER_T2389719130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorKeyboardProvider
struct  EmulatorKeyboardProvider_t2389719130  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Gvr.Internal.EmulatorKeyboardProvider::stub
	GameObject_t1113636619 * ___stub_0;
	// System.Boolean Gvr.Internal.EmulatorKeyboardProvider::showing
	bool ___showing_1;
	// GvrKeyboard/KeyboardCallback Gvr.Internal.EmulatorKeyboardProvider::keyboardCallback
	KeyboardCallback_t3330588312 * ___keyboardCallback_2;
	// System.String Gvr.Internal.EmulatorKeyboardProvider::editorText
	String_t* ___editorText_3;
	// GvrKeyboardInputMode Gvr.Internal.EmulatorKeyboardProvider::mode
	int32_t ___mode_4;
	// UnityEngine.Matrix4x4 Gvr.Internal.EmulatorKeyboardProvider::worldMatrix
	Matrix4x4_t1817901843  ___worldMatrix_5;
	// System.Boolean Gvr.Internal.EmulatorKeyboardProvider::isValid
	bool ___isValid_6;

public:
	inline static int32_t get_offset_of_stub_0() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___stub_0)); }
	inline GameObject_t1113636619 * get_stub_0() const { return ___stub_0; }
	inline GameObject_t1113636619 ** get_address_of_stub_0() { return &___stub_0; }
	inline void set_stub_0(GameObject_t1113636619 * value)
	{
		___stub_0 = value;
		Il2CppCodeGenWriteBarrier((&___stub_0), value);
	}

	inline static int32_t get_offset_of_showing_1() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___showing_1)); }
	inline bool get_showing_1() const { return ___showing_1; }
	inline bool* get_address_of_showing_1() { return &___showing_1; }
	inline void set_showing_1(bool value)
	{
		___showing_1 = value;
	}

	inline static int32_t get_offset_of_keyboardCallback_2() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___keyboardCallback_2)); }
	inline KeyboardCallback_t3330588312 * get_keyboardCallback_2() const { return ___keyboardCallback_2; }
	inline KeyboardCallback_t3330588312 ** get_address_of_keyboardCallback_2() { return &___keyboardCallback_2; }
	inline void set_keyboardCallback_2(KeyboardCallback_t3330588312 * value)
	{
		___keyboardCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardCallback_2), value);
	}

	inline static int32_t get_offset_of_editorText_3() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___editorText_3)); }
	inline String_t* get_editorText_3() const { return ___editorText_3; }
	inline String_t** get_address_of_editorText_3() { return &___editorText_3; }
	inline void set_editorText_3(String_t* value)
	{
		___editorText_3 = value;
		Il2CppCodeGenWriteBarrier((&___editorText_3), value);
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_worldMatrix_5() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___worldMatrix_5)); }
	inline Matrix4x4_t1817901843  get_worldMatrix_5() const { return ___worldMatrix_5; }
	inline Matrix4x4_t1817901843 * get_address_of_worldMatrix_5() { return &___worldMatrix_5; }
	inline void set_worldMatrix_5(Matrix4x4_t1817901843  value)
	{
		___worldMatrix_5 = value;
	}

	inline static int32_t get_offset_of_isValid_6() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___isValid_6)); }
	inline bool get_isValid_6() const { return ___isValid_6; }
	inline bool* get_address_of_isValid_6() { return &___isValid_6; }
	inline void set_isValid_6(bool value)
	{
		___isValid_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORKEYBOARDPROVIDER_T2389719130_H
#ifndef KEYBOARDCALLBACK_T3330588312_H
#define KEYBOARDCALLBACK_T3330588312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/KeyboardCallback
struct  KeyboardCallback_t3330588312  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDCALLBACK_T3330588312_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ONSAFETYREGIONEVENT_T980662704_H
#define ONSAFETYREGIONEVENT_T980662704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset/OnSafetyRegionEvent
struct  OnSafetyRegionEvent_t980662704  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSAFETYREGIONEVENT_T980662704_H
#ifndef ONRECENTEREVENT_T1755824842_H
#define ONRECENTEREVENT_T1755824842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset/OnRecenterEvent
struct  OnRecenterEvent_t1755824842  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRECENTEREVENT_T1755824842_H
#ifndef DUMMYHEADSETPROVIDER_T3229995161_H
#define DUMMYHEADSETPROVIDER_T3229995161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.DummyHeadsetProvider
struct  DummyHeadsetProvider_t3229995161  : public RuntimeObject
{
public:
	// Gvr.Internal.HeadsetState Gvr.Internal.DummyHeadsetProvider::dummyState
	HeadsetState_t378905490  ___dummyState_0;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(DummyHeadsetProvider_t3229995161, ___dummyState_0)); }
	inline HeadsetState_t378905490  get_dummyState_0() const { return ___dummyState_0; }
	inline HeadsetState_t378905490 * get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(HeadsetState_t378905490  value)
	{
		___dummyState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYHEADSETPROVIDER_T3229995161_H
#ifndef EDITORHEADSETPROVIDER_T660777464_H
#define EDITORHEADSETPROVIDER_T660777464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EditorHeadsetProvider
struct  EditorHeadsetProvider_t660777464  : public RuntimeObject
{
public:
	// Gvr.Internal.HeadsetState Gvr.Internal.EditorHeadsetProvider::dummyState
	HeadsetState_t378905490  ___dummyState_0;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(EditorHeadsetProvider_t660777464, ___dummyState_0)); }
	inline HeadsetState_t378905490  get_dummyState_0() const { return ___dummyState_0; }
	inline HeadsetState_t378905490 * get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(HeadsetState_t378905490  value)
	{
		___dummyState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORHEADSETPROVIDER_T660777464_H
#ifndef ONVIDEOEVENTCALLBACK_T2376626694_H
#define ONVIDEOEVENTCALLBACK_T2376626694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/OnVideoEventCallback
struct  OnVideoEventCallback_t2376626694  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVIDEOEVENTCALLBACK_T2376626694_H
#ifndef STANDARDCALLBACK_T3095007891_H
#define STANDARDCALLBACK_T3095007891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/StandardCallback
struct  StandardCallback_t3095007891  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDCALLBACK_T3095007891_H
#ifndef ONEXCEPTIONCALLBACK_T1696428116_H
#define ONEXCEPTIONCALLBACK_T1696428116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/OnExceptionCallback
struct  OnExceptionCallback_t1696428116  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEXCEPTIONCALLBACK_T1696428116_H
#ifndef EDITTEXTCALLBACK_T1702213000_H
#define EDITTEXTCALLBACK_T1702213000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/EditTextCallback
struct  EditTextCallback_t1702213000  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITTEXTCALLBACK_T1702213000_H
#ifndef EVENTDELEGATE_T481206256_H
#define EVENTDELEGATE_T481206256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventExecutor/EventDelegate
struct  EventDelegate_t481206256  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDELEGATE_T481206256_H
#ifndef ERRORCALLBACK_T2310212740_H
#define ERRORCALLBACK_T2310212740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/ErrorCallback
struct  ErrorCallback_t2310212740  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCALLBACK_T2310212740_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef GVRCONTROLLERTOOLTIPSSIMPLE_T2073124053_H
#define GVRCONTROLLERTOOLTIPSSIMPLE_T2073124053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerTooltipsSimple
struct  GvrControllerTooltipsSimple_t2073124053  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshRenderer GvrControllerTooltipsSimple::tooltipRenderer
	MeshRenderer_t587009260 * ___tooltipRenderer_2;
	// GvrBaseArmModel GvrControllerTooltipsSimple::<ArmModel>k__BackingField
	GvrBaseArmModel_t3382200842 * ___U3CArmModelU3Ek__BackingField_3;
	// UnityEngine.MaterialPropertyBlock GvrControllerTooltipsSimple::materialPropertyBlock
	MaterialPropertyBlock_t3213117958 * ___materialPropertyBlock_4;
	// System.Int32 GvrControllerTooltipsSimple::colorId
	int32_t ___colorId_5;

public:
	inline static int32_t get_offset_of_tooltipRenderer_2() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t2073124053, ___tooltipRenderer_2)); }
	inline MeshRenderer_t587009260 * get_tooltipRenderer_2() const { return ___tooltipRenderer_2; }
	inline MeshRenderer_t587009260 ** get_address_of_tooltipRenderer_2() { return &___tooltipRenderer_2; }
	inline void set_tooltipRenderer_2(MeshRenderer_t587009260 * value)
	{
		___tooltipRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___tooltipRenderer_2), value);
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t2073124053, ___U3CArmModelU3Ek__BackingField_3)); }
	inline GvrBaseArmModel_t3382200842 * get_U3CArmModelU3Ek__BackingField_3() const { return ___U3CArmModelU3Ek__BackingField_3; }
	inline GvrBaseArmModel_t3382200842 ** get_address_of_U3CArmModelU3Ek__BackingField_3() { return &___U3CArmModelU3Ek__BackingField_3; }
	inline void set_U3CArmModelU3Ek__BackingField_3(GvrBaseArmModel_t3382200842 * value)
	{
		___U3CArmModelU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_materialPropertyBlock_4() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t2073124053, ___materialPropertyBlock_4)); }
	inline MaterialPropertyBlock_t3213117958 * get_materialPropertyBlock_4() const { return ___materialPropertyBlock_4; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_materialPropertyBlock_4() { return &___materialPropertyBlock_4; }
	inline void set_materialPropertyBlock_4(MaterialPropertyBlock_t3213117958 * value)
	{
		___materialPropertyBlock_4 = value;
		Il2CppCodeGenWriteBarrier((&___materialPropertyBlock_4), value);
	}

	inline static int32_t get_offset_of_colorId_5() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t2073124053, ___colorId_5)); }
	inline int32_t get_colorId_5() const { return ___colorId_5; }
	inline int32_t* get_address_of_colorId_5() { return &___colorId_5; }
	inline void set_colorId_5(int32_t value)
	{
		___colorId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERTOOLTIPSSIMPLE_T2073124053_H
#ifndef GVREDITOREMULATOR_T2879390294_H
#define GVREDITOREMULATOR_T2879390294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEditorEmulator
struct  GvrEditorEmulator_t2879390294  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREDITOREMULATOR_T2879390294_H
#ifndef INSTANTPREVIEW_T778898416_H
#define INSTANTPREVIEW_T778898416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview
struct  InstantPreview_t778898416  : public MonoBehaviour_t3962482529
{
public:
	// Gvr.Internal.InstantPreview/Resolutions Gvr.Internal.InstantPreview::OutputResolution
	int32_t ___OutputResolution_5;
	// Gvr.Internal.InstantPreview/MultisampleCounts Gvr.Internal.InstantPreview::MultisampleCount
	int32_t ___MultisampleCount_6;
	// Gvr.Internal.InstantPreview/BitRates Gvr.Internal.InstantPreview::BitRate
	int32_t ___BitRate_7;
	// System.Boolean Gvr.Internal.InstantPreview::InstallApkOnRun
	bool ___InstallApkOnRun_8;
	// UnityEngine.Object Gvr.Internal.InstantPreview::InstantPreviewApk
	Object_t631007953 * ___InstantPreviewApk_9;

public:
	inline static int32_t get_offset_of_OutputResolution_5() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___OutputResolution_5)); }
	inline int32_t get_OutputResolution_5() const { return ___OutputResolution_5; }
	inline int32_t* get_address_of_OutputResolution_5() { return &___OutputResolution_5; }
	inline void set_OutputResolution_5(int32_t value)
	{
		___OutputResolution_5 = value;
	}

	inline static int32_t get_offset_of_MultisampleCount_6() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___MultisampleCount_6)); }
	inline int32_t get_MultisampleCount_6() const { return ___MultisampleCount_6; }
	inline int32_t* get_address_of_MultisampleCount_6() { return &___MultisampleCount_6; }
	inline void set_MultisampleCount_6(int32_t value)
	{
		___MultisampleCount_6 = value;
	}

	inline static int32_t get_offset_of_BitRate_7() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___BitRate_7)); }
	inline int32_t get_BitRate_7() const { return ___BitRate_7; }
	inline int32_t* get_address_of_BitRate_7() { return &___BitRate_7; }
	inline void set_BitRate_7(int32_t value)
	{
		___BitRate_7 = value;
	}

	inline static int32_t get_offset_of_InstallApkOnRun_8() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___InstallApkOnRun_8)); }
	inline bool get_InstallApkOnRun_8() const { return ___InstallApkOnRun_8; }
	inline bool* get_address_of_InstallApkOnRun_8() { return &___InstallApkOnRun_8; }
	inline void set_InstallApkOnRun_8(bool value)
	{
		___InstallApkOnRun_8 = value;
	}

	inline static int32_t get_offset_of_InstantPreviewApk_9() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___InstantPreviewApk_9)); }
	inline Object_t631007953 * get_InstantPreviewApk_9() const { return ___InstantPreviewApk_9; }
	inline Object_t631007953 ** get_address_of_InstantPreviewApk_9() { return &___InstantPreviewApk_9; }
	inline void set_InstantPreviewApk_9(Object_t631007953 * value)
	{
		___InstantPreviewApk_9 = value;
		Il2CppCodeGenWriteBarrier((&___InstantPreviewApk_9), value);
	}
};

struct InstantPreview_t778898416_StaticFields
{
public:
	// Gvr.Internal.InstantPreview Gvr.Internal.InstantPreview::<Instance>k__BackingField
	InstantPreview_t778898416 * ___U3CInstanceU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416_StaticFields, ___U3CInstanceU3Ek__BackingField_3)); }
	inline InstantPreview_t778898416 * get_U3CInstanceU3Ek__BackingField_3() const { return ___U3CInstanceU3Ek__BackingField_3; }
	inline InstantPreview_t778898416 ** get_address_of_U3CInstanceU3Ek__BackingField_3() { return &___U3CInstanceU3Ek__BackingField_3; }
	inline void set_U3CInstanceU3Ek__BackingField_3(InstantPreview_t778898416 * value)
	{
		___U3CInstanceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTPREVIEW_T778898416_H
#ifndef GVRHEADSET_T1874684537_H
#define GVRHEADSET_T1874684537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset
struct  GvrHeadset_t1874684537  : public MonoBehaviour_t3962482529
{
public:
	// Gvr.Internal.IHeadsetProvider GvrHeadset::headsetProvider
	RuntimeObject* ___headsetProvider_3;
	// Gvr.Internal.HeadsetState GvrHeadset::headsetState
	HeadsetState_t378905490  ___headsetState_4;
	// System.Collections.IEnumerator GvrHeadset::standaloneUpdate
	RuntimeObject* ___standaloneUpdate_5;
	// UnityEngine.WaitForEndOfFrame GvrHeadset::waitForEndOfFrame
	WaitForEndOfFrame_t1314943911 * ___waitForEndOfFrame_6;
	// GvrHeadset/OnSafetyRegionEvent GvrHeadset::safetyRegionDelegate
	OnSafetyRegionEvent_t980662704 * ___safetyRegionDelegate_7;
	// GvrHeadset/OnRecenterEvent GvrHeadset::recenterDelegate
	OnRecenterEvent_t1755824842 * ___recenterDelegate_8;

public:
	inline static int32_t get_offset_of_headsetProvider_3() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___headsetProvider_3)); }
	inline RuntimeObject* get_headsetProvider_3() const { return ___headsetProvider_3; }
	inline RuntimeObject** get_address_of_headsetProvider_3() { return &___headsetProvider_3; }
	inline void set_headsetProvider_3(RuntimeObject* value)
	{
		___headsetProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&___headsetProvider_3), value);
	}

	inline static int32_t get_offset_of_headsetState_4() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___headsetState_4)); }
	inline HeadsetState_t378905490  get_headsetState_4() const { return ___headsetState_4; }
	inline HeadsetState_t378905490 * get_address_of_headsetState_4() { return &___headsetState_4; }
	inline void set_headsetState_4(HeadsetState_t378905490  value)
	{
		___headsetState_4 = value;
	}

	inline static int32_t get_offset_of_standaloneUpdate_5() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___standaloneUpdate_5)); }
	inline RuntimeObject* get_standaloneUpdate_5() const { return ___standaloneUpdate_5; }
	inline RuntimeObject** get_address_of_standaloneUpdate_5() { return &___standaloneUpdate_5; }
	inline void set_standaloneUpdate_5(RuntimeObject* value)
	{
		___standaloneUpdate_5 = value;
		Il2CppCodeGenWriteBarrier((&___standaloneUpdate_5), value);
	}

	inline static int32_t get_offset_of_waitForEndOfFrame_6() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___waitForEndOfFrame_6)); }
	inline WaitForEndOfFrame_t1314943911 * get_waitForEndOfFrame_6() const { return ___waitForEndOfFrame_6; }
	inline WaitForEndOfFrame_t1314943911 ** get_address_of_waitForEndOfFrame_6() { return &___waitForEndOfFrame_6; }
	inline void set_waitForEndOfFrame_6(WaitForEndOfFrame_t1314943911 * value)
	{
		___waitForEndOfFrame_6 = value;
		Il2CppCodeGenWriteBarrier((&___waitForEndOfFrame_6), value);
	}

	inline static int32_t get_offset_of_safetyRegionDelegate_7() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___safetyRegionDelegate_7)); }
	inline OnSafetyRegionEvent_t980662704 * get_safetyRegionDelegate_7() const { return ___safetyRegionDelegate_7; }
	inline OnSafetyRegionEvent_t980662704 ** get_address_of_safetyRegionDelegate_7() { return &___safetyRegionDelegate_7; }
	inline void set_safetyRegionDelegate_7(OnSafetyRegionEvent_t980662704 * value)
	{
		___safetyRegionDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&___safetyRegionDelegate_7), value);
	}

	inline static int32_t get_offset_of_recenterDelegate_8() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___recenterDelegate_8)); }
	inline OnRecenterEvent_t1755824842 * get_recenterDelegate_8() const { return ___recenterDelegate_8; }
	inline OnRecenterEvent_t1755824842 ** get_address_of_recenterDelegate_8() { return &___recenterDelegate_8; }
	inline void set_recenterDelegate_8(OnRecenterEvent_t1755824842 * value)
	{
		___recenterDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&___recenterDelegate_8), value);
	}
};

struct GvrHeadset_t1874684537_StaticFields
{
public:
	// GvrHeadset GvrHeadset::instance
	GvrHeadset_t1874684537 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537_StaticFields, ___instance_2)); }
	inline GvrHeadset_t1874684537 * get_instance_2() const { return ___instance_2; }
	inline GvrHeadset_t1874684537 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GvrHeadset_t1874684537 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRHEADSET_T1874684537_H
#ifndef GVRTOOLTIP_T491261851_H
#define GVRTOOLTIP_T491261851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrTooltip
struct  GvrTooltip_t491261851  : public MonoBehaviour_t3962482529
{
public:
	// GvrTooltip/Location GvrTooltip::location
	int32_t ___location_9;
	// UnityEngine.UI.Text GvrTooltip::text
	Text_t1901882714 * ___text_10;
	// System.Boolean GvrTooltip::alwaysVisible
	bool ___alwaysVisible_11;
	// System.Boolean GvrTooltip::isOnLeft
	bool ___isOnLeft_12;
	// UnityEngine.RectTransform GvrTooltip::rectTransform
	RectTransform_t3704657025 * ___rectTransform_13;
	// UnityEngine.CanvasGroup GvrTooltip::canvasGroup
	CanvasGroup_t4083511760 * ___canvasGroup_14;
	// GvrBaseArmModel GvrTooltip::<ArmModel>k__BackingField
	GvrBaseArmModel_t3382200842 * ___U3CArmModelU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_location_9() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___location_9)); }
	inline int32_t get_location_9() const { return ___location_9; }
	inline int32_t* get_address_of_location_9() { return &___location_9; }
	inline void set_location_9(int32_t value)
	{
		___location_9 = value;
	}

	inline static int32_t get_offset_of_text_10() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___text_10)); }
	inline Text_t1901882714 * get_text_10() const { return ___text_10; }
	inline Text_t1901882714 ** get_address_of_text_10() { return &___text_10; }
	inline void set_text_10(Text_t1901882714 * value)
	{
		___text_10 = value;
		Il2CppCodeGenWriteBarrier((&___text_10), value);
	}

	inline static int32_t get_offset_of_alwaysVisible_11() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___alwaysVisible_11)); }
	inline bool get_alwaysVisible_11() const { return ___alwaysVisible_11; }
	inline bool* get_address_of_alwaysVisible_11() { return &___alwaysVisible_11; }
	inline void set_alwaysVisible_11(bool value)
	{
		___alwaysVisible_11 = value;
	}

	inline static int32_t get_offset_of_isOnLeft_12() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___isOnLeft_12)); }
	inline bool get_isOnLeft_12() const { return ___isOnLeft_12; }
	inline bool* get_address_of_isOnLeft_12() { return &___isOnLeft_12; }
	inline void set_isOnLeft_12(bool value)
	{
		___isOnLeft_12 = value;
	}

	inline static int32_t get_offset_of_rectTransform_13() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___rectTransform_13)); }
	inline RectTransform_t3704657025 * get_rectTransform_13() const { return ___rectTransform_13; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_13() { return &___rectTransform_13; }
	inline void set_rectTransform_13(RectTransform_t3704657025 * value)
	{
		___rectTransform_13 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_13), value);
	}

	inline static int32_t get_offset_of_canvasGroup_14() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___canvasGroup_14)); }
	inline CanvasGroup_t4083511760 * get_canvasGroup_14() const { return ___canvasGroup_14; }
	inline CanvasGroup_t4083511760 ** get_address_of_canvasGroup_14() { return &___canvasGroup_14; }
	inline void set_canvasGroup_14(CanvasGroup_t4083511760 * value)
	{
		___canvasGroup_14 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_14), value);
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___U3CArmModelU3Ek__BackingField_15)); }
	inline GvrBaseArmModel_t3382200842 * get_U3CArmModelU3Ek__BackingField_15() const { return ___U3CArmModelU3Ek__BackingField_15; }
	inline GvrBaseArmModel_t3382200842 ** get_address_of_U3CArmModelU3Ek__BackingField_15() { return &___U3CArmModelU3Ek__BackingField_15; }
	inline void set_U3CArmModelU3Ek__BackingField_15(GvrBaseArmModel_t3382200842 * value)
	{
		___U3CArmModelU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_15), value);
	}
};

struct GvrTooltip_t491261851_StaticFields
{
public:
	// UnityEngine.Quaternion GvrTooltip::RIGHT_SIDE_ROTATION
	Quaternion_t2301928331  ___RIGHT_SIDE_ROTATION_2;
	// UnityEngine.Quaternion GvrTooltip::LEFT_SIDE_ROTATION
	Quaternion_t2301928331  ___LEFT_SIDE_ROTATION_3;
	// UnityEngine.Vector2 GvrTooltip::SQUARE_CENTER
	Vector2_t2156229523  ___SQUARE_CENTER_4;
	// UnityEngine.Vector2 GvrTooltip::PIVOT
	Vector2_t2156229523  ___PIVOT_5;

public:
	inline static int32_t get_offset_of_RIGHT_SIDE_ROTATION_2() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851_StaticFields, ___RIGHT_SIDE_ROTATION_2)); }
	inline Quaternion_t2301928331  get_RIGHT_SIDE_ROTATION_2() const { return ___RIGHT_SIDE_ROTATION_2; }
	inline Quaternion_t2301928331 * get_address_of_RIGHT_SIDE_ROTATION_2() { return &___RIGHT_SIDE_ROTATION_2; }
	inline void set_RIGHT_SIDE_ROTATION_2(Quaternion_t2301928331  value)
	{
		___RIGHT_SIDE_ROTATION_2 = value;
	}

	inline static int32_t get_offset_of_LEFT_SIDE_ROTATION_3() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851_StaticFields, ___LEFT_SIDE_ROTATION_3)); }
	inline Quaternion_t2301928331  get_LEFT_SIDE_ROTATION_3() const { return ___LEFT_SIDE_ROTATION_3; }
	inline Quaternion_t2301928331 * get_address_of_LEFT_SIDE_ROTATION_3() { return &___LEFT_SIDE_ROTATION_3; }
	inline void set_LEFT_SIDE_ROTATION_3(Quaternion_t2301928331  value)
	{
		___LEFT_SIDE_ROTATION_3 = value;
	}

	inline static int32_t get_offset_of_SQUARE_CENTER_4() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851_StaticFields, ___SQUARE_CENTER_4)); }
	inline Vector2_t2156229523  get_SQUARE_CENTER_4() const { return ___SQUARE_CENTER_4; }
	inline Vector2_t2156229523 * get_address_of_SQUARE_CENTER_4() { return &___SQUARE_CENTER_4; }
	inline void set_SQUARE_CENTER_4(Vector2_t2156229523  value)
	{
		___SQUARE_CENTER_4 = value;
	}

	inline static int32_t get_offset_of_PIVOT_5() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851_StaticFields, ___PIVOT_5)); }
	inline Vector2_t2156229523  get_PIVOT_5() const { return ___PIVOT_5; }
	inline Vector2_t2156229523 * get_address_of_PIVOT_5() { return &___PIVOT_5; }
	inline void set_PIVOT_5(Vector2_t2156229523  value)
	{
		___PIVOT_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRTOOLTIP_T491261851_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef GVRALLEVENTSTRIGGER_T2174159095_H
#define GVRALLEVENTSTRIGGER_T2174159095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAllEventsTrigger
struct  GvrAllEventsTrigger_t2174159095  : public MonoBehaviour_t3962482529
{
public:
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerClick
	TriggerEvent_t1225966107 * ___OnPointerClick_2;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerDown
	TriggerEvent_t1225966107 * ___OnPointerDown_3;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerUp
	TriggerEvent_t1225966107 * ___OnPointerUp_4;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerEnter
	TriggerEvent_t1225966107 * ___OnPointerEnter_5;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerExit
	TriggerEvent_t1225966107 * ___OnPointerExit_6;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnScroll
	TriggerEvent_t1225966107 * ___OnScroll_7;
	// System.Boolean GvrAllEventsTrigger::listenersAdded
	bool ___listenersAdded_8;

public:
	inline static int32_t get_offset_of_OnPointerClick_2() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerClick_2)); }
	inline TriggerEvent_t1225966107 * get_OnPointerClick_2() const { return ___OnPointerClick_2; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerClick_2() { return &___OnPointerClick_2; }
	inline void set_OnPointerClick_2(TriggerEvent_t1225966107 * value)
	{
		___OnPointerClick_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerClick_2), value);
	}

	inline static int32_t get_offset_of_OnPointerDown_3() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerDown_3)); }
	inline TriggerEvent_t1225966107 * get_OnPointerDown_3() const { return ___OnPointerDown_3; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerDown_3() { return &___OnPointerDown_3; }
	inline void set_OnPointerDown_3(TriggerEvent_t1225966107 * value)
	{
		___OnPointerDown_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerDown_3), value);
	}

	inline static int32_t get_offset_of_OnPointerUp_4() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerUp_4)); }
	inline TriggerEvent_t1225966107 * get_OnPointerUp_4() const { return ___OnPointerUp_4; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerUp_4() { return &___OnPointerUp_4; }
	inline void set_OnPointerUp_4(TriggerEvent_t1225966107 * value)
	{
		___OnPointerUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerUp_4), value);
	}

	inline static int32_t get_offset_of_OnPointerEnter_5() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerEnter_5)); }
	inline TriggerEvent_t1225966107 * get_OnPointerEnter_5() const { return ___OnPointerEnter_5; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerEnter_5() { return &___OnPointerEnter_5; }
	inline void set_OnPointerEnter_5(TriggerEvent_t1225966107 * value)
	{
		___OnPointerEnter_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerEnter_5), value);
	}

	inline static int32_t get_offset_of_OnPointerExit_6() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerExit_6)); }
	inline TriggerEvent_t1225966107 * get_OnPointerExit_6() const { return ___OnPointerExit_6; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerExit_6() { return &___OnPointerExit_6; }
	inline void set_OnPointerExit_6(TriggerEvent_t1225966107 * value)
	{
		___OnPointerExit_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerExit_6), value);
	}

	inline static int32_t get_offset_of_OnScroll_7() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnScroll_7)); }
	inline TriggerEvent_t1225966107 * get_OnScroll_7() const { return ___OnScroll_7; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnScroll_7() { return &___OnScroll_7; }
	inline void set_OnScroll_7(TriggerEvent_t1225966107 * value)
	{
		___OnScroll_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnScroll_7), value);
	}

	inline static int32_t get_offset_of_listenersAdded_8() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___listenersAdded_8)); }
	inline bool get_listenersAdded_8() const { return ___listenersAdded_8; }
	inline bool* get_address_of_listenersAdded_8() { return &___listenersAdded_8; }
	inline void set_listenersAdded_8(bool value)
	{
		___listenersAdded_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRALLEVENTSTRIGGER_T2174159095_H
#ifndef GVRKEYBOARD_T2536560201_H
#define GVRKEYBOARD_T2536560201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard
struct  GvrKeyboard_t2536560201  : public MonoBehaviour_t3962482529
{
public:
	// KeyboardState GvrKeyboard::keyboardState
	KeyboardState_t4109162649 * ___keyboardState_4;
	// System.Collections.IEnumerator GvrKeyboard::keyboardUpdate
	RuntimeObject* ___keyboardUpdate_5;
	// GvrKeyboard/ErrorCallback GvrKeyboard::errorCallback
	ErrorCallback_t2310212740 * ___errorCallback_6;
	// GvrKeyboard/StandardCallback GvrKeyboard::showCallback
	StandardCallback_t3095007891 * ___showCallback_7;
	// GvrKeyboard/StandardCallback GvrKeyboard::hideCallback
	StandardCallback_t3095007891 * ___hideCallback_8;
	// GvrKeyboard/EditTextCallback GvrKeyboard::updateCallback
	EditTextCallback_t1702213000 * ___updateCallback_9;
	// GvrKeyboard/EditTextCallback GvrKeyboard::enterCallback
	EditTextCallback_t1702213000 * ___enterCallback_10;
	// System.Boolean GvrKeyboard::isKeyboardHidden
	bool ___isKeyboardHidden_11;
	// GvrKeyboardDelegateBase GvrKeyboard::keyboardDelegate
	GvrKeyboardDelegateBase_t30895224 * ___keyboardDelegate_15;
	// GvrKeyboardInputMode GvrKeyboard::inputMode
	int32_t ___inputMode_16;
	// System.Boolean GvrKeyboard::useRecommended
	bool ___useRecommended_17;
	// System.Single GvrKeyboard::distance
	float ___distance_18;

public:
	inline static int32_t get_offset_of_keyboardState_4() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___keyboardState_4)); }
	inline KeyboardState_t4109162649 * get_keyboardState_4() const { return ___keyboardState_4; }
	inline KeyboardState_t4109162649 ** get_address_of_keyboardState_4() { return &___keyboardState_4; }
	inline void set_keyboardState_4(KeyboardState_t4109162649 * value)
	{
		___keyboardState_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardState_4), value);
	}

	inline static int32_t get_offset_of_keyboardUpdate_5() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___keyboardUpdate_5)); }
	inline RuntimeObject* get_keyboardUpdate_5() const { return ___keyboardUpdate_5; }
	inline RuntimeObject** get_address_of_keyboardUpdate_5() { return &___keyboardUpdate_5; }
	inline void set_keyboardUpdate_5(RuntimeObject* value)
	{
		___keyboardUpdate_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardUpdate_5), value);
	}

	inline static int32_t get_offset_of_errorCallback_6() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___errorCallback_6)); }
	inline ErrorCallback_t2310212740 * get_errorCallback_6() const { return ___errorCallback_6; }
	inline ErrorCallback_t2310212740 ** get_address_of_errorCallback_6() { return &___errorCallback_6; }
	inline void set_errorCallback_6(ErrorCallback_t2310212740 * value)
	{
		___errorCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___errorCallback_6), value);
	}

	inline static int32_t get_offset_of_showCallback_7() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___showCallback_7)); }
	inline StandardCallback_t3095007891 * get_showCallback_7() const { return ___showCallback_7; }
	inline StandardCallback_t3095007891 ** get_address_of_showCallback_7() { return &___showCallback_7; }
	inline void set_showCallback_7(StandardCallback_t3095007891 * value)
	{
		___showCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___showCallback_7), value);
	}

	inline static int32_t get_offset_of_hideCallback_8() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___hideCallback_8)); }
	inline StandardCallback_t3095007891 * get_hideCallback_8() const { return ___hideCallback_8; }
	inline StandardCallback_t3095007891 ** get_address_of_hideCallback_8() { return &___hideCallback_8; }
	inline void set_hideCallback_8(StandardCallback_t3095007891 * value)
	{
		___hideCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___hideCallback_8), value);
	}

	inline static int32_t get_offset_of_updateCallback_9() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___updateCallback_9)); }
	inline EditTextCallback_t1702213000 * get_updateCallback_9() const { return ___updateCallback_9; }
	inline EditTextCallback_t1702213000 ** get_address_of_updateCallback_9() { return &___updateCallback_9; }
	inline void set_updateCallback_9(EditTextCallback_t1702213000 * value)
	{
		___updateCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___updateCallback_9), value);
	}

	inline static int32_t get_offset_of_enterCallback_10() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___enterCallback_10)); }
	inline EditTextCallback_t1702213000 * get_enterCallback_10() const { return ___enterCallback_10; }
	inline EditTextCallback_t1702213000 ** get_address_of_enterCallback_10() { return &___enterCallback_10; }
	inline void set_enterCallback_10(EditTextCallback_t1702213000 * value)
	{
		___enterCallback_10 = value;
		Il2CppCodeGenWriteBarrier((&___enterCallback_10), value);
	}

	inline static int32_t get_offset_of_isKeyboardHidden_11() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___isKeyboardHidden_11)); }
	inline bool get_isKeyboardHidden_11() const { return ___isKeyboardHidden_11; }
	inline bool* get_address_of_isKeyboardHidden_11() { return &___isKeyboardHidden_11; }
	inline void set_isKeyboardHidden_11(bool value)
	{
		___isKeyboardHidden_11 = value;
	}

	inline static int32_t get_offset_of_keyboardDelegate_15() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___keyboardDelegate_15)); }
	inline GvrKeyboardDelegateBase_t30895224 * get_keyboardDelegate_15() const { return ___keyboardDelegate_15; }
	inline GvrKeyboardDelegateBase_t30895224 ** get_address_of_keyboardDelegate_15() { return &___keyboardDelegate_15; }
	inline void set_keyboardDelegate_15(GvrKeyboardDelegateBase_t30895224 * value)
	{
		___keyboardDelegate_15 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardDelegate_15), value);
	}

	inline static int32_t get_offset_of_inputMode_16() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___inputMode_16)); }
	inline int32_t get_inputMode_16() const { return ___inputMode_16; }
	inline int32_t* get_address_of_inputMode_16() { return &___inputMode_16; }
	inline void set_inputMode_16(int32_t value)
	{
		___inputMode_16 = value;
	}

	inline static int32_t get_offset_of_useRecommended_17() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___useRecommended_17)); }
	inline bool get_useRecommended_17() const { return ___useRecommended_17; }
	inline bool* get_address_of_useRecommended_17() { return &___useRecommended_17; }
	inline void set_useRecommended_17(bool value)
	{
		___useRecommended_17 = value;
	}

	inline static int32_t get_offset_of_distance_18() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___distance_18)); }
	inline float get_distance_18() const { return ___distance_18; }
	inline float* get_address_of_distance_18() { return &___distance_18; }
	inline void set_distance_18(float value)
	{
		___distance_18 = value;
	}
};

struct GvrKeyboard_t2536560201_StaticFields
{
public:
	// GvrKeyboard GvrKeyboard::instance
	GvrKeyboard_t2536560201 * ___instance_2;
	// Gvr.Internal.IKeyboardProvider GvrKeyboard::keyboardProvider
	RuntimeObject* ___keyboardProvider_3;
	// System.Collections.Generic.List`1<GvrKeyboardEvent> GvrKeyboard::threadSafeCallbacks
	List_1_t806272884 * ___threadSafeCallbacks_13;
	// System.Object GvrKeyboard::callbacksLock
	RuntimeObject * ___callbacksLock_14;
	// GvrKeyboard/KeyboardCallback GvrKeyboard::<>f__mg$cache0
	KeyboardCallback_t3330588312 * ___U3CU3Ef__mgU24cache0_19;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___instance_2)); }
	inline GvrKeyboard_t2536560201 * get_instance_2() const { return ___instance_2; }
	inline GvrKeyboard_t2536560201 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GvrKeyboard_t2536560201 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_keyboardProvider_3() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___keyboardProvider_3)); }
	inline RuntimeObject* get_keyboardProvider_3() const { return ___keyboardProvider_3; }
	inline RuntimeObject** get_address_of_keyboardProvider_3() { return &___keyboardProvider_3; }
	inline void set_keyboardProvider_3(RuntimeObject* value)
	{
		___keyboardProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardProvider_3), value);
	}

	inline static int32_t get_offset_of_threadSafeCallbacks_13() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___threadSafeCallbacks_13)); }
	inline List_1_t806272884 * get_threadSafeCallbacks_13() const { return ___threadSafeCallbacks_13; }
	inline List_1_t806272884 ** get_address_of_threadSafeCallbacks_13() { return &___threadSafeCallbacks_13; }
	inline void set_threadSafeCallbacks_13(List_1_t806272884 * value)
	{
		___threadSafeCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&___threadSafeCallbacks_13), value);
	}

	inline static int32_t get_offset_of_callbacksLock_14() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___callbacksLock_14)); }
	inline RuntimeObject * get_callbacksLock_14() const { return ___callbacksLock_14; }
	inline RuntimeObject ** get_address_of_callbacksLock_14() { return &___callbacksLock_14; }
	inline void set_callbacksLock_14(RuntimeObject * value)
	{
		___callbacksLock_14 = value;
		Il2CppCodeGenWriteBarrier((&___callbacksLock_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_19() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___U3CU3Ef__mgU24cache0_19)); }
	inline KeyboardCallback_t3330588312 * get_U3CU3Ef__mgU24cache0_19() const { return ___U3CU3Ef__mgU24cache0_19; }
	inline KeyboardCallback_t3330588312 ** get_address_of_U3CU3Ef__mgU24cache0_19() { return &___U3CU3Ef__mgU24cache0_19; }
	inline void set_U3CU3Ef__mgU24cache0_19(KeyboardCallback_t3330588312 * value)
	{
		___U3CU3Ef__mgU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARD_T2536560201_H
#ifndef GVRSCROLLSETTINGS_T33837608_H
#define GVRSCROLLSETTINGS_T33837608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrScrollSettings
struct  GvrScrollSettings_t33837608  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GvrScrollSettings::inertiaOverride
	bool ___inertiaOverride_2;
	// System.Single GvrScrollSettings::decelerationRateOverride
	float ___decelerationRateOverride_3;

public:
	inline static int32_t get_offset_of_inertiaOverride_2() { return static_cast<int32_t>(offsetof(GvrScrollSettings_t33837608, ___inertiaOverride_2)); }
	inline bool get_inertiaOverride_2() const { return ___inertiaOverride_2; }
	inline bool* get_address_of_inertiaOverride_2() { return &___inertiaOverride_2; }
	inline void set_inertiaOverride_2(bool value)
	{
		___inertiaOverride_2 = value;
	}

	inline static int32_t get_offset_of_decelerationRateOverride_3() { return static_cast<int32_t>(offsetof(GvrScrollSettings_t33837608, ___decelerationRateOverride_3)); }
	inline float get_decelerationRateOverride_3() const { return ___decelerationRateOverride_3; }
	inline float* get_address_of_decelerationRateOverride_3() { return &___decelerationRateOverride_3; }
	inline void set_decelerationRateOverride_3(float value)
	{
		___decelerationRateOverride_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRSCROLLSETTINGS_T33837608_H
#ifndef GVRVIDEOPLAYERTEXTURE_T3546202735_H
#define GVRVIDEOPLAYERTEXTURE_T3546202735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture
struct  GvrVideoPlayerTexture_t3546202735  : public MonoBehaviour_t3962482529
{
public:
	// System.IntPtr GvrVideoPlayerTexture::videoPlayerPtr
	intptr_t ___videoPlayerPtr_2;
	// System.Int32 GvrVideoPlayerTexture::videoPlayerEventBase
	int32_t ___videoPlayerEventBase_3;
	// UnityEngine.Texture GvrVideoPlayerTexture::initialTexture
	Texture_t3661962703 * ___initialTexture_4;
	// UnityEngine.Texture GvrVideoPlayerTexture::surfaceTexture
	Texture_t3661962703 * ___surfaceTexture_5;
	// System.Single[] GvrVideoPlayerTexture::videoMatrixRaw
	SingleU5BU5D_t1444911251* ___videoMatrixRaw_6;
	// UnityEngine.Matrix4x4 GvrVideoPlayerTexture::videoMatrix
	Matrix4x4_t1817901843  ___videoMatrix_7;
	// System.Int32 GvrVideoPlayerTexture::videoMatrixPropertyId
	int32_t ___videoMatrixPropertyId_8;
	// System.Int64 GvrVideoPlayerTexture::lastVideoTimestamp
	int64_t ___lastVideoTimestamp_9;
	// System.Boolean GvrVideoPlayerTexture::initialized
	bool ___initialized_10;
	// System.Int32 GvrVideoPlayerTexture::texWidth
	int32_t ___texWidth_11;
	// System.Int32 GvrVideoPlayerTexture::texHeight
	int32_t ___texHeight_12;
	// System.Int64 GvrVideoPlayerTexture::lastBufferedPosition
	int64_t ___lastBufferedPosition_13;
	// System.Single GvrVideoPlayerTexture::framecount
	float ___framecount_14;
	// UnityEngine.Renderer GvrVideoPlayerTexture::screen
	Renderer_t2627027031 * ___screen_15;
	// System.IntPtr GvrVideoPlayerTexture::renderEventFunction
	intptr_t ___renderEventFunction_16;
	// System.Boolean GvrVideoPlayerTexture::playOnResume
	bool ___playOnResume_17;
	// System.Collections.Generic.List`1<System.Action`1<System.Int32>> GvrVideoPlayerTexture::onEventCallbacks
	List_1_t300520794 * ___onEventCallbacks_18;
	// System.Collections.Generic.List`1<System.Action`2<System.String,System.String>> GvrVideoPlayerTexture::onExceptionCallbacks
	List_1_t1147278120 * ___onExceptionCallbacks_19;
	// UnityEngine.UI.Text GvrVideoPlayerTexture::statusText
	Text_t1901882714 * ___statusText_21;
	// GvrVideoPlayerTexture/VideoType GvrVideoPlayerTexture::videoType
	int32_t ___videoType_22;
	// System.String GvrVideoPlayerTexture::videoURL
	String_t* ___videoURL_23;
	// System.String GvrVideoPlayerTexture::videoContentID
	String_t* ___videoContentID_24;
	// System.String GvrVideoPlayerTexture::videoProviderId
	String_t* ___videoProviderId_25;
	// GvrVideoPlayerTexture/VideoResolution GvrVideoPlayerTexture::initialResolution
	int32_t ___initialResolution_26;
	// System.Boolean GvrVideoPlayerTexture::adjustAspectRatio
	bool ___adjustAspectRatio_27;
	// System.Boolean GvrVideoPlayerTexture::useSecurePath
	bool ___useSecurePath_28;

public:
	inline static int32_t get_offset_of_videoPlayerPtr_2() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoPlayerPtr_2)); }
	inline intptr_t get_videoPlayerPtr_2() const { return ___videoPlayerPtr_2; }
	inline intptr_t* get_address_of_videoPlayerPtr_2() { return &___videoPlayerPtr_2; }
	inline void set_videoPlayerPtr_2(intptr_t value)
	{
		___videoPlayerPtr_2 = value;
	}

	inline static int32_t get_offset_of_videoPlayerEventBase_3() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoPlayerEventBase_3)); }
	inline int32_t get_videoPlayerEventBase_3() const { return ___videoPlayerEventBase_3; }
	inline int32_t* get_address_of_videoPlayerEventBase_3() { return &___videoPlayerEventBase_3; }
	inline void set_videoPlayerEventBase_3(int32_t value)
	{
		___videoPlayerEventBase_3 = value;
	}

	inline static int32_t get_offset_of_initialTexture_4() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___initialTexture_4)); }
	inline Texture_t3661962703 * get_initialTexture_4() const { return ___initialTexture_4; }
	inline Texture_t3661962703 ** get_address_of_initialTexture_4() { return &___initialTexture_4; }
	inline void set_initialTexture_4(Texture_t3661962703 * value)
	{
		___initialTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___initialTexture_4), value);
	}

	inline static int32_t get_offset_of_surfaceTexture_5() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___surfaceTexture_5)); }
	inline Texture_t3661962703 * get_surfaceTexture_5() const { return ___surfaceTexture_5; }
	inline Texture_t3661962703 ** get_address_of_surfaceTexture_5() { return &___surfaceTexture_5; }
	inline void set_surfaceTexture_5(Texture_t3661962703 * value)
	{
		___surfaceTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceTexture_5), value);
	}

	inline static int32_t get_offset_of_videoMatrixRaw_6() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoMatrixRaw_6)); }
	inline SingleU5BU5D_t1444911251* get_videoMatrixRaw_6() const { return ___videoMatrixRaw_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_videoMatrixRaw_6() { return &___videoMatrixRaw_6; }
	inline void set_videoMatrixRaw_6(SingleU5BU5D_t1444911251* value)
	{
		___videoMatrixRaw_6 = value;
		Il2CppCodeGenWriteBarrier((&___videoMatrixRaw_6), value);
	}

	inline static int32_t get_offset_of_videoMatrix_7() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoMatrix_7)); }
	inline Matrix4x4_t1817901843  get_videoMatrix_7() const { return ___videoMatrix_7; }
	inline Matrix4x4_t1817901843 * get_address_of_videoMatrix_7() { return &___videoMatrix_7; }
	inline void set_videoMatrix_7(Matrix4x4_t1817901843  value)
	{
		___videoMatrix_7 = value;
	}

	inline static int32_t get_offset_of_videoMatrixPropertyId_8() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoMatrixPropertyId_8)); }
	inline int32_t get_videoMatrixPropertyId_8() const { return ___videoMatrixPropertyId_8; }
	inline int32_t* get_address_of_videoMatrixPropertyId_8() { return &___videoMatrixPropertyId_8; }
	inline void set_videoMatrixPropertyId_8(int32_t value)
	{
		___videoMatrixPropertyId_8 = value;
	}

	inline static int32_t get_offset_of_lastVideoTimestamp_9() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___lastVideoTimestamp_9)); }
	inline int64_t get_lastVideoTimestamp_9() const { return ___lastVideoTimestamp_9; }
	inline int64_t* get_address_of_lastVideoTimestamp_9() { return &___lastVideoTimestamp_9; }
	inline void set_lastVideoTimestamp_9(int64_t value)
	{
		___lastVideoTimestamp_9 = value;
	}

	inline static int32_t get_offset_of_initialized_10() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___initialized_10)); }
	inline bool get_initialized_10() const { return ___initialized_10; }
	inline bool* get_address_of_initialized_10() { return &___initialized_10; }
	inline void set_initialized_10(bool value)
	{
		___initialized_10 = value;
	}

	inline static int32_t get_offset_of_texWidth_11() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___texWidth_11)); }
	inline int32_t get_texWidth_11() const { return ___texWidth_11; }
	inline int32_t* get_address_of_texWidth_11() { return &___texWidth_11; }
	inline void set_texWidth_11(int32_t value)
	{
		___texWidth_11 = value;
	}

	inline static int32_t get_offset_of_texHeight_12() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___texHeight_12)); }
	inline int32_t get_texHeight_12() const { return ___texHeight_12; }
	inline int32_t* get_address_of_texHeight_12() { return &___texHeight_12; }
	inline void set_texHeight_12(int32_t value)
	{
		___texHeight_12 = value;
	}

	inline static int32_t get_offset_of_lastBufferedPosition_13() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___lastBufferedPosition_13)); }
	inline int64_t get_lastBufferedPosition_13() const { return ___lastBufferedPosition_13; }
	inline int64_t* get_address_of_lastBufferedPosition_13() { return &___lastBufferedPosition_13; }
	inline void set_lastBufferedPosition_13(int64_t value)
	{
		___lastBufferedPosition_13 = value;
	}

	inline static int32_t get_offset_of_framecount_14() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___framecount_14)); }
	inline float get_framecount_14() const { return ___framecount_14; }
	inline float* get_address_of_framecount_14() { return &___framecount_14; }
	inline void set_framecount_14(float value)
	{
		___framecount_14 = value;
	}

	inline static int32_t get_offset_of_screen_15() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___screen_15)); }
	inline Renderer_t2627027031 * get_screen_15() const { return ___screen_15; }
	inline Renderer_t2627027031 ** get_address_of_screen_15() { return &___screen_15; }
	inline void set_screen_15(Renderer_t2627027031 * value)
	{
		___screen_15 = value;
		Il2CppCodeGenWriteBarrier((&___screen_15), value);
	}

	inline static int32_t get_offset_of_renderEventFunction_16() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___renderEventFunction_16)); }
	inline intptr_t get_renderEventFunction_16() const { return ___renderEventFunction_16; }
	inline intptr_t* get_address_of_renderEventFunction_16() { return &___renderEventFunction_16; }
	inline void set_renderEventFunction_16(intptr_t value)
	{
		___renderEventFunction_16 = value;
	}

	inline static int32_t get_offset_of_playOnResume_17() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___playOnResume_17)); }
	inline bool get_playOnResume_17() const { return ___playOnResume_17; }
	inline bool* get_address_of_playOnResume_17() { return &___playOnResume_17; }
	inline void set_playOnResume_17(bool value)
	{
		___playOnResume_17 = value;
	}

	inline static int32_t get_offset_of_onEventCallbacks_18() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___onEventCallbacks_18)); }
	inline List_1_t300520794 * get_onEventCallbacks_18() const { return ___onEventCallbacks_18; }
	inline List_1_t300520794 ** get_address_of_onEventCallbacks_18() { return &___onEventCallbacks_18; }
	inline void set_onEventCallbacks_18(List_1_t300520794 * value)
	{
		___onEventCallbacks_18 = value;
		Il2CppCodeGenWriteBarrier((&___onEventCallbacks_18), value);
	}

	inline static int32_t get_offset_of_onExceptionCallbacks_19() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___onExceptionCallbacks_19)); }
	inline List_1_t1147278120 * get_onExceptionCallbacks_19() const { return ___onExceptionCallbacks_19; }
	inline List_1_t1147278120 ** get_address_of_onExceptionCallbacks_19() { return &___onExceptionCallbacks_19; }
	inline void set_onExceptionCallbacks_19(List_1_t1147278120 * value)
	{
		___onExceptionCallbacks_19 = value;
		Il2CppCodeGenWriteBarrier((&___onExceptionCallbacks_19), value);
	}

	inline static int32_t get_offset_of_statusText_21() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___statusText_21)); }
	inline Text_t1901882714 * get_statusText_21() const { return ___statusText_21; }
	inline Text_t1901882714 ** get_address_of_statusText_21() { return &___statusText_21; }
	inline void set_statusText_21(Text_t1901882714 * value)
	{
		___statusText_21 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_21), value);
	}

	inline static int32_t get_offset_of_videoType_22() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoType_22)); }
	inline int32_t get_videoType_22() const { return ___videoType_22; }
	inline int32_t* get_address_of_videoType_22() { return &___videoType_22; }
	inline void set_videoType_22(int32_t value)
	{
		___videoType_22 = value;
	}

	inline static int32_t get_offset_of_videoURL_23() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoURL_23)); }
	inline String_t* get_videoURL_23() const { return ___videoURL_23; }
	inline String_t** get_address_of_videoURL_23() { return &___videoURL_23; }
	inline void set_videoURL_23(String_t* value)
	{
		___videoURL_23 = value;
		Il2CppCodeGenWriteBarrier((&___videoURL_23), value);
	}

	inline static int32_t get_offset_of_videoContentID_24() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoContentID_24)); }
	inline String_t* get_videoContentID_24() const { return ___videoContentID_24; }
	inline String_t** get_address_of_videoContentID_24() { return &___videoContentID_24; }
	inline void set_videoContentID_24(String_t* value)
	{
		___videoContentID_24 = value;
		Il2CppCodeGenWriteBarrier((&___videoContentID_24), value);
	}

	inline static int32_t get_offset_of_videoProviderId_25() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoProviderId_25)); }
	inline String_t* get_videoProviderId_25() const { return ___videoProviderId_25; }
	inline String_t** get_address_of_videoProviderId_25() { return &___videoProviderId_25; }
	inline void set_videoProviderId_25(String_t* value)
	{
		___videoProviderId_25 = value;
		Il2CppCodeGenWriteBarrier((&___videoProviderId_25), value);
	}

	inline static int32_t get_offset_of_initialResolution_26() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___initialResolution_26)); }
	inline int32_t get_initialResolution_26() const { return ___initialResolution_26; }
	inline int32_t* get_address_of_initialResolution_26() { return &___initialResolution_26; }
	inline void set_initialResolution_26(int32_t value)
	{
		___initialResolution_26 = value;
	}

	inline static int32_t get_offset_of_adjustAspectRatio_27() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___adjustAspectRatio_27)); }
	inline bool get_adjustAspectRatio_27() const { return ___adjustAspectRatio_27; }
	inline bool* get_address_of_adjustAspectRatio_27() { return &___adjustAspectRatio_27; }
	inline void set_adjustAspectRatio_27(bool value)
	{
		___adjustAspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_useSecurePath_28() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___useSecurePath_28)); }
	inline bool get_useSecurePath_28() const { return ___useSecurePath_28; }
	inline bool* get_address_of_useSecurePath_28() { return &___useSecurePath_28; }
	inline void set_useSecurePath_28(bool value)
	{
		___useSecurePath_28 = value;
	}
};

struct GvrVideoPlayerTexture_t3546202735_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<System.Action> GvrVideoPlayerTexture::ExecuteOnMainThread
	Queue_1_t1110636971 * ___ExecuteOnMainThread_20;
	// System.Action`2<System.String,System.String> GvrVideoPlayerTexture::<>f__am$cache0
	Action_2_t3970170674 * ___U3CU3Ef__amU24cache0_30;
	// GvrVideoPlayerTexture/OnVideoEventCallback GvrVideoPlayerTexture::<>f__mg$cache0
	OnVideoEventCallback_t2376626694 * ___U3CU3Ef__mgU24cache0_31;
	// GvrVideoPlayerTexture/OnExceptionCallback GvrVideoPlayerTexture::<>f__mg$cache1
	OnExceptionCallback_t1696428116 * ___U3CU3Ef__mgU24cache1_32;

public:
	inline static int32_t get_offset_of_ExecuteOnMainThread_20() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735_StaticFields, ___ExecuteOnMainThread_20)); }
	inline Queue_1_t1110636971 * get_ExecuteOnMainThread_20() const { return ___ExecuteOnMainThread_20; }
	inline Queue_1_t1110636971 ** get_address_of_ExecuteOnMainThread_20() { return &___ExecuteOnMainThread_20; }
	inline void set_ExecuteOnMainThread_20(Queue_1_t1110636971 * value)
	{
		___ExecuteOnMainThread_20 = value;
		Il2CppCodeGenWriteBarrier((&___ExecuteOnMainThread_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_30() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735_StaticFields, ___U3CU3Ef__amU24cache0_30)); }
	inline Action_2_t3970170674 * get_U3CU3Ef__amU24cache0_30() const { return ___U3CU3Ef__amU24cache0_30; }
	inline Action_2_t3970170674 ** get_address_of_U3CU3Ef__amU24cache0_30() { return &___U3CU3Ef__amU24cache0_30; }
	inline void set_U3CU3Ef__amU24cache0_30(Action_2_t3970170674 * value)
	{
		___U3CU3Ef__amU24cache0_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_31() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735_StaticFields, ___U3CU3Ef__mgU24cache0_31)); }
	inline OnVideoEventCallback_t2376626694 * get_U3CU3Ef__mgU24cache0_31() const { return ___U3CU3Ef__mgU24cache0_31; }
	inline OnVideoEventCallback_t2376626694 ** get_address_of_U3CU3Ef__mgU24cache0_31() { return &___U3CU3Ef__mgU24cache0_31; }
	inline void set_U3CU3Ef__mgU24cache0_31(OnVideoEventCallback_t2376626694 * value)
	{
		___U3CU3Ef__mgU24cache0_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_32() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735_StaticFields, ___U3CU3Ef__mgU24cache1_32)); }
	inline OnExceptionCallback_t1696428116 * get_U3CU3Ef__mgU24cache1_32() const { return ___U3CU3Ef__mgU24cache1_32; }
	inline OnExceptionCallback_t1696428116 ** get_address_of_U3CU3Ef__mgU24cache1_32() { return &___U3CU3Ef__mgU24cache1_32; }
	inline void set_U3CU3Ef__mgU24cache1_32(OnExceptionCallback_t1696428116 * value)
	{
		___U3CU3Ef__mgU24cache1_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRVIDEOPLAYERTEXTURE_T3546202735_H
#ifndef GVRBASEPOINTER_T822782720_H
#define GVRBASEPOINTER_T822782720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer
struct  GvrBasePointer_t822782720  : public MonoBehaviour_t3962482529
{
public:
	// GvrBasePointer/RaycastMode GvrBasePointer::raycastMode
	int32_t ___raycastMode_2;
	// UnityEngine.Camera GvrBasePointer::overridePointerCamera
	Camera_t4157153871 * ___overridePointerCamera_3;
	// System.Boolean GvrBasePointer::<ShouldUseExitRadiusForRaycast>k__BackingField
	bool ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_raycastMode_2() { return static_cast<int32_t>(offsetof(GvrBasePointer_t822782720, ___raycastMode_2)); }
	inline int32_t get_raycastMode_2() const { return ___raycastMode_2; }
	inline int32_t* get_address_of_raycastMode_2() { return &___raycastMode_2; }
	inline void set_raycastMode_2(int32_t value)
	{
		___raycastMode_2 = value;
	}

	inline static int32_t get_offset_of_overridePointerCamera_3() { return static_cast<int32_t>(offsetof(GvrBasePointer_t822782720, ___overridePointerCamera_3)); }
	inline Camera_t4157153871 * get_overridePointerCamera_3() const { return ___overridePointerCamera_3; }
	inline Camera_t4157153871 ** get_address_of_overridePointerCamera_3() { return &___overridePointerCamera_3; }
	inline void set_overridePointerCamera_3(Camera_t4157153871 * value)
	{
		___overridePointerCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___overridePointerCamera_3), value);
	}

	inline static int32_t get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GvrBasePointer_t822782720, ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4)); }
	inline bool get_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4() const { return ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4() { return &___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4; }
	inline void set_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4(bool value)
	{
		___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRBASEPOINTER_T822782720_H
#ifndef INSTANTPREVIEWHELPER_T1993029064_H
#define INSTANTPREVIEWHELPER_T1993029064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantPreviewHelper
struct  InstantPreviewHelper_t1993029064  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstantPreviewHelper_t1993029064_StaticFields
{
public:
	// System.String InstantPreviewHelper::AdbPath
	String_t* ___AdbPath_2;

public:
	inline static int32_t get_offset_of_AdbPath_2() { return static_cast<int32_t>(offsetof(InstantPreviewHelper_t1993029064_StaticFields, ___AdbPath_2)); }
	inline String_t* get_AdbPath_2() const { return ___AdbPath_2; }
	inline String_t** get_address_of_AdbPath_2() { return &___AdbPath_2; }
	inline void set_AdbPath_2(String_t* value)
	{
		___AdbPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___AdbPath_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTPREVIEWHELPER_T1993029064_H
#ifndef GVRKEYBOARDDELEGATEBASE_T30895224_H
#define GVRKEYBOARDDELEGATEBASE_T30895224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardDelegateBase
struct  GvrKeyboardDelegateBase_t30895224  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDDELEGATEBASE_T30895224_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_2)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_3)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_4)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_5)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_6)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_7)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef BASERAYCASTER_T4150874583_H
#define BASERAYCASTER_T4150874583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t4150874583  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T4150874583_H
#ifndef GVRPOINTERINPUTMODULE_T4124566278_H
#define GVRPOINTERINPUTMODULE_T4124566278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerInputModule
struct  GvrPointerInputModule_t4124566278  : public BaseInputModule_t2019268878
{
public:
	// System.Boolean GvrPointerInputModule::vrModeOnly
	bool ___vrModeOnly_8;
	// GvrPointerScrollInput GvrPointerInputModule::scrollInput
	GvrPointerScrollInput_t3738414627 * ___scrollInput_9;
	// GvrPointerInputModuleImpl GvrPointerInputModule::<Impl>k__BackingField
	GvrPointerInputModuleImpl_t2260544298 * ___U3CImplU3Ek__BackingField_10;
	// GvrEventExecutor GvrPointerInputModule::<EventExecutor>k__BackingField
	GvrEventExecutor_t2551165091 * ___U3CEventExecutorU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_vrModeOnly_8() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t4124566278, ___vrModeOnly_8)); }
	inline bool get_vrModeOnly_8() const { return ___vrModeOnly_8; }
	inline bool* get_address_of_vrModeOnly_8() { return &___vrModeOnly_8; }
	inline void set_vrModeOnly_8(bool value)
	{
		___vrModeOnly_8 = value;
	}

	inline static int32_t get_offset_of_scrollInput_9() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t4124566278, ___scrollInput_9)); }
	inline GvrPointerScrollInput_t3738414627 * get_scrollInput_9() const { return ___scrollInput_9; }
	inline GvrPointerScrollInput_t3738414627 ** get_address_of_scrollInput_9() { return &___scrollInput_9; }
	inline void set_scrollInput_9(GvrPointerScrollInput_t3738414627 * value)
	{
		___scrollInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___scrollInput_9), value);
	}

	inline static int32_t get_offset_of_U3CImplU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t4124566278, ___U3CImplU3Ek__BackingField_10)); }
	inline GvrPointerInputModuleImpl_t2260544298 * get_U3CImplU3Ek__BackingField_10() const { return ___U3CImplU3Ek__BackingField_10; }
	inline GvrPointerInputModuleImpl_t2260544298 ** get_address_of_U3CImplU3Ek__BackingField_10() { return &___U3CImplU3Ek__BackingField_10; }
	inline void set_U3CImplU3Ek__BackingField_10(GvrPointerInputModuleImpl_t2260544298 * value)
	{
		___U3CImplU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImplU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CEventExecutorU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t4124566278, ___U3CEventExecutorU3Ek__BackingField_11)); }
	inline GvrEventExecutor_t2551165091 * get_U3CEventExecutorU3Ek__BackingField_11() const { return ___U3CEventExecutorU3Ek__BackingField_11; }
	inline GvrEventExecutor_t2551165091 ** get_address_of_U3CEventExecutorU3Ek__BackingField_11() { return &___U3CEventExecutorU3Ek__BackingField_11; }
	inline void set_U3CEventExecutorU3Ek__BackingField_11(GvrEventExecutor_t2551165091 * value)
	{
		___U3CEventExecutorU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventExecutorU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERINPUTMODULE_T4124566278_H
#ifndef GVRBASEPOINTERRAYCASTER_T3158030611_H
#define GVRBASEPOINTERRAYCASTER_T3158030611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointerRaycaster
struct  GvrBasePointerRaycaster_t3158030611  : public BaseRaycaster_t4150874583
{
public:
	// GvrBasePointer/PointerRay GvrBasePointerRaycaster::lastRay
	PointerRay_t3451016640  ___lastRay_2;
	// GvrBasePointer/RaycastMode GvrBasePointerRaycaster::<CurrentRaycastModeForHybrid>k__BackingField
	int32_t ___U3CCurrentRaycastModeForHybridU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_lastRay_2() { return static_cast<int32_t>(offsetof(GvrBasePointerRaycaster_t3158030611, ___lastRay_2)); }
	inline PointerRay_t3451016640  get_lastRay_2() const { return ___lastRay_2; }
	inline PointerRay_t3451016640 * get_address_of_lastRay_2() { return &___lastRay_2; }
	inline void set_lastRay_2(PointerRay_t3451016640  value)
	{
		___lastRay_2 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentRaycastModeForHybridU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GvrBasePointerRaycaster_t3158030611, ___U3CCurrentRaycastModeForHybridU3Ek__BackingField_3)); }
	inline int32_t get_U3CCurrentRaycastModeForHybridU3Ek__BackingField_3() const { return ___U3CCurrentRaycastModeForHybridU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCurrentRaycastModeForHybridU3Ek__BackingField_3() { return &___U3CCurrentRaycastModeForHybridU3Ek__BackingField_3; }
	inline void set_U3CCurrentRaycastModeForHybridU3Ek__BackingField_3(int32_t value)
	{
		___U3CCurrentRaycastModeForHybridU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRBASEPOINTERRAYCASTER_T3158030611_H
#ifndef DROPDOWN_T2274391225_H
#define DROPDOWN_T2274391225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Dropdown
struct  Dropdown_t2274391225  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t3704657025 * ___m_Template_16;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_t1901882714 * ___m_CaptionText_17;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_t2670269651 * ___m_CaptionImage_18;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_t1901882714 * ___m_ItemText_19;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_t2670269651 * ___m_ItemImage_20;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_21;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t1438173104 * ___m_Options_22;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t4040729994 * ___m_OnValueChanged_23;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_t1113636619 * ___m_Dropdown_24;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_t1113636619 * ___m_Blocker_25;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t2924027637 * ___m_Items_26;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t3520241082 * ___m_AlphaTweenRunner_27;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_28;

public:
	inline static int32_t get_offset_of_m_Template_16() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Template_16)); }
	inline RectTransform_t3704657025 * get_m_Template_16() const { return ___m_Template_16; }
	inline RectTransform_t3704657025 ** get_address_of_m_Template_16() { return &___m_Template_16; }
	inline void set_m_Template_16(RectTransform_t3704657025 * value)
	{
		___m_Template_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Template_16), value);
	}

	inline static int32_t get_offset_of_m_CaptionText_17() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionText_17)); }
	inline Text_t1901882714 * get_m_CaptionText_17() const { return ___m_CaptionText_17; }
	inline Text_t1901882714 ** get_address_of_m_CaptionText_17() { return &___m_CaptionText_17; }
	inline void set_m_CaptionText_17(Text_t1901882714 * value)
	{
		___m_CaptionText_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionText_17), value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_18() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionImage_18)); }
	inline Image_t2670269651 * get_m_CaptionImage_18() const { return ___m_CaptionImage_18; }
	inline Image_t2670269651 ** get_address_of_m_CaptionImage_18() { return &___m_CaptionImage_18; }
	inline void set_m_CaptionImage_18(Image_t2670269651 * value)
	{
		___m_CaptionImage_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionImage_18), value);
	}

	inline static int32_t get_offset_of_m_ItemText_19() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemText_19)); }
	inline Text_t1901882714 * get_m_ItemText_19() const { return ___m_ItemText_19; }
	inline Text_t1901882714 ** get_address_of_m_ItemText_19() { return &___m_ItemText_19; }
	inline void set_m_ItemText_19(Text_t1901882714 * value)
	{
		___m_ItemText_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemText_19), value);
	}

	inline static int32_t get_offset_of_m_ItemImage_20() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemImage_20)); }
	inline Image_t2670269651 * get_m_ItemImage_20() const { return ___m_ItemImage_20; }
	inline Image_t2670269651 ** get_address_of_m_ItemImage_20() { return &___m_ItemImage_20; }
	inline void set_m_ItemImage_20(Image_t2670269651 * value)
	{
		___m_ItemImage_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemImage_20), value);
	}

	inline static int32_t get_offset_of_m_Value_21() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Value_21)); }
	inline int32_t get_m_Value_21() const { return ___m_Value_21; }
	inline int32_t* get_address_of_m_Value_21() { return &___m_Value_21; }
	inline void set_m_Value_21(int32_t value)
	{
		___m_Value_21 = value;
	}

	inline static int32_t get_offset_of_m_Options_22() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Options_22)); }
	inline OptionDataList_t1438173104 * get_m_Options_22() const { return ___m_Options_22; }
	inline OptionDataList_t1438173104 ** get_address_of_m_Options_22() { return &___m_Options_22; }
	inline void set_m_Options_22(OptionDataList_t1438173104 * value)
	{
		___m_Options_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_22), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_23() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_OnValueChanged_23)); }
	inline DropdownEvent_t4040729994 * get_m_OnValueChanged_23() const { return ___m_OnValueChanged_23; }
	inline DropdownEvent_t4040729994 ** get_address_of_m_OnValueChanged_23() { return &___m_OnValueChanged_23; }
	inline void set_m_OnValueChanged_23(DropdownEvent_t4040729994 * value)
	{
		___m_OnValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_23), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_24() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Dropdown_24)); }
	inline GameObject_t1113636619 * get_m_Dropdown_24() const { return ___m_Dropdown_24; }
	inline GameObject_t1113636619 ** get_address_of_m_Dropdown_24() { return &___m_Dropdown_24; }
	inline void set_m_Dropdown_24(GameObject_t1113636619 * value)
	{
		___m_Dropdown_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_24), value);
	}

	inline static int32_t get_offset_of_m_Blocker_25() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Blocker_25)); }
	inline GameObject_t1113636619 * get_m_Blocker_25() const { return ___m_Blocker_25; }
	inline GameObject_t1113636619 ** get_address_of_m_Blocker_25() { return &___m_Blocker_25; }
	inline void set_m_Blocker_25(GameObject_t1113636619 * value)
	{
		___m_Blocker_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blocker_25), value);
	}

	inline static int32_t get_offset_of_m_Items_26() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Items_26)); }
	inline List_1_t2924027637 * get_m_Items_26() const { return ___m_Items_26; }
	inline List_1_t2924027637 ** get_address_of_m_Items_26() { return &___m_Items_26; }
	inline void set_m_Items_26(List_1_t2924027637 * value)
	{
		___m_Items_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Items_26), value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_27() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_AlphaTweenRunner_27)); }
	inline TweenRunner_1_t3520241082 * get_m_AlphaTweenRunner_27() const { return ___m_AlphaTweenRunner_27; }
	inline TweenRunner_1_t3520241082 ** get_address_of_m_AlphaTweenRunner_27() { return &___m_AlphaTweenRunner_27; }
	inline void set_m_AlphaTweenRunner_27(TweenRunner_1_t3520241082 * value)
	{
		___m_AlphaTweenRunner_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaTweenRunner_27), value);
	}

	inline static int32_t get_offset_of_validTemplate_28() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___validTemplate_28)); }
	inline bool get_validTemplate_28() const { return ___validTemplate_28; }
	inline bool* get_address_of_validTemplate_28() { return &___validTemplate_28; }
	inline void set_validTemplate_28(bool value)
	{
		___validTemplate_28 = value;
	}
};

struct Dropdown_t2274391225_StaticFields
{
public:
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t3270282352 * ___s_NoOptionData_29;

public:
	inline static int32_t get_offset_of_s_NoOptionData_29() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225_StaticFields, ___s_NoOptionData_29)); }
	inline OptionData_t3270282352 * get_s_NoOptionData_29() const { return ___s_NoOptionData_29; }
	inline OptionData_t3270282352 ** get_address_of_s_NoOptionData_29() { return &___s_NoOptionData_29; }
	inline void set_s_NoOptionData_29(OptionData_t3270282352 * value)
	{
		___s_NoOptionData_29 = value;
		Il2CppCodeGenWriteBarrier((&___s_NoOptionData_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWN_T2274391225_H
#ifndef GVRPOINTERPHYSICSRAYCASTER_T304947617_H
#define GVRPOINTERPHYSICSRAYCASTER_T304947617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerPhysicsRaycaster
struct  GvrPointerPhysicsRaycaster_t304947617  : public GvrBasePointerRaycaster_t3158030611
{
public:
	// UnityEngine.LayerMask GvrPointerPhysicsRaycaster::raycasterEventMask
	LayerMask_t3493934918  ___raycasterEventMask_6;
	// System.Int32 GvrPointerPhysicsRaycaster::maxRaycastHits
	int32_t ___maxRaycastHits_7;
	// UnityEngine.RaycastHit[] GvrPointerPhysicsRaycaster::hits
	RaycastHitU5BU5D_t1690781147* ___hits_8;
	// GvrPointerPhysicsRaycaster/HitComparer GvrPointerPhysicsRaycaster::hitComparer
	HitComparer_t1984117280 * ___hitComparer_9;

public:
	inline static int32_t get_offset_of_raycasterEventMask_6() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_t304947617, ___raycasterEventMask_6)); }
	inline LayerMask_t3493934918  get_raycasterEventMask_6() const { return ___raycasterEventMask_6; }
	inline LayerMask_t3493934918 * get_address_of_raycasterEventMask_6() { return &___raycasterEventMask_6; }
	inline void set_raycasterEventMask_6(LayerMask_t3493934918  value)
	{
		___raycasterEventMask_6 = value;
	}

	inline static int32_t get_offset_of_maxRaycastHits_7() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_t304947617, ___maxRaycastHits_7)); }
	inline int32_t get_maxRaycastHits_7() const { return ___maxRaycastHits_7; }
	inline int32_t* get_address_of_maxRaycastHits_7() { return &___maxRaycastHits_7; }
	inline void set_maxRaycastHits_7(int32_t value)
	{
		___maxRaycastHits_7 = value;
	}

	inline static int32_t get_offset_of_hits_8() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_t304947617, ___hits_8)); }
	inline RaycastHitU5BU5D_t1690781147* get_hits_8() const { return ___hits_8; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hits_8() { return &___hits_8; }
	inline void set_hits_8(RaycastHitU5BU5D_t1690781147* value)
	{
		___hits_8 = value;
		Il2CppCodeGenWriteBarrier((&___hits_8), value);
	}

	inline static int32_t get_offset_of_hitComparer_9() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_t304947617, ___hitComparer_9)); }
	inline HitComparer_t1984117280 * get_hitComparer_9() const { return ___hitComparer_9; }
	inline HitComparer_t1984117280 ** get_address_of_hitComparer_9() { return &___hitComparer_9; }
	inline void set_hitComparer_9(HitComparer_t1984117280 * value)
	{
		___hitComparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___hitComparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERPHYSICSRAYCASTER_T304947617_H
#ifndef GVRDROPDOWN_T2772907210_H
#define GVRDROPDOWN_T2772907210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrDropdown
struct  GvrDropdown_t2772907210  : public Dropdown_t2274391225
{
public:
	// UnityEngine.GameObject GvrDropdown::currentBlocker
	GameObject_t1113636619 * ___currentBlocker_30;

public:
	inline static int32_t get_offset_of_currentBlocker_30() { return static_cast<int32_t>(offsetof(GvrDropdown_t2772907210, ___currentBlocker_30)); }
	inline GameObject_t1113636619 * get_currentBlocker_30() const { return ___currentBlocker_30; }
	inline GameObject_t1113636619 ** get_address_of_currentBlocker_30() { return &___currentBlocker_30; }
	inline void set_currentBlocker_30(GameObject_t1113636619 * value)
	{
		___currentBlocker_30 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlocker_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRDROPDOWN_T2772907210_H
#ifndef GVRPOINTERGRAPHICRAYCASTER_T348070255_H
#define GVRPOINTERGRAPHICRAYCASTER_T348070255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerGraphicRaycaster
struct  GvrPointerGraphicRaycaster_t348070255  : public GvrBasePointerRaycaster_t3158030611
{
public:
	// System.Boolean GvrPointerGraphicRaycaster::ignoreReversedGraphics
	bool ___ignoreReversedGraphics_5;
	// GvrPointerGraphicRaycaster/BlockingObjects GvrPointerGraphicRaycaster::blockingObjects
	int32_t ___blockingObjects_6;
	// UnityEngine.LayerMask GvrPointerGraphicRaycaster::blockingMask
	LayerMask_t3493934918  ___blockingMask_7;
	// UnityEngine.Canvas GvrPointerGraphicRaycaster::targetCanvas
	Canvas_t3310196443 * ___targetCanvas_8;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> GvrPointerGraphicRaycaster::raycastResults
	List_1_t3132410353 * ___raycastResults_9;
	// UnityEngine.Camera GvrPointerGraphicRaycaster::cachedPointerEventCamera
	Camera_t4157153871 * ___cachedPointerEventCamera_10;

public:
	inline static int32_t get_offset_of_ignoreReversedGraphics_5() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___ignoreReversedGraphics_5)); }
	inline bool get_ignoreReversedGraphics_5() const { return ___ignoreReversedGraphics_5; }
	inline bool* get_address_of_ignoreReversedGraphics_5() { return &___ignoreReversedGraphics_5; }
	inline void set_ignoreReversedGraphics_5(bool value)
	{
		___ignoreReversedGraphics_5 = value;
	}

	inline static int32_t get_offset_of_blockingObjects_6() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___blockingObjects_6)); }
	inline int32_t get_blockingObjects_6() const { return ___blockingObjects_6; }
	inline int32_t* get_address_of_blockingObjects_6() { return &___blockingObjects_6; }
	inline void set_blockingObjects_6(int32_t value)
	{
		___blockingObjects_6 = value;
	}

	inline static int32_t get_offset_of_blockingMask_7() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___blockingMask_7)); }
	inline LayerMask_t3493934918  get_blockingMask_7() const { return ___blockingMask_7; }
	inline LayerMask_t3493934918 * get_address_of_blockingMask_7() { return &___blockingMask_7; }
	inline void set_blockingMask_7(LayerMask_t3493934918  value)
	{
		___blockingMask_7 = value;
	}

	inline static int32_t get_offset_of_targetCanvas_8() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___targetCanvas_8)); }
	inline Canvas_t3310196443 * get_targetCanvas_8() const { return ___targetCanvas_8; }
	inline Canvas_t3310196443 ** get_address_of_targetCanvas_8() { return &___targetCanvas_8; }
	inline void set_targetCanvas_8(Canvas_t3310196443 * value)
	{
		___targetCanvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetCanvas_8), value);
	}

	inline static int32_t get_offset_of_raycastResults_9() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___raycastResults_9)); }
	inline List_1_t3132410353 * get_raycastResults_9() const { return ___raycastResults_9; }
	inline List_1_t3132410353 ** get_address_of_raycastResults_9() { return &___raycastResults_9; }
	inline void set_raycastResults_9(List_1_t3132410353 * value)
	{
		___raycastResults_9 = value;
		Il2CppCodeGenWriteBarrier((&___raycastResults_9), value);
	}

	inline static int32_t get_offset_of_cachedPointerEventCamera_10() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___cachedPointerEventCamera_10)); }
	inline Camera_t4157153871 * get_cachedPointerEventCamera_10() const { return ___cachedPointerEventCamera_10; }
	inline Camera_t4157153871 ** get_address_of_cachedPointerEventCamera_10() { return &___cachedPointerEventCamera_10; }
	inline void set_cachedPointerEventCamera_10(Camera_t4157153871 * value)
	{
		___cachedPointerEventCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedPointerEventCamera_10), value);
	}
};

struct GvrPointerGraphicRaycaster_t348070255_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> GvrPointerGraphicRaycaster::sortedGraphics
	List_1_t3132410353 * ___sortedGraphics_11;
	// System.Comparison`1<UnityEngine.UI.Graphic> GvrPointerGraphicRaycaster::<>f__am$cache0
	Comparison_1_t1435266790 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_sortedGraphics_11() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255_StaticFields, ___sortedGraphics_11)); }
	inline List_1_t3132410353 * get_sortedGraphics_11() const { return ___sortedGraphics_11; }
	inline List_1_t3132410353 ** get_address_of_sortedGraphics_11() { return &___sortedGraphics_11; }
	inline void set_sortedGraphics_11(List_1_t3132410353 * value)
	{
		___sortedGraphics_11 = value;
		Il2CppCodeGenWriteBarrier((&___sortedGraphics_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Comparison_1_t1435266790 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Comparison_1_t1435266790 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Comparison_1_t1435266790 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERGRAPHICRAYCASTER_T348070255_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (Builder_t2712992173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[2] = 
{
	Builder_t2712992173::get_offset_of_resultIsReadOnly_0(),
	Builder_t2712992173::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (Builder_t895705486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[2] = 
{
	Builder_t895705486::get_offset_of_resultIsReadOnly_0(),
	Builder_t895705486::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (GvrControllerTooltipsSimple_t2073124053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[4] = 
{
	GvrControllerTooltipsSimple_t2073124053::get_offset_of_tooltipRenderer_2(),
	GvrControllerTooltipsSimple_t2073124053::get_offset_of_U3CArmModelU3Ek__BackingField_3(),
	GvrControllerTooltipsSimple_t2073124053::get_offset_of_materialPropertyBlock_4(),
	GvrControllerTooltipsSimple_t2073124053::get_offset_of_colorId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (GvrTooltip_t491261851), -1, sizeof(GvrTooltip_t491261851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2504[14] = 
{
	GvrTooltip_t491261851_StaticFields::get_offset_of_RIGHT_SIDE_ROTATION_2(),
	GvrTooltip_t491261851_StaticFields::get_offset_of_LEFT_SIDE_ROTATION_3(),
	GvrTooltip_t491261851_StaticFields::get_offset_of_SQUARE_CENTER_4(),
	GvrTooltip_t491261851_StaticFields::get_offset_of_PIVOT_5(),
	0,
	0,
	0,
	GvrTooltip_t491261851::get_offset_of_location_9(),
	GvrTooltip_t491261851::get_offset_of_text_10(),
	GvrTooltip_t491261851::get_offset_of_alwaysVisible_11(),
	GvrTooltip_t491261851::get_offset_of_isOnLeft_12(),
	GvrTooltip_t491261851::get_offset_of_rectTransform_13(),
	GvrTooltip_t491261851::get_offset_of_canvasGroup_14(),
	GvrTooltip_t491261851::get_offset_of_U3CArmModelU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (Location_t4049147969)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2505[6] = 
{
	Location_t4049147969::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (GvrBasePointer_t822782720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[3] = 
{
	GvrBasePointer_t822782720::get_offset_of_raycastMode_2(),
	GvrBasePointer_t822782720::get_offset_of_overridePointerCamera_3(),
	GvrBasePointer_t822782720::get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (RaycastMode_t1421504155)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[4] = 
{
	RaycastMode_t1421504155::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (PointerRay_t3451016640)+ sizeof (RuntimeObject), sizeof(PointerRay_t3451016640 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2508[3] = 
{
	PointerRay_t3451016640::get_offset_of_ray_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointerRay_t3451016640::get_offset_of_distanceFromStart_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointerRay_t3451016640::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (GvrBasePointerRaycaster_t3158030611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[2] = 
{
	GvrBasePointerRaycaster_t3158030611::get_offset_of_lastRay_2(),
	GvrBasePointerRaycaster_t3158030611::get_offset_of_U3CCurrentRaycastModeForHybridU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (GvrExecuteEventsExtension_t3940832397), -1, sizeof(GvrExecuteEventsExtension_t3940832397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2511[2] = 
{
	GvrExecuteEventsExtension_t3940832397_StaticFields::get_offset_of_s_HoverHandler_0(),
	GvrExecuteEventsExtension_t3940832397_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (GvrPointerGraphicRaycaster_t348070255), -1, sizeof(GvrPointerGraphicRaycaster_t348070255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2512[9] = 
{
	0,
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_ignoreReversedGraphics_5(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_blockingObjects_6(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_blockingMask_7(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_targetCanvas_8(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_raycastResults_9(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_cachedPointerEventCamera_10(),
	GvrPointerGraphicRaycaster_t348070255_StaticFields::get_offset_of_sortedGraphics_11(),
	GvrPointerGraphicRaycaster_t348070255_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (BlockingObjects_t813457210)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2513[5] = 
{
	BlockingObjects_t813457210::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (GvrPointerPhysicsRaycaster_t304947617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[6] = 
{
	0,
	0,
	GvrPointerPhysicsRaycaster_t304947617::get_offset_of_raycasterEventMask_6(),
	GvrPointerPhysicsRaycaster_t304947617::get_offset_of_maxRaycastHits_7(),
	GvrPointerPhysicsRaycaster_t304947617::get_offset_of_hits_8(),
	GvrPointerPhysicsRaycaster_t304947617::get_offset_of_hitComparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (HitComparer_t1984117280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (GvrPointerScrollInput_t3738414627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[17] = 
{
	0,
	0,
	GvrPointerScrollInput_t3738414627::get_offset_of_inertia_2(),
	GvrPointerScrollInput_t3738414627::get_offset_of_decelerationRate_3(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrPointerScrollInput_t3738414627::get_offset_of_scrollHandlers_15(),
	GvrPointerScrollInput_t3738414627::get_offset_of_scrollingObjects_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (ScrollInfo_t1733197845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[6] = 
{
	ScrollInfo_t1733197845::get_offset_of_isScrollingX_0(),
	ScrollInfo_t1733197845::get_offset_of_isScrollingY_1(),
	ScrollInfo_t1733197845::get_offset_of_initScroll_2(),
	ScrollInfo_t1733197845::get_offset_of_lastScroll_3(),
	ScrollInfo_t1733197845::get_offset_of_scrollVelocity_4(),
	ScrollInfo_t1733197845::get_offset_of_scrollSettings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (GvrScrollSettings_t33837608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[2] = 
{
	GvrScrollSettings_t33837608::get_offset_of_inertiaOverride_2(),
	GvrScrollSettings_t33837608::get_offset_of_decelerationRateOverride_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (GvrAllEventsTrigger_t2174159095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[7] = 
{
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerClick_2(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerDown_3(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerUp_4(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerEnter_5(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerExit_6(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnScroll_7(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_listenersAdded_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (TriggerEvent_t1225966107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (GvrEventExecutor_t2551165091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[1] = 
{
	GvrEventExecutor_t2551165091::get_offset_of_eventTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (EventDelegate_t481206256), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (GvrPointerInputModule_t4124566278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[4] = 
{
	GvrPointerInputModule_t4124566278::get_offset_of_vrModeOnly_8(),
	GvrPointerInputModule_t4124566278::get_offset_of_scrollInput_9(),
	GvrPointerInputModule_t4124566278::get_offset_of_U3CImplU3Ek__BackingField_10(),
	GvrPointerInputModule_t4124566278::get_offset_of_U3CEventExecutorU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (GvrPointerInputModuleImpl_t2260544298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[9] = 
{
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CModuleControllerU3Ek__BackingField_0(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CEventExecutorU3Ek__BackingField_1(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CVrModeOnlyU3Ek__BackingField_2(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CScrollInputU3Ek__BackingField_3(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CCurrentEventDataU3Ek__BackingField_4(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_pointer_5(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_lastPose_6(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_isPointerHovering_7(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_isActive_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (GvrCardboardHelpers_t3063197799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (GvrEditorEmulator_t2879390294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (GvrSettings_t2768269135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (ViewerPlatformType_t1613185256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	ViewerPlatformType_t1613185256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (UserPrefsHandedness_t3496962503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2532[4] = 
{
	UserPrefsHandedness_t3496962503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (GvrUnitySdkVersion_t2768813923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (GvrHeadset_t1874684537), -1, sizeof(GvrHeadset_t1874684537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2534[7] = 
{
	GvrHeadset_t1874684537_StaticFields::get_offset_of_instance_2(),
	GvrHeadset_t1874684537::get_offset_of_headsetProvider_3(),
	GvrHeadset_t1874684537::get_offset_of_headsetState_4(),
	GvrHeadset_t1874684537::get_offset_of_standaloneUpdate_5(),
	GvrHeadset_t1874684537::get_offset_of_waitForEndOfFrame_6(),
	GvrHeadset_t1874684537::get_offset_of_safetyRegionDelegate_7(),
	GvrHeadset_t1874684537::get_offset_of_recenterDelegate_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (OnSafetyRegionEvent_t980662704), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (OnRecenterEvent_t1755824842), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t1803228010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[4] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t1803228010::get_offset_of_U24this_0(),
	U3CEndOfFrameU3Ec__Iterator0_t1803228010::get_offset_of_U24current_1(),
	U3CEndOfFrameU3Ec__Iterator0_t1803228010::get_offset_of_U24disposing_2(),
	U3CEndOfFrameU3Ec__Iterator0_t1803228010::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (GvrEventType_t1628138291)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2538[5] = 
{
	GvrEventType_t1628138291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (GvrRecenterEventType_t513699134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	GvrRecenterEventType_t513699134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (GvrRecenterFlags_t370890970)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2540[2] = 
{
	GvrRecenterFlags_t370890970::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (GvrErrorType_t1921600730)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2541[6] = 
{
	GvrErrorType_t1921600730::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (GvrSafetyRegionType_t1943469016)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2542[3] = 
{
	GvrSafetyRegionType_t1943469016::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (HeadsetProviderFactory_t520764709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (gvr_feature_t1054564930)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2544[2] = 
{
	gvr_feature_t1054564930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (gvr_property_type_t1812416635)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2545[6] = 
{
	gvr_property_type_t1812416635::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (gvr_value_type_t2156477760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2546[15] = 
{
	gvr_value_type_t2156477760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (gvr_recenter_flags_t2414511954)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2547[2] = 
{
	gvr_recenter_flags_t2414511954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (DummyHeadsetProvider_t3229995161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[1] = 
{
	DummyHeadsetProvider_t3229995161::get_offset_of_dummyState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (EditorHeadsetProvider_t660777464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[1] = 
{
	EditorHeadsetProvider_t660777464::get_offset_of_dummyState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (HeadsetState_t378905490)+ sizeof (RuntimeObject), sizeof(HeadsetState_t378905490 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2550[7] = 
{
	HeadsetState_t378905490::get_offset_of_eventType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_eventFlags_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_eventTimestampNs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_recenterEventType_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_recenterEventFlags_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_recenteredPosition_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_recenteredRotation_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (InstantPreview_t778898416), -1, sizeof(InstantPreview_t778898416_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2552[8] = 
{
	0,
	InstantPreview_t778898416_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_3(),
	0,
	InstantPreview_t778898416::get_offset_of_OutputResolution_5(),
	InstantPreview_t778898416::get_offset_of_MultisampleCount_6(),
	InstantPreview_t778898416::get_offset_of_BitRate_7(),
	InstantPreview_t778898416::get_offset_of_InstallApkOnRun_8(),
	InstantPreview_t778898416::get_offset_of_InstantPreviewApk_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (Resolutions_t3321708501)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2553[4] = 
{
	Resolutions_t3321708501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (ResolutionSize_t3677657137)+ sizeof (RuntimeObject), sizeof(ResolutionSize_t3677657137 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2554[2] = 
{
	ResolutionSize_t3677657137::get_offset_of_width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ResolutionSize_t3677657137::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (MultisampleCounts_t552109702)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2555[5] = 
{
	MultisampleCounts_t552109702::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (BitRates_t2681405699)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2556[7] = 
{
	BitRates_t2681405699::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (UnityRect_t2898233164)+ sizeof (RuntimeObject), sizeof(UnityRect_t2898233164 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2557[4] = 
{
	UnityRect_t2898233164::get_offset_of_right_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityRect_t2898233164::get_offset_of_left_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityRect_t2898233164::get_offset_of_top_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityRect_t2898233164::get_offset_of_bottom_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (UnityEyeViews_t678228735)+ sizeof (RuntimeObject), sizeof(UnityEyeViews_t678228735 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2558[4] = 
{
	UnityEyeViews_t678228735::get_offset_of_leftEyePose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityEyeViews_t678228735::get_offset_of_rightEyePose_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityEyeViews_t678228735::get_offset_of_leftEyeViewSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityEyeViews_t678228735::get_offset_of_rightEyeViewSize_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (InstantPreviewHelper_t1993029064), -1, sizeof(InstantPreviewHelper_t1993029064_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2559[1] = 
{
	InstantPreviewHelper_t1993029064_StaticFields::get_offset_of_AdbPath_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (GvrKeyboardEvent_t3629165438)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2560[9] = 
{
	GvrKeyboardEvent_t3629165438::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (GvrKeyboardError_t3210682397)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2561[5] = 
{
	GvrKeyboardError_t3210682397::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (GvrKeyboardInputMode_t518947509)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2562[3] = 
{
	GvrKeyboardInputMode_t518947509::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (GvrKeyboard_t2536560201), -1, sizeof(GvrKeyboard_t2536560201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2563[18] = 
{
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_instance_2(),
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_keyboardProvider_3(),
	GvrKeyboard_t2536560201::get_offset_of_keyboardState_4(),
	GvrKeyboard_t2536560201::get_offset_of_keyboardUpdate_5(),
	GvrKeyboard_t2536560201::get_offset_of_errorCallback_6(),
	GvrKeyboard_t2536560201::get_offset_of_showCallback_7(),
	GvrKeyboard_t2536560201::get_offset_of_hideCallback_8(),
	GvrKeyboard_t2536560201::get_offset_of_updateCallback_9(),
	GvrKeyboard_t2536560201::get_offset_of_enterCallback_10(),
	GvrKeyboard_t2536560201::get_offset_of_isKeyboardHidden_11(),
	0,
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_threadSafeCallbacks_13(),
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_callbacksLock_14(),
	GvrKeyboard_t2536560201::get_offset_of_keyboardDelegate_15(),
	GvrKeyboard_t2536560201::get_offset_of_inputMode_16(),
	GvrKeyboard_t2536560201::get_offset_of_useRecommended_17(),
	GvrKeyboard_t2536560201::get_offset_of_distance_18(),
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (StandardCallback_t3095007891), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (EditTextCallback_t1702213000), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (ErrorCallback_t2310212740), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (KeyboardCallback_t3330588312), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (U3CExecuterU3Ec__Iterator0_t892308174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[4] = 
{
	U3CExecuterU3Ec__Iterator0_t892308174::get_offset_of_U24this_0(),
	U3CExecuterU3Ec__Iterator0_t892308174::get_offset_of_U24current_1(),
	U3CExecuterU3Ec__Iterator0_t892308174::get_offset_of_U24disposing_2(),
	U3CExecuterU3Ec__Iterator0_t892308174::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (GvrKeyboardDelegateBase_t30895224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (GvrKeyboardIntent_t3874861606), -1, sizeof(GvrKeyboardIntent_t3874861606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2570[3] = 
{
	0,
	0,
	GvrKeyboardIntent_t3874861606_StaticFields::get_offset_of_theInstance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (KeyboardCallback_t4011255843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (KeyboardProviderFactory_t3069358895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (AndroidNativeKeyboardProvider_t4081466012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[21] = 
{
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_renderEventFunction_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_keyboard_context_9(),
	0,
	0,
	0,
	0,
	0,
	0,
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_mode_16(),
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_editorText_17(),
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_worldMatrix_18(),
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_isValid_19(),
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_isReady_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (gvr_clock_time_point_t2797008802)+ sizeof (RuntimeObject), sizeof(gvr_clock_time_point_t2797008802 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2575[1] = 
{
	gvr_clock_time_point_t2797008802::get_offset_of_monotonic_system_time_nanos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (gvr_recti_t2249612514)+ sizeof (RuntimeObject), sizeof(gvr_recti_t2249612514 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2576[4] = 
{
	gvr_recti_t2249612514::get_offset_of_left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gvr_recti_t2249612514::get_offset_of_right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gvr_recti_t2249612514::get_offset_of_bottom_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gvr_recti_t2249612514::get_offset_of_top_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (DummyKeyboardProvider_t4235634217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[2] = 
{
	DummyKeyboardProvider_t4235634217::get_offset_of_dummyState_0(),
	DummyKeyboardProvider_t4235634217::get_offset_of_U3CEditorTextU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (EmulatorKeyboardProvider_t2389719130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[7] = 
{
	EmulatorKeyboardProvider_t2389719130::get_offset_of_stub_0(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_showing_1(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_keyboardCallback_2(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_editorText_3(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_mode_4(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_worldMatrix_5(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_isValid_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (KeyboardState_t4109162649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[5] = 
{
	KeyboardState_t4109162649::get_offset_of_editorText_0(),
	KeyboardState_t4109162649::get_offset_of_mode_1(),
	KeyboardState_t4109162649::get_offset_of_isValid_2(),
	KeyboardState_t4109162649::get_offset_of_isReady_3(),
	KeyboardState_t4109162649::get_offset_of_worldMatrix_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (Pose3D_t2649470188), -1, sizeof(Pose3D_t2649470188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2580[4] = 
{
	Pose3D_t2649470188_StaticFields::get_offset_of_FLIP_Z_0(),
	Pose3D_t2649470188::get_offset_of_U3CPositionU3Ek__BackingField_1(),
	Pose3D_t2649470188::get_offset_of_U3COrientationU3Ek__BackingField_2(),
	Pose3D_t2649470188::get_offset_of_U3CMatrixU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (MutablePose3D_t3352419872), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (GvrInfo_t2187998870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[2] = 
{
	GvrInfo_t2187998870::get_offset_of_text_0(),
	GvrInfo_t2187998870::get_offset_of_numLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (GvrDropdown_t2772907210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[1] = 
{
	GvrDropdown_t2772907210::get_offset_of_currentBlocker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (GvrActivityHelper_t700161863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (GvrCursorHelper_t4026897861), -1, sizeof(GvrCursorHelper_t4026897861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2585[2] = 
{
	GvrCursorHelper_t4026897861_StaticFields::get_offset_of_cachedHeadEmulationActive_0(),
	GvrCursorHelper_t4026897861_StaticFields::get_offset_of_cachedControllerEmulationActive_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (GvrDaydreamApi_t820520409), -1, sizeof(GvrDaydreamApi_t820520409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2586[5] = 
{
	0,
	0,
	0,
	0,
	GvrDaydreamApi_t820520409_StaticFields::get_offset_of_m_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (U3CLaunchVrHomeAsyncU3Ec__AnonStorey0_t1042273844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[1] = 
{
	U3CLaunchVrHomeAsyncU3Ec__AnonStorey0_t1042273844::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (GvrIntent_t255451010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (GvrMathHelpers_t769385329), -1, sizeof(GvrMathHelpers_t769385329_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2589[1] = 
{
	GvrMathHelpers_t769385329_StaticFields::get_offset_of_sphericalCoordinatesResult_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (GvrUIHelpers_t853958893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (GvrVideoPlayerTexture_t3546202735), -1, sizeof(GvrVideoPlayerTexture_t3546202735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[31] = 
{
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoPlayerPtr_2(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoPlayerEventBase_3(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_initialTexture_4(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_surfaceTexture_5(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoMatrixRaw_6(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoMatrix_7(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoMatrixPropertyId_8(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_lastVideoTimestamp_9(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_initialized_10(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_texWidth_11(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_texHeight_12(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_lastBufferedPosition_13(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_framecount_14(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_screen_15(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_renderEventFunction_16(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_playOnResume_17(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_onEventCallbacks_18(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_onExceptionCallbacks_19(),
	GvrVideoPlayerTexture_t3546202735_StaticFields::get_offset_of_ExecuteOnMainThread_20(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_statusText_21(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoType_22(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoURL_23(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoContentID_24(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoProviderId_25(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_initialResolution_26(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_adjustAspectRatio_27(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_useSecurePath_28(),
	0,
	GvrVideoPlayerTexture_t3546202735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_30(),
	GvrVideoPlayerTexture_t3546202735_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_31(),
	GvrVideoPlayerTexture_t3546202735_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (VideoType_t2491562340)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2592[4] = 
{
	VideoType_t2491562340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (VideoResolution_t1062057780)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2593[6] = 
{
	VideoResolution_t1062057780::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (VideoPlayerState_t3323603301)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2594[6] = 
{
	VideoPlayerState_t3323603301::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (VideoEvents_t3555787859)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2595[6] = 
{
	VideoEvents_t3555787859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (StereoMode_t1039127149)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2596[5] = 
{
	StereoMode_t1039127149::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (RenderCommand_t1121160834)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2597[8] = 
{
	RenderCommand_t1121160834::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (OnVideoEventCallback_t2376626694), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (OnExceptionCallback_t1696428116), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
